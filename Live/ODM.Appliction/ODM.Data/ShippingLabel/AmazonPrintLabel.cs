﻿﻿using MarketplaceWebServiceOrders;
using MarketplaceWebServiceOrders.Model;
using Microsoft.ApplicationBlocks.Data;
using MWSClientCsRuntime;
using MWSMerchantFulfillmentService;
using MWSMerchantFulfillmentService.Model;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace ODM.Data.ShippingLabel
{
    public class AmazonPrintLabel
    {
        SqlConnection connection = new SqlConnection();
        Image imgLabel;
        // Developer AWS access key
        string accessKey = "",
            // Developer AWS secret key
        secretKey = "",
            // The client application name
        appName = "",
            // The client application version
        appVersion = "",
            // The endpoint for region service and version (see developer guide)
            // ex: https://mws.amazonservices.com
        serviceURL = "",
            //Developer AWS seller ID
        sellerID = "",
        AmazonOrderID = "",
        MappedCourierName = string.Empty;

        /// <summary>
        /// To call the create shipment API and calling other APIs which are requried before Create Shipment API call
        /// </summary>
        /// <param name="objDtOrderDetail">Data Table with the details of order which we got on Process Order Screen</param>
        /// <param name="OrderID">Order Id for which we are going to make API calls</param>
        /// <param name="ShipmentID">Shipment id is required for the Cancel Shipment API call and we used it as ref because
        /// we need to store the in DB for Cancel shipment API call</param>
        /// <returns>Tracking Number of an Order</returns>
        public string ExecuteAmazonShippingAPI(DataTable objDtOrderDetail, Guid OrderID, ref Guid ShipmentID)
        {
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            List<linnworks.scripting.core.ReturnClass.ShippingLabel> lstShippingLabel = new List<linnworks.scripting.core.ReturnClass.ShippingLabel>();
            DataTable objDt = new DataTable();
            DataTable objDtSenderDetails = new DataTable();
            string strTrackingNumber = "";
            GlobalVariablesAndMethods.OrderIdToLog = OrderID;
            try
            {
                ShipmentID = Guid.NewGuid();
                AmazonOrderID = Convert.ToString(objDtOrderDetail.Rows[0]["ReferenceNum"]);
                objDt = SetGlobalVarForAmazonAPI(OrderID);
                objDtSenderDetails = GetAmazonSenderDetails();
                // Create a configuration object for Order API calss
                MarketplaceWebServiceOrdersConfig Orderconfig = new MarketplaceWebServiceOrdersConfig();
                Orderconfig.ServiceURL = serviceURL;
                MarketplaceWebServiceOrders.MarketplaceWebServiceOrders Orderclient = new MarketplaceWebServiceOrdersClient(accessKey, secretKey, appName, appVersion, Orderconfig);
                MarketplaceWebServiceOrdersSample Ordersample = new MarketplaceWebServiceOrdersSample(Orderclient);
                //

                MarketplaceWebServiceOrdersSample.SellerID = sellerID;
                MarketplaceWebServiceOrdersSample.AmazonOrderId = this.AmazonOrderID;

                GlobalVariablesAndMethods.LogHistory("Calling InvokeListOrderItems API call");
                ListOrderItemsResponse objOrderItemResponse = Ordersample.InvokeListOrderItems();
                GlobalVariablesAndMethods.LogHistory("API call InvokeListOrderItems completed");

                MWSMerchantFulfillmentServiceSample.OrderItemID = objOrderItemResponse.ListOrderItemsResult.OrderItems[0].OrderItemId;
                MWSMerchantFulfillmentServiceSample.SellerID = sellerID;

                // Create a configuration object for Fulfillment services
                MWSMerchantFulfillmentServiceConfig config = new MWSMerchantFulfillmentServiceConfig();
                config.ServiceURL = serviceURL;
                MWSMerchantFulfillmentService.MWSMerchantFulfillmentService client = new MWSMerchantFulfillmentServiceClient(accessKey, secretKey, appName, appVersion, config);
                MWSMerchantFulfillmentServiceSample objFulfillmentService = new MWSMerchantFulfillmentServiceSample(client);
                //

                MWSMerchantFulfillmentServiceSample.AmazonOrderID = this.AmazonOrderID;

                for (int rowIndex = 0; rowIndex < objDtOrderDetail.Rows.Count; rowIndex++)
                {
                    var item = new MWSMerchantFulfillmentService.Model.Item();
                    item.OrderItemId = objOrderItemResponse.ListOrderItemsResult.OrderItems[rowIndex].OrderItemId;
                    item.Quantity = Convert.ToInt32(objDtOrderDetail.Rows[rowIndex]["Quantity"]);
                    objFulfillmentService.AddItemToItemList(item);
                }

                objFulfillmentService.Item.OrderItemId = objOrderItemResponse.ListOrderItemsResult.OrderItems[0].OrderItemId;
                objFulfillmentService.Item.Quantity = Convert.ToInt32(objDtOrderDetail.Rows[0]["Quantity"]);

                objFulfillmentService.Address.Name = Convert.ToString(objDtSenderDetails.Rows[0]["Name"]);
                objFulfillmentService.Address.AddressLine1 = Convert.ToString(objDtSenderDetails.Rows[0]["AddressLine1"]);
                objFulfillmentService.Address.Email = Convert.ToString(objDtSenderDetails.Rows[0]["Email"]);
                objFulfillmentService.Address.City = Convert.ToString(objDtSenderDetails.Rows[0]["City"]);
                objFulfillmentService.Address.PostalCode = Convert.ToString(objDtSenderDetails.Rows[0]["PostalCode"]);
                objFulfillmentService.Address.CountryCode = Convert.ToString(objDtSenderDetails.Rows[0]["CountryCode"]);
                objFulfillmentService.Address.Phone = Convert.ToString(objDtSenderDetails.Rows[0]["Phone"]).RemoveCharacters(")(,+- ");

                objFulfillmentService.PackageDimensions.Height = Convert.ToInt32(objDtOrderDetail.Rows[0]["Height"]);
                objFulfillmentService.PackageDimensions.Width = Convert.ToInt32(objDtOrderDetail.Rows[0]["Width"]);
                objFulfillmentService.PackageDimensions.Length = Convert.ToInt32(objDtOrderDetail.Rows[0]["Depth"]);
                objFulfillmentService.PackageDimensions.Unit = "centimeters";

                objFulfillmentService.ShippingServiceOptions.DeliveryExperience = "DeliveryConfirmationWithoutSignature";
                objFulfillmentService.ShippingServiceOptions.CarrierWillPickUp = true;

                objFulfillmentService.Weight.Value = Convert.ToInt32(Convert.ToDecimal(objDtOrderDetail.Rows[0]["TotalWeight"]) * 1000);
                objFulfillmentService.Weight.Unit = "g";

                GlobalVariablesAndMethods.LogHistory("Calling InvokeGetEligibleShippingServices API call");

                var mappedCourier = (string)objDt.Rows[0]["MappedCourierName"] ?? string.Empty;
                mappedCourier = mappedCourier.Equals("Amazon Shipping Standard Delivery Tracked") ? "Amazon Shipping Standard Tracked" : mappedCourier;
                var amazonReturedShippingServices = objFulfillmentService.InvokeGetEligibleShippingServices(mappedCourier);
                var supportedShippingServicesList = amazonReturedShippingServices.GetEligibleShippingServicesResult.ShippingServiceList;
                if (!supportedShippingServicesList
                    .Select(x => x.ShippingServiceName)
                    .ToList().Contains(mappedCourier))
                {
                    string messageToShow = "Requested Service not found for this order" + Environment.NewLine;
                    messageToShow += Environment.NewLine + "Supported Services : ";
                    foreach (var service in supportedShippingServicesList.Select(x => x.ShippingServiceName).ToList())
                    {
                        messageToShow += Environment.NewLine + service;
                    }
                    throw new Exception(messageToShow);
                }


                GlobalVariablesAndMethods.LogHistory("API call InvokeGetEligibleShippingServices completed");

                GlobalVariablesAndMethods.LogHistory("Calling InvokeCreateShipment API call");
                CreateShipmentResponse objCreateShipmentResponse = objFulfillmentService.InvokeCreateShipment();
                string strCreateAPImsg = "API call InvokeCreateShipment completed";

                string strLabelContent = objCreateShipmentResponse.CreateShipmentResult.Shipment.Label.FileContents.Contents;
                ShipmentID = new Guid(objCreateShipmentResponse.CreateShipmentResult.Shipment.ShipmentId);
                strCreateAPImsg = strCreateAPImsg + "&&Shipment ID =" + ShipmentID.ToString();
                strTrackingNumber = objCreateShipmentResponse.CreateShipmentResult.Shipment.TrackingId;
                strCreateAPImsg = strCreateAPImsg + "&&Tracking Number =" + strTrackingNumber;
                GlobalVariablesAndMethods.LogHistory(strCreateAPImsg);
                ConvertBase64ToPdf(strLabelContent);
                PrintLabelAmazon();
                //objFulfillmentService.InvokeCancelShipment();
            }
            catch (Exception ex)
            {
                if (MwsAQCall.StrErrorResponse != "")
                {
                    GlobalVariablesAndMethods.LogHistory("Error returned by Amazon API&&" + MwsAQCall.StrErrorResponse);
                    ThrowCustomException();
                }
                else
                {
                    throw ex;
                }
            }
            finally
            {
                connection.Close();
            }
            return strTrackingNumber;
        }

        /// <summary>
        /// Setting all the Global and required variables which Amazon is using to creat an API call
        /// </summary>
        /// <param name="OrderID">Order ID to get details from db for Amazon APIs</param>
        /// <returns>Data Table with the details of a specific Order</returns>
        private DataTable SetGlobalVarForAmazonAPI(Guid OrderID)
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
            }
            DataTable objDt = new DataTable();
            try
            {
                //Get and set the required setting details for Amazon from db
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@OrderId", SqlDbType.UniqueIdentifier).Value = OrderID;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPrintLabelDetailsForAmazon";
                cmd.CommandTimeout = 200;
                SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
                objAdapt.Fill(objDt);

                GlobalVariablesAndMethods.LogHistory("Setting global variables for Amazon Courier");
                sellerID = Convert.ToString(objDt.Rows[0]["SellerID"]);
                accessKey = Convert.ToString(objDt.Rows[0]["AccessKey"]);
                secretKey = Convert.ToString(objDt.Rows[0]["SecretKey"]);
                appName = Convert.ToString(objDt.Rows[0]["AppName"]);
                appVersion = Convert.ToString(objDt.Rows[0]["AppVersion"]);
                serviceURL = Convert.ToString(objDt.Rows[0]["ServiceURL"]);
                MappedCourierName = Convert.ToString(objDt.Rows[0]["MappedCourierName"]);
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Error Occurred while setting global variables for Amazon API calls&&" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }

        /// <summary>
        /// Get Amazon Sender Details for Amazon API calls
        /// </summary>
        /// <returns>DataTable result with details of Amazon Sender Details</returns>
        private DataTable GetAmazonSenderDetails()
        {
            DataSet objDs = new DataSet();
            DataTable objDtDetails = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            try
            {
                Guid dummyGuid = new Guid();//This is dummy because 
                SqlParameter[] objParam = new SqlParameter[10];

                objParam[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = dummyGuid;

                objParam[1] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                objParam[1].Value = "";

                objParam[2] = new SqlParameter("@AddressLine1", SqlDbType.VarChar, 50);
                objParam[2].Value = "";

                objParam[3] = new SqlParameter("@Email", SqlDbType.VarChar, 50);
                objParam[3].Value = "";

                objParam[4] = new SqlParameter("@City", SqlDbType.VarChar, 50);
                objParam[4].Value = "";

                objParam[5] = new SqlParameter("@PostalCode", SqlDbType.VarChar, 50);
                objParam[5].Value = "";

                objParam[6] = new SqlParameter("@CountryID", SqlDbType.UniqueIdentifier);
                objParam[6].Value = dummyGuid;

                objParam[7] = new SqlParameter("@Phone", SqlDbType.VarChar, 50);
                objParam[7].Value = "";

                objParam[8] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[8].Value = "GET";

                objParam[9] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[9].Direction = ParameterDirection.Output;

                objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateAmazonSenderDetails", objParam);
                string strResult = objParam[9].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    GlobalVariablesAndMethods.LogHistory("Exception occurred while populating Amazon Sender Details&&" + strResult);
                }
                if (objDs.Tables.Count > 0)
                {
                    objDtDetails = objDs.Tables[1];
                    if (objDtDetails.Rows.Count == 0)
                    {
                        throw new CustomException("Please insert data for sender details screen before processing Amazon Orders");
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Exception occurred while Getting Amazon Sender Details for API call&&" + ex.Message);
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return objDtDetails;
        }
        /// <summary>
        /// Cancel Amazon Order shipment
        /// </summary>
        /// <param name="OrderID">Order ID for which we want cancel shipment</param>
        public void CancelShipment(Guid OrderID)
        {
            GlobalVariablesAndMethods.OrderIdToLog = OrderID;
            GlobalVariablesAndMethods.OrderIdToLog = OrderID;
            if (connection.State == ConnectionState.Closed)
            {
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
            }
            DataTable ObjDtShipmentDetails = new DataTable();
            Guid dummyGuid = new Guid();//this just to pass to sp, we need exact shipment only when inserting data in db
            try
            {
                DataTable objDt = SetGlobalVarForAmazonAPI(OrderID);
                MWSMerchantFulfillmentServiceSample.SellerID = sellerID;
                ObjDtShipmentDetails = GlobalVariablesAndMethods.GetAddDeleteShipmentDetails(dummyGuid, "GET", OrderID);
                if (ObjDtShipmentDetails.Rows.Count > 0)
                {
                    //Create a configuration object for Fulfillment services
                    MWSMerchantFulfillmentServiceConfig config = new MWSMerchantFulfillmentServiceConfig();
                    config.ServiceURL = serviceURL;
                    MWSMerchantFulfillmentService.MWSMerchantFulfillmentService client = new MWSMerchantFulfillmentServiceClient(accessKey, secretKey, appName, appVersion, config);
                    MWSMerchantFulfillmentServiceSample objFulfillmentService = new MWSMerchantFulfillmentServiceSample(client);
                    //
                    MWSMerchantFulfillmentServiceSample.ShipmentID = Convert.ToString(ObjDtShipmentDetails.Rows[0]["ShipmentID"]);
                    GlobalVariablesAndMethods.LogHistory("Calling InvokeCancelShipment API method");
                    objFulfillmentService.InvokeCancelShipment();
                    GlobalVariablesAndMethods.LogHistory("InvokeCancelShipment API method call completed");
                }
            }
            catch (Exception ex)
            {
                if (MwsAQCall.StrErrorResponse != "")
                {
                    GlobalVariablesAndMethods.LogHistory("Server returned error for InvokeCancelShipment API method&&" + MwsAQCall.StrErrorResponse);
                    ThrowCustomException();
                }
                else
                {
                    throw ex;
                }
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// To find the custom exception from the response of Amazon courier.
        /// "Message" node is optional from the response of Amazon. So when we are getting Message node from response then we are
        /// returning Error Code and Error Message otherwise returning just error code.
        /// </summary>
        private static void ThrowCustomException()
        {
            string strErrorCode = "", strErrorMessage = "", strFinalError = "";
            strErrorCode = GlobalVariablesAndMethods.GetSubString(MwsAQCall.StrErrorResponse, "<Code>", "</Code>");
            if (MwsAQCall.StrErrorResponse.Contains("<Message>"))
            {
                strErrorMessage = GlobalVariablesAndMethods.GetSubString(MwsAQCall.StrErrorResponse, "<Message>", "</Message>");
            }
            strFinalError = strErrorMessage != "" ? "Error Code:-" + strErrorCode + " \nError Message:-" + strErrorMessage : "Error Code:-" + strErrorCode;
            throw new CustomException(strFinalError);
        }

        /// <summary>
        /// Convert and decompress the label returned from Amazon
        /// </summary>
        /// <param name="barcode">Label returned from Amazon</param>
        private void ConvertBase64ToPdf(string barcode)
        {
            try
            {
                byte[] decoded = base64_decode(barcode);
                byte[] decompressed = Decompress(decoded);
                string filepath = System.IO.Path.GetTempPath(); //get temp folder path
                Random _r = new Random();
                int n = _r.Next();
                string file_location = filepath + n + ".png";
                System.IO.File.WriteAllBytes(file_location, decompressed);
                imgLabel = System.Drawing.Image.FromFile(file_location);
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory(string.Format("Exception occurred in ConvertBase64ToPdf method,&&Exception:- {0}", ex.Message));
            }
        }

        public static byte[] base64_decode(string encodedData)
        {
            byte[] encodedDataAsBytes = Convert.FromBase64String(encodedData);
            return encodedDataAsBytes;
        }

        public static byte[] Decompress(byte[] data)
        {
            using (var compressedStream = new MemoryStream(data))
            using (var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
            using (var resultStream = new MemoryStream())
            {
                var buffer = new byte[4096];
                int read;

                while ((read = zipStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    resultStream.Write(buffer, 0, read);
                }
                return resultStream.ToArray();
            }
        }

        private void PrintPage(object o, PrintPageEventArgs e)
        {
            float MarginLeft = 0.00f;
            float MarginTop = 0.00f;
            e.Graphics.DrawImage(imgLabel, MarginLeft, MarginTop);
        }

        private void PrintLabelAmazon()
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForAmazon))
                {
                    pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForAmazon;
                }
                pd.PrintPage += PrintPage;
                pd.Print();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
