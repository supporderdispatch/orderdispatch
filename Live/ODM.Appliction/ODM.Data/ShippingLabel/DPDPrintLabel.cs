﻿using linnworks.module.dpd;
using Newtonsoft.Json;
using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;

namespace ODM.Data.ShippingLabel
{
    public class DPDPrintLabel
    {
        private void PrintPage(object o, PrintPageEventArgs e)
        {
            StringFormat sf = new StringFormat();
            Font font = new Font("Courier New", 10);// set default font size
            float fontHeight = font.GetHeight();
            int startX = 5;// Position of x-axis
            int startY = 2;//starting position of y-axis
            int Offset = 0;
            Offset = Offset + 16;
            e.Graphics.DrawString(PrintLabel.LabelToPrint, new Font("Courier New", 12, FontStyle.Bold), new SolidBrush(System.Drawing.Color.Black), startX, startY + Offset);
            Offset = Offset + 20;
            Offset = 0;
        }

        public void PrintLabelDPD()
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForDPD))
                {
                    pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForDPD;
                    PrinterHelper.SendStringToPrinter(pd.DefaultPageSettings.PrinterSettings.PrinterName, PrintLabel.LabelToPrint);
                }
                //pd.PrintPage += PrintPage;
                //pd.Print();
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }

        public string ExecuteDPDShippingAPI(Order objOrder, Guid OrderID)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            DataTable objDt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@OrderId", SqlDbType.UniqueIdentifier).Value = OrderID;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPrintLabelDetailsForDPD";
                cmd.CommandTimeout = 200;
                SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
                objAdapt.Fill(objDt);

                GlobalVariablesAndMethods.UseLive = true;
                GlobalVariablesAndMethods.APIUser = Convert.ToString(objDt.Rows[0]["UserName"]);
                GlobalVariablesAndMethods.APIPass = Convert.ToString(objDt.Rows[0]["Password"]);
                GlobalVariablesAndMethods.AccountNumber = Convert.ToString(objDt.Rows[0]["AccountNumber"]);
                string Json = PopulateClassToGetJson(objDt);
                ShippingLabelDPD objShipping = new ShippingLabelDPD();
                objShipping.InsertShipment(Json);
                PrintLabelDPD();
                return PrintLabel.TrackingNumber;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        private string PopulateClassToGetJson(DataTable objDtDetails)
        {
            try
            {
                DPDEntity objDPDEntity = new DPDEntity();
                Consignment objConsignment = new Consignment();
                ContactDetails objContactDetails = new ContactDetails();
                SenderAddress objSenderAddress = new SenderAddress();
                OrderAddress objOrderAddress = new OrderAddress();
                CollectionDetails objCollectionDetails = new CollectionDetails();
                NotificationDetails objNotificationDetails = new NotificationDetails();
                DeliveryDetails objDeliveryDetails = new DeliveryDetails();
                Parcels objParcel = new Parcels();
                List<Parcels> lstParcels = new List<Parcels>();
                parcelProduct objparcelProduct = new parcelProduct();
                List<parcelProduct> lstParcelProduct = new List<parcelProduct>();
                List<Consignment> lstConsignment = new List<Consignment>();

                decimal dcTotalCharge = Convert.ToDecimal(objDtDetails.Rows[0]["TotalCharge"]);
                string strTotalCharge = dcTotalCharge > 0 ? Convert.ToString(dcTotalCharge) : "0.01";
                objSenderAddress.organisation = Convert.ToString(objDtDetails.Rows[0]["SenderCompany"]);
                objSenderAddress.countryCode = Convert.ToString(objDtDetails.Rows[0]["SenderCountryCode"]);
                objSenderAddress.postcode = Convert.ToString(objDtDetails.Rows[0]["SenderPostCode"]);
                objSenderAddress.street = Convert.ToString(objDtDetails.Rows[0]["SenderAddress1"]);
                objSenderAddress.locality = Convert.ToString(objDtDetails.Rows[0]["SenderAddress2"]);
                objSenderAddress.town = Convert.ToString(objDtDetails.Rows[0]["SenderAddressTown"]);
                objSenderAddress.county = Convert.ToString(objDtDetails.Rows[0]["SenderCountry"]);

                objOrderAddress.organisation = Convert.ToString(objDtDetails.Rows[0]["Company"]);
                objOrderAddress.countryCode = Convert.ToString(objDtDetails.Rows[0]["AddressCountryCode"]);
                objOrderAddress.postcode = Convert.ToString(objDtDetails.Rows[0]["PostCode"]);
                objOrderAddress.street = Convert.ToString(objDtDetails.Rows[0]["Address1"]);
                objOrderAddress.locality = Convert.ToString(objDtDetails.Rows[0]["Address2"]);
                objOrderAddress.town = Convert.ToString(objDtDetails.Rows[0]["Town"]);
                objOrderAddress.county = Convert.ToString(objDtDetails.Rows[0]["AddressCountry"]);

                objNotificationDetails.email = Convert.ToString(objDtDetails.Rows[0]["EmailAddress"]);
                objNotificationDetails.mobile = Convert.ToString(objDtDetails.Rows[0]["PhoneNumber"]).RemoveCharacters(")(,+- ");

                objContactDetails.contactName = Convert.ToString(objDtDetails.Rows[0]["FullName"]);
                objContactDetails.telephone = Convert.ToString(objDtDetails.Rows[0]["PhoneNumber"]).RemoveCharacters(")(,+- ");

                objCollectionDetails.contactDetails = objContactDetails;
                objCollectionDetails.address = objSenderAddress;

                objDeliveryDetails.contactDetails = objContactDetails;
                objDeliveryDetails.address = objOrderAddress;
                objDeliveryDetails.notificationDetails = objNotificationDetails;

                objParcel.packageNumber = "1";

                objparcelProduct.productCode = Convert.ToString(objDtDetails.Rows[0]["SKU"]);
                objparcelProduct.productTypeDescription = "electrical accessory";
                objparcelProduct.productItemsDescription = Convert.ToString(objDtDetails.Rows[0]["Title"]);
                objparcelProduct.productFabricContent = "none";
                objparcelProduct.countryOfOrigin = "GB";
                objparcelProduct.productHarmonisedCode = "";
                objparcelProduct.numberOfItems = "1";
                objparcelProduct.unitWeight = Convert.ToString(objDtDetails.Rows[0]["TotalWeight"]);
                objparcelProduct.unitValue = strTotalCharge;
                lstParcelProduct.Add(objparcelProduct);
                objParcel.parcelProduct = lstParcelProduct;
                lstParcels.Add(objParcel);

                objConsignment.consignmentNumber = null;
                objConsignment.consignmentRef = null;
                objConsignment.parcel = lstParcels;
                objConsignment.collectionDetails = objCollectionDetails;
                objConsignment.deliveryDetails = objDeliveryDetails;
                objConsignment.networkCode = Convert.ToString(objDtDetails.Rows[0]["NetworkCode"]);
                objConsignment.numberOfParcels = 1;//That is fixed as Oliver told to us.
                objConsignment.totalWeight = Convert.ToDecimal(objDtDetails.Rows[0]["TotalWeight"]);
                objConsignment.shippingRef1 = Convert.ToString(objDtDetails.Rows[0]["NumOrderId"]);
                objConsignment.shippingRef2 = Convert.ToString(objDtDetails.Rows[0]["ExternalReferenceNum"]).LimitLengthTo(20);
                objConsignment.shippingRef3 = Convert.ToString(objDtDetails.Rows[0]["SecondaryReference"]).LimitLengthTo(20);
                objConsignment.customsValue = Convert.ToString(objDtDetails.Rows[0]["TotalPrice"]);
                objConsignment.deliveryInstructions = "DO NOT LEAVE WITHOUT SIGNATURE";
                objConsignment.parcelDescription = "electrical accessories";
                objConsignment.liabilityValue = null;
                objConsignment.liability = false;

                lstConsignment.Add(objConsignment);

                objDPDEntity.job_id = null;
                objDPDEntity.collectionOnDelivery = false;
                objDPDEntity.invoice = null;
                objDPDEntity.collectionDate = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                objDPDEntity.consolidate = false;
                objDPDEntity.consignment = lstConsignment;
                string strJson = JsonConvert.SerializeObject(objDPDEntity);
                return strJson;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
