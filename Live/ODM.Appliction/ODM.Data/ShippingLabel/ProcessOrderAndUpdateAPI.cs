﻿using Newtonsoft.Json;
using ODM.Data.Util;
using System;
using System.Net.Http;

namespace ODM.Data.ShippingLabel
{
    public class ProcessOrderAndUpdateAPI
    {
        public static Boolean UpdateOrderAPI(Guid OrderID, bool IsScanPerformed, Nullable<Guid> LocationId, string strTrackingNumber)
        {
            string strToken = "";
            string strShippingInfo = "";
            bool blIsOrderProcessed = false;
            string strApiResponse = "";
            string StrResponseData = "", strServer = "";
            try
            {
                //Calling of Authorize api to get the Authorization key(Token key) to call the other order dispatch APIs
                using (var AuthClient = new HttpClient())
                {
                    var urls = "https://api.linnworks.net//api/Auth/AuthorizeByApplication?applicationId=53e8b308-3563-439d-b7b7-36c4b91e132a&applicationSecret=32d0c53e-76dc-460a-b63f-bafc3f1bb6d3&token=be6c4095945b0457151129ac87f2ef9e";
                    AuthClient.BaseAddress = new Uri(urls);
                    HttpResponseMessage responsemsg = AuthClient.GetAsync(urls).Result;
                    string jsonData = responsemsg.Content.ReadAsStringAsync().Result;
                    dynamic objData = JsonConvert.DeserializeObject(jsonData);
                    strApiResponse = responsemsg.StatusCode.ToString();
                    if (objData["Token"] != null)
                    {
                        strServer = Convert.ToString(objData["Server"]);
                        strToken = Convert.ToString(objData["Token"]);
                        GlobalVariablesAndMethods.LogHistory(string.Format("Auth/AuthorizeByApplication API call status {0} and Got Token", strApiResponse));
                    }
                    else
                    {
                        GlobalVariablesAndMethods.LogHistory(string.Format("Error occurred in Auth API call, Status {0}, Response {1}", strApiResponse, jsonData));
                        throw new CustomException("Error occurred in Auth API call");
                    }
                }
                //End of Calling of Authorize api

                //Call of GetOrderById to fetch the node of shipping info so that we could pass to SetOrderShippingInfo api
                using (var Client = new HttpClient())
                {
                    HttpResponseMessage responsemsg = new HttpResponseMessage();
                    Client.DefaultRequestHeaders.Accept.Clear();
                    string urls = strServer + "//api/Orders/GetOrderById?pkOrderId=" + OrderID;
                    Client.DefaultRequestHeaders.Add("Accept", "application/*+xml;version=5.1");
                    Client.DefaultRequestHeaders.Add("Authorization", strToken);
                    Client.BaseAddress = new Uri(urls);
                    responsemsg = Client.GetAsync(urls).Result;
                    dynamic OrderData = JsonConvert.DeserializeObject(responsemsg.Content.ReadAsStringAsync().Result);
                    //string strTrackingNum = Convert.ToString(OrderData["ShippingInfo"]["TrackingNumber"]);
                    OrderData["ShippingInfo"]["TrackingNumber"] = strTrackingNumber;
                    strApiResponse = responsemsg.StatusCode.ToString();
                    if (OrderData["ShippingInfo"] != null)
                    {
                        GlobalVariablesAndMethods.LogHistory("Orders/GetOrderById?pkOrderId API call completed, Status Code:-" + strApiResponse);
                        strShippingInfo = Convert.ToString(OrderData["ShippingInfo"]);
                    }
                    else
                    {
                        GlobalVariablesAndMethods.LogHistory(string.Format("Orders/GetOrderById?pkOrderId response status:-{0}, data:-{1}", strApiResponse, Convert.ToString(OrderData)));
                    }
                }
                //End of GetOrderById

                //Call of SetOrderShippingInfo to set the tracking number of an order so that we could dispatch that
                using (var Client = new HttpClient())
                {
                    HttpResponseMessage responsemsg = new HttpResponseMessage();
                    Client.DefaultRequestHeaders.Accept.Clear();
                    string urls = strServer + "//api/Orders/SetOrderShippingInfo?orderId=" + OrderID + "&info=" + strShippingInfo;
                    Client.DefaultRequestHeaders.Add("Accept", "application/*+xml;version=5.1");
                    Client.DefaultRequestHeaders.Add("Authorization", strToken);
                    Client.BaseAddress = new Uri(urls);
                    responsemsg = Client.GetAsync(urls).Result;
                    dynamic ShippingDetails = JsonConvert.DeserializeObject(responsemsg.Content.ReadAsStringAsync().Result);
                    string strResponse = "";
                    strApiResponse = responsemsg.StatusCode.ToString();
                    StrResponseData = Convert.ToString(ShippingDetails);
                    if (ShippingDetails["Code"] == null && ShippingDetails["Message"] == null && ShippingDetails["Error"] == null)
                    {
                        strResponse = responsemsg.Content.ReadAsStringAsync().Result;
                        GlobalVariablesAndMethods.LogHistory(string.Format("/Orders/SetOrderShippingInfo?orderId API call completed, status code:-{0}", strApiResponse));
                    }
                    else if (ShippingDetails["Message"] == null && ShippingDetails["Error"] != null)
                    {
                        GlobalVariablesAndMethods.LogHistory(string.Format("/Orders/SetOrderShippingInfo?orderId API call completed with error, status code:-{0}, data:-{1}", strApiResponse, StrResponseData));
                        throw new CustomException(Convert.ToString(ShippingDetails["Error"]));
                    }
                    else
                    {
                        GlobalVariablesAndMethods.LogHistory(string.Format("/Orders/SetOrderShippingInfo?orderId API call completed with error, status code:-{0}, data:-{1}", strApiResponse, StrResponseData));
                        throw new CustomException(Convert.ToString(ShippingDetails["Message"]));
                    }
                }
                //End of SetOrderShippingInfo

                //Code to call api of Process order to mark the order shipped.
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    string urls = strServer + "//api/Orders/ProcessOrder?orderId=" + OrderID + "&scanPerformed=" + IsScanPerformed + "&locationId=" + LocationId;

                    client.DefaultRequestHeaders.Add("Accept", "application/*+xml;version=5.1");
                    client.DefaultRequestHeaders.Add("Authorization", strToken);
                    client.BaseAddress = new Uri(urls);
                    HttpResponseMessage responsemsg = client.GetAsync(urls).Result;
                    dynamic OrderProcessDetails = JsonConvert.DeserializeObject(responsemsg.Content.ReadAsStringAsync().Result);
                    strApiResponse = responsemsg.StatusCode.ToString();
                    StrResponseData = Convert.ToString(OrderProcessDetails);

                    if (OrderProcessDetails["Processed"] != null)
                    {
                        GlobalVariablesAndMethods.LogHistory("/Orders/ProcessOrder?orderId API call completed, Status Code:-" + strApiResponse);
                        blIsOrderProcessed = Convert.ToBoolean(Convert.ToString(OrderProcessDetails["Processed"]));
                    }
                    if (!blIsOrderProcessed)
                    {
                        if (OrderProcessDetails["Message"] == null && OrderProcessDetails["Error"] != null)
                        {
                            GlobalVariablesAndMethods.LogHistory(string.Format("/Orders/ProcessOrder?orderId API call completed with error, Status Code:- {0}, Data:- {1}", strApiResponse, StrResponseData));
                            string strError = Convert.ToString(OrderProcessDetails["Error"]);
                            throw new CustomException(strError);
                        }
                        else
                        {
                            GlobalVariablesAndMethods.LogHistory(string.Format("/Orders/ProcessOrder?orderId API call completed with error, Status Code:- {0}, Data:- {1}" + strApiResponse, StrResponseData));
                            throw new CustomException(Convert.ToString(OrderProcessDetails["Message"]));
                        }
                    }
                }
                //End of Code to call api of process order
                return blIsOrderProcessed;
            }
            catch (Exception Ex)
            {
                GlobalVariablesAndMethods.LogHistory("Error occurred while processing order, error message:-" + Ex.Message);
                throw Ex;
            }
        }
    }
}
