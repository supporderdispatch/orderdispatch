﻿using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;

namespace ODM.Data.ShippingLabel
{
    public class FedExPrintLabel
    {
        #region "FedEx Global Properties and Objects"
        SqlConnection connection = new SqlConnection();
        DataTable objDt = new DataTable();
        public static List<System.Drawing.Image> lstLabel { get; set; }
        public System.Drawing.Image imgLabel { get; set; }
        public string ShippingMethod { get; set; }
        public static string Key { get; set; }
        public static string AccountNumber { get; set; }
        public static string MeterNumber { get; set; }
        public static string Password { get; set; }
        public static string Url { get; set; }
        public static string LabelStockType { get; set; }
        #endregion

        #region "Methods"
        /// <summary>
        /// Event to Print image of label
        /// </summary>
        private void PrintPage(object o, PrintPageEventArgs e)
        {
            System.Drawing.Image img = imgLabel;
            float MarginLeft = 0.00f;
            float MarginTop = 0.00f;
            e.Graphics.DrawImage(img, MarginLeft, MarginTop);
        }

        /// <summary>
        /// Set printer to print label and call print event to run selected printer
        /// </summary>
        public void PrintLabel()
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForFedEx))
                {
                    pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForFedEx;
                    if (lstLabel != null)
                    {
                        for (int i = 0; i < lstLabel.Count; i++)
                        {
                            imgLabel = lstLabel[i];
                            pd.PrintPage += PrintPage;
                            pd.Print();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }

        /// <summary>
        /// Call create shipment api and get tracking number
        /// </summary>
        /// <param name="objOrder">Order details</param>
        /// <param name="OrderID">Order id to create shipment</param>
        /// <returns>Tracking number</returns>
        public string ExecuteFedExShippingAPI(Order objOrder, Guid OrderID)
        {
            try
            {
                FedExPrintLabel.lstLabel = null;
                SetVariablesForFedExApiCall(OrderID);
                ShippingLabelFedEx objFedEx = new ShippingLabelFedEx();
                string strTrackingNumber = objFedEx.CreateShipment(objDt);
                PrintLabel();
                return strTrackingNumber;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        
        /// <summary>
        /// Call cancel shipment api to cancel an order
        /// </summary>
        /// <param name="OrderID">Order id to cancel</param>
        /// <param name="strTrackingNumber">Tracking number of order to cancel</param>
        /// <returns>Cancel response</returns>
        public string CancelShipment(Guid OrderID, string strTrackingNumber)
        {
            try
            {
                SetVariablesForFedExApiCall(OrderID);
                ShippingLabelFedEx objFedEx = new ShippingLabelFedEx();
                return objFedEx.CancelShipment(objDt, strTrackingNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Set global variables that we are going to use for FedEx api calls
        /// </summary>
        /// <param name="OrderID">Order id for which we are going to call FedEx api calls</param>
        private void SetVariablesForFedExApiCall(Guid OrderID)
        {
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.Add("@OrderId", SqlDbType.UniqueIdentifier).Value = OrderID;
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetPrintLabelDetailsForFedEx";
            cmd.CommandTimeout = 200;
            SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
            objAdapt.Fill(objDt);
            bool IsLive = Convert.ToBoolean(objDt.Rows[0]["IsLive"]);
            LabelStockType = Convert.ToString(objDt.Rows[0]["LabelStockType"]);
            if (IsLive)
            {
                Key = Convert.ToString(objDt.Rows[0]["AccessKeyLive"]);
                Password = Convert.ToString(objDt.Rows[0]["PasswordLive"]);
                AccountNumber = Convert.ToString(objDt.Rows[0]["AccountNumberLive"]);
                MeterNumber = Convert.ToString(objDt.Rows[0]["MeterNumberLive"]);
                Url = "https://ws.fedex.com/web-services/ship";
            }
            else
            {
                Key = Convert.ToString(objDt.Rows[0]["AccessKeyTest"]);
                Password = Convert.ToString(objDt.Rows[0]["PasswordTest"]);
                AccountNumber = Convert.ToString(objDt.Rows[0]["AccountNumberTest"]);
                MeterNumber = Convert.ToString(objDt.Rows[0]["MeterNumberTest"]);
                Url = "https://wsbeta.fedex.com:443/web-services";
            }
        }
        #endregion
    }
}
