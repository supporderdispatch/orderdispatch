﻿using GhostscriptSharp;
using GhostscriptSharp.Settings;
using ODM.Data.Entity;
using ODM.Data.Util;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Xml;

namespace linnworks.finaware.CommonData.Objects                // leave untouched                            
{                                                                                               // leave untouched                            
    public class ShippingLabelMacroSpring                        // leave untouched                            
    {
        // leave untouched                            
        static string defaultWeightInKg;
        static bool verboseLogging;
        static string APIKey;
        static string LoginName;
        static string Password;
        static string TestUrl = "http://testapi.iabol.com/api/abolapi.asmx?wsdl";
        static string LiveUrl = "https://eu.iabol.com/spring/api/abolapi.asmx?wsdl";
        static string root;
        public List<linnworks.scripting.core.ReturnClass.ShippingLabel> Initialize(linnworks.scripting.core.Debugger debug, SqlConnection ActiveConnection, Order order, linnworks.scripting.core.Classes.OptionCollection ServiceOptions)           // leave untouched                            
        {
            initialise(debug);
            return getShippingLabel(order);
        }
        public ShippingLabelMacroSpring()
        {
            defaultWeightInKg = GlobalVariablesAndMethods.strDefaultWeightInKg;
            verboseLogging = GlobalVariablesAndMethods.blVerboseLogging;
            APIKey = GlobalVariablesAndMethods.strApiToken;
            LoginName = GlobalVariablesAndMethods.APIUser;
            Password = GlobalVariablesAndMethods.APIPass;
            root = GlobalVariablesAndMethods.UseLive ? LiveUrl : TestUrl;
        }
        // This is the weight in KG that will be used if the parcel weight cannot be determined from the order or is zero.
        // Every parcel must have a weight, and the weight affects the price of the parcel.
        // You should ensure that your orders and inventory correctly specify a weight so that this default will not be used.

        
        // -------------------------------------------------------------------------------------------------------------
        //
        // Initialisation
        //
        // -------------------------------------------------------------------------------------------------------------


        /**
        * Performs initialisation prior to script execution, eg loading preferences.
        *
        */
        public static void initialise(linnworks.scripting.core.Debugger debug)
        {
            createWorkingDirectory();
            clearLogFile();
            LOG = new Logger(debug);
        }

        /**
        * Find the working directory and create it if not found.
        *
        */
        public static void createWorkingDirectory()
        {
            if (!verboseLogging) return;
            workingDirectory = getHomeDirectory() + "/linnworks";
            if (!System.IO.Directory.Exists(workingDirectory))
            {
                System.IO.Directory.CreateDirectory(workingDirectory);
            }
        }

        /**
        * Find the user's home directory.
        *
        */
        public static string getHomeDirectory()
        {
            return (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX)
                ? Environment.GetEnvironmentVariable("HOME")
                : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
        }

        // -------------------------------------------------------------------------------------------------------------
        //
        // API configuration
        //
        // -------------------------------------------------------------------------------------------------------------

        /**
        * Get the full URI for a parcels request.
        *
        */
        public static string getParcelsUri()
        {
            return root;
        }

        // -------------------------------------------------------------------------------------------------------------
        //
        // Get Labels
        //
        // -------------------------------------------------------------------------------------------------------------


        /**
        * Export an order to create a parcel in Spring, returning shipping labels.
        *
        */
        public static List<linnworks.scripting.core.ReturnClass.ShippingLabel> getShippingLabel(Order order)
        {
            LOG.blankLines(2);
            LOG.Log("Executing with script version '" + scriptVersion + "'");
            string parcelSummaries = exportToParcels(order);
            return getShippingLabelFor(order, parcelSummaries);
        }

        /**
        * Convert Order to 'parcels' XML, submit to Spring to create a parcel, obtaining tracking barcodes.
        *
        */
        public static string exportToParcels(Order order)
        {
            LOG.Log("Exporting order with id '" + order.NumOrderId + "' to myHermes parcels");
            string parcelXML = parcelXMLFor(order);
            LOG.Log("Converted order to XML:");
            LOG.Log(parcelXML);
            string parcelSummaries = postForString(getParcelsUri(), parcelXML);
            LOG.Log(parcelSummaries);
            LOG.Log("Finished exporting order with id '" + order.NumOrderId + "' to myHermes parcels");

            // This if block is used for custom error handling
            if (parcelSummaries.ToLower().Contains("error"))
            {
                string strError = GlobalVariablesAndMethods.GetSubString(parcelSummaries, "<Message>", "</Message>");
                if (strError.Trim().ToUpper() != "SUCCESS")
                {
                    throw (new SpringCustomException(strError));
                }
            }
            //End of custom error handling

            return parcelSummaries;
        }

        /**
        * Get a shipping label for the tracking barcode.
        *
        */
        public static List<linnworks.scripting.core.ReturnClass.ShippingLabel> getShippingLabelFor(Order order, string parcelSummaries)
        {
            LOG.Log("Retrieving shipping label for order with id '" + order.NumOrderId + "'");

            string barcode = firstValueOf(parcelSummaries);
            LOG.Log("Got TrackingNbr: " + barcode);

            string labelBase64 = GetLabel(parcelSummaries);
            LOG.Log("Got Label1"); //: " + labelBase64);				

            List<linnworks.scripting.core.ReturnClass.ShippingLabel> labels = new List<linnworks.scripting.core.ReturnClass.ShippingLabel>();
            labels.Add(labelFor(barcode, labelBase64));

            LOG.Log("Finished retrieving shipping label for order with id '" + order.NumOrderId + "'");
            return labels;
        }

        /**
        * Get a label image for the given barcode and create the Linnworks shipping label.
        *
        */
        public static linnworks.scripting.core.ReturnClass.ShippingLabel labelFor(string tracking, string barcode)
        {
            byte[] bytes = Convert.FromBase64String(barcode);
            System.Drawing.Image image;
            System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes);
            image = System.Drawing.Image.FromStream(ms);
            //	string file_location = "\\WORK\\Users\\Public\\Shipping\\test.jpeg"; //image location
            //	image.Save(workingDirectory, System.Drawing.Imaging.ImageFormat.Jpeg); //save the label image
            return new linnworks.scripting.core.ReturnClass.ShippingLabel(tracking, "", image);
        }


        // -------------------------------------------------------------------------------------------------------------
        //
        // Data Mapping
        //
        // -------------------------------------------------------------------------------------------------------------


        /**
        * Convert an Order to XML.
        *
        */
        public static string parcelXMLFor(Order order)
        {
            Dictionary<String, String> parameters = new Dictionary<string, string>();
            parameters.Add("BillType", "102"); //Bill Sender
            parameters.Add("DriverInstructions", ""); // can be disabled?
            parameters.Add("ResidentialFlag", "false");
            parameters.Add("SaturdayDeliveryFlag", "false");
            parameters.Add("SaturdayPickupFlag", "false");
            parameters.Add("LabelType", "6"); //PNG
            parameters.Add("LabelSize", "0");  //4X6
            parameters.Add("HolidayDeliveryFlag", "false");
            parameters.Add("SundayDeliveryFlag", "false");
            parameters.Add("DimensionUnit", "CM");
            parameters.Add("WeightUnit", "KG");
            parameters.Add("ShipDate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss-00:00"));
            parameters.Add("ServiceCode", SelectServiceCode(order.ShippingMethod)); //Service code, need changing
            parameters.Add("DeclaredValue", "5");
            parameters.Add("CurrencyCode", "USD");
            parameters.Add("DimWeight", "0");
            parameters.Add("Length", "1");
            parameters.Add("Height", "1");
            parameters.Add("Width", "1");
            parameters.Add("PackagingType", "105");
            parameters.Add("ReferenceNumber", order.NumOrderId.ToString());
            parameters.Add("Weight", getParcelWeightInKg(order).ToString());
            parameters.Add("AddlPackageReference1", order.ReferenceNum.ToString());
            parameters.Add("ContentDescription", "Accessories");
            parameters.Add("ImageRotation", "0");
            parameters.Add("EveningDeliveryFlag", "false");
            parameters.Add("ReturnType", "0");
            parameters.Add("DeliveryConfirmationFlag", "false");
            parameters.Add("SignatureConfirmationFlag", "false");
            parameters.Add("SignatureConfirmationType", "100");
            parameters.Add("InsuranceAmount", "0"); // Do you need to set any in insurance below 3 fields?
            parameters.Add("InsurancePayType", "0");
            parameters.Add("InsuranceType", "0");
            parameters.Add("EndorsementType", "0"); // ??
            parameters.Add("OverrideShipToAddressValidationFlag", "false");
            parameters.Add("CN22DescriptionType", "104");

            List<string> list = new List<string>();
            list.Add(getCompany(order.Company.ToString()));
            list.Add(order.FullName.ToString());
            list.Add(order.Address1.ToString());
            list.Add(order.Address2.ToString());
            list.Add(order.Town.ToString());
            list.Add(order.PostCode.ToString().ToUpper());
            list.Add(order.Country.ToString());

            String[] processedAddress = processAddress(list, 30);

            Dictionary<String, String> toAddress = new Dictionary<string, string>();
            toAddress.Add("Company", processedAddress[0]);
            toAddress.Add("Attn", processedAddress[1]);
            toAddress.Add("AddressLine1", processedAddress[2]);
            toAddress.Add("AddressLine2", processedAddress[3]);
            toAddress.Add("City", processedAddress[4]);
            toAddress.Add("StateCode", GlobalVariablesAndMethods.getStateCode(order.Region.ToString(), order.Country.ToString()));
            toAddress.Add("Zip", processedAddress[5]);
            toAddress.Add("Phone", getPhone(order.BuyerPhoneNumber.ToString()));
            toAddress.Add("CountryCode", order.CountryCode.ToString());
            toAddress.Add("ResidentialFlag", "false");
            toAddress.Add("IsPOBox", "false");

            //Please fill up the requrn address below
            Dictionary<String, String> returnAddress = new Dictionary<string, string>();
            returnAddress.Add("Company", "Electrolve Ltd");
            returnAddress.Add("Attn", "Returns Department");
            returnAddress.Add("AddressLine1", "148 Caistor Road");
            returnAddress.Add("City", "Laceby");
            returnAddress.Add("StateCode", "");
            returnAddress.Add("Zip", "DN37 7JG");
            returnAddress.Add("Phone", "0800 33 45 379");
            returnAddress.Add("CountryCode", "GB");
            returnAddress.Add("ResidentialFlag", "false");
            returnAddress.Add("IsPOBox", "false");

            Dictionary<String, String> shipItem = new Dictionary<string, string>();
            shipItem.Add("Code", order.NumOrderId.ToString());
            shipItem.Add("CN22DescriptionType", "104");
            shipItem.Add("Description", "Accessories");
            shipItem.Add("Weight", getParcelWeightInKg(order).ToString());
            shipItem.Add("UnitWeight", "KG");
            shipItem.Add("Price", "5");
            shipItem.Add("Quantity", "1");
            shipItem.Add("UnitQuantity", "pcs");
            shipItem.Add("UnitPrice", "5");
            shipItem.Add("CountryOfManufacture", "US");
            shipItem.Add("SalesTaxAmount", "0");
            shipItem.Add("DutyAmount", "0");
            shipItem.Add("Amount", "0");

            XmlDocument soapEnvelopeXml = new XmlDocument();

            String xml = @"<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:web=""http://abolsoft.com/webservices/"">
                           <soap:Header/>
                           <soap:Body>
                              <web:AbolSimpleShipment xmlns=""http://abolsoft.com/webservices/"">
                                 <web:AbolSimpleShipment>
                                    <web:AbolSimpleShip>
                                       <web:Request>
                                          <web:Authentication>
                                             <web:ActivationKey>" + APIKey + @"</web:ActivationKey>
                                             <web:LoginName>" + LoginName + @"</web:LoginName>
                                             <web:Password>" + Password + @"</web:Password>
                                          </web:Authentication>
                                          <web:SimpleShipment>";
            foreach (KeyValuePair<String, String> item in parameters)
            {
                xml += Environment.NewLine + "<web:" + item.Key + ">" + item.Value + @"</web:" + item.Key + ">";
            }

            xml += Environment.NewLine + "<web:ShipTo>";
            foreach (KeyValuePair<String, String> item in toAddress)
            {
                xml += Environment.NewLine + "<web:" + item.Key + ">" + item.Value + @"</web:" + item.Key + ">";
            }
            xml += Environment.NewLine + @"</web:ShipTo>  
                        <web:Return>";


            foreach (KeyValuePair<String, String> item in returnAddress)
            {
                xml += Environment.NewLine + "<web:" + item.Key + ">" + item.Value + @"</web:" + item.Key + ">";
            }
            xml += Environment.NewLine + @"</web:Return>                   
                     <web:SimpleExportLineItemList>
                        <web:SimpleExportLineItem>";
            foreach (KeyValuePair<String, String> item in shipItem)
            {
                xml += Environment.NewLine + "<web:" + item.Key + ">" + item.Value + @"</web:" + item.Key + ">";
            }

            xml += @"</web:SimpleExportLineItem>
                     </web:SimpleExportLineItemList>
                  </web:SimpleShipment>
               </web:Request>
            </web:AbolSimpleShip>
         </web:AbolSimpleShipment>
      </web:AbolSimpleShipment>
   </soap:Body>
</soap:Envelope>";

            return xml;
        }

        public static string getParcelWeightInKg(Order order)
        {
            if (order.Pack_TotalWeight == null || order.Pack_TotalWeight <= 0.0)
            {
                return useDefaultWeight();
            }
            return (order.Pack_TotalWeight * 0.8).ToString();
        }

        public static string useDefaultWeight()
        {
            LOG.Log("Order total weight not set or is zero. Using defaultWeightInKg which is set to '" + defaultWeightInKg + "'");
            double defaultWeightNumber = double.Parse(defaultWeightInKg.Trim(), System.Globalization.NumberStyles.AllowDecimalPoint);
            return defaultWeightNumber.ToString();
        }

        /**
        * Item description includes the item title and the quantity, and the whole description is limited to a maximum length.
        * eg an order for 1 apple and 2 bananas will have a description "apple, banana x2". Two properties control this method:
        * minDescriptionQuantity sets the smallest quantity at which any description is shown.
        * minMultiplierQuantity sets the smallest quantity at which the multiplier is shown.
        * eg if minDescriptionQuantity=1 and an item has quantity 0, it will not appear in the description.
        * eg if minMultiplierQuantity=2 and an item has quantity 1, it will show as a title without multiplier.
        * eg if the above two settings are true and an item has quantity 2, it will show as 'title xQuantity'.
        * If either property is set to a negative value, quantity will not affect the output.
        */
        public static string getItemDescriptionFor(List<OrderItem> orderItems)
        {
            System.Text.StringBuilder descriptionBuilder = new System.Text.StringBuilder();
            foreach (OrderItem item in orderItems)
            {
                LOG.Log(item.Title + " - " + item.Qty);
                if (minDescriptionQuantity >= 0 && item.Qty < minDescriptionQuantity)
                {
                    continue;
                }

                string itemTitle = titleFor(item);
                string multiplier = multiplierFor(item);

                descriptionBuilder.Append(itemTitle + multiplier);
                descriptionBuilder.Append(", ");
            }

            string description = descriptionBuilder.ToString();

            if (description.Length > MAX_DESCRIPTION_LENGTH)
            {
                return description.Substring(0, MAX_DESCRIPTION_LENGTH);

            }
            else if (description.EndsWith(", "))
            {
                return description.Remove(description.Length - 2, 2).ToString();
            }

            if (description.Length <= 0)
            {
                return "Not specified";
            }
            return description;
        }

        /**
        * Generate the item title to use in the parcel description.
        *
        */
        public static string titleFor(OrderItem item)
        {
            if (item.Title.Length <= 0)
            {
                return "Unknown";
            }
            return item.Title;
        }

        /**
        * Generate the multiplier to use for the item in the parcel description.
        *
        */
        public static string multiplierFor(OrderItem item)
        {
            if (minMultiplierQuantity >= 0 && item.Qty < minMultiplierQuantity)
            {
                return "";
            }
            return " x" + item.Qty;
        }

        /**
        * Limits reference number to max allowed length.
        *
        */
        public static string getReference(Order order)
        {
            string refNum = order.ReferenceNum.ToString();
            if (refNum.Length > MAX_REFERENCE_LENGTH)
            {
                return refNum.Substring(0, MAX_REFERENCE_LENGTH);
            }
            return refNum;
        }

        /**
        * Attempts to find the value of the first occurrence of the given property.
        *
        */
        public static string firstValueOf(string XML)
        {
            System.Xml.XmlDocument responseXML = new System.Xml.XmlDocument();
            responseXML.LoadXml(XML);
            string data = "";

            foreach (XmlNode n in responseXML.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes)
            {
                if (n.Name == "AlternateTrackingNumber")
                    data = n.InnerText;
                else
                    if (n.Name == "TrackingNbr")
                        data = n.InnerText;
            }
            return data;
        }

        public static string GetLabel(string XML)
        {
            System.Xml.XmlDocument responseXML = new System.Xml.XmlDocument();
            responseXML.LoadXml(XML);
            string data = "";
            foreach (XmlNode n in responseXML.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes)
            {
                if (n.Name == "LabelList")
                    data = n.ChildNodes[0].InnerText;
            }
            return data;
        }

        /**
        * Count how many times the given property appears in the XML.
        *
        */
        public static int countOccurrences(string key, string XML)
        {
            int count = 0;
            foreach (string s in XML.Split(','))
            {
                if (s.Contains(key))
                {
                    count++;
                }
            }
            return count;
        }


        // -------------------------------------------------------------------------------------------------------------
        //
        // Networking
        //
        // -------------------------------------------------------------------------------------------------------------


        /**
        * Post content to uri as contentType, returning response content as string if it is acceptable content type.
        *
        */
        public static string postForString(string uri, string content)
        {
            configureSsl();
            System.Net.HttpWebRequest request = createRequest(uri);

            request.Method = "POST";
            request.ContentType = "application/soap+xml;charset=UTF-8: ";
            setRequestContent(request, content);

            System.Net.HttpWebResponse response = execute(request);

            string responseString = getResponseAsString(response);
            LOG.Log("Response Received"); //Content: " + responseString);					
            return responseString;
        }

        /**
        * Construct an http request for the given uri. Will fail if URI syntax is not valid, but does not try to access the URI.
        *
        */
        public static System.Net.HttpWebRequest createRequest(string uri)
        {
            LOG.Log("Creating request for uri: " + uri);
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(uri);
            LOG.Log("Created request");
            return request;
        }
        public static System.Net.HttpWebRequest createRequestForVoidManifest(string uri)
        {
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(uri);
            LOG.Log("Created request");
            return request;
        }
        /**
        * Perform an http request. Will fail for unreachable host,not found, etc
        *
        */
        public static System.Net.HttpWebResponse execute(System.Net.HttpWebRequest request)
        {
            logRequest(request);
            System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
            LOG.Log("Response Status: " + response.StatusCode.ToString());
            return response;
        }


        /**
        * Put the content into the http request.
        *
        */
        public static void setRequestContent(System.Net.HttpWebRequest request, string content)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(content);
            request.ContentLength = bytes.Length;
            System.IO.Stream os = request.GetRequestStream();
            os.Write(bytes, 0, bytes.Length);
            os.Close();
        }

        /**
        * Read response content into a string.
        *
        */
        public static string getResponseAsString(System.Net.HttpWebResponse response)
        {
            try
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                return sr.ReadToEnd();

            }
            catch (Exception e)
            {
                failWithError("Failed to read content from http response", e);
                return null;
            }
        }

        /**
        * Apply configuration to avoid SSL errors.
        *
        */
        public static void configureSsl()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllSslCertificates);
        }

        /**
        * Ignore all SSL certificate errors.
        *
        */
        public static bool AcceptAllSslCertificates(
                object sender,
                System.Security.Cryptography.X509Certificates.X509Certificate certification,
                System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {

            return true;
        }



        // -------------------------------------------------------------------------------------------------------------
        //
        // Logging
        //
        // -------------------------------------------------------------------------------------------------------------

        /**
        * Get the current log file. Current log is appended to until full, then it is copied to the backup, replacing previous backup.
        *
        */
        public static string getLogFileName()
        {
            return workingDirectory + "/SpringGlobal.txt";
        }

        /**
        * Get the backup log file. 
        *
        */
        public static string getBackupLogFileName()
        {
            return workingDirectory + "/log-backup.txt";
        }

        /**
        * Keep log file below max size.
        *
        */
        public static void clearLogFile()
        {
            if (!verboseLogging) return;
            if (System.IO.File.Exists(getLogFileName()))
            {
                System.IO.FileInfo logFileInfo = new System.IO.FileInfo(getLogFileName());
                if (logFileInfo.Length > LOG_FILE_LENGTH)
                {
                    if (System.IO.File.Exists(getBackupLogFileName()))
                    {
                        System.IO.File.Delete(getBackupLogFileName());
                    }
                    System.IO.File.Move(getLogFileName(), getBackupLogFileName());
                }
            }
        }

        /**
        * Log the details of an http request, including headers.
        *
        */
        public static void logRequest(System.Net.HttpWebRequest request)
        {

            LOG.Log("Executing request");

            System.Net.WebHeaderCollection headers = request.Headers;

            for (int i = 0; i < headers.Count; ++i)
            {
                string header = headers.GetKey(i);

                foreach (string value in headers.GetValues(i))
                {
                    LOG.Log("Request header <" + header + "> with value <" + value + ">");
                }
            }
        }


        /**
        * Log the given message with a date and time stamp, and report the same message to debug output.
        *
        */
        public class Logger
        {
            private linnworks.scripting.core.Debugger debug;
            public Logger(linnworks.scripting.core.Debugger debug)
            {
                this.debug = debug;
            }

            /**
            * Output a number of blank lines in the log for clear separation between activities.
            *
            */
            public void blankLines(int number)
            {
                for (int i = 0; i < number; i++)
                {
                    Log("");
                }
            }

            /**
            * Log the message with a date and timestamp, also reporting to debugger console.
            *
            */
            public void Log(string msg)
            {
                debug.AddEntry(msg);
                if (!verboseLogging) return;
                using (System.IO.StreamWriter w = System.IO.File.AppendText(getLogFileName()))
                {
                    DateTime now = DateTime.Now;
                    w.WriteLine("{0} {1} - {2}", now.ToLongDateString(), now.ToLongTimeString(), msg);
                    w.WriteLine("");
                }

                //Storing log messages of linnwork those have length less that 200 chars and don't have header
                if (!string.IsNullOrEmpty(msg) && msg.Length <= 200 && !msg.Contains("Request header <") && !msg.Contains(".pdf"))
                {
                    GlobalVariablesAndMethods.LogHistory(msg);
                }
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        //
        // Error Handling
        //
        // -------------------------------------------------------------------------------------------------------------

        /**
        * Helps distinguish our own exceptions from standard ones in debug output etc.
        *
        */
        public class HermesException : Exception
        {
            public HermesException(String msg) : base(msg) { }
        }

        /**
        * Report a message to the user and the log file. Report details and exception details to the log file.
        *
        */
        public static void failWithError(String msg, string details, Exception e)
        {
            LOG.Log(msg);
            LOG.Log(details);
            LOG.Log(e.Message);
            LOG.Log(e.StackTrace);
            throw new HermesException(msg);
        }

        /**
        * Report a message to the user and the log file. Report exception details to the log file.
        *
        */
        public static void failWithError(String msg, Exception e)
        {
            LOG.Log(msg);
            LOG.Log(e.GetType().ToString());
            LOG.Log(e.Message);
            LOG.Log(e.StackTrace);
            throw new HermesException(msg);
        }

        /**
        * Report a message to the user and the log file. Report details to the log file.
        *
        */
        public static void failWithError(String msg, string details)
        {
            LOG.Log(msg);
            LOG.Log(details);
            throw new HermesException(msg);
        }

        /**
        * Report a message to the user and the log file. Prefer other methods giving more details for the log file.
        *
        */
        public static void failWithError(String msg)
        {
            LOG.Log(msg);
            throw new HermesException(msg);
        }

        public static string SelectServiceCode(string ShippingMethod)
        {
            Dictionary<string, string> SelectServiceCode = new Dictionary<string, string>();
            SelectServiceCode.Add("Spring Packet", "1351");
            SelectServiceCode.Add("Spring Packet Plus", "1352");
            SelectServiceCode.Add("Spring Parcel", "1353");
            SelectServiceCode.Add("Spring Packet Lite", "1356");
            SelectServiceCode.Add("Spring Packet Tracked", "1357");
            string serviceCode = SelectServiceCode[ShippingMethod];
            return serviceCode;
        }

        public static string getCompany(string company)
        {
            if (company == "")
            {
                return "no company";
            }
            else
            {
                return company;
            }
        }

        public static string getPhone(string phone)
        {
            if (phone == "")
            {
                return "07814994875";
            }
            else
            {
                return phone;
            }
        }

        

        private static String[] processAddress(List<String> input, Int16 cap)
        {
            String[] output = new String[7];

            //through error for really long values Except Name
            if (input[0].Trim().Length > cap * 2 || input[2].Trim().Length + input[3].Trim().Length > cap * 2 || input[4].Trim().Length > cap * 2 || input[5].Trim().Length > cap * 2 || input[6].Trim().Length > cap * 2)
                throw new Exception("Address need manual correction!");

            String address1 = String.Empty;
            String address2 = String.Empty;
            String town = input[4].Trim();

            if (input[2].Trim().Length <= cap && input[3].Trim().Length <= cap)
            {
                address1 = input[2].Trim();
                address2 = input[3].Trim();
            }
            else
            {
                String[] addressPieces = (input[2].Trim() + " " + input[3].Trim()).Split(new Char[] { ' ' });

                foreach (String item in addressPieces)
                {
                    String tempStr = item.Trim();
                    if (tempStr.Length > 0)
                    {
                        if (address1.Length + tempStr.Length + 1 < cap)
                        {
                            if (address1.Length == 0)
                                address1 += tempStr;
                            else
                                address1 += " " + tempStr;
                        }
                        else if (address2.Length + tempStr.Length + 1 < cap)
                        {
                            if (address2.Length == 0)
                                address2 += tempStr;
                            else
                                address2 += " " + tempStr;
                        }
                        else if (town.Length + tempStr.Length + 1 < cap)
                        {
                            if (town.Length == 0)
                                town = tempStr;
                            else
                                town = tempStr + " " + town;
                        }
                    }
                }
            }
            output[0] = input[0].Trim().Length <= cap ? input[0].Trim() : input[0].Trim().Substring(0, cap); //Company
            output[1] = input[1].Trim().Length <= cap ? input[1].Trim() : input[1].Trim().Substring(0, cap); //Name
            output[2] = address1; //Address 1
            output[3] = address2; //Address 2
            output[4] = town; //Town
            output[5] = input[5].Trim().Length <= cap ? input[5].Trim() : input[5].Trim().Substring(0, cap); //Postcode
            output[6] = input[6].Trim().Length <= cap ? input[6].Trim() : input[6].Trim().Substring(0, cap); //Country

            return output;
        }


        private string XmlForVoidManifest(string TrackingNumber)
        {
            XmlDocument soapEnvelopeXml = new XmlDocument();
            String xml = @"<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:web=""http://abolsoft.com/webservices/"">
                           <soap:Header/>
                           <soap:Body>
                                 <web:AbolVoidPackage>
                                    <web:AbolVoid>
                                          <web:Authentication>
                                             <web:ActivationKey>" + APIKey + @"</web:ActivationKey>
                                             <web:LoginName>" + LoginName + @"</web:LoginName>
                                             <web:Password>" + Password + @"</web:Password>
                                          </web:Authentication>
                                          <web:Void>
                                             <web:TrackingNumber>" + TrackingNumber + @"</web:TrackingNumber>
                                          </web:Void>
                                    </web:AbolVoid>
                                 </web:AbolVoidPackage>
                           </soap:Body>
                           </soap:Envelope>";
            return xml;
        }
        public void VoidManifestOrders(string TrackingNumbers, linnworks.scripting.core.Debugger debug)
        {
            initialise(debug);
            string VoidXml = XmlForVoidManifest(TrackingNumbers);
            string VoidSummaries = postForString(getParcelsUri(), VoidXml);
           
            string strMessage = GlobalVariablesAndMethods.GetSubString(VoidSummaries, "<message>", "</message>");

            if (strMessage.Trim().ToUpper() != "SUCCESS")
            {
                throw (new SpringCustomException(strMessage));
            }
        }

        private string XmlForFileManifest(string[] TrackingNumber)
        {
            XmlDocument soapEnvelopeXml = new XmlDocument();
            String xml = @"<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:web=""http://abolsoft.com/webservices/"">
                           <soap:Header/>
                           <soap:Body>
                                 <web:AbolCloseOut>
                                    <web:AbolCloseout>
                                      <web:request>
                                          <web:Authentication>
                                             <web:ActivationKey>" + APIKey + @"</web:ActivationKey>
                                             <web:LoginName>" + LoginName + @"</web:LoginName>
                                             <web:Password>" + Password + @"</web:Password>
                                          </web:Authentication>
                                          <web:Closeout>
                                            <web:RequestType>CARRIERID</web:RequestType>
                                            <web:RequestDetail>SPRING</web:RequestDetail>
                                            <web:CloseOutTrackingNbrs>" + TrackingNumber + @"</web:CloseOutTrackingNbrs>
                                          </web:Closeout>
                                       </web:request>
                                    </web:AbolCloseout>
                                 </web:AbolCloseOut>
                           </soap:Body>
                           </soap:Envelope>";
            return xml;
        }
        public System.Drawing.Image FileManifestOrders(string[] TrackingNumbers, linnworks.scripting.core.Debugger debug)
        {
            initialise(debug);
            string VoidXml = XmlForFileManifest(TrackingNumbers);
            string VoidSummaries = postForString(getParcelsUri(), VoidXml);
            string strMessage = GlobalVariablesAndMethods.GetSubString(VoidSummaries, "<message>", "</message>");
            if (strMessage.Trim().ToUpper() != "SUCCESS")
            {
                throw (new SpringCustomException(strMessage));
            }
            else
            {
               return SavePdfToLocal(VoidSummaries);
            }   
        }

        private System.Drawing.Image SavePdfToLocal(string strResponse)
        {
            string Response = strResponse;
            string labelDirectory = System.IO.Path.GetTempPath() + "MySpringLabels";
            string strURL = GlobalVariablesAndMethods.GetSubString(Response, "<URL>", "</URL>");
            string strRandom = Guid.NewGuid().ToString();
            string strFileName = labelDirectory + "\\" + strRandom + ".pdf";
            string strImagePath = labelDirectory + "\\" + strRandom + ".png";

            if (!System.IO.Directory.Exists(labelDirectory))
            {
                System.IO.Directory.CreateDirectory(labelDirectory);
            }
            using (var wc = new System.Net.WebClient())
            {
                wc.DownloadFile(strURL, strFileName);
            }
            return LoadImagePdf(strFileName, strImagePath);
        }

        public static System.Drawing.Image LoadImagePdf(string InputPDFFile, string OutPath)
        {
            string strOutputPath = OutPath;
            PdfDocument pdf = PdfReader.Open(InputPDFFile, PdfDocumentOpenMode.Import);
            int point1 = (int)pdf.Pages[0].Height.Point;
            int point2 = (int)pdf.Pages[0].Width.Point;
            GhostscriptSettings settings = new GhostscriptSettings();
            settings.Device = GhostscriptDevices.png256;
            settings.Size = new GhostscriptPageSize()
            {
                Manual = new Size(point2, point1)
            };
            Size size = new Size(270, 270);
            settings.Resolution = size;
            GhostscriptWrapper.GenerateOutput(InputPDFFile, strOutputPath, settings);
            Bitmap bitmap = new Bitmap(strOutputPath);
            try
            {
                System.IO.File.Delete(InputPDFFile);
                //System.IO.File.Delete(strOutputPath);
            }
            catch
            {
            }
            System.Drawing.Image imgLabel = System.Drawing.Image.FromFile(strOutputPath);
            return imgLabel;
        }

        // -------------------------------------------------------------------------------------------------------------
        //
        // System Properties - DO NOT CHANGE
        //
        // -------------------------------------------------------------------------------------------------------------

        // SYSTEM EXECUTION PROPERTIES
        static string scriptVersion = "1";

        // DESCRIPTION PROPERTIES
        // Add item to parcel description only when item quantity is this value or more. Set to -1 to always show.
        static int minDescriptionQuantity = -1;
        // Add multiplier to this item in parcel description only when item quantity is this value or more.  Set to -1 to always show.
        static int minMultiplierQuantity = 2;

        // FIXED PROPERTIES						
        static int MAX_DESCRIPTION_LENGTH = 255;
        static int MAX_REFERENCE_LENGTH = 32;
        static int LOG_FILE_LENGTH = 500000;
        static Logger LOG;

        static string workingDirectory;


        // -------------------------------------------------------------------------------------------------------------
        //
        // END - DO NOT EDIT BELOW HERE
        //
        // -------------------------------------------------------------------------------------------------------------						

        /**
        * This is just to keep the uneditable next line out of the way
        *
        */
        public class nothing
        {


            // COPY TO THIS LINE  

        }
        // leave untouched                            

        //Class to ganerate custom error if we got in response of Spring api call
        public class SpringCustomException : Exception
        {
            public SpringCustomException(string strErrorMsg)
                : base(strErrorMsg)
            {

            }
        }
        // leave untouched 
    }   // leave untouched                            
}      // leave untouched                            


