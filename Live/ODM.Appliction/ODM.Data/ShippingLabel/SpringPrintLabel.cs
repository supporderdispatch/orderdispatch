﻿using linnworks.finaware.CommonData.Objects;
using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;

namespace ODM.Data.ShippingLabel
{
    public class SpringPrintLabel
    {
        public List<linnworks.scripting.core.ReturnClass.ShippingLabel> Shipping { get; set; }
        public static System.Drawing.Image ManifestLabel { get; set; }
        private void PrintPage(object o, PrintPageEventArgs e)
        {
            System.Drawing.Image img = null;
            float MarginLeft = 0.00f;
            float MarginTop = 0.00f;
            if (Shipping != null && Shipping.Count > 0)
            {
                img = Shipping[0].LabelImage;
            }
            else if (ManifestLabel != null)
            {
                img = ManifestLabel;
                MarginLeft = -26.00f;
                MarginTop = 0.00f;
            }
            e.Graphics.DrawImage(img, MarginLeft, MarginTop);
        }

        public void PrintLabel()
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                if (Shipping != null && Shipping.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForSpring))
                    {
                        pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForSpring;
                    }
                }
                else if (ManifestLabel != null)
                {
                    if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForSpringManifest))
                    {
                        pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForSpringManifest;
                    }
                }

                pd.PrintPage += PrintPage;
                pd.Print();
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }

        public string ExecuteSpringShippingAPI(Order objOrder, Guid OrderID)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            List<linnworks.scripting.core.ReturnClass.ShippingLabel> lstShippingLabel = new List<linnworks.scripting.core.ReturnClass.ShippingLabel>();
            DataTable objDt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@OrderId", SqlDbType.UniqueIdentifier).Value = OrderID;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPrintLabelDetailsForSpring";
                cmd.CommandTimeout = 200;
                SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
                objAdapt.Fill(objDt);
                bool IsLiveMode = Convert.ToBoolean(objDt.Rows[0]["IsLive"]);
                if (IsLiveMode)
                {
                    GlobalVariablesAndMethods.UseLive = true;
                    GlobalVariablesAndMethods.strApiToken = Convert.ToString(objDt.Rows[0]["LiveActivationKey"]);
                    GlobalVariablesAndMethods.APIUser = Convert.ToString(objDt.Rows[0]["LiveLoginName"]);
                    GlobalVariablesAndMethods.APIPass = Convert.ToString(objDt.Rows[0]["LivePassword"]);
                }
                else
                {
                    GlobalVariablesAndMethods.UseLive = false;
                    GlobalVariablesAndMethods.strApiToken = Convert.ToString(objDt.Rows[0]["TestActivationKey"]);
                    GlobalVariablesAndMethods.APIUser = Convert.ToString(objDt.Rows[0]["TestLoginName"]);
                    GlobalVariablesAndMethods.APIPass = Convert.ToString(objDt.Rows[0]["TestPassword"]);
                }
                GlobalVariablesAndMethods.blVerboseLogging = Convert.ToBoolean(objDt.Rows[0]["IsBoseLogin"]);
                GlobalVariablesAndMethods.strDefaultWeightInKg = Convert.ToString(objDt.Rows[0]["DefaultWeight"]);

                ShippingLabelMacroSpring objMacro = new ShippingLabelMacroSpring();
                SqlConnection con = new SqlConnection();
                linnworks.scripting.core.Debugger objDebug = new linnworks.scripting.core.Debugger();
                linnworks.scripting.core.Classes.OptionCollection ServiceOptions = new linnworks.scripting.core.Classes.OptionCollection();
                lstShippingLabel = objMacro.Initialize(objDebug, con, objOrder, ServiceOptions);
                Shipping = lstShippingLabel;
                PrintLabel();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            if (lstShippingLabel.Count > 0)
            {
                return lstShippingLabel[0].TrackingNumber;
            }
            else
            {
                return "";
            }
        }
    }
}
