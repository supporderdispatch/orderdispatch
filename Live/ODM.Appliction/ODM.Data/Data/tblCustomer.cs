//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ODM.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCustomer
    {
        public tblCustomer()
        {
            this.tblAddresses = new HashSet<tblAddress>();
            this.tblOrders = new HashSet<tblOrder>();
        }
    
        public System.Guid CustomerId { get; set; }
        public string ChannelBuyerName { get; set; }
        public string EmailAddress { get; set; }
        public string Town { get; set; }
        public string FullName { get; set; }
        public string Company { get; set; }
        public System.Guid CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual ICollection<tblAddress> tblAddresses { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
    }
}
