﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ODM.Data.Data;
using System.Text;
using System.Threading.Tasks;
using ODM.Data;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Entity;
namespace ODM.Data.Service
{
    public class UserService : IUserService
    {
        private string conString;
        public UserService(string connStr)
        {
            conString = connStr;
        }

        User IUserService.CheckUserLogin(string loginName, string password)
        {

            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@LoginName", SqlDbType.VarChar);
            if (!string.IsNullOrEmpty(loginName))
            {
                param[0].Value = loginName;
            }
            else { param[0].Value = DBNull.Value; }

            param[1] = new SqlParameter("@Password", SqlDbType.VarChar);
            if (!string.IsNullOrEmpty(password))
            {
                param[1].Value = password;
            }
            else { param[1].Value = DBNull.Value; }

            User userobj = null;
            using (SqlConnection sqlConn = new SqlConnection(conString))
            {
                DataSet dtUser = SqlHelper.ExecuteDataset(sqlConn, CommandType.StoredProcedure, "[CheckLoginStatus]", param);
                if (dtUser != null && dtUser.Tables.Count > 0 && dtUser.Tables[0].Rows.Count > 0)
                {
                    userobj = new User();
                    Guid userid;
                    userobj.Firstname = dtUser.Tables[0].Rows[0]["Firstname"].ToString();
                    userobj.Lastname = dtUser.Tables[0].Rows[0]["Lastname"].ToString();
                    Guid.TryParse(dtUser.Tables[0].Rows[0]["Userid"].ToString(), out userid);
                    userobj.Userid = userid;
                    bool isAdmin = false;
                    //bool.TryParse(dtUser.Tables[0].Columns["IsAdmin"].ToString(), out isAdmin);
                    bool.TryParse(dtUser.Tables[0].Rows[0]["IsAdmin"].ToString(), out isAdmin);
                    //userobj.IsAdmin = true;
                    userobj.IsAdmin = isAdmin;
                    userobj.Loginname = dtUser.Tables[0].Rows[0]["Loginname"].ToString();
                }
            }
            return userobj;
        }
        int IUserService.ChangePassword(Guid userid, string password)
        {
            int result;
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@Userid", SqlDbType.UniqueIdentifier);
            if (userid != null)
            {
                param[0].Value = userid;
            }
            else { param[0].Value = DBNull.Value; }

            param[1] = new SqlParameter("@Password", SqlDbType.VarChar, 200);
            if (!string.IsNullOrEmpty(password))
            {
                param[1].Value = password;
            }
            else { param[1].Value = DBNull.Value; }

            using (SqlConnection sqlConn = new SqlConnection(conString))
            {
                result = SqlHelper.ExecuteNonQuery(sqlConn, CommandType.StoredProcedure, "[UserPasswordChange]", param);

            }
            return result;

        }

        string IUserService.AddUser(string loginName, string pass, string firstname, string lastname, string email, string phone, bool isAdmin, Guid createdBy)
        {

            try
            {
                SqlParameter[] param = new SqlParameter[10];
                string result = string.Empty;
                param[0] = new SqlParameter("@LoginName", SqlDbType.NVarChar, 100);
                if (!string.IsNullOrEmpty(loginName))
                {
                    param[0].Value = loginName;
                }
                else { param[0].Value = DBNull.Value; }

                param[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 100);
                if (!string.IsNullOrEmpty(pass))
                {
                    param[1].Value = pass;
                }
                else { param[1].Value = DBNull.Value; }

                param[2] = new SqlParameter("@Firstname", SqlDbType.NVarChar, 100);
                if (!string.IsNullOrEmpty(firstname))
                {
                    param[2].Value = firstname;
                }
                else { param[2].Value = DBNull.Value; }
                param[3] = new SqlParameter("@Lastname", SqlDbType.NVarChar, 100);
                if (!string.IsNullOrEmpty(lastname))
                {
                    param[3].Value = lastname;
                }
                else { param[3].Value = DBNull.Value; }

                param[4] = new SqlParameter("@Email", SqlDbType.NVarChar, 100);
                if (!string.IsNullOrEmpty(email))
                {
                    param[4].Value = email;
                }
                else { param[4].Value = DBNull.Value; }

                param[5] = new SqlParameter("@Phone", SqlDbType.NVarChar, 100);
                if (!string.IsNullOrEmpty(phone))
                {
                    param[5].Value = phone;
                }
                else { param[5].Value = DBNull.Value; }

                param[6] = new SqlParameter("@IsAdmin", SqlDbType.Bit);
                param[6].Value = isAdmin;


                param[7] = new SqlParameter("@CreatedBy", SqlDbType.UniqueIdentifier);
                param[7].Value = createdBy;

                param[8] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                param[8].Value = "N";

                param[9] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                param[9].Direction = ParameterDirection.Output;

                SqlConnection sqlConn = new SqlConnection(conString);

                SqlHelper.ExecuteNonQuery(sqlConn, CommandType.StoredProcedure, "[UserAddUpdateDelete]", param);
                result = param[9].Value.ToString();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        List<User> IUserService.GetAllUsers()
        {
            List<User> userList = new List<Entity.User>();
            using (SqlConnection sqlConn = new SqlConnection(conString))
            {
                SqlDataReader drUser = SqlHelper.ExecuteReader(sqlConn, CommandType.StoredProcedure, "[GetAllUsers]");
                while (drUser.Read())
                {
                    User user = new User();
                    Guid gid;
                    Guid.TryParse(drUser["userid"].ToString(), out gid);
                    user.Userid = gid;

                    user.Loginname = drUser["Loginname"].ToString();
                    userList.Add(user);
                }
                //result = param[9].Value.ToString();
            }
            return userList;
        }
    }
}
