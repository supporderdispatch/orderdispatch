﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WBS.BussinessLayer
{
    public sealed class TableNameConstants
    {
       
        //public const string SwipeRecords = "SwipeRecords_Read"; 
        public const string manufactureOrder = "GetAllManufactureOrder"; 
        public const string Person = "GetAllPerson"; 
        public const string Resource = "GetAllResources"; 
        public const string Timesheet = "GetTimeSheet"; 
        public const string Products = "GetAllProducts"; 
        public const string BOMProducts = "GetAllBOMaterial";
        public const string BOMaterial = "GetAllBOMaterialChild"; 
        public const string Location = "GetAllLocation";
        public const string stockMovement = "GetAllStockMovement";
        public const string stockMovementDetails = "GetAllStockMovementDetails";
        public const string PurchaseOrder = "GetAllPurchaseOrder";
        public const string SalesOrder = "GetAllSalesOrder";
        public const string LocationDetail = "GetAllLocationDetails";
        public const string SalesOrderItem = "GetAllSalesOrderItem";
        public const string PurchaseOrderItemDetail = "GetAllPurchaseOrderItem";
        public const string StockLocation = "GetAllStockLocation";
        public const string Supplier = "GetAllSupplier";
        public const string Customer = "GetAllCustomer";
        public const string ManufactureOrderDetail = "GetAllManufactureOrderDetail";
        public const string batch = "GetAllBatch";
        public const string batchDetail = "GetAllBatchDetail";
        public const string QueueInfo = "GetAllQueue";
        public const string Scanning = "SovScanning";
        public const string ColumnsDetail = "GetAllColumnsDetail";
        public const string ReadCompany = "GetAllCompany";
        public const string ReadUser = "GetAllUser";
        public const string ScreenDetail = "GetAllScreenDetail";
        public const string UserGroup = "GetAllUserGroup";
        public const string Invoice = "GetAllInvoice";
        public const string InvoiceItemPopUp = "GetAllInvoiceChildPopUp";
        public const string HideUnHideColumnsDetail = "GetAllHide-UnHideColumnsDetail";
        public const string EditColumnsDescription = "GetAllColumnsDescription";
        public const string ManufactureOrderDetailPopUp = "GetAllManufactureOrderDetailPopUp";
        public const string SalesOrderItemPopUp = "GetAllSalesOrderItemPopUp";
       

    }
}
