﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace linnworks.module.dpd
{
    public class PrinterHelper
    {
        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, int level, [MarshalAs(UnmanagedType.LPStruct), In] PrinterHelper.DOCINFOA di);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, int dwCount, out int dwWritten);

        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, int dwCount)
        {
            int dwWritten = 0;
            IntPtr hPrinter = new IntPtr(0);
            PrinterHelper.DOCINFOA di = new PrinterHelper.DOCINFOA();
            bool flag = false;
            di.pDocName = "Linnworks DPD ZPL";
            di.pDataType = "RAW";
            if (PrinterHelper.OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
            {
                if (PrinterHelper.StartDocPrinter(hPrinter, 1, di))
                {
                    if (PrinterHelper.StartPagePrinter(hPrinter))
                    {
                        flag = PrinterHelper.WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        PrinterHelper.EndPagePrinter(hPrinter);
                    }
                    PrinterHelper.EndDocPrinter(hPrinter);
                }
                PrinterHelper.ClosePrinter(hPrinter);
            }
            if (!flag)
                Marshal.GetLastWin32Error();
            return flag;
        }

        public static bool SendFileToPrinter(string szPrinterName, string szFileName)
        {
            FileStream fileStream = new FileStream(szFileName, FileMode.Open);
            BinaryReader binaryReader = new BinaryReader((Stream)fileStream);
            byte[] numArray = new byte[fileStream.Length];
            IntPtr num1 = new IntPtr(0);
            int int32 = Convert.ToInt32(fileStream.Length);
            byte[] source = binaryReader.ReadBytes(int32);
            IntPtr num2 = Marshal.AllocCoTaskMem(int32);
            int startIndex = 0;
            IntPtr destination = num2;
            int length = int32;
            Marshal.Copy(source, startIndex, destination, length);
            int num3 = PrinterHelper.SendBytesToPrinter(szPrinterName, num2, int32) ? 1 : 0;
            Marshal.FreeCoTaskMem(num2);
            return num3 != 0;
        }

        public static bool SendStringToPrinter(string szPrinterName, string szString)
        {
           // int length = szString.Length;
            int length = (szString.Length + 1) * Marshal.SystemMaxDBCSCharSize;
            IntPtr coTaskMemAnsi = Marshal.StringToCoTaskMemAnsi(szString);
            PrinterHelper.SendBytesToPrinter(szPrinterName, coTaskMemAnsi, length);
            Marshal.FreeCoTaskMem(coTaskMemAnsi);
            return true;
        }

        [StructLayout(LayoutKind.Sequential)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;
        }
    }
}
