﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Entity;
using ODM.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net.Mail;
namespace ODM.Data.Util
{
    public class GlobalVariablesAndMethods
    {
        public static PickingBatchFilters UserAppliedFilters = new PickingBatchFilters();

        public static Item CmbPickListBatchSelected = new Item(string.Empty, string.Empty);

        public static User UserDetails { get; set; }

        public static Guid GuidForLog { get; set; }

        public static Guid OrderIdToLog { get; set; }

        public string OpenedOrderIds { get; set; }

        public static string strApiToken { get; set; }

        public static string strRequiredLabelFormat { get; set; }

        public static string strDefaultWeightInKg { get; set; }

        public static bool blVerboseLogging { get; set; }

        public static string SelectedPrinterNameForMyHermes { get; set; }

        public static string SelectedPrinterNameForHermes { get; set; }

        public static string SelectedPrinterNameForSpring { get; set; }

        public static string SelectedPrinterNameForSpring_New { get; set; }

        public static string SelectedPrinterNameForSpringManifest { get; set; }

        public static string SelectedPrinterNameForDPD { get; set; }

        public static string SelectedPrinterNameForRoyalMail { get; set; }

        public static string SelectedPrinterNameForRoyalMailManifest { get; set; }

        public static string SelectedPrinterNameForFedEx { get; set; }

        public static string SelectedPrinterNameForAmazon { get; set; }

        public static string SelectedPrinterNameForInvoice { get; set; }

        public static string SelectedPrinterNameForPickList { get; set; }

        public static string SelectedPrinterNameForCN22Form { get; set; }

        public static string AccountNumber { get; set; }

        // for new spring courier API (without manifest)
        public static string strClientID { get; set; }
        public static string strSecret { get; set; }
        public static string strShopID { get; set; }
        public static string strContractID { get; set; }
        public static string strCountryCode { get; set; }
        public static OrderItem objOrderItemDetailsForSpring { get; set; }
        public static Order objOrderDetailsForSpring { get; set; }

        public static string APIUser { get; set; }
        public static string APIPass { get; set; }
        public static bool UseLive { get; set; }

        //Properties for Royal Mail
        public static string ClientID { get; set; }
        public static string Secret { get; set; }
        public static string ApplicationID { get; set; }
        //End of Properties for Royal Mail

        //DispatchType will be either "All Orders" or "Urgent Orders"
        public static string DispatchType { get; set; }

        /// <summary>
        /// Get the IPv4 Address of the current system.
        /// </summary>
        public string GetSystemIpv4Address
        {
            get
            {
                string myHost = System.Net.Dns.GetHostName();
                string strIpv4 = null;

                for (int i = 0; i <= System.Net.Dns.GetHostEntry(myHost).AddressList.Length - 1; i++)
                {
                    if (System.Net.Dns.GetHostEntry(myHost).AddressList[i].IsIPv6LinkLocal == false)
                    {
                        strIpv4 = System.Net.Dns.GetHostEntry(myHost).AddressList[i].ToString();
                        break;
                    }
                }
                return strIpv4;
            }
        }

        /// <summary>
        /// Set the is opned status of orders so that other user can't open the already opned orders
        /// </summary>
        /// <param name="OrderIDs">Order ids that you want to set opned</param>
        /// <param name="strIsResetCall">Is user want to set opned or want to reset the opened status,
        /// so that after closing that other user could be able to open those
        /// set this "Y" when you are trying to close this window otherwise set "N"
        /// </param>
        public void setIsOpenStatus(string OrderIDs, string strIsResetCall)
        {
            SqlConnection connection = new SqlConnection();
            try
            {
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlParameter[] objorderParams = new SqlParameter[2];

                objorderParams[0] = new SqlParameter("@OrderIds", SqlDbType.VarChar, 8000);
                objorderParams[0].Value = OrderIDs;

                objorderParams[1] = new SqlParameter("@IsResetCall", SqlDbType.VarChar, 2);
                objorderParams[1].Value = strIsResetCall;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateIsOpnedStatus", objorderParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get the list of all printers attached with the current system.
        /// </summary>
        /// <param name="DefaultPrinterName">ref parameter that will return the default printer name(if any 1 is attached with current system)</param>
        /// <returns></returns>
        public List<string> AttachedPrinters(ref string DefaultPrinterName)
        {
            List<string> lstPrinters = new List<string>();
            try
            {
                PrinterSettings Settings = new PrinterSettings();
                foreach (string AttachedPrinterName in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                {
                    Settings.PrinterName = AttachedPrinterName;
                    if (Settings.IsDefaultPrinter)
                    {
                        DefaultPrinterName = AttachedPrinterName;
                    }
                    lstPrinters.Add(AttachedPrinterName);
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
            return lstPrinters;
        }

        /// <summary>
        /// Update the printer name for the print label process
        /// </summary>
        public void AddUpdatePrinterName()
        {
            SqlConnection connection = new SqlConnection();
            try
            {
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlParameter[] objPrinterParams = new SqlParameter[14];

                objPrinterParams[0] = new SqlParameter("@IPAddress", SqlDbType.VarChar, 100);
                objPrinterParams[0].Value = GetSystemIpv4Address;

                objPrinterParams[1] = new SqlParameter("@HermesPrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[1].Value = SelectedPrinterNameForHermes;

                objPrinterParams[2] = new SqlParameter("@MyHermesPrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[2].Value = SelectedPrinterNameForMyHermes;

                objPrinterParams[3] = new SqlParameter("@SpringPrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[3].Value = SelectedPrinterNameForSpring;

                objPrinterParams[4] = new SqlParameter("@DPDPrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[4].Value = SelectedPrinterNameForDPD;

                objPrinterParams[5] = new SqlParameter("@RoyalMailPrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[5].Value = SelectedPrinterNameForRoyalMail;

                objPrinterParams[6] = new SqlParameter("@RoyalMailManifestPrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[6].Value = SelectedPrinterNameForRoyalMailManifest;

                objPrinterParams[7] = new SqlParameter("@SpringManifestPrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[7].Value = SelectedPrinterNameForSpringManifest;

                objPrinterParams[8] = new SqlParameter("@FedExPrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[8].Value = SelectedPrinterNameForFedEx;

                objPrinterParams[9] = new SqlParameter("@InvoicePrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[9].Value = SelectedPrinterNameForInvoice;

                objPrinterParams[10] = new SqlParameter("@AmazonPrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[10].Value = SelectedPrinterNameForAmazon;

                objPrinterParams[11] = new SqlParameter("@SpringPrinterName_New", SqlDbType.VarChar, 100);
                objPrinterParams[11].Value = SelectedPrinterNameForSpring_New;

                objPrinterParams[12] = new SqlParameter("@PickListPrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[12].Value = SelectedPrinterNameForPickList;

                objPrinterParams[13] = new SqlParameter("@CN22PrinterName", SqlDbType.VarChar, 100);
                objPrinterParams[13].Value = SelectedPrinterNameForCN22Form;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdatePrinters", objPrinterParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// If any of the courier don't have printer set yet, then we are checking in the database.
        /// If we have printer name in the database then set printer name from database 
        ///otherwise get and set the system's current/default printer to print shipping label.
        /// </summary>
        public void GetPrinterNameByIpAddress()
        {
            SqlConnection connection = new SqlConnection();
            try
            {
                string strDefaultPrinterName = "";
                List<string> lstPrinters = AttachedPrinters(ref strDefaultPrinterName);
                DataTable objDt = new DataTable();
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlParameter[] objPrinterParams = new SqlParameter[1];

                objPrinterParams[0] = new SqlParameter("@IPAddress", SqlDbType.VarChar, 100);
                objPrinterParams[0].Value = GetSystemIpv4Address;

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetPrinterNameByIPAddress", objPrinterParams).Tables[0];
                if (objDt.Rows.Count > 0)
                {
                    string strPrinterName = string.Empty;
                    strPrinterName = Convert.ToString(objDt.Rows[0]["HermesPrinterName"]);
                    SelectedPrinterNameForHermes = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["MyHermesPrinterName"]);
                    SelectedPrinterNameForMyHermes = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["SpringPrinterName"]);
                    SelectedPrinterNameForSpring = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["DPDPrinterName"]);
                    SelectedPrinterNameForDPD = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["RoyalMailPrinterName"]);
                    SelectedPrinterNameForRoyalMail = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["RoyalMailManifestPrinterName"]);
                    SelectedPrinterNameForRoyalMailManifest = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["SpringManifestPrinterName"]);
                    SelectedPrinterNameForSpringManifest = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["FedExPrinterName"]);
                    SelectedPrinterNameForFedEx = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["InvoicePrinterName"]);
                    SelectedPrinterNameForInvoice = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["AmazonPrinterName"]);
                    SelectedPrinterNameForAmazon = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["SpringPrinterName_New"]);
                    SelectedPrinterNameForSpring_New = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["PickListPrinterName"]);
                    SelectedPrinterNameForPickList = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                    strPrinterName = Convert.ToString(objDt.Rows[0]["CN22PrinterName"]);
                    SelectedPrinterNameForCN22Form = !string.IsNullOrWhiteSpace(strPrinterName) ? strPrinterName : strDefaultPrinterName;

                }
                else
                {
                    SelectedPrinterNameForHermes = strDefaultPrinterName;
                    SelectedPrinterNameForMyHermes = strDefaultPrinterName;
                    SelectedPrinterNameForSpring = strDefaultPrinterName;
                    SelectedPrinterNameForDPD = strDefaultPrinterName;
                    SelectedPrinterNameForRoyalMail = strDefaultPrinterName;
                    SelectedPrinterNameForRoyalMailManifest = strDefaultPrinterName;
                    SelectedPrinterNameForSpringManifest = strDefaultPrinterName;
                    SelectedPrinterNameForFedEx = strDefaultPrinterName;
                    SelectedPrinterNameForInvoice = strDefaultPrinterName;
                    SelectedPrinterNameForAmazon = strDefaultPrinterName;
                    SelectedPrinterNameForSpring_New = strDefaultPrinterName;
                    SelectedPrinterNameForPickList = strDefaultPrinterName;
                    SelectedPrinterNameForCN22Form = strDefaultPrinterName;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get the Details of the courier by its name or ID (for now we are getting details by name from procedure)
        /// </summary>
        /// <param name="strCourierName">Courier Name</param>
        /// <param name="strCourierID">Courier ID</param>
        /// <returns></returns>
        public DataTable GetCourierDetailsByNameOrId(string strCourierName, string strCourierID)
        {
            SqlConnection connection = new SqlConnection();
            DataTable objDt = new DataTable();
            try
            {
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                Guid CourierID = new Guid(strCourierID);
                SqlParameter[] objParamsCourier = new SqlParameter[2];

                objParamsCourier[0] = new SqlParameter("@CourierName", SqlDbType.VarChar, 1000);
                objParamsCourier[0].Value = strCourierName;

                objParamsCourier[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                objParamsCourier[1].Value = CourierID;

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetCourierDetailsById_New", objParamsCourier).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objDt;
        }

        /// <summary>
        /// Get the Manifest Orders by Postal Service Name(Courier Name)
        /// </summary>
        /// <param name="strPostalServiceName">Postal Service/Courier Name</param>
        /// <returns>Datatable of Manifest Orders</returns>
        public DataTable GetManifestOrders(string strPostalServiceName, string strSearchText)
        {
            SqlConnection connection = new SqlConnection();
            DataTable objDt = new DataTable();
            try
            {
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlParameter[] objParams = new SqlParameter[2];

                objParams[0] = new SqlParameter("@PostalServiceName", SqlDbType.VarChar, 200);
                objParams[0].Value = strPostalServiceName;

                objParams[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, 100);
                objParams[1].Value = strSearchText;

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetManifestOrders", objParams).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }

        /// <summary>
        /// Get the Manifest Order's summary by Postal Service Name(Courier Name)
        /// </summary>
        /// <param name="strPostalServiceName">Postal Service/Courier Name</param>
        /// <returns>Datatable of Manifest Orders</returns>
        public DataTable GetManifestOrderSummary(string strPostalServiceName, string strSearchText)
        {
            SqlConnection connection = new SqlConnection();
            DataTable objDt = new DataTable();
            DataSet objDs = new DataSet();
            try
            {
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlParameter[] objParams = new SqlParameter[2];

                objParams[0] = new SqlParameter("@PostalServiceName", SqlDbType.VarChar, 200);
                objParams[0].Value = strPostalServiceName;

                objParams[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, 100);
                objParams[1].Value = strSearchText;


                objDs = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetManifestSummary", objParams);
                if (objDs.Tables.Count > 0)
                {
                    objDt = objDs.Tables[0];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }

        /// <summary>
        /// That will convert the string value to Base64 value
        /// </summary>
        /// <param name="strStringValue">String value that we want to convert</param>
        /// <returns>String</returns>
        public string ConvertStringToBase64(string strStringValue)
        {
            string strBase64Value = "";
            try
            {
                var Bytes = System.Text.Encoding.UTF8.GetBytes(strStringValue);
                strBase64Value = Convert.ToBase64String(Bytes);
                return strBase64Value;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// That will return a repeated string
        /// </summary>
        /// <param name="StringToRepeat">String that you want to repeat</param>
        /// <param name="NumberOfRepeat">Number of times that you want to repeat a string</param>
        /// <returns></returns>
        public static string RepeatString(string StringToRepeat, int NumberOfRepeat)
        {
            string strResult = "";
            try
            {
                for (int i = 0; i < NumberOfRepeat; i++)
                {
                    strResult = strResult + StringToRepeat;
                }
                return strResult;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get the all countries name.
        /// </summary>
        /// <returns>DataTable of countries</returns>
        public DataTable GetAllCountries()
        {
            SqlConnection connection = new SqlConnection();
            try
            {
                DataTable objDtCountry = new DataTable();
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                objDtCountry = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetAllCountry").Tables[0];
                return objDtCountry;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// To get the specific substring.
        /// </summary>
        /// <param name="strCompleteString">The complete string from where we want to get the substrings</param>
        /// <param name="strStartString">Starting string, from where to read string</param>
        /// <param name="strEndString">End string, to where to read the string</param>
        /// <returns></returns>
        public static string GetSubString(string strCompleteString, string strStartString, string strEndString)
        {
            try
            {
                string strData = string.Empty;
                int intStartOfString = strCompleteString.IndexOf(strStartString) + strStartString.Length;
                int intEndOfString = strCompleteString.IndexOf(strEndString);
                strData = strCompleteString.Substring(intStartOfString, intEndOfString - intStartOfString);
                return strData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get all the type of labels stock
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAllLabelStockType()
        {
            try
            {
                List<string> lstLabelStockType = new List<string>();
                lstLabelStockType.Add("PAPER_4X6");
                lstLabelStockType.Add("PAPER_4X8");
                lstLabelStockType.Add("PAPER_4X9");
                lstLabelStockType.Add("PAPER_6X4");
                lstLabelStockType.Add("PAPER_7X4.75");
                lstLabelStockType.Add("PAPER_8.5X11_BOTTOM_HALF_LABEL");
                lstLabelStockType.Add("PAPER_8.5X11_TOP_HALF_LABEL");
                lstLabelStockType.Add("PAPER_LETTER");
                lstLabelStockType.Add("STOCK_4X6");
                lstLabelStockType.Add("STOCK_4X6.75_LEADING_DOC_TAB");
                lstLabelStockType.Add("STOCK_4X6.75_TRAILING_DOC_TAB");
                lstLabelStockType.Add("STOCK_4X8");
                lstLabelStockType.Add("STOCK_4X9_LEADING_DOC_TAB");
                lstLabelStockType.Add("STOCK_4X9_TRAILING_DOC_TAB");
                return lstLabelStockType;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        /// <summary>
        /// Get state code by state and country
        /// </summary>
        /// <param name="state">State name</param>
        /// <param name="country">Country name</param>
        /// <returns></returns>
        public static string getStateCode(String state, String country)
        {

            System.Collections.Generic.Dictionary<String, List<String>> stateCodeReference = new System.Collections.Generic.Dictionary<string, List<string>>();

            stateCodeReference.Add("UNITED STATESAK", new List<String>(new String[] { "UNITED STATES", "ALASKA", "AK" }));
            stateCodeReference.Add("UNITED STATESAL", new List<String>(new String[] { "UNITED STATES", "ALABAMA", "AL" }));
            stateCodeReference.Add("UNITED STATESAR", new List<String>(new String[] { "UNITED STATES", "ARKANSAS", "AR" }));
            stateCodeReference.Add("UNITED STATESAS", new List<String>(new String[] { "UNITED STATES", "AMERICAN SAMOA", "AS" }));
            stateCodeReference.Add("UNITED STATESAZ", new List<String>(new String[] { "UNITED STATES", "ARIZONA", "AZ" }));
            stateCodeReference.Add("UNITED STATESCA", new List<String>(new String[] { "UNITED STATES", "CALIFORNIA", "CA" }));
            stateCodeReference.Add("UNITED STATESCO", new List<String>(new String[] { "UNITED STATES", "COLORADO", "CO" }));
            stateCodeReference.Add("UNITED STATESCT", new List<String>(new String[] { "UNITED STATES", "CONNECTICUT", "CT" }));
            stateCodeReference.Add("UNITED STATESDC", new List<String>(new String[] { "UNITED STATES", "DISTRICT OF COLUMBIA", "DC" }));
            //stateCodeReference.Add("UNITED STATESDC", new List<String>(new String[] {"UNITED STATES", "WASHINGTON, DC", "DC" }));
            stateCodeReference.Add("UNITED STATESDE", new List<String>(new String[] { "UNITED STATES", "DELAWARE", "DE" }));
            stateCodeReference.Add("UNITED STATESFM", new List<String>(new String[] { "UNITED STATES", "FEDERATED STATES OF MICRONESIA", "FM" }));
            stateCodeReference.Add("UNITED STATESFL", new List<String>(new String[] { "UNITED STATES", "FLORIDA", "FL" }));
            stateCodeReference.Add("UNITED STATESGA", new List<String>(new String[] { "UNITED STATES", "GEORGIA", "GA" }));
            stateCodeReference.Add("UNITED STATESGU", new List<String>(new String[] { "UNITED STATES", "GUAM", "GU" }));
            stateCodeReference.Add("UNITED STATESHI", new List<String>(new String[] { "UNITED STATES", "HAWAII", "HI" }));
            stateCodeReference.Add("UNITED STATESIA", new List<String>(new String[] { "UNITED STATES", "IOWA", "IA" }));
            stateCodeReference.Add("UNITED STATESID", new List<String>(new String[] { "UNITED STATES", "IDAHO", "ID" }));
            stateCodeReference.Add("UNITED STATESIL", new List<String>(new String[] { "UNITED STATES", "ILLINOIS", "IL" }));
            stateCodeReference.Add("UNITED STATESIN", new List<String>(new String[] { "UNITED STATES", "INDIANA", "IN" }));
            stateCodeReference.Add("UNITED STATESKS", new List<String>(new String[] { "UNITED STATES", "KANSAS", "KS" }));
            stateCodeReference.Add("UNITED STATESKY", new List<String>(new String[] { "UNITED STATES", "KENTUCKY", "KY" }));
            stateCodeReference.Add("UNITED STATESLA", new List<String>(new String[] { "UNITED STATES", "LOUISIANA", "LA" }));
            stateCodeReference.Add("UNITED STATESMA", new List<String>(new String[] { "UNITED STATES", "MASSACHUSETTS", "MA" }));
            stateCodeReference.Add("UNITED STATESMD", new List<String>(new String[] { "UNITED STATES", "MARYLAND", "MD" }));
            stateCodeReference.Add("UNITED STATESME", new List<String>(new String[] { "UNITED STATES", "MAINE", "ME" }));
            stateCodeReference.Add("UNITED STATESMH", new List<String>(new String[] { "UNITED STATES", "MARSHALL ISLANDS", "MH" }));
            stateCodeReference.Add("UNITED STATESMI", new List<String>(new String[] { "UNITED STATES", "MICHIGAN", "MI" }));
            stateCodeReference.Add("UNITED STATESMN", new List<String>(new String[] { "UNITED STATES", "MINNESOTA", "MN" }));
            stateCodeReference.Add("UNITED STATESMO", new List<String>(new String[] { "UNITED STATES", "MISSOURI", "MO" }));
            stateCodeReference.Add("UNITED STATESMP", new List<String>(new String[] { "UNITED STATES", "NORTHERN MARIANA ISLANDS", "MP" }));
            stateCodeReference.Add("UNITED STATESMS", new List<String>(new String[] { "UNITED STATES", "MISSISSIPPI", "MS" }));
            stateCodeReference.Add("UNITED STATESMT", new List<String>(new String[] { "UNITED STATES", "MONTANA", "MT" }));
            stateCodeReference.Add("UNITED STATESNC", new List<String>(new String[] { "UNITED STATES", "NORTH CAROLINA", "NC" }));
            stateCodeReference.Add("UNITED STATESND", new List<String>(new String[] { "UNITED STATES", "NORTH DAKOTA", "ND" }));
            stateCodeReference.Add("UNITED STATESNE", new List<String>(new String[] { "UNITED STATES", "NEBRASKA", "NE" }));
            stateCodeReference.Add("UNITED STATESNH", new List<String>(new String[] { "UNITED STATES", "NEW HAMPSHIRE", "NH" }));
            stateCodeReference.Add("UNITED STATESNJ", new List<String>(new String[] { "UNITED STATES", "NEW JERSEY", "NJ" }));
            stateCodeReference.Add("UNITED STATESNM", new List<String>(new String[] { "UNITED STATES", "NEW MEXICO", "NM" }));
            stateCodeReference.Add("UNITED STATESNV", new List<String>(new String[] { "UNITED STATES", "NEVADA", "NV" }));
            stateCodeReference.Add("UNITED STATESNY", new List<String>(new String[] { "UNITED STATES", "NEW YORK", "NY" }));
            stateCodeReference.Add("UNITED STATESOH", new List<String>(new String[] { "UNITED STATES", "OHIO", "OH" }));
            stateCodeReference.Add("UNITED STATESOK", new List<String>(new String[] { "UNITED STATES", "OKLAHOMA", "OK" }));
            stateCodeReference.Add("UNITED STATESOR", new List<String>(new String[] { "UNITED STATES", "OREGON", "OR" }));
            stateCodeReference.Add("UNITED STATESPA", new List<String>(new String[] { "UNITED STATES", "PENNSYLVANIA", "PA" }));
            stateCodeReference.Add("UNITED STATESPR", new List<String>(new String[] { "UNITED STATES", "PUERTO RICO", "PR" }));
            stateCodeReference.Add("UNITED STATESPW", new List<String>(new String[] { "UNITED STATES", "PALAU", "PW" }));
            stateCodeReference.Add("UNITED STATESRI", new List<String>(new String[] { "UNITED STATES", "RHODE ISLAND", "RI" }));
            stateCodeReference.Add("UNITED STATESSC", new List<String>(new String[] { "UNITED STATES", "SOUTH CAROLINA", "SC" }));
            stateCodeReference.Add("UNITED STATESSD", new List<String>(new String[] { "UNITED STATES", "SOUTH DAKOTA", "SD" }));
            stateCodeReference.Add("UNITED STATESTN", new List<String>(new String[] { "UNITED STATES", "TENNESSEE", "TN" }));
            stateCodeReference.Add("UNITED STATESTX", new List<String>(new String[] { "UNITED STATES", "TEXAS", "TX" }));
            stateCodeReference.Add("UNITED STATESUT", new List<String>(new String[] { "UNITED STATES", "UTAH", "UT" }));
            stateCodeReference.Add("UNITED STATESVA", new List<String>(new String[] { "UNITED STATES", "VIRGINIA", "VA" }));
            stateCodeReference.Add("UNITED STATESVI", new List<String>(new String[] { "UNITED STATES", "VIRGIN ISLANDS", "VI" }));
            stateCodeReference.Add("UNITED STATESVT", new List<String>(new String[] { "UNITED STATES", "VERMONT", "VT" }));
            stateCodeReference.Add("UNITED STATESWA", new List<String>(new String[] { "UNITED STATES", "WASHINGTON", "WA" }));
            stateCodeReference.Add("UNITED STATESWI", new List<String>(new String[] { "UNITED STATES", "WISCONSIN", "WI" }));
            stateCodeReference.Add("UNITED STATESWV", new List<String>(new String[] { "UNITED STATES", "WEST VIRGINIA", "WV" }));
            stateCodeReference.Add("UNITED STATESWY", new List<String>(new String[] { "UNITED STATES", "WYOMING", "WY" }));
            stateCodeReference.Add("CANADAAB", new List<String>(new String[] { "CANADA", "ALBERTA", "AB" }));
            stateCodeReference.Add("CANADANT", new List<String>(new String[] { "CANADA", "NORTHWEST TERRITORIES", "NT" }));
            stateCodeReference.Add("CANADABC", new List<String>(new String[] { "CANADA", "BRITISH COLOMBIA", "BC" }));
            stateCodeReference.Add("CANADAON", new List<String>(new String[] { "CANADA", "ONTARIO", "ON" }));
            stateCodeReference.Add("CANADAMB", new List<String>(new String[] { "CANADA", "MANITOBA", "MB" }));
            stateCodeReference.Add("CANADAPE ", new List<String>(new String[] { "CANADA", "PRINCE EDWARD ISLAND", "PE " }));
            stateCodeReference.Add("CANADANB", new List<String>(new String[] { "CANADA", "NEW BRUNSWICK", "NB" }));
            stateCodeReference.Add("CANADAQC", new List<String>(new String[] { "CANADA", "QUEBEC", "QC" }));
            stateCodeReference.Add("CANADANF", new List<String>(new String[] { "CANADA", "NEWFOUNDLAND", "NF" }));
            stateCodeReference.Add("CANADASK", new List<String>(new String[] { "CANADA", "SASKATCHEWAN", "SK" }));
            stateCodeReference.Add("CANADANS", new List<String>(new String[] { "CANADA", "NOVA SCOTIA", "NS" }));
            stateCodeReference.Add("CANADAYT", new List<String>(new String[] { "CANADA", "YUKON", "YT" }));
            stateCodeReference.Add("CANADANU", new List<String>(new String[] { "CANADA", "NUNAVUT", "NU" }));
            stateCodeReference.Add("AUSTRALIANSW", new List<String>(new String[] { "AUSTRALIA", "NEW SOUTH WALES", "NSW" }));
            stateCodeReference.Add("AUSTRALIAACT", new List<String>(new String[] { "AUSTRALIA", "AUSTRALIAN CAPITAL TERRITORY", "ACT" }));
            stateCodeReference.Add("AUSTRALIAVIC", new List<String>(new String[] { "AUSTRALIA", "VICTORIA", "VIC" }));
            stateCodeReference.Add("AUSTRALIAQLD", new List<String>(new String[] { "AUSTRALIA", "QUEENSLAND", "QLD" }));
            stateCodeReference.Add("AUSTRALIASA", new List<String>(new String[] { "AUSTRALIA", "SOUTH AUSTRALIA", "SA" }));
            stateCodeReference.Add("AUSTRALIAWA", new List<String>(new String[] { "AUSTRALIA", "WESTERN AUSTRALIA", "WA" }));
            stateCodeReference.Add("AUSTRALIATAS", new List<String>(new String[] { "AUSTRALIA", "TASMANIA", "TAS" }));
            stateCodeReference.Add("AUSTRALIANT", new List<String>(new String[] { "AUSTRALIA", "NORTHERN TERRITORY", "NT" }));

            if ((country.Trim().ToUpper() == "AUSTRALIA") || (country.Trim().ToUpper() == "UNITED STATES") || (country.Trim().ToUpper() == "CANADA"))
            {

                String key = country.Trim().ToUpper() + state.Trim().ToUpper();
                if (stateCodeReference.ContainsKey(key))
                    return stateCodeReference[key][2];
                else
                {
                    foreach (System.Collections.Generic.List<String> items in stateCodeReference.Values)
                    {
                        if (country.Trim().ToUpper() == items[0] && state.Trim().ToUpper() == items[1])
                        {
                            return items[2];
                        }
                    }
                    return state;
                }
            }
            else
            {
                return state.Substring(0, Math.Min(state.Length, 15));
            }
        }

        /// <summary>
        /// Log process history of order
        /// </summary>
        /// <param name="strLogText">Text to log in database</param>
        public static void LogHistory(string strLogText)
        {
            /*
             * Note:- IF log text containing charecters '&&' then that means we want to break line here on log history grid
             * we are replacing '&&' with \n to break logs in newlines
             */
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            Guid ID = new Guid();
            ID = GuidForLog;
            int intErrorCount = 0;
        ExecuteAgainToLogThisMethodToo:
            try
            {
                SqlParameter[] objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = ID;//Unique id for each log, we are setting that on GetOrderData window

                objParam[1] = new SqlParameter("@OrderID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = GlobalVariablesAndMethods.OrderIdToLog;//Order id for which we are storing log

                objParam[2] = new SqlParameter("@LogText", SqlDbType.VarChar, int.MaxValue);
                objParam[2].Value = strLogText;//Log text to store for order

                objParam[3] = new SqlParameter("@LoggedByID", SqlDbType.UniqueIdentifier);
                objParam[3].Value = GlobalVariablesAndMethods.UserDetails.Userid;//The user id who is dispatching order

                objParam[4] = new SqlParameter("@LoggedByName", SqlDbType.VarChar);
                objParam[4].Value = GlobalVariablesAndMethods.UserDetails.Firstname
                    + " " + GlobalVariablesAndMethods.UserDetails.Lastname; //The user id who is dispatching order

                objParam[5] = new SqlParameter("@Result", SqlDbType.VarChar, int.MaxValue);
                objParam[5].Direction = ParameterDirection.Output;//Output parameter to trace the operation of sp and that will return error if there is any in sp
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "LogHistory", objParam);

                string strResult = Convert.ToString(objParam[5].Value);

                /*
                 * IF result is nither updated nor inserted that means we got error while execution of "LogHistory" sp
                 * then we are storing that error in log as well
                 */
                if (strResult.ToUpper() != "UPDATED" && strResult.ToUpper() != "INSERTED")
                {
                    if (intErrorCount == 0)
                    {
                       // ID.ToString() = "00000000-0000-0000-0000-000000000000";
                        ID = Guid.Empty;
                        ID = Guid.NewGuid();
                        strLogText = "Error occurred in LogHistory method:- " + strResult;
                        intErrorCount++;
                        goto ExecuteAgainToLogThisMethodToo;
                    }
                    else
                        throw new CustomException(strResult);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Add/Update/Delete data under pending email table for send email purpose
        /// </summary>
        /// <param name="objSendEmail">Object of class SendEmailEntity for mail purpose</param>
        /// <returns>
        /// INSERTED:- When data saved after processing for send email purpose.
        /// UPDATED:- When mail have been sent to user.
        /// DELETED:- When mail have been deleted from pending email list
        /// <returns>Execution result</returns>
        public static string InsertUpdateDeleteDataForPendingEmail(SendEmailEntity objSendEmail)
        {
            string strLogText = "Making database call for details of pending email screen";
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                Guid OrderID = new Guid();
                if (objSendEmail.Mode == "INSERT")
                {
                    OrderID = new Guid(objSendEmail.OrderID);
                }
                SqlParameter[] objParam = new SqlParameter[10];

                objParam[0] = new SqlParameter("@IDs", SqlDbType.VarChar, int.MaxValue);
                objParam[0].Value = objSendEmail.IDs;

                objParam[1] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = OrderID;

                objParam[2] = new SqlParameter("@NumOrderId", SqlDbType.Int);
                objParam[2].Value = Convert.ToInt32(objSendEmail.NumberOrderID);

                objParam[3] = new SqlParameter("@Date", SqlDbType.DateTime);
                objParam[3].Value = objSendEmail.Date;

                objParam[4] = new SqlParameter("@FullName", SqlDbType.VarChar, 50);
                objParam[4].Value = objSendEmail.FullName;

                objParam[5] = new SqlParameter("@EmailAddress", SqlDbType.VarChar, 50);
                objParam[5].Value = objSendEmail.EmailID;

                objParam[6] = new SqlParameter("@SentMailBody", SqlDbType.VarChar, Int32.MaxValue);
                objParam[6].Value = objSendEmail.SentText;

                objParam[7] = new SqlParameter("@SentSubject", SqlDbType.VarChar, Int32.MaxValue);
                objParam[7].Value = objSendEmail.SentSubject;


                /* Values for Mode parameter
                 * INSERT:- When we want to store data in db for send email purpose
                 * SENT:- When we are sending email to user
                 * DELETE:- When we want to delete emails from pending email list
                 */
                objParam[8] = new SqlParameter("@Mode", SqlDbType.VarChar, 20);
                objParam[8].Value = objSendEmail.Mode;

                objParam[9] = new SqlParameter("@Result", SqlDbType.VarChar, int.MaxValue);
                objParam[9].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeletePendingEmail", objParam);
                string strResult = Convert.ToString(objParam[9].Value);

                //Loggin pending email status in db
                switch (strResult)
                {
                    //Means we have stored the details for mail purpose
                    case "INSERTED":
                        {
                            strLogText = strLogText + "&&Email Details save in database for NumOrderId:- " + objSendEmail.NumberOrderID;
                            GlobalVariablesAndMethods.LogHistory(strLogText);
                            break;
                        }

                    case "UPDATED":
                    case "DELETED":
                        {
                            /*these cases are for sent mail for delete mails
                             Logging details for each order ids for which we sent or deleted mail(s)
                             */
                            string[] strDeletedOrderIdsMail = objSendEmail.OrderIDs.Split(',');
                            for (int i = 0; i < strDeletedOrderIdsMail.Length; i++)
                            {
                                Guid DeletedOrderID = new Guid(strDeletedOrderIdsMail[i]);
                                GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
                                GlobalVariablesAndMethods.OrderIdToLog = DeletedOrderID;
                                string strLogWithResult = strResult == "UPDATED" ? string.Format("Email sent for order id {0}", strDeletedOrderIdsMail[i]) :
                                    string.Format("Order id {0} deleted from pending email list", strDeletedOrderIdsMail[i]);
                                GlobalVariablesAndMethods.LogHistory(strLogWithResult);
                            }
                            break;
                        }
                }
                //End of logging email status in db
                return strResult;
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory(strLogText + "&&Exception while save email details:-" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get pending or Sent email list
        /// </summary>
        /// <param name="strSentOrPending">
        /// Possible values:-
        /// 1) Sent :- Then that will return sent emails
        /// 2) "" or Pending:- Then that will return pending emails
        /// </param>
        /// <param name="FromDate">
        /// Filter by from date
        /// </param>
        /// <param name="FromDate">
        /// Filter by from To date
        /// </param>
        /// <param name="strSearchValue">
        /// Filter by Order Id or Number Order Id
        /// </param>
        /// <returns>DataTable with Sent/Pending email </returns>
        public static DataTable GetPendingOrSentEmail(string strSentOrPending, string FromDate = "", string ToDate = "", string strSearchValue = "")
         {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            DataTable objDt = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@SentOrPending", SqlDbType.VarChar, 20);
                objParam[0].Value = strSentOrPending;

                objParam[1] = new SqlParameter("@FromDate", SqlDbType.VarChar, 10);
                objParam[1].Value = FromDate;

                objParam[2] = new SqlParameter("@ToDate", SqlDbType.VarChar, 10);
                objParam[2].Value = ToDate;

                objParam[3] = new SqlParameter("@SearchValue", SqlDbType.VarChar, 40);
                objParam[3].Value = strSearchValue;

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDataForPendingEmail", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory(string.Format("Exception occurred while getting data for {0}&&Exception:- {1}", strSentOrPending, ex.Message));
            }
            finally { connection.Close(); }
            return objDt;
        }

        /// <summary>
        /// Get order/Order items details by order id
        /// </summary>
        /// <param name="OrderId">Order Id</param>
        /// <returns>Datatable with order/order item details</returns>
        public static DataTable GetOrderDetailsByOrderId(Guid OrderId)
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.Add("@OrderId", SqlDbType.UniqueIdentifier).Value = OrderId;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ProcessOrder";
                cmd.CommandTimeout = 200;
                SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
                objAdapt.Fill(objDt);
            }
            catch (Exception ex)
            {
                LogHistory("Error occurred while getting order details&&" + ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }

        /// <summary>
        /// Add/Update/Delete email conditions
        /// </summary>
        /// <param name="strIds">Either comman seprated Ids(to delete) or single id(to update)</param>
        /// <param name="strText">Condition text</param>
        /// <param name="strMode">can be either Insert,Update or delete</param>
        /// <returns>Result we got after the execution of sp</returns>
        public static string AddUpdateDeleteCondition(string strIds, string strText, string strMode)
        {
            string strResult = "";
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@IDs", SqlDbType.VarChar, int.MaxValue);
                objParam[0].Value = strIds;

                objParam[1] = new SqlParameter("@Text", SqlDbType.VarChar, int.MaxValue);
                objParam[1].Value = strText;

                objParam[2] = new SqlParameter("@Mode", SqlDbType.VarChar, 30);
                objParam[2].Value = strMode;

                objParam[3] = new SqlParameter("@Result", SqlDbType.VarChar, int.MaxValue);
                objParam[3].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteEmailCondition", objParam);
                strResult = Convert.ToString(objParam[3].Value);
            }
            catch (Exception ex)
            {
                LogHistory("Error occurred for email condion in " + strText + " mode&&" + ex.Message);
            }
            return strResult;
        }

        /// <summary>
        /// Get details ot all eamil accounts.
        /// </summary>
        /// <returns>DataTable of details of email account</returns>
        public static DataTable GetEmailAccounts()
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            try
            {
                connection.Open();
                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetAdditionalEmailAccountDetails").Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }

        /// <summary>
        /// Get details ot all eamil accounts.
        /// </summary>
        /// <returns>DataTable of details of email Conditions</returns>
        public static DataTable GetEmailConditions()
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            try
            {
                connection.Open();
                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetConditionData").Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }

        /// <summary>
        /// Get details ot all eamil accounts.
        /// </summary>
        /// <returns>DataTable of details of email Conditions</returns>
        public static DataTable GetEmailTemplates()
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            try
            {
                connection.Open();
                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetAllTemplates").Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }

        /// <summary>
        /// To get the details of template by template id
        /// </summary>
        /// <param name="TemplateId">Template Id</param>
        /// <returns>DataTable of detail of template</returns>
        public static DataTable GetTemplateDetailsByTemplateId(Guid TemplateId, Guid ConditionID)
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@TemplateID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = TemplateId;

                objParam[1] = new SqlParameter("@ConditionID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = ConditionID;

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDetailsByTemplateId", objParam).Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }
        // conversion of base64 string to image
        public static Image Base64StringToImage(string strBase64String)
        {
            //edit string to check response
            //strBase64String = "";

            byte[] bytImageBytes = Convert.FromBase64String(strBase64String);
            MemoryStream ms = new MemoryStream(bytImageBytes, 0, bytImageBytes.Length);
            ms.Write(bytImageBytes, 0, bytImageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        // get images and delete images stored in DB with respect to order id
        public static dynamic GetAddUpdateDeleteImageByOrderId(string strMode, Guid OrderId, string strImagetoBase64 = "")
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            string strResult;
            try
            {
                SqlParameter[] objParam = new SqlParameter[5];

                objParam[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[0].Value = OrderId;

                objParam[1] = new SqlParameter("@Mode", SqlDbType.VarChar, 100);
                objParam[1].Value = strMode.ToUpper();

                objParam[2] = new SqlParameter("@Image", SqlDbType.VarChar, strImagetoBase64.Length);
                objParam[2].Value = strImagetoBase64;

                objParam[3] = new SqlParameter("@CreatedBy", SqlDbType.VarChar, 100);
                objParam[3].Value = GlobalVariablesAndMethods.UserDetails.Firstname + " " + GlobalVariablesAndMethods.UserDetails.Lastname;

                objParam[4] = new SqlParameter("@Result", SqlDbType.VarChar, 100);
                objParam[4].Direction = ParameterDirection.Output;
                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateDeleteImagesByOrderId", objParam);
                }
                else if (strMode.ToUpper() == "DELETE" || strMode.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateDeleteImagesByOrderId", objParam);
                }

                strResult = objParam[4].Value.ToString();

                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                else if (strResult.ToString() == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];
                    }
                    return objDt;
                }
                else
                {
                    return strResult.ToString();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        public static void RemoveInvalidColumnIfExistsInString(ref string strDataColumnsText, ref string strDataColumnsForDatabaseUse, ref string[] strArrColumnsInSubject, string sqlEx)
        {
            try
            {
                string[] strArrErrorList = sqlEx.Split('\n');
                List<string> lstDataBaseColums = new List<string>(strDataColumnsForDatabaseUse.Split(','));
                List<string> lstDataColumnInText = new List<string>(strDataColumnsText.Split(','));
                List<string> lstArrColumnsInSubject = new List<string>(strArrColumnsInSubject);
                for (int x = 0; x < strArrErrorList.Length; x++)
                {
                    string strExactError = GlobalVariablesAndMethods.GetSubString(strArrErrorList[x], "'", "'.");
                    string strColumnToRemoveFromDbList = lstDataBaseColums.Where(P => P.Contains(strExactError + "]")).FirstOrDefault();
                    if (strColumnToRemoveFromDbList != null)
                    {
                        int intIndexOfErrorColumn = lstDataBaseColums.IndexOf(strColumnToRemoveFromDbList);
                        /*
                         * We are using loop instead of lambda here because we can't change case of strDataColumnsText because
                         * this is the exact text which we got from template editor screen and we need to send that exact to client
                         */
                        for (int i = 0; i < lstDataColumnInText.Count; i++)
                        {
                            if (lstDataColumnInText[i].ToUpper().Contains(strExactError + "}]"))
                            {
                                lstDataColumnInText.RemoveAt(i);
                                lstArrColumnsInSubject.RemoveAt(i);
                                break;
                            }
                        }
                        lstDataBaseColums.RemoveAt(intIndexOfErrorColumn);
                        strDataColumnsForDatabaseUse = string.Join(",", lstDataBaseColums);
                        strDataColumnsText = string.Join(",", lstDataColumnInText);
                        strArrColumnsInSubject = lstArrColumnsInSubject.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                LogHistory("Error occurred in RemoveInvalidColumnIfExistsInString method&&" + ex.Message);
                throw ex;
            }
        }

        public static DataTable GetAddDeleteShipmentDetails(Guid ShipmentID, string strMode, Guid OrderID)
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objorderParams = new SqlParameter[4];

                objorderParams[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objorderParams[0].Value = OrderID;

                objorderParams[1] = new SqlParameter("@ShipmentID", SqlDbType.UniqueIdentifier);
                objorderParams[1].Value = ShipmentID;

                objorderParams[2] = new SqlParameter("@Mode", SqlDbType.VarChar, 20);
                objorderParams[2].Value = strMode;

                objorderParams[3] = new SqlParameter("@Result", SqlDbType.VarChar, 200);
                objorderParams[3].Direction = ParameterDirection.Output;
                if (strMode.ToUpper() == "GET")
                {
                    objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetAddDeleteShipmentDetails", objorderParams).Tables[0];
                }
                else
                {
                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GetAddDeleteShipmentDetails", objorderParams);
                }
                int intResult = Convert.ToInt32(objorderParams[3].Value);
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }
        public static DataTable GetMessageAccount(string strMode)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetMessageAccounts", objParam);
                if (objDs.Tables.Count > 0)
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];
                    }
                }
                return objDt;

            }

            catch (Exception)
            {

                throw;
            }
            finally { conn.Close(); }

        }
        public static dynamic GetAddUpdateMessages(MailMessage Mail, string strMode, int intMessageUID, string strMessageType, string CustomerId, string Subject, string strMessageBody, string strFolderID, Guid AccountID, int PageIndex = 1, int PageSize = 50, bool IsReadMessages = false, int intSkip = 0)
        {

            DataTable objDt = new DataTable();
            DataSet objDs = new DataSet();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            byte btIsAttachment;
            string strResult;
            if (Mail.Attachments.Count != 0)
            {
                btIsAttachment = 1;
            }
            else
            {
                btIsAttachment = 0;
            }
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[15];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objParam[1] = new SqlParameter("@MessageUID", SqlDbType.VarChar, 50);
                objParam[1].Value = intMessageUID;

                objParam[2] = new SqlParameter("@CustomerId", SqlDbType.VarChar, 100);
                objParam[2].Value = CustomerId;

                objParam[3] = new SqlParameter("@MessageType", SqlDbType.VarChar, 50);
                objParam[3].Value = strMessageType.ToUpper();

                objParam[4] = new SqlParameter("@Subject", SqlDbType.VarChar, 200);
                objParam[4].Value = Subject;

                objParam[5] = new SqlParameter("@MessageBody", SqlDbType.VarChar, strMessageBody.Length);
                objParam[5].Value = strMessageBody;

                objParam[6] = new SqlParameter("@IsAttachment", SqlDbType.Bit);
                objParam[6].Value = btIsAttachment;

                objParam[8] = new SqlParameter("@FolderID", SqlDbType.UniqueIdentifier);
                objParam[8].Value = new Guid(strFolderID);

                objParam[9] = new SqlParameter("@AccountID", SqlDbType.UniqueIdentifier);
                objParam[9].Value = AccountID;

                objParam[10] = new SqlParameter("@IsRead", SqlDbType.Bit);
                objParam[10].Value = Convert.ToByte(IsReadMessages);

                objParam[11] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[11].Direction = ParameterDirection.Output;

                objParam[12] = new SqlParameter("@PageSize", SqlDbType.VarChar, 10);
                objParam[12].Value = PageSize.ToString();

                objParam[13] = new SqlParameter("@Skip", SqlDbType.Int);
                objParam[13].Value = intSkip;

                // for EbayUserID
                objParam[14] = new SqlParameter("@EbayUserID", SqlDbType.UniqueIdentifier);
                objParam[14].Value = new Guid();

                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateMessages", objParam);
                }
                else if (strMode.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateMessages", objParam);
                }
                strResult = objParam[11].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                else if (strResult == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {

                        objDt = objDs.Tables[0];

                    }
                    return objDt;
                }
                return strResult;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        // Get Order Details, Order Item Details, Address Details for showing on MessageBody screen
        public static DataTable GetOrderDetailsWithAddress(string strEmailAddress, string MessageType)
        {
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@CustomerEmail", SqlDbType.VarChar, 100);
                objParam[0].Value = strEmailAddress;
                objParam[1] = new SqlParameter("@MessageType", SqlDbType.VarChar, 50);
                objParam[1].Value = MessageType.ToUpper();
                objDt = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrderDetailsWithAddress", objParam).Tables[0];
                return objDt;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        // get order item details by order id 
        public static DataTable OrderItemDetailsByOrderId(string strMessageType, Guid OrderId)
        {
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[0].Value = OrderId;
                objParam[1] = new SqlParameter("@MessageType", SqlDbType.VarChar, 50);
                objParam[1].Value = strMessageType.ToUpper();
                objDt = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrderItemDetailsByOrderId", objParam).Tables[0];
                return objDt;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// Get details of all eamil accounts for resending invoice from Message Body screen.
        /// </summary>
        /// <returns>DataTable of details of email Conditions</returns>
        public static DataTable GetEmailConditionsForTemplateOnMessageBody()
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            try
            {
                connection.Open();
                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetConditionDataForMessages").Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }
        /// <summary>
        /// To get the details of template by template id for sending emails from Message Body
        /// </summary>
        /// <param name="TemplateId">Template Id</param>
        /// <returns>DataTable of detail of template</returns>
        public static DataTable GetTemplateDetailsByTemplateIdForMessages(Guid TemplateId, Guid ConditionID)
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@TemplateID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = TemplateId;

                objParam[1] = new SqlParameter("@ConditionID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = ConditionID;

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDetailsByTemplateIdForMessages", objParam).Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }
        public static DataTable GetAllMessageTemplateSubject()
        {
            DataTable objDt = new DataTable();
            DataSet objDs = new DataSet();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[0].Direction = ParameterDirection.Output;

                objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAllMessageTemplateSubject", objParam);
                string strResult = objParam[0].Value.ToString();
                if (strResult == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];

                    }
                }
                return objDt;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetAllMessageTemplates(string strSubjectId)
        {
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@SubjectId", SqlDbType.VarChar, 50);
                objParam[0].Value = strSubjectId;
                objParam[1] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[1].Direction = ParameterDirection.Output;

                objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAllMessageTemplates", objParam);
                string strResult = objParam[1].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                else if (strResult == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];
                    }
                }
                return objDt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetEbayAccountAppKeys(string strMode)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            DataTable objDt = new DataTable();
            DataSet objDs = new DataSet();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode;


                objParam[1] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[1].Direction = ParameterDirection.Output;
                objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetEbayAppKeys", objParam);
                string strResult = objParam[1].Value.ToString();

                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                else if (strResult == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];

                    }
                }

                return objDt;

            }
            catch (Exception)
            {
                throw;
            }
            finally { conn.Close(); }
        }
        public static dynamic AddUpdateDeleteEbayUsers(string strMode, Guid ID, Guid AccountID, string UserName, string EbayAuthToken)
        {
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objParam[1] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = ID;

                objParam[2] = new SqlParameter("@AccountID", SqlDbType.UniqueIdentifier);
                objParam[2].Value = AccountID;

                objParam[3] = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
                objParam[3].Value = UserName;

                objParam[4] = new SqlParameter("@EbayAuthToken", SqlDbType.VarChar, EbayAuthToken.Length);
                objParam[4].Value = EbayAuthToken;

                objParam[5] = new SqlParameter("@Result", SqlDbType.VarChar, 50);
                objParam[5].Direction = ParameterDirection.Output;

                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "AddUpdateDeleteEbayUsers", objParam);
                }
                else if (strMode.ToUpper() == "INSERT")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "AddUpdateDeleteEbayUsers", objParam);
                }
                else if (strMode.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "AddUpdateDeleteEbayUsers", objParam);
                }
                string strResult = objParam[5].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                if (strResult == "0")
                {
                    if (objDs.Tables.Count > 0)
                    {
                        if (objDs.Tables[0].Rows.Count > 0)
                        {
                            objDt = objDs.Tables[0];

                        }
                    }
                    return objDt;
                }
                else
                {
                    return strResult;
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            finally { conn.Close(); }
        }
        public static string AddUpdateDeleteTemplateSubject(string strMode, string strId, string strTemplateSubject)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {

                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objParam[1] = new SqlParameter("@ID", SqlDbType.VarChar, strId.Length);
                objParam[1].Value = strId;

                objParam[2] = new SqlParameter("@Text", SqlDbType.VarChar, 500);
                objParam[2].Value = strTemplateSubject;

                objParam[3] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[3].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "AddUpdateDeleteMessageTemplateSubject", objParam);

                string strResult = objParam[3].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                return strResult;

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        public static dynamic GetAddUpdateDeleteRules(string strMode, Guid ID, Guid FolderID, string strName, string strConditon, int Position, bool IsActive, string strConditonID)
        {
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[9];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode;

                objParam[1] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = ID;

                objParam[2] = new SqlParameter("@FolderID", SqlDbType.UniqueIdentifier);
                objParam[2].Value = FolderID;

                objParam[3] = new SqlParameter("@Name", SqlDbType.VarChar, 500);
                objParam[3].Value = strName;

                objParam[4] = new SqlParameter("@Condition", SqlDbType.VarChar, 500);
                objParam[4].Value = strConditon;

                objParam[5] = new SqlParameter("@Position", SqlDbType.SmallInt);
                objParam[5].Value = Position;

                objParam[6] = new SqlParameter("@IsActive", SqlDbType.Bit);
                objParam[6].Value = Convert.ToByte(IsActive);

                objParam[7] = new SqlParameter("@ConditionID", SqlDbType.UniqueIdentifier);
                objParam[7].Value = new Guid(strConditonID);

                objParam[8] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[8].Direction = ParameterDirection.Output;
                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateRules", objParam);
                }
                else if (strMode.ToUpper() == "UPDATE" || strMode.ToUpper() == "DELETE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateRules", objParam);
                }
                string strResult = objParam[8].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                else if (strResult == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];
                    }
                    return objDt;
                }
                else
                {
                    return strResult;
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        public static dynamic GetAddUpdateMessageRuleCondition(string strMode, string strRuleConditionID, string strRuleConditionText)
        {
            DataTable objDt = new DataTable();
            DataSet objDs = new DataSet();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objParam[1] = new SqlParameter("@ConditionID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = new Guid(strRuleConditionID);

                objParam[2] = new SqlParameter("@ConditionText", SqlDbType.VarChar, 500);
                objParam[2].Value = strRuleConditionText;

                objParam[3] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[3].Direction = ParameterDirection.Output;

                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateRuleConditions", objParam);
                }
                else if (strMode.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateRuleConditions", objParam);
                }
                string strResult = objParam[3].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                else if (strResult == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];
                    }
                    return objDt;
                }
                else
                {
                    return strResult;
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally { conn.Close(); }
        }

        public static void MoveOrderToLogDatabase(Guid orderID)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[0].Value = orderID;

                var data = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "MoveOrderToLogDatabase", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return;
        }
    }
    
   

    /// <summary>
    ///Custom Exception class, we are using this class where we want to show custom errors
    /// </summary>
    public class CustomException : Exception
    {
        public CustomException(string strErrorMsg)
            : base(strErrorMsg)
        { }
    }
}
