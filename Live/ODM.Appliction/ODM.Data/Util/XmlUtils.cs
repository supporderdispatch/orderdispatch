﻿/*
*	XmlUtils.cs
*
*	Description:
*	A utility class to manage the manipulation of existing documents as well as the creation of new ones.
*   It also provides methods to extract data from Xml in a strongly typed format since the documents
*   themselves do not have the datatype specified.
*
*	Author:		Kulwant
*
*	Reviewer:	None
*
*	Copyright: 	© OIC
*
*	Revisions:
*	Version	Date        Person				Comments	

*/


using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Collections;
using System.Xml.XPath;
using System.Globalization;
namespace WBS.BussinessLayer
{
    /// <summary>
    /// A utility class to manage the manipulation of existing documents as well as the creation of new ones.
    /// It also provides methods to extract data from Xml in a strongly typed format since the documents
    /// themselves do not have the datatype specified.
    /// </summary>
    public sealed class XmlUtils
    {

        #region String Constants

        private const string ValueAttributeName = "Value";
        public const string XmlMessageName = "Message";
        public const string XmlMessageErrorName = "Error";
        public const string XmlMessageWarningName = "Warning";
        public const string XmlMessageInformationName = "Information";
        private const string MessageMessageAttributeName = "message";
        private const string MessageTypeAttributeName = "type";

        #endregion
        private XmlUtils()
        {
        }

        /// <summary>
        /// Loads the provided Xml string into an XmlDocument object and returns that object.
        /// </summary>
        /// <param name="xml">Xml string to load.</param>
        /// <returns>XmlDocument object containing the loaded Xml.</returns>
        public static XmlDocument LoadXml(string xml)
        {
            XmlDocument doc = null;

            if (!string.IsNullOrEmpty(xml))
            {
                doc = new XmlDocument();
                doc.LoadXml(xml);
            }

            return doc;
        }

        /// <summary>
        /// Writes an XmlElement node to the document being constructed with the specified text
        /// value.
        /// </summary>
        /// <remarks>
        /// <para>The actual value is written in a Value attribute and not the text() portion
        /// of the XmlElement.</para>
        /// </remarks>
        /// <param name="writer">Writer object that the Xml is created in.</param>
        /// <param name="elementName">The name of the element tag.</param>
        /// <param name="valueText">The value to write for the element text.</param>
        public static void WriteElementWithValue(XmlTextWriter writer, string elementName, string valueText)
        {
            writer.WriteStartElement(elementName);
            writer.WriteAttributeString(ValueAttributeName, valueText);
            writer.WriteEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlNodeList">xmlNodeList</param>
        /// <param name="attributeName">attributName e.g. RowStatus</param>
        /// <param name="duplicateValue">duplicateValue if exists</param>
        /// <returns></returns>
        public static bool IsUniqueAttributeValue(XmlNodeList xmlNodeList, string attributeName, out string duplicateValue)
        {
            duplicateValue = "";
            int iCounter = 0;
            foreach (XmlNode xmlNode in xmlNodeList)
            {
                for (int jCounter = iCounter + 1; jCounter < xmlNodeList.Count; jCounter++)
                {
                    XmlNode xmlTempNode = xmlNodeList.Item(jCounter);
                    if (xmlNode.Attributes[attributeName].Value.ToUpper().CompareTo(xmlTempNode.Attributes[attributeName].Value.ToUpper()) == 0)
                    {
                        duplicateValue = xmlNode.Attributes[attributeName].Value;
                        return false;
                    }
                }
                iCounter++;
            }
            return true;
        }

        /// <summary>
        /// Sets the value of an attribute for the specified node.
        /// </summary>
        /// <param name="node">Node to set the attribute value on.</param>
        /// <param name="name">Name of the attribute.</param>
        /// <param name="value">Value of the attribute.</param>
        public static void SetAttribute(XmlNode node, string name, string value)
        {
            XmlElement el = node as XmlElement;

            if (el != null)
                el.SetAttribute(name, value);
        }


        public static XmlDocument GetXmlSchema(XmlDocument doc)
        {
            XmlDataDocument data = new XmlDataDocument();

            using (StringReader sr = new StringReader(doc.OuterXml))
            {
                using (XmlTextReader reader = new XmlTextReader(sr))
                {
                    data.DataSet.ReadXmlSchema(reader);
                }
            }

            XmlDocument outputDoc = new XmlDocument();
            outputDoc.LoadXml(data.DataSet.GetXmlSchema());

            return outputDoc;
        }

        /// <summary>
        /// Generates an .NET DataTable object based on the Xml node and the given XPath expression.
        /// </summary>
        /// <param name="tableName">Name to give the DataTable.</param>
        /// <param name="node">Xml node that contains the data.</param>
        /// <param name="xpathExpression">XPath expression to get all rows for the table.</param>
        /// <returns>DataTable of the data found or an empty table.</returns>
        public static DataTable GetDataTable(string tableName, XmlNode node, string xpathExpression)
        {
            // create the data document xml
            StringBuilder filteredXml = new StringBuilder();
            filteredXml.AppendFormat("<{0}>", tableName);
            foreach (XmlNode selectedNode in node.SelectNodes(xpathExpression))
                filteredXml.Append(selectedNode.OuterXml);
            filteredXml.AppendFormat("</{0}>", tableName);

            // load the Xml into the data document
            XmlDataDocument dataDoc = new XmlDataDocument();
            using (StringReader reader = new StringReader(filteredXml.ToString()))
            {
                using (XmlTextReader xmlReader = new XmlTextReader(reader))
                {
                    dataDoc.DataSet.ReadXml(xmlReader);
                }
            }
            DataTable tbl = null;

            if (dataDoc.DataSet.Tables.Count != 0)
            {
                tbl = dataDoc.DataSet.Tables[0].Copy();

                tbl.TableName = tableName;
            }
            else
                tbl = new DataTable(tableName);

            // send back the table created
            return tbl;
        }

        #region Xml Typed Data Get Methods

        /// <summary>
        /// Gets the boolean value of the expression.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Boolean value of the expression. (True,true = True; all else = False)</returns>
        public static bool GetBool(XmlNode parentNode, string xpathExpression)
        {
            bool? result = GetBoolNullable(parentNode, xpathExpression);

            if (result.HasValue)
                return result.Value;

            return false;
        }

        /// <summary>
        /// Gets the boolean value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Boolean value of the expression or NULL. (True,true = True; all else = False)</returns>
        public static bool? GetBoolNullable(XmlNode parentNode, string xpathExpression)
        {
            bool? result = null;

            XmlNode node = parentNode.SelectSingleNode(xpathExpression);
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                bool outValue;

                if (bool.TryParse(node.InnerText, out outValue))
                    result = new bool?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the short value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Short value of the expression or MinValue.</returns>
        public static short GetShort(XmlNode parentNode, string xpathExpression)
        {
            short? result = GetShortNullable(parentNode, xpathExpression);

            if (result.HasValue)
                return result.Value;

            return short.MinValue;
        }

        /// <summary>
        /// Gets the short value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Short value of the expression or NULL.</returns>
        public static short? GetShortNullable(XmlNode parentNode, string xpathExpression)
        {
            short? result = null;

            XmlNode node = parentNode.SelectSingleNode(xpathExpression);
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                short outValue;

                if (short.TryParse(node.InnerText, out outValue))
                    result = new short?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the int value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Int value of the expression or MinValue.</returns>
        public static int GetInt(XmlNode parentNode, string xpathExpression)
        {
            int? result = GetIntNullable(parentNode, xpathExpression);

            if (result.HasValue)
                return result.Value;

            return int.MinValue;
        }

        /// <summary>
        /// Gets the int value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Int value of the expression or NULL.</returns>
        public static int? GetIntNullable(XmlNode parentNode, string xpathExpression)
        {
            int? result = null;

            XmlNode node = parentNode.SelectSingleNode(xpathExpression);
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                int outValue;

                if (Int32.TryParse(node.InnerText, out outValue))
                    result = new int?(outValue);
            }

            return result;
        }


        /// <summary>
        /// Gets the int value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Int value of the expression or MinValue.</returns>
        public static long GetLong(XmlNode parentNode, string xpathExpression)
        {
            long? result = GetLongNullable(parentNode, xpathExpression);

            if (result.HasValue)
                return result.Value;

            return long.MinValue;
        }

        /// <summary>
        /// Gets the int value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Long value of the expression or NULL.</returns>
        public static long? GetLongNullable(XmlNode parentNode, string xpathExpression)
        {
            long? result = null;

            XmlNode node = parentNode.SelectSingleNode(xpathExpression);
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                long outValue;

                if (Int64.TryParse(node.InnerText, out outValue))
                    result = new long?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the double value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Double value of the expression or MinValue.</returns>
        public static double GetDouble(XmlNode parentNode, string xpathExpression)
        {
            double? result = GetDoubleNullable(parentNode, xpathExpression);

            if (result.HasValue)
                return result.Value;

            return double.MinValue;
        }

        /// <summary>
        /// Gets the double value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Double value of the expression or NULL.</returns>
        public static double? GetDoubleNullable(XmlNode parentNode, string xpathExpression)
        {
            double? result = null;

            XmlNode node = parentNode.SelectSingleNode(xpathExpression);
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                double outValue;

                if (double.TryParse(node.InnerText, out outValue))
                    result = new double?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the float value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Float value of the expression or MinValue.</returns>
        public static float GetFloat(XmlNode parentNode, string xpathExpression)
        {
            float? result = GetFloatNullable(parentNode, xpathExpression);

            if (result.HasValue)
                return result.Value;

            return float.MinValue;
        }

        /// <summary>
        /// Gets the float value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Float value of the expression or NULL.</returns>
        public static float? GetFloatNullable(XmlNode parentNode, string xpathExpression)
        {
            float? result = null;

            XmlNode node = parentNode.SelectSingleNode(xpathExpression);
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                float outValue;

                if (float.TryParse(node.InnerText, out outValue))
                    result = new float?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the date value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Date value of the expression or MinValue.</returns>
        public static DateTime GetDate(XmlNode parentNode, string xpathExpression)
        {
            DateTime result = XmlUtils.GetDateTime(parentNode, xpathExpression);

            return result.Date;
        }

        /// <summary>
        /// Gets the date value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>Date value of the expression or NULL.</returns>
        public static DateTime? GetDateNullable(XmlNode parentNode, string xpathExpression)
        {
            DateTime? result = XmlUtils.GetDateTimeNullable(parentNode, xpathExpression);

            if (result.HasValue)
                return new DateTime?(result.Value.Date);

            return null;
        }

        /// <summary>
        /// Gets the datetime value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>DateTime value of the expression or MinValue.</returns>
        public static DateTime GetDateTime(XmlNode parentNode, string xpathExpression)
        {
            DateTime? result = GetDateTimeNullable(parentNode, xpathExpression);

            if (result.HasValue)
                return result.Value;

            return DateTime.MinValue;
        }

        /// <summary>
        /// Gets the datetime value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>DateTime value of the expression or NULL.</returns>
        public static DateTime? GetDateTimeNullable(XmlNode parentNode, string xpathExpression)
        {
            DateTime? result = null;

            XmlNode node = parentNode.SelectSingleNode(xpathExpression);
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                DateTime outValue;

                if (DateTime.TryParse(node.InnerText, out outValue))
                    result = new DateTime?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the string value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="xpathExpression">XPath expression to find the value to return.</param>
        /// <returns>String value of the expression or NULL.</returns>
        public static string GetString(XmlNode parentNode, string xpathExpression)
        {
            string result = string.Empty;

            XmlNode node = parentNode.SelectSingleNode(xpathExpression);
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                result = node.InnerText.Trim();
            }

            return result;
        }
        #endregion

        #region New Attribute Data Methods

        /// <summary>
        /// Gets the boolean value of the expression.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Boolean value of the expression. (True,true = True; all else = False)</returns>
        public static bool GetAttributeBool(XmlNode parentNode, string attribute)
        {
            bool? result = GetAttributeBoolNullable(parentNode, attribute);

            if (result.HasValue)
                return result.Value;

            return false;
        }

        /// <summary>
        /// Gets the boolean value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Boolean value of the expression or NULL. (True,true = True; all else = False)</returns>
        public static bool? GetAttributeBoolNullable(XmlNode parentNode, string attribute)
        {
            bool? result = null;

            XmlNode node = parentNode.Attributes[attribute];
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                bool outValue;

                if (bool.TryParse(node.InnerText, out outValue))
                    result = new bool?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the short value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Short value of the expression or MinValue.</returns>
        public static short GetAttributeShort(XmlNode parentNode, string attribute)
        {
            short? result = GetAttributeShortNullable(parentNode, attribute);

            if (result.HasValue)
                return result.Value;

            return short.MinValue;
        }

        /// <summary>
        /// Gets the short value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Short value of the expression or NULL.</returns>
        public static short? GetAttributeShortNullable(XmlNode parentNode, string attribute)
        {
            short? result = null;

            XmlNode node = parentNode.Attributes[attribute];
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                short outValue;

                if (short.TryParse(node.InnerText, out outValue))
                    result = new short?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the int value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Int value of the expression or MinValue.</returns>
        public static int GetAttributeInt(XmlNode parentNode, string attribute)
        {
            int? result = GetAttributeIntNullable(parentNode, attribute);

            if (result.HasValue)
                return result.Value;

            return int.MinValue;
        }

        /// <summary>
        /// Gets the long value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Int value of the expression or MinValue.</returns>
        public static long GetAttributeLong(XmlNode parentNode, string attribute)
        {
            long? result = GetAttributeLongNullable(parentNode, attribute);

            if (result.HasValue)
                return result.Value;

            return long.MinValue;
        }

        /// <summary>
        /// Gets the int value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Int value of the expression or NULL.</returns>
        public static int? GetAttributeIntNullable(XmlNode parentNode, string attribute)
        {
            int? result = null;

            XmlNode node = parentNode.Attributes[attribute];
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                int outValue;

                if (Int32.TryParse(node.InnerText, out outValue))
                    result = new int?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the long value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Int value of the expression or NULL.</returns>
        public static long? GetAttributeLongNullable(XmlNode parentNode, string attribute)
        {
            long? result = null;

            XmlNode node = parentNode.Attributes[attribute];
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                long outValue;

                if (Int64.TryParse(node.InnerText, out outValue))
                    result = new long?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the double value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Double value of the expression or MinValue.</returns>
        public static double GetAttributeDouble(XmlNode parentNode, string attribute)
        {
            double? result = GetAttributeDoubleNullable(parentNode, attribute);

            if (result.HasValue)
                return result.Value;

            return double.MinValue;
        }

        /// <summary>
        /// Gets the double value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Double value of the expression or NULL.</returns>
        public static double? GetAttributeDoubleNullable(XmlNode parentNode, string attribute)
        {
            double? result = null;

            XmlNode node = parentNode.Attributes[attribute];
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                double outValue;

                if (double.TryParse(node.InnerText, out outValue))
                    result = new double?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the float value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Float value of the expression or MinValue.</returns>
        public static float GetAttributeFloat(XmlNode parentNode, string attribute)
        {
            float? result = GetAttributeFloatNullable(parentNode, attribute);

            if (result.HasValue)
                return result.Value;

            return float.MinValue;
        }

        /// <summary>
        /// Gets the float value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Float value of the expression or NULL.</returns>
        public static float? GetAttributeFloatNullable(XmlNode parentNode, string attribute)
        {
            float? result = null;

            XmlNode node = parentNode.Attributes[attribute];
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                float outValue;

                if (float.TryParse(node.InnerText, out outValue))
                    result = new float?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the date value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Date value of the expression or MinValue.</returns>
        public static DateTime GetAttributeDate(XmlNode parentNode, string attribute)
        {
            DateTime result = XmlUtils.GetAttributeDateTime(parentNode, attribute);

            return result.Date;
        }

        /// <summary>
        /// Gets the date value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>Date value of the expression or NULL.</returns>
        public static DateTime? GetAttributeDateNullable(XmlNode parentNode, string attribute)
        {
            DateTime? result = XmlUtils.GetAttributeDateTimeNullable(parentNode, attribute);

            if (result.HasValue)
                return new DateTime?(result.Value.Date);

            return null;
        }

        /// <summary>
        /// Gets the datetime value of the expression or MinValue if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>DateTime value of the expression or MinValue.</returns>
        public static DateTime GetAttributeDateTime(XmlNode parentNode, string attribute)
        {
            DateTime? result = GetAttributeDateTimeNullable(parentNode, attribute);

            if (result.HasValue)
                return result.Value;

            return DateTime.MinValue;
        }

        /// <summary>
        /// Gets the datetime value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>DateTime value of the expression or NULL.</returns>
        public static DateTime? GetAttributeDateTimeNullable(XmlNode parentNode, string attribute)
        {
            DateTime? result = null;

            XmlNode node = parentNode.Attributes[attribute];
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                DateTime outValue;

                if (DateTime.TryParse(node.InnerText, out outValue))
                    result = new DateTime?(outValue);
            }

            return result;
        }

        /// <summary>
        /// Gets the string value of the expression or NULL if not found.
        /// </summary>
        /// <param name="parentNode">Xml node to start at.</param>
        /// <param name="attribute">Attribute expression to find the value to return.</param>
        /// <returns>String value of the expression or NULL.</returns>
        public static string GetAttributeString(XmlNode parentNode, string attribute)
        {
            string result = string.Empty;
            XmlNode node = parentNode.Attributes[attribute];
            if (node != null && !string.IsNullOrEmpty(node.InnerText))
            {
                result = node.InnerText.Trim();
            }
            return result;
        }

        #endregion

        #region Xml Document Messaging Methods
        /// <summary>
        /// Adds a child message element of the specified type to the Xml node.
        /// </summary>
        /// <param name="parentNode">Xml node to add the message to.</param>
        /// <param name="type">Type of message being added (Error, Warning, Informational).</param>
        /// <param name="message">The actual message to include.</param>
        public static void AddMessage(XmlNode parentNode, string type, string message)
        {
            XmlDocument owner = null;
            if (parentNode is XmlDocument)
            {
                owner = parentNode as XmlDocument;

                parentNode = owner.DocumentElement;
            }
            else
                owner = parentNode.OwnerDocument;

            XmlElement err = owner.CreateElement(XmlMessageName);
            err.SetAttribute(MessageMessageAttributeName, message);
            err.SetAttribute(MessageTypeAttributeName, type);

            parentNode.AppendChild(err);
        }

        /// <summary>
        /// Convenience method to add a parameterized Error message to a node.
        /// </summary>
        /// <param name="parentNode">Xml node to add the message to.</param>
        /// <param name="message">Message format with parameterized parts.</param>
        /// <param name="args">Parameter values to place in the message.</param>
        public static void AddErrorMessage(XmlNode parentNode, string message, params object[] args)
        {
            AddErrorMessage(parentNode, string.Format(message, args));
        }

        /// <summary>
        /// Convenience method to add an Error message to a node.
        /// </summary>
        /// <param name="parentNode">Xml node to add the message to.</param>
        /// <param name="message">Message to include.</param>
        public static void AddErrorMessage(XmlNode parentNode, string message)
        {
            string isMarked = XmlUtils.GetAttributeString(parentNode, "isMarked");
            if (isMarked != string.Empty)
                XmlUtils.SetAttribute(parentNode, "isMarked", "Y");
            AddMessage(parentNode, XmlMessageErrorName, message);
        }

        /// <summary>
        /// Convenience method to add a parameterized Warning message to a node.
        /// </summary>
        /// <param name="parentNode">Xml node to add the message to.</param>
        /// <param name="message">Message format with parameterized parts.</param>
        /// <param name="args">Parameter values to place in the message.</param>
        public static void AddWarningMessage(XmlNode parentNode, string message, params object[] args)
        {
            AddWarningMessage(parentNode, string.Format(message, args));
        }

        /// <summary>
        /// Convenience method to add an Warning message to a node.
        /// </summary>
        /// <param name="parentNode">Xml node to add the message to.</param>
        /// <param name="message">Message to include.</param>
        public static void AddWarningMessage(XmlNode parentNode, string message)
        {
            AddMessage(parentNode, XmlMessageWarningName, message);
        }

        /// <summary>
        /// Convenience method to add a parameterized Informational message to a node.
        /// </summary>
        /// <param name="parentNode">Xml node to add the message to.</param>
        /// <param name="message">Message format with parameterized parts.</param>
        /// <param name="args">Parameter values to place in the message.</param>
        public static void AddInformationMessage(XmlNode parentNode, string message, params object[] args)
        {
            AddWarningMessage(parentNode, string.Format(message, args));
        }

        /// <summary>
        /// Convenience method to add an Informational message to a node.
        /// </summary>
        /// <param name="parentNode">Xml node to add the message to.</param>
        /// <param name="message">Message to include.</param>
        public static void AddInformationMessage(XmlNode parentNode, string message)
        {
            AddMessage(parentNode, XmlMessageInformationName, message);
        }
        #endregion

        /// <summary>
        /// Copies the Xml message nodes from one document to another.
        /// </summary>
        /// <param name="source">Source Xml document.</param>
        /// <param name="destination">Destination Xml document.</param>
        public static void CopyMessages(XmlDocument source, XmlDocument destination)
        {
            XmlNodeList messages = source.SelectNodes("//Message");

            foreach (XmlNode message in messages)
            {
                XmlNode newMessage = destination.ImportNode(message, true);
                destination.DocumentElement.AppendChild(newMessage);
            }
        }

        /// <summary>
        /// Determines if the node has the specified attribute with a value.
        /// </summary>
        /// <param name="parentNode">Base xml node to start at.</param>
        /// <param name="attributeXPath">Attribute XPath expression to check.</param>
        /// <returns>True if the attribute has a value; false otherwise.</returns>
        public static bool HasAttributeWithValue(XmlNode parentNode, string attributeXPath)
        {
            XmlNode attributeNode = parentNode.SelectSingleNode(attributeXPath);
            if (attributeNode != null && !string.IsNullOrEmpty(attributeNode.InnerText))
                return true;

            return false;
        }

        #region Xml Sorting
        //enum defined for the type of sorting
        public enum SortingType { Text = 1, Numerical = 2, DateSorting = 3, FormattedAmount = 4 }



        /// <summary>
        ///	The following method accepts various different parameters and performs the sorting for the XML.
        /// </summary>
        /// <param name="xDoc">Xml Document which contains the nodes which are to be sorted.</param>
        /// <param name="xPath">Path for the nodelist which is to be sorted.(eg. Root/Activities)</param>
        /// <param name="sortKey">The element or attribute name on which sorting is to be done. 
        /// In case of an element only the name with path is specified. In case of attribute, xpath of that attribute is specified.
        /// </param>
        /// <param name="sortOrder">It is to specify whether sorting is to be done in Ascending(1) or Descending(2) order.</param>
        /// <param name="caseOrder">It is to specify whether sorting is to be done on lower case, uppercase or ignore case.</param>
        /// <param name="lang"></param>
        /// <param name="sortType">it specifes the datatype of the sortkey</param>
        /// <param name="dateFormat">This param specifies the format of the sortkey column, if the sortType is DateSorting</param> 
        /// <returns>XmlnodeList</returns>
        public static string GetSortedXml(XmlDocument xDoc, string xPath, string sortKey, XmlSortOrder sortOrder, XmlCaseOrder caseOrder, string lang, SortingType sortType)
        {
            XPathNavigator xPathNavigator = xDoc.CreateNavigator();//creating a xpathnavigator from the xml document.
            XPathExpression xPathExpression = xPathNavigator.Compile(xPath);//creating ths xpathexpression

            if (sortType == SortingType.DateSorting)
            {
                SortDateComparer comp = new SortDateComparer(sortOrder);
                xPathExpression.AddSort(sortKey, (IComparer)comp);
            }
            else if (sortType == SortingType.FormattedAmount)
            {
                FormattedAmountComparer comp = new FormattedAmountComparer(sortOrder);
                xPathExpression.AddSort(sortKey, (IComparer)comp);
            }
            else
            {
                xPathExpression.AddSort(sortKey, sortOrder, caseOrder, lang, (XmlDataType)sortType);
            }

            //The following code iterate thorugh the nodes one by one and builds the string xml of the node using the function GetXMLString.
            XPathNodeIterator xPathNodeIterator = xPathNavigator.Select(xPathExpression);

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (XmlTextWriter writer = new XmlTextWriter(sw))
                {
                    writer.WriteStartElement(xDoc.DocumentElement.Name);
                    while (xPathNodeIterator.MoveNext())
                    {
                        //calling the function which accepts the xml node and returns the outerXml of the node as string
                        if (xPathNodeIterator.Current.NodeType == XPathNodeType.Text)
                        {
                            writer.WriteCData(xPathNodeIterator.Current.Value);
                        }
                        else
                        {
                            //for Elements
                            writer.WriteStartElement(xPathNodeIterator.Current.Name);
                            if (xPathNodeIterator.Current.MoveToFirstAttribute())//checking for the attributes of the nodes, if found then building xml.
                            {
                                writer.WriteAttributeString(xPathNodeIterator.Current.Name, xPathNodeIterator.Current.Value);
                                while (xPathNodeIterator.Current.MoveToNextAttribute())
                                {
                                    writer.WriteAttributeString(xPathNodeIterator.Current.Name, xPathNodeIterator.Current.Value);
                                }
                                xPathNodeIterator.Current.MoveToParent();
                            }
                            writer.WriteEndElement();
                        }
                    }
                    writer.WriteEndElement();
                }
            }
            return sb.ToString();

        }

        #endregion

        #region Inner class SortDateComparer
        /// <summary>		
        /// SortDateComparer instance is passed to the overloaded AddSort() function where a Date comparison logic is required.
        ///		<members name="dateFormat" type="string">
        ///			To be used when your date is not in a conventional format.
        ///		</members>
        ///		<members name="sortOrder" type="XmlSortOrder">
        ///			Ascending(1) or Descending(2)
        ///		</members>
        /// </summary>

        class SortDateComparer : IComparer
        {
            private XmlSortOrder m_SortOrder;
            public SortDateComparer(XmlSortOrder sortOrder)
            {
                this.m_SortOrder = sortOrder;
            }

            public int Compare(Object object1, Object object2) //Implements IComparer.Compare
            {
                DateTime date1, date2;
                date1 = string.IsNullOrEmpty(object1.ToString()) ? new DateTime() : DateTime.Parse(object1.ToString());
                date2 = string.IsNullOrEmpty(object2.ToString()) ? new DateTime() : DateTime.Parse(object2.ToString());
                return (m_SortOrder == XmlSortOrder.Ascending) ? DateTime.Compare(date1, date2) : DateTime.Compare(date2, date1);
            }
        }
        #endregion

        #region Inner class FormattedAmountComparer
        /// <summary>
        /// Its instance is passed to the overloaded AddSort() function where a 
        /// comparison logic is required for Amounts.
        ///		<members name="sortOrder" type="XmlSortOrder">
        ///			Ascending(1) or Descending(2)
        ///		</members>
        /// </summary>
        class FormattedAmountComparer : IComparer
        {
            private XmlSortOrder m_SortOrder;
            public FormattedAmountComparer(XmlSortOrder sortOrder)
            {
                this.m_SortOrder = sortOrder;
            }
            public int Compare(Object object1, Object object2) //Implements IComparer.Compare
            {
                int retVal;
                double outValue;
                double? double1 = null;
                double? double2 = null;
                if (double.TryParse(object1.ToString(), out outValue))
                    double1 = new double?(outValue);
                else
                    double1 = 0;

                if (double.TryParse(object2.ToString(), out outValue))
                    double2 = new double?(outValue);
                else
                    double2 = 0;
                if (double1 > double2) retVal = 1;
                else if (double1 < double2) retVal = -1;
                else retVal = 0;
                return (m_SortOrder == XmlSortOrder.Ascending) ? retVal : (0 - retVal);
            }
        }
        #endregion

        #region Utility methods for Codelist
        /// <summary>
        /// Method to get code id of a code list item
        /// </summary>
        /// <param name="xml">codelist xml</param>
        /// <param name="codeType">Code Type of item</param>
        /// <param name="code">code of item</param>
        /// <returns>code id</returns>
        public static int? GetCodeIdForCode(string xml, string codeType, string code)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return null;
            }
            int? codeId = null;
            XmlDocument doc = XmlUtils.LoadXml(xml);
            XmlNode node = doc.SelectSingleNode("CodeListItemList/CodeListItem[@CodeType = '" + codeType + "' and @Code='" + code + "']");
            if (node != null)
            {
                codeId = XmlUtils.GetInt(node, "@CodeListID");
            }

            return codeId;
        }


        /// <summary>
        /// Method to get code id of a code list item
        /// </summary>
        /// <param name="doc">xml Document with codelist items</param>
        /// <param name="codeType">Code Type of item</param>
        /// <param name="code">code of item</param>
        /// <returns>code id</returns>
        public static int? GetCodeIdForCode(XmlDocument doc, string codeType, string code)
        {
            if (doc == null)
            {
                return null;
            }
            int? codeId = null;
            XmlNode node = doc.SelectSingleNode("CodeListItemList/CodeListItem[@CodeType = '" + codeType + "' and @Code='" + code + "']");
            if (node != null)
            {
                codeId = XmlUtils.GetInt(node, "@CodeListID");
            }

            return codeId;
        }


        /// <summary>
        /// method to get the code of code list item with given code id
        /// </summary>
        /// <param name="xml">code list xml </param>
        /// <param name="codeId">code id </param>
        /// <returns>code</returns>
        public static string GetCodeForCodeId(string xml, string codeId)
        {
            string code = string.Empty;
            if (string.IsNullOrEmpty(xml))
            {
                return code;
            }

            XmlDocument doc = XmlUtils.LoadXml(xml);
            XmlNode node = doc.SelectSingleNode("CodeListItemList/CodeListItem[@CodeListID = '" + codeId.ToString() + "' ]");
            if (node != null)
            {
                code = XmlUtils.GetString(node, "@Code");
            }

            return code;
        }

        /// <summary>
        /// method to get the code of code list item with given code id
        /// </summary>
        /// <param name="doc">xml document containing code list items </param>
        /// <param name="codeId">code id </param>
        /// <returns>code</returns>
        public static string GetCodeForCodeId(XmlDocument doc, string codeId)
        {
            string code = string.Empty;
            if (doc == null)
            {
                return code;
            }

            XmlNode node = doc.SelectSingleNode("CodeListItemList/CodeListItem[@CodeListID = '" + codeId.ToString() + "' ]");
            if (node != null)
            {
                code = XmlUtils.GetString(node, "@Code");
            }

            return code;
        }



        /// <summary>
        /// method to get the detail of code list item with given code id
        /// </summary>
        /// <param name="xml">code list xml </param>
        /// <param name="codeId">code id </param>
        /// <returns>code</returns>
        public static string GetCodeDetailForCodeId(string xml, string codeId)
        {
            string codeDetail = string.Empty;
            if (string.IsNullOrEmpty(xml))
            {
                return codeDetail;
            }

            XmlDocument doc = XmlUtils.LoadXml(xml);
            XmlNode node = doc.SelectSingleNode("CodeListItemList/CodeListItem[@CodeListID = '" + codeId.ToString() + "' ]");
            if (node != null)
            {
                codeDetail = XmlUtils.GetString(node, "@Code");
            }

            return codeDetail;
        }

        /// <summary>
        /// method to get the detail of code list item with given code id
        /// </summary>
        /// <param name="doc">xml document containing code list items </param>
        /// <param name="codeId">code id </param>
        /// <returns>code</returns>
        public static string GetCodeDetailForCodeId(XmlDocument doc, string codeId)
        {
            string codeDetail = string.Empty;
            if (doc == null)
            {
                return codeDetail;
            }

            XmlNode node = doc.SelectSingleNode("CodeListItemList/CodeListItem[@CodeListID = '" + codeId.ToString() + "' ]");
            if (node != null)
            {
                codeDetail = XmlUtils.GetString(node, "@CodeDetail");
            }

            return codeDetail;
        }
        #endregion
    }
}
