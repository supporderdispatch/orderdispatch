﻿using System;
namespace ODM.Data.Entity
{
    public class Customer
    {
        public System.Guid CustomerId { get; set; }
        public string ChannelBuyerName { get; set; }
        public string EmailAddress { get; set; }
        public string Town { get; set; }
        public string FullName { get; set; }
        public string Company { get; set; }
        public System.Guid CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
