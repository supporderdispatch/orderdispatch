﻿using System;
using System.Collections.Generic;
namespace ODM.Data.Entity
{
    public class Order
    {
        public System.Guid Orderid { get; set; }
        public long NumOrderId { get; set; }
        public long Status { get; set; }
        public bool LabelPrinted { get; set; }
        public string LabelError { get; set; }
        public bool InvoicePrinted { get; set; }
        public bool PickListPrinted { get; set; }
        public string Notes { get; set; }
        public bool PartShipped { get; set; }
        public long Marker { get; set; }
        public string ReferenceNum { get; set; }
        public string SecondaryReference { get; set; }
        public string ExternalReferenceNum { get; set; }
        public Nullable<System.DateTime> ReceivedDate { get; set; }
        public string Source { get; set; }
        public string SubSource { get; set; }
        public Nullable<bool> HoldOrCancel { get; set; }
        public Nullable<System.DateTime> DespatchByDate { get; set; }
        public Nullable<System.Guid> Location { get; set; }
        public Nullable<long> NumItems { get; set; }
        public Nullable<decimal> Subtotal { get; set; }
        public Nullable<decimal> PostageCost { get; set; }
        public decimal Tax { get; set; }
        public decimal TotalCharge { get; set; }
        public string PaymentMethod { get; set; }
        public System.Guid PaymentMethodId { get; set; }
        public decimal ProfitMargin { get; set; }
        public decimal TotalDiscount { get; set; }
        public string Currency { get; set; }
        public decimal CountryTaxRate { get; set; }
        public System.Guid CustomerId { get; set; }

        //Adding these properties to implement the API call with MyHermes.
        public List<OrderItem> OrderItems { get; set; }
        public string PostCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string BuyerPhoneNumber { get; set; }
        public double? Pack_TotalWeight { get; set; }
        public string ShippingMethod { get; set; }
        public string PostalTrackingNumber { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }

        //Adding these properties to implement the api call with spring
        public string Company { get; set; }
        public string Region { get; set; }
    }
}
