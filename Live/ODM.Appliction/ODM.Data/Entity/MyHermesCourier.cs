﻿using System;
namespace ODM.Data.Entity
{
    public class MyHermesCourier
    {
        public Guid CourierId { get; set; }
        public string CourierName { get; set; }
        public string ApiKey { get; set; }
        public string ApiUserID { get; set; }
        public string Params { get; set; }
        public string Printer { get; set; }
        public string Paper { get; set; }
        public decimal MarginLeft { get; set; }
        public decimal MarginTop { get; set; }
        public bool IsLandScape { get; set; }
        public bool IsInvoiceLandscape { get; set; }
        public string LabelType { get; set; }
        public Guid ShippingServiceId { get; set; }
        public Guid CourierServiceId { get; set; }
    }
}
