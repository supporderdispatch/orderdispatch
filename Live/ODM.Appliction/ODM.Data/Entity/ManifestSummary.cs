﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Data.Entity
{
    public class ManifestSummary
    {
        public Guid OrderID { get; set; }
        public string ServiceName { get; set; }
        public decimal AvgWeight { get; set; }
        public int TotalItems { get; set; }
    }
}
