﻿namespace ODM.Data.Entity
{
    public partial class User
    {
        public System.Guid Userid { get; set; }
        public string Loginname { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsAdmin { get; set; }
    }
}