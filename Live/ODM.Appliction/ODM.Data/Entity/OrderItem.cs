﻿using System;
using System.Collections.Generic;

namespace ODM.Data.Entity
{
    public class OrderItem
    {
        public System.Guid OrderItemId { get; set; }
        public System.Guid OrderId { get; set; }
        public System.Guid ItemId { get; set; }
        public System.Guid StockItemId { get; set; }
        public string SKU { get; set; }
        public string ItemSource { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public string CategoryName { get; set; }
        public string CompositeAvailablity { get; set; }
        public System.Guid RowId { get; set; }
        public Nullable<bool> StockLevelsSpecified { get; set; }
        public Nullable<long> OnOrder { get; set; }
        public Nullable<long> InOrderBook { get; set; }
        public Nullable<long> Level { get; set; }
        public Nullable<long> MinimumLevel { get; set; }
        public Nullable<decimal> AvailableStock { get; set; }
        public Nullable<decimal> PricePerUnit { get; set; }
        public Nullable<decimal> UnitCost { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<decimal> TaxRate { get; set; }
        public Nullable<decimal> Cost { get; set; }
        public Nullable<decimal> CostIncTax { get; set; }

        //Because "CompositeSubItems" is an array property
        public List<CompositeSubItems> CompositeSubItems { get; set; }

        public Nullable<bool> IsService { get; set; }
        public Nullable<decimal> SalesTax { get; set; }
        public Nullable<bool> TaxCostInclusive { get; set; }
        public Nullable<bool> PartShipped { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public string BarcodeNumber { get; set; }
        public Nullable<long> Market { get; set; }
        public string ChannelSKU { get; set; }
        public string ChannelTitle { get; set; }
        public Nullable<bool> HasImage { get; set; }
        public System.Guid ImageId { get; set; }

        //Because "AdditionalInfo" is an array property
        public List<AdditionalInfos> AdditionalInfo { get; set; }

        public Nullable<long> StockLevelIndicator { get; set; }
        public string BinRack { get; set; }
        public System.Guid CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public System.Guid ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }


        //Adding these properties to implement the API call with MyHermes
        public int Qty { get; set; }

    }
    //We need these two classe for the mapping of OrderItem class with json,
    //because "Items" node contains "CompositeSubItems" and "AdditionalInfo" properties as array
    public class AdditionalInfos
    {
        public Guid pkOptionId { get; set; }
        public string Property { get; set; }
        public string Value { get; set; }
    }
    public class CompositeSubItems
    {
        //Using temp property because given json is not returning any value for that 
        //and we need to pass value to stored procedure thats why we used that temporarily
        public string TempProperty { get; set; }
    }
    //End of additionaly classes
}
