﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Data.DbContext
{
    public class InvoiceTemplateContext : Context
    {
        private const string StoredProcedureForInvoiceTemplate = "GetSetInvoiceTemplates";

        public DataTable GetAllInvoiceTemplates()
        {
            var invoiceTemplates = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ALL_INVOICE_TEMPLATES";


                invoiceTemplates = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForInvoiceTemplate, objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return invoiceTemplates;
        }
        public DataTable GetInvoiceTemplates(Guid invoiceTemplateId)
        {
            var invoiceTemplate = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_INVOICE_TEMPLATE";

                objParam[1] = new SqlParameter("@Id", SqlDbType.UniqueIdentifier);
                objParam[1].Value = invoiceTemplateId;

                invoiceTemplate = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForInvoiceTemplate, objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return invoiceTemplate;
        }

        public System.Guid AddInvoiceTemplate(Entities.InvoiceTemplate.InvoiceTemplate invoiceTemplate)
        {
            System.Guid result;
            try
            {
                SqlParameter[] objParam = new SqlParameter[13];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "INSERT_INVOICE_TEMPLATE";

                objParam[1] = new SqlParameter("@TemplateName", SqlDbType.VarChar, 100);
                objParam[1].Value = invoiceTemplate.TemplateName;

                objParam[2] = new SqlParameter("@LayoutDesign", SqlDbType.VarChar, Int32.MaxValue);
                objParam[2].Value = invoiceTemplate.LayoutDesign;

                objParam[3] = new SqlParameter("@PageSize", SqlDbType.VarChar, 200);
                objParam[3].Value = invoiceTemplate.PageSize;

                objParam[4] = new SqlParameter("@PageOrientation", SqlDbType.Int);
                objParam[4].Value = invoiceTemplate.PageOrientation;

                objParam[5] = new SqlParameter("@PrintConditionCreteriaMatch", SqlDbType.Int);
                objParam[5].Value = ODM.Entities.InvoiceTemplate.PrintConditionCretiaMatch.Any;

                objParam[6] = new SqlParameter("@PrintConditionID", SqlDbType.UniqueIdentifier);
                objParam[6].Value = invoiceTemplate.PrintContidtionID;

                objParam[7] = new SqlParameter("@IsAutoPrint", SqlDbType.Bit);
                objParam[7].Value = invoiceTemplate.IsAutoPrint;

                objParam[8] = new SqlParameter("@IsEnabled", SqlDbType.Bit);
                objParam[8].Value = invoiceTemplate.IsEnabled;

                objParam[9] = new SqlParameter("@IsConditional", SqlDbType.Bit);
                objParam[9].Value = invoiceTemplate.IsConditional;

                objParam[10] = new SqlParameter("@IsPdfTemplate", SqlDbType.Bit);
                objParam[10].Value = invoiceTemplate.IsPdfTemplate;

                objParam[11] = new SqlParameter("@IPv4Address", SqlDbType.VarChar, 100);
                objParam[11].Value = invoiceTemplate.IPv4Address;

                objParam[12] = new SqlParameter("@SelectedPrinter", SqlDbType.VarChar, 100);
                objParam[12].Value = invoiceTemplate.SelectedPrinter;

                var resultSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForInvoiceTemplate, objParam).Tables[0].Rows[0]["TemplateId"].ToString();
                result = Guid.Parse(resultSet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }
        public void UpdateInvoiceTemplate(Entities.InvoiceTemplate.InvoiceTemplate invoiceTemplate)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[13];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "UPDATE_INVOICE_TEMPLATE";

                objParam[1] = new SqlParameter("@TemplateName", SqlDbType.VarChar, 100);
                objParam[1].Value = invoiceTemplate.TemplateName;

                objParam[2] = new SqlParameter("@LayoutDesign", SqlDbType.VarChar, Int32.MaxValue);
                objParam[2].Value = invoiceTemplate.LayoutDesign;

                objParam[3] = new SqlParameter("@PageSize", SqlDbType.VarChar, 200);
                objParam[3].Value = invoiceTemplate.PageSize;

                objParam[4] = new SqlParameter("@PageOrientation", SqlDbType.Int);
                objParam[4].Value = invoiceTemplate.PageOrientation;

                objParam[5] = new SqlParameter("@PrintConditionID", SqlDbType.UniqueIdentifier);
                objParam[5].Value = invoiceTemplate.PrintContidtionID;

                objParam[6] = new SqlParameter("@IsAutoPrint", SqlDbType.Bit);
                objParam[6].Value = invoiceTemplate.IsAutoPrint;

                objParam[7] = new SqlParameter("@IsEnabled", SqlDbType.Bit);
                objParam[7].Value = invoiceTemplate.IsEnabled;

                objParam[8] = new SqlParameter("@IsConditional", SqlDbType.Bit);
                objParam[8].Value = invoiceTemplate.IsConditional;

                objParam[9] = new SqlParameter("@IsPdfTemplate", SqlDbType.Bit);
                objParam[9].Value = invoiceTemplate.IsPdfTemplate;

                objParam[10] = new SqlParameter("@Id", SqlDbType.UniqueIdentifier);
                objParam[10].Value = invoiceTemplate.TemplateId;

                objParam[11] = new SqlParameter("@IPv4Address", SqlDbType.VarChar, 100);
                objParam[11].Value = invoiceTemplate.IPv4Address;

                objParam[12] = new SqlParameter("@SelectedPrinter", SqlDbType.VarChar, 100);
                objParam[12].Value = invoiceTemplate.SelectedPrinter;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForInvoiceTemplate, objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }
        public void DeleteInvoiceTemplate(string templateId)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "DELETE_INVOICE_TEMPLATE";

                objParam[1] = new SqlParameter("@Id", SqlDbType.UniqueIdentifier);
                objParam[1].Value = Guid.Parse(templateId);

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForInvoiceTemplate, objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public DataTable GetAnyTemplate()
        {
            var invoiceTemplate = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ANY_TEMPLATE";


                invoiceTemplate = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForInvoiceTemplate, objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return invoiceTemplate;

        }

        public void CopyInvoiceTemplate(string templateId)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "COPY_TEMPLATE";

                objParam[1] = new SqlParameter("@Id", SqlDbType.UniqueIdentifier);
                objParam[1].Value = Guid.Parse(templateId);

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForInvoiceTemplate, objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public object GetDataForInvoice(Guid orderId)
        {
            object result = new object();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[0].Value = orderId;


                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetDataForInvoice", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }
        public DataTable GetPrintConditions(Guid printConditionsId)
        {
            var printConditions = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_PRINT_CONDITION";

                objParam[1] = new SqlParameter("@PrintConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = printConditionsId;


                printConditions = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplatePrintCondition", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return printConditions;

        }


        public bool DeletePrintConditionsOf(Guid printConditionsId)
        {
            var result = false;
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "DELETE_PRINT_CONDITIONS";

                objParam[1] = new SqlParameter("@PrintConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = printConditionsId;


                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplatePrintCondition", objParam);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public bool SavePrintConditionsOf(Guid guid, ODM.Entities.InvoiceTemplate.PrintCondition printCondition)
        {
            var result = false;
            try
            {
                SqlParameter[] objParam = new SqlParameter[7];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "INSERT_PRINT_CONDITION";

                objParam[1] = new SqlParameter("@PrintConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = printCondition.PrintConditionId;

                objParam[2] = new SqlParameter("@TableName", SqlDbType.VarChar, 100);
                objParam[2].Value = printCondition.TableName;

                objParam[3] = new SqlParameter("@ColumnName", SqlDbType.VarChar, 100);
                objParam[3].Value = printCondition.ColumnName;

                objParam[4] = new SqlParameter("@Creterion", SqlDbType.VarChar, 20);
                objParam[4].Value = printCondition.Creterion;

                objParam[5] = new SqlParameter("@CompareWith", SqlDbType.VarChar, 100);
                objParam[5].Value = printCondition.CompareWith;

                objParam[6] = new SqlParameter("@CriteriaDataType", SqlDbType.VarChar, 20);
                objParam[6].Value = string.IsNullOrEmpty(printCondition.CriteriaDataType) ? "Text" : printCondition.CriteriaDataType;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplatePrintCondition", objParam);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public DataTable GetAllPrintConditions()
        {
            var printConditions = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ALL_PRINT_CONDITIONS";


                printConditions = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplatePrintCondition", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return printConditions;
        }

        public DataTable GetInvoiceTemplate(Guid printConditionId)
        {
            var invoiceTemplate = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_INVOICE_TEMPLATE";

                objParam[1] = new SqlParameter("@PrintConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = printConditionId;

                invoiceTemplate = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplatePrintCondition", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return invoiceTemplate;
        }
        public DataTable GetAllTemplateNamesWithPrintConditionForPdfTemplate()
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_TEMPLATE_NAME_WITH_CONDITION_PDF_TEMPLATE";


                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplates", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public DataTable GetAllTemplateNamesWithPrintCondition()
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_TEMPLATE_NAME_WITH_CONDITION";


                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplates", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void UpdatePrintConditionMatchForTemplate(Guid printConditionId, Entities.InvoiceTemplate.PrintConditionCretiaMatch selectedPrintCreterionMatchStrat)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[3];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "UPDATE_TEMPLATE_CRETERION_MATCH";

                objParam[1] = new SqlParameter("@PrintConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = printConditionId;

                objParam[2] = new SqlParameter("@PrintConditionCreteriaMatch", SqlDbType.Int);
                objParam[2].Value = (int)selectedPrintCreterionMatchStrat;


                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplatePrintCondition", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public string GetPrintConditionCreteriaMatch(Guid printConditionId)
        {
            var result = string.Empty;
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_TEMPLATE_CRETERION_MATCH";

                objParam[1] = new SqlParameter("@PrintConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = printConditionId;


                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplatePrintCondition", objParam).Tables[0].Rows[0]["PrintConditionCreteriaMatch"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void SavePageSize(Entities.InvoiceTemplate.PageSize pageSize)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "ADD_PAGE_SIZE";

                objParam[1] = new SqlParameter("@Name", SqlDbType.VarChar, 200);
                objParam[1].Value = pageSize.PageSizeName;

                objParam[2] = new SqlParameter("@Width", SqlDbType.Int);
                objParam[2].Value = pageSize.Width;

                objParam[3] = new SqlParameter("@Height", SqlDbType.Int);
                objParam[3].Value = pageSize.Height;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetPageSizes", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public void SavePageSize(string pageSizeName)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "DELETE_PAGE_SIZE";

                objParam[1] = new SqlParameter("@Name", SqlDbType.VarChar, 200);
                objParam[1].Value = pageSizeName;


                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetPageSizes", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public object GetAllCustomPageSizes()
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ALL_PAGE_SIZES";

                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetPageSizes", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public DataTable GetCustomPageSizes(string pageSizeName)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_PAGE_SIZE";

                objParam[1] = new SqlParameter("@Name", SqlDbType.VarChar, 200);
                objParam[1].Value = pageSizeName;

                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetPageSizes", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void UpdateInvoiceTemplateSetting(Entities.InvoiceTemplate.InvoiceTemplate updatedInvoiceSetting)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[7];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "UPDATE_INVOICE_TEMPLATE_SETTING";

                objParam[1] = new SqlParameter("@Id", SqlDbType.UniqueIdentifier);
                objParam[1].Value = updatedInvoiceSetting.TemplateId;

                objParam[2] = new SqlParameter("@IsAutoPrint", SqlDbType.Bit);
                objParam[2].Value = updatedInvoiceSetting.IsAutoPrint;

                objParam[3] = new SqlParameter("@IsEnabled", SqlDbType.Bit);
                objParam[3].Value = updatedInvoiceSetting.IsEnabled;

                objParam[4] = new SqlParameter("@IsConditional", SqlDbType.Bit);
                objParam[4].Value = updatedInvoiceSetting.IsConditional;

                objParam[5] = new SqlParameter("@IsPdfTemplate", SqlDbType.Bit);
                objParam[5].Value = updatedInvoiceSetting.IsPdfTemplate;

                objParam[6] = new SqlParameter("@PrintPriority", SqlDbType.Int);
                objParam[6].Value = (int)updatedInvoiceSetting.PrintPriority;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForInvoiceTemplate, objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public DataTable GetPrinterFor(Guid templateId, string systemIpAddress)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[3];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_PRINTER_FOR_TEMPLATE";

                objParam[1] = new SqlParameter("@Id", SqlDbType.UniqueIdentifier);
                objParam[1].Value = templateId;

                objParam[2] = new SqlParameter("@IPv4Address", SqlDbType.VarChar, 100);
                objParam[2].Value = systemIpAddress;

                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplates", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }
    }
}
