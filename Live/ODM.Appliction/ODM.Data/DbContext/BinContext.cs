﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.Util;
using ODM.Entities;
using Microsoft.ApplicationBlocks.Data;

namespace ODM.Data.DbContext
{
    public class BinContext : Context
    {
        private string StoredProcedureForBin = "GetAddUpdateRemoveOrdersInBin";

        public DataTable GetMaxNumOfAllowedBins()
        {
            var result = new DataTable();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "GET_NUM_OF_BINS";
            try
            {
                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param).Tables[0];
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
        //
        public bool UpdateMaxNumberOfBins(int numberOfBins)
        {
            var result = false;
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "UPDATE_MAXBIN";
            param[1] = new SqlParameter("@MaxBinAllowedNum", SqlDbType.Int, 200);
            param[1].Value = numberOfBins;
            try
            {
                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param);
                result=true;
            }
            catch (Exception ex)
            {
                result=false;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
        //
        public object AddItemToBin(string orderNumber, string barcode, int quantity)
        {
            object result = new object();
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "ADD_ITEM_TO_BIN";
            param[1] = new SqlParameter("@NumOrderId", SqlDbType.Int, 200);
            param[1].Value = Convert.ToInt32(orderNumber);
            param[2] = new SqlParameter("@barcodeNumber", SqlDbType.VarChar, 200);
            param[2].Value = barcode;
            param[3] = new SqlParameter("@Quantity", SqlDbType.Int);
            param[3].Value = quantity;

            try
            {
                DataSet updatedBin = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param);
                result=new { Status = true, Datatable = updatedBin };
            }
            catch (Exception ex)
            {
                PostOperationEvents();
                result= new { Status = false, Datatable = String.Empty };
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
        //
        public DataTable GetBins()
        {
            var result = new DataTable();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "GET_BINS";

            try
            {
                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
        //
        public DataTable GetBinDetails(int orderNumber)
        {
            var result = new DataTable();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "GET_BIN_DETAILS";
            param[1] = new SqlParameter("@NumOrderId", SqlDbType.Int, 200);
            param[1].Value = Convert.ToInt32(orderNumber);
            try
            {
                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
        //
        public bool AddItemToBinHavingNoBarcode(int orderNumber, int numOfItemsHavingNoBarcode)
        {
            var result = false;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "ADD_ITEM_TO_BIN_NOBARCODE";
            param[1] = new SqlParameter("@NumOrderId", SqlDbType.Int, 200);
            param[1].Value = Convert.ToInt32(orderNumber);
            param[2] = new SqlParameter("@numberOfItemsToAddInBin", SqlDbType.Int, 200);
            param[2].Value = numOfItemsHavingNoBarcode;

            try
            {
                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
        //
        public bool AddIndividualItemToBin(string sku, int orderNumber, string barcode)
        {
            var result = false;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "ADD_INDIVIDUAL_ITEM_IN_BIN";
            param[1] = new SqlParameter("@NumOrderId", SqlDbType.Int, 200);
            param[1].Value = orderNumber;
            param[2] = new SqlParameter("@sku", SqlDbType.VarChar, 200);
            param[2].Value = sku.Trim();
            param[3] = new SqlParameter("@barcodeNumber", SqlDbType.VarChar, 200);
            param[3].Value = barcode.Trim();
            try
            {
                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param);
                result = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
        //
        public DataTable GetIndividualBin(int orderNumber)
        {
            var result = new DataTable();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "GET_BIN_DETAILS";
            param[1] = new SqlParameter("@NumOrderId", SqlDbType.Int, 200);
            param[1].Value = Convert.ToInt32(orderNumber);
            try
            {
                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
        //
        public void RemoveBinsOfProcessedOrders()
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "REMOVE_BINS_OF_PROCESSED_ORDERS";
            try
            {
                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
        }
        public DataTable GetBinsHavingThisBarcode(string barcode)
        {
            var result = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "CHECK_BARCODE_IN_CONSOLIDATED_ORDERS";
            param[1] = new SqlParameter("@barcodeNumber", SqlDbType.VarChar, 200);
            param[1].Value = barcode;
            try
            {
                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param).Tables[0];
            }
            catch (Exception ex)
            {
                PostOperationEvents();
                result=new DataTable();
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
        public bool RemoveOrderFromBin(int orderNumber)
        {
            var result = false;
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "REMOVE_ORDER_FROM_BIN";
            param[1] = new SqlParameter("@NumOrderId", SqlDbType.Int, 200);
            param[1].Value = Convert.ToInt32(orderNumber);
            try
            {
                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForBin, param);
                result=true;
            }
            catch
            {
                result=false;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
    }
}
