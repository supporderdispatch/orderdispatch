﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Data.DbContext
{
    public class VirtualTables
    {
        private static List<string> _Order = new List<string>()
        {
            "Orderid", "NumOrderId","Status","Notes","PartShipped",	"Marker",
            "ReferenceNum", "SecondaryReference","ExternalReferenceNum", "ReceivedDate",
            "Source", "SubSource","DespatchByDate","Location","NumItems","Subtotal",  
            "PostageCost","Tax","TotalCharge","PaymentMethod","PaymentMethodId","ProfitMargin", 
            "TotalDiscount", "Currency","CountryTaxRate","ProcessedDateTime","FulfilmentLocationId",
            "ServiceName",	"TrackingNumber"
        };

        public static List<string> Order 
        {
            get {
                _Order.Sort();
                return _Order;
            }
            set
            {
                _Order = value;
            }
        }
        private static List<string> _OrderItem = new List<string>()
        {
            "SKU","ItemSource",	"Title","Quantity",	"CategoryName",	"PricePerUnit",
            "UnitCost",	"Discount",	"Tax",	"TaxRate",	"Cost",	"CostIncTax",	"CompositeSubItems",
            "SalesTax",	"TaxCostInclusive",	"PartShipped",	"Weight",	"BarcodeNumber","Market",
            "ChannelSKU","ChannelTitle","AdditionalInfo","StockLevelIndicator"
        };

        public static List<string> OrderItem 
        {
            get
            {
                _OrderItem.Sort();
                return _OrderItem;
            }
            set
            {
                _OrderItem = value;
            }
        }
        
        private static List<string> _SenderAddress = new List<string>() 
        {
            "FirstName","LastName",	"PhoneNumber",	"Street1",	"Street2",	"StreetNumber",
            "StreetNumberSuffix","Region",	"City",	"Country",	"PostalCode",	"Company",	"Email"
        };

        public static List<string> SenderAddress
        {
            get
            {
                _SenderAddress.Sort();
                return _SenderAddress;
            }
            set
            {
                _SenderAddress = value;
            }
        }
        private static List<string> _CustomerAddress = new List<string>()
        {
            "FullName",	"Company",	"Address1",	"Address2",	"Address3",	"Town",	"Region",	"PostCode",
            "Country",	"EmailAddress",	"PhoneNumber"     
        };
        public static List<string> CustomerAddress
        {
            get
            {
                _CustomerAddress.Sort();
                return _CustomerAddress;
            }
            set
            {
                _CustomerAddress = value;
            }
        }
    }
}
