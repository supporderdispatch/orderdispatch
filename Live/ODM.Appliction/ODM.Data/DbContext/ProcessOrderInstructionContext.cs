﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ODM.Data.DbContext
{
  public  class ProcessOrderInstructionContext:Context
    {
      private const string StoredProcedureForConditions = "GetSetConditions";
      public DataTable GetAllConditions()
      {
          var invoiceTemplates = new DataTable();
          try
          {
              SqlParameter[] objParam = new SqlParameter[1];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "GET_ALL_Conditions";


              invoiceTemplates = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForConditions, objParam).Tables[0];
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
          return invoiceTemplates;
      }
      public bool DeleteConditionsCriteriaOf(Guid ConditionsId)
      {
          var result = false;
          try
          {
              SqlParameter[] objParam = new SqlParameter[2];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "DELETE_Process_Order_CONDITIONS";

              objParam[1] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
              objParam[1].Value = ConditionsId;


              SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetConditionCritera", objParam);
              result = true;
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
          return result;
      }
      public bool SaveConditionsCriteriaOf(Guid guid, ODM.Entities.ConditionCriteria conditionCriteria)
      {
          var result = false;
          try
          {
              SqlParameter[] objParam = new SqlParameter[9];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "INSERT_CONDITION";

              objParam[1] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
              objParam[1].Value = conditionCriteria.ConditionId;

              objParam[2] = new SqlParameter("@TableName", SqlDbType.VarChar, 100);
              objParam[2].Value = conditionCriteria.TableName;

              objParam[3] = new SqlParameter("@ColumnName", SqlDbType.VarChar, 100);
              objParam[3].Value = conditionCriteria.ColumnName;

              objParam[4] = new SqlParameter("@Creterion", SqlDbType.VarChar, 20);
              objParam[4].Value = conditionCriteria.Creterion;

              objParam[5] = new SqlParameter("@CompareWith", SqlDbType.VarChar, 100);
              objParam[5].Value = conditionCriteria.CompareWith;

              objParam[6] = new SqlParameter("@CriteriaDataType", SqlDbType.VarChar, 20);
              objParam[6].Value = string.IsNullOrEmpty(conditionCriteria.CriteriaDataType) ? "Text" : conditionCriteria.CriteriaDataType;

              objParam[7] = new SqlParameter("@ConditionCriteriaId", SqlDbType.UniqueIdentifier);
              objParam[7].Value = conditionCriteria.ConditionCriteriaId;

              objParam[8] = new SqlParameter("@ResultType", SqlDbType.VarChar, 20);
              objParam[8].Value = conditionCriteria.ResultType.ToString();


              SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetConditionCritera", objParam);
              result = true;
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
          return result;
      }
      public DataTable GetProcessOrderConditionCriteriaOf(Guid conditionId)
      {
          var ProcessOrderConditions = new DataTable();
          try
          {
              SqlParameter[] objParam = new SqlParameter[2];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "GET_Process_Order_Condition";

              objParam[1] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
              objParam[1].Value = conditionId;


              ProcessOrderConditions = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetConditionCritera", objParam).Tables[0];
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
          return ProcessOrderConditions;

      }
      public bool SaveProcessOrderConditionCriteriaOf(Guid guid, ODM.Entities.ConditionCriteria conditionCriteria)
      {
          var result = false;
          try
          {
              SqlParameter[] objParam = new SqlParameter[7];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "INSERT_CONDITION";

              objParam[1] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
              objParam[1].Value = conditionCriteria.ConditionId;

              objParam[2] = new SqlParameter("@TableName", SqlDbType.VarChar, 100);
              objParam[2].Value = conditionCriteria.TableName;

              objParam[3] = new SqlParameter("@ColumnName", SqlDbType.VarChar, 100);
              objParam[3].Value = conditionCriteria.ColumnName;

              objParam[4] = new SqlParameter("@Creterion", SqlDbType.VarChar, 20);
              objParam[4].Value = conditionCriteria.Creterion;

              objParam[5] = new SqlParameter("@CompareWith", SqlDbType.VarChar, 100);
              objParam[5].Value = conditionCriteria.CompareWith;

              objParam[6] = new SqlParameter("@CriteriaDataType", SqlDbType.VarChar, 20);
              objParam[6].Value = string.IsNullOrEmpty(conditionCriteria.CriteriaDataType) ? "Text" : conditionCriteria.CriteriaDataType;

              SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetConditionCritera", objParam);
              result = true;
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
          return result;
      }


      public void CopyCondition(Guid conditionId)
      {
          try
          {
              SqlParameter[] objParam = new SqlParameter[7];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "COPY_CONDITION";

              objParam[1] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
              objParam[1].Value = conditionId;


              SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForConditions, objParam);
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
      }

      public void DeleteCondition(Guid conditionId)
      {
          try
          {
              SqlParameter[] objParam = new SqlParameter[2];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "DELETE_CONDITION";

              objParam[1] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
              objParam[1].Value = conditionId;


              SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForConditions, objParam);
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
      }

      public void UpdateCondition(Entities.Condition updatedCondition)
      {
          try
          {
              SqlParameter[] objParam = new SqlParameter[4];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "UPDATE_CONDITION";

              objParam[1] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
              objParam[1].Value = updatedCondition.ConditionId;

              objParam[2] = new SqlParameter("@ConditionName", SqlDbType.VarChar,100);
              objParam[2].Value = updatedCondition.ConditionName;

              objParam[3] = new SqlParameter("@IsEnabled", SqlDbType.Bit);
              objParam[3].Value = updatedCondition.IsEnabled;



              SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForConditions, objParam);
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
      }

      public void SaveConditionCriteriaResult(Entities.ProcessOrderInstrunctions.CriteriaResult criteriaResult)
      {
          try
          {
              SqlParameter[] objParam = new SqlParameter[5];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "INSERT_CRITERIA_RESULT";

              objParam[1] = new SqlParameter("@ConditionCriteriaId", SqlDbType.UniqueIdentifier);
              objParam[1].Value = criteriaResult.ConditionCriteriaId;

              objParam[2] = new SqlParameter("@ResultType", SqlDbType.VarChar, 20);
              objParam[2].Value = criteriaResult.ResultType.ToString();

              objParam[3] = new SqlParameter("@ResultImage", SqlDbType.VarChar,int.MaxValue);
              objParam[3].Value = criteriaResult.ImageResult;

              objParam[4] = new SqlParameter("@ResultText", SqlDbType.VarChar,1000);
              objParam[4].Value = criteriaResult.TextResult;


              SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetConditionCriteriaResult", objParam);
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
      }

      public void DeleteCriteria(Guid criteriaId)
      {
          try
          {
              SqlParameter[] objParam = new SqlParameter[5];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "DELETE_CRITERIA";

              objParam[1] = new SqlParameter("@ConditionCriteriaId", SqlDbType.UniqueIdentifier);
              objParam[1].Value = criteriaId;

              SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetConditionCritera", objParam);
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
      }

      public object GetConditionCriteriaResult(Guid creteriaId)
      {
          var result = new DataTable();
          try
          {
              SqlParameter[] objParam = new SqlParameter[5];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "GET_CRITERIA_RESULT";

              objParam[1] = new SqlParameter("@ConditionCriteriaId", SqlDbType.UniqueIdentifier);
              objParam[1].Value = creteriaId;

              result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetConditionCriteriaResult", objParam).Tables[0];
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
          return result;
      }

      public object GetAllActiveCriteria()
      {
          var result = new DataTable();
          try
          {
              SqlParameter[] objParam = new SqlParameter[1];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "GET_ALL_ACTIVE_CRITERIA";


              result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetConditionCritera", objParam).Tables[0];
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
          return result;
      }

      public DataTable GetCriteriaResultOf(string successCriteria)
      {
          var result = new DataTable();
          try
          {
              SqlParameter[] objParam = new SqlParameter[2];

              objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
              objParam[0].Value = "GET_SUCCESS_CRITERIA_RESULT";

              objParam[1] = new SqlParameter("@SuccessConditionCriteriaIds", SqlDbType.VarChar, 1000);
              objParam[1].Value = successCriteria;

              result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetConditionCriteriaResult", objParam).Tables[0];
          }
          catch (Exception ex)
          {
              throw ex;
          }
          finally { PostOperationEvents(); }
          return result;
      }
    }
}
