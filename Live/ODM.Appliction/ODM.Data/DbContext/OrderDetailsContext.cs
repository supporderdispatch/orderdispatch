﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Data.DbContext
{
    public class OrderDetailsContext:Context
    {
        public OrderDetailsContext() { }

        public DataTable GetAllOpenOrders(string searchParam)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@SearchInput", SqlDbType.VarChar, 8000);
                param[0].Value = searchParam;
                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAllOpenOrders_New", param).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }

        public DataTable GetOrderItems(Guid orderId)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objorderParams = new SqlParameter[2];
                objorderParams[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objorderParams[0].Value = orderId;

                objorderParams[1] = new SqlParameter("@OrderItemId", SqlDbType.NVarChar, 1000);
                objorderParams[1].Value = string.Empty;

                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetItemDetail", objorderParams).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }

        public string GetOrderImage(Guid orderId)
        {
            var result = string.Empty;
            try
            {
                SqlParameter[] objorderParams = new SqlParameter[3];
                objorderParams[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 20);
                objorderParams[0].Value = "GET";

                objorderParams[1] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objorderParams[1].Value = orderId;

                objorderParams[2] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objorderParams[2].Direction = ParameterDirection.Output;

                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateDeleteImagesByOrderId", objorderParams).Tables[0].Rows[0]["Image"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
    }
}
