﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.Util;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace ODM.Data.DbContext
{
    public class Context
    {
        public SqlConnection conn;

        public Context()
        {
            conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
        }
        public void PostOperationEvents()
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
    }
}
