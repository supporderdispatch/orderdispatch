﻿using ODM.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.DbContext;
using System.Data.Entity;
using  System.ComponentModel.DataAnnotations.Schema;
using ODM.Data.Util;
using ODM.Entities;

namespace ODM.Core.UserTracking
{
    public class UsersStatisticsReportMethods:Context
    {
        IUserService userService;

        public UsersStatisticsReportMethods()
        {
            userService = new UserService(MakeConnection.GetConnection());
        }

        public List<FinalViewDigest> GetAllUsersReportForDate(DateTime OfDate)
        {
            var users = userService.GetAllUsers().Select(
                x => new {
                    UserId = x.Userid,
                    UserName = x.Loginname })
                    .ToList();

            var finalResultViewList = new List<FinalViewDigest>();
            foreach (var user in users)
            {
                var result = Activator.CreateInstance<Logger>().GetUserLogs(user.UserId, OfDate);
                result.UserId = user.UserId;
                result.UserName = user.UserName;
                result.OfDate = OfDate;
                finalResultViewList.Add(result);
            }
            return finalResultViewList;
        }

        public List<List<FinalViewDigest>> GetAllUsersRangeReport(DateTime fromDate,DateTime toDate)
        {
            return new Logger().GetAllUserStatisticsFrom(fromDate,toDate);
        }   
    }
}
