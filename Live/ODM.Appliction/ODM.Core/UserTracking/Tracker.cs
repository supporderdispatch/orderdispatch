﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace ODM.Core.UserTracking
{
    public class Tracker
    {
        public Stopwatch timer;

        public Tracker()
        {
            timer = new Stopwatch();
        }

        public void BeginTracking()
        {
            timer.Restart();
        }

        public double StopTracking()
        {
            timer.Stop();
            return timer.Elapsed.TotalSeconds;
        }
    }
}
