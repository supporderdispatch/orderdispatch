﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Core;
using ODM.Core.Helpers;
using ODM.Entities;
using System.Diagnostics;
using ODM.Data.Util;

namespace ODM.Core.UserTracking
{
    public static class UserTracker
    {
        public static Guid UserId;
        public static double SecondsElapsed;
        public static Tracker tracker = new Tracker();
        public static Guid SessionId = System.Guid.NewGuid();
        public static DateTime? LoginTime;
        public static DateTime? LogoutTime;
        public static List<OrderExecutionDetails> Digest;
        public static bool UserTrackingPaused;
        public static int NumberOfSessionWithoutAnyOrderProcessed = 0;

        public static void BeginTrackingForUser(Guid uid)
        {
            UserTrackingPaused = false;
            UserId = uid;
            LoginTime = DateTime.UtcNow;
            tracker.BeginTracking();
            BeginSynWithIntervalOf(5);
        }
        public static void BeginSynWithIntervalOf(int minutes)
        {
            var syncTimer = new System.Timers.Timer();
            syncTimer.Elapsed += (sender, e) =>
            {
                SecondsElapsed = tracker.StopTracking();
                UserTrackingPaused = !LogUserStatisticsInDb();
                if (UserTrackingPaused)
                {
                    LoginTime = null;
                    SessionId = Guid.NewGuid();
                    syncTimer.Stop();
                }
                else
                {
                    tracker.BeginTracking();
                }
            };
            var milisecs = (int)TimeSpan.FromMinutes(minutes).TotalMilliseconds;
            syncTimer.Interval = milisecs;
            syncTimer.Start();
        }
        public static void EndTracking()
        {
            try
            {
                LogoutTime = DateTime.UtcNow;
                LogUserStatisticsInDb();
            }
            catch 
            {
            }
        }

        public static bool LogUserStatisticsInDb()
        {
            Digest = OrderTracker.TrackingDigest.ToList();
            if (Digest.Where(x => x.Processed).ToList().Count == 0)
            {
                NumberOfSessionWithoutAnyOrderProcessed++;
            }

            var result = NumberOfSessionWithoutAnyOrderProcessed < 3 ? true : false;
            var logger = new Logger();
            var stats = new UserStatics();

            stats.OrderDigest = Digest;
            stats.UserId = UserId;
            stats.SessionId = SessionId;
            stats.LoginTime = object.ReferenceEquals(LoginTime, null) ? DateTime.UtcNow : LoginTime;
            stats.LogoutTime = LogoutTime;
            logger.AddUserLogs(stats);
            Digest.Clear();
            OrderTracker.TrackingDigest = OrderTracker.TrackingDigest.Where(x => !x.Processed).ToList();
            return result;
        }
    }
}
