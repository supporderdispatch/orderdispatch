﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Entities;
using ODM.Data.DbContext;

namespace ODM.Core
{
    public class ProcessStateOfOrder
    {
        ProcessStateContext context;

        public ProcessStateOfOrder()
        {
            context = new ProcessStateContext();
        }

        public void UpdateProcessesStatusOfOrder(Guid orderId, ProcessCompleted state)
        {
            context.UpdateProcessesStatusOfOrder(orderId,state);
        }
        public void UnblockOrders(List<string> orderIds)
        {
            foreach (var orderId in orderIds)
            {
                context.UnblockOrder(orderId);
            }
        }
    }
}
