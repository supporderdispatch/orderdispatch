﻿using ODM.Entities.InvoiceTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iText.Kernel.Geom;
using System.Reflection;
using ODM.Data.DbContext;
using System.Data;

namespace ODM.Core.InvoiceTemplate
{
    public class PageSettings
    {

        public static PageDimension GetPageDimentions(string pageSize)
        {
            var pageDimension = new PageDimension();
            var rectangle = GetPageSize(pageSize);
            pageDimension.Height = rectangle.GetHeight();
            pageDimension.width = rectangle.GetWidth();
            return pageDimension;
        }
        public static Rectangle GetPageSize(string pageSizeName)
        {
            if ((ODM.Core.InvoiceTemplate.PageSettings.GetAllCustomPageSizes() as DataTable).AsEnumerable()
                .Select(x => x.Field<string>("Name")).ToList().Contains(pageSizeName))
            {
                var pageSize=GetCustomPageSize(pageSizeName);
                return new Rectangle(pageSize.Width,pageSize.Height);
                
            }
            return (iText.Kernel.Geom.Rectangle)typeof(iText.Kernel.Geom.PageSize).GetField(pageSizeName, BindingFlags.Public | BindingFlags.Static).GetValue(null);
        }

        public static void SavePageSize(Entities.InvoiceTemplate.PageSize pageSize)
        {
           var context = new InvoiceTemplateContext();
           context.SavePageSize(pageSize);

        }

        public static void DeletePageSize(string pageSizeName)
        {
            var context = new InvoiceTemplateContext();
            context.SavePageSize(pageSizeName); 
        }

        public static object GetAllCustomPageSizes()
        {
            var context = new InvoiceTemplateContext();
            return context.GetAllCustomPageSizes();
        }

        public static ODM.Entities.InvoiceTemplate.PageSize GetCustomPageSize(string pageSizeName)
        {
            var context = new InvoiceTemplateContext();
            return context.GetCustomPageSizes(pageSizeName).AsEnumerable()
                .Select(x=>new ODM.Entities.InvoiceTemplate.PageSize() 
                {
                   PageSizeName=x.Field<string>("Name"),
                   Width=x.Field<int>("Width"),
                   Height=x.Field<int>("Height")
                }).FirstOrDefault();
        }
        public object GetPrinterFor(Guid templateId, string systemIpAddress)
        {
            var context = new InvoiceTemplateContext();
            return context.GetPrinterFor(templateId, systemIpAddress);
        }
    }
}
