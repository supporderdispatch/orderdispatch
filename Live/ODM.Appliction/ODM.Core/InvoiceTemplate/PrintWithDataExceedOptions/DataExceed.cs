﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ODM.Core.InvoiceTemplate.PrintWithDataExceedOptions
{
    public class DataExceed
    {
        public void SaveDataPrintSettings(Entities.InvoiceTemplate.InvoiceTemplateDataContent.DataPrintSettings dataPrintSettings)
        {
            var context=new ODM.Data.DbContext.InvoiceTemplateDataSettingContext();
            context.SaveDataPrintSettings(dataPrintSettings);
        }

        public Entities.InvoiceTemplate.InvoiceTemplateDataContent.DataPrintSettings GetDataSettings(Guid templateId)
        {
            var context = new ODM.Data.DbContext.InvoiceTemplateDataSettingContext();
            var result = context.GetDataPrintSettings(templateId);
            if (result.Rows.Count > 0)
            {
                return context.GetDataPrintSettings(templateId).AsEnumerable()
               .Select(x => new Entities.InvoiceTemplate.InvoiceTemplateDataContent.DataPrintSettings()
               {
                   TemplateId = templateId,
                   DataExceedActionSelected = (ODM.Entities.InvoiceTemplate.InvoiceTemplateDataContent.TableContentExceedOptions)x.Field<int>("DataExceedSelectedOption"),
                   TryAdjustBlock = Convert.ToBoolean(x.Field<bool>("TryAdjustBlock")),
                   TemplateSelectedWhenDataExceed = x.Field<Guid>("TemplateSelectedWhenDataExceed")
               }).FirstOrDefault();
            }
            return new Entities.InvoiceTemplate.InvoiceTemplateDataContent.DataPrintSettings() 
            {
                DataExceedActionSelected=ODM.Entities.InvoiceTemplate.InvoiceTemplateDataContent.TableContentExceedOptions.OverFlowHidden,
                TryAdjustBlock=true
            };
           
        }
    }
}
