﻿using HtmlAgilityPack;
using ODM.Core.Helpers;
using ODM.Entities.InvoiceTemplate;
using ODM.Entities.InvoiceTemplate.InvoiceTemplateDataContent;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Core.InvoiceTemplate.PrintWithDataExceedOptions
{
    public class CreateNewPageAdjustingOtherFields
    {
        public static string GetParsedInvoice(string layoutDesign, List<System.Data.DataTable> orderDetails, Entities.InvoiceTemplate.PageDimension pageDimension)
        {           

            //replacing non order item values with db one
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(layoutDesign);
            var htmlNodes = doc.DocumentNode.SelectNodes("//span");

            //replacing order item values and add rows corresponding to number of items

            PageDimension pageDimentionChangeAfterTable = new PageDimension()
            {
                Height = pageDimension.Height,
                width = pageDimension.width
            };
            var tableTop = 0.0f;

           
            var tableRow = doc.DocumentNode.SelectSingleNode("//table//tr");
            HtmlNode tableRowParentDiv = tableRow.ParentNode.ParentNode.ParentNode;
            var tableTopStr = (string)GetCssValue(tableRowParentDiv.GetAttributeValue("style", ""), "top").ToString().Replace("%", "");
            tableTop = Convert.ToSingle(string.IsNullOrEmpty(tableTopStr)?"0.0":tableTopStr);

            string tableMaxHeight = (string)GetCssValue(tableRowParentDiv.GetAttributeValue("style", ""), "height");
            var tableMaxHeightPx = string.IsNullOrEmpty(tableMaxHeight) ? 0 : (int)(Math.Ceiling(Convert.ToDouble(tableMaxHeight) * pageDimension.Height / 100));


            tableMaxHeightPx = (int)Math.Ceiling(pageDimension.Height);
          //  pageDimentionChangeAfterTable.Height = pageDimentionChangeAfterTable.Height * 2;
            tableRowParentDiv.ChildNodes.Where(x => x.Name.ToLower().Equals("table")).FirstOrDefault().SetAttributeValue("style", "height:auto;");
              tableRowParentDiv.SetAttributeValue("style", tableRowParentDiv.GetAttributeValue("style", "") + ";overflow: visible;height:auto;page-break-inside: avoid;");
            //tableRowParentDiv.SetAttributeValue("style", "margin-top:" + ((int)Math.Ceiling(tableTop * pageDimension.Height / 100) + "px"));

              HtmlDocument newDocument = new HtmlDocument();
              newDocument.LoadHtml(@"<html><head>
 <STYLE>
        @page {
            margin: 0pt;
            margin-right: 0pt !important;
            padding: 0pt;
        }
        
        th {
            display: inline-block;
            margin: 2px;
        }
        
        html,
        body {
            width: 100%;
            height: 100%;
            margin: 2px;
            padding: 0px;
            word-wrap: break-word;
        }
        
        body {
            overflow: hidden;
        }
        
        div,
        span,
        image,
        th {
            transition: width 1s ease-in-out;
        }
        
        div,
        image,
        th {
            display: inline-block;
            margin: 2px;
        }
        span{
        display: block;
            margin: 1px;
        }
        .ui-widget-content {
            background-color: gray;
        }
        
        img {
            display: inline-block;
        }
        
        .animating {
            border: 1px dotted blue;
        }
    </STYLE>
</head><body></body></html>");


            if (tableRow != null)
            {
                var coveredHeight = 50.0;

                var orderDetailsTable = orderDetails.Where(x => x.TableName.Equals("OrderItem")).FirstOrDefault();

                for (int i = 0; i < pageDimension.Height*1.5; i += 15)
                {
                    doc.DocumentNode.AppendChild(HtmlNode.CreateNode("<div style='display:block;'><div>"));
                }
                bool addRowInNewPageTable = false;
                foreach (DataRow row in orderDetailsTable.Rows)
                {
                    var thNode = tableRow.SelectNodes("//th").FirstOrDefault();
                    string thHeightPercentage = (string)GetCssValue(thNode.GetAttributeValue("style", ""), "height").ToString().Replace("px", "");
                    var thHeight = string.IsNullOrEmpty(thHeightPercentage) ? 18 : (int)(Math.Ceiling(Convert.ToDouble(thHeightPercentage)));
                    coveredHeight = coveredHeight + thHeight+4;
                    var rowStyle = tableRow.GetAttributeValue("style", true);
                    
                    HtmlNode newTableRow = HtmlNode.CreateNode("<tr style='" + rowStyle + "'></tr>");

                    var tableBeginAt=pageDimension.Height * tableTop / 100;
                    if (coveredHeight >= (pageDimension.Height - (tableBeginAt)))
                    {
                        var newPageTable=newDocument.DocumentNode.SelectSingleNode("//body").AppendChild(HtmlNode.CreateNode("<div></div>"));
                        newPageTable.AppendChild(HtmlNode.CreateNode("<table><tbody>"+tableRow.OuterHtml+"</tbody></table>"));
                        newPageTable.SetAttributeValue("style", tableRowParentDiv.GetAttributeValue("style", "") + ";top:1px;overflow:visible;");
                        //newTableRow.SetAttributeValue("style", newTableRow.GetAttributeValue("style", "") + ";background-color:blue;");
                        addRowInNewPageTable = true;
                        coveredHeight = 0.0;
                    }

                        foreach (HtmlNode node in tableRow.SelectNodes("//th"))
                        {
                            var nodeStyle = ReplacePercentWithPx(node.GetAttributeValue("style", ""), pageDimension, pageDimentionChangeAfterTable, tableTop);
                            node.SetAttributeValue("style", nodeStyle);
                            var mappedWithField = node.GetAttributeValue("field", node.InnerText.Trim());
                            HtmlNode newTd;
                            if (row[mappedWithField] != null)
                            {
                                // var style = mappedWithField.ToLower().Contains("title") || node.InnerText.Trim().ToLower().Contains("sku") ? "text-align:left;background-color: inherit;" : "text-align: center;background-color: inherit;";
                                //style += "font-size:" + nodeStyle.Split(';').Where(x => x.ToLower().Trim().Contains("font-size")).FirstOrDefault().Split(':')[1].ToString() + ";";
                                var dbValue = row[mappedWithField].ToString().Trim();
                                var dataType = node.GetAttributeValue("dataType", "Text");
                                var decimalValue = node.GetAttributeValue("decimalPlaces", "none");
                                dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                                if (dataType.ToLower().Equals("decimal") && !string.IsNullOrEmpty(decimalValue) && !decimalValue.Equals("none"))
                                {
                                    dbValue = Math.Round(Convert.ToDouble(dbValue), Convert.ToInt32(decimalValue)).ToString("n" + decimalValue);
                                    dbValue = dbValue.Replace(",", "");
                                }
                                if (mappedWithField.ToLower().Contains("title") || node.InnerText.Trim().ToLower().Contains("sku"))
                                {
                                    newTd = HtmlNode.CreateNode("<td style='" + node.GetAttributeValue("style", "") + ";font-weight:normal;" + "'>" + dbValue + "</td>");
                                }
                                else
                                {
                                    var tdContent = node.InnerHtml.Replace(node.InnerText.Replace(Environment.NewLine, string.Empty), dbValue);

                                    newTd = HtmlNode.CreateNode("<td style='" + node.GetAttributeValue("style", "") + ";font-weight:normal;" + "'>" + tdContent + "</td>");
                                }

                            }
                            else
                            {
                                newTd = HtmlNode.CreateNode("<td> </td>");
                            }
                            newTableRow.AppendChild(newTd);
                        }
                        if (addRowInNewPageTable)
                        {
                            newDocument.DocumentNode.SelectSingleNode("//table//tbody").AppendChild(newTableRow);
                        }
                        else
                        {
                            doc.DocumentNode.SelectSingleNode("//table//tbody").AppendChild(newTableRow);
                        }
                   
                }
            }

            //  pageDimension.Height = pageDimentionChangeAfterTable.Height;

            orderDetails.Where(x => x.TableName != "OrderItem").ToList().ForEach(x =>
            {
                foreach (DataColumn column in x.Columns)
                {
                    if (layoutDesign.Contains(column.ColumnName) && htmlNodes != null && htmlNodes.Count > 0)
                    {
                        var nodes = htmlNodes.Where(h => h.OuterHtml.Contains("_" + column.ColumnName)).ToList();
                        if (nodes.Count > 0)
                        {
                            nodes.ForEach(node =>
                            {
                                if (node != null)
                                {
                                    var dataType = node.GetAttributeValue("dataType", "Text");
                                    var dbValue = x.Rows[0][column].ToString().Trim();
                                    dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                                    var decimalValue = node.GetAttributeValue("decimalPlaces", "none");
                                    dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                                    if (dataType.ToLower().Equals("decimal") && !string.IsNullOrEmpty(decimalValue) && !decimalValue.Equals("none"))
                                    {
                                        dbValue = Math.Round(Convert.ToDouble(dbValue), Convert.ToInt32(decimalValue)).ToString("n" + decimalValue);
                                        dbValue = dbValue.Replace(",", "");
                                    }
                                    if (string.IsNullOrEmpty(dbValue))
                                    {
                                        var emptyNode = HtmlNode.CreateNode("");
                                        node.ParentNode.ReplaceChild(emptyNode, node);
                                    }
                                    else
                                    {
                                        var newOuterHtml = node.OuterHtml.Replace("_" + column, string.IsNullOrEmpty(dbValue) ? "&nbsp;&nbsp;" : dbValue);
                                        var newNode = HtmlNode.CreateNode(newOuterHtml);
                                        node.ParentNode.ReplaceChild(newNode, node);
                                    }
                                }
                            });
                        }

                    }

                }
            });


            //todo : replacing percentage to pixel if required

            //HtmlDocument newDocument = new HtmlDocument();
            //newDocument.LoadHtml("<html><body></body></html>");

            var bodyElements = doc.DocumentNode.SelectNodes("//body").FirstOrDefault().ChildNodes;

            for (int elementCount = 0; elementCount < bodyElements.Count; elementCount++)
            {
                var element = bodyElements[elementCount];
                if (element.OuterHtml.Contains("%"))
                {
                    var style = element.Attributes.Where(x => x.Name.ToLower().Equals("style")).FirstOrDefault();
                    if (style != null)
                    {
                        var newStyle = ReplacePercentWithPx(style.Value.ToString(), pageDimension, pageDimentionChangeAfterTable, tableTop);

                        var newNode = HtmlNode.CreateNode(element.OuterHtml.Replace(style.Value.ToString(), newStyle));

                        var ElementTop = (string)GetCssValue(style.Value.ToString(), "top");
                        if (!string.IsNullOrEmpty(ElementTop))
                        {
                            try
                            {


                                if (Convert.ToSingle((ElementTop).Replace("px","")) > tableTop)
                                {
                                    newDocument.DocumentNode.SelectNodes("//body").FirstOrDefault().AppendChild(newNode);
                                    element.ParentNode.ReplaceChild(HtmlNode.CreateNode("<div style='display:none;'></div>"), element);
                                }
                                else
                                {
                                    element.ParentNode.ReplaceChild(newNode, element);
                                }
                            }
                            catch (Exception ex)
                            {

                            }

                        }
                        else
                        {
                            element.ParentNode.ReplaceChild(newNode, element);
                        }                        
                    }
                }
            }


            var newPageBodyElements = newDocument.DocumentNode.SelectNodes("//body").FirstOrDefault().ChildNodes;

            for (int elementCount = 0; elementCount < newPageBodyElements.Count; elementCount++)
            {
                var element = newPageBodyElements[elementCount];
                if (element.OuterHtml.Contains("%"))
                {
                    var style = element.Attributes.Where(x => x.Name.ToLower().Equals("style")).FirstOrDefault();
                    if (style != null)
                    {
                        var newStyle = ReplacePercentWithPx(style.Value.ToString(), pageDimension,pageDimension,tableTop);
                        var newNode = HtmlNode.CreateNode(element.OuterHtml.Replace(style.Value.ToString(), newStyle));
                        element.ParentNode.ReplaceChild(newNode, element);
                    }
                }
            }

            HtmlNode bodyElement = doc.DocumentNode.SelectSingleNode("//body");
            bodyElement.SetAttributeValue("style", "font-size:9px;page-break-after:always");
            newDocument.DocumentNode.SelectSingleNode("//body").SetAttributeValue("style", "page-break-before:always");
            var anotherPageHtml = newDocument.DocumentNode.OuterHtml.ToString();

            return doc.DocumentNode.OuterHtml.ToString()+anotherPageHtml;
        }

        private static object GetCssValue(string style, string attribute)
        {
            try
            {


                if (!string.IsNullOrEmpty(style) && style.ToLower().Contains(attribute))
                {
                    var topList = style.Split(';').Where(x => x.ToLower().Contains(attribute)).ToList();
                    if (topList.Count > 0)
                    {
                        topList = topList.Where(c => c.ToLower().Split(':')[0].Trim().Equals(attribute)).ToList();
                        if (topList.Count > 0)
                        {
                            return topList.FirstOrDefault().Split(':')[1].Split('%')[0];
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        private static string ReplacePercentWithPx(string style, PageDimension pageDimension, PageDimension pageDimensionAfterTable, float tableTop)
        {
            var pageDimenstionToCompareWith = pageDimension;

            var styleElements = style.Split(';');
            var stylesToModify = styleElements.Where(x => x.Contains("%")).ToList();
            var attributesToIgnore = new List<string> { "line-height", "font-size" };


            var top = stylesToModify.Where(x => x.ToString().ToLower().Trim().Contains("top")).FirstOrDefault();

            if (!string.IsNullOrEmpty(top))
            {
                if (!(Convert.ToSingle(top.Split(':')[1].Split('%')[0]) <= tableTop))
                {
                    pageDimenstionToCompareWith = pageDimensionAfterTable;
                }
            }


            foreach (var modifyStyle in stylesToModify)
            {
                var styleProperties = modifyStyle.Split(':');
                var key = Convert.ToDouble(styleProperties[1].Split('%')[0]);
                double pxConversion = 0.0;
                var attr = styleProperties[0].ToString();

                if (!attributesToIgnore.Contains(attr.ToLower().Trim()))
                {
                    if (attr.ToLower().Contains("height") || attr.ToLower().Contains("top"))
                    {
                        pxConversion = pageDimenstionToCompareWith.Height * (key / 100);
                    }

                    else
                    {
                        pxConversion = pageDimension.width * (key / 100);
                    }
                    var value = (int)Math.Round(pxConversion) + "px";
                    style = style.Replace(key + "%", value);
                }
            }
            return style;
        }
    }
}
