﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Drawing;
using iText;
using iText.Html2pdf;
using iText.Kernel.Pdf;
using System.Reflection;
using ODM.Entities.InvoiceTemplate;
using iText.StyledXmlParser.Css.Media;

namespace ODM.Core.InvoiceTemplate
{
    public class PrintInvoice
    {
        public string Print(string orderNumber, string invoicePage, string pageSize, PageOrientation pageOrientation)
        {
            orderNumber += "-invoice";
            string pdffilename = System.IO.Path.GetTempPath() + orderNumber + ".pdf";
            PdfWriter writer = new PdfWriter(File.OpenWrite(pdffilename));
            PdfDocument doc = new PdfDocument(writer);
            ConverterProperties properties = new ConverterProperties();


            var rect = PageSettings.GetPageSize(pageSize);
            rect.ApplyMargins(0.0f, 0.0f, 0.0f, 0.0f, false);


            MediaDeviceDescription mediaDeviceDescription = new MediaDeviceDescription(MediaType.PRINT);
            if (pageOrientation == PageOrientation.Portrait)
            {
                var pageSizeRect = new iText.Kernel.Geom.Rectangle(rect.GetWidth(), rect.GetHeight());
                doc.SetDefaultPageSize(new iText.Kernel.Geom.PageSize(pageSizeRect));
                mediaDeviceDescription.SetWidth(rect.GetWidth());
            }
            else
            {
                var pageSizeRect = new iText.Kernel.Geom.Rectangle(rect.GetHeight(), rect.GetWidth());
                doc.SetDefaultPageSize(new iText.Kernel.Geom.PageSize(pageSizeRect));
                mediaDeviceDescription.SetWidth(rect.GetHeight());
            }


            properties.SetMediaDeviceDescription(mediaDeviceDescription);
            try
            {
                HtmlConverter.ConvertToPdf(invoicePage, doc, properties);
            }
            catch
            {
                pdffilename = string.Empty;
            }

            return pdffilename;
        }

        public void Print(string orderId, string invoicePage, string pageSize, PageOrientation pageOrientation, string printer)
        {
            orderId += "-invoice";
            string pdffilename = System.IO.Path.GetTempPath() + orderId + ".pdf";
            string imageFilename = System.IO.Path.GetTempPath();
            PdfWriter writer = new PdfWriter(File.OpenWrite(pdffilename));
            PdfDocument doc = new PdfDocument(writer);
            ConverterProperties properties = new ConverterProperties();


            var rect = PageSettings.GetPageSize(pageSize);
            rect.ApplyMargins(0.0f, 0.0f, 0.0f, 0.0f, false);


            MediaDeviceDescription mediaDeviceDescription = new MediaDeviceDescription(MediaType.PRINT);
            if (pageOrientation == PageOrientation.Portrait)
            {
                var pageSizeRect = new iText.Kernel.Geom.Rectangle(rect.GetWidth(), rect.GetHeight());
                doc.SetDefaultPageSize(new iText.Kernel.Geom.PageSize(pageSizeRect));
                mediaDeviceDescription.SetWidth(rect.GetWidth());
            }
            else
            {
                var pageSizeRect = new iText.Kernel.Geom.Rectangle(rect.GetHeight(), rect.GetWidth());
                doc.SetDefaultPageSize(new iText.Kernel.Geom.PageSize(pageSizeRect));
                mediaDeviceDescription.SetWidth(rect.GetHeight());
            }


            properties.SetMediaDeviceDescription(mediaDeviceDescription);
            HtmlConverter.ConvertToPdf(invoicePage, doc, properties);
            Activator.CreateInstance<ODM.Data.Printing.PrintInvoiceDynamically>().PrintInvoice(pdffilename, imageFilename, pageOrientation, printer);
        }
    }
}
