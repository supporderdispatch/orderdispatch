﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.DbContext;
using iText;
using ODM.Entities.InvoiceTemplate;
using System.Text.RegularExpressions;
using System.IO;
using System.Windows.Forms;
using HtmlAgilityPack;
using ODM.Core.Helpers;
using ODM.Data.Util;
using ODM.Entities.InvoiceTemplate.InvoiceTemplateDataContent;


namespace ODM.Core.InvoiceTemplate
{
    public class InvoiceTemplates
    {
        private InvoiceTemplateContext Context;

        public InvoiceTemplates()
        {
            Context = new InvoiceTemplateContext();
        }

        public DataTable GetAllInvoiceTemplates()
        {
            return Context.GetAllInvoiceTemplates();
        }


        public PageDimension GetPageSizeDimensions(string pageSize)
        {
            return ODM.Core.InvoiceTemplate.PageSettings.GetPageDimentions(pageSize);
        }
        public System.Guid AddInvoiceTemplate(Entities.InvoiceTemplate.InvoiceTemplate invoiceTemplate)
        {

            return Context.AddInvoiceTemplate(invoiceTemplate);
        }

        public ODM.Entities.InvoiceTemplate.InvoiceTemplate GetInvoiceTemplate(Guid templateId)
        {
            var result = Context.GetInvoiceTemplates(templateId).AsEnumerable().FirstOrDefault();
            return new ODM.Entities.InvoiceTemplate.InvoiceTemplate()
            {
                TemplateName = result["TemplateName"].ToString(),
                LayoutDesign = result["LayoutDesign"].ToString(),
                PageOrientation = Convert.ToInt32(result["PageOrientation"].ToString()),
                PageSize = result["PageSize"].ToString(),
                PrintContidtionID = Guid.Parse(result["PrintConditionId"].ToString()),
                IsAutoPrint = Convert.ToBoolean(result["IsAutoPrint"].ToString()),
                IsEnabled = Convert.ToBoolean(result["IsEnabled"].ToString()),
                IsConditional = Convert.ToBoolean(result["IsConditional"].ToString()),
                IsPdfTemplate = Convert.ToBoolean(result["IsPdfTemplate"].ToString()),
                SelectedPrinter = GetSelectedPrinterFor(templateId)
            };
        }

        private string GetSelectedPrinterFor(Guid templateId)
        {
            var printer = GlobalVariablesAndMethods.SelectedPrinterNameForInvoice;
            var printers = (DataTable)Activator.CreateInstance<ODM.Core.InvoiceTemplate.PageSettings>().GetPrinterFor(templateId, new GlobalVariablesAndMethods().GetSystemIpv4Address);
            if (printers.Rows.Count > 0)
            {
                printer = printers.Rows[0]["PrinterName"].ToString();
            }

            return printer;
        }

        public void DeleteInvoiceTemplate(string templateId)
        {
            Context.DeleteInvoiceTemplate(templateId);
        }

        public void UpdateInvoiceTemplate(Entities.InvoiceTemplate.InvoiceTemplate invoiceTemplate)
        {
            var result = true;
            try
            {
                Context.UpdateInvoiceTemplate(invoiceTemplate);
            }
            catch
            {
                result = false;
            }
            return;
        }

        public void CopyTemplate(string templateId)
        {
            Context.CopyInvoiceTemplate(templateId);
        }
        public string PrintInvoiceForOrder(Guid orderId, string orderNumber)
        {
            var invoicePath = string.Empty;
            var invoiceTemplate = new PrintConditions().GetCriteriaFulfillingTemplateFor(orderId);
            if (!String.IsNullOrEmpty(invoiceTemplate.LayoutDesign))
            {
                var parsedInvoice = GetInvoicePopulatedWithDBFields(invoiceTemplate, orderId);
                parsedInvoice = parsedInvoice.Replace("ui-", "class-removed");
                parsedInvoice = parsedInvoice.Replace(":gray", ":white");
                parsedInvoice = parsedInvoice.Replace("</style>", ".class-removedresizable-handle{ display:none;} .class-removedwidget-content { BACKGROUND-COLOR: inherit;} </style>");
                invoicePath = new PrintInvoice().Print(orderNumber, parsedInvoice, invoiceTemplate.PageSize, (PageOrientation)Enum.Parse(typeof(PageOrientation), invoiceTemplate.PageOrientation.ToString()));
            }
            return invoicePath;
        }
        public void PrintInvoiceForOrder(Guid orderId, bool autoPrint)
        {
            var invoiceTemplate = new PrintConditions().GetCriteriaFulfillingTemplateFor(orderId, autoPrint);
            if (!String.IsNullOrEmpty(invoiceTemplate.LayoutDesign))
            {
                var parsedInvoice = GetInvoicePopulatedWithDBFields(invoiceTemplate, orderId);
                parsedInvoice = parsedInvoice.Replace("ui-", "class-removed");
                parsedInvoice = parsedInvoice.Replace(":gray", ":white");
                parsedInvoice = parsedInvoice.Replace("</style>", ".class-removedresizable-handle{ display:none;} .class-removedwidget-content { BACKGROUND-COLOR: inherit;} </style>");
                new PrintInvoice().Print(Guid.NewGuid().ToString(), parsedInvoice, invoiceTemplate.PageSize, (ODM.Entities.InvoiceTemplate.PageOrientation)Enum.Parse(typeof(ODM.Entities.InvoiceTemplate.PageOrientation), invoiceTemplate.PageOrientation.ToString()), GetSelectedPrinterFor(invoiceTemplate.TemplateId));
            }
        }
        private string GetInvoicePopulatedWithDBFields(ODM.Entities.InvoiceTemplate.InvoiceTemplate invoiceTemplate, Guid orderId)
        {
            var dataset = ((DataSet)new ODM.Data.DbContext.InvoiceTemplateContext().GetDataForInvoice(orderId));
            var orderDetails = new List<DataTable>();
            var orderOfTables = new Dictionary<int, string>();
            orderOfTables.Add(0, "Order");
            orderOfTables.Add(1, "OrderItem");
            orderOfTables.Add(2, "CustomerAddress");
            orderOfTables.Add(3, "SenderAddress");

            for (int i = 0; i < dataset.Tables.Count; i++)
            {
                var table = dataset.Tables[i];
                table.TableName = orderOfTables[i];
                orderDetails.Add(table);
            }

            //replacing non order item values with db one
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(invoiceTemplate.LayoutDesign);
            var layoutDesign = invoiceTemplate.LayoutDesign;



            var selectedPageOrientation = (PageOrientation)Enum.Parse(typeof(PageOrientation), invoiceTemplate.PageOrientation.ToString());
            var pageSize = Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>()
                .GetPageSizeDimensions(invoiceTemplate.PageSize);
            var pageDimension = new PageDimension();
            if (selectedPageOrientation == PageOrientation.Portrait)
            {
                pageDimension.width = pageSize.width;
                pageDimension.Height = pageSize.Height;
            }
            if (selectedPageOrientation == PageOrientation.Landscape)
            {
                pageDimension.width = pageSize.Height;
                pageDimension.Height = pageSize.width;
            }
            pageDimension = GetDimentionsFromOriginalratios(pageDimension, selectedPageOrientation);


            //replacing order item values and add rows corresponding to number of items

            PageDimension pageDimentionChangeAfterTable = new PageDimension()
            {
                Height = pageDimension.Height,
                width = pageDimension.width
            };
            var tableTop = 0.0f;

            var tableRow = doc.DocumentNode.SelectSingleNode("//table//tr");
            HtmlNode tableRowParentDiv = tableRow.ParentNode.ParentNode.ParentNode;
            tableTop = Convert.ToSingle((string)GetCssValue(tableRowParentDiv.GetAttributeValue("style", ""), "top").ToString().Replace("%", ""));

            string tableMaxHeight = (string)GetCssValue(tableRowParentDiv.GetAttributeValue("style", ""), "height");
            var tableMaxHeightPx = string.IsNullOrEmpty(tableMaxHeight) ? 0 : (int)(Math.Ceiling(Convert.ToDouble(tableMaxHeight) * pageDimension.Height / 100));

            if (tableRow != null)
            {

                var orderDetailsTable = orderDetails.Where(x => x.TableName.Equals("OrderItem")).FirstOrDefault();
                var thNode = tableRow.SelectNodes("//th").FirstOrDefault();
                string thHeightPercentage = (string)GetCssValue(thNode.GetAttributeValue("style", ""), "height").ToString().Replace("px", "");
                var thHeight = string.IsNullOrEmpty(thHeightPercentage) ? 18 : (int)(Math.Ceiling(Convert.ToDouble(thHeightPercentage)));
                var coveredHeight = orderDetailsTable.Rows.Count * (thHeight + 4);

                if (coveredHeight > tableMaxHeightPx - (thHeight + 8))
                {
                    //MessageBox.Show("Content exceeded \n " + tableExceedSetting.ToString());
                    var dataExceedSetting = Activator.CreateInstance<ODM.Core.InvoiceTemplate.PrintWithDataExceedOptions.DataExceed>().GetDataSettings(invoiceTemplate.TemplateId);
                    if (dataExceedSetting.TryAdjustBlock)
                    {
                        var adjustedBlock = TryAdjustBlock(invoiceTemplate.LayoutDesign, pageDimension, coveredHeight);
                        if (adjustedBlock.Adjusted)
                        {
                            doc.LoadHtml(adjustedBlock.layoutDesign);
                            dataExceedSetting.DataExceedActionSelected = TableContentExceedOptions.OverFlowHidden;
                        }
                    }

                    switch (dataExceedSetting.DataExceedActionSelected)
                    {
                        case TableContentExceedOptions.OverFlowHidden:
                            {
                                tableRowParentDiv.SetAttributeValue("style", tableRowParentDiv.GetAttributeValue("style", "") + ";overflow:hidden;");
                                break;
                            }
                        case TableContentExceedOptions.CreateNewPageAdjustingOtherFields:
                            {
                                return ODM.Core.InvoiceTemplate.PrintWithDataExceedOptions.CreateNewPageAdjustingOtherFields.GetParsedInvoice(layoutDesign, orderDetails, pageDimension);
                            }
                        case TableContentExceedOptions.CreateNewPageRetainingOtherFields:
                            {
                                return ODM.Core.InvoiceTemplate.PrintWithDataExceedOptions.CreateNewPageRetainingOtherFields.GetParsedInvoice(layoutDesign, orderDetails, pageDimension);
                            }
                        case TableContentExceedOptions.ChooseAnotherTemplate:
                            {
                                return ODM.Core.InvoiceTemplate.PrintWithDataExceedOptions.PrintFromAnotherInvoice.GetParsedInvoice(dataExceedSetting.TemplateSelectedWhenDataExceed, orderDetails, pageDimension);
                            }
                    }
                }


                foreach (DataRow row in orderDetailsTable.Rows)
                {

                    var rowStyle = tableRow.GetAttributeValue("style", true);
                    HtmlNode newTableRow = HtmlNode.CreateNode("<tr style='" + rowStyle + "'></tr>");

                    foreach (HtmlNode node in tableRow.SelectNodes("//th"))
                    {
                        var nodeStyle = ReplacePercentWithPx(node.GetAttributeValue("style", ""), pageDimension);
                        node.SetAttributeValue("style", nodeStyle);
                        var mappedWithField = node.GetAttributeValue("field", node.InnerText.Trim());
                        HtmlNode newTd;
                        if (row[mappedWithField] != null)
                        {
                            // var style = mappedWithField.ToLower().Contains("title") || node.InnerText.Trim().ToLower().Contains("sku") ? "text-align:left;background-color: inherit;" : "text-align: center;background-color: inherit;";
                            //style += "font-size:" + nodeStyle.Split(';').Where(x => x.ToLower().Trim().Contains("font-size")).FirstOrDefault().Split(':')[1].ToString() + ";";
                            var dbValue = row[mappedWithField].ToString().Trim();
                            var dataType = node.GetAttributeValue("dataType", "Text");
                            var decimalValue = node.GetAttributeValue("decimalPlaces", "none");
                            dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                            if (dataType.ToLower().Equals("decimal") && !string.IsNullOrEmpty(decimalValue) && !decimalValue.Equals("none"))
                            {
                                dbValue = Math.Round(Convert.ToDouble(dbValue), Convert.ToInt32(decimalValue)).ToString("n" + decimalValue);
                                dbValue = dbValue.Replace(",", "");
                            }
                            if (mappedWithField.ToLower().Contains("title") || node.InnerText.Trim().ToLower().Contains("sku"))
                            {
                                newTd = HtmlNode.CreateNode("<td style='" + node.GetAttributeValue("style", "") + ";font-weight:normal;" + "'>" + dbValue + "</td>");
                            }
                            else
                            {
                                var tdContent = node.InnerHtml.Replace(node.InnerText.Replace(Environment.NewLine, string.Empty), dbValue);

                                newTd = HtmlNode.CreateNode("<td style='" + node.GetAttributeValue("style", "") + ";font-weight:normal;" + "'>" + tdContent + "</td>");
                            }

                        }
                        else
                        {
                            newTd = HtmlNode.CreateNode("<td> </td>");
                        }
                        newTableRow.AppendChild(newTd);
                    }
                    doc.DocumentNode.SelectSingleNode("//table//tbody").AppendChild(newTableRow);
                }
            }

            //  pageDimension.Height = pageDimentionChangeAfterTable.Height;
            var htmlNodes = doc.DocumentNode.SelectNodes("//span");
            orderDetails.Where(x => x.TableName != "OrderItem").ToList().ForEach(x =>
            {
                foreach (DataColumn column in x.Columns)
                {
                    if (layoutDesign.Contains(column.ColumnName) && htmlNodes != null && htmlNodes.Count > 0)
                    {
                        var nodes = htmlNodes.Where(h => h.OuterHtml.Contains("_" + column.ColumnName)).ToList();
                        if (nodes.Count > 0)
                        {
                            nodes.ForEach(node =>
                            {
                                if (node != null)
                                {
                                    var dataType = node.GetAttributeValue("dataType", "Text");
                                    var dbValue = x.Rows[0][column].ToString().Trim();
                                    dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                                    var decimalValue = node.GetAttributeValue("decimalPlaces", "none");
                                    dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                                    if (dataType.ToLower().Equals("decimal") && !string.IsNullOrEmpty(decimalValue) && !decimalValue.Equals("none"))
                                    {
                                        dbValue = Math.Round(Convert.ToDouble(dbValue), Convert.ToInt32(decimalValue)).ToString("n" + decimalValue);
                                        dbValue = dbValue.Replace(",", "");
                                    }
                                    if (string.IsNullOrEmpty(dbValue))
                                    {
                                        var emptyNode = HtmlNode.CreateNode("");
                                        node.ParentNode.ReplaceChild(emptyNode, node);
                                    }
                                    else
                                    {
                                        var newOuterHtml = node.OuterHtml.Replace("_" + column, string.IsNullOrEmpty(dbValue) ? "&nbsp;&nbsp;" : dbValue);
                                        var newNode = HtmlNode.CreateNode(newOuterHtml);
                                        node.ParentNode.ReplaceChild(newNode, node);
                                    }
                                }
                            });
                        }

                    }

                }
            });


            //todo : replacing percentage to pixel if required

            var bodyElements = doc.DocumentNode.SelectNodes("//body").FirstOrDefault().ChildNodes;

            for (int elementCount = 0; elementCount < bodyElements.Count; elementCount++)
            {
                var element = bodyElements[elementCount];
                if (element.OuterHtml.Contains("%"))
                {
                    var style = element.Attributes.Where(x => x.Name.ToLower().Equals("style")).FirstOrDefault();
                    if (style != null)
                    {
                        var newStyle = ReplacePercentWithPx(style.Value.ToString(), pageDimension);
                        var newNode = HtmlNode.CreateNode(element.OuterHtml.Replace(style.Value.ToString(), newStyle));
                        element.ParentNode.ReplaceChild(newNode, element);
                    }
                }
            }

            HtmlNode bodyElement = doc.DocumentNode.SelectSingleNode("//body");
            bodyElement.SetAttributeValue("style", "font-size:9px;overflow:hidden;");
            return doc.DocumentNode.OuterHtml.ToString();
        }

        private TableAdjustResult TryAdjustBlock(string layoutDesign, PageDimension pageDimension, int tableRequiredPx)
        {
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(layoutDesign);

            var blocks = new List<HtmlNode>();
            foreach (HtmlNode node in document.DocumentNode.SelectNodes("//body//div"))
            {
                if (!string.IsNullOrEmpty(node.GetAttributeValue("style", "")) && node.GetAttributeValue("style", "").ToLower().Contains("top"))
                {
                    blocks.Add(node);
                }
            }

            var styleBlocks = new List<HtmlElementWithTopAndHeight>();
            foreach (var block in blocks)
            {
                var blockStyle = block.GetAttributeValue("style", "");
                if (!string.IsNullOrEmpty(blockStyle) && blockStyle.ToLower().Contains("height") && blockStyle.ToLower().Contains("top"))
                {
                    var top = GetCssValue(blockStyle, "top").ToString().Replace("%", "").Replace("px", "");
                    var height = GetCssValue(blockStyle, "height").ToString().Replace("%", "").Replace("px", "");
                    height = string.IsNullOrEmpty(height) ? "1" : height;
                    if (!string.IsNullOrEmpty(top))
                    {
                        styleBlocks.Add(new HtmlElementWithTopAndHeight()
                        {
                            CustomNode = block,
                            Top = Convert.ToDouble(top),
                            height = Convert.ToDouble(height)
                        });
                    }
                }

            }
            var tableDiv = styleBlocks.Where(x => x.CustomNode.ChildNodes.Any(c => c.Name.ToLower().Trim().Equals("table"))).FirstOrDefault();
            styleBlocks = styleBlocks.OrderBy(x => x.Top).ToList();

            string tableDivOriginalHtml = tableDiv.CustomNode.OuterHtml.ToString();

            var tableDivPreElement = new HtmlElementWithTopAndHeight()
            {
                Top = 0,
                height = 1,
                CustomNode = HtmlNode.CreateNode("<div></div>")
            };

            if (styleBlocks.Count > 1 && (styleBlocks.IndexOf(tableDiv) != styleBlocks.Count))
            {
                if (!(styleBlocks.IndexOf(tableDiv) == 0))
                {
                    tableDivPreElement = styleBlocks[styleBlocks.IndexOf(tableDiv) - 1];
                }
                var tableDivNextElement = styleBlocks[styleBlocks.IndexOf(tableDiv) + 1];
                if (tableDivPreElement != null)
                {
                    tableDiv.Top = tableDivPreElement.Top + tableDivPreElement.height + 1;
                }
                if ((tableDiv.Top + tableDiv.height) < tableDivNextElement.Top)
                {
                    tableDiv.height = tableDivNextElement.Top - tableDiv.Top;
                }
            }


            var tableNode = tableDiv.CustomNode;
            var tableNodeStyle = tableNode.GetAttributeValue("style", "");
            tableNodeStyle = UpdateStyle(tableNodeStyle, "TOP", tableDiv.Top);
            tableNodeStyle = UpdateStyle(tableNodeStyle, "HEIGHT", tableDiv.height);
            tableNode.SetAttributeValue("style", tableNodeStyle);

            var newTableSpace = (tableDiv.height * pageDimension.Height) / 100;
            if (newTableSpace > tableRequiredPx)
            {

                return new TableAdjustResult()
                {
                    Adjusted = true,
                    layoutDesign = document.DocumentNode.OuterHtml.Replace(tableDivOriginalHtml, tableNode.OuterHtml)
                };
            }
            return new TableAdjustResult()
            {
                Adjusted = false,
                layoutDesign = string.Empty
            };

        }

        private string UpdateStyle(string tableNodeStyle, string attribute, double value)
        {
            var style = string.Empty;
            foreach (var styleElement in tableNodeStyle.Split(';'))
            {
                if (styleElement.ToLower().Trim().Contains(attribute.ToLower()))
                {
                    var attr = styleElement.Split(':')[0];
                    if (attr.ToLower().Trim().Equals(attribute.ToLower()))
                    {
                        style = style + attr + ":" + value + "%;";
                    }
                    else
                    {
                        style = style + styleElement + ";";
                    }
                }
                else
                {
                    style = style + styleElement + ";";
                }
            }
            return style;
        }

        private object GetCssValue(string style, string attribute)
        {
            try
            {


                if (!string.IsNullOrEmpty(style) && style.ToLower().Contains(attribute))
                {
                    var topList = style.Split(';').Where(x => x.ToLower().Contains(attribute)).ToList();
                    if (topList.Count > 0)
                    {
                        topList = topList.Where(c => c.ToLower().Split(':')[0].Trim().Equals(attribute)).ToList();
                        if (topList.Count > 0)
                        {
                            return topList.FirstOrDefault().Split(':')[1].Split('%')[0];
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        private PageDimension GetDimentionsFromOriginalratios(PageDimension pageDimension, PageOrientation orientation)
        {
            float scaling;
            if (orientation == PageOrientation.Landscape)
            {
                scaling = pageDimension.Height / pageDimension.width;
            }
            else
            {
                scaling = pageDimension.width / pageDimension.Height;
            }
            scaling = 1 + (scaling / 2.225f);
            pageDimension.width *= scaling;
            pageDimension.Height *= scaling;
            return pageDimension;
        }
        public class TableAdjustResult
        {
            public bool Adjusted { get; set; }
            public string layoutDesign { get; set; }
        }

        private string ReplacePercentWithPx(string style, PageDimension pageDimension)
        {
            var styleElements = style.Split(';');
            var stylesToModify = styleElements.Where(x => x.Contains("%")).ToList();
            var attributesToIgnore = new List<string> { "line-height", "font-size" };
            foreach (var modifyStyle in stylesToModify)
            {
                var styleProperties = modifyStyle.Split(':');
                var key = Convert.ToDouble(styleProperties[1].Split('%')[0]);
                double pxConversion = 0.0;
                var attr = styleProperties[0].ToString();

                if (!attributesToIgnore.Contains(attr.ToLower().Trim()))
                {
                    if (attr.ToLower().Contains("height") || attr.ToLower().Contains("top"))
                    {
                        pxConversion = pageDimension.Height * (key / 100);
                    }
                    else
                    {
                        pxConversion = pageDimension.width * (key / 100);
                    }
                    var value = (int)Math.Round(pxConversion) + "px";
                    style = style.Replace(key + "%", value);
                }
            }
            return style;
        }

        public void UpdateInvoiceTemplates(List<Entities.InvoiceTemplate.InvoiceTemplate> updatedInvoiceSettings)
        {
            foreach (var updatedInvoiceSetting in updatedInvoiceSettings)
            {
                Context.UpdateInvoiceTemplateSetting(updatedInvoiceSetting);
            }

        }
    }
    public class HtmlElementWithTopAndHeight
    {
        public double Top { get; set; }
        public double height { get; set; }
        public HtmlAgilityPack.HtmlNode CustomNode { get; set; }
    }
}
