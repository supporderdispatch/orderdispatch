﻿using ODM.Entities.InvoiceTemplate;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Core.Helpers
{
    public class Conversions
    {
        public static object ParseToCustomDataType(string valueToConvert, Type dataType)
        {
            if (dataType.Equals(typeof(Int32)))
            {
                return (int)Math.Ceiling(Convert.ToDouble(valueToConvert));
            }
            if (dataType.Equals(typeof(Boolean)))
            {
                return Convert.ToBoolean(valueToConvert);
            }
            if (dataType.Equals(typeof(Double)))
            {
                return Convert.ToDouble(valueToConvert);
            }
            else
            {
                return valueToConvert.ToString();
            }
        }

        public static Type GetPrintConditionCriteriaDataType(string typeName)
        {
            var type = (CustomDataTypes)Enum.Parse(typeof(CustomDataTypes), typeName);
            switch (type)
            {
                case CustomDataTypes.Number:
                    return typeof(Int32);

                case CustomDataTypes.Decimal:
                    return typeof(double);

                case CustomDataTypes.Boolean:
                    return typeof(Boolean);

                default:
                    return typeof(string);
            }
        }
        public static Image Base64StringToImage(string strBase64String)
        {
            byte[] bytImageBytes = Convert.FromBase64String(strBase64String);
            MemoryStream ms = new MemoryStream(bytImageBytes, 0, bytImageBytes.Length);
            ms.Write(bytImageBytes, 0, bytImageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        public static string GetLocalDateFromUtc(string utcDate)
        {
            return DateTime.SpecifyKind(DateTime.Parse(utcDate), DateTimeKind.Utc).ToLocalTime().ToString("yyyy-MM-dd hh:mm:ss tt", CultureInfo.InvariantCulture);
        }
    }
}
