﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.DbContext;
using ODM.Entities.Emails;
using System.Data;

namespace ODM.Core.Emails
{
    public class EmailTemplates
    {
        private EmailContext Context;
        public EmailTemplates() 
        {
            Context = new EmailContext();
        }
          
        public static EmailTemplates GetInstance()
        {
            return new EmailTemplates();
        }

        public void SaveEmailTemplate(EmailTemplate emailTemplate)
        {
            Context.SaveEmailTemplate(emailTemplate);
        }

        public DataTable GetAllEmailTemplates()
        {
            return Context.GetAllEmailTemplates();
        }

        public EmailTemplate GetEmailTemplate(Guid emailTemplateId)
        {
            return Context.GetEmailTemplate(emailTemplateId).AsEnumerable()
                .Select(x => new EmailTemplate()
                {
                    EmailTemplateId = x.Field<Guid>("EmailTemplateId"),
                    EmailTemplateName = x.Field<string>("EmailTemplateName"),
                    IsHtml = x.Field<bool>("IsHtml"),
                    IsEnabled = x.Field<bool>("IsEnabled"),
                    WithPdf = x.Field<bool>("WithPdf"),
                    WithImage = x.Field<bool>("WithImage"),
                    Cc = x.Field<string>("Cc"),
                    Bcc = x.Field<string>("Bcc"),
                    Subject = x.Field<string>("Subject"),
                    Body = x.Field<string>("Body"),
                    EmailTemplateConditionId = x.Field<Guid>("EmailTemplateConditionId"),
                    MailFrom = x.Field<Guid>("MailFrom")
                }).FirstOrDefault();

        }
        public void DeleteEmailTemplate(Guid emailTemplateId)
        {
            Context.DeleteEmailTemplate(emailTemplateId);
        }

        public void CopyTemplate(string emailTemplateId)
        {
            Context.CopyEmailTemplate(emailTemplateId);
        }
    }
}
