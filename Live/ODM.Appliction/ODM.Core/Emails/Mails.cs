﻿using ODM.Data.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Entities.Emails;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net.Mail;
using System.Drawing.Imaging;

namespace ODM.Core.Emails
{
    public class Mails
    {
        private EmailContext Context;
        public Mails() 
        {
            Context = new EmailContext();
        }

        public static Mails GetInstance()
        {
            return new Mails();
        }
        
        public void AddPendingMail(Email email)
        {
            Context.AddPendingMail(email);
        }

        public DataTable GetAllPendingMails()
        {
            return Context.GetAllPendingEmails();
        }
        public void SendMail(Email email)
        {
            var emailTemplate = ODM.Core.Emails.EmailTemplates.GetInstance().GetEmailTemplate(email.EmailTemplateId);
            var smtpServerDetails = GetEmailAccountFor(emailTemplate.MailFrom);
            var invoicePath = string.Empty;

            //begin sending email
            MailMessage mail = new MailMessage();
            SmtpClient smtpServer = new SmtpClient(smtpServerDetails.SMTP_Server.ToString(), Convert.ToInt32(smtpServerDetails.Port));
            mail.Sender = new MailAddress("customerservice@etwist.co.uk");
            mail.From = new MailAddress(smtpServerDetails.ReplyToEmail, smtpServerDetails.UserName);
            mail.To.Add(email.MailToAddress);
            mail.Subject = email.SentSubject;
            mail.Body = email.SentMailBody;
            
            //adding cc
            var cc = new List<string>();
            if (emailTemplate.Cc.Contains(","))
            {
                cc.AddRange(emailTemplate.Cc.Split(',').ToList().Where(x => !string.IsNullOrEmpty(x)).ToList());
            }
            else
            {
                if(!string.IsNullOrEmpty(emailTemplate.Cc))
                {
                    cc.Add(emailTemplate.Cc);
                }                
            }
            cc.ForEach(x => 
            {
                mail.CC.Add(x);
            });

            //adding bcc
            var bcc = new List<string>();
            if (emailTemplate.Bcc.Contains(","))
            {
                bcc.AddRange(emailTemplate.Bcc.Split(',').ToList().Where(x => !string.IsNullOrEmpty(x)).ToList());
            }
            else
            {
                if (!string.IsNullOrEmpty(emailTemplate.Bcc))
                {
                    bcc.Add(emailTemplate.Bcc);
                }
            }
            bcc.ForEach(x =>
            {
                mail.Bcc.Add(x);
            });

            mail.ReplyToList.Add(new MailAddress(smtpServerDetails.ReplyToEmail));
            if (emailTemplate.WithPdf)
            {
                invoicePath = Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().PrintInvoiceForOrder(email.OrderId, email.NumOrderId);
                if (!string.IsNullOrEmpty(invoicePath))
                {
                    Attachment attachment;
                    attachment = new Attachment(invoicePath);
                    mail.Attachments.Add(attachment);
                }
            }
            if (emailTemplate.WithImage)
            {
                var base64Image = string.Empty;
                var imageData = ODM.Data.Util.GlobalVariablesAndMethods.GetAddUpdateDeleteImageByOrderId("GET", email.OrderId);
                if (imageData.Rows.Count > 0)
                {
                    base64Image = imageData.Rows[0]["Image"].ToString(); ;
                }
                if (!string.IsNullOrEmpty(base64Image))
                {

                    Image image = ODM.Data.Util.GlobalVariablesAndMethods.Base64StringToImage(base64Image);
                    MemoryStream ms = new MemoryStream();
                    image.Save(ms, ImageFormat.Jpeg);
                    ms.Position = 0;
                    mail.Attachments.Add(new Attachment(ms, "OrderedPackage.jpg"));
                }
            }
            mail.IsBodyHtml = emailTemplate.IsHtml;
            smtpServer.Credentials = new System.Net.NetworkCredential(Convert.ToString(smtpServerDetails.UserName), Convert.ToString(smtpServerDetails.Password));
            smtpServer.EnableSsl = smtpServerDetails.IsSSL;
            smtpServer.Send(mail);
            mail.Dispose();
            if (!string.IsNullOrEmpty(invoicePath))
            {
                if (File.Exists(invoicePath))
                {
                    File.Delete(invoicePath);
                    invoicePath = string.Empty;
                }
            }
            Context.MarkMailAsSent(email.OrderId);
        }

        private SmtpAccount GetEmailAccountFor(Guid mailFromAccountId)
        {
            var data = ODM.Data.Util.GlobalVariablesAndMethods.GetEmailAccounts();
            return ODM.Data.Util.GlobalVariablesAndMethods.GetEmailAccounts().AsEnumerable()
                .Select(x => new SmtpAccount() 
                {
                    ID=x.Field<Guid>("ID"),
                    EmailAccount=x.Field<string>("EmailAccount"),
                    SMTP_Auth=x.Field<bool>("SMTP_Auth"),
                    FromName=x.Field<string>("FromName"),
                    IsEnabled=x.Field<bool>("IsEnabled"),
                    IsSSL=x.Field<bool>("IsSSL"),
                    Password=x.Field<string>("Password"),
                    Port=x.Field<string>("Port"),
                    ReplyToEmail=x.Field<string>("ReplyToEmail"),
                    SendCopyTo=x.Field<string>("SendCopyTo"),
                    SMTP_Server=x.Field<string>("SMTP_Server"),
                    UserName=x.Field<string>("UserName")
                }).ToList().Where(c => c.ID.Equals(mailFromAccountId)).FirstOrDefault();
        }

        public void DeleteMails(string orderIdsSelected)
        {
            orderIdsSelected += Guid.NewGuid().ToString();
            Context.DeleteMails(orderIdsSelected);
        }
        public class SmtpAccount
        {
            public Guid ID {get;set;}
            public string EmailAccount {get;set;}
            public bool IsEnabled {get;set;}
            public string SMTP_Server {get;set;}
            public string Port {get;set;}
            public bool SMTP_Auth {get;set;}
            public string UserName {get;set;}
            public string Password {get;set;}
            public bool IsSSL {get;set;}
            public string ReplyToEmail {get;set;}
            public string FromName {get;set;}
            public string SendCopyTo { get; set; }
        }

        public DataTable GetAllSentMails()
        {
            return Context.GetAllSentMails();
        }
    }
}
