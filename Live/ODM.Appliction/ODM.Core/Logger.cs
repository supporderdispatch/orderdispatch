﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.DbContext;
using System.Text.RegularExpressions;
using System.Data;
using ODM.Entities;
using System.Diagnostics;
using ODM.Data.Service;
using ODM.Data.Util;

namespace ODM.Core
{
    public class Logger
    {
        LoggerContext context;

        public Logger()
        {
            context = new LoggerContext();
        }

        public System.Data.DataTable GetLogsOfOrder(string orderNumber)
        {
            var logs = context.GetLogsOfOrders(orderNumber);
            for (int i = 0; i < logs.Rows.Count; i++)
            {
                string strData = Convert.ToString(logs.Rows[i]["LogText"]);
                if (strData.Contains("&&"))
                    logs.Rows[i]["LogText"] = Regex.Replace(strData, @"\&&+", "\n");
            }
            return logs;
        }

        private FinalViewDigest ParseUserDayStatToFinalViewDigest(List<UserStatDigest> userLogsList, Guid userId, DateTime ofDate)
        {
            var result = new FinalViewDigest();
            var lstLoggedSessions = new List<double>();
            var contdSessions = new Dictionary<DateTime, DateTime>();

            if (userLogsList.Count == 0)
                return result;

            var totalOrdersProcessed = userLogsList.Sum(x => x.TotalOrdersProcessed);

            var perOrderProcessRate = userLogsList.Sum(x => x.TotalPerProcessRate);
            userLogsList.ForEach(x => 
            {
                contdSessions.Add(x.LoginTime, x.LogoutTime); 
            });

            foreach (var userLog in userLogsList.ToList())
            {
                var newLoginTime = userLog.LoginTime.TimeOfDay;
                foreach (var loginTimeValue in contdSessions.Keys.ToList())
                {
                    var oldLoginTime = loginTimeValue.TimeOfDay;
                    if (contdSessions.Keys.Contains(loginTimeValue))
                    {
                        var logoutValue = contdSessions[loginTimeValue];
                        if (oldLoginTime < newLoginTime && newLoginTime < logoutValue.TimeOfDay)
                        {
                            if (logoutValue.TimeOfDay < userLog.LogoutTime.TimeOfDay)
                            {
                                contdSessions.Remove(userLog.LoginTime);
                                contdSessions.Remove(loginTimeValue);
                                contdSessions.Add(loginTimeValue, userLog.LogoutTime);

                            }
                            if (logoutValue.TimeOfDay > userLog.LogoutTime.TimeOfDay)
                            {
                                contdSessions.Remove(userLog.LoginTime);
                            }
                        }
                        if (oldLoginTime > newLoginTime && newLoginTime < logoutValue.TimeOfDay)
                        {
                            if (logoutValue.TimeOfDay < userLog.LogoutTime.TimeOfDay)
                            {
                                contdSessions.Remove(loginTimeValue);
                            }
                        }
                    }
                }
            }
            foreach (var k in contdSessions.Keys)
            {
                lstLoggedSessions.Add((contdSessions[k].TimeOfDay - k.TimeOfDay).TotalSeconds);
            }
            result.UserId = userId;
            result.OfDate = ofDate;
            result.TotalOrderProcessed = totalOrdersProcessed;
            result.PerOrderAvgProcessTime = perOrderProcessRate / (result.TotalOrderProcessed > 0 ? result.TotalOrderProcessed : 1);
            result.TotalLoggedInTime = TimeSpan.FromSeconds(lstLoggedSessions.Sum());
            result.HourlyPerOrderProcessRate = Convert.ToDecimal(result.TotalOrderProcessed) / Convert.ToDecimal(result.TotalLoggedInTime.TotalHours > 0 ? result.TotalLoggedInTime.TotalHours : 1);
            return result;
        }
        private List<UserStatDigest> ParseDataTableToUserStatDigestList(DataTable userLogs)
        {
            var result = new List<UserStatDigest>();
            foreach (DataRow row in userLogs.AsEnumerable().ToList())
            {
                var userStatDigest = new UserStatDigest()
                {
                    UserId = Guid.Parse(row["UserId"].ToString()),
                    LoginTime = DateTime.SpecifyKind(Convert.ToDateTime(row["LoginTime"].ToString()), DateTimeKind.Utc),
                    LogoutTime = DateTime.SpecifyKind(Convert.ToDateTime(row["LogoutTime"].ToString()), DateTimeKind.Utc),
                    TotalOrdersProcessed = String.IsNullOrEmpty(row["TotalOrdersProcessed"].ToString()) ? 0 : Convert.ToInt32(row["TotalOrdersProcessed"].ToString()),
                    TotalPerProcessRate = String.IsNullOrEmpty(row["TotalPerProcessRate"].ToString()) ? 0.0M : Convert.ToDecimal(row["TotalPerProcessRate"].ToString())
                };
                userStatDigest.LoginTime = userStatDigest.LoginTime.Date != userStatDigest.LogoutTime.Date ? Convert.ToDateTime(userStatDigest.LogoutTime.Date) : userStatDigest.LoginTime;
                result.Add(userStatDigest);
            }
            return result;
        }
        public FinalViewDigest GetUserLogs(Guid UserId, DateTime ofDate)
        {
            var result = new FinalViewDigest();
            var userLogs = context.GetUserLogs(UserId, ofDate);
            var userLogsList = new List<UserStatDigest>();
            userLogsList = ParseDataTableToUserStatDigestList(userLogs);
            if (userLogsList.Count > 0)
            {
                result = ParseUserDayStatToFinalViewDigest(userLogsList, UserId, ofDate);
            }
            return result;

        }
        public void AddUserLogs(UserStatics userStatistics)
        {
            var orderLogs = String.Empty;
            var digest = userStatistics.OrderDigest;
            var ordersCount = digest.Where(x => x.Processed).ToList().Count;
            var perOrderProcessRate = digest.Where(x => x.Processed).ToList().Sum(x => x.TimeTakenForExecution);
            perOrderProcessRate = ordersCount > 0 ? perOrderProcessRate / ordersCount : perOrderProcessRate;
            if (userStatistics.OrderDigest.Where(x => x.Processed).ToList().Count > 0)
            {
                foreach (var d in userStatistics.OrderDigest.Where(x => x.Processed).ToList())
                {
                    var secondsTake = Convert.ToDecimal(d.TimeTakenForExecution);
                    orderLogs += d.OrderId + "@" + Convert.ToString(Decimal.Round(secondsTake)) + ",";
                }
            }
            context.AddUserLog(userStatistics, ordersCount, perOrderProcessRate, orderLogs);
        }

        public List<List<FinalViewDigest>> GetAllUserStatisticsFrom(DateTime fromDate, DateTime toDate)
        {
            var resultSet = new List<List<FinalViewDigest>>();
            IUserService userService = new UserService(MakeConnection.GetConnection());
            var allRecords = ParseDataTableToUserStatDigestList(context.GetAllUserLogs(fromDate, toDate));
            var dateGroupedRecords = allRecords.GroupBy(x => x.LoginTime.Date).Select(x => x.ToList()).ToList();
            var users = userService.GetAllUsers().Select(x => new { UserId = x.Userid, UserName = x.Loginname }).ToList();

            foreach (var dateGroupedRecord in dateGroupedRecords)
            {
                var result = new List<FinalViewDigest>();
                foreach (var user in users)
                {
                    var userLog = ParseUserDayStatToFinalViewDigest(dateGroupedRecord.Where(x => x.UserId == user.UserId).ToList(), user.UserId, dateGroupedRecord.FirstOrDefault().LogoutTime);
                    userLog.UserName = user.UserName;
                    userLog.UserId = user.UserId;
                    userLog.OfDate = dateGroupedRecord.FirstOrDefault().LoginTime;
                    result.Add(userLog);
                }
                resultSet.Add(result);
            }
            resultSet.Reverse();
            return resultSet;
        }

        public List<FinalViewDigest> GetUserStatisticsOfPrevDays(Guid userid)
        {
            var finalDigestList = new List<FinalViewDigest>();
            for (int count = 1; count < 7; count++)
            {
                //getting log of previous days.
                var log = GetUserLogs(userid, DateTime.UtcNow.AddDays(0 - count));
                if (log.TotalOrderProcessed > 0)
                {
                    finalDigestList.Add(log);
                }
                if (finalDigestList.Count == 5)
                {
                    break;
                }
            }
            return finalDigestList;
        }

    }
}
