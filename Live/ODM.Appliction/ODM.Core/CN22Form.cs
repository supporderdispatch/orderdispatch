﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.DbContext;
using System.Data;
using System.Windows.Forms;
using ODM.Data.Printing;
using ODM.Data.Util;

namespace ODM.Core
{
    public class CN22Form
    {
        private readonly CN22FormContext _Context;

        public CN22Form()
        {
            _Context = new CN22FormContext();
        }

        public DataTable GetPostalServicesMappedWithCN22()
        {
            return _Context.GetDataOfPostalServicesMappedWithCN22();
        }


        public void UpdateCN22PostalServicesMappedSettingsFor(string strSelectedPostalIds)
        {
            _Context.UpdateCN22PostalServicesMappedSettingsFor(strSelectedPostalIds);
        }
        public void PrintForOrderId(Guid orderId)
        {
            var orderItems=_Context.GetItemDetailsOfOrder(orderId);
            if (orderItems.Rows.Count > 0)
            {
                var resultSet = orderItems.AsEnumerable()
                    .Select(x => new ItemDetails()
                    {
                        ItemTitle = x.Field<string>("Title"),
                        Weight = x.Field<Decimal>("Weight"),
                        PricePerUnit = x.Field<Decimal>("PricePerUnit"),
                        Quantity = x.Field<Decimal>("Quantity"),
                        Currency = x.Field<string>("cCurrency")
                    })
                    .ToList();
                var parsedItemstocn22Form = parseItemstocn22Form(resultSet);
                Activator.CreateInstance<PrintCN22Form>().printCN22Form(orderId.ToString(),parsedItemstocn22Form);
            }
        }

        private string parseItemstocn22Form(List<ItemDetails> itemDetails)
        {
            
            string headComponent = string.Empty;
            string itemTable = string.Empty;
            string finalDecleration = string.Empty;

            headComponent = @"<html>

<head>
    <title>CN22 Form</title>
<style>
    @page {
    margin: 0;
    }
 body {
            margin-right: 0px !important;
            margin-top: 0px !important;
            font-family: sans-serif;
            letter-spacing: -1px;
            position: relative;
            width: 620px;
            height: 400px !important;
            min-height: 100% !important;
            border:1px solid black;
        }
        
        .header-left-sidebar {
            border: 1px solid black;
            border-top:2px solid black;
            font-size: 18px;
            font-weight: bold;
            width: 100%;
            line-height: 12px;
        }
        
        .right_sm_txt {
            font-size: 14px;
        }
        
        .instuctions {
            width: 100%;
            font-size: 14px;
            border: 1px solid black;
            border-top:0;
        }
        
        .selection {
            width: 100%;
            font-size: 13px;
            padding: 0px 14px;
            border: 1px solid black;
            border-collapse: collapse;
            border-top: 0;
            border-bottom: 0;
        }
        
        .check_box {
            border: 1px solid black;
            width: 35px;
            height: 16px;
            border-top: 0;
        }
        
        .label_chk {
            padding-left: 5px;
        }
        
        .bold {
            font-weight: bold;
        }
        
        .main {
            width: 100%;
            text-align: left;
            border-top: 0;
            border-collapse: collapse;
            border: 1px solid black;
        }
        
        .main tr td:nth-of-type(3) {
            border-right: 0;
        }
        
        .main tr th:nth-of-type(3) {
            border-right: 0;
        }
        
        .main td,
        .main th {
            font-size: 13px;
            border-right: 1px solid black;
            padding: 0px 1px;
            vertical-align: top;
        }
        
        .main td {
            border-bottom: 2px black dotted;
            padding: 1px;
        }
        
        .last_item td {
            border-bottom: 1px solid black;
        }
        
        .no_border td {
            border-bottom: 0;
        }
        
        th {
            padding-left: 2px !important;
        }
        
        
</style>
</head>
<body>
    <div class='main-container'>
        <div class='container'>
            <div class='cn22-header'>
                  <table class='header-left-sidebar'>
                    <tr>
                        <td>CUSTOMS DECLARATION</td>
                        <td style='text-align:right'>CN 22<br/></td>
                    </tr>
                    <tr>
                        <td>DÉCLARATIONS EN DOUANE</td>
                        <td style='text-align:right;font-size: 15px'>
                         <span>May be opened officially</span><br/>
                         <span style='font-weight:normal'>Peut être ouvert d'office</span>
                        </td>
                    </tr>
                </table>
                <table class='instuctions'>
                    <tr>
                        <td class='head1'>
                            <span style='font-weight: bold'>Great Britain</span>
                            <span>\Grande Bretagne</span>
                        </td>
                        <td style='font-weight: bold; text-align: center'>Important!</td>
                        <td style='font-weight: bold; text-align: right'>See Instructions on the back</td>
                    </tr>
                </table>
            </div>
            <table cellpadding='0' cellspacing='0' class='selection'>
                <tr>
                    <td class='check_box'></td>
                    <td style='width:25%' class='label_chk'>
                        <span class='bold'>Gift</span>\Cadeau
                    </td>
                    <td class='check_box' style='text-align: center;font-weight: bold'>x</td>
                    <td class='label_chk'>
                        <span class='bold'>Commercial sample</span>\Échantillon commercial
                    </td>
                </tr>
                <tr>
                    <td style='border-bottom:0' class='check_box'></td>
                    <td class='label_chk'>
                        <span class='bold'>Documents</span>
                    </td>
                    <td style='border-bottom:0' class='check_box'></td>
                    <td class='label_chk'>
                        <span class='bold'>Other</span>\Autre
                        <span style='padding-left: 50px;'>Tick one or more boxes</span>
                    </td>
                </tr>
            </table>";

            finalDecleration = @"
                <tr>
                    <th style='border-top:1px solid black;border-right:0;padding-bottom: 0px;font-size:12px;line-height: 10px;padding-top: 0px;' colspan='3'>I, the undersigned, whose name and address are given on the item, certify that the particulars given in this declaration are correct and that this item does not contain any dangerous article or articles prohibited by legislation or by postal or customs regulations<br/> Date and sender's signature(8) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style='font-weight:normal'>" + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss") + @"</span></th>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>";
            itemTable = @"<table class='main'>
                <tr>
                    <th style='width:68%;'>Quantity and detailed description of contents(1)<br/> Quantité et description détaillée du contenu</th>
                    <th>Weight (in kg)(2)<br/> Poids</th>
                    <th>Value(3)<br/> Valeur</th>
                </tr>";
            if (itemDetails.Count < 3)
            {
                for (int i = 0; i <= 3 - itemDetails.Count; i++)
                {
                    itemDetails.Add(new ItemDetails());
                }
            }

                    for (int i = 0; i <= 2;i++ )
                    {
                        var item = itemDetails.ElementAt(i);
                        var className = string.Empty;
                        if (i == 2)
                        {
                            className = "last_item";
                        }
                        if (i == 0)
                        {
                            className = "custom-dotted-td";
                        }

                        if (item.ItemTitle != null)
                        {                            
                            var newRow = "<tr class='" + className + "'>";
                            newRow += "<td style='letter-spacing:0px;font-size:10px;border:0;border-top: 1px dotted black;border-right: 1px solid black '>" + item.ItemTitle.Trim().LimitLengthTo(105) + " x " + Math.Floor(item.Quantity).ToString().Trim() + "</td>";
                            newRow += "<td style='letter-spacing:0px;font-size:10px;border:0;border-top: 1px dotted black;border-right: 1px solid black'>" + Math.Round(item.Weight,2).ToString().Trim() + "</td>";
                            newRow += "<td style='letter-spacing:0px;font-size:10px;border:0;border-top: 1px dotted black'>" + String.Format("{0:F2}",Math.Round(item.PricePerUnit,2).ToString().Trim())+ "(" + item.Currency.Trim() + ")" + "</td>";
                            newRow += "</tr>";
                            itemTable += newRow;
                        }
                        else
                        {
                            var newRow = "<tr class='" + className + "'>";
                            newRow += "<td style='font-size:10px;border:0;border-top: 1px dotted black;border-right: 1px solid black'>&nbsp;</td>";
                            newRow += "<td style='font-size:10px;border:0;border-top: 1px dotted black;border-right: 1px solid black'></td>";
                            newRow += "<td style='font-size:10px;border:0;border-top: 1px dotted black'></td>";
                            newRow += "</tr>";
                            itemTable += newRow;
                        }
                    }

            itemTable += @"
                <tr>
                    <th style='border-top:1px solid black'>For commercial items only<br/> If known, HS tarrif number(4) and country of origin of goods (5)<br/> <span style='font-weight: normal '>N°tarifaire du SH et pays d'origine des marchandises (si connus)</span></th>
                    <th style='border-top:1px solid black'>Total Weight<br/> Poids total <br/> (in kg)(6)</th>
                    <th style='border-top:1px solid black'>Total Value(7)<br/> Valeur totale </th>
                </tr>";

            itemTable += "<tr class='no_border'><td></td><td>" + Math.Round(itemDetails.Sum(x => x.Weight * x.Quantity),2).ToString().Trim() + "</td>";
            itemTable += "<td>" + Math.Round(itemDetails.Sum(x => x.PricePerUnit * x.Quantity), 2).ToString().Trim() + "(" + itemDetails.FirstOrDefault().Currency + ")" + "</td></tr>";

            return headComponent+itemTable+finalDecleration;
        }
        public static void PrintForOrder(Guid OrderId)
        {
            new CN22Form().PrintForOrderId(OrderId);
        }
    }

    public struct ItemDetails
    {
        public string ItemTitle;
        public Decimal Weight;
        public Decimal PricePerUnit;
        public Decimal Quantity;
        public string Currency;
    }
}
