﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities
{
    public enum ProcessState
    {
        None = 0,
        Loading = 1,
        Printing = 2,
        Completed = 3,
        Failed = 4
    }
    public enum ProcessCompleted
    {
        None=0,
        Label_Printed=1,
        Invoice_Printed=2,
        Shipping_Method_Changed=3,
        Processed=4
    }
    public struct RowData
    {
        public int RowIndex { get; set; }
        public string FieldData { get; set; }
    }
}
