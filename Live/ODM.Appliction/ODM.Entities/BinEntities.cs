﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities
{
    public class Item
    {
        public string Name;
        public string Value;

        public Item(string name, string Value)
        {
            this.Name = name;
            this.Value = Value;
        }

        public override string ToString()
        {
            return Name;
        }
    }
    public enum ItemStatus
    {
        None = 0,
        NotAddedInBin = 1,
        AlreadyAddedToBin = 2,
        CompletesOrder = 3,
        ConsolidationCompleted = 4
    }

    public enum BinPromptResult
    {
        Cancel = 0,
        Yes = 1,
        ProceedToScan = 2,
        AddItemsHavingNoBacode = 3,
        DeleteBin = 4
    }

    public struct Bin
    {
        public int BinNumber;
        public int OrderNumber;
        public int TotalNumberofItems;
        public int NumberOfItemsAlreadyAddedInBin;
    }
}
