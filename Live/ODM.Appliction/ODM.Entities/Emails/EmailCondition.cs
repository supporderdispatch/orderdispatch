﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.Emails
{
    public class EmailCondition
    {
        public Guid EmailConditionId { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string Creterion { get; set; }
        public string CompareWith { get; set; }
        public int EmailConditionCriteriaMatch { get; set; }
        public string CriteriaDataType { get; set; }
    }
}
