﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.Emails
{
    public class Email
    {
        public Guid OrderId {get;set;}
        public string NumOrderId { get; set; }
        public string MailToAddress {get;set;}
        public string SentMailBody {get;set;}
        public string SentSubject {get;set;}
        public bool IsMailSent {get;set;}
        public Guid EmailTemplateId { get; set; }
        public DateTime CreatedOn { get; set; } 
    }
}
