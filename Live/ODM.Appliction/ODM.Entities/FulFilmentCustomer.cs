﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities
{
    public class FulFilmentCustomer
    {
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid ConditionId { get; set; }
    }
}
