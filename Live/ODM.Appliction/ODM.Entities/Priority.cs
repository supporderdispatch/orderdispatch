﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities
{
    public enum Priority
    {
        High,
        Medium,
        Normal,
        Low,
        None
    }
}
