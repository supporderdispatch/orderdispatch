﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ODM.Appliction.Extensions
{
   public class SavedFilterSetting
    {
       public string ColumnName { get; set; }
       public RecordFilter AppliedRecordFilter {get;set;} 
    }

   public enum RecordFilter
   {
       NoFilter,
       Contains,
       DoesNotContain,
       StartsWith,
       Equals,
       NotEqualTo,
       GreaterThan,
       LessThan,
       GreaterThanOrEqualTo,
       LessThanOrEqualTo,
       IsEmpty,
       IsNotEmpty,
       IsNull,
       IsNotNull
   }
}
