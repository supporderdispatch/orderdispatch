﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.InvoiceTemplate
{
    public class InvoiceTemplate
    {
        public Guid TemplateId { get; set; }
        public string TemplateName {get;set;}
        public string LayoutDesign {get;set;}
        public string PageSize {get;set;}
        public int PageOrientation{get;set;}
        public Guid PrintContidtionID {get;set;}
        public bool IsAutoPrint { get; set; }
        public bool IsEnabled {get;set;}
        public bool IsConditional {get;set;}
        public bool IsPdfTemplate {get;set;}
        public Priority PrintPriority { get; set; }
        public string IPv4Address { get; set; }
        public string SelectedPrinter { get; set; }
    }
}
