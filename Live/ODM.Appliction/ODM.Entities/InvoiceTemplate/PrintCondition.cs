﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.InvoiceTemplate
{
    public class PrintCondition
    {
        public Guid PrintConditionId { get; set; }
        public string TableName {get;set;}
        public string ColumnName {get;set;}
        public string Creterion {get;set;}
        public string CompareWith { get; set; }
        public string PrintConditionMatch { get; set; }
        public string CriteriaDataType { get; set; }
    }
}
