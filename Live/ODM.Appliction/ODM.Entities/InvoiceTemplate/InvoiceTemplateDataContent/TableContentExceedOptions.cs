﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.InvoiceTemplate.InvoiceTemplateDataContent
{
    public enum TableContentExceedOptions
    {
        OverFlowHidden,
        CreateNewPageRetainingOtherFields,
        CreateNewPageAdjustingOtherFields,
        ChooseAnotherTemplate
    }
}
