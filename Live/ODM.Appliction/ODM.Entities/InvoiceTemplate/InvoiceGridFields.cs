﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.InvoiceTemplate
{
    public class InvoiceField
    {
        public string Field { get; set; }
        public string DisplayName { get; set; }
        public bool Inserted { get; set; }
        public CustomDataTypes DataType { get; set; }
        public string DecimalPlaces { get; set; }
    }
}
