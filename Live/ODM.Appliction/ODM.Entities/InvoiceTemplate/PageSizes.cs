﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.InvoiceTemplate
{
    public enum Pagesize
    {
        A0,
        A1,
        A10,
        A2,
        A3,
        A4,
        A5,
        A6,
        A7,
        A8,
        A9,
        B0,
        B1,
        B10,
        B2,
        B3,
        B4,
        B5,
        B6,
        B7,
        B8,
        B9,
        Default,
        EXECUTIVE,
        LEDGER,
        LEGAL,
        LETTER,
        TABLOID
    }
}
