﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities
{
    public enum FilterOption
    {
        All=0,
        Yes=1,
        No=2
    }
    public struct PickListBatch
    {
        public string BatchName;
        public int OrdersRemaining;
    }
}
