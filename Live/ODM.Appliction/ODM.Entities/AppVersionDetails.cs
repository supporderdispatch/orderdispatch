﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities
{
    public class AppVersionDetails
    {
        public string AppVersion {get;set;}
        public string Remark {get;set;}
        public string DownloadUrl {get;set;}
        public bool Approved {get;set;}
        public DateTime CreatedOn { get; set; }
    }
}
