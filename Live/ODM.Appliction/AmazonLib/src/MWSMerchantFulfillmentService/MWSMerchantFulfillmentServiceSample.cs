/*******s   ************************************************************************
 * Copyright 2009-2016 Amazon Services. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 *
 * You may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *******************************************************************************
 * MWS Merchant Fulfillment Service
 * API Version: 2015-06-01
 * Library Version: 2016-03-30
 * Generated: Fri Nov 11 06:01:15 PST 2016
 */

using MWSMerchantFulfillmentService.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MWSMerchantFulfillmentService
{

    /// <summary>
    /// Runnable sample code to demonstrate usage of the C# client.
    ///
    /// To use, import the client source as a console application,
    /// and mark this class as the startup object. Then, replace
    /// parameters below with sensible values and run.
    /// </summary>
    public class MWSMerchantFulfillmentServiceSample
    {

        private readonly MWSMerchantFulfillmentService client;


        public Item Item = new Item();
        public Address Address = new Address();
        public PackageDimensions PackageDimensions = new PackageDimensions();
        public Weight Weight = new Weight();
        public ShippingServiceOptions ShippingServiceOptions = new ShippingServiceOptions();
        public static string SellerID { get; set; }
        public static string AmazonOrderID { get; set; }
        public static string ShippingServiceID { get; set; }
        public static string ShippingOfferID { get; set; }
        public static string OrderItemID { get; set; }
        public static string ShipmentID { get; set; }
      //  public static string MappedCourierName { get; set; }
        ShipmentRequestDetails objshipmentRequestDetails = new ShipmentRequestDetails();

        public MWSMerchantFulfillmentServiceSample(MWSMerchantFulfillmentService client)
        {
            this.client = client;
        }

        public CancelShipmentResponse InvokeCancelShipment()
        {
            // Create a request.
            CancelShipmentRequest request = new CancelShipmentRequest();
            request.SellerId = SellerID;
            request.ShipmentId = ShipmentID;
            return this.client.CancelShipment(request);
        }

        public CreateShipmentResponse InvokeCreateShipment()
        {
            CreateShipmentRequest request = new CreateShipmentRequest();
            request.SellerId = SellerID;
            request.ShipmentRequestDetails = objshipmentRequestDetails;
            request.ShippingServiceId = ShippingServiceID;
            request.ShippingServiceOfferId = ShippingOfferID;
            string hazmatType = "None";
            request.HazmatType = hazmatType;
            CreateShipmentResponse objCreateShipmentResponse = this.client.CreateShipment(request);
            ShipmentID = objCreateShipmentResponse.CreateShipmentResult.Shipment.ShipmentId;
            return objCreateShipmentResponse;
        }

        public GetEligibleShippingServicesResponse InvokeGetEligibleShippingServices(string mappedCourier)
        {
           
            GetEligibleShippingServicesRequest request = new GetEligibleShippingServicesRequest();
            request.SellerId = SellerID;

            objshipmentRequestDetails.AmazonOrderId = AmazonOrderID;
            objshipmentRequestDetails.PackageDimensions = this.PackageDimensions;
            objshipmentRequestDetails.Weight = this.Weight;
            objshipmentRequestDetails.ShipFromAddress = this.Address;
            objshipmentRequestDetails.ShippingServiceOptions = this.ShippingServiceOptions;

            if (!objshipmentRequestDetails.ItemList.Any(x => x.OrderItemId.Equals(this.Item.OrderItemId)))
            {
                objshipmentRequestDetails.ItemList.Add(this.Item);
            }

            request.ShipmentRequestDetails = objshipmentRequestDetails;
            GetEligibleShippingServicesResponse objGetEligibleShippingServicesResponse = new GetEligibleShippingServicesResponse();
            objGetEligibleShippingServicesResponse = this.client.GetEligibleShippingServices(request);
            if (objGetEligibleShippingServicesResponse.GetEligibleShippingServicesResult.ShippingServiceList.Where(x => x.ShippingServiceName.Equals(mappedCourier)).Any())
            {
                ShippingServiceID = objGetEligibleShippingServicesResponse.GetEligibleShippingServicesResult.ShippingServiceList.Where(x => x.ShippingServiceName.Equals(mappedCourier)).FirstOrDefault().ShippingServiceId;
                ShippingOfferID = objGetEligibleShippingServicesResponse.GetEligibleShippingServicesResult.ShippingServiceList.Where(x => x.ShippingServiceName.Equals(mappedCourier)).FirstOrDefault().ShippingServiceOfferId;
            }
            
            return objGetEligibleShippingServicesResponse;
        }

        public GetShipmentResponse InvokeGetShipment()
        {
            // Create a request.
            GetShipmentRequest request = new GetShipmentRequest();
            string sellerId = "example";
            request.SellerId = sellerId;
            string mwsAuthToken = "example";
            request.MWSAuthToken = mwsAuthToken;
            string shipmentId = "example";
            request.ShipmentId = shipmentId;
            return this.client.GetShipment(request);
        }

        public GetServiceStatusResponse InvokeGetServiceStatus()
        {
            // Create a request.
            GetServiceStatusRequest request = new GetServiceStatusRequest();
            string sellerId = "example";
            request.SellerId = sellerId;
            string mwsAuthToken = "example";
            request.MWSAuthToken = mwsAuthToken;
            return this.client.GetServiceStatus(request);
        }

        public void AddItemToItemList(Model.Item item)
        {
            this.objshipmentRequestDetails.ItemList.Add(item);
        }
    }
}
