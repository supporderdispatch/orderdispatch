﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using ODM.Core.UserTracking;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using ODM.Data.DbContext;
using ODM.Core;
using ODM.Appliction.Extensions;

namespace ODM.Appliction
{
    public partial class CheckOrderStatus : Form
    {
        DataView objDv = new DataView();
        DataTable objDt = new DataTable();
        DataTable tmpTable = new DataTable();
        string strOrderID = string.Empty;
        string strNumOrderID = string.Empty;
        static int currentPageIndex = 0;
        public int pageSize = 20;
        SendEmailEntity objSendEmail = new SendEmailEntity();
        DataTable apiGrid = new DataTable();
        DataTable apiGridSource = new DataTable();
        private bool FilterActivated = false;

        public CheckOrderStatus()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            //BindGrid();
            btnPrevious.Enabled = false;
            SetPhysicalStyleOfGridView();
            dtpFromDate.Value = DateTime.Now.AddDays(-1);
            InitializeSearchByComboBox();
            grdOrders.Layout += grdOrders_Layout;
            grdOrders.MouseMove += grdOrders_MouseMove;
        }

        void grdOrders_MouseMove(object sender, MouseEventArgs e)
        {
            if (ODM.Appliction.Extensions.DataGridViewFilter.activateFilters)
            {
                HidePaginationControls();
            }
            else
            {
                ShowPaginationControls();
            }
        }

        private void grdOrders_Layout(object sender, LayoutEventArgs e)
        {

            //LoadApiGrid();
        }

        private void InitializeSearchByComboBox()
        {
            cmbSearch.Items.Clear();
            (new List<string>() { "OrderId", "OrderNumber", "PostCode", "ReferenceNumber", "SecondaryReference", "ExternalReferenceNum", "TrackingNumber", "CustomerName", "ItemTitle", "ItemSKU" }).ForEach(x => { cmbSearch.Items.Add(x); });
            this.cmbSearch.DrawItem += cmbSearch_DrawItem;
        }

        void cmbSearch_DrawItem(object sender, DrawItemEventArgs e)
        {
            ComboBox box = sender as ComboBox;

            if (Object.ReferenceEquals(null, box))
            {
                return;
            }
            e.DrawBackground();
            if (e.Index >= 0)
            {
                Graphics g = e.Graphics;
                using (Brush brush = ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                      ? new SolidBrush(SystemColors.Highlight)
                      : new SolidBrush(Color.WhiteSmoke))
                {
                    using (Brush textBrush = new SolidBrush(e.ForeColor))
                    {
                        g.FillRectangle(brush, e.Bounds);

                        g.DrawString(box.Items[e.Index].ToString(),
                                     e.Font,
                                     textBrush,
                                     e.Bounds,
                                     StringFormat.GenericDefault);
                    }
                }

            }
            e.DrawFocusRectangle();

        }
        private void SetPhysicalStyleOfGridView()
        {
            CheckForIllegalCrossThreadCalls = false;
            //grdOrders.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            //grdOrders.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            //grdOrders.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            //grdOrders.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            //grdOrders.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdOrders.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            //grdOrders.ScrollBars = ScrollBars.Both;
            grdOrders.RowHeadersVisible = false;
            grdOrders.DoubleBuffered(true);

        }
        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                ValidateAndBindGrid();
            }
        }
        private void ValidateAndBindGrid()
        {
            if (string.IsNullOrEmpty(txtSearch.Text))
            {
                lblRequired.Show();
            }
            else
            {
                BindGrid("");
            }
        }
        private void PopulateColumns()
        {
            grdOrders.Columns["Num Order Id"].DisplayIndex = 0;
            grdOrders.Columns["Orderid"].Visible = false;
            grdOrders.Columns["ImageID"].Visible = false;
            //grdOrders.Columns["Image Bytes"].Visible = false;
        }
        public void LoadApiGrid()
        {
            grdOrders.DataSource = apiGridSource.Clone();
            //ResetGridView();
            if (apiGridSource.Rows.Count > 0)
            {
                //if (string.IsNullOrEmpty(apiGridSource.Rows[0]["OrderId"].ToString()))
                //{
                //    apiGridSource.Rows.RemoveAt(0);
                //    apiGridSource.AcceptChanges();
                //}
                btnNext.Enabled = true;
                apiGrid = new DataTable();
                apiGrid = apiGridSource.Clone();
                int startIndex = currentPageIndex * pageSize;
                int endIndex = currentPageIndex * pageSize + pageSize;
                if (endIndex > apiGridSource.Rows.Count)
                {

                    btnNext.Enabled = false;
                    endIndex = apiGridSource.Rows.Count;
                }
                for (int i = startIndex; i < endIndex; i++)
                {
                    DataRow newRow = apiGrid.NewRow();
                    GetNewRow(ref newRow, apiGridSource.Rows[i]);
                    apiGrid.Rows.Add(newRow);
                }
                try
                {
                    if (apiGridSource.Rows.Count <= pageSize)
                    {
                        btnNext.Enabled = false;
                    }
                    var currentPage = currentPageIndex + 1;
                    var totalPages = (int)Math.Ceiling(Convert.ToDecimal(apiGridSource.Rows.Count) / pageSize);
                    if (currentPage == totalPages)
                    {
                        btnNext.Enabled = false;
                    }
                    lblPageIndex.Text = (currentPageIndex + 1) + "/" + (int)Math.Ceiling(Convert.ToDecimal(apiGridSource.Rows.Count) / pageSize);
                }
                catch
                {
                    lblPageIndex.Text = (currentPageIndex + 1) + "/" + (int)Math.Ceiling(Convert.ToDecimal(apiGridSource.Rows.Count) / 10);
                }
                if (ODM.Appliction.Extensions.DataGridViewFilter.activateFilters)
                {
                    grdOrders.ApplyFilter(apiGrid);
                }

                CustomBind(apiGrid);
                ConvertImageColumnToLinkColumn();
                //  SetPhysicalStyleOfGridView();
            }
            else
            {
                grdOrders.DataSource = apiGridSource;
                lblPageIndex.Text = "0/0";
                lblTotal.Text = "Total Records : " + apiGridSource.Rows.Count.ToString();
            }
            AddOrderDetailsColumn();
            lblTotal.Text = "Total Records : " + apiGridSource.Rows.Count.ToString();
            OrderGridColumns();
        }

        private void OrderGridColumns()
        {
            grdOrders.Columns["Orderid"].DisplayIndex = 0;
            grdOrders.Columns["Num Order Id"].DisplayIndex = 1;
            grdOrders.Columns["FullName"].DisplayIndex = 2;
            grdOrders.Columns["ProcessedDateTime"].DisplayIndex = 3;
            grdOrders.Columns["ReceivedDate"].DisplayIndex = 4;
            grdOrders.Columns["ReferenceNum"].DisplayIndex = 5;
            grdOrders.Columns["SecondaryReference"].DisplayIndex = 6;
            grdOrders.Columns["ExternalReferenceNum"].DisplayIndex = 7;
            grdOrders.Columns["PostCode"].DisplayIndex = 8;
            grdOrders.Columns["Status"].DisplayIndex = 9;
            grdOrders.Columns["TrackingNumber"].DisplayIndex = 10;
            grdOrders.Columns["Image"].DisplayIndex = 11;
            grdOrders.Columns["btnOrderDetails"].DisplayIndex = 12;

        }

        private void ShowPaginationControls()
        {
            btnNext.Visible = true;
            btnPrevious.Visible = true;
            txtSearchNoOfRecords.Visible = true;
            lblTotal.Visible = true;
            lblPageIndex.Visible = true;
        }

        private void HidePaginationControls()
        {
            btnNext.Visible = false;
            btnPrevious.Visible = false;
            txtSearchNoOfRecords.Visible = false;
            lblTotal.Visible = false;
            lblPageIndex.Visible = false;
        }
        public delegate void InvokeMe();

        public void Invoker()
        {
            apiGridSource = (DataTable)grdOrders.DataSource;
            LoadApiGrid();
            //   grdOrders.ReadOnly = false;
            // grdOrders.EditMode = DataGridViewEditMode.EditOnEnter;
        }
        private async void BindGrid(string strOrderType)
        {
            picLoading.Visible = true;
            ResetGridView();
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:

            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = MakeConnection.GetConnection();
                conn.Open();

                SqlParameter[] objPraram = new SqlParameter[6];
                objPraram[0] = new SqlParameter("@SearchField", SqlDbType.VarChar, 100);
                objPraram[0].Value = string.IsNullOrEmpty(strOrderType) ? (string)cmbSearch.SelectedItem ?? cmbSearch.Text : string.Empty;

                objPraram[1] = new SqlParameter("@SearchValue", SqlDbType.VarChar, 100);
                objPraram[1].Value = txtSearch.Text;

                objPraram[2] = new SqlParameter("@SearchByDateType", SqlDbType.VarChar, 100);
                objPraram[2].Value = GetSelectedDateType();

                objPraram[3] = new SqlParameter("@OrderType", SqlDbType.VarChar, 2);
                objPraram[3].Value = strOrderType;

                objPraram[4] = new SqlParameter("@FromDate", SqlDbType.DateTime);
                objPraram[4].Value = dtpFromDate.Value;

                objPraram[5] = new SqlParameter("ToDate", SqlDbType.DateTime);
                objPraram[5].Value = dtpToDate.Value;


                // objDt = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrderProcessStatus", objPraram).Tables[0];
                objDt = await Task.Run<System.Data.DataTable>(() =>
                {
                    var result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrderProcessStatusWithImages_New", objPraram).Tables[0];
                    foreach (DataRow row in result.Rows)
                    {
                        var processedDate = row["ProcessedDateTime"].GetType().Equals(typeof(DBNull)) ? "" : row["ProcessedDateTime"].ToString();
                        var receivedDate = row["ReceivedDate"].GetType().Equals(typeof(DBNull)) ? "" : row["ReceivedDate"].ToString();

                        if (!string.IsNullOrEmpty(processedDate))
                        {
                            row["ProcessedDateTime"] = ODM.Core.Helpers.Conversions.GetLocalDateFromUtc(processedDate);
                        }
                        if (!string.IsNullOrEmpty(receivedDate))
                        {
                            row["ReceivedDate"] = ODM.Core.Helpers.Conversions.GetLocalDateFromUtc(receivedDate);
                        }
                    }
                    result.AcceptChanges();
                    return result;
                });
                objDv = new DataView(objDt);
                SetPhysicalStyleOfGridView();
                if (string.IsNullOrEmpty(txtSearchNoOfRecords.Text))
                {

                    txtSearchNoOfRecords.Text = pageSize.ToString();
                }
                InvokeMe callBack = Invoker;
                if (!FilterActivated)
                {
                    grdOrders.ApplyFilter(objDt, callBack);
                    FilterActivated = true;
                }
                else
                {
                    var grd = ODM.Appliction.Extensions.DataGridViewFilter.grd;
                    if (grd != null && grd.Parent != null && grd.Parent != grdOrders.Parent)
                    {
                        grdOrders.ApplyFilter(objDt, callBack);
                        FilterActivated = true;
                    }
                    else
                    {
                        ODM.Appliction.Extensions.DataGridViewFilter.MainDataSource = objDt;
                    }
                }
                apiGridSource = objDt;
                LoadApiGrid();
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    MessageBox.Show("Error occurred please try again after sometime", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred while binding order grid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                picLoading.Visible = false;
                conn.Close();
            }
        }

        private void AddOrderDetailsColumn()
        {
            if (grdOrders.Rows.Count > 0)
            {
                if (!grdOrders.Columns.Contains("btnOrderDetails"))
                {
                    DataGridViewButtonColumn orderDetails = new DataGridViewButtonColumn();
                    orderDetails.Name = "btnOrderDetails";
                    orderDetails.HeaderText = string.Empty;
                    orderDetails.DefaultCellStyle.NullValue = "Details";
                    orderDetails.FlatStyle = FlatStyle.Flat;
                    grdOrders.Columns.Insert(grdOrders.Columns.Count, orderDetails);
                    grdOrders.Columns["btnOrderDetails"].DisplayIndex = grdOrders.Columns.Count - 1;
                    grdOrders.Columns["btnOrderDetails"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                }
                else
                {
                    grdOrders.Columns["btnOrderDetails"].DisplayIndex = grdOrders.Columns.Count - 1;
                }
                grdOrders.Columns["Image"].DisplayIndex = grdOrders.Columns.Count - 2;
            }

        }

        private void ResetGridView()
        {
            grdOrders.DefaultCellStyle.WrapMode = DataGridViewTriState.NotSet;
            grdOrders.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            grdOrders.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            grdOrders.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            grdOrders.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
        }

        private void ConvertImageColumnToLinkColumn()
        {

            foreach (DataGridViewRow row in grdOrders.Rows)
            {
                if (row.Cells["Image"].Value.ToString().Trim().ToUpper().Equals("NO IMAGE"))
                {
                    row.Cells["Image"].Style.ForeColor = row.DefaultCellStyle.BackColor;
                }
                else
                {
                    row.Cells["Image"] = new DataGridViewButtonCell() { };
                    row.Cells["Image"].Value = "Image";
                    row.Cells["Image"].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    (row.Cells["Image"] as DataGridViewButtonCell).FlatStyle = FlatStyle.Flat;

                }
            }
        }

        private string GetSelectedDateType()
        {
            if (rbtnByProcessedDate.Checked)
            {
                return "ProcessedDate";
            }
            if (rbtnByReceivedDate.Checked)
            {
                return "ReceivedDate";
            }
            else
            {
                return "AllDates";
            }
        }

        private void CustomBind(DataTable dataTable)
        {
            if (dataTable.Rows.Count > 0)
            {
                CheckForIllegalCrossThreadCalls = false;
                grdOrders.AllowUserToAddRows = false;
                grdOrders.DataSource = dataTable;
                grdOrders.Columns["OrderId"].Width = 240;
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    grdOrders.Columns[i].ReadOnly = true;
                }
                lblMsg.Hide();
                PopulateColumns();
                grdOrders.Show();
                btnProcessed.Show();
                btnPending.Show();
                lblStatusMsg.Show();
                lblRequired.Hide();
                lblTotal.Text = "Total Records : " + objDt.Rows.Count.ToString();
                // 
            }
            else
            {
                lblMsg.Show();
                lblStatusMsg.Hide();
                grdOrders.Hide();
            }
        }

        private void GetNewRow(ref DataRow newRow, DataRow dataRow)
        {
            foreach (DataColumn col in objDt.Columns)
            {
                newRow[col.ColumnName] = dataRow[col.ColumnName];
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ValidateAndBindGrid();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            lblStatusMsg.Text = "All Orders";
            lblStatusMsg.ForeColor = Color.Black;
            BindGrid("");
        }


        private void grdOrders_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewRow row = grdOrders.Rows[e.RowIndex];
            string strProcessStatus = Convert.ToString(row.Cells["Status"].Value);
            row.DefaultCellStyle.BackColor = strProcessStatus.ToUpper() == "PROCESSED" ? btnProcessed.BackColor : btnPending.BackColor;
            if (objDv.ToTable().Rows.Count == 1)
            {
                if (strProcessStatus.ToUpper() == "PROCESSED")
                {
                    lblStatusMsg.Text = "Processed Orders";
                    lblStatusMsg.ForeColor = Color.Green;
                }
                else
                {
                    lblStatusMsg.Text = "Pending Orders";
                    lblStatusMsg.ForeColor = Color.Orange;
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnProcessed_Click(object sender, EventArgs e)
        {
            currentPageIndex = 0;
            btnPrevious.Enabled = false;
            btnNext.Enabled = true;
            lblStatusMsg.ForeColor = Color.Green;
            txtSearch.Text = "";
            lblStatusMsg.Text = "Processed Orders";
            BindGrid("1");
        }

        private void btnPending_Click(object sender, EventArgs e)
        {
            currentPageIndex = 0;
            btnPrevious.Enabled = false;
            btnNext.Enabled = true;
            lblStatusMsg.ForeColor = Color.Orange;
            txtSearch.Text = "";
            lblStatusMsg.Text = "Pending Orders";
            BindGrid("0");
        }

        private void grdOrders_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int intSelectedRowIndex = grdOrders.HitTest(e.X, e.Y).RowIndex;
                int intCount = 0;
                ContextMenu cm = new ContextMenu();

                if (intSelectedRowIndex >= 0)
                {
                    strOrderID = grdOrders.Rows[intSelectedRowIndex].Cells["OrderId"].Value.ToString();
                    strNumOrderID = grdOrders.Rows[intSelectedRowIndex].Cells["Num Order Id"].Value.ToString();
                    string strStatus = grdOrders.Rows[intSelectedRowIndex].Cells["Status"].Value.ToString();

                    foreach (DataGridViewRow row in grdOrders.Rows)
                    {
                        if (intCount == intSelectedRowIndex)
                        { row.Selected = true; }
                        else { row.Selected = false; }
                        intCount++;
                    }
                    if (strStatus.ToUpper() == "PENDING")
                    {
                        cm.MenuItems.Add(new MenuItem("Change to processed ", ChangeToProcessed_click));

                    }
                    if (strStatus.ToUpper() == "PROCESSED")
                    {
                        cm.MenuItems.Add(new MenuItem("Print Invoice", PrintInvoice_click));
                    }
                }
                cm.MenuItems.Add(new MenuItem("View Logs", ViewLog_click));
                cm.Show(grdOrders, new Point(e.X, e.Y));
            }
        }

        private async void PrintInvoice_click(object sender, EventArgs e)
        {
            picLoading.Visible = true;
            await Task.Run(() =>
            {
                CheckForIllegalCrossThreadCalls = false;
                Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().PrintInvoiceForOrder(Guid.Parse(strOrderID), false);
            }).ContinueWith((result) =>
            {
                picLoading.Visible = false;
            });
        }

        private void ViewLog_click(object sender, EventArgs e)
        {
            var fr = new AllOpenOrders();
            fr.ShowLogsOfSelectedOrders(new List<string>() { strNumOrderID.ToString() });

        }

        private void ChangeToProcessed_click(object sender, EventArgs e)
        {
            SetProcessedInDatabase();
            BindGrid("");
        }

        private void SetProcessedInDatabase()
        {
            try
            {
                OrderTracker.BeginTrackerForOrder(strOrderID);
            }
            catch
            {
            }



            SqlConnection connection = new SqlConnection();
            try
            {
                Guid orderID = new Guid(strOrderID);
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlParameter[] objorderParams = new SqlParameter[3];

                objorderParams[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objorderParams[0].Value = orderID;

                objorderParams[1] = new SqlParameter("@OrderItemId", SqlDbType.NVarChar, 1000);
                objorderParams[1].Value = "";

                objorderParams[2] = new SqlParameter("@IsAllItemShipped", SqlDbType.NVarChar, 1);
                objorderParams[2].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdatePartShipped", objorderParams);

                string IsAllItemShipped = Convert.ToString(objorderParams[2].Value);
                if (IsAllItemShipped == "Y")
                {
                    GlobalVariablesAndMethods.OrderIdToLog = orderID;
                    GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
                    GlobalVariablesAndMethods.LogHistory("This order has been manually updated to processed");

                    //Mapping and saving details for email functionality 
                    //DataTable objDt = GlobalVariablesAndMethods.GetOrderDetailsByOrderId(orderID);
                    //objSendEmail.OrderID = strOrderID;
                    //objSendEmail.NumberOrderID = Convert.ToString(objDt.Rows[0]["NumOrderId"]);
                    //objSendEmail.Date = Convert.ToDateTime(Convert.ToString(objDt.Rows[0]["DespatchByDate"]));
                    //objSendEmail.FullName = Convert.ToString(objDt.Rows[0]["FullName"]);
                    //objSendEmail.EmailID = Convert.ToString(objDt.Rows[0]["EmailAddress"]);
                    //objSendEmail.Mode = "INSERT";
                    //string strResult = GlobalVariablesAndMethods.InsertUpdateDeleteDataForPendingEmail(objSendEmail);
                    //End of Mapping for email functionality
                    ODM.Core.Emails.CreatePendingEmail.CreatePendingEmailForOrder(Guid.Parse(strOrderID));
                    OrderTracker.MarkOrderProcessed(strOrderID);
                }
                else
                {
                    GlobalVariablesAndMethods.LogHistory("Something went wrong while updating process status in Database");
                }
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Error occurred while updating process status from context menu&&" + ex.Message);
                MessageBox.Show("Error occurred while updating process status from context menu\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                OrderTracker.EndTracking();
            }
            catch { }
        }

        private async void grdOrders_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int intRowIndex = e.RowIndex;
            // bool blIsImageExists = false;

            if (grdOrders.Columns[e.ColumnIndex].Name.Equals("Image"))
            {
                if (grdOrders.Rows[intRowIndex].Cells["Image"].Value.ToString().ToUpper() == "NO IMAGE")
                {
                    //   blIsImageExists = false;
                    MessageBox.Show("No Image Exists for This Order.", "Order Image", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {

                    // blIsImageExists = true;
                    grdOrders.Rows[intRowIndex].Cells["Image"].Value = "...";
                    string strNumOrderId = grdOrders.Rows[intRowIndex].Cells["Num Order Id"].Value.ToString();
                    var image = string.Empty;
                   // image = new ODM.Data.DbContextEf.OrderImages().GetOrderImage(Guid.Parse(grdOrders.Rows[intRowIndex].Cells["OrderId"].Value.ToString()));
                    image = Activator.CreateInstance<ODM.Core.Orders.OrderDetails>().GetOrderImage(Guid.Parse(grdOrders.Rows[intRowIndex].Cells["OrderId"].Value.ToString()));
                    if (!string.IsNullOrEmpty(image))
                    {
                        var viewAndSavedOrderImage = Application.OpenForms["ViewAndSaveOrderImage"];
                        if (viewAndSavedOrderImage == null)
                        {
                            ViewAndSaveOrderImage objViewAndSaveOrderImage = new ViewAndSaveOrderImage(image, strNumOrderId);
                            if (objViewAndSaveOrderImage.ShowDialog() != null)
                                grdOrders.Rows[intRowIndex].Cells["Image"].Value = "Image";
                        }
                    }

                }
            }
            if (grdOrders.Columns[e.ColumnIndex].Name.Equals("btnOrderDetails"))
            {
                var orderId = grdOrders.Rows[e.RowIndex].Cells["OrderId"].Value.ToString() ?? string.Empty;
                var orderNumber = (string)grdOrders.Rows[e.RowIndex].Cells["Num Order Id"].Value.ToString() ?? string.Empty;
                var details = Application.OpenForms["OrderDetails"];
                if (!string.IsNullOrEmpty(orderId))
                {
                    if (details == null)
                    {
                        var orderDetails = new ODM.Appliction.ProcessedOrders.OrderDetails(Guid.Parse(orderId), orderNumber);
                        orderDetails.Show();
                    }
                    else
                    {
                        details.Close();
                        var orderDetails = new ODM.Appliction.ProcessedOrders.OrderDetails(Guid.Parse(orderId), orderNumber);
                        orderDetails.Show();
                    }
                }

            }
        }
        private void SearchOrderByDateTypeChanged()
        {
            if (rbtnByReceivedDate.Checked == true)
            {
                rbtnByProcessedDate.Checked = false;
                rbtnByReceivedDate.Checked = true;
                rbtnByAllDates.Checked = false;
            }
            else if (rbtnByProcessedDate.Checked == true)
            {
                rbtnByProcessedDate.Checked = true;
                rbtnByReceivedDate.Checked = false;
                rbtnByAllDates.Checked = false;
            }
            else
            {
                rbtnByAllDates.Checked = true;
                rbtnByProcessedDate.Checked = false;
                rbtnByReceivedDate.Checked = false;
            }
        }

        private void rbtnByReceivedDate_CheckedChanged(object sender, EventArgs e)
        {
            SearchOrderByDateTypeChanged();
        }

        private void rbtnByProcessedDate_CheckedChanged(object sender, EventArgs e)
        {
            SearchOrderByDateTypeChanged();
        }

        private void rbtnByAllDates_CheckedChanged(object sender, EventArgs e)
        {
            SearchOrderByDateTypeChanged();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Cursor currentCursor = this.Cursor;
            this.Cursor = Cursors.WaitCursor;
            currentPageIndex--;
            if (currentPageIndex == 0)
            {
                btnPrevious.Enabled = false;
            }
            btnNext.Enabled = true;
            LoadApiGrid();
            this.Cursor = currentCursor;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Cursor currentCursor = this.Cursor;
            this.Cursor = Cursors.WaitCursor;
            currentPageIndex++;
            if (currentPageIndex == (int)Math.Ceiling(Convert.ToDecimal(objDt.Rows.Count) / pageSize) - 1)
            {
                btnNext.Enabled = false;
            }
            btnPrevious.Enabled = true;

            LoadApiGrid();
            this.Cursor = currentCursor;
        }

        private void txtbox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                if (string.IsNullOrEmpty(txtSearchNoOfRecords.Text))
                {
                    return;
                }

                currentPageIndex = 0;
                btnPrevious.Enabled = false;
                btnPending.Enabled = true;
                int Page;
                if (Int32.TryParse(txtSearchNoOfRecords.Text, out Page))
                {
                    pageSize = Page;

                }
                LoadApiGrid();
            }
            e.Handled = true;
        }

    }
}
