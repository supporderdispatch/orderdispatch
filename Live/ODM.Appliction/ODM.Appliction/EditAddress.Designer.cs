﻿namespace ODM.Appliction
{
    partial class EditAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditAddress));
            this.ddlEditCounrty = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtTown = new System.Windows.Forms.TextBox();
            this.lblTown = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEditEmailAddress = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.lblEditCompany = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.lblEditAddress1 = new System.Windows.Forms.Label();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.lblEditAddress2 = new System.Windows.Forms.Label();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.lblEditPhoneNumber = new System.Windows.Forms.Label();
            this.txtPostalCode = new System.Windows.Forms.TextBox();
            this.lblEditCountry = new System.Windows.Forms.Label();
            this.lblEditPostalCode = new System.Windows.Forms.Label();
            this.txtRegion = new System.Windows.Forms.TextBox();
            this.lblEditRegion = new System.Windows.Forms.Label();
            this.txtAddress3 = new System.Windows.Forms.TextBox();
            this.lblEditAddress3 = new System.Windows.Forms.Label();
            this.lblEditFullName = new System.Windows.Forms.Label();
            this.txtChannelBuyer = new System.Windows.Forms.TextBox();
            this.lblEditChannelBuyerName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ddlEditCounrty
            // 
            this.ddlEditCounrty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEditCounrty.FormattingEnabled = true;
            this.ddlEditCounrty.Location = new System.Drawing.Point(126, 302);
            this.ddlEditCounrty.Name = "ddlEditCounrty";
            this.ddlEditCounrty.Size = new System.Drawing.Size(191, 21);
            this.ddlEditCounrty.TabIndex = 36;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(232, 386);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(85, 23);
            this.btnCancel.TabIndex = 41;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(124, 386);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 23);
            this.btnSave.TabIndex = 39;
            this.btnSave.Text = "OK";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(125, 215);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(192, 20);
            this.txtTown.TabIndex = 31;
            // 
            // lblTown
            // 
            this.lblTown.AutoSize = true;
            this.lblTown.Location = new System.Drawing.Point(81, 218);
            this.lblTown.Name = "lblTown";
            this.lblTown.Size = new System.Drawing.Size(34, 13);
            this.lblTown.TabIndex = 42;
            this.lblTown.Text = "Town";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(125, 41);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(192, 20);
            this.txtEmail.TabIndex = 23;
            // 
            // lblEditEmailAddress
            // 
            this.lblEditEmailAddress.AutoSize = true;
            this.lblEditEmailAddress.Location = new System.Drawing.Point(83, 44);
            this.lblEditEmailAddress.Name = "lblEditEmailAddress";
            this.lblEditEmailAddress.Size = new System.Drawing.Size(32, 13);
            this.lblEditEmailAddress.TabIndex = 25;
            this.lblEditEmailAddress.Text = "Email";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(125, 101);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(192, 20);
            this.txtCompany.TabIndex = 26;
            // 
            // lblEditCompany
            // 
            this.lblEditCompany.AutoSize = true;
            this.lblEditCompany.Location = new System.Drawing.Point(33, 104);
            this.lblEditCompany.Name = "lblEditCompany";
            this.lblEditCompany.Size = new System.Drawing.Size(82, 13);
            this.lblEditCompany.TabIndex = 35;
            this.lblEditCompany.Text = "Company Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(125, 71);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(192, 20);
            this.txtName.TabIndex = 24;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(125, 131);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(192, 20);
            this.txtAddress1.TabIndex = 27;
            // 
            // lblEditAddress1
            // 
            this.lblEditAddress1.AutoSize = true;
            this.lblEditAddress1.Location = new System.Drawing.Point(65, 134);
            this.lblEditAddress1.Name = "lblEditAddress1";
            this.lblEditAddress1.Size = new System.Drawing.Size(54, 13);
            this.lblEditAddress1.TabIndex = 32;
            this.lblEditAddress1.Text = "Address 1";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(125, 158);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(191, 20);
            this.txtAddress2.TabIndex = 29;
            // 
            // lblEditAddress2
            // 
            this.lblEditAddress2.Location = new System.Drawing.Point(64, 161);
            this.lblEditAddress2.Name = "lblEditAddress2";
            this.lblEditAddress2.Size = new System.Drawing.Size(55, 17);
            this.lblEditAddress2.TabIndex = 37;
            this.lblEditAddress2.Text = "Address 2";
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Location = new System.Drawing.Point(124, 335);
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(192, 20);
            this.txtPhoneNumber.TabIndex = 38;
            // 
            // lblEditPhoneNumber
            // 
            this.lblEditPhoneNumber.Location = new System.Drawing.Point(40, 338);
            this.lblEditPhoneNumber.Name = "lblEditPhoneNumber";
            this.lblEditPhoneNumber.Size = new System.Drawing.Size(82, 17);
            this.lblEditPhoneNumber.TabIndex = 43;
            this.lblEditPhoneNumber.Text = "Phone Number";
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Location = new System.Drawing.Point(125, 274);
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Size = new System.Drawing.Size(192, 20);
            this.txtPostalCode.TabIndex = 34;
            // 
            // lblEditCountry
            // 
            this.lblEditCountry.Location = new System.Drawing.Point(74, 304);
            this.lblEditCountry.Name = "lblEditCountry";
            this.lblEditCountry.Size = new System.Drawing.Size(43, 17);
            this.lblEditCountry.TabIndex = 46;
            this.lblEditCountry.Text = "Country";
            // 
            // lblEditPostalCode
            // 
            this.lblEditPostalCode.Location = new System.Drawing.Point(55, 277);
            this.lblEditPostalCode.Name = "lblEditPostalCode";
            this.lblEditPostalCode.Size = new System.Drawing.Size(64, 17);
            this.lblEditPostalCode.TabIndex = 45;
            this.lblEditPostalCode.Text = "Postal Code";
            // 
            // txtRegion
            // 
            this.txtRegion.Location = new System.Drawing.Point(125, 244);
            this.txtRegion.Name = "txtRegion";
            this.txtRegion.Size = new System.Drawing.Size(192, 20);
            this.txtRegion.TabIndex = 33;
            // 
            // lblEditRegion
            // 
            this.lblEditRegion.Location = new System.Drawing.Point(48, 247);
            this.lblEditRegion.Name = "lblEditRegion";
            this.lblEditRegion.Size = new System.Drawing.Size(71, 17);
            this.lblEditRegion.TabIndex = 44;
            this.lblEditRegion.Text = "Region/State";
            // 
            // txtAddress3
            // 
            this.txtAddress3.Location = new System.Drawing.Point(125, 187);
            this.txtAddress3.Name = "txtAddress3";
            this.txtAddress3.Size = new System.Drawing.Size(192, 20);
            this.txtAddress3.TabIndex = 30;
            // 
            // lblEditAddress3
            // 
            this.lblEditAddress3.Location = new System.Drawing.Point(64, 190);
            this.lblEditAddress3.Name = "lblEditAddress3";
            this.lblEditAddress3.Size = new System.Drawing.Size(55, 17);
            this.lblEditAddress3.TabIndex = 40;
            this.lblEditAddress3.Text = "Address 3";
            // 
            // lblEditFullName
            // 
            this.lblEditFullName.AutoSize = true;
            this.lblEditFullName.Location = new System.Drawing.Point(64, 74);
            this.lblEditFullName.Name = "lblEditFullName";
            this.lblEditFullName.Size = new System.Drawing.Size(54, 13);
            this.lblEditFullName.TabIndex = 28;
            this.lblEditFullName.Text = "Full Name";
            // 
            // txtChannelBuyer
            // 
            this.txtChannelBuyer.Location = new System.Drawing.Point(124, 10);
            this.txtChannelBuyer.Name = "txtChannelBuyer";
            this.txtChannelBuyer.Size = new System.Drawing.Size(192, 20);
            this.txtChannelBuyer.TabIndex = 22;
            // 
            // lblEditChannelBuyerName
            // 
            this.lblEditChannelBuyerName.AutoSize = true;
            this.lblEditChannelBuyerName.Location = new System.Drawing.Point(8, 13);
            this.lblEditChannelBuyerName.Name = "lblEditChannelBuyerName";
            this.lblEditChannelBuyerName.Size = new System.Drawing.Size(107, 13);
            this.lblEditChannelBuyerName.TabIndex = 21;
            this.lblEditChannelBuyerName.Text = "Channel Buyer Name";
            // 
            // EditAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 439);
            this.Controls.Add(this.ddlEditCounrty);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtTown);
            this.Controls.Add(this.lblTown);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.lblEditEmailAddress);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.lblEditCompany);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtAddress1);
            this.Controls.Add(this.lblEditAddress1);
            this.Controls.Add(this.txtAddress2);
            this.Controls.Add(this.lblEditAddress2);
            this.Controls.Add(this.txtPhoneNumber);
            this.Controls.Add(this.lblEditPhoneNumber);
            this.Controls.Add(this.txtPostalCode);
            this.Controls.Add(this.lblEditCountry);
            this.Controls.Add(this.lblEditPostalCode);
            this.Controls.Add(this.txtRegion);
            this.Controls.Add(this.lblEditRegion);
            this.Controls.Add(this.txtAddress3);
            this.Controls.Add(this.lblEditAddress3);
            this.Controls.Add(this.lblEditFullName);
            this.Controls.Add(this.txtChannelBuyer);
            this.Controls.Add(this.lblEditChannelBuyerName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditAddress";
            this.Text = "Edit Address";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlEditCounrty;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtTown;
        private System.Windows.Forms.Label lblTown;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEditEmailAddress;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label lblEditCompany;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.Label lblEditAddress1;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label lblEditAddress2;
        private System.Windows.Forms.TextBox txtPhoneNumber;
        private System.Windows.Forms.Label lblEditPhoneNumber;
        private System.Windows.Forms.TextBox txtPostalCode;
        private System.Windows.Forms.Label lblEditCountry;
        private System.Windows.Forms.Label lblEditPostalCode;
        private System.Windows.Forms.TextBox txtRegion;
        private System.Windows.Forms.Label lblEditRegion;
        private System.Windows.Forms.TextBox txtAddress3;
        private System.Windows.Forms.Label lblEditAddress3;
        private System.Windows.Forms.Label lblEditFullName;
        private System.Windows.Forms.TextBox txtChannelBuyer;
        private System.Windows.Forms.Label lblEditChannelBuyerName;

    }
}