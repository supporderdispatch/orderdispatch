﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class myHermesSettings : Form
    {
        string _CourierName = "";
        string _CourierID = "";
        SqlConnection connection;
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        string strDefaultPrinter = "";
        public myHermesSettings()
        {
            InitializeComponent();
        }

        public myHermesSettings(string strCourierName, string strCourierID)
        {
            InitializeComponent();
            _CourierName = strCourierName;
            _CourierID = strCourierID;
            PopulateControls();
            BindGrid();
        }

        private void PopulateControls()
        {
            try
            {
                DataTable ObjdtCourier = new DataTable();
                ObjdtCourier = GlobalVar.GetCourierDetailsByNameOrId(_CourierName, _CourierID);

                if (ObjdtCourier.Rows.Count > 0)
                {
                    txtApiKey.Text = Convert.ToString(ObjdtCourier.Rows[0]["ApiKey"]);
                    txtApiUserId.Text = Convert.ToString(ObjdtCourier.Rows[0]["ApiUserID"]);
                    txtParams.Text = Convert.ToString(ObjdtCourier.Rows[0]["Params"]);
                    txtWeight.Text = Convert.ToString(ObjdtCourier.Rows[0]["DefaultWeight"]);
                    chkIsBoseLogIn.Checked = Convert.ToBoolean(ObjdtCourier.Rows[0]["IsBoseLoggin"]);
                    ddlLabelType.SelectedIndex = 0;
                    ddlPaper.SelectedIndex = 0;

                    ddlPrinter.DataSource = GlobalVar.AttachedPrinters(ref strDefaultPrinter);

                    ddlPrinter.SelectedItem = !string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForMyHermes) ?
                        GlobalVariablesAndMethods.SelectedPrinterNameForMyHermes : strDefaultPrinter;

                    txtMarginLeft.Text = Convert.ToString(ObjdtCourier.Rows[0]["MarginLeft"]);
                    txtMarginTop.Text = Convert.ToString(ObjdtCourier.Rows[0]["MarginTop"]);
                    chkLandscape.Checked = Convert.ToBoolean(ObjdtCourier.Rows[0]["IsLandScape"]);
                    chkInvoiceLandscape.Checked = Convert.ToBoolean(ObjdtCourier.Rows[0]["IsInvoiceLandScape"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindGrid()
        {
            try
            {
                connection = new SqlConnection();
                DataTable objDtGridData = new DataTable();
                DataTable objCourierList = new DataTable();
                connection.ConnectionString = MakeConnection.GetConnection();
                objDtGridData = SqlHelper.ExecuteDataset(connection, "GetmyHermesMappedCouriers").Tables[0];
                objCourierList = SqlHelper.ExecuteDataset(connection, "GetAllCourierToMapWithServices").Tables[0];

                if (objDtGridData.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(objDtGridData.Rows[0]["ID"])))
                {
                    grdMappedServices.Visible = true;
                    grdMappedServices.AllowUserToAddRows = false;
                    grdMappedServices.DataSource = objDtGridData;
                    grdMappedServices.AutoGenerateColumns = false;
                    grdMappedServices.Columns[0].Visible = false;
                    grdMappedServices.Columns[1].Visible = false;
                    grdMappedServices.Columns[2].Visible = false;
                    grdMappedServices.Columns[4].Visible = false;
                    grdMappedServices.Columns[3].ReadOnly = true;
                    grdMappedServices.Columns[3].Width = 370;

                    DataGridViewComboBoxColumn ddlCouriers = new DataGridViewComboBoxColumn();
                    ddlCouriers.HeaderText = "Courier Name";
                    ddlCouriers.Name = "Courier_Name";
                    ddlCouriers.ValueMember = "CourierID";
                    ddlCouriers.DataPropertyName = "MapedCourierId";
                    ddlCouriers.DisplayMember = "CourierName";
                    grdMappedServices.Columns.Add(ddlCouriers);
                    ddlCouriers.DataSource = objCourierList;
                    grdMappedServices.Columns[5].Width = 370;
                    lblMessage.Visible = false;
                }
                else
                {
                    grdMappedServices.Visible = false;
                    lblMessage.Visible = true;
                }
            }
            catch
            {

            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Form objAddService = Application.OpenForms["AddServices"];
            if (objAddService == null)
            {
                AddServices objServices = new AddServices(_CourierName, _CourierID);
                objServices.Show();
            }
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                SaveSettings();
                MapCourierToService();
                GlobalVariablesAndMethods.SelectedPrinterNameForMyHermes = ddlPrinter.SelectedItem.ToString();
                GlobalVar.AddUpdatePrinterName();
            }
        }

        private void MapCourierToService()
        {
            try
            {
                int intResult = 100;
                if (grdMappedServices.Rows.Count > 0)
                {
                    for (int i = 0; i < grdMappedServices.Rows.Count; i++)
                    {
                        string strSelectedCourierID = Convert.ToString(grdMappedServices.Rows[i].Cells["ID"].Value);
                        string strSelectedValue = Convert.ToString(grdMappedServices[5, i].Value);
                        string strServiceName = Convert.ToString(grdMappedServices.Rows[i].Cells["PostalServiceName"].Value);
                        if (strSelectedValue == "")
                        {
                            MessageBox.Show(string.Format("Please select courier for the service {0}", strServiceName));
                            intResult = -1;
                            break;
                        }
                        else
                        {
                            connection = new SqlConnection();
                            Guid MappedID = new Guid(strSelectedValue);
                            Guid ID = new Guid(strSelectedCourierID);
                            Guid DummyID = new Guid();
                            connection.ConnectionString = MakeConnection.GetConnection();
                            SqlParameter[] objParams = new SqlParameter[6];

                            objParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                            objParams[0].Value = ID;

                            objParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                            objParams[1].Value = DummyID;

                            objParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                            objParams[2].Value = DummyID;

                            objParams[3] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                            objParams[3].Value = MappedID;

                            objParams[4] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                            objParams[4].Value = "UPDATE";

                            objParams[5] = new SqlParameter("@Result", SqlDbType.Int);
                            objParams[5].Direction = ParameterDirection.Output;

                            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteServicesToMapCourier", objParams);

                            intResult = Convert.ToInt32(objParams[5].Value);
                        }
                    }
                    if (intResult == 1)
                    {
                        this.Close();
                        MessageBox.Show("Settings Saved Successfully !");
                    }
                }
                else
                {
                    this.Close();
                    MessageBox.Show("Settings Saved Successfully !");
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
            finally
            {
                connection.Close();
            }
        }

        private void DeleteService()
        {
            try
            {
                int RowIndex = 0;
                if (grdMappedServices.CurrentCell != null)
                {
                    RowIndex = Convert.ToInt32(grdMappedServices.CurrentCell.RowIndex);

                    string strSelectedCourierID = Convert.ToString(grdMappedServices.Rows[RowIndex].Cells[0].Value);
                    connection.ConnectionString = MakeConnection.GetConnection();
                    Guid ID = new Guid(strSelectedCourierID);
                    Guid DummyID = new Guid();

                    SqlParameter[] objParams = new SqlParameter[6];

                    objParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                    objParams[0].Value = ID;

                    objParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                    objParams[1].Value = DummyID;

                    objParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                    objParams[2].Value = DummyID;

                    objParams[3] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                    objParams[3].Value = DummyID;

                    objParams[4] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                    objParams[4].Value = "DELETE";

                    objParams[5] = new SqlParameter("@Result", SqlDbType.Int);
                    objParams[5].Direction = ParameterDirection.Output;

                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteServicesToMapCourier", objParams);

                    int intResult = Convert.ToInt32(objParams[5].Value);
                    if (intResult == 2)
                    {
                        BindGrid();
                        MessageBox.Show("Record deleted successfully !");
                    }
                }
                else
                {
                    MessageBox.Show("Please select a record to delete");
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
            finally
            {
                connection.Close();
            }
        }

        private void SaveSettings()
        {
            try
            {
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                Guid CourierID = new Guid(_CourierID);
                Guid ShippingID = new Guid();

                SqlParameter[] objParams = new SqlParameter[16];

                objParams[0] = new SqlParameter("@CourierId", SqlDbType.UniqueIdentifier);
                objParams[0].Value = CourierID;

                objParams[1] = new SqlParameter("@CourierName", SqlDbType.VarChar, 500);
                objParams[1].Value = _CourierName;

                objParams[2] = new SqlParameter("@ApiKey", SqlDbType.VarChar, 50);
                objParams[2].Value = txtApiKey.Text;

                objParams[3] = new SqlParameter("@ApiUserId", SqlDbType.VarChar, 50);
                objParams[3].Value = txtApiUserId.Text;

                objParams[4] = new SqlParameter("@Params", SqlDbType.VarChar, 8000);
                objParams[4].Value = txtParams.Text;

                objParams[5] = new SqlParameter("@Printer", SqlDbType.VarChar, 5000);
                objParams[5].Value = ddlPrinter.SelectedItem;

                objParams[6] = new SqlParameter("@Paper", SqlDbType.VarChar, 500);
                objParams[6].Value = ddlPaper.SelectedItem;

                objParams[7] = new SqlParameter("@MarginLeft", SqlDbType.Decimal);
                objParams[7].Value = txtMarginLeft.Text;

                objParams[8] = new SqlParameter("@MarginTop", SqlDbType.Decimal);
                objParams[8].Value = txtMarginTop.Text;

                objParams[9] = new SqlParameter("@IsLandscape", SqlDbType.Bit);
                objParams[9].Value = chkLandscape.Checked;

                objParams[10] = new SqlParameter("@IsInvoiceLandscape", SqlDbType.Bit);
                objParams[10].Value = chkInvoiceLandscape.Checked;

                objParams[11] = new SqlParameter("@LabelType", SqlDbType.VarChar, 500);
                objParams[11].Value = ddlLabelType.SelectedItem;

                objParams[12] = new SqlParameter("@IsBoseLoggin", SqlDbType.Bit);
                objParams[12].Value = chkIsBoseLogIn.Checked;

                objParams[13] = new SqlParameter("@DefaultWeight", SqlDbType.Decimal);
                objParams[13].Value = txtWeight.Text;

                objParams[14] = new SqlParameter("@ShippingServiceId", SqlDbType.UniqueIdentifier);
                objParams[14].Value = ShippingID;

                objParams[15] = new SqlParameter("@CourierServiceId", SqlDbType.UniqueIdentifier);
                objParams[15].Value = ShippingID;

                SqlHelper.ExecuteNonQuery(connection, "AddUpdateMyHermes", objParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Form frmAddService = Application.OpenForms["AddServices"];
            if (frmAddService != null)
            {
                frmAddService.Close();
            }
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure, you want to delete Postal Service ?", "Delete Postal Service", MessageBoxButtons.YesNo);
            if (result.Equals(DialogResult.Yes))
            {
                DeleteService();
            }
        }

        private bool ValidateControls()
        {
            bool IsValid = true;

            if (string.IsNullOrWhiteSpace(txtApiKey.Text))
            {
                lblApiKeyRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblApiKeyRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtApiUserId.Text))
            {
                lblUserIdRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblUserIdRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtParams.Text))
            {
                lblParamsRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblParamsRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtWeight.Text))
            {
                lblWeightRequired.Visible = true;
                lblWeightInvalid.Visible = false;
                IsValid = false;
            }
            else
            {
                double DefaultWeight = 0.00;
                bool IsValidWeight = double.TryParse(txtWeight.Text, out DefaultWeight);
                if (!IsValidWeight)
                {
                    lblWeightInvalid.Visible = true;
                    IsValid = false;
                }
                else
                {
                    lblWeightInvalid.Visible = false;
                }
                lblWeightRequired.Visible = false;
            }
            return IsValid;
        }
    }
}
