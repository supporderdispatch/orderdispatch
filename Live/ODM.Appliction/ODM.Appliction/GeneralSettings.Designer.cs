﻿namespace ODM.Appliction
{
    partial class GeneralSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneralSettings));
            this.lblInvoicePrinter = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.ddlPrinter = new System.Windows.Forms.ComboBox();
            this.btnDeleteImagesByOrderId = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lblVideoDevice = new System.Windows.Forms.Label();
            this.lblSnapshotResolution = new System.Windows.Forms.Label();
            this.ddlSnapshotResolution = new System.Windows.Forms.ComboBox();
            this.ddlVideoResolution = new System.Windows.Forms.ComboBox();
            this.lblVideoResolution = new System.Windows.Forms.Label();
            this.ddlVideoDevice = new System.Windows.Forms.ComboBox();
            this.btnSaveBinSetting = new System.Windows.Forms.Button();
            this.btnEditBinSetting = new System.Windows.Forms.Button();
            this.txtNumberOfBins = new System.Windows.Forms.TextBox();
            this.lblMaximumNumberOfBins = new System.Windows.Forms.Label();
            this.btnSavePickListPrinterSetting = new System.Windows.Forms.Button();
            this.ddlPickListPrinters = new System.Windows.Forms.ComboBox();
            this.lblPickListPrinter = new System.Windows.Forms.Label();
            this.ddlCN22Printer = new System.Windows.Forms.ComboBox();
            this.lblCN22Printer = new System.Windows.Forms.Label();
            this.btnSaveCN22FormSettings = new System.Windows.Forms.Button();
            this.grdCN22MappedServices = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.grdProcessOrderConditions = new System.Windows.Forms.DataGridView();
            this.btnSaveProcessOrderSettings = new System.Windows.Forms.Button();
            this.btnRefreshProcessOrderSettings = new System.Windows.Forms.Button();
            this.btnAddConditionsToProcessOrder = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdCN22MappedServices)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProcessOrderConditions)).BeginInit();
            this.SuspendLayout();
            // 
            // lblInvoicePrinter
            // 
            this.lblInvoicePrinter.AutoSize = true;
            this.lblInvoicePrinter.Location = new System.Drawing.Point(14, 28);
            this.lblInvoicePrinter.Name = "lblInvoicePrinter";
            this.lblInvoicePrinter.Size = new System.Drawing.Size(163, 13);
            this.lblInvoicePrinter.TabIndex = 0;
            this.lblInvoicePrinter.Text = "Select printer for printing invoices";
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(212, 69);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(115, 26);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save Settings";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ddlPrinter
            // 
            this.ddlPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPrinter.FormattingEnabled = true;
            this.ddlPrinter.Items.AddRange(new object[] {
            "ZDesigner GC420d(EPL)",
            "Brother DCP-1510 series"});
            this.ddlPrinter.Location = new System.Drawing.Point(212, 25);
            this.ddlPrinter.Name = "ddlPrinter";
            this.ddlPrinter.Size = new System.Drawing.Size(256, 21);
            this.ddlPrinter.TabIndex = 1;
            // 
            // btnDeleteImagesByOrderId
            // 
            this.btnDeleteImagesByOrderId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteImagesByOrderId.Location = new System.Drawing.Point(246, 181);
            this.btnDeleteImagesByOrderId.Name = "btnDeleteImagesByOrderId";
            this.btnDeleteImagesByOrderId.Size = new System.Drawing.Size(115, 26);
            this.btnDeleteImagesByOrderId.TabIndex = 16;
            this.btnDeleteImagesByOrderId.Text = "Delete Order Images";
            this.btnDeleteImagesByOrderId.UseVisualStyleBackColor = true;
            this.btnDeleteImagesByOrderId.Click += new System.EventHandler(this.btnDeleteImagesByOrderId_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(110, 181);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 26);
            this.button1.TabIndex = 4;
            this.button1.Text = "Save Settings";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnSaveCameraSettings_Click);
            // 
            // lblVideoDevice
            // 
            this.lblVideoDevice.AutoSize = true;
            this.lblVideoDevice.Location = new System.Drawing.Point(18, 54);
            this.lblVideoDevice.Name = "lblVideoDevice";
            this.lblVideoDevice.Size = new System.Drawing.Size(79, 13);
            this.lblVideoDevice.TabIndex = 15;
            this.lblVideoDevice.Text = "Video Devices:";
            // 
            // lblSnapshotResolution
            // 
            this.lblSnapshotResolution.AutoSize = true;
            this.lblSnapshotResolution.Location = new System.Drawing.Point(243, 54);
            this.lblSnapshotResolution.Name = "lblSnapshotResolution";
            this.lblSnapshotResolution.Size = new System.Drawing.Size(108, 13);
            this.lblSnapshotResolution.TabIndex = 14;
            this.lblSnapshotResolution.Text = "Snapshot Resolution:";
            // 
            // ddlSnapshotResolution
            // 
            this.ddlSnapshotResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSnapshotResolution.FormattingEnabled = true;
            this.ddlSnapshotResolution.Location = new System.Drawing.Point(360, 51);
            this.ddlSnapshotResolution.Name = "ddlSnapshotResolution";
            this.ddlSnapshotResolution.Size = new System.Drawing.Size(148, 21);
            this.ddlSnapshotResolution.TabIndex = 13;
            // 
            // ddlVideoResolution
            // 
            this.ddlVideoResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlVideoResolution.FormattingEnabled = true;
            this.ddlVideoResolution.Location = new System.Drawing.Point(110, 110);
            this.ddlVideoResolution.Name = "ddlVideoResolution";
            this.ddlVideoResolution.Size = new System.Drawing.Size(122, 21);
            this.ddlVideoResolution.TabIndex = 12;
            // 
            // lblVideoResolution
            // 
            this.lblVideoResolution.AutoSize = true;
            this.lblVideoResolution.Location = new System.Drawing.Point(12, 115);
            this.lblVideoResolution.Name = "lblVideoResolution";
            this.lblVideoResolution.Size = new System.Drawing.Size(90, 13);
            this.lblVideoResolution.TabIndex = 11;
            this.lblVideoResolution.Text = "Video Resolution:";
            // 
            // ddlVideoDevice
            // 
            this.ddlVideoDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlVideoDevice.FormattingEnabled = true;
            this.ddlVideoDevice.Location = new System.Drawing.Point(110, 51);
            this.ddlVideoDevice.Name = "ddlVideoDevice";
            this.ddlVideoDevice.Size = new System.Drawing.Size(121, 21);
            this.ddlVideoDevice.TabIndex = 10;
            this.ddlVideoDevice.SelectedIndexChanged += new System.EventHandler(this.ddlVideoDevice_SelectedIndexChanged);
            // 
            // btnSaveBinSetting
            // 
            this.btnSaveBinSetting.Enabled = false;
            this.btnSaveBinSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveBinSetting.Location = new System.Drawing.Point(145, 106);
            this.btnSaveBinSetting.Name = "btnSaveBinSetting";
            this.btnSaveBinSetting.Size = new System.Drawing.Size(115, 26);
            this.btnSaveBinSetting.TabIndex = 3;
            this.btnSaveBinSetting.Text = "Save Settings";
            this.btnSaveBinSetting.UseVisualStyleBackColor = true;
            this.btnSaveBinSetting.Click += new System.EventHandler(this.btnSaveBinSetting_Click);
            // 
            // btnEditBinSetting
            // 
            this.btnEditBinSetting.Enabled = false;
            this.btnEditBinSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditBinSetting.Location = new System.Drawing.Point(267, 58);
            this.btnEditBinSetting.Name = "btnEditBinSetting";
            this.btnEditBinSetting.Size = new System.Drawing.Size(70, 24);
            this.btnEditBinSetting.TabIndex = 2;
            this.btnEditBinSetting.Text = "Edit";
            this.btnEditBinSetting.UseVisualStyleBackColor = true;
            this.btnEditBinSetting.Click += new System.EventHandler(this.btnEditBinSetting_Click);
            // 
            // txtNumberOfBins
            // 
            this.txtNumberOfBins.Location = new System.Drawing.Point(145, 63);
            this.txtNumberOfBins.Name = "txtNumberOfBins";
            this.txtNumberOfBins.ReadOnly = true;
            this.txtNumberOfBins.Size = new System.Drawing.Size(98, 20);
            this.txtNumberOfBins.TabIndex = 1;
            // 
            // lblMaximumNumberOfBins
            // 
            this.lblMaximumNumberOfBins.AutoSize = true;
            this.lblMaximumNumberOfBins.Location = new System.Drawing.Point(15, 64);
            this.lblMaximumNumberOfBins.Name = "lblMaximumNumberOfBins";
            this.lblMaximumNumberOfBins.Size = new System.Drawing.Size(124, 13);
            this.lblMaximumNumberOfBins.TabIndex = 0;
            this.lblMaximumNumberOfBins.Text = "Maximum number of Bins";
            // 
            // btnSavePickListPrinterSetting
            // 
            this.btnSavePickListPrinterSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSavePickListPrinterSetting.Location = new System.Drawing.Point(212, 208);
            this.btnSavePickListPrinterSetting.Name = "btnSavePickListPrinterSetting";
            this.btnSavePickListPrinterSetting.Size = new System.Drawing.Size(115, 26);
            this.btnSavePickListPrinterSetting.TabIndex = 4;
            this.btnSavePickListPrinterSetting.Text = "Save Settings";
            this.btnSavePickListPrinterSetting.UseVisualStyleBackColor = true;
            this.btnSavePickListPrinterSetting.Click += new System.EventHandler(this.btnSavePickListPrinterSetting_Click);
            // 
            // ddlPickListPrinters
            // 
            this.ddlPickListPrinters.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPickListPrinters.FormattingEnabled = true;
            this.ddlPickListPrinters.Items.AddRange(new object[] {
            "ZDesigner GC420d(EPL)",
            "Brother DCP-1510 series"});
            this.ddlPickListPrinters.Location = new System.Drawing.Point(212, 160);
            this.ddlPickListPrinters.Name = "ddlPickListPrinters";
            this.ddlPickListPrinters.Size = new System.Drawing.Size(247, 21);
            this.ddlPickListPrinters.TabIndex = 2;
            // 
            // lblPickListPrinter
            // 
            this.lblPickListPrinter.AutoSize = true;
            this.lblPickListPrinter.Location = new System.Drawing.Point(16, 160);
            this.lblPickListPrinter.Name = "lblPickListPrinter";
            this.lblPickListPrinter.Size = new System.Drawing.Size(161, 13);
            this.lblPickListPrinter.TabIndex = 1;
            this.lblPickListPrinter.Text = "Select printer for printing PickList";
            // 
            // ddlCN22Printer
            // 
            this.ddlCN22Printer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCN22Printer.FormattingEnabled = true;
            this.ddlCN22Printer.Items.AddRange(new object[] {
            "ZDesigner GC420d(EPL)",
            "Brother DCP-1510 series"});
            this.ddlCN22Printer.Location = new System.Drawing.Point(100, 225);
            this.ddlCN22Printer.Name = "ddlCN22Printer";
            this.ddlCN22Printer.Size = new System.Drawing.Size(247, 21);
            this.ddlCN22Printer.TabIndex = 4;
            // 
            // lblCN22Printer
            // 
            this.lblCN22Printer.AutoSize = true;
            this.lblCN22Printer.Location = new System.Drawing.Point(5, 228);
            this.lblCN22Printer.Name = "lblCN22Printer";
            this.lblCN22Printer.Size = new System.Drawing.Size(70, 13);
            this.lblCN22Printer.TabIndex = 2;
            this.lblCN22Printer.Text = "CN 22 Printer";
            // 
            // btnSaveCN22FormSettings
            // 
            this.btnSaveCN22FormSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveCN22FormSettings.Location = new System.Drawing.Point(100, 277);
            this.btnSaveCN22FormSettings.Name = "btnSaveCN22FormSettings";
            this.btnSaveCN22FormSettings.Size = new System.Drawing.Size(115, 26);
            this.btnSaveCN22FormSettings.TabIndex = 1;
            this.btnSaveCN22FormSettings.Text = "Save Settings";
            this.btnSaveCN22FormSettings.UseVisualStyleBackColor = true;
            this.btnSaveCN22FormSettings.Click += new System.EventHandler(this.btnSaveCN22FormSettings_Click);
            // 
            // grdCN22MappedServices
            // 
            this.grdCN22MappedServices.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grdCN22MappedServices.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdCN22MappedServices.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdCN22MappedServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdCN22MappedServices.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdCN22MappedServices.Location = new System.Drawing.Point(6, 35);
            this.grdCN22MappedServices.Name = "grdCN22MappedServices";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdCN22MappedServices.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grdCN22MappedServices.Size = new System.Drawing.Size(360, 127);
            this.grdCN22MappedServices.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 31);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(819, 423);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage1.Controls.Add(this.btnSavePickListPrinterSetting);
            this.tabPage1.Controls.Add(this.ddlPickListPrinters);
            this.tabPage1.Controls.Add(this.ddlPrinter);
            this.tabPage1.Controls.Add(this.lblPickListPrinter);
            this.tabPage1.Controls.Add(this.lblInvoicePrinter);
            this.tabPage1.Controls.Add(this.btnSave);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(811, 397);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Printer Settings";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage2.Controls.Add(this.btnDeleteImagesByOrderId);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.lblSnapshotResolution);
            this.tabPage2.Controls.Add(this.lblVideoDevice);
            this.tabPage2.Controls.Add(this.ddlVideoDevice);
            this.tabPage2.Controls.Add(this.lblVideoResolution);
            this.tabPage2.Controls.Add(this.ddlSnapshotResolution);
            this.tabPage2.Controls.Add(this.ddlVideoResolution);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(811, 397);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Camera Settings";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage3.Controls.Add(this.ddlCN22Printer);
            this.tabPage3.Controls.Add(this.lblCN22Printer);
            this.tabPage3.Controls.Add(this.grdCN22MappedServices);
            this.tabPage3.Controls.Add(this.btnSaveCN22FormSettings);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(811, 397);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "CN 22 Form Settings";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage4.Controls.Add(this.btnSaveBinSetting);
            this.tabPage4.Controls.Add(this.btnEditBinSetting);
            this.tabPage4.Controls.Add(this.txtNumberOfBins);
            this.tabPage4.Controls.Add(this.lblMaximumNumberOfBins);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(811, 397);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Bins Settings";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage5.Controls.Add(this.grdProcessOrderConditions);
            this.tabPage5.Controls.Add(this.btnSaveProcessOrderSettings);
            this.tabPage5.Controls.Add(this.btnRefreshProcessOrderSettings);
            this.tabPage5.Controls.Add(this.btnAddConditionsToProcessOrder);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(811, 397);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Instructions  for ProcessOrderSetting";
            // 
            // grdProcessOrderConditions
            // 
            this.grdProcessOrderConditions.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grdProcessOrderConditions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdProcessOrderConditions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.grdProcessOrderConditions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProcessOrderConditions.DefaultCellStyle = dataGridViewCellStyle5;
            this.grdProcessOrderConditions.Location = new System.Drawing.Point(25, 66);
            this.grdProcessOrderConditions.Name = "grdProcessOrderConditions";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdProcessOrderConditions.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.grdProcessOrderConditions.Size = new System.Drawing.Size(672, 310);
            this.grdProcessOrderConditions.TabIndex = 3;
            // 
            // btnSaveProcessOrderSettings
            // 
            this.btnSaveProcessOrderSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveProcessOrderSettings.Location = new System.Drawing.Point(547, 20);
            this.btnSaveProcessOrderSettings.Name = "btnSaveProcessOrderSettings";
            this.btnSaveProcessOrderSettings.Size = new System.Drawing.Size(150, 23);
            this.btnSaveProcessOrderSettings.TabIndex = 2;
            this.btnSaveProcessOrderSettings.Text = "Save Settings";
            this.btnSaveProcessOrderSettings.UseVisualStyleBackColor = true;
            this.btnSaveProcessOrderSettings.Click += new System.EventHandler(this.btnSaveProcessOrderSettings_Click);
            // 
            // btnRefreshProcessOrderSettings
            // 
            this.btnRefreshProcessOrderSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefreshProcessOrderSettings.Location = new System.Drawing.Point(391, 20);
            this.btnRefreshProcessOrderSettings.Name = "btnRefreshProcessOrderSettings";
            this.btnRefreshProcessOrderSettings.Size = new System.Drawing.Size(150, 23);
            this.btnRefreshProcessOrderSettings.TabIndex = 1;
            this.btnRefreshProcessOrderSettings.Text = "Refresh";
            this.btnRefreshProcessOrderSettings.UseVisualStyleBackColor = true;
            this.btnRefreshProcessOrderSettings.Click += new System.EventHandler(this.btnRefreshProcessOrderSettings_Click);
            // 
            // btnAddConditionsToProcessOrder
            // 
            this.btnAddConditionsToProcessOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddConditionsToProcessOrder.Location = new System.Drawing.Point(25, 20);
            this.btnAddConditionsToProcessOrder.Name = "btnAddConditionsToProcessOrder";
            this.btnAddConditionsToProcessOrder.Size = new System.Drawing.Size(150, 23);
            this.btnAddConditionsToProcessOrder.TabIndex = 0;
            this.btnAddConditionsToProcessOrder.Text = "Add Conditions";
            this.btnAddConditionsToProcessOrder.UseVisualStyleBackColor = true;
            this.btnAddConditionsToProcessOrder.Click += new System.EventHandler(this.btnAddConditionsToProcessOrder_Click);
            // 
            // GeneralSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 461);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "GeneralSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "General Settings";
            this.Load += new System.EventHandler(this.GeneralSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdCN22MappedServices)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProcessOrderConditions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblInvoicePrinter;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox ddlPrinter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblVideoDevice;
        private System.Windows.Forms.Label lblSnapshotResolution;
        private System.Windows.Forms.ComboBox ddlSnapshotResolution;
        private System.Windows.Forms.ComboBox ddlVideoResolution;
        private System.Windows.Forms.Label lblVideoResolution;
        private System.Windows.Forms.ComboBox ddlVideoDevice;
        private System.Windows.Forms.Button btnDeleteImagesByOrderId;
        private System.Windows.Forms.Button btnSaveBinSetting;
        private System.Windows.Forms.Button btnEditBinSetting;
        private System.Windows.Forms.TextBox txtNumberOfBins;
        private System.Windows.Forms.Label lblMaximumNumberOfBins;
        private System.Windows.Forms.Button btnSavePickListPrinterSetting;
        private System.Windows.Forms.ComboBox ddlPickListPrinters;
        private System.Windows.Forms.Label lblPickListPrinter;
        private System.Windows.Forms.ComboBox ddlCN22Printer;
        private System.Windows.Forms.Label lblCN22Printer;
        private System.Windows.Forms.Button btnSaveCN22FormSettings;
        private System.Windows.Forms.DataGridView grdCN22MappedServices;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView grdProcessOrderConditions;
        private System.Windows.Forms.Button btnSaveProcessOrderSettings;
        private System.Windows.Forms.Button btnRefreshProcessOrderSettings;
        private System.Windows.Forms.Button btnAddConditionsToProcessOrder;
    }
}