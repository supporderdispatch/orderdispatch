﻿using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class AllOrders : Form
    {
        public AllOrders()
        {
            InitializeComponent();
            BindGrid();
        }
        private void BindGrid()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:

            SqlConnection connection = new SqlConnection();
            try
            {
                DataTable ObjdtOrders = new DataTable();
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "GetAllOrders";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 200;
                SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
                objAdapt.Fill(ObjdtOrders);

               // ObjdtOrders = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetAllOrders").Tables[0];
                if (ObjdtOrders.Rows.Count > 0)
                {
                    grdAllOrders.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
                    grdAllOrders.AllowUserToAddRows = false;
                    grdAllOrders.Visible = true;
                    grdAllOrders.DataSource = ObjdtOrders;
                    grdAllOrders.Columns["OrderId"].Visible = false;
                    grdAllOrders.Columns["Received Date"].Width = 130;
                    grdAllOrders.Columns["Received Date"].DefaultCellStyle.Format = "dd/MM/yyyy hh:mm:ss tt";
                    grdAllOrders.Columns["Despatch By Date"].DefaultCellStyle.Format = "dd/MM/yyyy hh:mm:ss tt";
                    grdAllOrders.Columns["Despatch By Date"].Width = 130;
                    grdAllOrders.Columns["Shipping Service"].Width = 130;
                    lblError.Visible = false;
                }
                else
                {
                    grdAllOrders.Visible = false;
                    lblError.Visible = true;
                    lblError.Text = "No Record Found !";
                }
                lblTotal.Text = "Total Orders : " + ObjdtOrders.Rows.Count;
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    MessageBox.Show("Error occurred while binding orders", "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}
