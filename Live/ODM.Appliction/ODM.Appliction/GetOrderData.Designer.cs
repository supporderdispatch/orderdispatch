﻿namespace ODM.Appliction
{
    partial class GetOrderData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GetOrderData));
            this.grdOrder = new System.Windows.Forms.DataGridView();
            this.chkscanning = new System.Windows.Forms.CheckBox();
            this.chkfirst = new System.Windows.Forms.CheckBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.btnScan = new System.Windows.Forms.Button();
            this.lblerror = new System.Windows.Forms.Label();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbtnUrgent = new System.Windows.Forms.RadioButton();
            this.rbtnAllOrders = new System.Windows.Forms.RadioButton();
            this.chkConsolidateOrderItems = new System.Windows.Forms.CheckBox();
            this.lblTotalNumOfProcessedOrders = new System.Windows.Forms.Label();
            this.lblLoggedInTime = new System.Windows.Forms.Label();
            this.lblOrdersPerHour = new System.Windows.Forms.Label();
            this.lblPerOrderProcessTime = new System.Windows.Forms.Label();
            this.cmbOccupiedBins = new System.Windows.Forms.ComboBox();
            this.lblPickingBatch = new System.Windows.Forms.Label();
            this.cmbPickingBatches = new System.Windows.Forms.ComboBox();
            this.lblRemaningOrdersCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrder)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdOrder
            // 
            this.grdOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOrder.Location = new System.Drawing.Point(1, 203);
            this.grdOrder.Name = "grdOrder";
            this.grdOrder.Size = new System.Drawing.Size(815, 344);
            this.grdOrder.TabIndex = 5;
            // 
            // chkscanning
            // 
            this.chkscanning.AutoSize = true;
            this.chkscanning.Location = new System.Drawing.Point(26, 131);
            this.chkscanning.Name = "chkscanning";
            this.chkscanning.Size = new System.Drawing.Size(188, 17);
            this.chkscanning.TabIndex = 2;
            this.chkscanning.Text = "Proceed to process after scanning";
            this.chkscanning.UseVisualStyleBackColor = true;
            // 
            // chkfirst
            // 
            this.chkfirst.AutoSize = true;
            this.chkfirst.Location = new System.Drawing.Point(26, 154);
            this.chkfirst.Name = "chkfirst";
            this.chkfirst.Size = new System.Drawing.Size(126, 17);
            this.chkfirst.TabIndex = 3;
            this.chkfirst.Text = "Process first one only";
            this.chkfirst.UseVisualStyleBackColor = true;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(152, 24);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(210, 20);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.TextAlignChanged += new System.EventHandler(this.txtSearch_TextAlignChanged);
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(377, 21);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(79, 21);
            this.btnFind.TabIndex = 1;
            this.btnFind.Text = "Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(326, 154);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(100, 22);
            this.btnScan.TabIndex = 4;
            this.btnScan.Text = "Proceed To Scan";
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // lblerror
            // 
            this.lblerror.AutoSize = true;
            this.lblerror.Location = new System.Drawing.Point(26, 174);
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(0, 13);
            this.lblerror.TabIndex = 10;
            this.lblerror.Visible = false;
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.Location = new System.Drawing.Point(20, 29);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(115, 13);
            this.lblBarcode.TabIndex = 11;
            this.lblBarcode.Text = "Enter Barcode Number";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbtnUrgent);
            this.panel1.Controls.Add(this.rbtnAllOrders);
            this.panel1.Location = new System.Drawing.Point(9, 101);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 25);
            this.panel1.TabIndex = 12;
            // 
            // rbtnUrgent
            // 
            this.rbtnUrgent.AutoSize = true;
            this.rbtnUrgent.Location = new System.Drawing.Point(145, 3);
            this.rbtnUrgent.Name = "rbtnUrgent";
            this.rbtnUrgent.Size = new System.Drawing.Size(160, 17);
            this.rbtnUrgent.TabIndex = 15;
            this.rbtnUrgent.TabStop = true;
            this.rbtnUrgent.Text = "Dispatch Only Urgent Orders";
            this.rbtnUrgent.UseVisualStyleBackColor = true;
            this.rbtnUrgent.CheckedChanged += new System.EventHandler(this.rbtnUrgent_CheckedChanged);
            // 
            // rbtnAllOrders
            // 
            this.rbtnAllOrders.AutoSize = true;
            this.rbtnAllOrders.Location = new System.Drawing.Point(17, 3);
            this.rbtnAllOrders.Name = "rbtnAllOrders";
            this.rbtnAllOrders.Size = new System.Drawing.Size(115, 17);
            this.rbtnAllOrders.TabIndex = 14;
            this.rbtnAllOrders.TabStop = true;
            this.rbtnAllOrders.Text = "Dispatch All Orders";
            this.rbtnAllOrders.UseVisualStyleBackColor = true;
            this.rbtnAllOrders.CheckedChanged += new System.EventHandler(this.rbtnAllOrders_CheckedChanged);
            // 
            // chkConsolidateOrderItems
            // 
            this.chkConsolidateOrderItems.AutoSize = true;
            this.chkConsolidateOrderItems.Location = new System.Drawing.Point(612, 97);
            this.chkConsolidateOrderItems.Name = "chkConsolidateOrderItems";
            this.chkConsolidateOrderItems.Size = new System.Drawing.Size(138, 17);
            this.chkConsolidateOrderItems.TabIndex = 14;
            this.chkConsolidateOrderItems.Text = "Consolidate Order Items";
            this.chkConsolidateOrderItems.UseVisualStyleBackColor = true;
            // 
            // lblTotalNumOfProcessedOrders
            // 
            this.lblTotalNumOfProcessedOrders.AutoSize = true;
            this.lblTotalNumOfProcessedOrders.Location = new System.Drawing.Point(609, 12);
            this.lblTotalNumOfProcessedOrders.Name = "lblTotalNumOfProcessedOrders";
            this.lblTotalNumOfProcessedOrders.Size = new System.Drawing.Size(0, 13);
            this.lblTotalNumOfProcessedOrders.TabIndex = 16;
            // 
            // lblLoggedInTime
            // 
            this.lblLoggedInTime.AutoSize = true;
            this.lblLoggedInTime.Location = new System.Drawing.Point(609, 27);
            this.lblLoggedInTime.Name = "lblLoggedInTime";
            this.lblLoggedInTime.Size = new System.Drawing.Size(0, 13);
            this.lblLoggedInTime.TabIndex = 17;
            // 
            // lblOrdersPerHour
            // 
            this.lblOrdersPerHour.AutoSize = true;
            this.lblOrdersPerHour.Location = new System.Drawing.Point(609, 43);
            this.lblOrdersPerHour.Name = "lblOrdersPerHour";
            this.lblOrdersPerHour.Size = new System.Drawing.Size(0, 13);
            this.lblOrdersPerHour.TabIndex = 18;
            // 
            // lblPerOrderProcessTime
            // 
            this.lblPerOrderProcessTime.AutoSize = true;
            this.lblPerOrderProcessTime.Location = new System.Drawing.Point(609, 58);
            this.lblPerOrderProcessTime.Name = "lblPerOrderProcessTime";
            this.lblPerOrderProcessTime.Size = new System.Drawing.Size(0, 13);
            this.lblPerOrderProcessTime.TabIndex = 19;
            // 
            // cmbOccupiedBins
            // 
            this.cmbOccupiedBins.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbOccupiedBins.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cmbOccupiedBins.DropDownHeight = 175;
            this.cmbOccupiedBins.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOccupiedBins.FormattingEnabled = true;
            this.cmbOccupiedBins.IntegralHeight = false;
            this.cmbOccupiedBins.ItemHeight = 40;
            this.cmbOccupiedBins.Location = new System.Drawing.Point(612, 120);
            this.cmbOccupiedBins.Name = "cmbOccupiedBins";
            this.cmbOccupiedBins.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmbOccupiedBins.Size = new System.Drawing.Size(192, 46);
            this.cmbOccupiedBins.TabIndex = 20;
            this.cmbOccupiedBins.SelectedIndexChanged += new System.EventHandler(this.cmbOccupiedBins_SelectedIndexChanged);
            // 
            // lblPickingBatch
            // 
            this.lblPickingBatch.AutoSize = true;
            this.lblPickingBatch.Location = new System.Drawing.Point(23, 59);
            this.lblPickingBatch.Name = "lblPickingBatch";
            this.lblPickingBatch.Size = new System.Drawing.Size(73, 13);
            this.lblPickingBatch.TabIndex = 21;
            this.lblPickingBatch.Text = "Picking Batch";
            // 
            // cmbPickingBatches
            // 
            this.cmbPickingBatches.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cmbPickingBatches.DropDownHeight = 100;
            this.cmbPickingBatches.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPickingBatches.DropDownWidth = 210;
            this.cmbPickingBatches.FormattingEnabled = true;
            this.cmbPickingBatches.IntegralHeight = false;
            this.cmbPickingBatches.ItemHeight = 27;
            this.cmbPickingBatches.Location = new System.Drawing.Point(152, 55);
            this.cmbPickingBatches.Name = "cmbPickingBatches";
            this.cmbPickingBatches.Size = new System.Drawing.Size(210, 33);
            this.cmbPickingBatches.TabIndex = 22;
            this.cmbPickingBatches.SelectedIndexChanged += new System.EventHandler(this.cmbPickingBatches_SelectedIndexChanged);
            // 
            // lblRemaningOrdersCount
            // 
            this.lblRemaningOrdersCount.AutoSize = true;
            this.lblRemaningOrdersCount.Location = new System.Drawing.Point(374, 68);
            this.lblRemaningOrdersCount.Name = "lblRemaningOrdersCount";
            this.lblRemaningOrdersCount.Size = new System.Drawing.Size(100, 13);
            this.lblRemaningOrdersCount.TabIndex = 23;
            this.lblRemaningOrdersCount.Text = "0 Orders Remaining";
            // 
            // GetOrderData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 547);
            this.Controls.Add(this.lblRemaningOrdersCount);
            this.Controls.Add(this.cmbPickingBatches);
            this.Controls.Add(this.lblPickingBatch);
            this.Controls.Add(this.cmbOccupiedBins);
            this.Controls.Add(this.lblPerOrderProcessTime);
            this.Controls.Add(this.lblOrdersPerHour);
            this.Controls.Add(this.lblLoggedInTime);
            this.Controls.Add(this.lblTotalNumOfProcessedOrders);
            this.Controls.Add(this.chkConsolidateOrderItems);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblBarcode);
            this.Controls.Add(this.lblerror);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.chkfirst);
            this.Controls.Add(this.chkscanning);
            this.Controls.Add(this.grdOrder);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GetOrderData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Get Order Data";
            ((System.ComponentModel.ISupportInitialize)(this.grdOrder)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdOrder;
        private System.Windows.Forms.CheckBox chkscanning;
        private System.Windows.Forms.CheckBox chkfirst;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Label lblerror;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbtnUrgent;
        private System.Windows.Forms.RadioButton rbtnAllOrders;
        private System.Windows.Forms.CheckBox chkConsolidateOrderItems;
        private System.Windows.Forms.Label lblTotalNumOfProcessedOrders;
        private System.Windows.Forms.Label lblLoggedInTime;
        private System.Windows.Forms.Label lblOrdersPerHour;
        private System.Windows.Forms.Label lblPerOrderProcessTime;
        private System.Windows.Forms.ComboBox cmbOccupiedBins;
        private System.Windows.Forms.Label lblPickingBatch;
        private System.Windows.Forms.ComboBox cmbPickingBatches;
        private System.Windows.Forms.Label lblRemaningOrdersCount;
    }
}