﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Entities.InvoiceTemplate;
using ODM.Data.Util;
using ODM.Data.DbContext;
using mshtml;
using System.Text.RegularExpressions;
using System.IO;
using ODM.Appliction.Extensions;


namespace ODM.Appliction
{
    public partial class InvoiceTemplateEditor : Form
    {
        private IHTMLDocument2 doc;
        private IHTMLDocument2 blockEditor;
        private string WebEditorInitialText;
        private string DefaultPageSize;
        private PageOrientation DefaultPageOrientation;
        private string DefaultTemplateName;
        private bool IsTemplateEnabled;
        private bool IsTemplatePdfTemplate;
        private bool IsTemplateConditionalTemplate;
        private bool IsNewTemplate;
        private bool IsAutoPrint;
        private Guid TemplateId;
        private static string ExectionPath = Path.GetDirectoryName(Application.ExecutablePath);
        private string SampleHtml = @"<!doctype html><html><head></head><link href = 'https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css'rel = 'stylesheet'><script src = 'https://code.jquery.com/jquery-1.10.2.js'></script><script src = 'https://code.jquery.com/ui/1.10.4/jquery-ui.js'></script><script>$(function() {$('img').resizable();</script><style>span { display:inline-block;margin:2px;} img {height:auto;}</style><body style='padding:2px;margin:2px;'><div id='content' class='resizable-element' style='width:280px;min-height:280px;'>&nbsp;</div></body></html>";
        private string SampleHtmlTabular = @"<!doctype html><html><head></head><link href = 'https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css'rel = 'stylesheet'><script src = 'https://code.jquery.com/jquery-1.10.2.js'></script><script src = 'https://code.jquery.com/ui/1.10.4/jquery-ui.js'></script><script>$(function() {$('img').resizable();</script><style>span { display:inline-block;margin:2px;}</style><body style='padding:2px;margin:2px;'><table id='content'><tbody><tr></tr></tbody></table></body></html>";
        private bool AutoInsertLineBreak = false;
        private bool KeyValuePairingOfInvoiceFields = false;
        private string KeyValueSeprator = string.Empty;
        private BlockType DefaultBlockEditorType = BlockType.Simple;
        private Guid PrintConditionId;
        private bool ColorFilling = false;
        private bool DataBlockEditing = false;
        private string DefaultInvoicePrinter;

        public InvoiceTemplateEditor()
        {
            InitializeComponent();
            RegisterEvents();
            InitializeDefaultValues();
            InitializeCommons();
        }

        private void InitializeCommons()
        {
            SetEditorPanelWidth();
            InitializeTemplateSaving();
            PrepareWebBasedEditor();
            RegisterWebEditorEvents();
            SetupFontComboBox();
            SetupFontSizeComboBox();
            LoadPrinters();
            txtTemplateName.Focus();
        }

        private void SetdefaultBodyFont()
        {
            if (cmbPageOrientations.SelectedItem == null || cmbViewportView.SelectedItem == null)
            {
                return;
            }
            if (cmbPageOrientations.SelectedItem.ToString().Equals(PageOrientation.Landscape.ToString()) || cmbViewportView.SelectedItem.ToString().Equals(ViewportView.Original))
            {
                wbTemplateEditor.Document.Body.Style = "font-size:9px;";
            }
        }

        private void SetEditorPanelWidth()
        {
            var windowWidth = this.Width;
            pnlInvoiceTemplate.Width = (int)Math.Ceiling(windowWidth * 0.60);
            pnlInvoiceTemplate.Left = pnlDataBlockEditorControl.Left - pnlInvoiceTemplate.Width;
            lblTemplateSavedToolbar.Width = pnlInvoiceTemplate.Left;
            pnlInvoiceTemplate.AutoScroll = true;
        }


        public InvoiceTemplateEditor(Guid templateId)
        {
            Cursor = Cursors.WaitCursor;
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            RegisterEvents();
            InitializeTemplateEditorFor(templateId);
            InitializeCommons();
            Cursor = Cursors.Default;
        }
        public string GetBase64(Image image)
        {
            byte[] bytearray;
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                bytearray = ms.ToArray();
            }
            return Convert.ToBase64String(bytearray);
        }
        public Image AddGridlines(int gridSize, Color color)
        {
            var bmp = new Bitmap(pnlInvoiceTemplate.Width, pnlInvoiceTemplate.Height);
            Image img = (Image)bmp;
            Graphics g = Graphics.FromImage(img);

            int x = gridSize;
            int y = gridSize;
            var pen = new Pen(color, 2) { Width = 2.0f, DashCap = System.Drawing.Drawing2D.DashCap.Round, DashPattern = new float[] { 1.0F, 1.0F, 1.0F, 1.0F } };

            while (x < img.Width)
            {
                g.DrawLine(pen, x, 0, x, img.Height);

                x += gridSize;

            }

            while (y < img.Height)
            {

                g.DrawLine(pen, 0, y, img.Width, y);

                y += gridSize;

            }

            return img;

        }
        private void InitializeDefaultValues()
        {
            var backGround = GetBase64(AddGridlines(25, Color.White));
            WebEditorInitialText = @"<!DOCTYPE HTML><html><head>
<LINK rel=stylesheet href='https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css'></LINK>
<SCRIPT src='https://code.jquery.com/jquery-1.10.2.js'></SCRIPT>
<SCRIPT src='https://code.jquery.com/ui/1.10.4/jquery-ui.js'></SCRIPT>
<SCRIPT>  
$(function() {

            function MakeContentDraggableAndResizable() {
                $('.ui-resizable-handle').remove();
                $('.draggable-element').draggable({
                    start: function() {
                        $(this).addClass('animating');
                    },
                    stop: function() {
                        $(this).removeClass('animating');
                        $(this).css({
                            position: 'absolute'
                        });
                        var l = (100 * parseFloat($(this).position().left / parseFloat($(this).parent().width()))) + '%';
                        var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + '%';
                        $(this).css('left', l);
                        $(this).css('top', t);

                    }
                });

                $('.resizable-element').resizable({
                    start: function() {
                        $(this).addClass('animating');
                    },
                    stop: function(e, ui) {
                        var parent = ui.element.parent();
                        var maxWidth = parent.width();
                        if(ui.element.width()>maxWidth) {
                           ui.element.width(maxWidth-5);
                        }
                        if (!$(this).is('th')) {
                            ui.element.css({
                                width: ui.element.width() / parent.width() * 100 + '%',
                                height: ui.element.height() / parent.height() * 100 + '%'
                            });
                        
                        }

                        if ($(this).parent().hasClass('draggable-element')) {
                            var dragElement = $(this).parent('draggable-element');
                            var l = (100 * parseFloat($(dragElement).position().left / parseFloat($(dragElement).parent().width()))) + '%';
                            var t = (100 * parseFloat($(dragElement).position().top / parseFloat($(dragElement).parent().height()))) + '%';
                            $(dragElement).css('left', l);
                            $(dragElement).css('top', t);
                        }
                        if($(this).hasClass('draggable-element')){
                         $(this).css({
                            position: 'absolute'
                         });
                        var l = (100 * parseFloat($(this).position().left / parseFloat($(this).parent().width()))) + '%';
                        var t = (100 * parseFloat($(this).position().top / parseFloat($(this).parent().height()))) + '%';
                        $(this).css('left', l);
                        $(this).css('top', t);
                        }
                        $(this).removeClass('animating');
                    }
                });
            }
            MakeContentDraggableAndResizable();
        });
    </SCRIPT>

    <STYLE>
        @page {
            margin: 0pt;
            margin-right: 0pt !important;
            padding: 0pt;
        }
        
        th {
            display: inline-block;
            margin: 2px;
        }
        
        html,
        body {
            width: 100%;
            height: 100%;
            margin: 2px;
            padding: 0px;
            word-wrap: break-word;
        }
        
        body {
            overflow: hidden;
        }
        
        div,
        span,
        image,
        th {
            transition: width 1s ease-in-out;
        }
        
        div,
        image,
        th {
            display: inline-block;
            margin: 2px;
        }
        span{
        display: block;
            margin: 1px;
        }
        .ui-widget-content {
            background-color: gray;
        }
        
        img {
            display: inline-block;
        }
        
        .animating {
            border: 1px dotted blue;
        }
    </STYLE>
</HEAD>
<BODY></BODY>
</HTML>";
            DefaultPageSize = "A4";
            wbTemplateEditor.ScrollBarsEnabled = false;
            DefaultPageOrientation = PageOrientation.Portrait;
            IsTemplateEnabled = true;
            IsTemplateConditionalTemplate = true;
            IsTemplatePdfTemplate = true;
            IsAutoPrint = false;
            PrintConditionId = Guid.NewGuid();
            DefaultTemplateName = string.Empty;
            IsNewTemplate = true;
            LoadEditorDefault();
            UpdateInvoiceDataFields(null, null);
        }
        private void InitializeTemplateEditorFor(Guid templateId)
        {
            Cursor = Cursors.WaitCursor;
            var templateData = Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>()
                .GetInvoiceTemplate(templateId) as InvoiceTemplate;

            WebEditorInitialText = templateData.LayoutDesign;
            DefaultPageSize = templateData.PageSize;
            PrintConditionId = templateData.PrintContidtionID;
            DefaultPageOrientation = (PageOrientation)templateData.PageOrientation;
            IsTemplateEnabled = templateData.IsEnabled;
            IsTemplateConditionalTemplate = templateData.IsConditional;
            IsTemplatePdfTemplate = templateData.IsPdfTemplate;
            IsNewTemplate = false;
            IsAutoPrint = templateData.IsAutoPrint;
            TemplateId = templateId;
            DefaultTemplateName = templateData.TemplateName;
            LoadEditorDefault();
            DefaultInvoicePrinter = templateData.SelectedPrinter;
            UpdateInvoiceDataFields(null, null);
        }
        private void PrepareWebBasedEditor()
        {
            wbTemplateEditor.DocumentText = WebEditorInitialText;
            doc = wbTemplateEditor.Document.DomDocument as IHTMLDocument2;
            doc.designMode = "On";

            wbTemplateEditor.DocumentCompleted += (sender, e) =>
            {
                SetdefaultBodyFont();
                wbTemplateEditor.Document.Body.KeyUp += Body_KeyUp;
            };
        }

        void Body_KeyUp(object sender, HtmlElementEventArgs e)
        {
            if (wbTemplateEditor.Document.Body.GetElementsByTagName("a").Count > 0)
            {
                foreach (HtmlElement anchor in wbTemplateEditor.Document.GetElementsByTagName("a"))
                {
                    if (string.IsNullOrEmpty(anchor.Style))
                        anchor.Style = "color:black;text-decoration:none;";
                }
            }
        }

        private void RegisterWebEditorEvents()
        {
            this.Click += InvoiceTemplateEditor_Click;
            wbTemplateEditor.GotFocus += wbTemplateEditor_GotFocus;
            if (wbTemplateEditor.Version.Major >= 9)
                wbTemplateEditor.Document.Click += Document_Click;
            grdInvoiceFields.CellContentClick += grdInvoiceFields_CellContentClick;
            this.Resize += ResizeEditorAccToPageSettings;
        }
        private void InitializeTemplateSaving()
        {
            if (!IsNewTemplate)
            {
                btnSaveAndExit.Click -= btnSaveAndExit_Click;
                btnSaveAndExit.Click += UpdateInvoicetemplate;
            }
        }
        private void grdInvoiceFields_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (wbTemplateEditor.Document == null)
                btnAddBlock.PerformClick();

            grdInvoiceFields.EndEdit();
            if (e.RowIndex >= 0 & e.ColumnIndex == grdInvoiceFields.Columns["Inserted"].Index)
            {
                var row = grdInvoiceFields.Rows[e.RowIndex];
                DataGridViewCheckBoxCell insertedChecked = (DataGridViewCheckBoxCell)row.Cells["Inserted"];
                if (Convert.ToBoolean(insertedChecked.Value))
                {
                    var invoiceField = GetInvoiceField(row);
                    InsertCheckedFieldInDataBlock(invoiceField);
                }
                else
                {
                    var invoiceField = GetInvoiceField(row);
                    RemoveUnCheckedFieldFromDataBlock(invoiceField);
                }

            }
        }

        private InvoiceField GetInvoiceField(DataGridViewRow row)
        {


            var dataType = row.Cells["DataType"].Value;
            if (dataType == null)
            {
                dataType = "Text";
            }
            return new InvoiceField()
            {
                Field = row.Cells["Fields"].Value.ToString(),
                DisplayName = row.Cells["DisplayName"].Value.ToString(),
                Inserted = Convert.ToBoolean(row.Cells["Inserted"].Value),
                DecimalPlaces = row.Cells["DecimalPlaces"].Value.ToString(),
                DataType = (CustomDataTypes)Enum.Parse(typeof(CustomDataTypes), dataType.ToString())
            };

        }
        private void RemoveUnCheckedFieldFromDataBlock(InvoiceField invoiceField)
        {
            if (wbDataBlockEditor.Document.Body != null)
            {
                if (wbDataBlockEditor.Document.Body.InnerHtml.Contains(invoiceField.Field))
                {
                    if ((BlockType)Enum.Parse(typeof(BlockType), cmbBlockType.SelectedItem.ToString()) == BlockType.Simple)
                    {
                        foreach (HtmlElement element in wbDataBlockEditor.Document.Body.Children)
                        {
                            foreach (HtmlElement el in element.Children)
                            {
                                if (el.InnerHtml != null)
                                {
                                    if (el.InnerHtml.Contains(invoiceField.Field))
                                    {
                                        el.OuterHtml = string.Empty;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var tableId = wbDataBlockEditor.Document.GetElementsByTagName("table")[0].Id;
                        var tableRow = wbDataBlockEditor.Document.GetElementsByTagName("tr")[0];
                        var tableRowStyle = tableRow.Style;
                        var tableHeaders = new List<string>();
                        foreach (HtmlElement el in tableRow.Children)
                        {
                            if (!string.IsNullOrEmpty(el.InnerText))
                            {
                                if (!el.InnerHtml.Contains(invoiceField.DisplayName))
                                {
                                    tableHeaders.Add(el.InnerHtml);
                                }
                            }

                        }
                        wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtmlTabular);
                        wbDataBlockEditor.Document.GetElementById("content").Id = tableId;
                        var newRow = wbDataBlockEditor.Document.GetElementsByTagName("tr")[0];
                        newRow.Style = tableRowStyle;
                        newRow.SetAttribute("class", "resizable-element draggable-element");
                        foreach (var tableheader in tableHeaders)
                        {
                            var th = wbDataBlockEditor.Document.CreateElement("th");
                            th.InnerHtml = tableheader;
                            th.SetAttribute("class", "ui-widget-content resizable-element");
                            newRow.AppendChild(th);
                        }
                        wbDataBlockEditor.Document.GetElementsByTagName("tbody")[0].AppendChild(newRow);
                        var newHtml = wbDataBlockEditor.Document.GetElementsByTagName("html")[0].OuterHtml;
                        wbDataBlockEditor.Document.OpenNew(false).Write(newHtml);
                    }
                }
            }
        }
        private void InsertCheckedFieldInDataBlock(InvoiceField invoiceField)
        {
            var displayStyle = AutoInsertLineBreak ? ";display:block;width:100%;" : ";display:inline-block;width:auto;";
            if (wbDataBlockEditor.Document.Body != null)
            {
                if (DefaultBlockEditorType == BlockType.Tabular)
                {
                    HtmlElement element = wbDataBlockEditor.Document.GetElementsByTagName("table")[0];
                    if (wbDataBlockEditor.Document.GetElementsByTagName("tr").Count == 0)
                        return;

                    var tr = wbDataBlockEditor.Document.GetElementsByTagName("tr")[0];
                    if (cmbDataBlockBorder.SelectedItem != null)
                        tr.Style = string.Format("border:2px {0} black;", GetBorder(cmbDataBlockBorder.SelectedItem.ToString()).ToString());

                    HtmlElement th = wbDataBlockEditor.Document.CreateElement("th");
                    th.InnerText = invoiceField.DisplayName;
                    th.SetAttribute("class", "ui-widget-content resizable-element");
                    th.SetAttribute("actualFont", "100%");
                    th.SetAttribute("contentEditable", "true");
                    th.SetAttribute("dataType", invoiceField.DataType.ToString());
                    th.SetAttribute("decimalPlaces", invoiceField.DecimalPlaces);
                    th.SetAttribute("field", invoiceField.Field);
                    if (cmbDataBlockBorder.SelectedItem != null)
                    {
                        if (GetBorder(cmbDataBlockBorder.SelectedItem.ToString()) != CustomBorder.None)
                        {
                            th.Style = string.Format("text-align:center;background-color: inherit !important;border:1px {0} black;", GetBorder(cmbDataBlockBorder.SelectedItem.ToString()).ToString());
                        }
                        else
                        {
                            th.Style = "text-align:center;";
                        }
                    }
                    else
                    {
                        th.Style = "text-align:center;";
                    }
                    tr.AppendChild(th);
                }
                else
                {
                    HtmlElement element = null;
                    if (KeyValuePairingOfInvoiceFields)
                    {
                        displayStyle = AutoInsertLineBreak ? ";display:block;" : ";display:inline;";
                        element = wbDataBlockEditor.Document.CreateElement("div");
                        element.SetAttribute("contentEditable", "true");
                        element.Style = string.Format("border:1px {0} black;padding:1px" + displayStyle, GetBorder(cmbDataBlockBorder.SelectedItem.ToString()).ToString());
                        element.InnerText = invoiceField.Field + " " + KeyValueSeprator + " ";
                        HtmlElement subel = wbDataBlockEditor.Document.CreateElement("span");
                        subel.SetAttribute("contentEditable", "true");
                        subel.InnerText = "_" + invoiceField.Field;
                        subel.SetAttribute("dataType", invoiceField.DataType.ToString());
                        subel.Style = "display:inline;";
                        subel.SetAttribute("decimalPlaces", invoiceField.DecimalPlaces);
                        element.SetAttribute("class", "resizable-element");
                        subel.SetAttribute("class", "ui-widget-content");
                        element.AppendChild(subel);
                    }
                    else
                    {
                        element = wbDataBlockEditor.Document.CreateElement("span");
                        var border = (string)cmbDataBlockBorder.SelectedItem ?? "None";
                        element.Style = string.Format("border:1px {0} black;padding:1px" + displayStyle, GetBorder(border).ToString());
                        element.InnerText = "_" + invoiceField.Field;
                        element.SetAttribute("class", "ui-widget-content resizable-element");
                        element.SetAttribute("dataType", invoiceField.DataType.ToString());
                        element.SetAttribute("decimalPlaces", invoiceField.DecimalPlaces);
                        element.SetAttribute("contentEditable", "true");
                    }
                    var parent = wbDataBlockEditor.Document.GetElementById("content");
                    if (parent == null)
                    {
                        if (wbDataBlockEditor.Document.Body == null)
                        {
                            wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
                        }
                        else
                        {
                            parent = wbDataBlockEditor.Document.Body.FirstChild;
                        }
                    }

                    parent.AppendChild(element);
                }

                //if (AutoInsertLineBreak)
                //{
                //    var el = wbDataBlockEditor.Document.CreateElement("br");
                //    wbDataBlockEditor.Document.GetElementById("content").AppendChild(el);
                //}
            }
        }
        private void RegisterEvents()
        {
            this.cmbPageSizes.SelectedIndexChanged += ResizeEditorAccToPageSettings;
            this.cmbPageOrientations.SelectedIndexChanged += ResizeEditorAccToPageSettings;
            this.cmbViewportView.SelectedIndexChanged += ResizeEditorAccToPageSettings;
            this.chkOrderDetails.CheckedChanged += UpdateInvoiceDataFields;
            this.chkOrderItem.CheckedChanged += UpdateInvoiceDataFields;
            this.chkSenderDetail.CheckedChanged += UpdateInvoiceDataFields;
            this.chkCustomerDetail.CheckedChanged += UpdateInvoiceDataFields;
            this.chkGridLines.CheckedChanged += chkGridLines_CheckedChanged;
            this.GridLinesSqureSize.Scroll += GridLinesSqureSize_Scroll;
            this.MouseHover += InvoiceTemplateEditor_MouseHover;
            this.wbDataBlockEditor.PreviewKeyDown += wbDataBlockEditor_PreviewKeyDown;
        }

        void wbDataBlockEditor_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (wbDataBlockEditor.Document != null && wbDataBlockEditor.Document.Body != null)
            {
                wbDataBlockEditor.Document.Body.KeyUp -= wbDataBody_KeyUp;
                wbDataBlockEditor.Document.Body.KeyUp += wbDataBody_KeyUp;
            }
        }

        private void wbDataBody_KeyUp(object sender, HtmlElementEventArgs e)
        {
            if (wbDataBlockEditor.Document.Body.GetElementsByTagName("a").Count > 0)
            {
                foreach (HtmlElement anchor in wbDataBlockEditor.Document.GetElementsByTagName("a"))
                {
                    if (string.IsNullOrEmpty(anchor.Style))
                        anchor.Style = "color:black;text-decoration:none;";
                }
            }
        }

        private void InvoiceTemplateEditor_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }
        private void LoadEditorDefault()
        {
            cmbPageSizes.Items.Clear();
            cmbPageOrientations.Items.Clear();
            cmbViewportView.Items.Clear();

            cmbPageOrientations.Items.AddRange(Enum.GetNames(typeof(PageOrientation)));
            cmbPageSizes.Items.AddRange(Enum.GetNames(typeof(Pagesize)));
            AddCreateNewPageSizeOptionToPageSizes();
            cmbViewportView.Items.AddRange(Enum.GetNames(typeof(ViewportView)));
            cmbPageOrientations.SelectedIndex = ((int)DefaultPageOrientation) - 1;
            cmbPageSizes.SelectedItem = DefaultPageSize;
            txtTemplateName.Text = DefaultTemplateName;
            cmbViewportView.SelectedIndex = 0;
            chkPdfTemplate.Checked = IsTemplatePdfTemplate;
            chkTemplateEnabled.Checked = IsTemplateEnabled;
            chkConditional.Checked = IsTemplateConditionalTemplate;
            chkAutoPrint.Checked = IsAutoPrint;

        }
        private void AddCreateNewPageSizeOptionToPageSizes()
        {
            cmbPageSizes.Items.Insert(0, "Create New");
            var customPageSizes = (ODM.Core.InvoiceTemplate.PageSettings.GetAllCustomPageSizes() as DataTable).AsEnumerable()
                .Select(x => x.Field<string>("Name")).ToList();
            if (customPageSizes.Count > 0)
            {
                foreach (var pageSizeName in customPageSizes)
                {
                    cmbPageSizes.Items.Add(pageSizeName);
                }
            }
        }

        private void LoadPrinters()
        {
            string strDefaultPrinter = string.Empty;
            cmbInvoiceTemplatePrinter.DataSource = Activator.CreateInstance<GlobalVariablesAndMethods>()
                .AttachedPrinters(ref strDefaultPrinter);
            cmbInvoiceTemplatePrinter.SelectedItem = DefaultInvoicePrinter;
        }
        private void ResizeEditorAccToPageSettings(object sender, object e)
        {
            if (cmbPageSizes.SelectedItem != null && cmbPageSizes.SelectedItem.ToString().Equals("Create New"))
            {
                ShowNewPageSizePrompt(string.Empty);
                cmbPageSizes.SelectedItem = DefaultPageSize;
                return;
            }

            SetEditorPanelWidth();
            if (cmbViewportView.SelectedItem != null & cmbPageOrientations.SelectedItem != null & cmbPageSizes.SelectedItem != null)
            {
                var selectedViewportView = (ViewportView)Enum.Parse(typeof(ViewportView), cmbViewportView.SelectedItem.ToString());
                var selectedPageOrientation = (PageOrientation)Enum.Parse(typeof(PageOrientation), cmbPageOrientations.SelectedItem.ToString());
                var dimension = Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>()
                    .GetPageSizeDimensions(cmbPageSizes.SelectedItem.ToString());
                var dHeight = (int)Math.Ceiling(dimension.Height);
                var dWidth = (int)Math.Ceiling(dimension.width);

                if (selectedViewportView == ViewportView.Streched)
                {
                    if (cmbPageSizes.SelectedItem != null)
                    {
                        if (selectedPageOrientation == PageOrientation.Portrait)
                        {
                            var valueRequiredToReachPanelWidth = pnlInvoiceTemplate.Width - 25 - dWidth;
                            float percentageToIncrease = (100 + ((float)valueRequiredToReachPanelWidth / (float)dWidth) * 100) / 100;
                            wbTemplateEditor.Height = (int)Math.Ceiling(dHeight * percentageToIncrease);
                            wbTemplateEditor.Width = (int)Math.Ceiling(dWidth * percentageToIncrease);
                        }
                        if (selectedPageOrientation == PageOrientation.Landscape)
                        {
                            if (pnlInvoiceTemplate.Width > dHeight)
                            {
                                var valueRequiredToReachPanelWidth = pnlInvoiceTemplate.Width - dHeight;
                                float percentageToIncrease = (100 + ((float)(valueRequiredToReachPanelWidth) / (float)(dHeight)) * 100) / 100;
                                wbTemplateEditor.Height = (int)Math.Ceiling(dWidth * percentageToIncrease);
                                wbTemplateEditor.Width = (int)Math.Ceiling(dHeight * (1 + percentageToIncrease));
                            }
                            else
                            {
                                wbTemplateEditor.Height = dWidth;
                                wbTemplateEditor.Width = dHeight;
                            }
                        }
                    }
                }
                if (selectedViewportView == ViewportView.Original)
                {
                    if (cmbPageSizes.SelectedItem != null)
                    {
                        if (selectedPageOrientation == PageOrientation.Portrait)
                        {
                            wbTemplateEditor.Height = dHeight;
                            wbTemplateEditor.Width = dWidth;
                        }
                        if (selectedPageOrientation == PageOrientation.Landscape)
                        {
                            wbTemplateEditor.Height = dWidth;
                            wbTemplateEditor.Width = dHeight;
                        }
                    }
                }
            }
            if (wbTemplateEditor.Document != null && wbTemplateEditor.Document.Body != null)
            {

                float widthFraction = 0.0f;
                var selectedPageOrientation = (PageOrientation)Enum.Parse(typeof(PageOrientation), cmbPageOrientations.SelectedItem.ToString());

                if (selectedPageOrientation == PageOrientation.Portrait)
                    widthFraction = ((1382 / (1382 - wbTemplateEditor.Width)) * 50);
                else
                    widthFraction = 100;

                if (wbTemplateEditor.Document.Body.Style != null)
                {
                    wbTemplateEditor.Document.Body.Style += ";font-size:" + widthFraction + "%;";
                }
                else
                {
                    wbTemplateEditor.Document.Body.Style = "font-size:" + widthFraction + "%;";
                }
                foreach (HtmlElement element in wbTemplateEditor.Document.GetElementsByTagName("TH"))
                {
                    var elStyle = element.Style;

                    if (string.IsNullOrEmpty(element.GetAttribute("actualFont")))
                    {
                        element.SetAttribute("actualFont", "100%");
                    }
                    var actualFont = element.GetAttribute("actualFont").Split('%')[0].Trim();

                    if (!(!string.IsNullOrEmpty(element.Style) && element.Style.ToLower().Contains("font-size")))
                    {

                        element.Style = elStyle + ";font-size:" + actualFont + "%;";
                    }
                    else
                    {

                        element.Style = elStyle + ";font-size:" + Convert.ToDouble(actualFont) * (widthFraction / 100) + "%;";
                    }
                }
                foreach (HtmlElement element in wbTemplateEditor.Document.GetElementsByTagName("FONT"))
                {
                    var strSize = string.IsNullOrEmpty(element.GetAttribute("size")) ? "3" : element.GetAttribute("size");
                    var size = Convert.ToInt32(strSize);
                    if (String.IsNullOrEmpty(element.GetAttribute("originalSize")))
                    {
                        element.SetAttribute("originalSize", size.ToString());
                    }
                    var originalSize = element.GetAttribute("originalSize");
                    var sizeDrop = (Convert.ToInt32(originalSize) * widthFraction) / 100;
                    element.SetAttribute("size", ((int)Math.Round(sizeDrop)).ToString());
                }
                SetdefaultBodyFont();
            }
        }
        private void UpdateInvoiceDataFields(object sender, object e)
        {
            var invoiceFields = new DataTable();
            var invoiceFieldsList = new List<string>();
            invoiceFields.Columns.Add("Fields");
            invoiceFields.Columns.Add("DisplayName");
            invoiceFields.Columns.Add("Inserted");
            invoiceFields.Columns.Add("DataType");
            invoiceFields.Columns.Add("DecimalPlaces");

            invoiceFields.Columns["Inserted"].DataType = typeof(bool);

            Action<List<string>> addListToDataTable = (fieldList) =>
            {
                foreach (var field in fieldList)
                {
                    invoiceFieldsList.Add(field);
                }
            };

            if (chkOrderDetails.Checked)
            {
                addListToDataTable(VirtualTables.Order);
            }
            if (chkOrderItem.Checked)
            {
                addListToDataTable(VirtualTables.OrderItem);
            }
            if (chkSenderDetail.Checked)
            {
                var senderDetails = new List<string>();
                VirtualTables.SenderAddress.ForEach(x =>
                {
                    senderDetails.Add("Sender-" + x);
                });
                addListToDataTable(senderDetails);
            }
            if (chkCustomerDetail.Checked)
            {
                var customerDetails = new List<string>();
                VirtualTables.CustomerAddress.ForEach(x =>
                {
                    customerDetails.Add("Customer-" + x);
                });
                addListToDataTable(customerDetails);
            }
            invoiceFieldsList.Sort();
            var invoiceData = string.Empty;
            if (wbDataBlockEditor.Document != null)
            {
                var htmlElements = wbDataBlockEditor.Document.GetElementsByTagName("html");
                if (htmlElements.Count > 0)
                {
                    invoiceData = htmlElements[0].OuterHtml; ;
                }
            }
            foreach (var field in invoiceFieldsList)
            {
                var row = invoiceFields.NewRow();
                row["Fields"] = field;
                row["Inserted"] = invoiceData.Contains(field) ? 1 : 0;
                row["DisplayName"] = !Convert.ToBoolean(row["inserted"].ToString()) ? field : GetCellValueUsingAttribute(row["fields"].ToString(), invoiceData);
                row["DataType"] = !Convert.ToBoolean(row["inserted"].ToString()) ? CustomDataTypes.Text.ToString() : GetCustomDataForCellFromAttribute(row["DisplayName"].ToString(), invoiceData, "dataType");
                row["DecimalPlaces"] = !Convert.ToBoolean(row["inserted"].ToString()) ? "none" : GetCustomDataForCellFromAttribute(row["fields"].ToString(), invoiceData, "decimalPlaces");
                invoiceFields.Rows.Add(row);
            }
            invoiceFields.AcceptChanges();
            grdInvoiceFields.DataSource = invoiceFields;
            grdInvoiceFields.RowHeadersVisible = false;
            grdInvoiceFields.AllowUserToAddRows = false;
            grdInvoiceFields.Columns["Fields"].MinimumWidth = 140;
            grdInvoiceFields.Columns["Inserted"].Width = 60;
            grdInvoiceFields.Columns["DataType"].Visible = false;

            if (grdInvoiceFields.Columns["cellDataType"] == null)
            {
                DataGridViewComboBoxColumn cellDataType = new DataGridViewComboBoxColumn();
                cellDataType.HeaderText = "Data Type";
                cellDataType.Name = "cellDataType";
                cellDataType.FlatStyle = FlatStyle.Flat;
                cellDataType.DataSource = Enum.GetNames(typeof(CustomDataTypes));
                cellDataType.DataPropertyName = "DataType";
                cellDataType.Width = 90;
                grdInvoiceFields.Columns.Add(cellDataType);
            }
            grdInvoiceFields.Columns["DisplayName"].ReadOnly = false;
            grdInvoiceFields.Columns["DecimalPlaces"].ReadOnly = false;

            grdInvoiceFields.Columns["Fields"].DisplayIndex = 0;
            grdInvoiceFields.Columns["DisplayName"].DisplayIndex = 1;
            grdInvoiceFields.Columns["Inserted"].DisplayIndex = 2;
            grdInvoiceFields.Columns["cellDataType"].DisplayIndex = 3;
            grdInvoiceFields.Columns["DecimalPlaces"].DisplayIndex = 4;

            if (!chkOrderItem.Checked)
            {
                grdInvoiceFields.Columns["DisplayName"].Visible = false;
                // grdInvoiceFields.Columns["DecimalPlaces"].Visible = false;
            }
            else
            {
                grdInvoiceFields.Columns["DisplayName"].Visible = true;
                //grdInvoiceFields.Columns["DecimalPlaces"].Visible = true;
            }
            grdInvoiceFields.DoubleBuffered(true);
        }

        private string GetCustomDataForCellFromAttribute(string field, string invoiceData, string attribute)
        {
            string result = string.Empty;
            if (invoiceData.Contains(field))
            {
                HtmlElementCollection elementCollection = null;
                var selectedBlockType = (BlockType)Enum.Parse(typeof(BlockType), cmbBlockType.SelectedItem.ToString());
                if (selectedBlockType == BlockType.Tabular)
                {
                    elementCollection = wbDataBlockEditor.Document.GetElementsByTagName("TH");
                }
                else
                {
                    elementCollection = wbDataBlockEditor.Document.GetElementsByTagName("SPAN");
                }
                foreach (HtmlElement element in elementCollection)
                {
                    if (element != null)
                    {
                        if (element.OuterHtml.ToString().Contains(field))
                        {
                            result = element.GetAttribute(attribute);
                            break;
                        }
                    }
                }
            }
            return result;
        }
        private string GetCellValueUsingAttribute(string field, string invoiceData)
        {
            string result = field;
            if (invoiceData.Contains(field))
            {
                HtmlElementCollection elementCollection = null;
                var selectedBlockType = (BlockType)Enum.Parse(typeof(BlockType), cmbBlockType.SelectedItem.ToString());
                if (selectedBlockType == BlockType.Tabular)
                {
                    elementCollection = wbDataBlockEditor.Document.GetElementsByTagName("TH");
                }
                else
                {
                    elementCollection = wbDataBlockEditor.Document.GetElementsByTagName("SPAN");
                }
                foreach (HtmlElement element in elementCollection)
                {
                    if (element != null)
                    {
                        if (element.OuterHtml.ToString().Contains(field))
                        {
                            if (element.GetAttribute("field").Equals(field))
                            {
                                result = element.InnerText;
                                break;
                            }

                        }
                    }
                }
            }
            return result;
        }
        private void btnSaveAndExit_Click(object sender, EventArgs e)
        {
            if (SaveTemplate())
            {
                this.Close();
            }
        }

        private bool SaveTemplate()
        {

            chkGridLines.Checked = false;
            if (string.IsNullOrEmpty(txtTemplateName.Text))
            {
                MessageBox.Show("Please enter Template name");
                return false;
            }
            var invoiceTemplate = new InvoiceTemplate();
            invoiceTemplate.TemplateName = txtTemplateName.Text;
            invoiceTemplate.LayoutDesign = GetHtmlWithOrignialBounds(wbTemplateEditor.Document.GetElementsByTagName("HTML")[0].OuterHtml);
            invoiceTemplate.PageOrientation = (int)Enum.Parse(typeof(PageOrientation), cmbPageOrientations.SelectedItem.ToString());
            invoiceTemplate.PageSize = cmbPageSizes.SelectedItem.ToString();
            invoiceTemplate.PrintContidtionID = PrintConditionId;
            invoiceTemplate.IsEnabled = chkTemplateEnabled.Checked;
            invoiceTemplate.IsPdfTemplate = chkPdfTemplate.Checked;
            invoiceTemplate.IsConditional = chkConditional.Checked;
            invoiceTemplate.IsAutoPrint = chkAutoPrint.Checked;
            invoiceTemplate.IPv4Address = new GlobalVariablesAndMethods().GetSystemIpv4Address;
            invoiceTemplate.SelectedPrinter = (string)cmbInvoiceTemplatePrinter.SelectedItem ?? GlobalVariablesAndMethods.SelectedPrinterNameForInvoice;
            TemplateId = Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().AddInvoiceTemplate(invoiceTemplate);
            IsNewTemplate = false;
            InitializeTemplateSaving();
            return true;
        }

        private void UpdateInvoicetemplate(object sender, EventArgs e)
        {
            UpdateTemplate();
            this.Close();
        }

        private bool UpdateTemplate()
        {
            chkGridLines.Checked = false;
            var invoiceTemplate = new InvoiceTemplate();
            invoiceTemplate.TemplateId = TemplateId;
            invoiceTemplate.TemplateName = txtTemplateName.Text;
            invoiceTemplate.LayoutDesign = GetHtmlWithOrignialBounds(wbTemplateEditor.Document.GetElementsByTagName("HTML")[0].OuterHtml);
            invoiceTemplate.PageOrientation = (int)Enum.Parse(typeof(PageOrientation), cmbPageOrientations.SelectedItem.ToString());
            invoiceTemplate.PageSize = cmbPageSizes.SelectedItem.ToString();
            invoiceTemplate.PrintContidtionID = PrintConditionId;
            invoiceTemplate.IsAutoPrint = chkAutoPrint.Checked;
            invoiceTemplate.IsEnabled = chkTemplateEnabled.Checked;
            invoiceTemplate.IsPdfTemplate = chkPdfTemplate.Checked;
            invoiceTemplate.IsConditional = chkConditional.Checked;
            invoiceTemplate.IPv4Address = new GlobalVariablesAndMethods().GetSystemIpv4Address;
            invoiceTemplate.SelectedPrinter = Convert.ToString(cmbInvoiceTemplatePrinter.SelectedItem ?? cmbInvoiceTemplatePrinter.Items[0]);
            Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().UpdateInvoiceTemplate(invoiceTemplate);
            return true;
        }

        private string GetHtmlWithOrignialBounds(string invoiceHtml)
        {
            var tempWb = new WebBrowser();
            tempWb.ScriptErrorsSuppressed = true;
            tempWb.DocumentText = SampleHtml;
            tempWb.Document.OpenNew(false).Write(invoiceHtml);

            if (tempWb.Document.Body != null)
            {
                if (tempWb.Document.Body.Style != null)
                {
                    tempWb.Document.Body.Style += ";font-size:100%;";
                }
                else
                {
                    tempWb.Document.Body.Style = "font-size:100%;";
                }
                foreach (HtmlElement element in tempWb.Document.GetElementsByTagName("TH"))
                {
                    var actualFont = element.GetAttribute("actualFont") ?? "100%";
                    var elementStyle = element.Style;
                    element.Style = elementStyle + ";font-size:" + actualFont;
                }
                foreach (HtmlElement element in tempWb.Document.GetElementsByTagName("FONT"))
                {
                    var strSize = string.IsNullOrEmpty(element.GetAttribute("size")) ? "3" : element.GetAttribute("size");
                    var size = Convert.ToInt32(strSize);
                    if (String.IsNullOrEmpty(element.GetAttribute("originalSize")))
                    {
                        element.SetAttribute("originalSize", size.ToString());
                    }
                    var originalSize = element.GetAttribute("originalSize");
                    element.SetAttribute("size", originalSize.ToString());
                }
            }

            return tempWb.Document.GetElementsByTagName("HTML")[0].OuterHtml;
        }

        private void btnExitWithoutSave_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnAddBlock_Click(object sender, EventArgs e)
        {
            PrepareBorderCombox();
            EnableBlockForEditing();
            cmbDataBlockBorder.Enabled = true;
            cmbDataBlockBorder.SelectedIndex = 0;
            btnAddBlockToTemplate.Enabled = true;
            btnAutoInsertLineBreak.Enabled = true;
            btnResetBlock.Enabled = true;
            grdInvoiceFields.Enabled = true;
            grdInvoiceFields.DefaultCellStyle.ForeColor = Color.Black;
            btnKeyValueParing.Enabled = true;
            PrepareKeyValueSeprator();
            LoadBlockEditorTypes();
        }

        private void PrepareKeyValueSeprator()
        {
            cmbKeyValueSeprator.Items.Clear();
            cmbKeyValueSeprator.Items.Add("[ ](Single Space)");
            cmbKeyValueSeprator.Items.Add("[:](Colons)");
            cmbKeyValueSeprator.Items.Add("[-](Dash)");
            cmbKeyValueSeprator.Items.Add("[=](Equal to)");
            cmbKeyValueSeprator.SelectedIndex = 0;
            cmbKeyValueSeprator.Enabled = true;
        }
        #region wbtemplate editor
        private void btnBold_Click(object sender, EventArgs e)
        {
            wbTemplateEditor.Document.ExecCommand("Bold", false, null);
        }
        private void btnItalic_Click(object sender, EventArgs e)
        {
            wbTemplateEditor.Document.ExecCommand("Italic", false, null);
        }
        private void btnUnderLine_Click(object sender, EventArgs e)
        {
            wbTemplateEditor.Document.ExecCommand("Underline", false, null);
        }

        private void SetAlignmentOfBlock(string alignment)
        {
            var selectedBlock = wbTemplateEditor.Document.ActiveElement;
            if (selectedBlock.FirstChild != null && selectedBlock.FirstChild.TagName.ToLower().Contains("table"))
            {

                foreach (HtmlElement th in selectedBlock.GetElementsByTagName("th"))
                {
                    if (th.InnerText.Contains((string)(((IHTMLTxtRange)doc.selection.createRange()).text) ?? ""))
                    {
                        var thStyle = th.Style;
                        th.Style = thStyle + ";text-align:" + alignment + ";";
                    }
                }
            }
            else
            {
                var selectedBlockStyle = String.IsNullOrEmpty(selectedBlock.Style) ? "" : selectedBlock.Style + ";";
                wbTemplateEditor.Document.ActiveElement.Style = selectedBlockStyle + "text-align:" + alignment + ";";
            }
        }
        private void btnJustifyLeft_Click(object sender, EventArgs e)
        {
            var selectedBlock = wbTemplateEditor.Document.ActiveElement;
            if (selectedBlock.TagName.Equals("DIV"))
            {
                SetAlignmentOfBlock("left");
            }
            else
            {
                wbTemplateEditor.Document.ExecCommand("JustifyLeft", false, null);
            }

        }
        private void btnJustifyCenter_Click(object sender, EventArgs e)
        {
            var selectedBlock = wbTemplateEditor.Document.ActiveElement;
            if (selectedBlock.TagName.Equals("DIV"))
            {
                SetAlignmentOfBlock("center");

            }
            else
            {
                wbTemplateEditor.Document.ExecCommand("JustifyCenter", false, null);
            }

        }
        private void btnJustifyFull_Click(object sender, EventArgs e)
        {
            var selectedBlock = wbTemplateEditor.Document.ActiveElement;
            if (selectedBlock.TagName.Equals("DIV"))
            {
                SetAlignmentOfBlock("center");

            }
            else
            {
                wbTemplateEditor.Document.ExecCommand("JustifyCenter", false, null);
            }
        }
        private void btnJustifyRight_Click(object sender, EventArgs e)
        {
            var selectedBlock = wbTemplateEditor.Document.ActiveElement;
            if (selectedBlock.TagName.Equals("DIV"))
            {
                SetAlignmentOfBlock("right");

            }
            else
            {
                wbTemplateEditor.Document.ExecCommand("JustifyRight", false, null);
            }
        }
        private void btnOutdent_Click(object sender, EventArgs e)
        {
            wbTemplateEditor.Document.ExecCommand("Outdent", false, null);
        }
        private void btnIndent_Click(object sender, EventArgs e)
        {
            wbTemplateEditor.Document.ExecCommand("Indent", false, null);
        }
        private void btnCut_Click(object sender, EventArgs e)
        {
            wbTemplateEditor.Document.ExecCommand("Cut", false, null);
        }
        private void btnCopy_Click(object sender, EventArgs e)
        {
            wbTemplateEditor.Document.ExecCommand("Copy", false, null);
        }
        private void btnPaste_Click(object sender, EventArgs e)
        {
            wbTemplateEditor.Document.ExecCommand("Paste", false, null);
        }
        private void SetupFontComboBox()
        {
            AutoCompleteStringCollection ac = new AutoCompleteStringCollection();
            foreach (FontFamily fam in FontFamily.Families)
            {
                fontComboBox.Items.Add(fam.Name);
                ac.Add(fam.Name);
            }
            fontComboBox.SelectedIndexChanged += fontComboBox_SelectedIndexChanged;
            fontComboBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            fontComboBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            fontComboBox.AutoCompleteCustomSource = ac;
            fontComboBox.SelectedItem = "Calibri";
        }
        private void fontComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            FontFamily ff;
            try
            {
                ff = new FontFamily(fontComboBox.Text);
            }
            catch (Exception)
            {
                fontComboBox.Text = FontName.GetName(0);
                return;
            }
            FontName = ff;
        }
        [Browsable(false)]
        public FontFamily FontName
        {
            get
            {
                string name = doc.queryCommandValue("FontName") as string;
                if (name == null) return null;
                return new FontFamily(name);
            }
            set
            {
                if (value != null)
                    wbTemplateEditor.Document.ExecCommand("FontName", false, value.Name);
            }
        }
        private void SetupFontSizeComboBox()
        {
            fontSizeComboBox.Items.Clear();
            for (int x = 1; x <= 7; x++)
            {
                fontSizeComboBox.Items.Add(x.ToString());
            }
            fontSizeComboBox.SelectedIndexChanged += fontSizeComboBox_SelectedIndexChanged;
            fontSizeComboBox.SelectedIndex = 0;
        }

        private void fontSizeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (wbTemplateEditor.Document.ActiveElement != null)
            {
                var docs = wbTemplateEditor.Document.DomDocument as IHTMLDocument2;
                IHTMLSelectionObject selectedItem = docs.selection;
                IHTMLTxtRange selected = (IHTMLTxtRange)selectedItem.createRange();
                var selectedText = ((string)selected.text ?? string.Empty).Trim();
                var fontSize = Convert.ToInt32(fontSizeComboBox.SelectedItem.ToString()) * 50;
                var activeElement = wbTemplateEditor.Document.ActiveElement;

                if (string.IsNullOrEmpty(selectedText))
                {
                    return;
                }
                Action replaceCurrentTextWithFontSpan = () =>
                {
                    var document = wbTemplateEditor.Document.GetElementsByTagName("HTML")[0].OuterHtml;
                    wbTemplateEditor.DocumentText = document.Replace(selectedText, string.Format("<span style='display:inline;font-size:{0}%;'>{1}</span>", fontSize.ToString(), selectedText));
                };

                if (activeElement.InnerText.Trim().Equals(selectedText))
                {
                    if (activeElement.TagName.Trim().ToLower().Equals("body"))
                    {
                        replaceCurrentTextWithFontSpan();
                    }
                    else if (activeElement.FirstChild != null && activeElement.FirstChild.TagName.ToLower().Contains("table"))
                    {
                        if (selectedText.Trim().Equals(activeElement.FirstChild.InnerText.Trim()))
                        {
                            foreach (HtmlElement element in activeElement.FirstChild.FirstChild.FirstChild.Children)
                            {
                                var elementStyle = element.Style;
                                element.Style = string.IsNullOrEmpty(elementStyle) ? "font-size:" + fontSize + "%; !important;" :
                                    elementStyle + ";font-size:" + fontSize + "% !important;";

                                element.SetAttribute("actualFont", fontSize + "%");

                            }
                        }
                    }
                    else
                    {
                        var activeElementCurrentStyle = activeElement.Style;
                        activeElement.Style = activeElementCurrentStyle + ";font-size:" + fontSize + "% !important;";
                    }
                }

                if (activeElement.InnerText.Contains(selectedText) && !string.IsNullOrEmpty(selectedText) && !activeElement.InnerText.Trim().Equals(selectedText))
                {

                    if (wbTemplateEditor.Document.ActiveElement.Children.Count > 0)
                    {
                        var elementNotFoundInAnyChild = true;

                        if (activeElement.InnerText.Trim().Contains(selectedText))
                        {
                            if (activeElement.TagName.Trim().ToLower().Equals("body"))
                            {
                                replaceCurrentTextWithFontSpan();
                            }
                            if (activeElement.FirstChild != null && activeElement.FirstChild.TagName.ToLower().Contains("table"))
                            {
                                //iterating table>tbody>tr>th
                                foreach (HtmlElement element in activeElement.FirstChild.FirstChild.FirstChild.Children)
                                {
                                    if (element.InnerText.Trim().Equals(selectedText))
                                    {
                                        var elementStyle = element.Style;
                                        element.Style = string.IsNullOrEmpty(elementStyle) ? "font-size:" + fontSize + "%; !important;" :
                                            elementStyle + ";font-size:" + fontSize + "% !important;";

                                        element.SetAttribute("actualFont", fontSize + "%");
                                    }
                                }
                            }
                            else
                            {
                                foreach (HtmlElement element in activeElement.Children)
                                {
                                    if (element.InnerText != null && element.InnerText.Trim().Equals(selectedText))
                                    {
                                        var elementStyle = element.Style;
                                        element.Style = string.IsNullOrEmpty(elementStyle) ? "font-size:" + fontSize + "%; !important;" :
                                            elementStyle + ";font-size:" + fontSize + "% !important;";
                                        return;
                                    }
                                }

                                elementNotFoundInAnyChild = false;
                                var document = wbTemplateEditor.Document.GetElementsByTagName("HTML")[0].OuterHtml;
                                wbTemplateEditor.DocumentText = document.Replace(selectedText, string.Format("<span style='display:inline;font-size:{0}%;'>{1}</span>", fontSize.ToString(), selectedText));
                            }
                            return;
                        }

                        foreach (HtmlElement element in wbTemplateEditor.Document.ActiveElement.Children)
                        {
                            Action setElementFontSize = () =>
                            {
                                elementNotFoundInAnyChild = false;
                                if (element.GetAttribute("class") == null)
                                {
                                    element.OuterHtml = element.OuterHtml.Replace(selectedText, string.Format("<span style='display:inline;font-size:{0}%;'>{1}</span><br/>", fontSize.ToString(), selectedText));
                                }
                                else
                                {
                                    element.Style = "font-size:" + fontSize + "%;";
                                }

                            };
                            if (element.OuterHtml.Contains(selectedText))
                            {
                                setElementFontSize.Invoke();
                            }
                            else if (element.InnerText != null && element.InnerText.Contains(selectedText))
                            {
                                setElementFontSize.Invoke();
                            }
                        }
                        if (elementNotFoundInAnyChild)
                        {
                            replaceCurrentTextWithFontSpan();
                        }
                    }
                    else
                    {
                        replaceCurrentTextWithFontSpan();
                    }
                }

            }
        }


        private void Document_Click(object sender, HtmlElementEventArgs e)
        {
            MakeContentEditable();
            if (DataBlockEditing)
            {
                EditSelectedDataBlock(sender, e);
            }
        }

        private void EditSelectedDataBlock(object sender, HtmlElementEventArgs e)
        {
            try
            {
                MakeContentEditable();

                Point ScreenCoord = new Point(MousePosition.X, MousePosition.Y);
                Point BrowserCoord = wbTemplateEditor.PointToClient(ScreenCoord);
                HtmlElement elem = wbTemplateEditor.Document.GetElementFromPoint(BrowserCoord);

                if (elem.Id != null || elem.Parent.TagName.Contains("TR") || elem.Parent.TagName.Contains("TBODY") || elem.Parent.TagName.Contains("TABLE") || elem.Parent.TagName.Contains("Table"))
                {
                    cmbBlockType.Visible = true;
                    cmbBlockType.Enabled = true;
                    LoadBlockEditorTypes();
                    var id = elem.Id;
                    if (string.IsNullOrEmpty(id))
                    {
                        if (string.IsNullOrEmpty(elem.Parent.Id))
                        {
                            if (string.IsNullOrEmpty(elem.Parent.Parent.Id))
                            {
                                if (string.IsNullOrEmpty(elem.Parent.Parent.Parent.Id))
                                {
                                    id = elem.Parent.Parent.Parent.Parent.Id;
                                }
                                else
                                {
                                    id = elem.Parent.Parent.Parent.Id;
                                }
                            }
                            else
                            {
                                id = elem.Parent.Parent.Id;
                            }
                        }
                        else
                        {
                            id = elem.Parent.Id;
                        }
                    }
                    if (elem.Parent.TagName.Contains("TR") || elem.Parent.TagName.Contains("TBODY") || elem.Parent.TagName.Contains("TABLE") || elem.Parent.TagName.Contains("Table"))
                    {

                        cmbBlockType.SelectedIndex = 1;
                        wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtmlTabular);
                    }
                    else
                    {
                        cmbBlockType.SelectedIndex = 0;
                        wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
                    }

                    var content = wbDataBlockEditor.Document.GetElementById("content");
                    content.OuterHtml = wbTemplateEditor.Document.GetElementById(id).OuterHtml;
                    if (wbTemplateEditor.Document.GetElementById(id).Style != null)
                        wbDataBlockEditor.Document.GetElementById(id).Style = "";
                    var updatedHtml = wbDataBlockEditor.Document.GetElementsByTagName("html")[0].OuterHtml;
                    wbDataBlockEditor.Document.OpenNew(false).Write(updatedHtml);

                    btnUpdateBlock.Visible = true;
                    btnCancelUpdateBlock.Visible = true;
                }
                else
                {
                    if (wbDataBlockEditor.Document == null)
                        wbDataBlockEditor.DocumentText = SampleHtml;
                    wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
                    ResetBlockEditorButtonsToDefault();
                    btnUpdateBlock.Visible = false;
                    btnCancelUpdateBlock.Visible = false;
                }
                UpdateInvoiceDataFields(null, null);
                grdInvoiceFields.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void InvoiceTemplateEditor_Click(object sender, EventArgs e)
        {
            if (wbTemplateEditor.Document.Body != null)
            {
                wbTemplateEditor.Document.Body.SetAttribute("contentEditable", "false");
            }
            if (wbDataBlockEditor.Document != null)
            {
                if (wbDataBlockEditor.Document.Body != null)
                    btnSaveAndExit.Focus();
            }
        }
        private void MakeContentUnEditable()
        {
            if (wbTemplateEditor.Document.Body != null)
                wbTemplateEditor.Document.Body.SetAttribute("contentEditable", "false");
        }
        private void MakeContentEditable()
        {
            if (wbTemplateEditor.Document.Body != null)
                wbTemplateEditor.Document.Body.SetAttribute("contentEditable", "true");
        }
        private void wbTemplateEditor_GotFocus(object sender, EventArgs e)
        {
            var s = e;
            if (wbTemplateEditor.Document != null &&
                wbTemplateEditor.Document.Body != null)
                wbTemplateEditor.Document.Body.Focus();
        }
        #endregion

        #region wbBlock editor
        private void btnAddImage_Click(object sender, EventArgs e)
        {
            var document = wbTemplateEditor.Document;
            if (document.Body != null)
            {
                document.ExecCommand("InsertImage", true, false);
                var images = wbTemplateEditor.Document.GetElementsByTagName("img");
                foreach (HtmlElement image in images)
                {
                    if (string.IsNullOrEmpty(image.Id))
                    {
                        var img = image;
                        img.Style = "height:auto;width:100%;";
                        img.Id = DateTime.Now.ToString();
                        HtmlElement copiedImage = img;
                        img.OuterHtml = string.Empty;
                        HtmlElement div = document.CreateElement("div");
                        div.SetAttribute("class", "resizable-element draggable-element");
                        div.Style = "width:35% !important;POSITION: absolute;";
                        div.AppendChild(copiedImage);
                        wbTemplateEditor.Document.Body.AppendChild(div);
                        var documentText = ReplaceFileSystemImages(document.GetElementsByTagName("HTML")[0].OuterHtml);
                        wbTemplateEditor.DocumentText = documentText;
                    }
                }
                if (images.Count > 0)
                {

                }
            }
            wbTemplateEditor.Document.InvokeScript("MakeContentDraggableAndResizable");

        }
        private string ReplaceFileSystemImages(string html)
        {
            var matches = Regex.Matches(html, @"<img[^>]*?src\s*=\s*([""']?[^'"">]+?['""])[^>]*?>", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline);
            foreach (Match match in matches)
            {
                string src = match.Groups[1].Value;
                src = src.Trim('\"');
                if (File.Exists(src))
                {
                    var ext = Path.GetExtension(src);
                    if (ext.Length > 0)
                    {
                        ext = ext.Substring(1);
                        src = string.Format("'data:image/{0};base64,{1} '", ext, Convert.ToBase64String(File.ReadAllBytes(src)));
                        html = html.Replace(match.Groups[1].Value, src);
                    }
                }
            }
            return html;
        }
        private void PrepareBorderCombox()
        {
            cmbDataBlockBorder.Items.Clear();
            cmbDataBlockBorder.Items.Add("No Border");
            cmbDataBlockBorder.Items.Add("(Dashed) - - - - - -");
            cmbDataBlockBorder.Items.Add("(Line)_______________");
            cmbDataBlockBorder.Items.Add("(Dotted)..............");
        }
        private void EnableBlockForEditing()
        {
            wbDataBlockEditor.DocumentText = SampleHtml;
            blockEditor = wbDataBlockEditor.Document.DomDocument as IHTMLDocument2;
            blockEditor.designMode = "On";
            wbDataBlockEditor.DocumentCompleted += wbDataBlockEditor_DocumentCompleted;
        }
        void wbDataBlockEditor_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            //if (wbDataBlockEditor.Document.Body != null)
            //{
            //    wbDataBlockEditor.Document.Body.SetAttribute("contentEditable", "true");
            //}
            if (wbDataBlockEditor.Document.GetElementById("content") != null)
            {
                wbDataBlockEditor.Document.GetElementById("content").SetAttribute("contentEditable", "true");
            }
        }
        private void cmbDataBlockBorder_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (wbDataBlockEditor.Document.Body != null)
            {
                var bodyElements = wbDataBlockEditor.Document.Body.InnerHtml;
            }
            if (wbDataBlockEditor.Document.Body == null)
            {
                wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
            }
            var selectedBorder = cmbDataBlockBorder.SelectedItem.ToString();
            var border = GetBorder(selectedBorder);
            if (wbDataBlockEditor.Document.Body == null)
            {
                wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
            }
            HtmlElement el = wbDataBlockEditor.Document.GetElementById("content");
            if (el != null)
            {
                if (border != CustomBorder.None)
                {
                    el.Style = string.Format("border:2px {0} black;padding:2px;margin:2px;", border.ToString());
                }
                else
                {
                    el.Style = string.Format("padding:2px;margin:2px;");
                }
            }

        }
        private CustomBorder GetBorder(string selectedBorder)
        {
            var result = CustomBorder.None;

            if (selectedBorder.Contains("Dotted"))
            {
                result = CustomBorder.Dotted;
            }
            if (selectedBorder.Contains("Dashed"))
            {
                result = CustomBorder.Dashed;
            }
            if (selectedBorder.Contains("Line"))
            {
                result = CustomBorder.Solid;
            }

            return result;
        }
        public enum CustomBorder
        {
            None = 0,
            Dashed = 1,
            Solid = 2,
            Dotted = 3
        }
        private void btnInsertLineBreak_Click(object sender, EventArgs e)
        {
            if (btnAutoInsertLineBreak.BackColor == SystemColors.Control)
            {
                AutoInsertLineBreak = true;
                btnAutoInsertLineBreak.BackColor = SystemColors.ActiveCaption;
            }
            else
            {
                AutoInsertLineBreak = false;
                btnAutoInsertLineBreak.BackColor = SystemColors.Control;
            }
        }
        private void btnResetBlock_Click(object sender, EventArgs e)
        {
            var selectedBlockType = (BlockType)Enum.Parse(typeof(BlockType), cmbBlockType.SelectedItem.ToString());
            if (selectedBlockType == BlockType.Tabular)
            {
                wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtmlTabular);
            }
            else
            {
                wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
            }
            grdInvoiceFields.Invalidate();
        }
        private void btnAddBlockToTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                var blockId = "block-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + "-" + DateTime.Now.Millisecond;
                var selectedBlockType = (BlockType)Enum.Parse(typeof(BlockType), cmbBlockType.SelectedItem.ToString());
                if (selectedBlockType == BlockType.Simple)
                {
                    var block = wbDataBlockEditor.Document.GetElementById("content");
                    var blk = wbTemplateEditor.Document.CreateElement("div");
                    blk.SetAttribute("class", "draggable-element resizable-element added-block ");
                    blk.OuterHtml = block.OuterHtml;
                    blk.InnerHtml = block.InnerHtml;
                    blk.SetAttribute("contentEditable", "true");
                    var border = GetBorder(cmbDataBlockBorder.SelectedItem.ToString());
                    if (border != CustomBorder.None)
                    {
                        blk.Style = string.Format("border:2px {0} black;", border.ToString());
                    }
                    blk.Style += ";z-index:100;top:15%;width:40%;background-color: inherit !important";
                    blk.Id = blockId;
                    wbTemplateEditor.Document.Body.AppendChild(blk);
                    wbTemplateEditor.DocumentText = wbTemplateEditor.Document.GetElementsByTagName("HTML")[0].OuterHtml;
                }
                if (selectedBlockType == BlockType.Tabular)
                {
                    var table = wbDataBlockEditor.Document.GetElementsByTagName("table")[0];
                    table.Id = blockId;
                    var tableData = wbDataBlockEditor.Document.GetElementsByTagName("table")[0].OuterHtml;
                    var editorText = wbTemplateEditor.Document.GetElementsByTagName("HTML")[0].OuterHtml;
                    editorText = editorText.Replace("</BODY>", "<div class='draggable-element resizable-element added-block' style='z-index:100;top:15%;overflow:hidden;'>" + tableData + "</div></BODY>");
                    wbTemplateEditor.DocumentText = editorText;
                }
                if (selectedBlockType == BlockType.BackGroundColorBox)
                {
                    var block = wbDataBlockEditor.Document.GetElementById("content");
                    var blk = wbTemplateEditor.Document.CreateElement("div");
                    blk.SetAttribute("class", "draggable-element resizable-element added-block ");
                    blk.OuterHtml = block.OuterHtml;
                    blk.InnerHtml = block.InnerHtml;
                    blk.Style = block.Style + ";z-index:0 !important;height:10%;width:10%;left:20%;top:15%;";
                    blk.Id = blockId;
                    //wbTemplateEditor.Document.GetElementById("first-child").AppendChild(blk);

                    wbTemplateEditor.Document.Body.InsertAdjacentElement(HtmlElementInsertionOrientation.AfterBegin, blk);
                    wbTemplateEditor.DocumentText = wbTemplateEditor.Document.GetElementsByTagName("HTML")[0].OuterHtml;
                }
                wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
                grdInvoiceFields.Invalidate();
                ResetBlockEditorButtonsToDefault();
                wbTemplateEditor.Focus();
            }
            catch (Exception ex)
            {
                wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
                ResetBlockEditorButtonsToDefault();
            }
        }

        private void ResetBlockEditorButtonsToDefault()
        {
            btnResetBlock.Enabled = false;
            btnAutoInsertLineBreak.Enabled = false;
            cmbDataBlockBorder.Enabled = false;
            btnAddBlockToTemplate.Enabled = false;
            btnKeyValueParing.Enabled = false;
            cmbKeyValueSeprator.Enabled = false;
            cmbBlockType.Visible = false;
        }
        private void LoadBlockEditorTypes()
        {
            cmbBlockType.Items.Clear();
            cmbBlockType.Items.AddRange(Enum.GetNames(typeof(BlockType)));
            cmbBlockType.SelectedIndex = 0;
            cmbBlockType.Visible = true;
        }
        public enum BlockType
        {
            Simple,
            Tabular,
            BackGroundColorBox
        }
        #endregion

        private void btnKeyValueParing_Click(object sender, EventArgs e)
        {
            if (btnKeyValueParing.BackColor == SystemColors.Control)
            {
                KeyValuePairingOfInvoiceFields = true;
                btnKeyValueParing.BackColor = SystemColors.ActiveCaption;
            }
            else
            {
                KeyValuePairingOfInvoiceFields = false;
                btnKeyValueParing.BackColor = SystemColors.Control;
            }
        }

        private void cmbKeyValueSeprator_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbKeyValueSeprator.SelectedItem != null)
            {
                var selectedKeyValueSeprator = cmbKeyValueSeprator.SelectedItem.ToString();
                if (selectedKeyValueSeprator.Contains("Single Space"))
                    KeyValueSeprator = " ";

                if (selectedKeyValueSeprator.Contains("Colons"))
                    KeyValueSeprator = ":";

                if (selectedKeyValueSeprator.Contains("Equal"))
                    KeyValueSeprator = "=";

                if (selectedKeyValueSeprator.Contains("Dash"))
                    KeyValueSeprator = "-";
            }
        }

        private void cmbBlockType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DefaultBlockEditorType = (BlockType)Enum.Parse(typeof(BlockType), cmbBlockType.SelectedItem.ToString());
            if (wbDataBlockEditor.Document == null || wbDataBlockEditor.Document.Body == null)
                wbDataBlockEditor.DocumentText = SampleHtml;
            if (DefaultBlockEditorType == BlockType.Tabular)
            {
                btnFillColor.Enabled = false;
                grdInvoiceFields.Enabled = true;
                wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtmlTabular);
            }
            if (DefaultBlockEditorType == BlockType.Simple)
            {
                btnFillColor.Enabled = false;
                grdInvoiceFields.Enabled = true;
                wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
            }
            if (DefaultBlockEditorType == BlockType.BackGroundColorBox)
            {
                btnFillColor.Enabled = true;
                grdInvoiceFields.Enabled = false;
                wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
            }
            UpdateInvoiceDataFields(null, null);
        }

        private void btnCancelUpdateBlock_Click(object sender, EventArgs e)
        {
            wbDataBlockEditor.Document.OpenNew(false).Write(SampleHtml);
            btnUpdateBlock.Visible = false;
            btnCancelUpdateBlock.Visible = false;
        }

        private void btnUpdateBlock_Click(object sender, EventArgs e)
        {
            var updatedHtmlElement = wbDataBlockEditor.Document.Body.FirstChild;
            var blockId = updatedHtmlElement.Id;
            wbDataBlockEditor.Document.GetElementById(blockId).Style = wbTemplateEditor.Document.GetElementById(blockId).Style;
            var updatedHtml = wbDataBlockEditor.Document.GetElementById(blockId).InnerHtml;
            var oldElement = wbTemplateEditor.Document.GetElementById(blockId).InnerHtml;
            var mainEditorContent = wbTemplateEditor.Document.GetElementsByTagName("html")[0].OuterHtml;
            mainEditorContent = mainEditorContent.Replace(oldElement, updatedHtml);
            wbTemplateEditor.DocumentText = mainEditorContent;
            wbTemplateEditor.Document.GetElementById(blockId).SetAttribute("class", "draggable-element resizable-element");
            wbTemplateEditor.Document.InvokeScript("MakeContentDraggableAndResizable");
            btnCancelUpdateBlock.PerformClick();
            wbTemplateEditor.Focus();
        }

        private void btnSaveTemplate_Click(object sender, EventArgs e)
        {
            Action postSavedAction = () =>
            {
                CheckForIllegalCrossThreadCalls = false;
                lblTemplateSaved.Text = "Saved";
                lblTemplateSaved.ForeColor = Color.Green;
                var task = new Task(() =>
                {
                    lblTemplateSaved.Text = string.Empty;
                    lblTemplateSaved.ForeColor = Color.Black;
                });
                task.Wait(1000);
                task.Start();
            };

            if (TemplateId == Guid.Empty)
            {
                if (SaveTemplate())
                {
                    postSavedAction.Invoke();
                }
            }
            else
            {
                if (UpdateTemplate())
                {
                    postSavedAction.Invoke();
                }
            }
        }

        private void wbTemplateEditor_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            wbTemplateEditor.Document.InvokeScript("MakeContentDraggableAndResizable");
        }

        private void btnPrintCondition_Click(object sender, EventArgs e)
        {
            if (TemplateId.Equals(Guid.Empty))
            {
                MessageBox.Show("Please save current template first!");
                return;
            }
            var printConditionsEditor = new InvoiceTemplatePrintConditions(txtTemplateName.Text, PrintConditionId);
            printConditionsEditor.Show();

        }

        private void chkGridLines_CheckedChanged(object sender, EventArgs e)
        {
            ApplyGridLinesOnTemplateEditor();
        }

        private void GridLinesSqureSize_Scroll(object sender, EventArgs e)
        {
            ApplyGridLinesOnTemplateEditor();
        }

        private void ApplyGridLinesOnTemplateEditor()
        {
            var gridSelectedValue = GridLinesSqureSize.Value * 10;
            if (wbTemplateEditor.Document != null && GridLinesSqureSize.Value > 0)
            {
                if (wbTemplateEditor.Document.Body == null)
                    return;

                var bodyStyle = wbTemplateEditor.Document.Body.Style;
                bodyStyle = String.IsNullOrEmpty(bodyStyle) ? "" : bodyStyle + ";";

                if (chkGridLines.Checked)
                {
                    wbTemplateEditor.Document.Body.Style = bodyStyle + "background-image: url(' data:image/png;base64," + GetBase64(AddGridlines(gridSelectedValue, Color.LightGray)) + @"');";
                }
                else
                {
                    wbTemplateEditor.Document.Body.Style = bodyStyle + "background-image: none;";
                }
            }
        }

        private void ShowNewPageSizePrompt(string pageSizeName)
        {
            var prompt = new Form()
            {
                ClientSize = new Size(400, 182),
                ShowIcon = false,
                Text = "Create New Page Size",
                StartPosition = FormStartPosition.CenterScreen
            };
            var lblPageSizeHeight = new Label()
            {
                Location = new System.Drawing.Point(29, 74),
                Size = new System.Drawing.Size(38, 13),
                Text = "Height",
                Name = "lblPageSizeHeight"
            };
            var lblPageSizeWidth = new Label()
            {
                Location = new System.Drawing.Point(29, 98),
                Size = new System.Drawing.Size(35, 13),
                Text = "Width",
                Name = "lblPageSizeWidth"
            };
            var txtPageSizeHeight = new TextBox()
            {
                Location = new System.Drawing.Point(97, 71),
                Size = new System.Drawing.Size(75, 20),
                Name = "txtPageSizeHeight"
            };
            var txtPageSizeWidth = new TextBox()
            {
                Location = new System.Drawing.Point(97, 98),
                Size = new System.Drawing.Size(75, 20),
                Name = "txtPageSizeWidth"
            };
            var txtPageSizeHeightInch = new TextBox()
            {
                Location = new System.Drawing.Point(220, 71),
                Size = new System.Drawing.Size(75, 20),
                Name = "txtPageSizeHeightInch"
            };
            var txtPageSizeWidthInch = new TextBox()
            {
                Location = new System.Drawing.Point(220, 98),
                Size = new System.Drawing.Size(75, 20),
                Name = "txtPageSizeWidthInch"

            };
            var lblpx = new Label()
            {
                Location = new System.Drawing.Point(178, 74),
                Size = new System.Drawing.Size(18, 13),
                Text = "px"
            };
            var lblpx2 = new Label()
            {
                Location = new System.Drawing.Point(178, 101),
                Size = new System.Drawing.Size(18, 13),
                Text = "px"
            };



            var orLabel1 = new Label()
            {
                Location = new System.Drawing.Point(197, 74),
                Size = new System.Drawing.Size(18, 13),
                Text = "or"
            };
            var orLabel2 = new Label()
            {
                Location = new System.Drawing.Point(200, 101),
                Size = new System.Drawing.Size(18, 13),
                Text = "or"
            };

            var lblInch1 = new Label()
            {
                Location = new System.Drawing.Point(300, 74),
                Size = new System.Drawing.Size(18, 13),
                Text = "in"
            };
            var lblInch2 = new Label()
            {
                Location = new System.Drawing.Point(300, 101),
                Size = new System.Drawing.Size(18, 13),
                Text = "in"
            };

            var txtPageSizeName = new TextBox()
            {
                Location = new System.Drawing.Point(97, 42),
                Size = new System.Drawing.Size(99, 20)
            };
            var lblPageSizeName = new Label()
            {
                Location = new System.Drawing.Point(29, 42),
                Size = new System.Drawing.Size(35, 13),
                Text = "Name"
            };
            var btnPageSizeSave = new Button()
            {
                Location = new System.Drawing.Point(121, 147),
                Size = new System.Drawing.Size(75, 23),
                Text = "Add",
                UseVisualStyleBackColor = true
            };
            var btnPageSizeDelete = new Button()
            {
                Location = new System.Drawing.Point(202, 147),
                Size = new System.Drawing.Size(75, 23),
                Text = "Delete",
                UseVisualStyleBackColor = true
            };

            Action<object, KeyEventArgs> keyPressForTextBlocks = (sender, e) =>
            {
                var senderName = (sender as TextBox).Name;

                if (String.IsNullOrEmpty(((TextBox)sender).Text))
                {
                    return;
                }
                switch (senderName)
                {
                    case "txtPageSizeHeight":
                        {
                            txtPageSizeHeightInch.Text = Math.Round((Convert.ToDouble(txtPageSizeHeight.Text) / 72), 2).ToString();
                            break;
                        }

                    case "txtPageSizeHeightInch":
                        {
                            txtPageSizeHeight.Text = Math.Round((Convert.ToDouble(txtPageSizeHeightInch.Text) * 72), 2).ToString();
                            break;
                        }


                    case "txtPageSizeWidth":
                        {
                            txtPageSizeWidthInch.Text = Math.Round((Convert.ToDouble(txtPageSizeWidth.Text) / 72), 2).ToString();
                            break;
                        }

                    case "txtPageSizeWidthInch":
                        {
                            txtPageSizeWidth.Text = Math.Round((Convert.ToDouble(txtPageSizeWidthInch.Text) * 72), 2).ToString();
                            break;
                        }
                }
            };

            txtPageSizeHeight.KeyUp += (s, e) => { keyPressForTextBlocks.Invoke(s, e); };
            txtPageSizeHeightInch.KeyUp += (s, e) => { keyPressForTextBlocks.Invoke(s, e); };
            txtPageSizeWidth.KeyUp += (s, e) => { keyPressForTextBlocks.Invoke(s, e); };
            txtPageSizeWidthInch.KeyUp += (s, e) => { keyPressForTextBlocks.Invoke(s, e); };

            btnPageSizeSave.Click += (sender, e) =>
            {
                ODM.Core.InvoiceTemplate.PageSettings.SavePageSize(new PageSize()
                {
                    PageSizeName = txtPageSizeName.Text,
                    Width = (int)Math.Ceiling(Convert.ToDouble(txtPageSizeWidth.Text)),
                    Height = (int)Math.Ceiling(Convert.ToDouble(txtPageSizeHeight.Text))
                });
                LoadEditorDefault();
                prompt.Close();
            };
            btnPageSizeDelete.Click += (Sender, e) =>
            {
                ODM.Core.InvoiceTemplate.PageSettings.DeletePageSize(txtPageSizeName.Text);
                LoadEditorDefault();
                prompt.Close();
            };
            prompt.Controls.Add(btnPageSizeDelete);
            prompt.Controls.Add(btnPageSizeSave);
            prompt.Controls.Add(txtPageSizeName);
            prompt.Controls.Add(lblPageSizeName);
            prompt.Controls.Add(lblpx2);
            prompt.Controls.Add(lblpx);
            prompt.Controls.Add(txtPageSizeWidth);
            prompt.Controls.Add(txtPageSizeHeight);
            prompt.Controls.Add(txtPageSizeWidthInch);
            prompt.Controls.Add(txtPageSizeHeightInch);
            prompt.Controls.Add(lblPageSizeWidth);
            prompt.Controls.Add(lblPageSizeHeight);
            prompt.Controls.Add(orLabel1);
            prompt.Controls.Add(orLabel2);
            prompt.Controls.Add(lblInch1);
            prompt.Controls.Add(lblInch2);

            if (!string.IsNullOrEmpty(pageSizeName))
            {
                var pageSize = ODM.Core.InvoiceTemplate.PageSettings.GetCustomPageSize(pageSizeName);
                txtPageSizeName.Text = pageSize.PageSizeName;
                txtPageSizeWidth.Text = pageSize.Width.ToString();
                txtPageSizeHeight.Text = pageSize.Height.ToString();
                txtPageSizeWidthInch.Text = Math.Round((Convert.ToDouble(txtPageSizeWidth.Text) / 72), 2).ToString();
                txtPageSizeHeightInch.Text = Math.Round((Convert.ToDouble(txtPageSizeHeight.Text) / 72), 2).ToString();
            }


            prompt.ShowDialog();
        }

        private void linkEditCustomPageSize_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var prompt = new Form()
            {
                ClientSize = new Size(302, 182),
                ShowIcon = false,
                StartPosition = FormStartPosition.CenterScreen
            };
            var grdCustomPageSizes = new DataGridView()
            {
                Location = new Point(10, 10),
                Width = 280,
                Height = 170,
                DataSource = ODM.Core.InvoiceTemplate.PageSettings.GetAllCustomPageSizes(),
                AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells,
                RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing,
                ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize,
                AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill,
                ScrollBars = ScrollBars.Both,
                RowHeadersVisible = false,
                EnableHeadersVisualStyles = false,
                AllowUserToAddRows = false
            };
            grdCustomPageSizes.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdCustomPageSizes.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdCustomPageSizes.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdCustomPageSizes.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdCustomPageSizes.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdCustomPageSizes.DoubleBuffered(true);
            grdCustomPageSizes.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdCustomPageSizes.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdCustomPageSizes.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdCustomPageSizes.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdCustomPageSizes.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdCustomPageSizes.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdCustomPageSizes.DefaultCellStyle.Padding = new Padding(5, 5, 5, 5);
            grdCustomPageSizes.BackgroundColor = SystemColors.ButtonFace;
            grdCustomPageSizes.BorderStyle = BorderStyle.None;
            grdCustomPageSizes.Anchor = (AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom);

            DataGridViewLinkColumn pageSizeName = new DataGridViewLinkColumn();
            pageSizeName.HeaderText = "Name";
            pageSizeName.Name = "Name";
            pageSizeName.DataPropertyName = "Name";
            pageSizeName.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdCustomPageSizes.CellContentClick += (Sender, evt) =>
            {
                if (evt.RowIndex >= 0 && evt.ColumnIndex >= 0)
                {
                    if (grdCustomPageSizes.Columns[evt.ColumnIndex].Name.Equals("Name"))
                    {
                        ShowNewPageSizePrompt(grdCustomPageSizes.Rows[evt.RowIndex].Cells["Name"].Value.ToString());
                        prompt.Close();
                    }
                }
            };

            grdCustomPageSizes.Columns.Add(pageSizeName);
            prompt.Controls.Add(grdCustomPageSizes);
            prompt.ShowDialog();
        }

        private void btnFillColor_Click(object sender, EventArgs e)
        {
            ColorFilling = ColorFilling ? false : true;

            if (ColorFilling)
            {
                if (colorDialog1.ShowDialog() == DialogResult.OK)
                {
                    SetFillBucketCursorForWebControls();
                    pnlDataBlockEditorControl.MouseHover += pnl_MouseHover;
                }
            }
            else
            {
                UnSetFillBucketCursorForWebControls();
                pnlDataBlockEditorControl.MouseHover -= pnl_MouseHover;
            }
            SetDefaultCursorForNonWebControls();
        }

        private void SetDefaultCursorForNonWebControls()
        {
            foreach (var control in this.Controls)
            {
                var controlName = ((Control)control).Name.ToString();
                if (!(controlName.Contains("pnl") || controlName.Contains("wb")))
                {
                    ((Control)control).Cursor = Cursors.Default;
                }
            }
            this.Cursor = Cursors.Default;
        }

        private void pnl_MouseHover(object sender, EventArgs e)
        {
            SetFillBucketCursorForWebControls();
        }

        private void UnSetFillBucketCursorForWebControls()
        {
            btnFillColor.BackColor = InvoiceTemplateEditor.DefaultBackColor;
            this.Cursor = Cursors.Default;
            ((Control)wbDataBlockEditor).Enabled = true;
            DisableFillOnClick();
        }

        private void SetFillBucketCursorForWebControls()
        {
            btnFillColor.BackColor = colorDialog1.Color;
            var b = (Bitmap)(System.Drawing.Image)(new System.ComponentModel.ComponentResourceManager(typeof(InvoiceTemplateEditor)).GetObject("btnFillColor.Image"));
            IntPtr ptr = b.GetHicon();
            this.Cursor = new Cursor(ptr);
            Cursor.Current = new Cursor(ptr);

            ((Control)wbDataBlockEditor).Enabled = false;

            EnableFillOnClick();
        }

        private void EnableFillOnClick()
        {
            pnlDataBlockEditorControl.Click -= ColorFillClick;
            pnlDataBlockEditorControl.Click += ColorFillClick;
        }

        void ColorFillClick(object sender, EventArgs e)
        {

            var evntSenderName = ((Panel)sender).Name;
            if (evntSenderName.Equals(pnlDataBlockEditorControl.Name))
            {
                if (wbDataBlockEditor.Document != null && wbDataBlockEditor.Document.Body != null)
                {
                    ((Control)wbDataBlockEditor).Enabled = true;

                    wbDataBlockEditor.Document.GetElementById("content").Style = "background-color: rgb(" + colorDialog1.Color.R + "," + colorDialog1.Color.G + "," + colorDialog1.Color.B + ");";
                    wbDataBlockEditor.Document.Body.Style = "background-color: rgb(" + colorDialog1.Color.R + "," + colorDialog1.Color.G + "," + colorDialog1.Color.B + ");";
                    ((Control)wbDataBlockEditor).Enabled = false;
                }
            }
        }
        private void DisableFillOnClick()
        {
            pnlDataBlockEditorControl.Click -= ColorFillClick;
        }

        private void btnTextColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                string colorstr = string.Format("#{0:X2}{1:X2}{2:X2}", colorDialog1.Color.R, colorDialog1.Color.G, colorDialog1.Color.B);
                if (wbDataBlockEditor.Document != null)
                    wbDataBlockEditor.Document.ExecCommand("ForeColor", true, colorstr);

                if (wbTemplateEditor.Document != null)
                    wbTemplateEditor.Document.ExecCommand("ForeColor", true, colorstr);
            }
        }

        private void btnLineHeight_Click(object sender, EventArgs e)
        {
            InitializeLineHeightOpt();
        }

        private void InitializeLineHeightOpt()
        {
            cmbLineHeight.Items.Clear();
            new double[] { 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10 }.ToList().ForEach(x =>
            {
                cmbLineHeight.Items.Add(x);
            });
            cmbLineHeight.SelectedIndexChanged -= cmbLineHeight_SelectedIndexChanged;
            cmbLineHeight.SelectedIndex = 3;
            cmbLineHeight.Visible = true;
            cmbLineHeight.SelectedIndexChanged += cmbLineHeight_SelectedIndexChanged;
        }

        void cmbLineHeight_SelectedIndexChanged(object sender, EventArgs e)
        {
            var lineHeight = Convert.ToDouble(cmbLineHeight.SelectedItem.ToString());
            var activeElement = wbTemplateEditor.Document.ActiveElement;
            if (activeElement.Style == null)
                activeElement.Style = string.Format("line-height:{0}%", lineHeight * 50);
            else
                activeElement.Style += string.Format(";line-height:{0}%;", lineHeight * 50);

            cmbLineHeight.Visible = false;
        }

        private void cmbLineHeight_Click(object sender, EventArgs e)
        {

        }

        private void linkToggleDataBlockEdit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DataBlockEditing = DataBlockEditing ? false : true;
            if (DataBlockEditing)
            {
                linkToggleDataBlockEdit.Text = "Disable Data Block Edit";
                linkToggleDataBlockEdit.LinkColor = Color.Blue;
            }
            else
            {
                linkToggleDataBlockEdit.Text = "Enable Data Bock Edit";
                linkToggleDataBlockEdit.LinkColor = Color.Red;
            }
        }

        private void toolStripBtnRefresh_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            ((Control)wbTemplateEditor).Enabled = false;
            var documentText = wbTemplateEditor.Document.GetElementsByTagName("HTML")[0].OuterHtml;
            wbTemplateEditor.DocumentText = documentText;
            ((Control)wbTemplateEditor).Enabled = true;
            this.Cursor = Cursors.Default;
        }

        private void btnAutoInsertLineBreak_Click(object sender, EventArgs e)
        {
            AutoInsertLineBreak = AutoInsertLineBreak ? false : true;
            if (AutoInsertLineBreak)
            {
                btnAutoInsertLineBreak.BackColor = SystemColors.ActiveCaption;
            }
            else
            {
                btnAutoInsertLineBreak.BackColor = SystemColors.Control;
            }
        }

        private void linkDataPrintSettings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (TemplateId.Equals(Guid.Empty))
            {
                MessageBox.Show("Please save current template first!");
                return;
            }

            var dataPrintSettingsForm = Application.OpenForms["InvoiceTemplateDataPrintSettings"];
            if (dataPrintSettingsForm == null)
            {
                var dataPrintSettings = new InvoiceTemplateDataPrintSettings(TemplateId);
                dataPrintSettings.ShowDialog();
            }
        }

    }
}

