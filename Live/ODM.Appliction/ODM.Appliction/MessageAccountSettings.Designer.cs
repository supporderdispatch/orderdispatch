﻿namespace ODM.Appliction
{
    partial class MessageAccountSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageAccountSettings));
            this.pnlAccountDetails = new System.Windows.Forms.Panel();
            this.lblNoRecordError = new System.Windows.Forms.Label();
            this.grdMessageAccounts = new System.Windows.Forms.DataGridView();
            this.rdbtnEmail = new System.Windows.Forms.RadioButton();
            this.rdbtnEbay = new System.Windows.Forms.RadioButton();
            this.btnAddNewAccount = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnDeleteMessageAccount = new System.Windows.Forms.Button();
            this.btnEbayAccountSettings = new System.Windows.Forms.Button();
            this.pnlAccountDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMessageAccounts)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlAccountDetails
            // 
            this.pnlAccountDetails.Controls.Add(this.lblNoRecordError);
            this.pnlAccountDetails.Controls.Add(this.grdMessageAccounts);
            this.pnlAccountDetails.Location = new System.Drawing.Point(1, 98);
            this.pnlAccountDetails.Name = "pnlAccountDetails";
            this.pnlAccountDetails.Size = new System.Drawing.Size(540, 357);
            this.pnlAccountDetails.TabIndex = 0;
            // 
            // lblNoRecordError
            // 
            this.lblNoRecordError.AutoSize = true;
            this.lblNoRecordError.Location = new System.Drawing.Point(205, 147);
            this.lblNoRecordError.Name = "lblNoRecordError";
            this.lblNoRecordError.Size = new System.Drawing.Size(92, 13);
            this.lblNoRecordError.TabIndex = 1;
            this.lblNoRecordError.Text = "No Record Found";
            this.lblNoRecordError.Visible = false;
            // 
            // grdMessageAccounts
            // 
            this.grdMessageAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMessageAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdMessageAccounts.Location = new System.Drawing.Point(0, 0);
            this.grdMessageAccounts.Name = "grdMessageAccounts";
            this.grdMessageAccounts.Size = new System.Drawing.Size(540, 357);
            this.grdMessageAccounts.TabIndex = 0;
            this.grdMessageAccounts.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMessageAccounts_CellDoubleClick);
            // 
            // rdbtnEmail
            // 
            this.rdbtnEmail.AutoSize = true;
            this.rdbtnEmail.Location = new System.Drawing.Point(40, 12);
            this.rdbtnEmail.Name = "rdbtnEmail";
            this.rdbtnEmail.Size = new System.Drawing.Size(98, 17);
            this.rdbtnEmail.TabIndex = 0;
            this.rdbtnEmail.TabStop = true;
            this.rdbtnEmail.Text = "Email Accounts";
            this.rdbtnEmail.UseVisualStyleBackColor = true;
            this.rdbtnEmail.CheckedChanged += new System.EventHandler(this.rdbtnEmail_CheckedChanged);
            // 
            // rdbtnEbay
            // 
            this.rdbtnEbay.AutoSize = true;
            this.rdbtnEbay.Location = new System.Drawing.Point(178, 12);
            this.rdbtnEbay.Name = "rdbtnEbay";
            this.rdbtnEbay.Size = new System.Drawing.Size(97, 17);
            this.rdbtnEbay.TabIndex = 1;
            this.rdbtnEbay.TabStop = true;
            this.rdbtnEbay.Text = "Ebay Accounts";
            this.rdbtnEbay.UseVisualStyleBackColor = true;
            this.rdbtnEbay.CheckedChanged += new System.EventHandler(this.rdbtnEbay_CheckedChanged);
            // 
            // btnAddNewAccount
            // 
            this.btnAddNewAccount.Location = new System.Drawing.Point(28, 59);
            this.btnAddNewAccount.Name = "btnAddNewAccount";
            this.btnAddNewAccount.Size = new System.Drawing.Size(75, 23);
            this.btnAddNewAccount.TabIndex = 2;
            this.btnAddNewAccount.Text = "Add New";
            this.btnAddNewAccount.UseVisualStyleBackColor = true;
            this.btnAddNewAccount.Click += new System.EventHandler(this.btnAddNewAccount_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(331, 9);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnDeleteMessageAccount
            // 
            this.btnDeleteMessageAccount.Location = new System.Drawing.Point(199, 59);
            this.btnDeleteMessageAccount.Name = "btnDeleteMessageAccount";
            this.btnDeleteMessageAccount.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteMessageAccount.TabIndex = 4;
            this.btnDeleteMessageAccount.Text = "Delete";
            this.btnDeleteMessageAccount.UseVisualStyleBackColor = true;
            this.btnDeleteMessageAccount.Visible = false;
            this.btnDeleteMessageAccount.Click += new System.EventHandler(this.btnDeleteMessageAccount_Click);
            // 
            // btnEbayAccountSettings
            // 
            this.btnEbayAccountSettings.Location = new System.Drawing.Point(331, 59);
            this.btnEbayAccountSettings.Name = "btnEbayAccountSettings";
            this.btnEbayAccountSettings.Size = new System.Drawing.Size(151, 23);
            this.btnEbayAccountSettings.TabIndex = 5;
            this.btnEbayAccountSettings.Text = "Ebay Account Settings";
            this.btnEbayAccountSettings.UseVisualStyleBackColor = true;
            this.btnEbayAccountSettings.Click += new System.EventHandler(this.btnEbayAccountSettings_Click);
            // 
            // MessageAccountSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 458);
            this.Controls.Add(this.btnEbayAccountSettings);
            this.Controls.Add(this.btnDeleteMessageAccount);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnAddNewAccount);
            this.Controls.Add(this.rdbtnEbay);
            this.Controls.Add(this.rdbtnEmail);
            this.Controls.Add(this.pnlAccountDetails);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MessageAccountSettings";
            this.Text = "MessageAccountSettings";
            this.pnlAccountDetails.ResumeLayout(false);
            this.pnlAccountDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMessageAccounts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlAccountDetails;
        private System.Windows.Forms.DataGridView grdMessageAccounts;
        private System.Windows.Forms.RadioButton rdbtnEmail;
        private System.Windows.Forms.RadioButton rdbtnEbay;
        private System.Windows.Forms.Button btnAddNewAccount;
        private System.Windows.Forms.Label lblNoRecordError;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDeleteMessageAccount;
        private System.Windows.Forms.Button btnEbayAccountSettings;
    }
}