﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class MessageTemplateSettings : Form
    {
        string strMode;
        DataTable objDt = new DataTable();
        SqlConnection connection = new SqlConnection();
        bool IsLoadCall = false;

        string strSelectedTableColumnName = "", strIsMailOrSubjectCall = "";
        public MessageTemplateSettings()
        {

            InitializeComponent();
            connection.ConnectionString = MakeConnection.GetConnection();
            IsLoadCall = true;
            BindDropdownListForSubject();
            PopulateControls();
            CreateAndAttachMenuStrip();
            IsLoadCall = false;
        }

        private void MessageTemplateSettings_Load(object sender, EventArgs e)
        {

            //BindDropdownListForSubject();
        }
        private void BindDropdownListForSubject()
        {
            DataTable objTemplateSubject = GlobalVariablesAndMethods.GetAllMessageTemplateSubject(); ;
            ddlTemplateSubject.DataSource = objTemplateSubject;
            if (objTemplateSubject.Rows.Count > 0)
            {
                ddlTemplateSubject.ValueMember = "ID";
                ddlTemplateSubject.DisplayMember = "Text";
                ddlTemplateSubject.SelectedIndex = 0;
                btnChangeSubject.Visible = true;
            }

        }

        private void btnAddNewSubject_Click(object sender, EventArgs e)
        {
            strMode = "Create";
            AddTemplateSubject objAddTemplateSubject = new AddTemplateSubject("Add New Subject", strMode, "", "");
            objAddTemplateSubject.ShowDialog();
            if (objAddTemplateSubject.IsCreated == true)
            {
                BindDropdownListForSubject();
                PopulateControls();
                CreateAndAttachMenuStrip();
            }

        }

        private void ddlTemplateSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!IsLoadCall)
            {
                PopulateControls();
            }
        }

        private void PopulateControls()
        {
            string strSubjectConditionId = string.Empty;
            if (ddlTemplateSubject.SelectedValue != null)
            {
                strSubjectConditionId = ddlTemplateSubject.SelectedValue.ToString();
            }
            else
            {
                strSubjectConditionId = new Guid().ToString();
            }
            DataTable objDt = new DataTable();
            objDt = GlobalVariablesAndMethods.GetAllMessageTemplates(strSubjectConditionId);
            if (objDt.Rows.Count > 0)
            {
                chkIsActive.Checked = Convert.ToString(objDt.Rows[0]["IsEnabled"]) == null ? false : Convert.ToBoolean(Convert.ToString(objDt.Rows[0]["IsEnabled"]));
                txtTemplateText.Text = Convert.ToString(objDt.Rows[0]["TemplateText"]);
            }
            else
            {
                chkIsActive.Checked = false;
                txtTemplateText.Text = null;
            }

        }
        // to add menu strip for choosing columns for Order Details and Customer Details
        private void CreateAndAttachMenuStrip()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            string strCallFrom = "MESSAGE";
            SqlParameter[] objParam = new SqlParameter[1];

            objParam[0] = new SqlParameter("@CallFrom", SqlDbType.VarChar, 50);
            objParam[0].Value = strCallFrom.ToUpper();
            DataSet ds = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDataAndColumnNamesForMenu", objParam);

            //Creating object of menustrip and setting its location on this screen
            MenuStrip objMenu = new MenuStrip();
            objMenu.Name = "MenuOD";
            objMenu.Text = "Make Selection";
            objMenu.Dock = DockStyle.None;
            objMenu.Location = new Point(45, 152);
            //End of creation of menustrip and location

            //Main and first menu that will display for user
            ToolStripMenuItem MnuStripItemMain = new ToolStripMenuItem("Choose Column");
            objMenu.Items.Add(MnuStripItemMain);
            //End of main menu

            if (ds != null && ds.Tables.Count > 0)
            {
                //Looping for the tables which we got to add as menu
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    DataTable objDt = new DataTable();
                    objDt = ds.Tables[i];
                    string strTableName = Convert.ToString(objDt.Rows[i]["Table_Name"]);//Table name as inner main menu

                    //Creating inner main menu to show list of tables
                    ToolStripMenuItem MenuTable = new ToolStripMenuItem(strTableName);
                    MnuStripItemMain.DropDownItems.Add(MenuTable);

                    //Looping to get columns existence under a table
                    foreach (DataRow dr in objDt.Rows)
                    {
                        //Adding column as menu and that will get data from db to show in mail, for subject and mail body
                        ToolStripMenuItem MenuColumnName = new ToolStripMenuItem(dr["Column_Name"].ToString(), null, MenuOfColumnsClick);
                        MenuTable.DropDownItems.Add(MenuColumnName);
                    }
                }
            }

            this.Controls.Add(objMenu);
        }
        private void MenuOfColumnsClick(object sender, EventArgs e)
        {
            //Getting the text of selected menu(column) and its parent(table) and making a condition by concating [{ and }]
            ToolStripMenuItem SelectedMenu = (ToolStripMenuItem)sender;
            string strSelectedMenu = SelectedMenu.Text;
            string strSelectedMenuParent = SelectedMenu.OwnerItem.Text;
            strSelectedTableColumnName = "[{" + strSelectedMenuParent + "." + strSelectedMenu + "}]";//Condition text
            //End of making condition for subject and mail body

            //strIsMailOrSubjectCall == "MB" means we have focus on Mail Body i.e email template textbox
            if (strIsMailOrSubjectCall == "MB")
            {
                int intSelection = txtTemplateText.SelectionStart;//Current cursor point on template textbox
                //putting condition text at the current cursor location in txtTemplate textbox
                txtTemplateText.Text = txtTemplateText.Text.Insert(intSelection, strSelectedTableColumnName);
                //to keep cursor point where it is now in txtTemplate textbox   
                txtTemplateText.SelectionStart = intSelection + strSelectedTableColumnName.Length;
            }
            //strIsMailOrSubjectCall == "SUB" means we have focus on Subject part of mail i.e subject textbox
            //else if (strIsMailOrSubjectCall == "SUB")
            //{
            //    int intSelection = txtSubject.SelectionStart;//Current cursor point on subject textbox
            //    txtSubject.Text = txtSubject.Text.Insert(txtSubject.SelectionStart, strSelectedTableColumnName);
            //    txtSubject.SelectionStart = intSelection + strSelectedTableColumnName.Length;
            //}
        }
        private void btnChangeSubject_Click(object sender, EventArgs e)
        {
            strMode = "Update";
            AddTemplateSubject objAddTemplateSubject = new AddTemplateSubject("Update Subject", strMode, ddlTemplateSubject.Text.ToUpper().ToString(), ddlTemplateSubject.SelectedValue.ToString());
            objAddTemplateSubject.ShowDialog();


        }

        private void btnDeleteSubject_Click(object sender, EventArgs e)
        {
            strMode = "Delete";
            string strResult = "";
            DialogResult objDialogueResult = MessageBox.Show("Are you sure you want to delete this subject ?", "Delete Subject", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (objDialogueResult == DialogResult.Yes)
            {
                strResult = GlobalVariablesAndMethods.AddUpdateDeleteTemplateSubject(strMode.ToUpper(), ddlTemplateSubject.SelectedValue.ToString(), ddlTemplateSubject.SelectedText.ToUpper().ToString());
            }
            if (strResult == "4")
            {
                BindDropdownListForSubject();
                PopulateControls();
                CreateAndAttachMenuStrip();

            }
        }

        private void btnSaveTemplate_Click(object sender, EventArgs e)
        {
            string strResult = AddUpdateMessageTemplates("UPDATE");
            if (strResult == "0")
            {
                MessageBox.Show("Template Saved Successfully!", "Add New Template", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (strResult == "1")
            {
                MessageBox.Show("Template Updated Successfully!", "Update Template", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private string AddUpdateMessageTemplates(string strMode)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                //Pirmary Key ID generated in store procedure. so leaving that parameter


                objParam[1] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = new Guid();

                GuidConverter GuidConverter = new GuidConverter();

                objParam[2] = new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier);
                objParam[2].Value = GuidConverter.ConvertFromString(ddlTemplateSubject.SelectedValue.ToString());

                objParam[3] = new SqlParameter("@TemplateText", SqlDbType.VarChar, int.MaxValue);
                objParam[3].Value = txtTemplateText.Text.ToString();

                objParam[4] = new SqlParameter("@IsENabled", SqlDbType.Bit);
                objParam[4].Value = Convert.ToByte(chkIsActive.Checked);

                objParam[5] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[5].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "AddUpdateMessageTemplates", objParam);
                string strResult = objParam[5].Value.ToString();

                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                return strResult;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        private void txtTemplateText_Enter(object sender, EventArgs e)
        {
            strIsMailOrSubjectCall = "MB";
        }

        private void txtTemplateText_Leave(object sender, EventArgs e)
        {
            strIsMailOrSubjectCall = "";
        }
    }
}
