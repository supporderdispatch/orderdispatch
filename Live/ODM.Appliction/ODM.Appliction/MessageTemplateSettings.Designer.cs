﻿namespace ODM.Appliction
{
    partial class MessageTemplateSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageTemplateSettings));
            this.txtTemplateText = new System.Windows.Forms.RichTextBox();
            this.ddlTemplateSubject = new System.Windows.Forms.ComboBox();
            this.lblTemplateSubject = new System.Windows.Forms.Label();
            this.btnAddNewTemplate = new System.Windows.Forms.Button();
            this.btnSaveTemplate = new System.Windows.Forms.Button();
            this.chkIsActive = new System.Windows.Forms.CheckBox();
            this.btnChangeSubject = new System.Windows.Forms.Button();
            this.btnDeleteSubject = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtTemplateText
            // 
            this.txtTemplateText.Location = new System.Drawing.Point(2, 182);
            this.txtTemplateText.Name = "txtTemplateText";
            this.txtTemplateText.Size = new System.Drawing.Size(881, 316);
            this.txtTemplateText.TabIndex = 0;
            this.txtTemplateText.Text = "";
            this.txtTemplateText.Enter += new System.EventHandler(this.txtTemplateText_Enter);
            this.txtTemplateText.Leave += new System.EventHandler(this.txtTemplateText_Leave);
            // 
            // ddlTemplateSubject
            // 
            this.ddlTemplateSubject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlTemplateSubject.FormattingEnabled = true;
            this.ddlTemplateSubject.Location = new System.Drawing.Point(111, 54);
            this.ddlTemplateSubject.Name = "ddlTemplateSubject";
            this.ddlTemplateSubject.Size = new System.Drawing.Size(335, 21);
            this.ddlTemplateSubject.TabIndex = 1;
            this.ddlTemplateSubject.SelectedIndexChanged += new System.EventHandler(this.ddlTemplateSubject_SelectedIndexChanged);
            // 
            // lblTemplateSubject
            // 
            this.lblTemplateSubject.AutoSize = true;
            this.lblTemplateSubject.Location = new System.Drawing.Point(38, 59);
            this.lblTemplateSubject.Name = "lblTemplateSubject";
            this.lblTemplateSubject.Size = new System.Drawing.Size(43, 13);
            this.lblTemplateSubject.TabIndex = 3;
            this.lblTemplateSubject.Text = "Subject";
            // 
            // btnAddNewTemplate
            // 
            this.btnAddNewTemplate.Location = new System.Drawing.Point(600, 52);
            this.btnAddNewTemplate.Name = "btnAddNewTemplate";
            this.btnAddNewTemplate.Size = new System.Drawing.Size(121, 23);
            this.btnAddNewTemplate.TabIndex = 5;
            this.btnAddNewTemplate.Text = "Add New Subject";
            this.btnAddNewTemplate.UseVisualStyleBackColor = true;
            this.btnAddNewTemplate.Click += new System.EventHandler(this.btnAddNewSubject_Click);
            // 
            // btnSaveTemplate
            // 
            this.btnSaveTemplate.Location = new System.Drawing.Point(600, 12);
            this.btnSaveTemplate.Name = "btnSaveTemplate";
            this.btnSaveTemplate.Size = new System.Drawing.Size(64, 23);
            this.btnSaveTemplate.TabIndex = 6;
            this.btnSaveTemplate.Text = "Save";
            this.btnSaveTemplate.UseVisualStyleBackColor = true;
            this.btnSaveTemplate.Click += new System.EventHandler(this.btnSaveTemplate_Click);
            // 
            // chkIsActive
            // 
            this.chkIsActive.AutoSize = true;
            this.chkIsActive.Location = new System.Drawing.Point(148, 95);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(73, 17);
            this.chkIsActive.TabIndex = 7;
            this.chkIsActive.Text = "IsEnabled";
            this.chkIsActive.UseVisualStyleBackColor = true;
            // 
            // btnChangeSubject
            // 
            this.btnChangeSubject.Location = new System.Drawing.Point(478, 52);
            this.btnChangeSubject.Name = "btnChangeSubject";
            this.btnChangeSubject.Size = new System.Drawing.Size(110, 23);
            this.btnChangeSubject.TabIndex = 8;
            this.btnChangeSubject.Text = "Change Subject";
            this.btnChangeSubject.UseVisualStyleBackColor = true;
            this.btnChangeSubject.Visible = false;
            this.btnChangeSubject.Click += new System.EventHandler(this.btnChangeSubject_Click);
            // 
            // btnDeleteSubject
            // 
            this.btnDeleteSubject.Location = new System.Drawing.Point(734, 52);
            this.btnDeleteSubject.Name = "btnDeleteSubject";
            this.btnDeleteSubject.Size = new System.Drawing.Size(113, 23);
            this.btnDeleteSubject.TabIndex = 9;
            this.btnDeleteSubject.Text = "Delete Subject";
            this.btnDeleteSubject.UseVisualStyleBackColor = true;
            this.btnDeleteSubject.Click += new System.EventHandler(this.btnDeleteSubject_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(697, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // MessageTemplateSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 499);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDeleteSubject);
            this.Controls.Add(this.btnChangeSubject);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnSaveTemplate);
            this.Controls.Add(this.btnAddNewTemplate);
            this.Controls.Add(this.lblTemplateSubject);
            this.Controls.Add(this.ddlTemplateSubject);
            this.Controls.Add(this.txtTemplateText);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MessageTemplateSettings";
            this.Text = "MessageTemplateSettings";
            this.Load += new System.EventHandler(this.MessageTemplateSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtTemplateText;
        private System.Windows.Forms.ComboBox ddlTemplateSubject;
        private System.Windows.Forms.Label lblTemplateSubject;
        private System.Windows.Forms.Button btnAddNewTemplate;
        private System.Windows.Forms.Button btnSaveTemplate;
        private System.Windows.Forms.CheckBox chkIsActive;
        private System.Windows.Forms.Button btnChangeSubject;
        private System.Windows.Forms.Button btnDeleteSubject;
        private System.Windows.Forms.Button btnClose;
    }
}