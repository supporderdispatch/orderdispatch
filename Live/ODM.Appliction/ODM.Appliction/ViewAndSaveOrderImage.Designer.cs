﻿namespace ODM.Appliction
{
    partial class ViewAndSaveOrderImage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewAndSaveOrderImage));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolstripButtonRotate = new System.Windows.Forms.ToolStripDropDownButton();
            this.rotateRight90ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateLeft90ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotate180ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flipVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flipHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveRotatedImage = new System.Windows.Forms.ToolStripButton();
            this.btnSaveOriginalImage = new System.Windows.Forms.ToolStripButton();
            this.pbOrderImage = new System.Windows.Forms.PictureBox();
            this.pnlPbOrderImage = new System.Windows.Forms.Panel();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbOrderImage)).BeginInit();
            this.pnlPbOrderImage.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolstripButtonRotate,
            this.btnSaveRotatedImage,
            this.btnSaveOriginalImage});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(917, 25);
            this.toolStrip1.TabIndex = 25;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolstripButtonRotate
            // 
            this.toolstripButtonRotate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rotateRight90ToolStripMenuItem,
            this.rotateLeft90ToolStripMenuItem,
            this.rotate180ToolStripMenuItem,
            this.flipVerticalToolStripMenuItem,
            this.flipHorizontalToolStripMenuItem});
            this.toolstripButtonRotate.Image = ((System.Drawing.Image)(resources.GetObject("toolstripButtonRotate.Image")));
            this.toolstripButtonRotate.ImageTransparentColor = System.Drawing.Color.White;
            this.toolstripButtonRotate.Name = "toolstripButtonRotate";
            this.toolstripButtonRotate.Size = new System.Drawing.Size(70, 22);
            this.toolstripButtonRotate.Text = "Rotate";
            // 
            // rotateRight90ToolStripMenuItem
            // 
            this.rotateRight90ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rotateRight90ToolStripMenuItem.Image")));
            this.rotateRight90ToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.rotateRight90ToolStripMenuItem.Name = "rotateRight90ToolStripMenuItem";
            this.rotateRight90ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.rotateRight90ToolStripMenuItem.Text = "Rotate right 90°";
            this.rotateRight90ToolStripMenuItem.Click += new System.EventHandler(this.rotateRight90ToolStripMenuItem_Click);
            // 
            // rotateLeft90ToolStripMenuItem
            // 
            this.rotateLeft90ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rotateLeft90ToolStripMenuItem.Image")));
            this.rotateLeft90ToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.rotateLeft90ToolStripMenuItem.Name = "rotateLeft90ToolStripMenuItem";
            this.rotateLeft90ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.rotateLeft90ToolStripMenuItem.Text = "Rotate left 90°";
            this.rotateLeft90ToolStripMenuItem.Click += new System.EventHandler(this.rotateLeft90ToolStripMenuItem_Click);
            // 
            // rotate180ToolStripMenuItem
            // 
            this.rotate180ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rotate180ToolStripMenuItem.Image")));
            this.rotate180ToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.rotate180ToolStripMenuItem.Name = "rotate180ToolStripMenuItem";
            this.rotate180ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.rotate180ToolStripMenuItem.Text = "Rotate 180°";
            this.rotate180ToolStripMenuItem.Click += new System.EventHandler(this.rotate180ToolStripMenuItem_Click);
            // 
            // flipVerticalToolStripMenuItem
            // 
            this.flipVerticalToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("flipVerticalToolStripMenuItem.Image")));
            this.flipVerticalToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.flipVerticalToolStripMenuItem.Name = "flipVerticalToolStripMenuItem";
            this.flipVerticalToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.flipVerticalToolStripMenuItem.Text = "Flip vertical";
            this.flipVerticalToolStripMenuItem.Click += new System.EventHandler(this.flipVerticalToolStripMenuItem_Click);
            // 
            // flipHorizontalToolStripMenuItem
            // 
            this.flipHorizontalToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("flipHorizontalToolStripMenuItem.Image")));
            this.flipHorizontalToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.flipHorizontalToolStripMenuItem.Name = "flipHorizontalToolStripMenuItem";
            this.flipHorizontalToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.flipHorizontalToolStripMenuItem.Text = "Flip horizontal";
            this.flipHorizontalToolStripMenuItem.Click += new System.EventHandler(this.flipHorizontalToolStripMenuItem_Click);
            // 
            // btnSaveRotatedImage
            // 
            this.btnSaveRotatedImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSaveRotatedImage.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveRotatedImage.Image")));
            this.btnSaveRotatedImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveRotatedImage.Name = "btnSaveRotatedImage";
            this.btnSaveRotatedImage.Size = new System.Drawing.Size(79, 22);
            this.btnSaveRotatedImage.Text = "Save Rotated";
            this.btnSaveRotatedImage.Click += new System.EventHandler(this.btnSaveRotatedImage_Click);
            // 
            // btnSaveOriginalImage
            // 
            this.btnSaveOriginalImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSaveOriginalImage.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveOriginalImage.Image")));
            this.btnSaveOriginalImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveOriginalImage.Name = "btnSaveOriginalImage";
            this.btnSaveOriginalImage.Size = new System.Drawing.Size(80, 22);
            this.btnSaveOriginalImage.Text = "Save Original";
            this.btnSaveOriginalImage.Click += new System.EventHandler(this.btnSaveOriginalImage_Click);
            // 
            // pbOrderImage
            // 
            this.pbOrderImage.Location = new System.Drawing.Point(3, 3);
            this.pbOrderImage.Name = "pbOrderImage";
            this.pbOrderImage.Size = new System.Drawing.Size(100, 50);
            this.pbOrderImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbOrderImage.TabIndex = 0;
            this.pbOrderImage.TabStop = false;
            // 
            // pnlPbOrderImage
            // 
            this.pnlPbOrderImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPbOrderImage.AutoScroll = true;
            this.pnlPbOrderImage.Controls.Add(this.pbOrderImage);
            this.pnlPbOrderImage.Location = new System.Drawing.Point(12, 52);
            this.pnlPbOrderImage.Name = "pnlPbOrderImage";
            this.pnlPbOrderImage.Size = new System.Drawing.Size(893, 489);
            this.pnlPbOrderImage.TabIndex = 26;
            // 
            // ViewAndSaveOrderImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 547);
            this.Controls.Add(this.pnlPbOrderImage);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewAndSaveOrderImage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ViewAndSaveOrderImage";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbOrderImage)).EndInit();
            this.pnlPbOrderImage.ResumeLayout(false);
            this.pnlPbOrderImage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolstripButtonRotate;
        private System.Windows.Forms.ToolStripMenuItem rotateRight90ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateLeft90ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotate180ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flipVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flipHorizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnSaveRotatedImage;
        private System.Windows.Forms.ToolStripButton btnSaveOriginalImage;
        private System.Windows.Forms.PictureBox pbOrderImage;
        private System.Windows.Forms.Panel pnlPbOrderImage;
    }
}