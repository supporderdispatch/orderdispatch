﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class AddEBayAccountAppKeys : Form
    {
        Guid AccountID = new Guid();
        Guid ID = new Guid();
        string strMode = string.Empty;
        public AddEBayAccountAppKeys(Guid ID, string strMode)
        {
            InitializeComponent();
            this.ID = ID;
            this.strMode = strMode;
            BindDropDownList();
        }
        public AddEBayAccountAppKeys(Guid AccountID, string strMode, Guid ID, string RuName, string CertKey, string DevKey, string AppKey,string AppKeysName)
        {
            InitializeComponent();
            this.AccountID = AccountID;
            this.strMode = strMode;
            this.ID = ID;
            TxtRuName.Text = RuName;
            TxtCertificate.Text = CertKey;
            TxtDeveloper.Text = DevKey;
            TxtApplication.Text = AppKey;
            TxtAppKeysName.Text = AppKeysName;
            BindDropDownList();
            btnSaveAppKeys.Text = "Update";
        }

        private void BindDropDownList()
        {
            DataTable objDt = new DataTable();
            objDt = GlobalVariablesAndMethods.GetMessageAccount("EBAY");
            if (objDt.Rows.Count > 0)
            {
                DataView dv = new DataView(objDt);
                dv.RowFilter = "IsEbay=true";
                DataTable objDtEbayAccounts = dv.ToTable();
                ddlEbayAccounts.DataSource = objDtEbayAccounts;
                ddlEbayAccounts.ValueMember = "ID";
                ddlEbayAccounts.DisplayMember = "AccountName";
                if (this.AccountID.ToString() != "00000000-0000-0000-0000-000000000000")
                {
                    ddlEbayAccounts.SelectedValue = this.AccountID;
                }

            }
            else
            {
                MessageBox.Show("Please Add an Ebay Account First.", "Add Ebay App Keys", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }


        }
        private void btnSaveAppKeys_Click(object sender, EventArgs e)
        {
            string strResult = AddUpdateEbayAccountAppKeys(new Guid(ddlEbayAccounts.SelectedValue.ToString()), strMode, ID);
            if (strResult == "1")
            {
                MessageBox.Show("Application Keys Added Successfully!", "Add Ebay App Keys", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else if (strResult == "2")
            {
                MessageBox.Show("Application Keys updated Successfully!", "Update Ebay App Keys", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            this.Close();
        }
        public string AddUpdateEbayAccountAppKeys(Guid AccountID, string strMode, Guid ID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            DataTable objDt = new DataTable();
            DataSet objDs = new DataSet();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[9];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode;

                objParam[1] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = ID;

                objParam[2] = new SqlParameter("@AccountId", SqlDbType.UniqueIdentifier);
                objParam[2].Value = AccountID;

                objParam[3] = new SqlParameter("@RuName", SqlDbType.VarChar, 100);
                objParam[3].Value = TxtRuName.Text.ToString();

                objParam[4] = new SqlParameter("@CertKey", SqlDbType.VarChar, 100);
                objParam[4].Value = TxtCertificate.Text.ToString();

                objParam[5] = new SqlParameter("@AppKey", SqlDbType.VarChar, 100);
                objParam[5].Value = TxtApplication.Text.ToString();

                objParam[6] = new SqlParameter("@DevKey", SqlDbType.VarChar, 100);
                objParam[6].Value = TxtDeveloper.Text.ToString();

                objParam[7] = new SqlParameter("@AppKeysName", SqlDbType.VarChar, 100);
                objParam[7].Value = TxtAppKeysName.Text.ToString();


                objParam[8] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[8].Direction = ParameterDirection.Output;
                if (strMode.ToUpper() == "UPDATE" || strMode.ToUpper() == "INSERT")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "AddUpdateEbayAppKeys", objParam);
                }



                string strResult = objParam[7].Value.ToString();


                return strResult;






            }
            catch (Exception ex)
            {
                throw;
            }
            finally { conn.Close(); }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
