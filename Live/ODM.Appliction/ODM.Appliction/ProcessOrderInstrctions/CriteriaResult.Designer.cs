﻿namespace ODM.Appliction.ProcessOrderInstrctions
{
    partial class CriteriaResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CriteriaResult));
            this.picImageResult = new System.Windows.Forms.PictureBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.richTextResult = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbResultType = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picImageResult)).BeginInit();
            this.SuspendLayout();
            // 
            // picImageResult
            // 
            this.picImageResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picImageResult.Location = new System.Drawing.Point(28, 95);
            this.picImageResult.Name = "picImageResult";
            this.picImageResult.Size = new System.Drawing.Size(277, 155);
            this.picImageResult.TabIndex = 0;
            this.picImageResult.TabStop = false;
            // 
            // btnBrowse
            // 
            this.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowse.Location = new System.Drawing.Point(28, 268);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // richTextResult
            // 
            this.richTextResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextResult.Location = new System.Drawing.Point(327, 95);
            this.richTextResult.Name = "richTextResult";
            this.richTextResult.Size = new System.Drawing.Size(251, 155);
            this.richTextResult.TabIndex = 2;
            this.richTextResult.Text = "";
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(315, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1, 155);
            this.label1.TabIndex = 3;
            // 
            // cmbResultType
            // 
            this.cmbResultType.Enabled = false;
            this.cmbResultType.FormattingEnabled = true;
            this.cmbResultType.Location = new System.Drawing.Point(28, 56);
            this.cmbResultType.Name = "cmbResultType";
            this.cmbResultType.Size = new System.Drawing.Size(121, 21);
            this.cmbResultType.TabIndex = 4;
            this.cmbResultType.SelectedIndexChanged += new System.EventHandler(this.cmbResultType_SelectedIndexChanged);
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(521, 55);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save Result";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // CriteriaResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 340);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cmbResultType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextResult);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.picImageResult);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CriteriaResult";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CriteriaResult";
            ((System.ComponentModel.ISupportInitialize)(this.picImageResult)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picImageResult;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.RichTextBox richTextResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbResultType;
        private System.Windows.Forms.Button btnSave;
    }
}