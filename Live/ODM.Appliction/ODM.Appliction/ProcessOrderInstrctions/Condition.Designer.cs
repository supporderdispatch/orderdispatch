﻿namespace ODM.Appliction.ProcessOrderInstrctions
{
    partial class Condition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Condition));
            this.btnAddCriteriaToProcessOrder = new System.Windows.Forms.Button();
            this.btnSaveProcessOrderCriteria = new System.Windows.Forms.Button();
            this.grdCondition = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.grdCondition)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddCriteriaToProcessOrder
            // 
            this.btnAddCriteriaToProcessOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCriteriaToProcessOrder.Location = new System.Drawing.Point(27, 23);
            this.btnAddCriteriaToProcessOrder.Name = "btnAddCriteriaToProcessOrder";
            this.btnAddCriteriaToProcessOrder.Size = new System.Drawing.Size(150, 23);
            this.btnAddCriteriaToProcessOrder.TabIndex = 0;
            this.btnAddCriteriaToProcessOrder.Text = "Add Criteria";
            this.btnAddCriteriaToProcessOrder.UseVisualStyleBackColor = true;
            this.btnAddCriteriaToProcessOrder.Click += new System.EventHandler(this.btnAddCriteriaToProcessOrder_Click);
            // 
            // btnSaveProcessOrderCriteria
            // 
            this.btnSaveProcessOrderCriteria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveProcessOrderCriteria.Location = new System.Drawing.Point(210, 23);
            this.btnSaveProcessOrderCriteria.Name = "btnSaveProcessOrderCriteria";
            this.btnSaveProcessOrderCriteria.Size = new System.Drawing.Size(150, 23);
            this.btnSaveProcessOrderCriteria.TabIndex = 1;
            this.btnSaveProcessOrderCriteria.Text = "Save";
            this.btnSaveProcessOrderCriteria.UseVisualStyleBackColor = true;
            this.btnSaveProcessOrderCriteria.Click += new System.EventHandler(this.btnSaveProcessOrderCriteria_Click);
            // 
            // grdCondition
            // 
            this.grdCondition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdCondition.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grdCondition.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdCondition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCondition.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdCondition.Location = new System.Drawing.Point(27, 67);
            this.grdCondition.Name = "grdCondition";
            this.grdCondition.Size = new System.Drawing.Size(774, 342);
            this.grdCondition.TabIndex = 6;
            // 
            // Condition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 421);
            this.Controls.Add(this.grdCondition);
            this.Controls.Add(this.btnSaveProcessOrderCriteria);
            this.Controls.Add(this.btnAddCriteriaToProcessOrder);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Condition";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Condition";
            ((System.ComponentModel.ISupportInitialize)(this.grdCondition)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddCriteriaToProcessOrder;
        private System.Windows.Forms.Button btnSaveProcessOrderCriteria;
        private System.Windows.Forms.DataGridView grdCondition;
    }
}