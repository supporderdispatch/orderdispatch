﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Data.DbContext;
using ODM.Entities;
using ODM.Core;
using System.Linq;
using ODM.Entities.InvoiceTemplate;

namespace ODM.Appliction.ProcessOrderInstrctions
{
    public partial class Condition : Form
    {
        private Guid ConditionId;
        private string base64String;
        private DataTable dt;
        private Guid guid;


        public Condition()
        {
            InitializeComponent();
            InitializeConditionGrid();
            RegisterEvents();
        }

        public Condition(Guid guid)
        {
            // TODO: Complete member initialization
            ConditionId = guid;
            InitializeComponent();
            InitializeConditionGrid();
            RegisterEvents();
        }

        private void RegisterEvents()
        {
            grdCondition.EditingControlShowing += grdCondition_EditingControlShowing;
            grdCondition.CellContentClick += grdCondition_CellContentClick;
            grdCondition.CurrentCellDirtyStateChanged += grdCondition_CurrentCellDirtyStateChanged;
            grdCondition.DataError += grdCondition_DataError;
        }

        void grdCondition_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //throw new NotImplementedException();
        }

        void grdCondition_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            btnSaveProcessOrderCriteria.Font = new Font(btnSaveProcessOrderCriteria.Font.Name, btnSaveProcessOrderCriteria.Font.Size, FontStyle.Bold);
        }

        void grdCondition_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var selectedRow=grdCondition.Rows[e.RowIndex];
            var selectedGridRowCell=selectedRow.Cells[e.ColumnIndex];
            if (e.RowIndex >= 0)
            {
                if (selectedGridRowCell.Value.ToString().Equals("Delete"))
                {
                    var criteriaId = selectedRow.Cells["ConditionCriteriaId"].Value.ToString();
                    if (criteriaId.GetType() == typeof(DBNull) || string.IsNullOrEmpty(criteriaId))
                    {
                        grdCondition.Rows.Remove(grdCondition.Rows[e.RowIndex]);
                    }
                    else
                    {
                        Activator.CreateInstance<ODM.Core.ProcessOrderInstructions.ProcessOrderCondition>().DeleteCriteria(Guid.Parse(criteriaId));
                        RenderGridWithControls();
                        return;
                    }
                    
                }
                else if (selectedGridRowCell.Value.ToString().Equals("EditResult"))
                {
                    var criteriaId = selectedRow.Cells["ConditionCriteriaId"].Value.ToString();
                    var resultType = selectedRow.Cells["CriteriaResultType"].Value;
                    resultType=resultType.GetType().Equals(typeof(DBNull))?"Text":selectedRow.Cells["CriteriaResultType"].Value??"Text";

                    if (criteriaId.GetType()==typeof(DBNull) || string.IsNullOrEmpty(criteriaId))
                    {
                        if (SaveAllCriteria())
                        {
                            btnSaveProcessOrderCriteria.PerformClick();
                            grdCondition_CellContentClick(sender, e);
                        }
                    }
                    else
                    {
                        var creteriaResultOpenForm = Application.OpenForms["CriteriaResult"];
                        if(creteriaResultOpenForm==null)
                        {
                            var creteriaResult = new ODM.Appliction.ProcessOrderInstrctions.CriteriaResult(Guid.Parse(criteriaId),resultType.ToString());
                            if (creteriaResult.ShowDialog() != null)
                            {
                                RenderGridWithControls();
                            }
                        }
                    }
                   
                }
            }
        }

        void grdCondition_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.CellStyle.BackColor = SystemColors.ButtonFace;
            ComboBox combo = e.Control as ComboBox;
            if (combo != null)
            {
                combo.SelectedIndexChanged -=
                    new EventHandler(ComboBox_SelectedIndexChanged);

                combo.SelectedIndexChanged +=
                    new EventHandler(ComboBox_SelectedIndexChanged);
            }
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cell = sender as ComboBox;
            if (GetTables().AsEnumerable().Select(x => x.Field<string>("Table")).ToList().Contains(cell.Text))
            {
                var gridCel = grdCondition.CurrentRow.Cells["Column"];
                var gridCell = gridCel as DataGridViewComboBoxCell;
                gridCell.DataSource = GetColumns(cell.Text);
            }
            grdCondition.Refresh();
        }

        private object GetColumns(string tableName)
        {
            return typeof(VirtualTables).GetProperty(tableName).GetValue(VirtualTables.Order);
        }

        private void InitializeConditionGrid()
        {
            
            grdCondition.DoubleBuffered(true);
            grdCondition.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdCondition.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdCondition.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdCondition.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdCondition.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdCondition.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdCondition.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdCondition.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdCondition.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdCondition.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdCondition.ScrollBars = ScrollBars.Both;
            grdCondition.RowHeadersVisible = false;
            grdCondition.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdCondition.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdCondition.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdCondition.EnableHeadersVisualStyles = false;
            grdCondition.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdCondition.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdCondition.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdCondition.DefaultCellStyle.Padding = new Padding(5, 5, 5, 5);
            grdCondition.CellBorderStyle = DataGridViewCellBorderStyle.RaisedHorizontal;
            grdCondition.AllowUserToAddRows = false;
            RenderGridWithControls();
        }

        private void RenderGridWithControls()
        {
            grdCondition.Visible = false;
            grdCondition.DataSource = null;
            grdCondition.Refresh();
            grdCondition.Rows.Clear();
            grdCondition.Columns.Clear();

            ConditionId = ConditionId == Guid.Empty ? Guid.NewGuid() : ConditionId;
            grdCondition.DataSource = GetUpdatedDataSource(Activator.CreateInstance<ODM.Core.ProcessOrderInstructions.ProcessOrderCondition>()
                .GetConditinsFor(ConditionId.ToString()));
            grdCondition.Columns["ConditionId"].Visible = false;
            grdCondition.Columns["ConditionCriteriaId"].Visible = false;
            grdCondition.Columns["TableName"].Visible = false;
            grdCondition.Columns["ColumnName"].Visible = false;
            grdCondition.Columns["Creterion"].Visible = false;
            grdCondition.Columns["CriteriaDataType"].Visible = false;
            grdCondition.Columns["ResultType"].Visible = false;

            DataGridViewComboBoxColumn table = new DataGridViewComboBoxColumn();
            table.HeaderText = "Table";
            table.Name = "Table";
            table.DataSource = GetTables();
            table.DataPropertyName = "TableName";
            table.ValueMember = "Table";
            table.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            table.FlatStyle = FlatStyle.Flat;
            grdCondition.Columns.Insert(0, table);

            DataGridViewComboBoxColumn column = new DataGridViewComboBoxColumn();
            column.HeaderText = "Column";
            column.Name = "Column";
            column.DataSource = GetAllColumns();
            column.FlatStyle = FlatStyle.Flat;
            column.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            column.DataPropertyName = "ColumnName";
            grdCondition.Columns.Insert(1, column);

            DataGridViewComboBoxColumn criteria = new DataGridViewComboBoxColumn();
            criteria.HeaderText = "criteria";
            criteria.Name = "criteria";
            criteria.FlatStyle = FlatStyle.Flat;
            criteria.DataSource = Enum.GetNames(typeof(PrintConditionCreterion));
            criteria.DataPropertyName = "Creterion";
            grdCondition.Columns.Insert(2, criteria);

            grdCondition.Columns["CompareWith"].DisplayIndex = 3;

            DataGridViewComboBoxColumn criteriaDataType = new DataGridViewComboBoxColumn();
            criteriaDataType.HeaderText = "criteriaDataType";
            criteriaDataType.Name = "criteriaDataType";
            criteriaDataType.FlatStyle = FlatStyle.Flat;
            criteriaDataType.DataSource = Enum.GetNames(typeof(CustomDataTypes));
            criteriaDataType.DataPropertyName = "CriteriaDataType";
            grdCondition.Columns.Insert(4, criteriaDataType);


            DataGridViewComboBoxColumn criteriaResult = new DataGridViewComboBoxColumn();
            criteriaResult.HeaderText = "Result Type";
            criteriaResult.Name = "CriteriaResultType";
            criteriaResult.FlatStyle = FlatStyle.Flat;
            criteriaResult.DataSource = Enum.GetNames(typeof(ODM.Entities.ProcessOrderInstrunctions.CriteriaResultType));
            criteriaResult.DataPropertyName = "ResultType";
            criteriaResult.DefaultCellStyle.NullValue = "Text";
            grdCondition.Columns.Insert(5, criteriaResult);

            DataGridViewButtonColumn getSetCriteriaResult = new DataGridViewButtonColumn();
            getSetCriteriaResult.HeaderText = "";
            getSetCriteriaResult.Text = "EditResult";
            getSetCriteriaResult.UseColumnTextForButtonValue = true;
            getSetCriteriaResult.Name = "EditCriteriaResult";
            getSetCriteriaResult.FlatStyle = FlatStyle.Flat;
            getSetCriteriaResult.DefaultCellStyle.ForeColor = Color.Black;
            grdCondition.Columns.Insert(6, getSetCriteriaResult);        


            DataGridViewButtonColumn delete = new DataGridViewButtonColumn();
            delete.HeaderText = "";
            delete.Text = "Delete";
            delete.UseColumnTextForButtonValue = true;
            delete.Name = "Delete";
            delete.FlatStyle = FlatStyle.Flat;
            delete.DefaultCellStyle.ForeColor = Color.Black;
            grdCondition.Columns.Insert(7, delete);        
   
            grdCondition.AllowUserToAddRows = false;
            grdCondition.ClearSelection();
            grdCondition.Visible = true;
            grdCondition.AllowUserToAddRows = false;
            grdCondition.ClearSelection();
            grdCondition.Visible = true;

            grdCondition.Refresh();

        }

        private DataTable GetUpdatedDataSource(DataTable dataTable)
        {
            foreach(DataRow row in dataTable.Rows)
            {
                var resultType = (string)(row["ResultType"].GetType() == typeof(DBNull) ?""  : row["ResultType"]??"");
                if (string.IsNullOrEmpty(resultType) || resultType.GetType() == typeof(DBNull))
                {
                    row["ResultType"] = ODM.Entities.ProcessOrderInstrunctions.CriteriaResultType.Text.ToString();
                }
            }
            return dataTable;
        }

        private void LoadConditionValidation()
        {
            
        }
        private DataTable GetTables()
        {
            var tables = new DataTable();
            tables.Columns.Add("Table");
            foreach (var tableName in typeof(VirtualTables).GetProperties())
            {
                tables.Rows.Add(tableName.Name);
            }
            return tables;
        }
        private object GetAllColumns()
        {
            var result = new List<string>();
            foreach (var tableName in typeof(VirtualTables).GetProperties())
            {
                var list = typeof(VirtualTables).GetProperty(tableName.Name).GetValue(VirtualTables.Order);
                result.AddRange((List<string>)(list));
            }
            return result;
        }

        private void btnAddCriteriaToProcessOrder_Click(object sender, EventArgs e)
        {
            SaveAllCriteria();
            var Condition = new ConditionCriteria()
            {
              ConditionId = ConditionId
            };
            Activator.CreateInstance<ODM.Core.ProcessOrderInstructions.ProcessOrderCondition>()
               .AddEmptyCondition(ConditionId.ToString(), Condition);
            RenderGridWithControls();
         
           
        }

        private bool SaveAllCriteria()
        {
            var result = true;
            var ConditionCriterias = new List<ConditionCriteria>();
            foreach (DataGridViewRow row in grdCondition.Rows)
            {
                try
                {
                    var conditionCriteria = new ConditionCriteria();
                    var conditionId = row.Cells["ConditionCriteriaId"].Value.ToString();
                    conditionId = string.IsNullOrEmpty(conditionId) ? Guid.Empty.ToString() : conditionId;

                    conditionCriteria.ConditionId = ConditionId;
                    conditionCriteria.ConditionCriteriaId = conditionId.Equals(Guid.Empty.ToString()) ? Guid.NewGuid() : Guid.Parse(conditionId);
                    conditionCriteria.TableName = (string)row.Cells["Table"].Value ?? string.Empty;
                    conditionCriteria.ColumnName = (string)row.Cells["Column"].Value ?? string.Empty;
                    conditionCriteria.Creterion = (string)row.Cells["criteria"].Value ?? string.Empty;
                    conditionCriteria.CompareWith = (string)row.Cells["CompareWith"].Value ?? string.Empty;
                    conditionCriteria.CriteriaDataType = (string)row.Cells["criteriaDataType"].Value ?? string.Empty;
                    var resultType = row.Cells["CriteriaResultType"].Value;
                    resultType = resultType.GetType().Equals(typeof(DBNull)) ? "Text" : resultType;
                    conditionCriteria.ResultType = (ODM.Entities.ProcessOrderInstrunctions.CriteriaResultType)Enum.Parse(typeof(ODM.Entities.ProcessOrderInstrunctions.CriteriaResultType), string.IsNullOrEmpty(resultType.ToString())?"Text":resultType.ToString());
                    ConditionCriterias.Add(conditionCriteria);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please fill all the fields");
                    result = false;
                    break;
                }
            }
            Activator.CreateInstance<ODM.Core.ProcessOrderInstructions.ProcessOrderCondition>().SaveConditionsCriteriaFor(ConditionId.ToString(), ConditionCriterias);
            btnSaveProcessOrderCriteria.Font = new Font(btnSaveProcessOrderCriteria.Font.Name, btnSaveProcessOrderCriteria.Font.Size, FontStyle.Regular);
            return result;
        }

        private void btnSaveProcessOrderCriteria_Click(object sender, EventArgs e)
        {
            SaveAllCriteria();
            RenderGridWithControls();

        }      
    }
}
