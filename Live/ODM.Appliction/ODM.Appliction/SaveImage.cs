﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class SaveImage : Form
    {
        public SaveImage(Image img)
        {
            InitializeComponent();
            picBoxSaveImage.Image = img;
            //picBoxSaveImage.Size = imgSize;
            populateControls();


        }
        public void populateControls()
        {

            this.picBoxSaveImage.Size = picBoxSaveImage.Image.Size;

        }
        //public bool ImageSaved = false;
        [System.ComponentModel.DefaultValue(false)]
        public bool blIsImageSaved { get; set; }
        private void btnTryAgain_Click(object sender, EventArgs e)
        {
            this.Close();
            Form frmCaptureImage = Application.OpenForms["CaptureImage"];
            if (frmCaptureImage == null)
            {
                CaptureImage objCaptureImage = new CaptureImage();
                objCaptureImage.Show();
            }

        }
        private void btnSaveImage_Click(object sender, EventArgs e)
        {
            //cropImage();
            string strImagetoBase64 = ImageToBase64(picBoxSaveImage.Image);
            this.blIsImageSaved = SaveImageByOrderId(GlobalVariablesAndMethods.OrderIdToLog, "UPDATE", strImagetoBase64);
            if (this.blIsImageSaved)
            {
                this.Close();
            }
        }

        public string ImageToBase64(Image img)
        {
            string strImageToBase64 = "";
            using (MemoryStream m = new MemoryStream())
            {
                try
                {
                    img.Save(m, ImageFormat.Jpeg);
                    byte[] imageBytes = m.ToArray();
                    strImageToBase64 = Convert.ToBase64String(imageBytes);
                }
                catch (Exception)
                {
                    throw;
                }

            }

            return strImageToBase64;
        }
        public bool SaveImageByOrderId(Guid OrderId, string strMode, string strImagetoBase64)
        {
            string strResult = GlobalVariablesAndMethods.GetAddUpdateDeleteImageByOrderId("Update", OrderId, strImagetoBase64);
            if (strResult == "1" || strResult == "2")
            {   //for strResult = 2, image is saved into the database
                //for strResult = 1, image is updated into the database
                //  MessageBox.Show("Image saved successfully!", "Save Image", MessageBoxButtons.OK, MessageBoxIcon.Information);
                GlobalVariablesAndMethods.LogHistory("&&Image Captured");
                this.blIsImageSaved = true;
            }
            return this.blIsImageSaved;


        }

        private void SaveImage_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.blIsImageSaved)
            {

            }
            else
            {
                this.blIsImageSaved = false;
            }
            e.Cancel = false;

        }


    }
}
