﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class AddEmailAccount : Form
    {
        public AddEmailAccount()
        {
            InitializeComponent();
        }
        public AddEmailAccount(DataView objDvEmailAccounts)
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddEamilAccount();
        }
        private void AddEamilAccount()
        {
            if (string.IsNullOrEmpty(txtAccountName.Text))
            {
                lblAccountNameRequired.Visible = true;
            }
            else
            {
                lblAccountNameRequired.Visible = false;
                try
                {
                    AdditionalEmailAccount objAdditionalAccount = new AdditionalEmailAccount();
                    string strResult = objAdditionalAccount.AddUpdateDeleteEmailAccount(strMode: "ADD", strEmailAccount: txtAccountName.Text);
                    if (strResult != "")
                    {
                        MessageBox.Show(strResult, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtAccountName.SelectAll();
                        txtAccountName.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Record Saved Successfully", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtAccountName.Focus();
                        txtAccountName.SelectAll();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Error occurred while adding email account", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtAccountName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                AddEamilAccount();
            }
        }
        private void AddEmailAccount_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form frmAdditionalAccount = Application.OpenForms["AdditionalEmailAccount"];
            if (frmAdditionalAccount != null)
            {
                frmAdditionalAccount.Close();
                AdditionalEmailAccount AdditionalAccount = new AdditionalEmailAccount();
                AdditionalAccount.Show();
            }
        }
    }
}
