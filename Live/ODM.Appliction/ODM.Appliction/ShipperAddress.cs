﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class ShipperAddress : Form
    {
        #region "Global Variables"
        string _CourierName = "";
        string _CourierID = "";
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        #endregion

        #region "Constructors"
        public ShipperAddress()
        {
            InitializeComponent();
        }
        public ShipperAddress(string strCourierName, string strCourierID)
        {
            InitializeComponent();
            _CourierName = strCourierName;
            _CourierID = strCourierID;
            PopulateControls();
        }
        #endregion

        #region "Methods"
        private void PopulateControls()
        {
            try
            {
                DataTable ObjdtCourier = new DataTable();
                ObjdtCourier = GlobalVar.GetCourierDetailsByNameOrId(_CourierName, _CourierID);
                if (ObjdtCourier.Rows.Count > 0)
                {
                    BindCountryDDL();
                    txtCompany.Text = Convert.ToString(ObjdtCourier.Rows[0]["Company"]);
                    txtName.Text = Convert.ToString(ObjdtCourier.Rows[0]["Name"]);
                    txtAddressLine1.Text = Convert.ToString(ObjdtCourier.Rows[0]["AddressLine1"]);
                    txtAddressLine2.Text = Convert.ToString(ObjdtCourier.Rows[0]["AddressLine2"]);
                    txtStateRegion.Text = Convert.ToString(ObjdtCourier.Rows[0]["State"]);
                    txtCityTown.Text = Convert.ToString(ObjdtCourier.Rows[0]["City"]);
                    txtPostCode.Text = Convert.ToString(ObjdtCourier.Rows[0]["ZipCode"]);
                    txtTelephone.Text = Convert.ToString(ObjdtCourier.Rows[0]["Phone"]);
                    ddlCountry.SelectedValue = Convert.ToString(ObjdtCourier.Rows[0]["CountryID"]);
                    txtEmail.Text = Convert.ToString(ObjdtCourier.Rows[0]["Email"]);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void BindCountryDDL()
        {
            try
            {
                ddlCountry.ValueMember = "CountryId";
                ddlCountry.DisplayMember = "CountryName";
                ddlCountry.DataSource = GlobalVar.GetAllCountries();
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void SaveSenderDetails()
        {
            try
            {
                SqlConnection connection = new SqlConnection();

                Guid CountryID = new Guid(ddlCountry.SelectedValue.ToString());
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();

                SqlParameter[] objParam = new SqlParameter[10];

                objParam[0] = new SqlParameter("@Company", SqlDbType.VarChar, 100);
                objParam[0].Value = txtCompany.Text;

                objParam[1] = new SqlParameter("@Name", SqlDbType.VarChar, 100);
                objParam[1].Value = txtName.Text;

                objParam[2] = new SqlParameter("@AddressLine1", SqlDbType.VarChar, 100);
                objParam[2].Value = txtAddressLine1.Text;

                objParam[3] = new SqlParameter("@AddressLine2", SqlDbType.VarChar, 100);
                objParam[3].Value = txtAddressLine2.Text;

                objParam[4] = new SqlParameter("@City", SqlDbType.VarChar, 100);
                objParam[4].Value = txtCityTown.Text;

                objParam[5] = new SqlParameter("@State", SqlDbType.VarChar, 100);
                objParam[5].Value = txtStateRegion.Text;

                objParam[6] = new SqlParameter("@ZipCode", SqlDbType.VarChar, 50);
                objParam[6].Value = txtPostCode.Text;

                objParam[7] = new SqlParameter("@Phone", SqlDbType.VarChar, 50);
                objParam[7].Value = txtTelephone.Text;

                objParam[8] = new SqlParameter("@CountryID", SqlDbType.UniqueIdentifier);
                objParam[8].Value = CountryID;

                objParam[9] = new SqlParameter("@Email", SqlDbType.VarChar, 50);
                objParam[9].Value = txtEmail.Text;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateShipperDetails", objParam);
                this.Close();
                MessageBox.Show("Sender Details Saved Sucessfully");
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool ValidateControls()
        {
            bool IsValid = true;
            if (string.IsNullOrWhiteSpace(txtCompany.Text))
            {
                lblCompanyRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblCompanyRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                lblNameRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblNameRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAddressLine1.Text))
            {
                lblAddressLine1Required.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAddressLine1Required.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAddressLine2.Text))
            {
                lblAddressLine2Required.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAddressLine2Required.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtStateRegion.Text))
            {
                lblStateRegionRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblStateRegionRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtTelephone.Text))
            {
                lblPhoneRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPhoneRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPostCode.Text))
            {
                lblPostCodeRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPostCodeRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtCityTown.Text))
            {
                lblCityTownRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblCityTownRequired.Visible = false;
            }

            if (ddlCountry.Text != "" && ddlCountry.SelectedValue == null)
            {
                lblCountryRequired.Visible = true;
                lblCountryRequired.Text = "Invalid Country";
                IsValid = false;
            }
            else if (ddlCountry.Text == "" && ddlCountry.SelectedValue == null)
            {
                lblCountryRequired.Visible = true;
                lblCountryRequired.Text = "Required";
                IsValid = false;
            }
            else
            {
                lblCountryRequired.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(txtEmail.Text))
            {
                lblEmailRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblEmailRequired.Visible = false;
            }
            return IsValid;
        }
        #endregion

        #region "Events"
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                SaveSenderDetails();
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

    }
}
