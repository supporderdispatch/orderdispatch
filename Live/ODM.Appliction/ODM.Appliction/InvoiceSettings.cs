﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class InvoiceSettings : Form
    {
        public InvoiceSettings()
        {
            InitializeComponent();
            InitializeControls();
            RegisterEvents();
        }

        private void RegisterEvents()
        {
            this.grdInvoiceTemplates.CellContentClick += grdInvoiceTemplates_CellContentClick;
            this.grdInvoiceTemplates.CurrentCellDirtyStateChanged += grdInvoiceTemplates_CurrentCellDirtyStateChanged;
        }

        void grdInvoiceTemplates_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            btnSaveSettings.Font = new Font(btnSaveSettings.Font.Name, btnSaveSettings.Font.Size, FontStyle.Bold);
        }

        private void grdInvoiceTemplates_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                var row = senderGrid.Rows[e.RowIndex];
                var buttonText = row.Cells[e.ColumnIndex].Value.ToString();
                var templateId = row.Cells["ID"].Value.ToString();
                var templateName = row.Cells["TemplateName"].Value.ToString();
                switch (buttonText)
                {
                    case "Edit":
                        EditTemplate(templateId);
                        break;

                    case "Delete":
                        DeleteTemplate(templateId, templateName);
                        break;

                    case "Copy":
                        CopyTemplate(templateId);
                        break;

                    default:
                        break;
                }
            }
        }

        private void CopyTemplate(string templateId)
        {
            Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().CopyTemplate(templateId);
            PopulateGridWithTemplatesData();
        }
        private void DeleteTemplate(string templateId, string templateName)
        {
            DialogResult result = MessageBox.Show("Do You Want To Delete \" " + templateName + " " + "\"?",
                "Delete Invoice Template",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().DeleteInvoiceTemplate(templateId);
                PopulateGridWithTemplatesData();
            }
            else
            {
                PopulateGridWithTemplatesData();
            }

        }

        private void EditTemplate(string templateId)
        {
            Cursor = Cursors.WaitCursor;
            var invoiceTemplateEditorForm = new InvoiceTemplateEditor(Guid.Parse(templateId));
            var dialogResult = invoiceTemplateEditorForm.ShowDialog();
            if (dialogResult != null)
            {
                btnRefresh_Click(null, null);
                Cursor = Cursors.Default;
            }
        }



        private void InitializeControls()
        {
            PopulateGridWithTemplatesData();
            SetGridSettings();
        }

        private void PopulateGridWithTemplatesData()
        {
            var templates = Activator.CreateInstance<ODM.Data.DbContext.InvoiceTemplateContext>()
                .GetAllInvoiceTemplates();
            foreach (DataRow row in templates.Rows)
            {
                if (Convert.ToBoolean(row["IsAutoPrint"]) || (Convert.ToBoolean(row["IsAutoPrint"]) == false && Convert.ToBoolean(row["IsConditional"]) == false))
                {
                    var priority = row["PrintPriority"] is DBNull ? "2" : row["PrintPriority"] ?? "2";
                    row["PrintPriority"] = ((ODM.Entities.Priority)Convert.ToInt32(priority)).ToString();
                }
                else
                {
                    row["PrintPriority"] = "None";
                }
            }
            grdInvoiceTemplates.DataSource = templates;
        }

        private void SetGridSettings()
        {
            grdInvoiceTemplates.Columns["TemplateName"].MinimumWidth = 250;
            grdInvoiceTemplates.Columns["TemplateName"].HeaderText = "Name";
            grdInvoiceTemplates.Columns["ID"].Visible = false;
            grdInvoiceTemplates.Columns["PrintPriority"].Visible = false;
            grdInvoiceTemplates.DoubleBuffered(true);
            grdInvoiceTemplates.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdInvoiceTemplates.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdInvoiceTemplates.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdInvoiceTemplates.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdInvoiceTemplates.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdInvoiceTemplates.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdInvoiceTemplates.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdInvoiceTemplates.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdInvoiceTemplates.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdInvoiceTemplates.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdInvoiceTemplates.ScrollBars = ScrollBars.Both;
            grdInvoiceTemplates.RowHeadersVisible = false;
            grdInvoiceTemplates.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdInvoiceTemplates.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdInvoiceTemplates.Columns["TemplateName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            grdInvoiceTemplates.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdInvoiceTemplates.EnableHeadersVisualStyles = false;
            grdInvoiceTemplates.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdInvoiceTemplates.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdInvoiceTemplates.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdInvoiceTemplates.DefaultCellStyle.Padding = new Padding(5, 5, 5, 5);
            grdInvoiceTemplates.AllowUserToAddRows = false;

            //priority
            DataGridViewComboBoxColumn priorityColumn = new DataGridViewComboBoxColumn();
            priorityColumn.HeaderText = "Priority";
            priorityColumn.Name = "Priority";
            priorityColumn.FlatStyle = FlatStyle.Flat;
            priorityColumn.DataSource = Enum.GetNames(typeof(ODM.Entities.Priority));
            priorityColumn.DataPropertyName = "PrintPriority";
            grdInvoiceTemplates.Columns.Add(priorityColumn);

            var buttonsToadd = new List<string>() 
            {
                "Edit",
                "Copy",
                "Delete"
            };

            foreach (var button in buttonsToadd)
            {
                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                btn.HeaderText = string.Empty;
                btn.Name = "btn" + button + "InvoiceTemplate";
                btn.Text = button;
                btn.UseColumnTextForButtonValue = true;
                btn.Width = 30;
                btn.FlatStyle = FlatStyle.Flat;
                grdInvoiceTemplates.Columns.Add(btn);
            }
            grdInvoiceTemplates.Columns["IsAutoPrint"].HeaderText = "Auto Print";
            grdInvoiceTemplates.Columns["IsConditional"].HeaderText = "Conditional";
            grdInvoiceTemplates.Columns["IsPdfTemplate"].HeaderText = "PdfTemplate";
            grdInvoiceTemplates.Columns["IsEnabled"].HeaderText = "Enabled";
        }


        private void btnAddInvoiceTemplate_Click(object sender, EventArgs e)
        {
            Form invoiceTemplateEditor = Application.OpenForms["InvoiceTemplateEditor"];
            if (invoiceTemplateEditor == null)
            {
                var invoiceTemplateEditorForm = new InvoiceTemplateEditor();
                var dialogClosed = invoiceTemplateEditorForm.ShowDialog();
                if (dialogClosed != null)
                {
                    btnRefresh_Click(null, null);
                }
            }
            else
            {
                invoiceTemplateEditor.Focus();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            PopulateGridWithTemplatesData();
        }

        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            var updatedInvoiceSettings = new List<ODM.Entities.InvoiceTemplate.InvoiceTemplate>();
            grdInvoiceTemplates.EndEdit();
            foreach (DataGridViewRow row in grdInvoiceTemplates.Rows)
            {
                updatedInvoiceSettings.Add(
                    new ODM.Entities.InvoiceTemplate.InvoiceTemplate()
                    {
                        TemplateId = Guid.Parse(row.Cells["ID"].Value.ToString()),
                        IsEnabled = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells["IsEnabled"]).Value),
                        IsAutoPrint = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells["IsAutoPrint"]).Value),
                        IsConditional = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells["IsConditional"]).Value),
                        IsPdfTemplate = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells["IsPdfTemplate"]).Value),
                        PrintPriority = (ODM.Entities.Priority)Enum.Parse(typeof(ODM.Entities.Priority), row.Cells["printPriority"].Value.ToString())
                    });
            }
            Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().UpdateInvoiceTemplates(updatedInvoiceSettings);
            btnSaveSettings.Font = new Font(btnSaveSettings.Font.Name, btnSaveSettings.Font.Size, FontStyle.Regular);
            btnRefresh.PerformClick();
        }


    }
}
