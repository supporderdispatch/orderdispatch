﻿namespace ODM.Appliction
{
    partial class DPDCourierSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DPDCourierSettings));
            this.lblSLIDRequired = new System.Windows.Forms.Label();
            this.lblAuthRequired = new System.Windows.Forms.Label();
            this.lblAccRequired = new System.Windows.Forms.Label();
            this.lblPswdRequired = new System.Windows.Forms.Label();
            this.lblUserNameRequired = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSaveClose = new System.Windows.Forms.Button();
            this.grdMappedServices = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpBoxShippingSettings = new System.Windows.Forms.GroupBox();
            this.ddlPrinter = new System.Windows.Forms.ComboBox();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.txtSLID = new System.Windows.Forms.TextBox();
            this.lblSLID = new System.Windows.Forms.Label();
            this.txtAuthCode = new System.Windows.Forms.TextBox();
            this.lblAuth = new System.Windows.Forms.Label();
            this.txtAccNum = new System.Windows.Forms.TextBox();
            this.lblAccNum = new System.Windows.Forms.Label();
            this.txtPswd = new System.Windows.Forms.TextBox();
            this.lblPswd = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.btnSenderDetails = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).BeginInit();
            this.grpBoxShippingSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSLIDRequired
            // 
            this.lblSLIDRequired.AutoSize = true;
            this.lblSLIDRequired.ForeColor = System.Drawing.Color.Red;
            this.lblSLIDRequired.Location = new System.Drawing.Point(714, 79);
            this.lblSLIDRequired.Name = "lblSLIDRequired";
            this.lblSLIDRequired.Size = new System.Drawing.Size(50, 13);
            this.lblSLIDRequired.TabIndex = 59;
            this.lblSLIDRequired.Text = "Required";
            this.lblSLIDRequired.Visible = false;
            // 
            // lblAuthRequired
            // 
            this.lblAuthRequired.AutoSize = true;
            this.lblAuthRequired.ForeColor = System.Drawing.Color.Red;
            this.lblAuthRequired.Location = new System.Drawing.Point(714, 45);
            this.lblAuthRequired.Name = "lblAuthRequired";
            this.lblAuthRequired.Size = new System.Drawing.Size(50, 13);
            this.lblAuthRequired.TabIndex = 58;
            this.lblAuthRequired.Text = "Required";
            this.lblAuthRequired.Visible = false;
            // 
            // lblAccRequired
            // 
            this.lblAccRequired.AutoSize = true;
            this.lblAccRequired.ForeColor = System.Drawing.Color.Red;
            this.lblAccRequired.Location = new System.Drawing.Point(714, 13);
            this.lblAccRequired.Name = "lblAccRequired";
            this.lblAccRequired.Size = new System.Drawing.Size(50, 13);
            this.lblAccRequired.TabIndex = 57;
            this.lblAccRequired.Text = "Required";
            this.lblAccRequired.Visible = false;
            // 
            // lblPswdRequired
            // 
            this.lblPswdRequired.AutoSize = true;
            this.lblPswdRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPswdRequired.Location = new System.Drawing.Point(316, 40);
            this.lblPswdRequired.Name = "lblPswdRequired";
            this.lblPswdRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPswdRequired.TabIndex = 55;
            this.lblPswdRequired.Text = "Required";
            this.lblPswdRequired.Visible = false;
            // 
            // lblUserNameRequired
            // 
            this.lblUserNameRequired.AutoSize = true;
            this.lblUserNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblUserNameRequired.Location = new System.Drawing.Point(316, 13);
            this.lblUserNameRequired.Name = "lblUserNameRequired";
            this.lblUserNameRequired.Size = new System.Drawing.Size(50, 13);
            this.lblUserNameRequired.TabIndex = 54;
            this.lblUserNameRequired.Text = "Required";
            this.lblUserNameRequired.Visible = false;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(339, 75);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(92, 13);
            this.lblMessage.TabIndex = 15;
            this.lblMessage.Text = "No Record Found";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(590, 193);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(180, 29);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSaveClose
            // 
            this.btnSaveClose.Location = new System.Drawing.Point(404, 198);
            this.btnSaveClose.Name = "btnSaveClose";
            this.btnSaveClose.Size = new System.Drawing.Size(180, 29);
            this.btnSaveClose.TabIndex = 13;
            this.btnSaveClose.Text = "Save && Close";
            this.btnSaveClose.UseVisualStyleBackColor = true;
            this.btnSaveClose.Click += new System.EventHandler(this.btnSaveClose_Click);
            // 
            // grdMappedServices
            // 
            this.grdMappedServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMappedServices.Location = new System.Drawing.Point(3, 19);
            this.grdMappedServices.Name = "grdMappedServices";
            this.grdMappedServices.Size = new System.Drawing.Size(781, 150);
            this.grdMappedServices.TabIndex = 12;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(430, 128);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(164, 26);
            this.btnDelete.TabIndex = 52;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(244, 128);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(180, 26);
            this.btnAdd.TabIndex = 51;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grpBoxShippingSettings
            // 
            this.grpBoxShippingSettings.Controls.Add(this.lblMessage);
            this.grpBoxShippingSettings.Controls.Add(this.btnCancel);
            this.grpBoxShippingSettings.Controls.Add(this.btnSaveClose);
            this.grpBoxShippingSettings.Controls.Add(this.grdMappedServices);
            this.grpBoxShippingSettings.Location = new System.Drawing.Point(10, 160);
            this.grpBoxShippingSettings.Name = "grpBoxShippingSettings";
            this.grpBoxShippingSettings.Size = new System.Drawing.Size(787, 226);
            this.grpBoxShippingSettings.TabIndex = 53;
            this.grpBoxShippingSettings.TabStop = false;
            this.grpBoxShippingSettings.Text = "Shipping Mapping";
            // 
            // ddlPrinter
            // 
            this.ddlPrinter.FormattingEnabled = true;
            this.ddlPrinter.Location = new System.Drawing.Point(118, 73);
            this.ddlPrinter.Name = "ddlPrinter";
            this.ddlPrinter.Size = new System.Drawing.Size(191, 21);
            this.ddlPrinter.TabIndex = 50;
            // 
            // lblPrinter
            // 
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Location = new System.Drawing.Point(12, 76);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(70, 13);
            this.lblPrinter.TabIndex = 49;
            this.lblPrinter.Text = "Select Printer";
            // 
            // txtSLID
            // 
            this.txtSLID.Location = new System.Drawing.Point(517, 76);
            this.txtSLID.Name = "txtSLID";
            this.txtSLID.Size = new System.Drawing.Size(191, 20);
            this.txtSLID.TabIndex = 47;
            // 
            // lblSLID
            // 
            this.lblSLID.AutoSize = true;
            this.lblSLID.Location = new System.Drawing.Point(411, 76);
            this.lblSLID.Name = "lblSLID";
            this.lblSLID.Size = new System.Drawing.Size(31, 13);
            this.lblSLID.TabIndex = 46;
            this.lblSLID.Text = "SLID";
            // 
            // txtAuthCode
            // 
            this.txtAuthCode.Location = new System.Drawing.Point(517, 44);
            this.txtAuthCode.Name = "txtAuthCode";
            this.txtAuthCode.Size = new System.Drawing.Size(191, 20);
            this.txtAuthCode.TabIndex = 45;
            // 
            // lblAuth
            // 
            this.lblAuth.AutoSize = true;
            this.lblAuth.Location = new System.Drawing.Point(411, 44);
            this.lblAuth.Name = "lblAuth";
            this.lblAuth.Size = new System.Drawing.Size(57, 13);
            this.lblAuth.TabIndex = 44;
            this.lblAuth.Text = "Auth Code";
            // 
            // txtAccNum
            // 
            this.txtAccNum.Location = new System.Drawing.Point(517, 10);
            this.txtAccNum.Name = "txtAccNum";
            this.txtAccNum.Size = new System.Drawing.Size(191, 20);
            this.txtAccNum.TabIndex = 43;
            // 
            // lblAccNum
            // 
            this.lblAccNum.AutoSize = true;
            this.lblAccNum.Location = new System.Drawing.Point(411, 13);
            this.lblAccNum.Name = "lblAccNum";
            this.lblAccNum.Size = new System.Drawing.Size(87, 13);
            this.lblAccNum.TabIndex = 42;
            this.lblAccNum.Text = "Account Number";
            // 
            // txtPswd
            // 
            this.txtPswd.Location = new System.Drawing.Point(119, 41);
            this.txtPswd.Name = "txtPswd";
            this.txtPswd.Size = new System.Drawing.Size(191, 20);
            this.txtPswd.TabIndex = 39;
            // 
            // lblPswd
            // 
            this.lblPswd.AutoSize = true;
            this.lblPswd.Location = new System.Drawing.Point(15, 44);
            this.lblPswd.Name = "lblPswd";
            this.lblPswd.Size = new System.Drawing.Size(53, 13);
            this.lblPswd.TabIndex = 38;
            this.lblPswd.Text = "Password";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(119, 13);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(191, 20);
            this.txtUserName.TabIndex = 37;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(16, 13);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(60, 13);
            this.lblUserName.TabIndex = 36;
            this.lblUserName.Text = "User Name";
            // 
            // btnSenderDetails
            // 
            this.btnSenderDetails.Location = new System.Drawing.Point(600, 128);
            this.btnSenderDetails.Name = "btnSenderDetails";
            this.btnSenderDetails.Size = new System.Drawing.Size(164, 26);
            this.btnSenderDetails.TabIndex = 60;
            this.btnSenderDetails.Text = "Update Sender Details";
            this.btnSenderDetails.UseVisualStyleBackColor = true;
            this.btnSenderDetails.Click += new System.EventHandler(this.btnSenderDetails_Click);
            // 
            // DPDCourierSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 390);
            this.Controls.Add(this.btnSenderDetails);
            this.Controls.Add(this.lblSLIDRequired);
            this.Controls.Add(this.lblAuthRequired);
            this.Controls.Add(this.lblAccRequired);
            this.Controls.Add(this.lblPswdRequired);
            this.Controls.Add(this.lblUserNameRequired);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpBoxShippingSettings);
            this.Controls.Add(this.ddlPrinter);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.txtSLID);
            this.Controls.Add(this.lblSLID);
            this.Controls.Add(this.txtAuthCode);
            this.Controls.Add(this.lblAuth);
            this.Controls.Add(this.txtAccNum);
            this.Controls.Add(this.lblAccNum);
            this.Controls.Add(this.txtPswd);
            this.Controls.Add(this.lblPswd);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.lblUserName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DPDCourierSettings";
            this.Text = "DPD Courier Settings";
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).EndInit();
            this.grpBoxShippingSettings.ResumeLayout(false);
            this.grpBoxShippingSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSLIDRequired;
        private System.Windows.Forms.Label lblAuthRequired;
        private System.Windows.Forms.Label lblAccRequired;
        private System.Windows.Forms.Label lblPswdRequired;
        private System.Windows.Forms.Label lblUserNameRequired;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSaveClose;
        private System.Windows.Forms.DataGridView grdMappedServices;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grpBoxShippingSettings;
        private System.Windows.Forms.ComboBox ddlPrinter;
        private System.Windows.Forms.Label lblPrinter;
        private System.Windows.Forms.TextBox txtSLID;
        private System.Windows.Forms.Label lblSLID;
        private System.Windows.Forms.TextBox txtAuthCode;
        private System.Windows.Forms.Label lblAuth;
        private System.Windows.Forms.TextBox txtAccNum;
        private System.Windows.Forms.Label lblAccNum;
        private System.Windows.Forms.TextBox txtPswd;
        private System.Windows.Forms.Label lblPswd;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Button btnSenderDetails;
    }
}