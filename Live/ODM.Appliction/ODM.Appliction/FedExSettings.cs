﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class FedExSettings : Form
    {
        string _CourierName = "";
        string _CourierID = "";
        SqlConnection connection;
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        string strDefaultPrinter = "";

        public FedExSettings()
        {
            InitializeComponent();
        }

        public FedExSettings(string strCourierName, string strCourierID)
        {
            InitializeComponent();
            _CourierName = strCourierName;
            _CourierID = strCourierID;
            PopulateControls();
            BindGrid();
            this.grdMappedServices.DataError += grdMappedServices_DataError;
        }

        void grdMappedServices_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //MessageBox.Show("error occured");
        }

        private void PopulateControls()
        {
            try
            {

                DataTable ObjdtCourier = new DataTable();
                ObjdtCourier = GlobalVar.GetCourierDetailsByNameOrId(_CourierName, _CourierID);
                if (ObjdtCourier.Rows.Count > 0)
                {
                    string strIsLive = Convert.ToString(ObjdtCourier.Rows[0]["IsLive"]);
                    txtApiAccessKeyLive.Text = Convert.ToString(ObjdtCourier.Rows[0]["AccessKeyLive"]);
                    txtApiPassLive.Text = Convert.ToString(ObjdtCourier.Rows[0]["PasswordLive"]);
                    txtAccountNumberLive.Text = Convert.ToString(ObjdtCourier.Rows[0]["AccountNumberLive"]);
                    txtMeterNumberLive.Text = Convert.ToString(ObjdtCourier.Rows[0]["MeterNumberLive"]);
                    txtApiAccessKeyTest.Text = Convert.ToString(ObjdtCourier.Rows[0]["AccessKeyTest"]);
                    txtApiPassTest.Text = Convert.ToString(ObjdtCourier.Rows[0]["PasswordTest"]);
                    txtAccountNumberTest.Text = Convert.ToString(ObjdtCourier.Rows[0]["AccountNumberTest"]);
                    txtMeterNumberTest.Text = Convert.ToString(ObjdtCourier.Rows[0]["MeterNumberTest"]);
                    txtDefaultWeight.Text = Convert.ToString(ObjdtCourier.Rows[0]["DefaultWight"]);
                    chkIsLive.Checked = string.IsNullOrWhiteSpace(strIsLive) ? false : Convert.ToBoolean(strIsLive);
                    ddlPrinter.DataSource = GlobalVar.AttachedPrinters(ref strDefaultPrinter);
                    ddlStockLabel.DataSource = GlobalVariablesAndMethods.GetAllLabelStockType();
                    ddlStockLabel.SelectedItem = Convert.ToString(ObjdtCourier.Rows[0]["LabelStockType"]);
                    ddlPrinter.SelectedItem = !string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForFedEx) ?
                                               GlobalVariablesAndMethods.SelectedPrinterNameForFedEx : strDefaultPrinter;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        public void BindGrid()
        {
            try
            {
                connection = new SqlConnection();
                DataTable objDtGridData = new DataTable();
                DataTable objCourierList = new DataTable();
                DataTable objPackagingTypes = new DataTable();
                connection.ConnectionString = MakeConnection.GetConnection();
                objDtGridData = SqlHelper.ExecuteDataset(connection, "GetFedExMappedCouriers").Tables[0];
                objCourierList = SqlHelper.ExecuteDataset(connection, "GetAllFedExCourierToMapWithServices").Tables[0];
                objPackagingTypes = SqlHelper.ExecuteDataset(connection, "GetAllFedExPackagingTypesToMapWithServices").Tables[0];

                if (objDtGridData.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(objDtGridData.Rows[0]["ID"])))
                {
                    grdMappedServices.Visible = true;
                    grdMappedServices.AllowUserToAddRows = false;
                    grdMappedServices.DataSource = objDtGridData;
                    grdMappedServices.AutoGenerateColumns = false;
                    grdMappedServices.Columns[0].Visible = false;
                    grdMappedServices.Columns[1].Visible = false;
                    grdMappedServices.Columns[2].Visible = false;
                    grdMappedServices.Columns[4].Visible = false;
                    grdMappedServices.Columns[3].ReadOnly = true;
                    grdMappedServices.Columns[3].Width = 200;
                    grdMappedServices.Columns["MappedPackageTypeId"].Visible = false;
                    grdMappedServices.Columns["PackageTypeName"].Visible = false;

                    DataGridViewComboBoxColumn ddlCouriers = new DataGridViewComboBoxColumn();
                    ddlCouriers.HeaderText = "Courier Name";
                    ddlCouriers.Name = "Courier_Name";
                    ddlCouriers.ValueMember = "CourierID";
                    ddlCouriers.DataPropertyName = "MapedCourierId";
                    ddlCouriers.DisplayMember = "CourierName";
                    grdMappedServices.Columns.Add(ddlCouriers);
                    ddlCouriers.DataSource = objCourierList;
                    grdMappedServices.Columns["Courier_Name"].Width = 270;

                    DataGridViewComboBoxColumn ddlPackagingTypes = new DataGridViewComboBoxColumn();
                    ddlPackagingTypes.HeaderText = "Packaging Type";
                    ddlPackagingTypes.Name = "Packaging_Type";
                    ddlPackagingTypes.ValueMember = "PackageTypeID";
                    ddlPackagingTypes.DataPropertyName = "MappedPackageTypeId";
                    ddlPackagingTypes.DisplayMember = "PackageTypeName";
                    grdMappedServices.Columns.Add(ddlPackagingTypes);
                    ddlPackagingTypes.DataSource = objPackagingTypes;
                    grdMappedServices.Columns["Packaging_Type"].Width = 250;
                    lblMessage.Visible = false;
                }
                else
                {
                    grdMappedServices.Visible = false;
                    lblMessage.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An Error Occurred Contact the Administrator");
            }
            finally
            {
                connection.Close();
            }
        }
      
        private void SaveSettings()
        {
            try
            {
                connection = new SqlConnection();
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                Guid CourierID = new Guid(_CourierID);
                SqlParameter[] objParams = new SqlParameter[13];

                objParams[0] = new SqlParameter("@CourierId", SqlDbType.UniqueIdentifier);
                objParams[0].Value = CourierID;

                objParams[1] = new SqlParameter("@CourierName", SqlDbType.VarChar, 50);
                objParams[1].Value = _CourierName;

                objParams[2] = new SqlParameter("@AccessKeyLive", SqlDbType.VarChar, 50);
                objParams[2].Value = txtApiAccessKeyLive.Text;

                objParams[3] = new SqlParameter("@PasswordLive", SqlDbType.VarChar, 50);
                objParams[3].Value = txtApiPassLive.Text;

                objParams[4] = new SqlParameter("@AccountNumberLive", SqlDbType.VarChar, 50);
                objParams[4].Value = txtAccountNumberLive.Text;

                objParams[5] = new SqlParameter("@MeterNumberLive", SqlDbType.VarChar, 50);
                objParams[5].Value = txtMeterNumberLive.Text;

                objParams[6] = new SqlParameter("@AccessKeyTest", SqlDbType.VarChar, 50);
                objParams[6].Value = txtApiAccessKeyTest.Text;

                objParams[7] = new SqlParameter("@PasswordTest", SqlDbType.VarChar, 50);
                objParams[7].Value = txtApiPassTest.Text;

                objParams[8] = new SqlParameter("@AccountNumberTest", SqlDbType.VarChar, 50);
                objParams[8].Value = txtAccountNumberTest.Text;

                objParams[9] = new SqlParameter("@MeterNumberTest", SqlDbType.VarChar, 50);
                objParams[9].Value = txtMeterNumberTest.Text;

                objParams[10] = new SqlParameter("@DefaultWeight", SqlDbType.VarChar, 50);
                objParams[10].Value = txtDefaultWeight.Text;

                objParams[11] = new SqlParameter("@IsLive", SqlDbType.Bit);
                objParams[11].Value = chkIsLive.Checked;

                objParams[12] = new SqlParameter("@LabelStockType", SqlDbType.VarChar, 50);
                objParams[12].Value = ddlStockLabel.SelectedItem.ToString();

                SqlHelper.ExecuteNonQuery(connection, "AddUpdateFedEx", objParams);
                //this.Close();
                //MessageBox.Show("Settings Saved Successfully !");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        private void MapCourierToService()
        {
            try
            {
                int intResult = 100;
                if (grdMappedServices.Rows.Count > 0)
                {
                    for (int i = 0; i < grdMappedServices.Rows.Count; i++)
                    {
                        string strSelectedCourierID = Convert.ToString(grdMappedServices.Rows[i].Cells["ID"].Value);
                        //string strSelectedValue = Convert.ToString(grdMappedServices[5, i].Value);
                        string strSelectedValue = Convert.ToString(grdMappedServices.Rows[i].Cells["Courier_Name"].Value);
                        string strServiceName = Convert.ToString(grdMappedServices.Rows[i].Cells["PostalServiceName"].Value);
                        string strSelectedPackageType = Convert.ToString(grdMappedServices.Rows[i].Cells["Packaging_Type"].Value);
                        if (strSelectedValue == "")
                        {
                            MessageBox.Show(string.Format("Please select courier for the service {0}", strServiceName));
                            intResult = -1;
                            break;
                        }
                        else
                        {
                            connection = new SqlConnection();
                            Guid MappedID = new Guid(strSelectedValue);
                            Guid ID = new Guid(strSelectedCourierID);
                            Guid DummyID = new Guid();
                            Guid packageTypeID = new Guid(strSelectedPackageType);
                            connection.ConnectionString = MakeConnection.GetConnection();
                            SqlParameter[] objParams = new SqlParameter[7];

                            objParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                            objParams[0].Value = ID;

                            objParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                            objParams[1].Value = DummyID;

                            objParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                            objParams[2].Value = DummyID;

                            objParams[3] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                            objParams[3].Value = MappedID;

                            objParams[4] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                            objParams[4].Value = "UPDATE";

                            objParams[5] = new SqlParameter("@PackageTypeID", SqlDbType.UniqueIdentifier);
                            objParams[5].Value = packageTypeID;

                            objParams[6] = new SqlParameter("@Result", SqlDbType.Int);
                            objParams[6].Direction = ParameterDirection.Output;

                            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteServicesToMapFedExCourier", objParams);

                            intResult = Convert.ToInt32(objParams[6].Value);
                        }
                    }
                    if (intResult == 1)
                    {
                        this.Close();
                        MessageBox.Show("Settings Saved Successfully !");
                    }
                }
                else
                {
                    this.Close();
                    MessageBox.Show("Settings Saved Successfully !");
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
            finally
            {
                connection.Close();
            }
        }

        private void DeleteService()
        {
            try
            {
                int RowIndex = 0;
                if (grdMappedServices.CurrentCell != null)
                {
                    RowIndex = Convert.ToInt32(grdMappedServices.CurrentCell.RowIndex);

                    string strSelectedCourierID = Convert.ToString(grdMappedServices.Rows[RowIndex].Cells[0].Value);
                    connection.ConnectionString = MakeConnection.GetConnection();
                    Guid ID = new Guid(strSelectedCourierID);
                    Guid DummyID = new Guid();

                    SqlParameter[] objParams = new SqlParameter[6];

                    objParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                    objParams[0].Value = ID;

                    objParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                    objParams[1].Value = DummyID;

                    objParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                    objParams[2].Value = DummyID;

                    objParams[3] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                    objParams[3].Value = DummyID;

                    objParams[4] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                    objParams[4].Value = "DELETE";

                    objParams[5] = new SqlParameter("@Result", SqlDbType.Int);
                    objParams[5].Direction = ParameterDirection.Output;

                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteServicesToMapFedExCourier", objParams);

                    int intResult = Convert.ToInt32(objParams[5].Value);
                    if (intResult == 2)
                    {
                        BindGrid();
                        MessageBox.Show("Record deleted successfully !");
                    }
                }
                else
                {
                    MessageBox.Show("Please select a record to delete");
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
            finally
            {
                connection.Close();
            }
        }

        private bool ValidateControls()
        {
            bool IsValid = true;
            if (string.IsNullOrWhiteSpace(txtApiAccessKeyLive.Text))
            {
                lblAccessKeyLiveRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAccessKeyLiveRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtApiPassLive.Text))
            {
                lblPswdLiveRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPswdLiveRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtMeterNumberLive.Text))
            {
                lblMeterLiveRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblMeterLiveRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAccountNumberLive.Text))
            {
                lblAccountLiveRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAccountLiveRequired.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(txtApiAccessKeyTest.Text))
            {
                lblAccessKeyTestRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAccessKeyTestRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtApiPassTest.Text))
            {
                lblPswdTestRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPswdTestRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtMeterNumberTest.Text))
            {
                lblMeterTestRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblMeterTestRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAccountNumberTest.Text))
            {
                lblAccountTestRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAccountTestRequired.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(txtDefaultWeight.Text))
            {
                lblWeightRequired.Visible = true;
                lblWeightInvalid.Visible = false;
                IsValid = false;
            }
            else
            {
                double DefaultWeight = 0.00;
                bool IsValidWeight = double.TryParse(txtDefaultWeight.Text, out DefaultWeight);
                if (!IsValidWeight)
                {
                    lblWeightInvalid.Visible = true;
                    IsValid = false;
                }
                else
                {
                    lblWeightInvalid.Visible = false;
                }
                lblWeightRequired.Visible = false;
            }
            return IsValid;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Form objAddService = Application.OpenForms["AddServices"];
            if (objAddService == null)
            {
                AddServices objServices = new AddServices(_CourierName, _CourierID);
                objServices.Show();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure, you want to delete Postal Service ?", "Delete Postal Service", MessageBoxButtons.YesNo);
            if (result.Equals(DialogResult.Yes))
            {
                DeleteService();
            }
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                SaveSettings();
                MapCourierToService();
                GlobalVariablesAndMethods.SelectedPrinterNameForFedEx = ddlPrinter.SelectedItem.ToString();
                GlobalVar.AddUpdatePrinterName();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Form frmAddService = Application.OpenForms["AddServices"];
            if (frmAddService != null)
            {
                frmAddService.Close();
            }
            this.Close();
        }

        private void btnShipper_Click(object sender, EventArgs e)
        {

            Form objShipperAddress = Application.OpenForms["ShipperAddress"];
            if (objShipperAddress == null)
            {
                ShipperAddress objServices = new ShipperAddress(_CourierName, _CourierID);
                objServices.Show();
            }
        }

        private void btnVoidShipment_Click(object sender, EventArgs e)
        {
            Form objShipperAddress = Application.OpenForms["CancelFedExShipment"];
            if (objShipperAddress == null)
            {
                CancelFedExShipment frmFedExShipment = new CancelFedExShipment();
                frmFedExShipment.Show();
            }
        }
    }
}
