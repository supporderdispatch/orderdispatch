﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction.Updates
{
    public partial class UpdateWindow : Form
    {
        private Entities.AppVersionDetails latestAppVersionDetails;

        public UpdateWindow(Entities.AppVersionDetails latestAppVersionDetails)
        {
            InitializeComponent();
            this.latestAppVersionDetails = latestAppVersionDetails;
            LoadDownloadInfo();
            HideDownloadingComponents();
        }

        private void HideDownloadingComponents()
        {
            btnInstall.Visible = false;
            lblDownloading.Visible = false;
            lblDownloadingBar.Visible = false;
            infoBox.Visible = true;
        }

        private void LoadDownloadInfo()
        {
            var info = "New Version Available " + Environment.NewLine + Environment.NewLine;

            info += "Latest App version\t: " + latestAppVersionDetails.AppVersion + Environment.NewLine;
            using (var client = new System.Net.WebClient())
            {
                client.OpenRead(latestAppVersionDetails.DownloadUrl);
                var contentlength = SizeSuffix(Convert.ToInt64(client.ResponseHeaders["Content-Length"]));
                info += "Size\t\t\t: " + contentlength + Environment.NewLine;
                client.Dispose();
            }
            infoBox.Text = info;

        }

        private void btnInstall_Click(object sender, EventArgs e)
        {
            var tempUpdatorAppPath=Path.GetTempPath()+"\\"+"ODM.Updator.exe";

            if (File.Exists(tempUpdatorAppPath))
            {
                File.Delete(tempUpdatorAppPath);
            }

            File.Copy(Application.StartupPath + "\\" + "ODM.Updator.exe", tempUpdatorAppPath);
            ProcessStartInfo psi = new ProcessStartInfo();
            psi.FileName = tempUpdatorAppPath;
            Process.Start(psi);
            try
            {
                ODM.Core.UserTracking.UserTracker.EndTracking();
                Application.Exit();
            }
            catch 
            {
                
            }
            
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            btnDownload.Enabled = false;
            ShowDownloadingControls();
            var webClient = new WebClient();
            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadCompleted);
            webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
            webClient.DownloadFileAsync(new Uri(latestAppVersionDetails.DownloadUrl), Path.GetTempPath()+"\\OdUpdate.zip");
            webClient.Dispose();
        }

        private void ShowDownloadingControls()
        {
            infoBox.Visible = false;
            lblDownloading.Visible = true;
            lblDownloadingBar.Visible = true;
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            lblDownloadingBar.Invalidate();
            lblDownloadingBar.Paint += (s, ev) =>
            {
                var g = ev.Graphics;
                var width = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(ev.ClipRectangle.Width * e.ProgressPercentage)) / 100);
                g.FillRectangle(Brushes.WhiteSmoke, ev.ClipRectangle.X, ev.ClipRectangle.Y, ev.ClipRectangle.Width, ev.ClipRectangle.Height);
                g.FillRectangle(Brushes.DarkSeaGreen, ev.ClipRectangle.X, ev.ClipRectangle.Y, width, ev.ClipRectangle.Height);
                g.DrawString(e.ProgressPercentage + "%", infoBox.Font, Brushes.Black, new PointF(ev.ClipRectangle.Width / 2.2f, ev.ClipRectangle.Height / 6.0f));
            };
        }

        private void DownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            lblDownloading.Text="Download completed";
            btnDownload.Visible = false;
            btnInstall.Visible = true;
            this.WindowState = FormWindowState.Normal;
            this.Focus();

        }
        static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        static string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (value < 0) { return "-" + SizeSuffix(-value); }

            int i = 0;
            decimal dValue = (decimal)value;
            while (Math.Round(dValue, decimalPlaces) >= 1000)
            {
                dValue /= 1024;
                i++;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}", dValue, SizeSuffixes[i]);
        }

        private void btnRemindMeLater_Click(object sender, EventArgs e)
        {
            Updator.updateChecker.Stop();
            this.Close();
        }

    }
}
