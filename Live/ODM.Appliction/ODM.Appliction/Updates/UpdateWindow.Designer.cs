﻿namespace ODM.Appliction.Updates
{
    partial class UpdateWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateWindow));
            this.btnInstall = new System.Windows.Forms.Button();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnRemindMeLater = new System.Windows.Forms.Button();
            this.infoBox = new System.Windows.Forms.RichTextBox();
            this.lblDownloading = new System.Windows.Forms.Label();
            this.lblDownloadingBar = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnInstall
            // 
            this.btnInstall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInstall.Location = new System.Drawing.Point(145, 90);
            this.btnInstall.Name = "btnInstall";
            this.btnInstall.Size = new System.Drawing.Size(75, 23);
            this.btnInstall.TabIndex = 0;
            this.btnInstall.Text = "Install";
            this.btnInstall.UseVisualStyleBackColor = true;
            this.btnInstall.Click += new System.EventHandler(this.btnInstall_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDownload.Location = new System.Drawing.Point(145, 90);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(75, 23);
            this.btnDownload.TabIndex = 1;
            this.btnDownload.Text = "Download";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnRemindMeLater
            // 
            this.btnRemindMeLater.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemindMeLater.Location = new System.Drawing.Point(226, 90);
            this.btnRemindMeLater.Name = "btnRemindMeLater";
            this.btnRemindMeLater.Size = new System.Drawing.Size(135, 23);
            this.btnRemindMeLater.TabIndex = 2;
            this.btnRemindMeLater.Text = "Show On Next Startup";
            this.btnRemindMeLater.UseVisualStyleBackColor = true;
            this.btnRemindMeLater.Click += new System.EventHandler(this.btnRemindMeLater_Click);
            // 
            // infoBox
            // 
            this.infoBox.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.infoBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.infoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoBox.Location = new System.Drawing.Point(11, 4);
            this.infoBox.Name = "infoBox";
            this.infoBox.ReadOnly = true;
            this.infoBox.Size = new System.Drawing.Size(347, 80);
            this.infoBox.TabIndex = 3;
            this.infoBox.Text = "";
            // 
            // lblDownloading
            // 
            this.lblDownloading.AutoSize = true;
            this.lblDownloading.Location = new System.Drawing.Point(22, 24);
            this.lblDownloading.Name = "lblDownloading";
            this.lblDownloading.Size = new System.Drawing.Size(69, 13);
            this.lblDownloading.TabIndex = 4;
            this.lblDownloading.Text = "Downloading";
            // 
            // lblDownloadingBar
            // 
            this.lblDownloadingBar.Location = new System.Drawing.Point(22, 47);
            this.lblDownloadingBar.Name = "lblDownloadingBar";
            this.lblDownloadingBar.Size = new System.Drawing.Size(337, 23);
            this.lblDownloadingBar.TabIndex = 5;
            // 
            // UpdateWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 119);
            this.Controls.Add(this.lblDownloadingBar);
            this.Controls.Add(this.lblDownloading);
            this.Controls.Add(this.infoBox);
            this.Controls.Add(this.btnRemindMeLater);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.btnInstall);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UpdateWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Update";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnInstall;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button btnRemindMeLater;
        private System.Windows.Forms.RichTextBox infoBox;
        private System.Windows.Forms.Label lblDownloading;
        private System.Windows.Forms.Label lblDownloadingBar;
    }
}