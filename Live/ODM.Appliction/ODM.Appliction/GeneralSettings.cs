﻿using AForge.Video.DirectShow;
using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using ODM.Core;
using System.Collections.Generic;
using System.Drawing;
namespace ODM.Appliction
{
    public partial class GeneralSettings : Form
    {
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        string strDefaultPrinter = "";

        // camera settings addition
        private FilterInfoCollection CaptureDevice;
        private VideoCaptureDevice finalFrame;
        private VideoCapabilities[] videoCapabilities;
        private VideoCapabilities[] snapshotCapabilities;

        public GeneralSettings()
        {
            InitializeComponent();
            EnableBinSettingsForAdmin();
            // PopulateControls();
            InitializeCN22FormPanel();
            InitializeProcessOrderInstructions();
            RegisterEvents();
        }
        private void RegisterEvents()
        {
            this.grdProcessOrderConditions.CellContentClick += grdProcessOrderConditions_CellContentClick;
            this.grdProcessOrderConditions.CurrentCellDirtyStateChanged += grdProcessOrderConditions_CurrentCellDirtyStateChanged;
        }

        void grdProcessOrderConditions_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {

            btnSaveProcessOrderSettings.Font = new Font(btnSaveProcessOrderSettings.Font.Name, btnSaveProcessOrderSettings.Font.Size, FontStyle.Bold);
        }

        void grdProcessOrderConditions_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                var row = senderGrid.Rows[e.RowIndex];
                var buttonText = row.Cells[e.ColumnIndex].Value.ToString();
                var conditionId = row.Cells["ConditionId"].Value.ToString();
                var ConditionName = row.Cells["ConditionName"].Value.ToString();
                switch (buttonText)
                {
                    case "Edit":
                        EditCondition(conditionId);
                        break;

                    case "Delete":
                        DeleteCondition(conditionId, ConditionName);
                        break;

                    case "Copy":
                        CopyCondition(conditionId);
                        break;

                    default:
                        break;
                }
            }
        }

        private void InitializeProcessOrderInstructions()
        {
            PopulateGridWithProcessOrderConditions();
            SetGridSettings();
        }
        private void PopulateGridWithProcessOrderConditions()
        {
            var Conditions = Activator.CreateInstance<ODM.Data.DbContext.ProcessOrderInstructionContext>()
                .GetAllConditions();

            grdProcessOrderConditions.DataSource = Conditions;
        }
        private void SetGridSettings()
        {

            grdProcessOrderConditions.Columns["ConditionName"].MinimumWidth = 250;
            grdProcessOrderConditions.Columns["ConditionName"].HeaderText = "Name";
            grdProcessOrderConditions.Columns["ConditionID"].Visible = false;
            grdProcessOrderConditions.Columns["IsEnabled"].HeaderText = "Enabled";
            var buttonsToadd = new List<string>() 
            {
                "Edit",
                "Copy",
                "Delete"
            };

            foreach (var button in buttonsToadd)
            {
                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                btn.HeaderText = string.Empty;
                btn.Name = "btn" + button + "InvoiceTemplate";
                btn.Text = button;
                btn.UseColumnTextForButtonValue = true;
                btn.Width = 30;
                btn.FlatStyle = FlatStyle.Flat;
                grdProcessOrderConditions.Columns.Add(btn);
            }
            grdProcessOrderConditions.DoubleBuffered(true);
            grdProcessOrderConditions.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdProcessOrderConditions.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdProcessOrderConditions.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdProcessOrderConditions.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdProcessOrderConditions.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdProcessOrderConditions.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdProcessOrderConditions.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdProcessOrderConditions.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdProcessOrderConditions.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdProcessOrderConditions.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdProcessOrderConditions.ScrollBars = ScrollBars.Both;
            grdProcessOrderConditions.RowHeadersVisible = false;
            grdProcessOrderConditions.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdProcessOrderConditions.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdProcessOrderConditions.Columns["ConditionName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            grdProcessOrderConditions.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdProcessOrderConditions.EnableHeadersVisualStyles = false;
            grdProcessOrderConditions.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdProcessOrderConditions.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdProcessOrderConditions.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdProcessOrderConditions.DefaultCellStyle.Padding = new Padding(5, 5, 5, 5);
            grdProcessOrderConditions.AllowUserToAddRows = false;

        }
        private void InitializeCN22FormPanel()
        {
            grdCN22MappedServices.DataSource = Activator.CreateInstance<CN22Form>().GetPostalServicesMappedWithCN22();
            grdCN22MappedServices.Columns["pkPostalServiceId"].Visible = false;
            grdCN22MappedServices.Columns["Is Urgent"].Visible = false;
            grdCN22MappedServices.Columns["Postal Service Name"].Width = 200;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveSettings();
            this.Close();
            MessageBox.Show("Settings Saved Successfully !");
        }
        // Collect supported video and snapshot sizes
        public void EnumeratedSupportedFrameSizes(VideoCaptureDevice videoDevice)
        {
            this.Cursor = Cursors.WaitCursor;

            ddlVideoResolution.Items.Clear();
            ddlSnapshotResolution.Items.Clear();

            try
            {
                videoCapabilities = videoDevice.VideoCapabilities;
                snapshotCapabilities = videoDevice.SnapshotCapabilities;

                foreach (VideoCapabilities capabilty in videoCapabilities)
                {
                    ddlVideoResolution.Items.Add(string.Format("{0} x {1}",
                        capabilty.FrameSize.Width, capabilty.FrameSize.Height));
                }

                foreach (VideoCapabilities capabilty in snapshotCapabilities)
                {
                    ddlSnapshotResolution.Items.Add(string.Format("{0} x {1}",
                        capabilty.FrameSize.Width, capabilty.FrameSize.Height));
                }

                if (videoCapabilities.Length == 0)
                {
                    ddlVideoResolution.Items.Add("Not supported");
                }
                if (snapshotCapabilities.Length == 0)
                {
                    ddlSnapshotResolution.Items.Add("Not supported");
                }

                ddlVideoResolution.SelectedIndex = 0;
                ddlSnapshotResolution.SelectedIndex = 0;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void PopulateControls()
        {
            DataTable objDtPopulateControls = new DataTable();
            try
            {
                //populate printer details
                ddlPrinter.DataSource = GlobalVar.AttachedPrinters(ref strDefaultPrinter);
                ddlPrinter.SelectedItem = !string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForInvoice) ?
                                           GlobalVariablesAndMethods.SelectedPrinterNameForInvoice : strDefaultPrinter;

                //populate pick list printer details
                ddlPickListPrinters.DataSource = GlobalVar.AttachedPrinters(ref strDefaultPrinter);
                ddlPickListPrinters.SelectedItem = !string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForPickList) ?
                                           GlobalVariablesAndMethods.SelectedPrinterNameForPickList : strDefaultPrinter;

                //populate cn22 from printer details
                ddlCN22Printer.DataSource = GlobalVar.AttachedPrinters(ref strDefaultPrinter);
                ddlCN22Printer.SelectedItem = !string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForCN22Form) ?
                                           GlobalVariablesAndMethods.SelectedPrinterNameForCN22Form : strDefaultPrinter;


                //populate camera details
                CaptureDevice = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                foreach (FilterInfo device in CaptureDevice)
                {
                    ddlVideoDevice.Items.Add(device.Name);
                }
                ddlVideoDevice.SelectedIndex = 0;
                finalFrame = new VideoCaptureDevice();
                objDtPopulateControls = GetUpdateCameraSettings("GET");
                if (objDtPopulateControls.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(objDtPopulateControls.Rows[0]["VideoDevice"].ToString()))
                    {
                        ddlVideoDevice.SelectedItem = objDtPopulateControls.Rows[0]["VideoDevice"].ToString();
                    }
                    if (!string.IsNullOrEmpty(objDtPopulateControls.Rows[0]["VideoResolution"].ToString()))
                    {
                        ddlVideoResolution.SelectedItem = objDtPopulateControls.Rows[0]["VideoResolution"].ToString();
                    }
                    if (!string.IsNullOrEmpty(objDtPopulateControls.Rows[0]["SnapshotResolution"].ToString()))
                    {
                        ddlSnapshotResolution.SelectedItem = objDtPopulateControls.Rows[0]["SnapshotResolution"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }
        // Get,add,update camera settings
        public DataTable GetUpdateCameraSettings(string strMode)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            string strResult = "";
            try
            {
                SqlParameter[] objParam = new SqlParameter[7];

                objParam[0] = new SqlParameter("@IPAddress", SqlDbType.VarChar, 100);
                objParam[0].Value = new GlobalVariablesAndMethods().GetSystemIpv4Address;

                objParam[1] = new SqlParameter("@VideoDevice", SqlDbType.VarChar, 100);
                objParam[1].Value = ddlVideoDevice.SelectedItem;

                objParam[2] = new SqlParameter("@VideoResolution", SqlDbType.VarChar, 100);
                objParam[2].Value = ddlVideoResolution.SelectedItem;

                objParam[3] = new SqlParameter("@SnapshotResolution", SqlDbType.VarChar, 100);
                objParam[3].Value = ddlSnapshotResolution.SelectedItem;

                objParam[4] = new SqlParameter("@CreatedBy", SqlDbType.VarChar, 100);
                objParam[4].Value = GlobalVariablesAndMethods.UserDetails.Firstname + " " + GlobalVariablesAndMethods.UserDetails.Lastname;

                objParam[5] = new SqlParameter("@Mode", SqlDbType.VarChar, 100);
                objParam[5].Value = strMode;

                objParam[6] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[6].Direction = ParameterDirection.Output;

                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateCameraSettingsByIPAddress", objParam);
                    strResult = objParam[6].Value.ToString();
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];
                    }
                }
                else if (strMode.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateCameraSettingsByIPAddress", objParam);
                    strResult = objParam[6].Value.ToString();
                    if (strResult == "1" || strResult == "2")
                    {
                        MessageBox.Show("Camera Settings saved successfully.", "Save Camera Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }

                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                return objDt;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        private void SaveSettings()
        {
            try
            {
                GlobalVariablesAndMethods.SelectedPrinterNameForInvoice = ddlPrinter.SelectedItem.ToString();
                GlobalVariablesAndMethods.SelectedPrinterNameForPickList = ddlPickListPrinters.SelectedItem.ToString();
                GlobalVariablesAndMethods.SelectedPrinterNameForCN22Form = ddlCN22Printer.SelectedItem.ToString();
                GlobalVar.AddUpdatePrinterName();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void ddlVideoDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CaptureDevice.Count != 0)
            {
                finalFrame = new VideoCaptureDevice(CaptureDevice[ddlVideoDevice.SelectedIndex].MonikerString);
                EnumeratedSupportedFrameSizes(finalFrame);
            }
        }

        private void btnSaveCameraSettings_Click(object sender, EventArgs e)
        {
            GetUpdateCameraSettings("UPDATE");
        }

        private void GeneralSettings_Load(object sender, EventArgs e)
        {
            PopulateControls();
        }

        private void btnDeleteImagesByOrderId_Click(object sender, EventArgs e)
        {
            DialogResult confirmDeleteStatus = MessageBox.Show("Are you sure you want to delete Order Image(s) ?", "Delete Order Images", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            string strResult = "";
            if (confirmDeleteStatus == DialogResult.Yes)
            {
                strResult = GlobalVariablesAndMethods.GetAddUpdateDeleteImageByOrderId("DELETE", GlobalVariablesAndMethods.OrderIdToLog);
            }
            //strResult = 3, then the images have been deleted which are older than 60 days or more and whose email has been sent
            if (strResult.ToString() == "3")
            {
                MessageBox.Show("Order Image(s) deleted successfully!", "Delete Order Images", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //strResult = 4, then the images are not found.
            else if (strResult.ToString() == "4")
            {
                MessageBox.Show("No Image(s) fulfilling the condition are found.", "Delete Order Images", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        private void btnEditBinSetting_Click(object sender, EventArgs e)
        {
            txtNumberOfBins.ReadOnly = false;
        }

        private void btnSaveBinSetting_Click(object sender, EventArgs e)
        {
            var blIsSuccessfullyUpdated = new Bins().UpdateMaxNumberOfBins(Convert.ToInt32(txtNumberOfBins.Text));
            if (blIsSuccessfullyUpdated)
            {
                txtNumberOfBins.ReadOnly = true;
                //MessageBox.Show("Number of Bins successfully updated","Number of Bins",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }
        private void EnableBinSettingsForAdmin()
        {
            var blIsAdmin = GlobalVariablesAndMethods.UserDetails.IsAdmin;
            if (blIsAdmin)
            {
                txtNumberOfBins.Text = new Bins().GetNumberOfBins().ToString();
                btnSaveBinSetting.Enabled = true;
                btnEditBinSetting.Enabled = true;
            }
        }

        private void btnSavePickListPrinterSetting_Click(object sender, EventArgs e)
        {
            SaveSettings();
            this.Close();
            MessageBox.Show("Settings Saved Successfully !");
        }

        private void btnSaveCN22FormSettings_Click(object sender, EventArgs e)
        {
            SaveCN22MappedPostalServices();
            SaveSettings();
            this.Close();
            MessageBox.Show("Settings Saved Successfully !");
        }

        private void SaveCN22MappedPostalServices()
        {

            var strSelectedPostalIds = string.Empty;

            foreach (DataGridViewRow gvr in grdCN22MappedServices.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)gvr.Cells["IsCN22_Required"];

                if (chk.Value == DBNull.Value) chk.Value = false;

                if (Convert.ToBoolean(chk.Value) == true)
                {
                    if (strSelectedPostalIds == "")
                    {
                        strSelectedPostalIds = grdCN22MappedServices.Rows[gvr.Index].Cells["pkPostalServiceId"].Value.ToString();
                    }
                    else
                    {
                        strSelectedPostalIds = strSelectedPostalIds + "," + grdCN22MappedServices.Rows[gvr.Index].Cells["pkPostalServiceId"].Value.ToString();
                    }
                }
            }
            Activator.CreateInstance<CN22Form>().UpdateCN22PostalServicesMappedSettingsFor(strSelectedPostalIds);
        }

        private void btnAddConditionsToProcessOrder_Click(object sender, EventArgs e)
        {
            Activator.CreateInstance<ODM.Appliction.ProcessOrderInstrctions.Condition>().Show();
        }

        private void btnRefreshProcessOrderSettings_Click(object sender, EventArgs e)
        {
            PopulateGridWithProcessOrderConditions();
        }

        private void EditCondition(string conditionId)
        {
            Cursor = Cursors.WaitCursor;
            var EditorForm = new ODM.Appliction.ProcessOrderInstrctions.Condition(Guid.Parse(conditionId));
            var dialogResult = EditorForm.ShowDialog();
            if (dialogResult != null)
            {
                btnRefreshProcessOrderSettings.PerformClick();
                Cursor = Cursors.Default;
            }
        }
        private void DeleteCondition(string conditionId, string conditionName)
        {
            DialogResult result = MessageBox.Show("Do You Want To Delete \" " + conditionName + " " + "\"?",
                "Delete Invoice Template",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Activator.CreateInstance<ODM.Core.ProcessOrderInstructions.ProcessOrderCondition>().DeleteCondition(conditionId);
                btnRefreshProcessOrderSettings.PerformClick();
            }
            else
            {
                btnRefreshProcessOrderSettings.PerformClick();
            }

        }
        private void CopyCondition(string conditionId)
        {
            Activator.CreateInstance<ODM.Core.ProcessOrderInstructions.ProcessOrderCondition>().CopyCondition(conditionId);
            btnRefreshProcessOrderSettings.PerformClick();
        }

        private void btnSaveProcessOrderSettings_Click(object sender, EventArgs e)
        {
            var updatedConditions = new List<ODM.Entities.Condition>();
            grdProcessOrderConditions.EndEdit();
            foreach (DataGridViewRow row in grdProcessOrderConditions.Rows)
            {
                updatedConditions.Add(
                    new ODM.Entities.Condition()
                    {
                        ConditionId = Guid.Parse(row.Cells["ConditionId"].Value.ToString()),
                        ConditionName = row.Cells["ConditionName"].Value.ToString(),
                        IsEnabled = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells["IsEnabled"]).Value)
                    });
            }
            Activator.CreateInstance<ODM.Core.ProcessOrderInstructions.ProcessOrderCondition>().UpdateConditions(updatedConditions);
            btnSaveProcessOrderSettings.Font = new Font(btnSaveProcessOrderSettings.Font.Name, btnSaveProcessOrderSettings.Font.Size, FontStyle.Regular);
            btnRefreshProcessOrderSettings.PerformClick();  
        }
    }
}
