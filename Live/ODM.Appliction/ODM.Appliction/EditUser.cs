﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class EditUser : Form
    {
        string strUserIDToUpdate = "";
        public EditUser()
        {
            InitializeComponent();
        }
        public EditUser(string strUserID)
        {
            InitializeComponent();
            strUserIDToUpdate = strUserID;
            PopulateControls(strUserID);
        }
        private void PopulateControls(string UserID)
        {
            if (UserID != "")
            {
                DataTable objDt = new DataTable();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlParameter[] objUsers = new SqlParameter[2];
                Guid guidUserID = new Guid(UserID);
                objUsers[0] = new SqlParameter("@UserID", SqlDbType.UniqueIdentifier);
                objUsers[0].Value = guidUserID;

                objUsers[1] = new SqlParameter("@Action", SqlDbType.VarChar, 2);
                objUsers[1].Value = "U";

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetUsersList", objUsers).Tables[0];
                if (objDt != null && objDt.Rows.Count > 0)
                {
                    txtFirstName.Text = Convert.ToString(objDt.Rows[0]["FirstName"]);
                    txtLastName.Text = Convert.ToString(objDt.Rows[0]["LastName"]);
                    txtEmail.Text = Convert.ToString(objDt.Rows[0]["Email"]);
                    txtPhone.Text = Convert.ToString(objDt.Rows[0]["Phone"]);
                    chkIsAdmin.Checked = Convert.ToBoolean(objDt.Rows[0]["IsAdmin"]);
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlParameter[] objUsers = new SqlParameter[7];
                Guid guidUserID = new Guid(strUserIDToUpdate);
               
                objUsers[0] = new SqlParameter("@UserID", SqlDbType.UniqueIdentifier);
                objUsers[0].Value = guidUserID;

                objUsers[1] = new SqlParameter("@FirstName", SqlDbType.VarChar,30);
                objUsers[1].Value = txtFirstName.Text;

                objUsers[2] = new SqlParameter("@LastName", SqlDbType.VarChar, 30);
                objUsers[2].Value = txtLastName.Text;

                objUsers[3] = new SqlParameter("@Phone", SqlDbType.VarChar, 30);
                objUsers[3].Value = txtPhone.Text;

                objUsers[4] = new SqlParameter("@Email", SqlDbType.VarChar, 50);
                objUsers[4].Value = txtEmail.Text;

                objUsers[5] = new SqlParameter("@IsAdmin", SqlDbType.Bit);
                objUsers[5].Value = chkIsAdmin.Checked;

                objUsers[6] = new SqlParameter("@Result", SqlDbType.Int);
                objUsers[6].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateUser", objUsers);
                int intResult = Convert.ToInt32(objUsers[6].Value);
                if(intResult==1)
                {
                    Form frmEditUser = Application.OpenForms["UsersList"];
                    if (frmEditUser != null)
                    {
                        frmEditUser.Close();
                        this.Close();
                        UsersList userList = new UsersList();
                        userList.Show();
                        MessageBox.Show("User Updated Successfully");
                    }
                }
            }
            catch(Exception ex)
            {
                string strError = ex.Message;
            }
        }
    }
}
