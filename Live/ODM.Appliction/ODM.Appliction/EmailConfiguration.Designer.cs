﻿namespace ODM.Appliction
{
    partial class EmailConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailConfiguration));
            this.grpbxTemplate = new System.Windows.Forms.GroupBox();
            this.grdTemplates = new System.Windows.Forms.DataGridView();
            this.btnAdditonalEmail = new System.Windows.Forms.Button();
            this.grpbxTemplate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTemplates)).BeginInit();
            this.SuspendLayout();
            // 
            // grpbxTemplate
            // 
            this.grpbxTemplate.Controls.Add(this.grdTemplates);
            this.grpbxTemplate.Location = new System.Drawing.Point(0, 41);
            this.grpbxTemplate.Name = "grpbxTemplate";
            this.grpbxTemplate.Size = new System.Drawing.Size(850, 556);
            this.grpbxTemplate.TabIndex = 0;
            this.grpbxTemplate.TabStop = false;
            this.grpbxTemplate.Text = "Templates";
            // 
            // grdTemplates
            // 
            this.grdTemplates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdTemplates.Location = new System.Drawing.Point(0, 13);
            this.grdTemplates.Name = "grdTemplates";
            this.grdTemplates.Size = new System.Drawing.Size(850, 537);
            this.grdTemplates.TabIndex = 0;
            this.grdTemplates.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdTemplates_CellDoubleClick);
            this.grdTemplates.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdTemplates_CellFormatting);
            // 
            // btnAdditonalEmail
            // 
            this.btnAdditonalEmail.Location = new System.Drawing.Point(343, 12);
            this.btnAdditonalEmail.Name = "btnAdditonalEmail";
            this.btnAdditonalEmail.Size = new System.Drawing.Size(151, 23);
            this.btnAdditonalEmail.TabIndex = 20;
            this.btnAdditonalEmail.Text = "Additional Email Accounts";
            this.btnAdditonalEmail.UseVisualStyleBackColor = true;
            this.btnAdditonalEmail.Click += new System.EventHandler(this.btnAdditonalEmail_Click);
            // 
            // EmailConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 599);
            this.Controls.Add(this.btnAdditonalEmail);
            this.Controls.Add(this.grpbxTemplate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmailConfiguration";
            this.Text = "Email Sender Configuration";
            this.grpbxTemplate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdTemplates)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpbxTemplate;
        private System.Windows.Forms.DataGridView grdTemplates;
        private System.Windows.Forms.Button btnAdditonalEmail;

    }
}