﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class AmazonCourierSettings : Form
    {
        #region "Global Variables"
        string _CourierName = "";
        string _CourierID = "";
        SqlConnection connection;
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        string strDefaultPrinter = "";
        bool blIsFirstCall = false;
        DataTable objDt = new DataTable();
        #endregion

        #region "Constructors"
        public AmazonCourierSettings()
        {
            InitializeComponent();
            blIsFirstCall = true;
            BindCountryDdlAndUpdateServiceURL("GET");
            blIsFirstCall = false;
        }
        public AmazonCourierSettings(string strCourierName, string strCourierID)
        {
            InitializeComponent();
            _CourierName = strCourierName;
            _CourierID = strCourierID;
            PopulateControls();
            BindGrid();
            blIsFirstCall = true;
            BindCountryDdlAndUpdateServiceURL("GET");
            blIsFirstCall = false;
        }
        #endregion

        #region "Events"
        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                SaveSettings();
                BindCountryDdlAndUpdateServiceURL("UPDATE");
                MapCourierToService();
                GlobalVariablesAndMethods.SelectedPrinterNameForAmazon = ddlPrinter.SelectedItem.ToString();
                GlobalVar.AddUpdatePrinterName();
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Form objAddService = Application.OpenForms["AddServices"];
            if (objAddService == null)
            {
                AddServices objServices = new AddServices(_CourierName, _CourierID);
                objServices.Show();
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure, you want to delete Postal Service ?", "Delete Postal Service", MessageBoxButtons.YesNo);
            if (result.Equals(DialogResult.Yes))
            {
                DeleteService();
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Form frmAddService = Application.OpenForms["AddServices"];
            if (frmAddService != null)
            {
                frmAddService.Close();
            }
            this.Close();
        }
        #endregion

        #region "Methods"
        private void BindCountryDdlAndUpdateServiceURL(string strMode)
        {
            SqlConnection conn = new SqlConnection();
            string strID = ""; int intResult = 100;
            try
            {
                conn.ConnectionString = MakeConnection.GetConnection();
                conn.Open();
                if (ddlCountryName.SelectedValue != null)
                {
                    strID = Convert.ToString(ddlCountryName.SelectedValue.ToString());
                }
                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 20);
                objParam[0].Value = strMode;

                objParam[1] = new SqlParameter("@ID", SqlDbType.VarChar, 40);
                objParam[1].Value = strID;

                objParam[2] = new SqlParameter("@ServiceURL", SqlDbType.VarChar, 500);
                objParam[2].Value = txtServiceURL.Text;

                objParam[3] = new SqlParameter("@Result", SqlDbType.Int);
                objParam[3].Direction = ParameterDirection.Output;

                if (strMode.ToUpper() == "GET")
                {
                    objDt = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAndUpdateServiceURL", objParam).Tables[0];
                }
                else if (strMode.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAndUpdateServiceURL", objParam);
                }
                intResult = Convert.ToInt32(objParam[3].Value);
                if (blIsFirstCall)
                {
                    if (objDt.Rows.Count > 0)
                    {
                        ddlCountryName.DataSource = objDt;
                        ddlCountryName.ValueMember = "ID";
                        ddlCountryName.DisplayMember = "CountryName";
                        ddlCountryName.SelectedValue = Convert.ToString(objDt.Rows[0]["ID"]);
                        txtServiceURL.Text = Convert.ToString(objDt.Rows[0]["ServiceURL"]);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occrred", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }
        private void MapCourierToService()
        {
            try
            {
                int intResult = 100;
                if (grdMappedServices.Rows.Count > 0)
                {
                    for (int i = 0; i < grdMappedServices.Rows.Count; i++)
                    {
                        string strSelectedCourierID = Convert.ToString(grdMappedServices.Rows[i].Cells["ID"].Value);
                        string strSelectedValue = Convert.ToString(grdMappedServices[5, i].Value);
                        string strServiceName = Convert.ToString(grdMappedServices.Rows[i].Cells["PostalServiceName"].Value);
                        if (strSelectedValue == "")
                        {
                            MessageBox.Show(string.Format("Please select courier for the service {0}", strServiceName));
                            intResult = -1;
                            break;
                        }
                        else
                        {
                            connection = new SqlConnection();
                            Guid MappedID = new Guid(strSelectedValue);
                            Guid ID = new Guid(strSelectedCourierID);
                            Guid DummyID = new Guid();
                            connection.ConnectionString = MakeConnection.GetConnection();
                            SqlParameter[] objParams = new SqlParameter[6];

                            objParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                            objParams[0].Value = ID;

                            objParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                            objParams[1].Value = DummyID;

                            objParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                            objParams[2].Value = DummyID;

                            objParams[3] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                            objParams[3].Value = MappedID;

                            objParams[4] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                            objParams[4].Value = "UPDATE";

                            objParams[5] = new SqlParameter("@Result", SqlDbType.Int);
                            objParams[5].Direction = ParameterDirection.Output;

                            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteServicesToMapAmazonCourier", objParams);

                            intResult = Convert.ToInt32(objParams[5].Value);
                        }
                    }
                    if (intResult == 1)
                    {
                        this.Close();
                        MessageBox.Show("Settings Saved Successfully !", "Record Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    this.Close();
                    MessageBox.Show("Settings Saved Successfully !", "Record Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }
        private void BindGrid()
        {
            try
            {
                connection = new SqlConnection();
                DataTable objDtGridData = new DataTable();
                DataTable objCourierList = new DataTable();
                connection.ConnectionString = MakeConnection.GetConnection();
                objDtGridData = SqlHelper.ExecuteDataset(connection, "GetAmazonMappedCouriers").Tables[0];
                objCourierList = SqlHelper.ExecuteDataset(connection, "GetAllAmazonCourierToMapWithServices").Tables[0];

                if (objDtGridData.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(objDtGridData.Rows[0]["ID"])))
                {
                    grdMappedServices.Visible = true;
                    grdMappedServices.AllowUserToAddRows = false;
                    grdMappedServices.DataSource = objDtGridData;
                    grdMappedServices.AutoGenerateColumns = false;

                    grdMappedServices.Columns[0].Visible = false;
                    grdMappedServices.Columns[1].Visible = false;
                    grdMappedServices.Columns[2].Visible = false;
                    grdMappedServices.Columns[4].Visible = false;
                    grdMappedServices.Columns[3].ReadOnly = true;
                    grdMappedServices.Columns[3].Width = 370;

                    DataGridViewComboBoxColumn ddlCouriers = new DataGridViewComboBoxColumn();
                    ddlCouriers.HeaderText = "Courier Name";
                    ddlCouriers.Name = "Courier_Name";
                    ddlCouriers.ValueMember = "CourierID";
                    ddlCouriers.DataPropertyName = "MapedCourierId";
                    ddlCouriers.DisplayMember = "CourierName";
                    grdMappedServices.Columns.Add(ddlCouriers);
                    ddlCouriers.DataSource = objCourierList;
                    grdMappedServices.Columns[5].Width = 370;
                    lblMessage.Visible = false;
                }
                else
                {
                    grdMappedServices.Visible = false;
                    lblMessage.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An Error Occurred Contact the Administrator");
            }
            finally
            {
                connection.Close();
            }
        }
        private void SaveSettings()
        {
            connection = new SqlConnection();
            try
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                Guid CourierID = new Guid(_CourierID);
                SqlParameter[] objParams = new SqlParameter[7];

                objParams[0] = new SqlParameter("@CourierId", SqlDbType.UniqueIdentifier);
                objParams[0].Value = CourierID;

                objParams[1] = new SqlParameter("@CourierName", SqlDbType.VarChar, 500);
                objParams[1].Value = _CourierName;

                objParams[2] = new SqlParameter("@AccessKey", SqlDbType.VarChar, 100);
                objParams[2].Value = txtAccessKey.Text;

                objParams[3] = new SqlParameter("@SecretKey", SqlDbType.VarChar, 100);
                objParams[3].Value = txtSecretKey.Text;

                objParams[4] = new SqlParameter("@SellerID", SqlDbType.VarChar, 100);
                objParams[4].Value = txtSellerID.Text;

                objParams[5] = new SqlParameter("@AppName", SqlDbType.VarChar, 100);
                objParams[5].Value = txtAppName.Text;

                objParams[6] = new SqlParameter("@AppVersion", SqlDbType.VarChar, 500);
                objParams[6].Value = txtAppVersion.Text;

                SqlHelper.ExecuteNonQuery(connection, "AddUpdateAmazon", objParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        private void PopulateControls()
        {
            try
            {
                DataTable ObjdtCourier = new DataTable();
                ObjdtCourier = GlobalVar.GetCourierDetailsByNameOrId(_CourierName, _CourierID);
                if (ObjdtCourier.Rows.Count > 0)
                {
                    txtAccessKey.Text = Convert.ToString(ObjdtCourier.Rows[0]["AccessKey"]);
                    txtSecretKey.Text = Convert.ToString(ObjdtCourier.Rows[0]["SecretKey"]);
                    txtSellerID.Text = Convert.ToString(ObjdtCourier.Rows[0]["SellerID"]);
                    txtAppName.Text = Convert.ToString(ObjdtCourier.Rows[0]["AppName"]);
                    txtAppVersion.Text = Convert.ToString(ObjdtCourier.Rows[0]["AppVersion"]);

                    ddlPrinter.DataSource = GlobalVar.AttachedPrinters(ref strDefaultPrinter);
                    ddlPrinter.SelectedItem = !string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForAmazon) ?
                                              GlobalVariablesAndMethods.SelectedPrinterNameForAmazon : strDefaultPrinter;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool ValidateControls()
        {
            bool IsValid = true;

            if (string.IsNullOrWhiteSpace(txtAccessKey.Text))
            {
                lblAccessKeyRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAccessKeyRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtSecretKey.Text))
            {
                lblSecretKeyRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblSecretKeyRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtSellerID.Text))
            {
                lblSellerIDRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblSellerIDRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAppVersion.Text))
            {
                lblAppVersionRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAppVersionRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAppName.Text))
            {
                lblAppNameRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAppNameRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtServiceURL.Text))
            {
                lblUrlRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblUrlRequired.Visible = false;
            }
            return IsValid;
        }
        private void DeleteService()
        {
            try
            {
                int RowIndex = 0;
                if (grdMappedServices.CurrentCell != null)
                {
                    RowIndex = Convert.ToInt32(grdMappedServices.CurrentCell.RowIndex);
                    string strSelectedCourierID = Convert.ToString(grdMappedServices.Rows[RowIndex].Cells[0].Value);
                    connection.ConnectionString = MakeConnection.GetConnection();
                    Guid ID = new Guid(strSelectedCourierID);
                    Guid DummyID = new Guid();

                    SqlParameter[] objParams = new SqlParameter[6];

                    objParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                    objParams[0].Value = ID;

                    objParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                    objParams[1].Value = DummyID;

                    objParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                    objParams[2].Value = DummyID;

                    objParams[3] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                    objParams[3].Value = DummyID;

                    objParams[4] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                    objParams[4].Value = "DELETE";

                    objParams[5] = new SqlParameter("@Result", SqlDbType.Int);
                    objParams[5].Direction = ParameterDirection.Output;

                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteServicesToMapAmazonCourier", objParams);

                    int intResult = Convert.ToInt32(objParams[5].Value);
                    if (intResult == 2)
                    {
                        BindGrid();
                        MessageBox.Show("Record deleted successfully !");
                    }
                }
                else
                {
                    MessageBox.Show("Please select a record to delete");
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }
        #endregion

        private void ddlCountryName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!blIsFirstCall)
            {
                BindCountryDdlAndUpdateServiceURL("GET");
                if (objDt.Rows.Count > 0)
                {
                    txtServiceURL.Text = Convert.ToString(objDt.Rows[0]["ServiceURL"]);
                    txtServiceURL.Focus();
                }
            }
        }

        private void btnManifest_Click(object sender, EventArgs e)
        {
            Form frmGetManifest = Application.OpenForms["Manifest"];
            if (frmGetManifest == null)
            {
                Manifest objManifest = new Manifest(_CourierName);
                objManifest.Show();
            }
        }

        private void btnSenderDetails_Click(object sender, EventArgs e)
        {
            Form frmAmazonSenderDetails = Application.OpenForms["AmazonSenderDetails"];
            if (frmAmazonSenderDetails == null)
            {
                AmazonSenderDetails objAmazonSenderDetails = new AmazonSenderDetails();
                objAmazonSenderDetails.Show();
            }
        }
    }
}
