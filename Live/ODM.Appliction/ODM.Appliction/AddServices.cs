﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class AddServices : Form
    {
        string _CourierName = "";
        string _CourierID = "";
        string _AllCourierName = "ALL";
        public AddServices()
        {
            InitializeComponent();
        }

        public AddServices(string strCourierName, string strCourierID)
        {
            InitializeComponent();
            _CourierName = strCourierName;
            _CourierID = strCourierID;
            BindGrid();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int intRowIndex = 0;
                Guid DummyID = new Guid();
                Guid CourierId = new Guid(_CourierID);

                intRowIndex = grdServices.CurrentCell.RowIndex;
                string strSelectedServiceID = Convert.ToString(grdServices.Rows[intRowIndex].Cells[0].Value);
                if (!string.IsNullOrEmpty(strSelectedServiceID))
                {
                    Guid ServiceID = new Guid(strSelectedServiceID);
                    AddServiceToMapCouriers(DummyID, CourierId, ServiceID);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An error occurred while adding service to map, contact the Administrator");
            }
        }

        private void AddServiceToMapCouriers(Guid DummyID, Guid CourierId, Guid ServiceID)
        {
            SqlConnection connection = new SqlConnection();
            try
            {
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                string strProcedureName = "";
                switch (_CourierName.ToUpper())
                {
                    case "MYHERMES":
                        {
                            strProcedureName = "AddUpdateDeleteServicesToMapCourier";
                        }
                        break;
                    case "HERMES":
                        {
                            strProcedureName = "AddUpdateDeleteServicesToMapHermesCourier";
                        }
                        break;
                    case "SPRING":
                        {
                            //strProcedureName = "AddUpdateDeleteServicesToMapSpringCourier";
                            strProcedureName = "AddUpdateDeleteServicesToMapSpringCourier_New";
                        }
                        break;
                    case"DPD UK":
                    case "DPD":
                        {
                            strProcedureName = "AddUpdateDeleteServicesToMapDPDCourier";
                        }
                        break;
                    case "ROYAL MAIL":
                        {
                            strProcedureName = "AddUpdateDeleteServicesToMapRoyalMailCourier";
                        }
                        break;
                    case "FEDEX":
                        {
                            strProcedureName = "AddUpdateDeleteServicesToMapFedExCourier";
                        }
                        break;
                    case "AMAZON":
                        {
                            strProcedureName = "AddUpdateDeleteServicesToMapAmazonCourier";
                        }
                        break;
                }

                SqlParameter[] objServicesParams = new SqlParameter[7];

                objServicesParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objServicesParams[0].Value = DummyID;

                objServicesParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                objServicesParams[1].Value = CourierId;

                objServicesParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                objServicesParams[2].Value = ServiceID;

                objServicesParams[3] = new SqlParameter("@PackageTypeID", SqlDbType.UniqueIdentifier);
                objServicesParams[3].Value = DummyID;


                objServicesParams[4] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                objServicesParams[4].Value = DummyID;

                objServicesParams[5] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                objServicesParams[5].Value = "ADD";

                objServicesParams[6] = new SqlParameter("@Result", SqlDbType.Int);
                objServicesParams[6].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, strProcedureName, objServicesParams);
                int intResult = Convert.ToInt32(objServicesParams[6].Value);
                if (intResult == 0)
                {
                    this.Close();
                    CloseWindowByCourierName();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        private void CloseWindowByCourierName()
        {
            switch (_CourierName.ToUpper())
            {
                case "MYHERMES":
                    {
                        Form frmMyHermesSettings = Application.OpenForms["myHermesSettings"];
                        if (frmMyHermesSettings != null)
                        {
                            frmMyHermesSettings.Close();
                            myHermesSettings objHermes = new myHermesSettings(_CourierName, _CourierID);
                            objHermes.Show();
                        }
                    }
                    break;
                case "HERMES":
                    {
                        Form frmHermesSettings = Application.OpenForms["HermesSetting"];
                        if (frmHermesSettings != null)
                        {
                            frmHermesSettings.Close();
                            HermesSetting objHermes = new HermesSetting(_CourierName, _CourierID);
                            objHermes.Show();
                        }
                    }
                    break;
                case "SPRING":
                    {
                        //Form frmMySpringSettings = Application.OpenForms["SpringSettings"];
                        Form frmMySpringSettings = Application.OpenForms["SpringCourierSettings_New"];
                        if (frmMySpringSettings != null)
                        {
                            frmMySpringSettings.Close();
                            //SpringSettings objSpring = new SpringSettings(_CourierName, _CourierID);
                            SpringCourierSettings_New objSpring = new SpringCourierSettings_New(_CourierName, _CourierID);
                            objSpring.Show();
                        }
                    }
                    break;
                case "DPD UK":
                case "DPD":
                    {
                        Form frmMyDPDSettings = Application.OpenForms["DPDCourierSettings"];
                        if (frmMyDPDSettings != null)
                        {
                            frmMyDPDSettings.Close();
                            DPDCourierSettings objDPD = new DPDCourierSettings(_CourierName, _CourierID);
                            objDPD.Show();
                        }
                    }
                    break;
                case "ROYAL MAIL":
                    {
                        Form frmRoyalMailSettings = Application.OpenForms["RoyalMailSettings"];
                        if (frmRoyalMailSettings != null)
                        {
                            frmRoyalMailSettings.Close();
                            RoyalMailSettings objRoyalMail = new RoyalMailSettings(_CourierName, _CourierID);
                            objRoyalMail.Show();
                        }
                    }
                    break;
                case "FEDEX":
                    {
                        Form frmFedExSettings = Application.OpenForms["FedExSettings"];
                        if (frmFedExSettings != null)
                        {
                            frmFedExSettings.Close();
                            FedExSettings objFedEx = new FedExSettings(_CourierName, _CourierID);
                            objFedEx.Show();
                        }
                    }
                    break;
                case "AMAZON":
                    {
                        Form frmMyAmazonSettings = Application.OpenForms["AmazonCourierSettings"];
                        if (frmMyAmazonSettings != null)
                        {
                            frmMyAmazonSettings.Close();
                            AmazonCourierSettings objAmazon = new AmazonCourierSettings(_CourierName, _CourierID);
                            objAmazon.Show();
                        }
                    }
                    break;
            }
        }

        private void BindGrid()
        {
            SqlConnection connection = new SqlConnection();
            try
            {
                DataTable objDt = new DataTable();
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();

                SqlParameter[] objParams = new SqlParameter[1];

                objParams[0] = new SqlParameter("@CourierName", SqlDbType.VarChar, 5000);
                //objParams[0].Value = _CourierName;
                objParams[0].Value = _AllCourierName.ToUpper();

                objDt = SqlHelper.ExecuteDataset(connection, "GetServicesToCourier", objParams).Tables[0];

                if (objDt.Rows.Count > 0)
                {
                    grdServices.EditMode = DataGridViewEditMode.EditProgrammatically;
                    grdServices.AllowUserToAddRows = false;
                    grdServices.DataSource = objDt;
                    grdServices.Columns[0].Visible = false;
                    grdServices.Columns[1].Width = 300;
                    lblError.Visible = false;
                    btnAdd.Visible = true;
                }
                else
                {
                    lblError.Text = "No Record Found!";
                    btnAdd.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
