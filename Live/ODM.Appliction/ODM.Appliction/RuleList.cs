﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class RuleList : Form
    {
        string strMode = "";
        public RuleList()
        {
            InitializeComponent();
            BindGridForRule();
        }
        private void PopulateData(ref DataGridView grdRuleList)
        {

            grdRuleList.Columns["ID"].Visible = false;
            grdRuleList.Columns["ConditionID"].Visible = false;
            grdRuleList.Columns["FolderID"].Visible = false;
            grdRuleList.Columns["ConditionID"].Visible = false;
            //    grdRuleList.Columns["IsActive"].Visible = false;


        }

        private void BindGridForRule()
        {
            DataTable objDt = new DataTable();
            strMode = "GET";
            string strConditionID = "00000000-0000-0000-0000-000000000000";
            objDt = GlobalVariablesAndMethods.GetAddUpdateDeleteRules(strMode, new Guid(), new Guid(), "", "", 0, true, strConditionID);
            if (objDt.Rows.Count > 0)
            {
                grdRuleList.EditMode = DataGridViewEditMode.EditProgrammatically;
                grdRuleList.AllowUserToAddRows = false;
                objDt.Columns["IsActive"].DataType = typeof(Boolean);
                grdRuleList.DataSource = objDt;
                PopulateData(ref grdRuleList);

            }
            else
            {
                lblNoRuleFound.Visible = true;
            }
        }
        private void btnAddRule_Click(object sender, EventArgs e)
        {
            RuleSettings objRuleSettings = new RuleSettings();
            objRuleSettings.ShowDialog();
            BindGridForRule();
        }



        private void grdRuleList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string strID = grdRuleList.CurrentRow.Cells["ID"].Value.ToString();
            string strFolderID = grdRuleList.CurrentRow.Cells["FolderID"].Value.ToString();
            string strRuleName = grdRuleList.CurrentRow.Cells["RuleName"].Value.ToString();
            string strRuleConditionText = grdRuleList.CurrentRow.Cells["Condition"].Value.ToString();
            string strRuleConditionID = grdRuleList.CurrentRow.Cells["ConditionID"].Value.ToString();
            int Position = Convert.ToInt16(grdRuleList.CurrentRow.Cells["Position"].Value.ToString());
            bool IsActive = Convert.ToBoolean(grdRuleList.CurrentRow.Cells["IsActive"].Value.ToString());
            RuleSettings objRuleSettings = new RuleSettings("UPDATE", strRuleName, strRuleConditionText, strFolderID, new Guid(strID), Position, IsActive, strRuleConditionID);
            objRuleSettings.ShowDialog();
            BindGridForRule();
        }

        private void btnRuleCondition_Click(object sender, EventArgs e)
        {
            RuleConditions objRuleConditions = new RuleConditions();
            objRuleConditions.Show();
        }

        private void btnDeleteRule_Click(object sender, EventArgs e)
        {
            DialogResult objDResult = MessageBox.Show("Are you sure you want to delete this Rule ?", "Delete Rule", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (objDResult == DialogResult.Yes)
            {
                string strDeleteRuleResult = GlobalVariablesAndMethods.GetAddUpdateDeleteRules("DELETE", new Guid(grdRuleList.CurrentRow.Cells["ID"].Value.ToString()), new Guid(grdRuleList.CurrentRow.Cells["FolderID"].Value.ToString()), grdRuleList.CurrentRow.Cells["RuleName"].Value.ToString(), grdRuleList.CurrentRow.Cells["Condition"].Value.ToString(), Convert.ToInt16(grdRuleList.CurrentRow.Cells["Position"].Value.ToString()), Convert.ToBoolean(grdRuleList.CurrentRow.Cells["IsActive"].Value.ToString()), grdRuleList.CurrentRow.Cells["ConditionID"].Value.ToString());
                if (strDeleteRuleResult == "3")
                {
                    MessageBox.Show("Rule has been deleted successfully", "Delete Rule", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    BindGridForRule();
                }
            }
        }


    }
}
