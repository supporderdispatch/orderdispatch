﻿using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Linq;
namespace ODM.Appliction
{
    public partial class GetOrder : Form
    {
        public GetOrder()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadJson();
            //===========================================================================================================
            //var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://eu1.linnworks.net//api/Orders/GetAllOpenOrders");
            //httpWebRequest.ContentType = "application/json";
            //httpWebRequest.Method = "POST";

            //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //{
            //    string json = "{\"Host\":\"eu1.linnworks.net\"," +
            //                   "\"Connection\":\"keep-alive\"," +
            //                  "\"Accept\":\"application/json, text/javascript, */*; q=0.01\"}" +
            //                  "\"Origin\":\"https://www.linnworks.net\"}" +
            //                  "\"Accept-Language\":\"en\"," +
            //                  "\"User-Agent\":\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36\"," +
            //                  "\"Content-Type\":\"application/x-www-form-urlencoded; charset=UTF-8\"," +
            //                  "\"Referer\":\"https://www.linnworks.net\"," +
            //                  "\"Accept-Encoding\":\"gzip, deflate\"," +
            //                  "\"Authorization:\":\"be6c4095945b0457151129ac87f2ef9e\"}";


            //    streamWriter.Write(json);
            //    streamWriter.Flush();
            //    streamWriter.Close();
            //}

            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //{
            //    var result = streamReader.ReadToEnd();
            //}
            //=============================================================================================================


          //   Create a request using a URL that can receive a post. 
            //WebRequest request = WebRequest.Create("https://eu1.linnworks.net//api/Orders/GetAllOpenOrders");
            //// Set the Method property of the request to POST.
            //request.Method = "POST";





            //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            //// Set the ContentType property of the WebRequest.
            //request.ContentType = "application/x-www-form-urlencoded";
            //// Set the ContentLength property of the WebRequest.
            //request.ContentLength = byteArray.Length;

            //// Get the request stream.
            //Stream dataStream = request.GetRequestStream();
            //// Write the data to the request stream.
            //dataStream.Write(byteArray, 0, byteArray.Length);
            //// Close the Stream object.
            //dataStream.Close();

            //// Get the response.
            //WebResponse response = request.GetResponse();
            //// Display the status.
            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            //// Get the stream containing content returned by the server.
            //dataStream = response.GetResponseStream();
            //// Open the stream using a StreamReader for easy access.
            //StreamReader reader = new StreamReader(dataStream);
            //// Read the content.
            //string responseFromServer = reader.ReadToEnd();
            //// Display the content.
            //Console.WriteLine(responseFromServer);
            //// Clean up the streams.
            //reader.Close();
            //dataStream.Close();
            //response.Close();



        }

        public void LoadJson()
        {
            try
            {
                using (StreamReader r = new StreamReader("order.json"))
                {
                    string json = r.ReadToEnd();
                    dynamic array = JsonConvert.DeserializeObject(json);
                    string strJson = Convert.ToString(array);
                    //List<OrderData> items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderData>>(json);
                    dynamic objData = Newtonsoft.Json.JsonConvert.DeserializeObject(strJson);

                    if (objData != null)
                    {
                        OrderData objOrderData = new OrderData();
                        Order objOrder = new Order();
                        Shipping objShipping = new Shipping();
                        Customer objCustomer = new Customer();
                        Address objCustomerAddress = new Address();
                        Address objBillingAddress = new Address();

                        SqlConnection connection = new SqlConnection();

                        //Generating new guids
                        Guid CustomerID = Guid.NewGuid();
                        Guid CreatedBy = Guid.NewGuid();
                        Guid ShippingID = Guid.NewGuid();
                        //Guid ModifiedBy = Guid.NewGuid();
                        Guid ModifiedBy = new Guid();
                        Guid OrderIteamId = Guid.NewGuid();
                        Guid AddressID = Guid.NewGuid();
                        Guid BillingAddressID = Guid.NewGuid();
                        //End of Generating new guids

                        #region "Mapping of Order class"
                        string strGetOrderInfo = Convert.ToString(objData["GeneralInfo"]);
                        objOrder = Newtonsoft.Json.JsonConvert.DeserializeObject<Order>(strGetOrderInfo);
                        objOrder.Orderid = (Guid)objData["OrderId"];
                        objOrder.NumOrderId = (long)objData["NumOrderId"];
                        objOrder.CustomerId = CustomerID;

                        //Map TotalsInfo node with Order Class
                        objOrder.Subtotal = (decimal)(objData["TotalsInfo"]["Subtotal"]);
                        objOrder.PostageCost = (decimal)(objData["TotalsInfo"]["PostageCost"]);
                        objOrder.Tax = (decimal)(objData["TotalsInfo"]["Tax"]);
                        objOrder.TotalCharge = (decimal)(objData["TotalsInfo"]["TotalCharge"]);
                        objOrder.PaymentMethod = Convert.ToString(objData["TotalsInfo"]["PaymentMethod"]);
                        objOrder.PaymentMethodId = (Guid)(objData["TotalsInfo"]["PaymentMethodId"]);
                        objOrder.ProfitMargin = (decimal)(objData["TotalsInfo"]["ProfitMargin"]);
                        objOrder.TotalDiscount = (decimal)(objData["TotalsInfo"]["TotalDiscount"]);
                        objOrder.Currency = Convert.ToString(objData["TotalsInfo"]["Currency"]);
                        objOrder.CountryTaxRate = (decimal)(objData["TotalsInfo"]["CountryTaxRate"]);
                        //End of Map TotalsInfo node with Order Class

                        #endregion

                        #region "Mapping of Customer class"
                        string strGetCustomerData = Convert.ToString(objData["CustomerInfo"]);
                        objCustomer = Newtonsoft.Json.JsonConvert.DeserializeObject<Customer>(strGetCustomerData);
                        objCustomer.ChannelBuyerName = Convert.ToString(objData["CustomerInfo"]["ChannelBuyerName"]);
                        objCustomer.EmailAddress = Convert.ToString(objData["CustomerInfo"]["Address"]["EmailAddress"]);
                        objCustomer.CustomerId = CustomerID;

                        //(Unsure fields)We have to confirm that from where we will get these values 
                        //(these values are available in both node Address/BillingAddress)
                        //currently getting these values from Address Node to fill dummy data
                        objCustomer.Town = Convert.ToString(objData["CustomerInfo"]["Address"]["Town"]);
                        objCustomer.FullName = Convert.ToString(objData["CustomerInfo"]["Address"]["FullName"]);
                        objCustomer.Company = Convert.ToString(objData["CustomerInfo"]["Address"]["Company"]);
                        //End of (Unsure fields)

                        objCustomer.CreatedBy = CreatedBy;
                        objCustomer.ModifiedBy = ModifiedBy;
                        #endregion

                        #region "Mapping of Shipping class"
                        string strGetShippingData = Convert.ToString(objData["ShippingInfo"]);
                        objShipping = Newtonsoft.Json.JsonConvert.DeserializeObject<Shipping>(strGetShippingData);
                        objShipping.OrderId = (Guid)(objData["OrderId"]);
                        objShipping.AddressId = AddressID;
                        objShipping.ShippingId = ShippingID;
                        #endregion

                        #region "Mapping of Address/BillingAddress for customer(Pointing Address Class)"
                        string strGetCustomerAddress = Convert.ToString(objData["CustomerInfo"]["Address"]);
                        objCustomerAddress = Newtonsoft.Json.JsonConvert.DeserializeObject<Address>(strGetCustomerAddress);
                        objCustomerAddress.CustomerId = CustomerID;

                        string strGetCustomerBillingAddress = Convert.ToString(objData["CustomerInfo"]["BillingAddress"]);
                        objBillingAddress = Newtonsoft.Json.JsonConvert.DeserializeObject<Address>(strGetCustomerBillingAddress);
                        objBillingAddress.CustomerId = CustomerID;
                        #endregion

                        #region "Mapping of Order Iteam Table"
                        string strGetOrderIteams = Convert.ToString(objData["Items"]);
                        //Deserializing "Items" node with List<OrderItem> object because that is an Array node
                        List<OrderItem> objListOrderIteam = (List<OrderItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(strGetOrderIteams, typeof(List<OrderItem>));
                        OrderItem objOrderIteam = new OrderItem();
                        objOrderIteam = objListOrderIteam[0];
                        objOrderIteam.OrderItemId = OrderIteamId;
                        objOrderIteam.CreatedBy = CreatedBy;
                        objOrderIteam.ModifiedBy = ModifiedBy;
                        #endregion

                        objOrderData.customer = objCustomer;
                        objOrderData.shipping = objShipping;
                        objOrderData.order = objOrder;

                        #region "Database Connectivity"

                        try
                        {
                            connection.ConnectionString = MakeConnection.GetConnection();
                            connection.Open();

                            //Note Keep the insertion of customer first than order becasue of pk-fk relationship
                            #region "Insert Data In Customer Table"
                            SqlParameter[] objCustomerParams = new SqlParameter[10];

                            objCustomerParams[0] = new SqlParameter("@CustomerId", SqlDbType.UniqueIdentifier);
                            objCustomerParams[0].Value = objCustomer.CustomerId;

                            objCustomerParams[1] = new SqlParameter("@ChannelBuyerName", SqlDbType.NVarChar, 200);
                            objCustomerParams[1].Value = objCustomer.ChannelBuyerName;

                            objCustomerParams[2] = new SqlParameter("@EmailAddress", SqlDbType.NVarChar, 200);
                            objCustomerParams[2].Value = objCustomer.EmailAddress;

                            objCustomerParams[3] = new SqlParameter("@Town", SqlDbType.NVarChar, 200);
                            objCustomerParams[3].Value = objCustomer.Town;

                            objCustomerParams[4] = new SqlParameter("@FullName", SqlDbType.NVarChar, 200);
                            objCustomerParams[4].Value = objCustomer.FullName;

                            objCustomerParams[5] = new SqlParameter("@Company", SqlDbType.NVarChar, 200);
                            objCustomerParams[5].Value = objCustomer.Company;

                            objCustomerParams[6] = new SqlParameter("@CreatedBy", SqlDbType.UniqueIdentifier);
                            objCustomerParams[6].Value = objCustomer.CreatedBy;

                            objCustomerParams[7] = new SqlParameter("@ModifiedBy", SqlDbType.UniqueIdentifier);
                            objCustomerParams[7].Value = objCustomer.ModifiedBy;

                            objCustomerParams[8] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                            objCustomerParams[8].Value = "N";

                            objCustomerParams[9] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                            objCustomerParams[9].Direction = ParameterDirection.Output;

                            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "CustomerAddUpdateDelete", objCustomerParams);
                            string strCustomerResult = objCustomerParams[9].Value.ToString();
                            #endregion

                            #region "Insert Data In Order Table"
                            SqlParameter[] objOrderParams = new SqlParameter[33];

                            objOrderParams[0] = new SqlParameter("@Orderid", SqlDbType.UniqueIdentifier);
                            objOrderParams[0].Value = objOrder.Orderid;

                            objOrderParams[1] = new SqlParameter("@NumOrderId", SqlDbType.BigInt);
                            objOrderParams[1].Value = objOrder.NumOrderId;

                            objOrderParams[2] = new SqlParameter("@Status", SqlDbType.BigInt);
                            objOrderParams[2].Value = objOrder.Status;

                            objOrderParams[3] = new SqlParameter("@LabelPrinted", SqlDbType.BigInt);
                            objOrderParams[3].Value = objOrder.LabelPrinted;

                            objOrderParams[4] = new SqlParameter("@LabelError", SqlDbType.NVarChar, 100);
                            objOrderParams[4].Value = objOrder.LabelError;

                            objOrderParams[5] = new SqlParameter("@InvoicePrinted", SqlDbType.Bit);
                            objOrderParams[5].Value = objOrder.InvoicePrinted;

                            objOrderParams[6] = new SqlParameter("@PickListPrinted", SqlDbType.BigInt);
                            objOrderParams[6].Value = objOrder.PickListPrinted;

                            objOrderParams[7] = new SqlParameter("@Notes", SqlDbType.NVarChar, 200);
                            objOrderParams[7].Value = objOrder.Notes;

                            objOrderParams[8] = new SqlParameter("@PartShipped", SqlDbType.Bit);
                            objOrderParams[8].Value = objOrder.PartShipped;

                            objOrderParams[9] = new SqlParameter("@Marker", SqlDbType.BigInt);
                            objOrderParams[9].Value = objOrder.Marker;

                            objOrderParams[10] = new SqlParameter("@ReferenceNum", SqlDbType.NVarChar, 100);
                            objOrderParams[10].Value = objOrder.ReferenceNum;

                            objOrderParams[11] = new SqlParameter("@SecondaryReference", SqlDbType.NVarChar, 100);
                            objOrderParams[11].Value = objOrder.SecondaryReference;

                            objOrderParams[12] = new SqlParameter("@ExternalReferenceNum", SqlDbType.NVarChar);
                            objOrderParams[12].Value = objOrder.ExternalReferenceNum;

                            objOrderParams[13] = new SqlParameter("@ReceivedDate", SqlDbType.Date);
                            objOrderParams[13].Value = objOrder.ReceivedDate;

                            objOrderParams[14] = new SqlParameter("@Source", SqlDbType.NVarChar, 50);
                            objOrderParams[14].Value = objOrder.Source;

                            objOrderParams[15] = new SqlParameter("@SubSource", SqlDbType.NVarChar, 50);
                            objOrderParams[15].Value = objOrder.SubSource;

                            objOrderParams[16] = new SqlParameter("@HoldOrCancel", SqlDbType.Bit);
                            objOrderParams[16].Value = objOrder.HoldOrCancel;

                            objOrderParams[17] = new SqlParameter("@DespatchByDate", SqlDbType.Date);
                            objOrderParams[17].Value = objOrder.DespatchByDate;

                            objOrderParams[18] = new SqlParameter("@Location", SqlDbType.UniqueIdentifier);
                            objOrderParams[18].Value = objOrder.Location;

                            objOrderParams[19] = new SqlParameter("@NumItems", SqlDbType.Bit);
                            objOrderParams[19].Value = objOrder.NumItems;

                            objOrderParams[20] = new SqlParameter("@Subtotal", SqlDbType.Money);
                            objOrderParams[20].Value = objOrder.Subtotal;

                            objOrderParams[21] = new SqlParameter("@PostageCost", SqlDbType.Money);
                            objOrderParams[21].Value = objOrder.PostageCost;

                            objOrderParams[22] = new SqlParameter("@Tax", SqlDbType.Decimal);
                            objOrderParams[22].Value = objOrder.Tax;

                            objOrderParams[23] = new SqlParameter("@TotalCharge", SqlDbType.Money);
                            objOrderParams[23].Value = objOrder.TotalCharge;

                            objOrderParams[24] = new SqlParameter("@PaymentMethod", SqlDbType.NVarChar, 100);
                            objOrderParams[24].Value = objOrder.PaymentMethod;

                            objOrderParams[25] = new SqlParameter("@PaymentMethodId", SqlDbType.UniqueIdentifier);
                            objOrderParams[25].Value = objOrder.PaymentMethodId;

                            objOrderParams[26] = new SqlParameter("@ProfitMargin", SqlDbType.Money);
                            objOrderParams[26].Value = objOrder.ProfitMargin;

                            objOrderParams[27] = new SqlParameter("@TotalDiscount", SqlDbType.Decimal);
                            objOrderParams[27].Value = objOrder.TotalDiscount;

                            objOrderParams[28] = new SqlParameter("@Currency", SqlDbType.NVarChar, 100);
                            objOrderParams[28].Value = objOrder.Currency;

                            objOrderParams[29] = new SqlParameter("@CountryTaxRate", SqlDbType.Decimal);
                            objOrderParams[29].Value = objOrder.CountryTaxRate;

                            objOrderParams[30] = new SqlParameter("@CustomerId", SqlDbType.UniqueIdentifier);
                            objOrderParams[30].Value = objOrder.CustomerId;

                            objOrderParams[31] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                            objOrderParams[31].Value = "N";

                            objOrderParams[32] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                            objOrderParams[32].Direction = ParameterDirection.Output;

                            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "OrderAddUpdateDelete", objOrderParams);
                            string strOrderResult = objOrderParams[32].Value.ToString();
                            #endregion

                            #region "Insert data in Address Table"

                            //Loopig because Address is containing two types of adress 1.Simple addres 2.Billing Address
                            for (int i = 0; i < 2; i++)
                            {
                                Address objAddress = new Address();
                                if (i == 0)
                                {
                                    objAddress = objCustomerAddress;
                                    objAddress.IsBilling = false;
                                    objAddress.IsShipping = true;
                                    objAddress.AddressId = AddressID;

                                }
                                else
                                {
                                    objAddress = objBillingAddress;
                                    objAddress.IsBilling = true;
                                    objAddress.IsShipping = false;
                                    objAddress.AddressId = BillingAddressID;
                                }
                                //Guid AddressID = Guid.NewGuid();

                                //Code to insert data in Country Table 
                                //Note:-Insert code of country must be placed before Address becasue Address Table have Fk reference of Country Table
                                //I am not sure we have to add this code here or not but for now added this in loop becasue we have two country id one under Address and second under BillingAddress node
                                SqlParameter[] objCountryParams = new SqlParameter[5];
                                objCountryParams[0] = new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier);
                                objCountryParams[0].Value = objAddress.CountryId;

                                objCountryParams[1] = new SqlParameter("@CountryName", SqlDbType.NVarChar, 200);
                                objCountryParams[1].Value = objAddress.Country;

                                objCountryParams[2] = new SqlParameter("@CountryCode", SqlDbType.NVarChar, 100);
                                objCountryParams[2].Value = "test country Code";

                                objCountryParams[3] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                                objCountryParams[3].Value = "N";

                                objCountryParams[4] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                                objCountryParams[4].Direction = ParameterDirection.Output;

                                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "CountryAddUpdateDelete", objCountryParams);
                                string strCountryResult = Convert.ToString(objCountryParams[4].Value);
                                //End of Code to insert data in Country Table

                                SqlParameter[] objAddressParams = new SqlParameter[19];

                                objAddressParams[0] = new SqlParameter("@AddressId", SqlDbType.UniqueIdentifier);
                                objAddressParams[0].Value = AddressID;

                                objAddressParams[1] = new SqlParameter("@IsBilling", SqlDbType.Bit);
                                objAddressParams[1].Value = objAddress.IsBilling;

                                objAddressParams[2] = new SqlParameter("@IsShipping", SqlDbType.Bit);
                                objAddressParams[2].Value = objAddress.IsShipping;

                                objAddressParams[3] = new SqlParameter("@EmailAddress", SqlDbType.NVarChar, 200);
                                objAddressParams[3].Value = objAddress.EmailAddress == null ? "" : objAddress.EmailAddress;

                                objAddressParams[4] = new SqlParameter("@Address1", SqlDbType.NVarChar, 200);
                                objAddressParams[4].Value = objAddress.Address1;

                                objAddressParams[5] = new SqlParameter("@Address2", SqlDbType.NVarChar, 200);
                                objAddressParams[5].Value = objAddress.Address2;

                                objAddressParams[6] = new SqlParameter("@Address3", SqlDbType.NVarChar, 200);
                                objAddressParams[6].Value = objAddress.Address3;

                                objAddressParams[7] = new SqlParameter("@Town", SqlDbType.NVarChar, 200);
                                objAddressParams[7].Value = objAddress.Town;

                                objAddressParams[8] = new SqlParameter("@Region", SqlDbType.NVarChar, 200);
                                objAddressParams[8].Value = objAddress.Region;

                                objAddressParams[9] = new SqlParameter("@PostCode", SqlDbType.NVarChar, 200);
                                objAddressParams[9].Value = objAddress.PostCode;

                                objAddressParams[10] = new SqlParameter("@Country", SqlDbType.NVarChar, 200);
                                objAddressParams[10].Value = objAddress.Country;

                                objAddressParams[11] = new SqlParameter("@FullName", SqlDbType.NVarChar, 200);
                                objAddressParams[11].Value = objAddress.FullName;

                                objAddressParams[12] = new SqlParameter("@Company", SqlDbType.NVarChar, 200);
                                objAddressParams[12].Value = objAddress.Company;

                                objAddressParams[13] = new SqlParameter("@PhoneNumber", SqlDbType.NVarChar, 200);
                                objAddressParams[13].Value = objAddress.PhoneNumber;

                                objAddressParams[14] = new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier);
                                objAddressParams[14].Value = objAddress.CountryId;

                                objAddressParams[15] = new SqlParameter("@CustomerId", SqlDbType.UniqueIdentifier);
                                objAddressParams[15].Value = objAddress.CustomerId;

                                objAddressParams[16] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                                objAddressParams[16].Value = objOrder.Orderid;

                                objAddressParams[17] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                                objAddressParams[17].Value = "N";

                                objAddressParams[18] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                                objAddressParams[18].Direction = ParameterDirection.Output;

                                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddressAddUpdateDelete", objAddressParams);
                                string strAddressResult = objAddressParams[18].Value.ToString();
                            }
                            #endregion

                            #region "Insert data in OrderItem Table"
                            SqlParameter[] objOrderIteamsParams = new SqlParameter[43];

                            objOrderIteamsParams[0] = new SqlParameter("@OrderItemId", SqlDbType.UniqueIdentifier);
                            objOrderIteamsParams[0].Value = objOrderIteam.OrderItemId;

                            objOrderIteamsParams[1] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                            objOrderIteamsParams[1].Value = objOrder.Orderid;

                            objOrderIteamsParams[2] = new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier);
                            objOrderIteamsParams[2].Value = objOrderIteam.ItemId;

                            objOrderIteamsParams[3] = new SqlParameter("@StockItemId", SqlDbType.UniqueIdentifier);
                            objOrderIteamsParams[3].Value = objOrderIteam.StockItemId;

                            objOrderIteamsParams[4] = new SqlParameter("@SKU", SqlDbType.NChar, 100);
                            objOrderIteamsParams[4].Value = objOrderIteam.SKU;

                            objOrderIteamsParams[5] = new SqlParameter("@ItemSource", SqlDbType.NChar, 200);
                            objOrderIteamsParams[5].Value = objOrderIteam.ItemSource;

                            objOrderIteamsParams[6] = new SqlParameter("@Title", SqlDbType.NChar, 200);
                            objOrderIteamsParams[6].Value = objOrderIteam.Title;

                            objOrderIteamsParams[7] = new SqlParameter("@Quantity", SqlDbType.Decimal);
                            objOrderIteamsParams[7].Value = objOrderIteam.Quantity;

                            objOrderIteamsParams[8] = new SqlParameter("@CategoryName", SqlDbType.NChar, 100);
                            objOrderIteamsParams[8].Value = objOrderIteam.CategoryName;

                            objOrderIteamsParams[9] = new SqlParameter("@CompositeAvailablity", SqlDbType.NChar, 100);
                            objOrderIteamsParams[9].Value = objOrderIteam.CompositeAvailablity;

                            objOrderIteamsParams[10] = new SqlParameter("@RowId", SqlDbType.UniqueIdentifier);
                            objOrderIteamsParams[10].Value = objOrderIteam.RowId;

                            objOrderIteamsParams[11] = new SqlParameter("@StockLevelsSpecified", SqlDbType.NVarChar, 100);
                            objOrderIteamsParams[11].Value = objOrderIteam.StockLevelsSpecified;

                            objOrderIteamsParams[12] = new SqlParameter("@OnOrder", SqlDbType.BigInt);
                            objOrderIteamsParams[12].Value = objOrderIteam.OnOrder;

                            objOrderIteamsParams[13] = new SqlParameter("@InOrderBook", SqlDbType.BigInt);
                            objOrderIteamsParams[13].Value = objOrderIteam.InOrderBook;

                            objOrderIteamsParams[14] = new SqlParameter("@Level", SqlDbType.Bit);
                            objOrderIteamsParams[14].Value = objOrderIteam.Level;

                            objOrderIteamsParams[15] = new SqlParameter("@MinimumLevel", SqlDbType.BigInt);
                            objOrderIteamsParams[15].Value = objOrderIteam.MinimumLevel;

                            objOrderIteamsParams[16] = new SqlParameter("@AvailableStock", SqlDbType.Decimal);
                            objOrderIteamsParams[16].Value = objOrderIteam.AvailableStock;

                            objOrderIteamsParams[17] = new SqlParameter("@PricePerUnit", SqlDbType.Money);
                            objOrderIteamsParams[17].Value = objOrderIteam.PricePerUnit;

                            objOrderIteamsParams[18] = new SqlParameter("@UnitCost", SqlDbType.Money);
                            objOrderIteamsParams[18].Value = objOrderIteam.UnitCost;

                            objOrderIteamsParams[19] = new SqlParameter("@Discount", SqlDbType.Decimal);
                            objOrderIteamsParams[19].Value = objOrderIteam.Discount;

                            objOrderIteamsParams[20] = new SqlParameter("@Tax", SqlDbType.Decimal);
                            objOrderIteamsParams[20].Value = objOrderIteam.Tax;

                            objOrderIteamsParams[21] = new SqlParameter("@TaxRate", SqlDbType.Decimal);
                            objOrderIteamsParams[21].Value = objOrderIteam.TaxRate;

                            objOrderIteamsParams[22] = new SqlParameter("@Cost", SqlDbType.Money);
                            objOrderIteamsParams[22].Value = objOrderIteam.Cost;

                            objOrderIteamsParams[23] = new SqlParameter("@CostIncTax", SqlDbType.Decimal);
                            objOrderIteamsParams[23].Value = objOrderIteam.CostIncTax;

                            objOrderIteamsParams[24] = new SqlParameter("@CompositeSubItems", SqlDbType.NChar, 100);
                            objOrderIteamsParams[24].Value = objOrderIteam.CompositeSubItems[0].TempProperty;

                            objOrderIteamsParams[25] = new SqlParameter("@IsService", SqlDbType.Bit);
                            objOrderIteamsParams[25].Value = objOrderIteam.IsService;

                            objOrderIteamsParams[26] = new SqlParameter("@SalesTax", SqlDbType.Decimal);
                            objOrderIteamsParams[26].Value = objOrderIteam.SalesTax;

                            objOrderIteamsParams[27] = new SqlParameter("@TaxCostInclusive", SqlDbType.Bit);
                            objOrderIteamsParams[27].Value = objOrderIteam.TaxCostInclusive;

                            objOrderIteamsParams[28] = new SqlParameter("@PartShipped", SqlDbType.Bit);
                            objOrderIteamsParams[28].Value = objOrderIteam.PartShipped;

                            objOrderIteamsParams[29] = new SqlParameter("@Weight", SqlDbType.Decimal);
                            objOrderIteamsParams[29].Value = objOrderIteam.Weight;

                            objOrderIteamsParams[30] = new SqlParameter("@BarcodeNumber", SqlDbType.NChar, 200);
                            objOrderIteamsParams[30].Value = objOrderIteam.BarcodeNumber;

                            objOrderIteamsParams[31] = new SqlParameter("@Market", SqlDbType.BigInt);
                            objOrderIteamsParams[31].Value = objOrderIteam.Market;

                            objOrderIteamsParams[32] = new SqlParameter("@ChannelSKU", SqlDbType.NChar, 10);
                            objOrderIteamsParams[32].Value = objOrderIteam.ChannelSKU;

                            objOrderIteamsParams[33] = new SqlParameter("@ChannelTitle", SqlDbType.NChar, 10);
                            objOrderIteamsParams[33].Value = objOrderIteam.ChannelTitle;

                            objOrderIteamsParams[34] = new SqlParameter("@HasImage", SqlDbType.Bit);
                            objOrderIteamsParams[34].Value = objOrderIteam.HasImage;

                            objOrderIteamsParams[35] = new SqlParameter("@ImageId", SqlDbType.UniqueIdentifier);
                            objOrderIteamsParams[35].Value = objOrderIteam.ImageId;

                            objOrderIteamsParams[36] = new SqlParameter("@AdditionalInfo", SqlDbType.NChar, 100);
                            objOrderIteamsParams[36].Value = objOrderIteam.AdditionalInfo[0].Value;

                            objOrderIteamsParams[37] = new SqlParameter("@StockLevelIndicator", SqlDbType.Decimal);
                            objOrderIteamsParams[37].Value = objOrderIteam.StockLevelIndicator;

                            objOrderIteamsParams[38] = new SqlParameter("@BinRack", SqlDbType.NChar, 100);
                            objOrderIteamsParams[38].Value = objOrderIteam.BinRack;

                            objOrderIteamsParams[39] = new SqlParameter("@CreatedBy", SqlDbType.UniqueIdentifier);
                            objOrderIteamsParams[39].Value = objOrderIteam.CreatedBy;

                            objOrderIteamsParams[40] = new SqlParameter("@ModifiedBy", SqlDbType.UniqueIdentifier);
                            objOrderIteamsParams[40].Value = objOrderIteam.ModifiedBy;

                            objOrderIteamsParams[41] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                            objOrderIteamsParams[41].Value = "N";

                            objOrderIteamsParams[42] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                            objOrderIteamsParams[42].Direction = ParameterDirection.Output;

                            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "OrderItemAddUpdateDelete", objOrderIteamsParams);
                            string strOrderIteamsResult = objOrderIteamsParams[42].Value.ToString();
                            #endregion


                            #region "Insert data in Shipping table"

                            SqlParameter[] objshippingParams = new SqlParameter[18];
                            objshippingParams[0] = new SqlParameter("@ShippingId", SqlDbType.UniqueIdentifier);
                            objshippingParams[0].Value = objShipping.ShippingId;

                            objshippingParams[1] = new SqlParameter("@Vendor", SqlDbType.NVarChar, 200);
                            objshippingParams[1].Value = objShipping.Vendor;

                            objshippingParams[2] = new SqlParameter("@PostalServiceId", SqlDbType.UniqueIdentifier);
                            objshippingParams[2].Value = objShipping.PostalServiceId;

                            objshippingParams[3] = new SqlParameter("@PostalServiceName", SqlDbType.NVarChar, 200);
                            objshippingParams[3].Value = objShipping.PostalServiceName;

                            objshippingParams[4] = new SqlParameter("@TotalWeight", SqlDbType.Decimal);
                            objshippingParams[4].Value = objShipping.TotalWeight;

                            objshippingParams[5] = new SqlParameter("@ItemWeight", SqlDbType.Decimal);
                            objshippingParams[5].Value = objShipping.ItemWeight;

                            objshippingParams[6] = new SqlParameter("@PackageCategoryId", SqlDbType.UniqueIdentifier);
                            objshippingParams[6].Value = objShipping.PackageCategoryId;

                            objshippingParams[7] = new SqlParameter("@PackageCategory", SqlDbType.NVarChar, 200);
                            objshippingParams[7].Value = objShipping.PackageCategory;

                            objshippingParams[8] = new SqlParameter("@PackageTypeId", SqlDbType.UniqueIdentifier);
                            objshippingParams[8].Value = objShipping.PackageTypeId;

                            objshippingParams[9] = new SqlParameter("@PackageType", SqlDbType.NVarChar, 100);
                            objshippingParams[9].Value = objShipping.PackageType;

                            objshippingParams[10] = new SqlParameter("@PostageCost", SqlDbType.Money);
                            objshippingParams[10].Value = objShipping.PostageCost;

                            objshippingParams[11] = new SqlParameter("@PostageCostExTax", SqlDbType.Decimal);
                            objshippingParams[11].Value = objShipping.PostageCost;

                            objshippingParams[12] = new SqlParameter("@TrackingNumber", SqlDbType.NVarChar, 200);
                            objshippingParams[12].Value = objShipping.TrackingNumber;

                            objshippingParams[13] = new SqlParameter("@ManualAdjust", SqlDbType.Bit);
                            objshippingParams[13].Value = objShipping.ManualAdjust;

                            objshippingParams[14] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                            objshippingParams[14].Value = objShipping.OrderId;

                            objshippingParams[15] = new SqlParameter("@AddressId", SqlDbType.UniqueIdentifier);
                            objshippingParams[15].Value = objShipping.AddressId;

                            objshippingParams[16] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                            objshippingParams[16].Value = "N";

                            objshippingParams[17] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                            objshippingParams[17].Direction = ParameterDirection.Output;
                            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "ShippingAddUpdateDelete", objshippingParams);
                            string str = objshippingParams[17].Value.ToString();
                            #endregion
                        }

                        catch (Exception ex)
                        {
                            string strError = ex.Message;
                        }
                        finally
                        {
                            connection.Close();
                            connection.Dispose();
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GetOrderData objOrderData = new GetOrderData();
            objOrderData.Show();
        }
    }
}
