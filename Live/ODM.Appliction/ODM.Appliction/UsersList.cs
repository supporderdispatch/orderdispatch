﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class UsersList : Form
    {
        public UsersList()
        {
            InitializeComponent();
            BindUsersGrid();
        }
        private void BindUsersGrid()
        {
            try
            {
                DataTable objDt = new DataTable();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlParameter[] objUsers = new SqlParameter[2];
                Guid dummyId = new Guid();
                objUsers[0] = new SqlParameter("@UserID", SqlDbType.UniqueIdentifier);
                objUsers[0].Value = dummyId;

                objUsers[1] = new SqlParameter("@Action", SqlDbType.VarChar, 2);
                objUsers[1].Value = "";

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetUsersList", objUsers).Tables[0];
                if (objDt != null)
                {
                    grdUserList.DataSource = objDt;
                    grdUserList.AllowUserToAddRows = false;
                    for (int i = 0; i < objDt.Columns.Count; i++)
                    {
                        grdUserList.Columns[i].ReadOnly = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
        }
        private string GetSelectedUserId()
        {
            string strUserID = "";
            if (grdUserList.CurrentCell == null)
            {
                MessageBox.Show("Please select a user");
            }
            else
            {
                int RowIndex = 0;
                RowIndex = Convert.ToInt32(grdUserList.CurrentCell.RowIndex);
                strUserID = grdUserList.Rows[RowIndex].Cells[0].Value.ToString();
            }
            return strUserID;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnEditUser_Click(object sender, EventArgs e)
        {
            string strUserID = GetSelectedUserId();
            Form frmEditUser = Application.OpenForms["EditUser"];
            if (frmEditUser != null)
            {
                frmEditUser.Close();
            }
            EditUser EditUser = new EditUser(strUserID);
            EditUser.Show();
        }
    }
}
