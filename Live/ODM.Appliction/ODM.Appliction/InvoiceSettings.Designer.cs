﻿namespace ODM.Appliction
{
    partial class InvoiceSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InvoiceSettings));
            this.btnAddInvoiceTemplate = new System.Windows.Forms.Button();
            this.grdInvoiceTemplates = new System.Windows.Forms.DataGridView();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdInvoiceTemplates)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddInvoiceTemplate
            // 
            this.btnAddInvoiceTemplate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddInvoiceTemplate.Location = new System.Drawing.Point(21, 35);
            this.btnAddInvoiceTemplate.Name = "btnAddInvoiceTemplate";
            this.btnAddInvoiceTemplate.Size = new System.Drawing.Size(150, 23);
            this.btnAddInvoiceTemplate.TabIndex = 0;
            this.btnAddInvoiceTemplate.Text = "Add Invoice Template";
            this.btnAddInvoiceTemplate.UseVisualStyleBackColor = true;
            this.btnAddInvoiceTemplate.Click += new System.EventHandler(this.btnAddInvoiceTemplate_Click);
            // 
            // grdInvoiceTemplates
            // 
            this.grdInvoiceTemplates.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdInvoiceTemplates.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grdInvoiceTemplates.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdInvoiceTemplates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdInvoiceTemplates.Location = new System.Drawing.Point(12, 87);
            this.grdInvoiceTemplates.Name = "grdInvoiceTemplates";
            this.grdInvoiceTemplates.Size = new System.Drawing.Size(878, 316);
            this.grdInvoiceTemplates.TabIndex = 1;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Location = new System.Drawing.Point(587, 35);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(150, 23);
            this.btnRefresh.TabIndex = 2;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveSettings.Location = new System.Drawing.Point(743, 35);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(150, 23);
            this.btnSaveSettings.TabIndex = 4;
            this.btnSaveSettings.Text = "Save Settings";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // InvoiceSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 407);
            this.Controls.Add(this.btnSaveSettings);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.grdInvoiceTemplates);
            this.Controls.Add(this.btnAddInvoiceTemplate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InvoiceSettings";
            this.Text = "InvoiceSettings";
            ((System.ComponentModel.ISupportInitialize)(this.grdInvoiceTemplates)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddInvoiceTemplate;
        private System.Windows.Forms.DataGridView grdInvoiceTemplates;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnSaveSettings;
    }
}