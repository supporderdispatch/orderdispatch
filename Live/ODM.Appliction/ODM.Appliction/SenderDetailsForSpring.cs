﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class SenderDetailsForSpring : Form
    {
        #region "Global Variables"
        string _CourierName = "";
        string _CourierID = "";
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        DataTable ObjdtCourier = new DataTable();
        #endregion

        #region "Constructors"
        public SenderDetailsForSpring()
        {
            InitializeComponent();
        }
        public SenderDetailsForSpring(string strCourierName, string strCourierID)
        {
            InitializeComponent();
            _CourierName = strCourierName;
            _CourierID = strCourierID;
            PopulateControls();
        }
        #endregion

        #region "Events"
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveSenderDetailsForSpring();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region "Methods"
        private void PopulateControls()
        {
            try
            {
                
                ObjdtCourier = GlobalVar.GetCourierDetailsByNameOrId(_CourierName, _CourierID);
                if (ObjdtCourier.Rows.Count > 0)
                {
                    BindCountryDDL();

                    txtFirstName.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderFirstName"]);
                    txtLastName.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderLastName"]);
                    txtPhone.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderPhoneNumber"]);
                    txtStreet1.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderStreet1"]);
                    txtStreet2.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderStreet2"]);
                    txtStreetNumber.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderStreetNumber"]);
                    txtStreeNumberSuffix.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderStreetNumberSuffix"]);
                    txtTown.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderRegion"]);
                    txtCity.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderCity"]);
                    ddlCountry.SelectedValue = Convert.ToString(ObjdtCourier.Rows[0]["SenderCountry"]);                    
                    txtPostCode.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderPostalCode"]);
                    txtCompany.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderCompany"]);
                    txtEmail.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderEmail"]);



                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void BindCountryDDL()
        {
            try
            {
                ddlCountry.ValueMember = "CountryId";
                ddlCountry.DisplayMember = "CountryName";
                ddlCountry.DataSource = GlobalVar.GetAllCountries();              
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool ValidateControls()
        {
            bool IsValid = true;
            if (string.IsNullOrWhiteSpace(txtFirstName.Text))
            {
                lblFirstNameRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblFirstNameRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPhone.Text))
            {
                lblPhoneRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPhoneRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtStreet1.Text))
            {
                lblAddress1Required.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAddress1Required.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtStreet2.Text))
            {
                lblAddress2Required.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAddress2Required.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtTown.Text))
            {
                lblTownRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblTownRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPostCode.Text))
            {
                lblPostCodeRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPostCodeRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtCompany.Text))
            {
                lblCompanyRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblCompanyRequired.Visible = false;
            }
            return IsValid;
        }
        private void SaveSenderDetailsForSpring()
        {
            try
            {
                SqlConnection connection = new SqlConnection();
                if (ValidateControls())
                {
                    Guid CountryID = new Guid(ddlCountry.SelectedValue.ToString());
                    connection.ConnectionString = MakeConnection.GetConnection();
                    connection.Open();

                    SqlParameter[] objParam = new SqlParameter[13];

                    objParam[0] = new SqlParameter("@SenderFirstName", SqlDbType.VarChar, 200);
                    objParam[0].Value = txtFirstName.Text;

                    objParam[1] = new SqlParameter("@SenderLastName", SqlDbType.VarChar, 200);
                    objParam[1].Value = txtLastName.Text;

                    objParam[2] = new SqlParameter("@SenderPhoneNumber", SqlDbType.VarChar, 100);
                    objParam[2].Value = txtPhone.Text;

                    objParam[3] = new SqlParameter("@SenderStreet1", SqlDbType.VarChar, 200);
                    objParam[3].Value = txtStreet1.Text;

                    objParam[4] = new SqlParameter("@SenderStreet2", SqlDbType.VarChar, 200);
                    objParam[4].Value = txtStreet2.Text;

                    objParam[5] = new SqlParameter("@SenderStreetNumber", SqlDbType.VarChar, 100);
                    objParam[5].Value = txtStreetNumber.Text;

                    objParam[6] = new SqlParameter("SenderStreetNumberSuffix", SqlDbType.VarChar, 100);
                    objParam[6].Value = txtStreeNumberSuffix.Text;

                    objParam[7] = new SqlParameter("@SenderPostalCode", SqlDbType.VarChar, 50);
                    objParam[7].Value = txtPostCode.Text;

                    objParam[8] = new SqlParameter("@SenderRegion", SqlDbType.VarChar, 100);
                    objParam[8].Value = txtTown.Text;

                    objParam[9] = new SqlParameter("@SenderCompany", SqlDbType.VarChar, 100);
                    objParam[9].Value = txtCompany.Text;

                    objParam[10] = new SqlParameter("@SenderCountry", SqlDbType.UniqueIdentifier);
                    objParam[10].Value = CountryID;

                    objParam[11] = new SqlParameter("@SenderCity", SqlDbType.VarChar,100);
                    objParam[11].Value = txtCity.Text;

                    objParam[12] = new SqlParameter("@SenderEmail", SqlDbType.VarChar,100);
                    objParam[12].Value = txtEmail.Text;

                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateSenderDetailsForSpring_New", objParam);
                    this.Close();
                    MessageBox.Show("Sender Details Saved Sucessfully");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


    }
}
