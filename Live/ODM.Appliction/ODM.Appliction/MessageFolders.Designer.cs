﻿namespace ODM.Appliction
{
    partial class MessageFolders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageFolders));
            this.btnAddFolder = new System.Windows.Forms.Button();
            this.txtFolderName = new System.Windows.Forms.TextBox();
            this.lblError = new System.Windows.Forms.Label();
            this.tlPanelMessageFolder = new System.Windows.Forms.TableLayoutPanel();
            this.grdMessageList = new System.Windows.Forms.DataGridView();
            this.grdMessageFolders = new System.Windows.Forms.DataGridView();
            this.lblNoRecordError = new System.Windows.Forms.Label();
            this.btnTemplateSettings = new System.Windows.Forms.Button();
            this.btnRuleList = new System.Windows.Forms.Button();
            this.btnAccountSettings = new System.Windows.Forms.Button();
            this.btnRefreshMessages = new System.Windows.Forms.Button();
            this.lblFolderName = new System.Windows.Forms.Label();
            this.lblFolderNameValue = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.chkShowReadMessages = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlPaging = new System.Windows.Forms.Panel();
            this.lblReadMessagesColorText = new System.Windows.Forms.Label();
            this.lblUnreadMessagesColorText = new System.Windows.Forms.Label();
            this.lblRepliedMessagesColorText = new System.Windows.Forms.Label();
            this.btnReadMessagesColor = new System.Windows.Forms.Button();
            this.btnUnReadMessagesColor = new System.Windows.Forms.Button();
            this.btnRepliedMessagesColor = new System.Windows.Forms.Button();
            this.btnSearchMessages = new System.Windows.Forms.Button();
            this.btnDeleteFolder = new System.Windows.Forms.Button();
            this.tlPanelMessageFolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMessageList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdMessageFolders)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddFolder
            // 
            this.btnAddFolder.AutoEllipsis = true;
            this.btnAddFolder.Location = new System.Drawing.Point(208, 111);
            this.btnAddFolder.Name = "btnAddFolder";
            this.btnAddFolder.Size = new System.Drawing.Size(97, 23);
            this.btnAddFolder.TabIndex = 3;
            this.btnAddFolder.Text = "Add New Folder";
            this.btnAddFolder.UseVisualStyleBackColor = true;
            this.btnAddFolder.Click += new System.EventHandler(this.btnAddFolder_Click);
            // 
            // txtFolderName
            // 
            this.txtFolderName.Location = new System.Drawing.Point(30, 113);
            this.txtFolderName.Name = "txtFolderName";
            this.txtFolderName.Size = new System.Drawing.Size(172, 20);
            this.txtFolderName.TabIndex = 4;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(129, 97);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(50, 13);
            this.lblError.TabIndex = 5;
            this.lblError.Text = "Required";
            this.lblError.Visible = false;
            // 
            // tlPanelMessageFolder
            // 
            this.tlPanelMessageFolder.ColumnCount = 2;
            this.tlPanelMessageFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.8226F));
            this.tlPanelMessageFolder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.17741F));
            this.tlPanelMessageFolder.Controls.Add(this.grdMessageList, 0, 0);
            this.tlPanelMessageFolder.Controls.Add(this.grdMessageFolders, 0, 0);
            this.tlPanelMessageFolder.Location = new System.Drawing.Point(2, 141);
            this.tlPanelMessageFolder.Name = "tlPanelMessageFolder";
            this.tlPanelMessageFolder.RowCount = 1;
            this.tlPanelMessageFolder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlPanelMessageFolder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 514F));
            this.tlPanelMessageFolder.Size = new System.Drawing.Size(1130, 514);
            this.tlPanelMessageFolder.TabIndex = 1;
            // 
            // grdMessageList
            // 
            this.grdMessageList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMessageList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdMessageList.Location = new System.Drawing.Point(351, 3);
            this.grdMessageList.Name = "grdMessageList";
            this.grdMessageList.Size = new System.Drawing.Size(776, 508);
            this.grdMessageList.TabIndex = 7;
            this.grdMessageList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMessageList_CellDoubleClick);
            this.grdMessageList.Sorted += new System.EventHandler(this.grdMessageList_Sorted);
            this.grdMessageList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.grdMessageList_MouseClick);
            // 
            // grdMessageFolders
            // 
            this.grdMessageFolders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMessageFolders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdMessageFolders.Location = new System.Drawing.Point(3, 3);
            this.grdMessageFolders.Name = "grdMessageFolders";
            this.grdMessageFolders.Size = new System.Drawing.Size(342, 508);
            this.grdMessageFolders.TabIndex = 0;
            this.grdMessageFolders.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMessageFolders_CellDoubleClick);
            // 
            // lblNoRecordError
            // 
            this.lblNoRecordError.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNoRecordError.AutoSize = true;
            this.lblNoRecordError.Location = new System.Drawing.Point(663, 342);
            this.lblNoRecordError.Name = "lblNoRecordError";
            this.lblNoRecordError.Size = new System.Drawing.Size(92, 13);
            this.lblNoRecordError.TabIndex = 9;
            this.lblNoRecordError.Text = "No Record Found";
            this.lblNoRecordError.Visible = false;
            // 
            // btnTemplateSettings
            // 
            this.btnTemplateSettings.Location = new System.Drawing.Point(878, 6);
            this.btnTemplateSettings.Name = "btnTemplateSettings";
            this.btnTemplateSettings.Size = new System.Drawing.Size(107, 23);
            this.btnTemplateSettings.TabIndex = 10;
            this.btnTemplateSettings.Text = "Template Settings";
            this.btnTemplateSettings.UseVisualStyleBackColor = true;
            this.btnTemplateSettings.Click += new System.EventHandler(this.btnTemplateSettings_Click);
            // 
            // btnRuleList
            // 
            this.btnRuleList.Location = new System.Drawing.Point(769, 6);
            this.btnRuleList.Name = "btnRuleList";
            this.btnRuleList.Size = new System.Drawing.Size(103, 23);
            this.btnRuleList.TabIndex = 11;
            this.btnRuleList.Text = "Rule List";
            this.btnRuleList.UseVisualStyleBackColor = true;
            this.btnRuleList.Click += new System.EventHandler(this.btnRuleList_Click);
            // 
            // btnAccountSettings
            // 
            this.btnAccountSettings.Location = new System.Drawing.Point(654, 6);
            this.btnAccountSettings.Name = "btnAccountSettings";
            this.btnAccountSettings.Size = new System.Drawing.Size(109, 23);
            this.btnAccountSettings.TabIndex = 12;
            this.btnAccountSettings.Text = "Account Settings";
            this.btnAccountSettings.UseVisualStyleBackColor = true;
            this.btnAccountSettings.Click += new System.EventHandler(this.btnAccountSettings_Click);
            // 
            // btnRefreshMessages
            // 
            this.btnRefreshMessages.Location = new System.Drawing.Point(537, 6);
            this.btnRefreshMessages.Name = "btnRefreshMessages";
            this.btnRefreshMessages.Size = new System.Drawing.Size(111, 23);
            this.btnRefreshMessages.TabIndex = 13;
            this.btnRefreshMessages.Text = "Refresh Messages";
            this.btnRefreshMessages.UseVisualStyleBackColor = true;
            this.btnRefreshMessages.Click += new System.EventHandler(this.btnRefreshMessages_Click);
            // 
            // lblFolderName
            // 
            this.lblFolderName.AutoSize = true;
            this.lblFolderName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFolderName.Location = new System.Drawing.Point(205, 9);
            this.lblFolderName.Name = "lblFolderName";
            this.lblFolderName.Size = new System.Drawing.Size(82, 13);
            this.lblFolderName.TabIndex = 14;
            this.lblFolderName.Text = "FolderName :";
            // 
            // lblFolderNameValue
            // 
            this.lblFolderNameValue.AutoSize = true;
            this.lblFolderNameValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFolderNameValue.Location = new System.Drawing.Point(293, 9);
            this.lblFolderNameValue.Name = "lblFolderNameValue";
            this.lblFolderNameValue.Size = new System.Drawing.Size(0, 13);
            this.lblFolderNameValue.TabIndex = 14;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(449, 107);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 13);
            this.lblStatus.TabIndex = 16;
            this.lblStatus.Visible = false;
            // 
            // btnFirst
            // 
            this.btnFirst.Location = new System.Drawing.Point(369, 102);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(30, 23);
            this.btnFirst.TabIndex = 18;
            this.btnFirst.Text = "< |";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Visible = false;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(404, 102);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(30, 23);
            this.btnPrevious.TabIndex = 18;
            this.btnPrevious.Text = "<";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Visible = false;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(501, 102);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(30, 23);
            this.btnNext.TabIndex = 18;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnLast
            // 
            this.btnLast.Location = new System.Drawing.Point(537, 102);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(30, 23);
            this.btnLast.TabIndex = 18;
            this.btnLast.Text = "| >";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Visible = false;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // chkShowReadMessages
            // 
            this.chkShowReadMessages.AutoSize = true;
            this.chkShowReadMessages.Location = new System.Drawing.Point(604, 105);
            this.chkShowReadMessages.Name = "chkShowReadMessages";
            this.chkShowReadMessages.Size = new System.Drawing.Size(133, 17);
            this.chkShowReadMessages.TabIndex = 19;
            this.chkShowReadMessages.Text = "Show Read Messages";
            this.chkShowReadMessages.UseVisualStyleBackColor = true;
            this.chkShowReadMessages.Visible = false;
            this.chkShowReadMessages.CheckedChanged += new System.EventHandler(this.chkShowReadMessages_CheckedChanged);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(991, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 20;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pnlPaging
            // 
            this.pnlPaging.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPaging.Location = new System.Drawing.Point(353, 80);
            this.pnlPaging.Name = "pnlPaging";
            this.pnlPaging.Size = new System.Drawing.Size(391, 54);
            this.pnlPaging.TabIndex = 25;
            this.pnlPaging.Visible = false;
            // 
            // lblReadMessagesColorText
            // 
            this.lblReadMessagesColorText.AutoSize = true;
            this.lblReadMessagesColorText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReadMessagesColorText.Location = new System.Drawing.Point(402, 50);
            this.lblReadMessagesColorText.Name = "lblReadMessagesColorText";
            this.lblReadMessagesColorText.Size = new System.Drawing.Size(105, 13);
            this.lblReadMessagesColorText.TabIndex = 26;
            this.lblReadMessagesColorText.Text = "Read Messages :";
            // 
            // lblUnreadMessagesColorText
            // 
            this.lblUnreadMessagesColorText.AutoSize = true;
            this.lblUnreadMessagesColorText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnreadMessagesColorText.Location = new System.Drawing.Point(617, 51);
            this.lblUnreadMessagesColorText.Name = "lblUnreadMessagesColorText";
            this.lblUnreadMessagesColorText.Size = new System.Drawing.Size(121, 13);
            this.lblUnreadMessagesColorText.TabIndex = 26;
            this.lblUnreadMessagesColorText.Text = "UnRead Messages :";
            // 
            // lblRepliedMessagesColorText
            // 
            this.lblRepliedMessagesColorText.AutoSize = true;
            this.lblRepliedMessagesColorText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRepliedMessagesColorText.Location = new System.Drawing.Point(811, 51);
            this.lblRepliedMessagesColorText.Name = "lblRepliedMessagesColorText";
            this.lblRepliedMessagesColorText.Size = new System.Drawing.Size(195, 13);
            this.lblRepliedMessagesColorText.TabIndex = 26;
            this.lblRepliedMessagesColorText.Text = "Messages Whose Resply is Sent:";
            // 
            // btnReadMessagesColor
            // 
            this.btnReadMessagesColor.BackColor = System.Drawing.Color.Green;
            this.btnReadMessagesColor.Location = new System.Drawing.Point(507, 48);
            this.btnReadMessagesColor.Name = "btnReadMessagesColor";
            this.btnReadMessagesColor.Size = new System.Drawing.Size(30, 19);
            this.btnReadMessagesColor.TabIndex = 27;
            this.btnReadMessagesColor.UseVisualStyleBackColor = false;
            // 
            // btnUnReadMessagesColor
            // 
            this.btnUnReadMessagesColor.BackColor = System.Drawing.Color.Red;
            this.btnUnReadMessagesColor.Location = new System.Drawing.Point(737, 49);
            this.btnUnReadMessagesColor.Name = "btnUnReadMessagesColor";
            this.btnUnReadMessagesColor.Size = new System.Drawing.Size(30, 19);
            this.btnUnReadMessagesColor.TabIndex = 27;
            this.btnUnReadMessagesColor.UseVisualStyleBackColor = false;
            // 
            // btnRepliedMessagesColor
            // 
            this.btnRepliedMessagesColor.BackColor = System.Drawing.Color.Gray;
            this.btnRepliedMessagesColor.Location = new System.Drawing.Point(1010, 48);
            this.btnRepliedMessagesColor.Name = "btnRepliedMessagesColor";
            this.btnRepliedMessagesColor.Size = new System.Drawing.Size(30, 19);
            this.btnRepliedMessagesColor.TabIndex = 27;
            this.btnRepliedMessagesColor.UseVisualStyleBackColor = false;
            // 
            // btnSearchMessages
            // 
            this.btnSearchMessages.Location = new System.Drawing.Point(869, 98);
            this.btnSearchMessages.Name = "btnSearchMessages";
            this.btnSearchMessages.Size = new System.Drawing.Size(100, 28);
            this.btnSearchMessages.TabIndex = 28;
            this.btnSearchMessages.Text = "Search Messages";
            this.btnSearchMessages.UseVisualStyleBackColor = true;
            this.btnSearchMessages.Click += new System.EventHandler(this.btnSearchMessages_Click);
            // 
            // btnDeleteFolder
            // 
            this.btnDeleteFolder.Location = new System.Drawing.Point(30, 51);
            this.btnDeleteFolder.Name = "btnDeleteFolder";
            this.btnDeleteFolder.Size = new System.Drawing.Size(91, 23);
            this.btnDeleteFolder.TabIndex = 29;
            this.btnDeleteFolder.Text = "Delete Folder";
            this.btnDeleteFolder.UseVisualStyleBackColor = true;
            this.btnDeleteFolder.Click += new System.EventHandler(this.btnDeleteFolder_Click);
            // 
            // MessageFolders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1133, 655);
            this.Controls.Add(this.btnDeleteFolder);
            this.Controls.Add(this.btnSearchMessages);
            this.Controls.Add(this.btnRepliedMessagesColor);
            this.Controls.Add(this.btnUnReadMessagesColor);
            this.Controls.Add(this.btnReadMessagesColor);
            this.Controls.Add(this.lblRepliedMessagesColorText);
            this.Controls.Add(this.lblUnreadMessagesColorText);
            this.Controls.Add(this.lblReadMessagesColorText);
            this.Controls.Add(this.chkShowReadMessages);
            this.Controls.Add(this.btnLast);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnFirst);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblFolderNameValue);
            this.Controls.Add(this.lblFolderName);
            this.Controls.Add(this.lblNoRecordError);
            this.Controls.Add(this.btnRefreshMessages);
            this.Controls.Add(this.btnAccountSettings);
            this.Controls.Add(this.btnRuleList);
            this.Controls.Add(this.btnTemplateSettings);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.txtFolderName);
            this.Controls.Add(this.btnAddFolder);
            this.Controls.Add(this.tlPanelMessageFolder);
            this.Controls.Add(this.pnlPaging);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MessageFolders";
            this.Text = "Message Folders";
            this.Load += new System.EventHandler(this.MessageFolders_Load);
            this.tlPanelMessageFolder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdMessageList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdMessageFolders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddFolder;
        private System.Windows.Forms.TextBox txtFolderName;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.TableLayoutPanel tlPanelMessageFolder;
        private System.Windows.Forms.DataGridView grdMessageList;
        private System.Windows.Forms.DataGridView grdMessageFolders;
        private System.Windows.Forms.Label lblNoRecordError;
        private System.Windows.Forms.Button btnTemplateSettings;
        private System.Windows.Forms.Button btnRuleList;
        private System.Windows.Forms.Button btnAccountSettings;
        private System.Windows.Forms.Button btnRefreshMessages;
        private System.Windows.Forms.Label lblFolderName;
        private System.Windows.Forms.Label lblFolderNameValue;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.CheckBox chkShowReadMessages;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel pnlPaging;
        private System.Windows.Forms.Label lblReadMessagesColorText;
        private System.Windows.Forms.Label lblUnreadMessagesColorText;
        private System.Windows.Forms.Label lblRepliedMessagesColorText;
        private System.Windows.Forms.Button btnReadMessagesColor;
        private System.Windows.Forms.Button btnUnReadMessagesColor;
        private System.Windows.Forms.Button btnRepliedMessagesColor;
        private System.Windows.Forms.Button btnSearchMessages;
        private System.Windows.Forms.Button btnDeleteFolder;
    }
}