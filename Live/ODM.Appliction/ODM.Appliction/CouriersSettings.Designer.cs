﻿namespace ODM.Appliction
{
    partial class CouriersSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CouriersSettings));
            this.grdCouriers = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdCouriers)).BeginInit();
            this.SuspendLayout();
            // 
            // grdCouriers
            // 
            this.grdCouriers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCouriers.Location = new System.Drawing.Point(2, 55);
            this.grdCouriers.Name = "grdCouriers";
            this.grdCouriers.Size = new System.Drawing.Size(465, 327);
            this.grdCouriers.TabIndex = 0;
            this.grdCouriers.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCouriers_CellClick);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(374, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(93, 36);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // CouriersSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 383);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grdCouriers);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CouriersSettings";
            this.Text = "Courier Settings";
            ((System.ComponentModel.ISupportInitialize)(this.grdCouriers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grdCouriers;
        private System.Windows.Forms.Button btnClose;
    }
}