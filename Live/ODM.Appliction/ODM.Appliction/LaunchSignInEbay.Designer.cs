﻿namespace ODM.Appliction
{
    partial class LaunchSignInEbay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LaunchSignInEbay));
            this.ddlEbayAcounts = new System.Windows.Forms.ComboBox();
            this.lblEbayAccount = new System.Windows.Forms.Label();
            this.btnEbayUserSignIn = new System.Windows.Forms.Button();
            this.btnFecthToken = new System.Windows.Forms.Button();
            this.chkLogIn = new System.Windows.Forms.CheckBox();
            this.lblEbayUserName = new System.Windows.Forms.Label();
            this.txtEbayUserName = new System.Windows.Forms.TextBox();
            this.ddlAppKeys = new System.Windows.Forms.ComboBox();
            this.lblEbayAppKeysName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ddlEbayAcounts
            // 
            this.ddlEbayAcounts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEbayAcounts.FormattingEnabled = true;
            this.ddlEbayAcounts.Location = new System.Drawing.Point(168, 34);
            this.ddlEbayAcounts.Name = "ddlEbayAcounts";
            this.ddlEbayAcounts.Size = new System.Drawing.Size(189, 21);
            this.ddlEbayAcounts.TabIndex = 0;
            this.ddlEbayAcounts.Visible = false;
            // 
            // lblEbayAccount
            // 
            this.lblEbayAccount.AutoSize = true;
            this.lblEbayAccount.Location = new System.Drawing.Point(82, 37);
            this.lblEbayAccount.Name = "lblEbayAccount";
            this.lblEbayAccount.Size = new System.Drawing.Size(80, 13);
            this.lblEbayAccount.TabIndex = 1;
            this.lblEbayAccount.Text = "Ebay Account :";
            this.lblEbayAccount.Visible = false;
            // 
            // btnEbayUserSignIn
            // 
            this.btnEbayUserSignIn.Location = new System.Drawing.Point(400, 114);
            this.btnEbayUserSignIn.Name = "btnEbayUserSignIn";
            this.btnEbayUserSignIn.Size = new System.Drawing.Size(131, 23);
            this.btnEbayUserSignIn.TabIndex = 2;
            this.btnEbayUserSignIn.Text = "Launch Sign In";
            this.btnEbayUserSignIn.UseVisualStyleBackColor = true;
            this.btnEbayUserSignIn.Click += new System.EventHandler(this.btnEbayUserSignIn_Click);
            // 
            // btnFecthToken
            // 
            this.btnFecthToken.Location = new System.Drawing.Point(172, 206);
            this.btnFecthToken.Name = "btnFecthToken";
            this.btnFecthToken.Size = new System.Drawing.Size(131, 23);
            this.btnFecthToken.TabIndex = 4;
            this.btnFecthToken.Text = "Fetch and Save Token";
            this.btnFecthToken.UseVisualStyleBackColor = true;
            this.btnFecthToken.Click += new System.EventHandler(this.btnFetchandSaveToken_Click);
            // 
            // chkLogIn
            // 
            this.chkLogIn.AutoSize = true;
            this.chkLogIn.Location = new System.Drawing.Point(171, 166);
            this.chkLogIn.Name = "chkLogIn";
            this.chkLogIn.Size = new System.Drawing.Size(326, 17);
            this.chkLogIn.TabIndex = 3;
            this.chkLogIn.Text = "I have logged into the account by clicking on the Sign In button";
            this.chkLogIn.UseVisualStyleBackColor = true;
            // 
            // lblEbayUserName
            // 
            this.lblEbayUserName.AutoSize = true;
            this.lblEbayUserName.Location = new System.Drawing.Point(72, 116);
            this.lblEbayUserName.Name = "lblEbayUserName";
            this.lblEbayUserName.Size = new System.Drawing.Size(90, 13);
            this.lblEbayUserName.TabIndex = 5;
            this.lblEbayUserName.Text = "Ebay UserName :";
            // 
            // txtEbayUserName
            // 
            this.txtEbayUserName.Location = new System.Drawing.Point(168, 116);
            this.txtEbayUserName.Name = "txtEbayUserName";
            this.txtEbayUserName.Size = new System.Drawing.Size(189, 20);
            this.txtEbayUserName.TabIndex = 6;
            // 
            // ddlAppKeys
            // 
            this.ddlAppKeys.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAppKeys.FormattingEnabled = true;
            this.ddlAppKeys.Location = new System.Drawing.Point(168, 76);
            this.ddlAppKeys.Name = "ddlAppKeys";
            this.ddlAppKeys.Size = new System.Drawing.Size(189, 21);
            this.ddlAppKeys.TabIndex = 0;
            // 
            // lblEbayAppKeysName
            // 
            this.lblEbayAppKeysName.AutoSize = true;
            this.lblEbayAppKeysName.Location = new System.Drawing.Point(73, 79);
            this.lblEbayAppKeysName.Name = "lblEbayAppKeysName";
            this.lblEbayAppKeysName.Size = new System.Drawing.Size(89, 13);
            this.lblEbayAppKeysName.TabIndex = 1;
            this.lblEbayAppKeysName.Text = "App Keys Name :";
            // 
            // LaunchSignInEbay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 275);
            this.Controls.Add(this.txtEbayUserName);
            this.Controls.Add(this.lblEbayUserName);
            this.Controls.Add(this.btnFecthToken);
            this.Controls.Add(this.chkLogIn);
            this.Controls.Add(this.btnEbayUserSignIn);
            this.Controls.Add(this.lblEbayAppKeysName);
            this.Controls.Add(this.lblEbayAccount);
            this.Controls.Add(this.ddlAppKeys);
            this.Controls.Add(this.ddlEbayAcounts);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LaunchSignInEbay";
            this.Text = "LaunchSignInEbay";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlEbayAcounts;
        private System.Windows.Forms.Label lblEbayAccount;
        private System.Windows.Forms.Button btnEbayUserSignIn;
        private System.Windows.Forms.Button btnFecthToken;
        private System.Windows.Forms.CheckBox chkLogIn;
        private System.Windows.Forms.Label lblEbayUserName;
        private System.Windows.Forms.TextBox txtEbayUserName;
        private System.Windows.Forms.ComboBox ddlAppKeys;
        private System.Windows.Forms.Label lblEbayAppKeysName;
    }
}