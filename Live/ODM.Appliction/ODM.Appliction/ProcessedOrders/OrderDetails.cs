﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction.ProcessedOrders
{
    public partial class OrderDetails : Form
    {
        private Guid OrderId;

        public OrderDetails(Guid orderId,string orderNumber)
        {
            InitializeComponent();
            this.OrderId = orderId;
            this.Text = orderNumber;
            RegisterEvents();
            LoadOrderItems();
        }

        private void LoadOrderItems()
        {
            var orderItems = Activator.CreateInstance<ODM.Core.Orders.OrderDetails>().GetOrderItems(OrderId);
            if (orderItems.Rows.Count > 0)
            {
                grdOrderItems.DataSource = orderItems;
                ApplyItemGridFormatting();
            }
        }

        private void ApplyItemGridFormatting()
        {
            grdOrderItems.DoubleBuffered(true);
            grdOrderItems.Columns["ItemTitle"].MinimumWidth = 250;
            grdOrderItems.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdOrderItems.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.Single;
            grdOrderItems.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdOrderItems.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdOrderItems.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdOrderItems.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdOrderItems.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdOrderItems.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdOrderItems.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdOrderItems.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdOrderItems.ScrollBars = ScrollBars.Both;
            grdOrderItems.RowHeadersVisible = false;
            grdOrderItems.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdOrderItems.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdOrderItems.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdOrderItems.EnableHeadersVisualStyles = false;
            grdOrderItems.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdOrderItems.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdOrderItems.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdOrderItems.DefaultCellStyle.Padding = new Padding(2, 2, 2, 2);
            grdOrderItems.Columns["title"].Visible = false;
        }

        private void RegisterEvents()
        {
            tabOrderDetails.DrawItem += tabOrderDetails_DrawItem;
        }

        void tabOrderDetails_DrawItem(object sender, DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush;

            // Get the item from the collection.
            TabPage _tabPage = tabOrderDetails.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = tabOrderDetails.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {

                // Draw a different background color, and don't paint a focus rectangle.
                _textBrush = new SolidBrush(Color.Black);
                g.FillRectangle(Brushes.WhiteSmoke, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                e.DrawBackground();
            }

            // Use our own font.
            Font _tabFont = tabOrderDetails.Font;

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
        }
    }
}
