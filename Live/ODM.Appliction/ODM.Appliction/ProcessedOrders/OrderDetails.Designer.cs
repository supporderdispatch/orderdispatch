﻿namespace ODM.Appliction.ProcessedOrders
{
    partial class OrderDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderDetails));
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.grdOrderItems = new System.Windows.Forms.DataGridView();
            this.tabOrderDetails = new System.Windows.Forms.TabControl();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderItems)).BeginInit();
            this.tabOrderDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage1.Controls.Add(this.grdOrderItems);
            this.tabPage1.Location = new System.Drawing.Point(124, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(743, 445);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Order Items";
            // 
            // grdOrderItems
            // 
            this.grdOrderItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdOrderItems.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grdOrderItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdOrderItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOrderItems.Location = new System.Drawing.Point(6, 6);
            this.grdOrderItems.Name = "grdOrderItems";
            this.grdOrderItems.Size = new System.Drawing.Size(731, 432);
            this.grdOrderItems.TabIndex = 0;
            // 
            // tabOrderDetails
            // 
            this.tabOrderDetails.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabOrderDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabOrderDetails.Controls.Add(this.tabPage1);
            this.tabOrderDetails.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabOrderDetails.ItemSize = new System.Drawing.Size(70, 120);
            this.tabOrderDetails.Location = new System.Drawing.Point(3, 22);
            this.tabOrderDetails.Multiline = true;
            this.tabOrderDetails.Name = "tabOrderDetails";
            this.tabOrderDetails.SelectedIndex = 0;
            this.tabOrderDetails.Size = new System.Drawing.Size(871, 453);
            this.tabOrderDetails.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabOrderDetails.TabIndex = 0;
            // 
            // OrderDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 476);
            this.Controls.Add(this.tabOrderDetails);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrderDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OrderDetails";
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderItems)).EndInit();
            this.tabOrderDetails.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabOrderDetails;
        private System.Windows.Forms.DataGridView grdOrderItems;


    }
}