﻿namespace ODM.Appliction
{
    partial class SpringCourierSettings_New
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpringCourierSettings_New));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSaveClose = new System.Windows.Forms.Button();
            this.grdMappedServices = new System.Windows.Forms.DataGridView();
            this.chkIsBoseLogin = new System.Windows.Forms.CheckBox();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblTestSecretRequired = new System.Windows.Forms.Label();
            this.lblTestClientIDRequired = new System.Windows.Forms.Label();
            this.lblLiveSecretRequired = new System.Windows.Forms.Label();
            this.lblLiveClientIDRequired = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpBoxShippingSettings = new System.Windows.Forms.GroupBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.ddlPrinter = new System.Windows.Forms.ComboBox();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.chkIsLive = new System.Windows.Forms.CheckBox();
            this.txtTestSecret = new System.Windows.Forms.TextBox();
            this.lblTestSecret = new System.Windows.Forms.Label();
            this.txtTestClientID = new System.Windows.Forms.TextBox();
            this.lblTestClientID = new System.Windows.Forms.Label();
            this.txtLiveSecret = new System.Windows.Forms.TextBox();
            this.lblLiveSecret = new System.Windows.Forms.Label();
            this.txtLiveClientID = new System.Windows.Forms.TextBox();
            this.lblLiveClientID = new System.Windows.Forms.Label();
            this.lblShopID = new System.Windows.Forms.Label();
            this.txtShopID = new System.Windows.Forms.TextBox();
            this.lblShopIDRequired = new System.Windows.Forms.Label();
            this.lblContractID = new System.Windows.Forms.Label();
            this.txtContractID = new System.Windows.Forms.TextBox();
            this.lblContractIDRequired = new System.Windows.Forms.Label();
            this.btnSenderDetails = new System.Windows.Forms.Button();
            this.lblTestContractID = new System.Windows.Forms.Label();
            this.lblTestShopID = new System.Windows.Forms.Label();
            this.txtTestShopID = new System.Windows.Forms.TextBox();
            this.txtTestContractID = new System.Windows.Forms.TextBox();
            this.lblTestShopIDRequired = new System.Windows.Forms.Label();
            this.lblWeightInvalid = new System.Windows.Forms.Label();
            this.lblWeightRequired = new System.Windows.Forms.Label();
            this.lblTestContractIDRequired = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).BeginInit();
            this.grpBoxShippingSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(563, 192);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(180, 29);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSaveClose
            // 
            this.btnSaveClose.Location = new System.Drawing.Point(378, 194);
            this.btnSaveClose.Name = "btnSaveClose";
            this.btnSaveClose.Size = new System.Drawing.Size(180, 29);
            this.btnSaveClose.TabIndex = 13;
            this.btnSaveClose.Text = "Save && Close";
            this.btnSaveClose.UseVisualStyleBackColor = true;
            this.btnSaveClose.Click += new System.EventHandler(this.btnSaveClose_Click);
            // 
            // grdMappedServices
            // 
            this.grdMappedServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMappedServices.Location = new System.Drawing.Point(-26, 19);
            this.grdMappedServices.Name = "grdMappedServices";
            this.grdMappedServices.Size = new System.Drawing.Size(781, 150);
            this.grdMappedServices.TabIndex = 12;
            this.grdMappedServices.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMappedServices_CellContentClick);
            // 
            // chkIsBoseLogin
            // 
            this.chkIsBoseLogin.AutoSize = true;
            this.chkIsBoseLogin.Location = new System.Drawing.Point(114, 191);
            this.chkIsBoseLogin.Name = "chkIsBoseLogin";
            this.chkIsBoseLogin.Size = new System.Drawing.Size(90, 17);
            this.chkIsBoseLogin.TabIndex = 66;
            this.chkIsBoseLogin.Text = "Is Bose Login";
            this.chkIsBoseLogin.UseVisualStyleBackColor = true;
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(512, 142);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(191, 20);
            this.txtWeight.TabIndex = 62;
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Location = new System.Drawing.Point(393, 149);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(78, 13);
            this.lblWeight.TabIndex = 63;
            this.lblWeight.Text = "Default Weight";
            // 
            // lblTestSecretRequired
            // 
            this.lblTestSecretRequired.AutoSize = true;
            this.lblTestSecretRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTestSecretRequired.Location = new System.Drawing.Point(709, 51);
            this.lblTestSecretRequired.Name = "lblTestSecretRequired";
            this.lblTestSecretRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTestSecretRequired.TabIndex = 60;
            this.lblTestSecretRequired.Text = "Required";
            this.lblTestSecretRequired.Visible = false;
            // 
            // lblTestClientIDRequired
            // 
            this.lblTestClientIDRequired.AutoSize = true;
            this.lblTestClientIDRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTestClientIDRequired.Location = new System.Drawing.Point(709, 18);
            this.lblTestClientIDRequired.Name = "lblTestClientIDRequired";
            this.lblTestClientIDRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTestClientIDRequired.TabIndex = 59;
            this.lblTestClientIDRequired.Text = "Required";
            this.lblTestClientIDRequired.Visible = false;
            // 
            // lblLiveSecretRequired
            // 
            this.lblLiveSecretRequired.AutoSize = true;
            this.lblLiveSecretRequired.ForeColor = System.Drawing.Color.Red;
            this.lblLiveSecretRequired.Location = new System.Drawing.Point(311, 45);
            this.lblLiveSecretRequired.Name = "lblLiveSecretRequired";
            this.lblLiveSecretRequired.Size = new System.Drawing.Size(50, 13);
            this.lblLiveSecretRequired.TabIndex = 57;
            this.lblLiveSecretRequired.Text = "Required";
            this.lblLiveSecretRequired.Visible = false;
            // 
            // lblLiveClientIDRequired
            // 
            this.lblLiveClientIDRequired.AutoSize = true;
            this.lblLiveClientIDRequired.ForeColor = System.Drawing.Color.Red;
            this.lblLiveClientIDRequired.Location = new System.Drawing.Point(311, 18);
            this.lblLiveClientIDRequired.Name = "lblLiveClientIDRequired";
            this.lblLiveClientIDRequired.Size = new System.Drawing.Size(50, 13);
            this.lblLiveClientIDRequired.TabIndex = 56;
            this.lblLiveClientIDRequired.Text = "Required";
            this.lblLiveClientIDRequired.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(600, 237);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(164, 29);
            this.btnDelete.TabIndex = 54;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(414, 237);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(180, 29);
            this.btnAdd.TabIndex = 53;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grpBoxShippingSettings
            // 
            this.grpBoxShippingSettings.Controls.Add(this.lblMessage);
            this.grpBoxShippingSettings.Controls.Add(this.btnCancel);
            this.grpBoxShippingSettings.Controls.Add(this.btnSaveClose);
            this.grpBoxShippingSettings.Controls.Add(this.grdMappedServices);
            this.grpBoxShippingSettings.Location = new System.Drawing.Point(10, 272);
            this.grpBoxShippingSettings.Name = "grpBoxShippingSettings";
            this.grpBoxShippingSettings.Size = new System.Drawing.Size(787, 229);
            this.grpBoxShippingSettings.TabIndex = 55;
            this.grpBoxShippingSettings.TabStop = false;
            this.grpBoxShippingSettings.Text = "Shipping Mapping";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(313, 75);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(92, 13);
            this.lblMessage.TabIndex = 15;
            this.lblMessage.Text = "No Record Found";
            // 
            // ddlPrinter
            // 
            this.ddlPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPrinter.FormattingEnabled = true;
            this.ddlPrinter.Location = new System.Drawing.Point(512, 172);
            this.ddlPrinter.Name = "ddlPrinter";
            this.ddlPrinter.Size = new System.Drawing.Size(191, 21);
            this.ddlPrinter.TabIndex = 52;
            // 
            // lblPrinter
            // 
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Location = new System.Drawing.Point(393, 180);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(70, 13);
            this.lblPrinter.TabIndex = 51;
            this.lblPrinter.Text = "Select Printer";
            // 
            // chkIsLive
            // 
            this.chkIsLive.AutoSize = true;
            this.chkIsLive.Location = new System.Drawing.Point(114, 167);
            this.chkIsLive.Name = "chkIsLive";
            this.chkIsLive.Size = new System.Drawing.Size(57, 17);
            this.chkIsLive.TabIndex = 50;
            this.chkIsLive.Text = "Is Live";
            this.chkIsLive.UseVisualStyleBackColor = true;
            // 
            // txtTestSecret
            // 
            this.txtTestSecret.Location = new System.Drawing.Point(512, 49);
            this.txtTestSecret.Name = "txtTestSecret";
            this.txtTestSecret.Size = new System.Drawing.Size(191, 20);
            this.txtTestSecret.TabIndex = 47;
            // 
            // lblTestSecret
            // 
            this.lblTestSecret.AutoSize = true;
            this.lblTestSecret.Location = new System.Drawing.Point(393, 49);
            this.lblTestSecret.Name = "lblTestSecret";
            this.lblTestSecret.Size = new System.Drawing.Size(62, 13);
            this.lblTestSecret.TabIndex = 46;
            this.lblTestSecret.Text = "Test Secret";
            // 
            // txtTestClientID
            // 
            this.txtTestClientID.Location = new System.Drawing.Point(512, 15);
            this.txtTestClientID.Name = "txtTestClientID";
            this.txtTestClientID.Size = new System.Drawing.Size(191, 20);
            this.txtTestClientID.TabIndex = 45;
            // 
            // lblTestClientID
            // 
            this.lblTestClientID.AutoSize = true;
            this.lblTestClientID.Location = new System.Drawing.Point(393, 18);
            this.lblTestClientID.Name = "lblTestClientID";
            this.lblTestClientID.Size = new System.Drawing.Size(71, 13);
            this.lblTestClientID.TabIndex = 44;
            this.lblTestClientID.Text = "Test Client ID";
            // 
            // txtLiveSecret
            // 
            this.txtLiveSecret.Location = new System.Drawing.Point(114, 46);
            this.txtLiveSecret.Name = "txtLiveSecret";
            this.txtLiveSecret.Size = new System.Drawing.Size(191, 20);
            this.txtLiveSecret.TabIndex = 41;
            // 
            // lblLiveSecret
            // 
            this.lblLiveSecret.AutoSize = true;
            this.lblLiveSecret.Location = new System.Drawing.Point(10, 49);
            this.lblLiveSecret.Name = "lblLiveSecret";
            this.lblLiveSecret.Size = new System.Drawing.Size(61, 13);
            this.lblLiveSecret.TabIndex = 40;
            this.lblLiveSecret.Text = "Live Secret";
            // 
            // txtLiveClientID
            // 
            this.txtLiveClientID.Location = new System.Drawing.Point(114, 18);
            this.txtLiveClientID.Name = "txtLiveClientID";
            this.txtLiveClientID.Size = new System.Drawing.Size(191, 20);
            this.txtLiveClientID.TabIndex = 39;
            // 
            // lblLiveClientID
            // 
            this.lblLiveClientID.AutoSize = true;
            this.lblLiveClientID.Location = new System.Drawing.Point(11, 18);
            this.lblLiveClientID.Name = "lblLiveClientID";
            this.lblLiveClientID.Size = new System.Drawing.Size(70, 13);
            this.lblLiveClientID.TabIndex = 38;
            this.lblLiveClientID.Text = "Live Client ID";
            // 
            // lblShopID
            // 
            this.lblShopID.AutoSize = true;
            this.lblShopID.Location = new System.Drawing.Point(10, 78);
            this.lblShopID.Name = "lblShopID";
            this.lblShopID.Size = new System.Drawing.Size(46, 13);
            this.lblShopID.TabIndex = 40;
            this.lblShopID.Text = "Shop ID";
            // 
            // txtShopID
            // 
            this.txtShopID.Location = new System.Drawing.Point(114, 75);
            this.txtShopID.Name = "txtShopID";
            this.txtShopID.Size = new System.Drawing.Size(191, 20);
            this.txtShopID.TabIndex = 41;
            // 
            // lblShopIDRequired
            // 
            this.lblShopIDRequired.AutoSize = true;
            this.lblShopIDRequired.ForeColor = System.Drawing.Color.Red;
            this.lblShopIDRequired.Location = new System.Drawing.Point(311, 74);
            this.lblShopIDRequired.Name = "lblShopIDRequired";
            this.lblShopIDRequired.Size = new System.Drawing.Size(50, 13);
            this.lblShopIDRequired.TabIndex = 57;
            this.lblShopIDRequired.Text = "Required";
            this.lblShopIDRequired.Visible = false;
            // 
            // lblContractID
            // 
            this.lblContractID.AutoSize = true;
            this.lblContractID.Location = new System.Drawing.Point(10, 109);
            this.lblContractID.Name = "lblContractID";
            this.lblContractID.Size = new System.Drawing.Size(61, 13);
            this.lblContractID.TabIndex = 40;
            this.lblContractID.Text = "Contract ID";
            // 
            // txtContractID
            // 
            this.txtContractID.Location = new System.Drawing.Point(114, 106);
            this.txtContractID.Name = "txtContractID";
            this.txtContractID.Size = new System.Drawing.Size(191, 20);
            this.txtContractID.TabIndex = 41;
            // 
            // lblContractIDRequired
            // 
            this.lblContractIDRequired.AutoSize = true;
            this.lblContractIDRequired.ForeColor = System.Drawing.Color.Red;
            this.lblContractIDRequired.Location = new System.Drawing.Point(311, 105);
            this.lblContractIDRequired.Name = "lblContractIDRequired";
            this.lblContractIDRequired.Size = new System.Drawing.Size(50, 13);
            this.lblContractIDRequired.TabIndex = 57;
            this.lblContractIDRequired.Text = "Required";
            this.lblContractIDRequired.Visible = false;
            // 
            // btnSenderDetails
            // 
            this.btnSenderDetails.Location = new System.Drawing.Point(227, 237);
            this.btnSenderDetails.Name = "btnSenderDetails";
            this.btnSenderDetails.Size = new System.Drawing.Size(180, 29);
            this.btnSenderDetails.TabIndex = 53;
            this.btnSenderDetails.Text = "Sender Details";
            this.btnSenderDetails.UseVisualStyleBackColor = true;
            this.btnSenderDetails.Click += new System.EventHandler(this.btnSenderDetails_Click);
            // 
            // lblTestContractID
            // 
            this.lblTestContractID.AutoSize = true;
            this.lblTestContractID.Location = new System.Drawing.Point(393, 112);
            this.lblTestContractID.Name = "lblTestContractID";
            this.lblTestContractID.Size = new System.Drawing.Size(85, 13);
            this.lblTestContractID.TabIndex = 67;
            this.lblTestContractID.Text = "Test Contract ID";
            // 
            // lblTestShopID
            // 
            this.lblTestShopID.AutoSize = true;
            this.lblTestShopID.Location = new System.Drawing.Point(394, 82);
            this.lblTestShopID.Name = "lblTestShopID";
            this.lblTestShopID.Size = new System.Drawing.Size(70, 13);
            this.lblTestShopID.TabIndex = 68;
            this.lblTestShopID.Text = "Test Shop ID";
            // 
            // txtTestShopID
            // 
            this.txtTestShopID.Location = new System.Drawing.Point(512, 79);
            this.txtTestShopID.Name = "txtTestShopID";
            this.txtTestShopID.Size = new System.Drawing.Size(191, 20);
            this.txtTestShopID.TabIndex = 69;
            // 
            // txtTestContractID
            // 
            this.txtTestContractID.Location = new System.Drawing.Point(512, 109);
            this.txtTestContractID.Name = "txtTestContractID";
            this.txtTestContractID.Size = new System.Drawing.Size(191, 20);
            this.txtTestContractID.TabIndex = 70;
            // 
            // lblTestShopIDRequired
            // 
            this.lblTestShopIDRequired.AutoSize = true;
            this.lblTestShopIDRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTestShopIDRequired.Location = new System.Drawing.Point(709, 82);
            this.lblTestShopIDRequired.Name = "lblTestShopIDRequired";
            this.lblTestShopIDRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTestShopIDRequired.TabIndex = 71;
            this.lblTestShopIDRequired.Text = "Required";
            this.lblTestShopIDRequired.Visible = false;
            // 
            // lblWeightInvalid
            // 
            this.lblWeightInvalid.AutoSize = true;
            this.lblWeightInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblWeightInvalid.Location = new System.Drawing.Point(709, 145);
            this.lblWeightInvalid.Name = "lblWeightInvalid";
            this.lblWeightInvalid.Size = new System.Drawing.Size(65, 13);
            this.lblWeightInvalid.TabIndex = 73;
            this.lblWeightInvalid.Text = "Invalid Input";
            this.lblWeightInvalid.Visible = false;
            // 
            // lblWeightRequired
            // 
            this.lblWeightRequired.AutoSize = true;
            this.lblWeightRequired.ForeColor = System.Drawing.Color.Red;
            this.lblWeightRequired.Location = new System.Drawing.Point(709, 145);
            this.lblWeightRequired.Name = "lblWeightRequired";
            this.lblWeightRequired.Size = new System.Drawing.Size(50, 13);
            this.lblWeightRequired.TabIndex = 74;
            this.lblWeightRequired.Text = "Required";
            this.lblWeightRequired.Visible = false;
            // 
            // lblTestContractIDRequired
            // 
            this.lblTestContractIDRequired.AutoSize = true;
            this.lblTestContractIDRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTestContractIDRequired.Location = new System.Drawing.Point(709, 112);
            this.lblTestContractIDRequired.Name = "lblTestContractIDRequired";
            this.lblTestContractIDRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTestContractIDRequired.TabIndex = 75;
            this.lblTestContractIDRequired.Text = "Required";
            this.lblTestContractIDRequired.Visible = false;
            // 
            // SpringCourierSettings_New
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 498);
            this.Controls.Add(this.lblTestContractIDRequired);
            this.Controls.Add(this.lblWeightRequired);
            this.Controls.Add(this.lblWeightInvalid);
            this.Controls.Add(this.lblTestShopIDRequired);
            this.Controls.Add(this.txtTestContractID);
            this.Controls.Add(this.txtTestShopID);
            this.Controls.Add(this.lblTestShopID);
            this.Controls.Add(this.lblTestContractID);
            this.Controls.Add(this.chkIsBoseLogin);
            this.Controls.Add(this.txtWeight);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.lblTestSecretRequired);
            this.Controls.Add(this.lblTestClientIDRequired);
            this.Controls.Add(this.lblContractIDRequired);
            this.Controls.Add(this.lblShopIDRequired);
            this.Controls.Add(this.lblLiveSecretRequired);
            this.Controls.Add(this.lblLiveClientIDRequired);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSenderDetails);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpBoxShippingSettings);
            this.Controls.Add(this.ddlPrinter);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.chkIsLive);
            this.Controls.Add(this.txtTestSecret);
            this.Controls.Add(this.lblTestSecret);
            this.Controls.Add(this.txtTestClientID);
            this.Controls.Add(this.txtContractID);
            this.Controls.Add(this.lblTestClientID);
            this.Controls.Add(this.lblContractID);
            this.Controls.Add(this.txtShopID);
            this.Controls.Add(this.lblShopID);
            this.Controls.Add(this.txtLiveSecret);
            this.Controls.Add(this.lblLiveSecret);
            this.Controls.Add(this.txtLiveClientID);
            this.Controls.Add(this.lblLiveClientID);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SpringCourierSettings_New";
            this.Text = "SpringCourierSettings_New";
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).EndInit();
            this.grpBoxShippingSettings.ResumeLayout(false);
            this.grpBoxShippingSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSaveClose;
        private System.Windows.Forms.DataGridView grdMappedServices;
        private System.Windows.Forms.CheckBox chkIsBoseLogin;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblTestSecretRequired;
        private System.Windows.Forms.Label lblTestClientIDRequired;
        private System.Windows.Forms.Label lblLiveSecretRequired;
        private System.Windows.Forms.Label lblLiveClientIDRequired;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grpBoxShippingSettings;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.ComboBox ddlPrinter;
        private System.Windows.Forms.Label lblPrinter;
        private System.Windows.Forms.CheckBox chkIsLive;
        private System.Windows.Forms.TextBox txtTestSecret;
        private System.Windows.Forms.Label lblTestSecret;
        private System.Windows.Forms.TextBox txtTestClientID;
        private System.Windows.Forms.Label lblTestClientID;
        private System.Windows.Forms.TextBox txtLiveSecret;
        private System.Windows.Forms.Label lblLiveSecret;
        private System.Windows.Forms.TextBox txtLiveClientID;
        private System.Windows.Forms.Label lblLiveClientID;
        private System.Windows.Forms.Label lblShopID;
        private System.Windows.Forms.TextBox txtShopID;
        private System.Windows.Forms.Label lblShopIDRequired;
        private System.Windows.Forms.Label lblContractID;
        private System.Windows.Forms.TextBox txtContractID;
        private System.Windows.Forms.Label lblContractIDRequired;
        private System.Windows.Forms.Button btnSenderDetails;
        private System.Windows.Forms.Label lblTestContractID;
        private System.Windows.Forms.Label lblTestShopID;
        private System.Windows.Forms.TextBox txtTestShopID;
        private System.Windows.Forms.TextBox txtTestContractID;
        private System.Windows.Forms.Label lblTestShopIDRequired;
        private System.Windows.Forms.Label lblWeightInvalid;
        private System.Windows.Forms.Label lblWeightRequired;
        private System.Windows.Forms.Label lblTestContractIDRequired;
    }
}