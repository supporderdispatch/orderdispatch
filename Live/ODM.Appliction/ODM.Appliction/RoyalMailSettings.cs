﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class RoyalMailSettings : Form
    {
        #region "Global Variables"
        string _CourierName = "";
        string _CourierID = "";
        SqlConnection connection;
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        string strDefaultPrinter = "";
        #endregion

        #region "Constructors"
        public RoyalMailSettings()
        {
            InitializeComponent();
        }

        public RoyalMailSettings(string strCourierName, string strCourierID)
        {
            InitializeComponent();
            _CourierName = strCourierName;
            _CourierID = strCourierID;
            PopulateControls();
            BindGrid();
        }
        #endregion

        #region "Methods"
        private void PopulateControls()
        {
            try
            {
                DataTable ObjdtCourier = new DataTable();
                ObjdtCourier = GlobalVar.GetCourierDetailsByNameOrId(_CourierName, _CourierID);
                if (ObjdtCourier.Rows.Count > 0)
                {
                    txtClientID.Text = Convert.ToString(ObjdtCourier.Rows[0]["ClientID"]);
                    txtSecret.Text = Convert.ToString(ObjdtCourier.Rows[0]["Secret"]);
                    txtApplicationID.Text = Convert.ToString(ObjdtCourier.Rows[0]["ApplicationID"]);
                    txtUserName.Text = Convert.ToString(ObjdtCourier.Rows[0]["UserName"]);
                    txtPassword.Text = Convert.ToString(ObjdtCourier.Rows[0]["Password"]);
                    ddlPrinter.DataSource = GlobalVar.AttachedPrinters(ref strDefaultPrinter);
                    ddlManifestPrinter.DataSource = GlobalVar.AttachedPrinters(ref strDefaultPrinter);

                    ddlPrinter.SelectedItem = !String.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForRoyalMail) ?
                                              GlobalVariablesAndMethods.SelectedPrinterNameForRoyalMail : strDefaultPrinter;

                    ddlManifestPrinter.SelectedItem = !String.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForRoyalMailManifest) ?
                                                      GlobalVariablesAndMethods.SelectedPrinterNameForRoyalMailManifest : strDefaultPrinter;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                connection = new SqlConnection();
                DataTable objDtGridData = new DataTable();
                DataTable objCourierList = new DataTable();
                connection.ConnectionString = MakeConnection.GetConnection();
                objDtGridData = SqlHelper.ExecuteDataset(connection, "GetRoyalMailMappedCouriers").Tables[0];
                objCourierList = SqlHelper.ExecuteDataset(connection, "GetAllRoyalMailCourierToMapWithServices").Tables[0];

                if (objDtGridData.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(objDtGridData.Rows[0]["ID"])))
                {
                    grdMappedServices.Visible = true;
                    grdMappedServices.AllowUserToAddRows = false;
                    grdMappedServices.DataSource = objDtGridData;
                    grdMappedServices.AutoGenerateColumns = false;
                    grdMappedServices.Columns[0].Visible = false;
                    grdMappedServices.Columns[1].Visible = false;
                    grdMappedServices.Columns[2].Visible = false;
                    grdMappedServices.Columns[4].Visible = false;
                    grdMappedServices.Columns[3].ReadOnly = true;
                    grdMappedServices.Columns[3].Width = 370;

                    DataGridViewComboBoxColumn ddlCouriers = new DataGridViewComboBoxColumn();
                    ddlCouriers.HeaderText = "Courier Name";
                    ddlCouriers.Name = "Courier_Name";
                    ddlCouriers.ValueMember = "CourierID";
                    ddlCouriers.DataPropertyName = "MapedCourierId";
                    ddlCouriers.DisplayMember = "CourierName";
                    grdMappedServices.Columns.Add(ddlCouriers);
                    ddlCouriers.DataSource = objCourierList;
                    grdMappedServices.Columns[5].Width = 370;
                    lblMessage.Visible = false;
                }
                else
                {
                    grdMappedServices.Visible = false;
                    lblMessage.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An Error Occurred Contact the Administrator");
            }
            finally
            {
                connection.Close();
            }
        }

        private void SaveSettings()
        {
            connection = new SqlConnection();
            try
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                Guid CourierID = new Guid(_CourierID);
                SqlParameter[] objParams = new SqlParameter[7];

                objParams[0] = new SqlParameter("@CourierId", SqlDbType.UniqueIdentifier);
                objParams[0].Value = CourierID;

                objParams[1] = new SqlParameter("@CourierName", SqlDbType.VarChar, 500);
                objParams[1].Value = _CourierName;

                objParams[2] = new SqlParameter("@ClientID", SqlDbType.VarChar, 100);
                objParams[2].Value = txtClientID.Text;

                objParams[3] = new SqlParameter("@Secret", SqlDbType.VarChar, 100);
                objParams[3].Value = txtSecret.Text;

                objParams[4] = new SqlParameter("@UserName", SqlDbType.VarChar, 100);
                objParams[4].Value = txtUserName.Text;

                objParams[5] = new SqlParameter("@Password", SqlDbType.VarChar, 100);
                objParams[5].Value = txtPassword.Text;

                objParams[6] = new SqlParameter("@ApplicationID", SqlDbType.VarChar, 100);
                objParams[6].Value = txtApplicationID.Text;

                SqlHelper.ExecuteNonQuery(connection, "AddUpdateRoyalMail", objParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        private void MapCourierToService()
        {
            try
            {
                int intResult = 100;
                if (grdMappedServices.Rows.Count > 0)
                {
                    for (int i = 0; i < grdMappedServices.Rows.Count; i++)
                    {
                        string strSelectedCourierID = Convert.ToString(grdMappedServices.Rows[i].Cells["ID"].Value);
                        string strSelectedValue = Convert.ToString(grdMappedServices[5, i].Value);
                        string strServiceName = Convert.ToString(grdMappedServices.Rows[i].Cells["PostalServiceName"].Value);
                        if (strSelectedValue == "")
                        {
                            MessageBox.Show(string.Format("Please select courier for the service {0}", strServiceName));
                            intResult = -1;
                            break;
                        }
                        else
                        {
                            connection = new SqlConnection();
                            Guid MappedID = new Guid(strSelectedValue);
                            Guid ID = new Guid(strSelectedCourierID);
                            Guid DummyID = new Guid();
                            connection.ConnectionString = MakeConnection.GetConnection();
                            SqlParameter[] objParams = new SqlParameter[6];

                            objParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                            objParams[0].Value = ID;

                            objParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                            objParams[1].Value = DummyID;

                            objParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                            objParams[2].Value = DummyID;

                            objParams[3] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                            objParams[3].Value = MappedID;

                            objParams[4] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                            objParams[4].Value = "UPDATE";

                            objParams[5] = new SqlParameter("@Result", SqlDbType.Int);
                            objParams[5].Direction = ParameterDirection.Output;

                            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteServicesToMapRoyalMailCourier", objParams);

                            intResult = Convert.ToInt32(objParams[5].Value);
                        }
                    }
                    if (intResult == 1)
                    {
                        this.Close();
                        MessageBox.Show("Settings Saved Successfully !");
                    }
                }
                else
                {
                    this.Close();
                    MessageBox.Show("Settings Saved Successfully !");
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }

        private void DeleteService()
        {
            try
            {
                int RowIndex = 0;
                if (grdMappedServices.CurrentCell != null)
                {
                    RowIndex = Convert.ToInt32(grdMappedServices.CurrentCell.RowIndex);
                    string strSelectedCourierID = Convert.ToString(grdMappedServices.Rows[RowIndex].Cells[0].Value);
                    connection.ConnectionString = MakeConnection.GetConnection();
                    Guid ID = new Guid(strSelectedCourierID);
                    Guid DummyID = new Guid();

                    SqlParameter[] objParams = new SqlParameter[6];

                    objParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                    objParams[0].Value = ID;

                    objParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                    objParams[1].Value = DummyID;

                    objParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                    objParams[2].Value = DummyID;

                    objParams[3] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                    objParams[3].Value = DummyID;

                    objParams[4] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                    objParams[4].Value = "DELETE";

                    objParams[5] = new SqlParameter("@Result", SqlDbType.Int);
                    objParams[5].Direction = ParameterDirection.Output;

                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteServicesToMapRoyalMailCourier", objParams);

                    int intResult = Convert.ToInt32(objParams[5].Value);
                    if (intResult == 2)
                    {
                        BindGrid();
                        MessageBox.Show("Record deleted successfully !");
                    }
                }
                else
                {
                    MessageBox.Show("Please select a record to delete");
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }

        private bool ValidateControls()
        {
            bool IsValid = true;
            if (string.IsNullOrWhiteSpace(txtClientID.Text))
            {
                lblClientIDRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblClientIDRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtApplicationID.Text))
            {
                lblApplicationIDRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblApplicationIDRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtSecret.Text))
            {
                lblSecretRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblSecretRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtUserName.Text))
            {
                lblUserNameRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblUserNameRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPassword.Text))
            {
                lblPasswordRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPasswordRequired.Visible = false;
            }
            return IsValid;
        }
        #endregion

        #region "Events"
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Form objAddService = Application.OpenForms["AddServices"];
            if (objAddService == null)
            {
                AddServices objServices = new AddServices(_CourierName, _CourierID);
                objServices.Show();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure, you want to delete Postal Service ?", "Delete Postal Service", MessageBoxButtons.YesNo);
            if (result.Equals(DialogResult.Yes))
            {
                DeleteService();
            }
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                SaveSettings();
                MapCourierToService();
                GlobalVariablesAndMethods.SelectedPrinterNameForRoyalMail = ddlPrinter.SelectedItem.ToString();
                GlobalVariablesAndMethods.SelectedPrinterNameForRoyalMailManifest = ddlManifestPrinter.SelectedItem.ToString();
                GlobalVar.AddUpdatePrinterName();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Form frmAddService = Application.OpenForms["AddServices"];
            if (frmAddService != null)
            {
                frmAddService.Close();
            }
            this.Close();
        }

        private void btnManifest_Click(object sender, EventArgs e)
        {
            Form frmGetManifest = Application.OpenForms["Manifest"];
            if (frmGetManifest == null)
            {
                Manifest objManifest = new Manifest(_CourierName);
                objManifest.Show();
            }
        }

        private void btnSenderDetails_Click(object sender, EventArgs e)
        {
            Form frmSenderDetails = Application.OpenForms["SenderDetailsForRoyalMail"];
            if (frmSenderDetails == null)
            {
                SenderDetailsForRoyalMail objSenderDetails = new SenderDetailsForRoyalMail(_CourierName, _CourierID);
                objSenderDetails.Show();
            }
        }
        #endregion

    }
}
