﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.ShippingLabel;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class CancelFedExShipment : Form
    {
        DataTable objDtOrderDetails = new DataTable();
        DataTable objDtTrackingNumber = new DataTable();
        public CancelFedExShipment()
        {
            InitializeComponent();
            lblMessage.Visible = true;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            if (ValidateOrderID())
            {
                BindGrids();
            }
        }
        private void BindGrids()
        {
            try
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = MakeConnection.GetConnection();
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@OrderNumID", SqlDbType.VarChar, 20);
                objParam[0].Value = txtOrderID.Text;
                objDtOrderDetails = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "GetFedExOrdersToCancel", objParam).Tables[0];
                if (objDtOrderDetails.Rows.Count == 0)
                {
                    grdOrderDetails.Visible = false;
                    grdTrackingNumber.Visible = false;
                    lblMessage.Visible = true;
                }
                else
                {
                    grdOrderDetails.Visible = true;
                    grdTrackingNumber.Visible = true;
                    lblMessage.Visible = false;
                    DataView objDv = new DataView(objDtOrderDetails);
                    objDtTrackingNumber = objDv.ToTable(true, "TrackingNumber");

                    grdOrderDetails.AllowUserToAddRows = false;
                    grdOrderDetails.DataSource = objDtOrderDetails;
                    grdOrderDetails.AutoGenerateColumns = false;

                    for (int i = 0; i < objDtOrderDetails.Columns.Count; i++)
                    {
                        grdOrderDetails.Columns[i].ReadOnly = true;
                    }

                    grdTrackingNumber.AllowUserToAddRows = false;
                    grdTrackingNumber.DataSource = objDtTrackingNumber;
                    grdTrackingNumber.AutoGenerateColumns = false;
                    grdTrackingNumber.Columns[0].ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }
        private bool ValidateOrderID()
        {
            bool IsValidInput = true;
            if (string.IsNullOrEmpty(txtOrderID.Text))
            {
                lblOrderIdRequired.Visible = true;
                lblOrderIdRequired.Text = "Please Enter 6 Digits Order Id";
                IsValidInput = false;
            }
            else
            {
                if (Regex.IsMatch(txtOrderID.Text, "[^0-9]"))
                {
                    lblOrderIdRequired.Visible = true;
                    lblOrderIdRequired.Text = "Invalid Order Id";
                    IsValidInput = false;
                }
                else
                {
                    lblOrderIdRequired.Visible = false;
                }
            }
            return IsValidInput;
        }

        private void txtOrderID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (ValidateOrderID())
                {
                    BindGrids();
                }
            }
        }

        private void grdOrderDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string strSelectedRow = grdOrderDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            if (strSelectedRow == "Cancel")
            {
                Guid OrderID = new Guid(grdOrderDetails.Rows[e.RowIndex].Cells[0].Value.ToString());
                string strTrackingNumber = grdTrackingNumber.Rows[e.RowIndex].Cells[0].Value.ToString();
                FedExPrintLabel objFedEx = new FedExPrintLabel();
                string strResult = objFedEx.CancelShipment(OrderID, strTrackingNumber);
                if (strResult == "SUCCESS" || strResult == "WARNING")
                {
                    RemoveTrackingNumberInDatabase(OrderID.ToString(), "Void");
                    BindGrids();
                }
            }
        }

        private void RemoveTrackingNumberInDatabase(string strOrderIds, string IsManifestOrVoidCall)
        {
            try
            {
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = MakeConnection.GetConnection();
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@OrderIDs", SqlDbType.VarChar, 8000);
                objParam[0].Value = strOrderIds;

                objParam[1] = new SqlParameter("@IsManifestOrVoidCall", SqlDbType.VarChar, 20);
                objParam[1].Value = IsManifestOrVoidCall;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "VoidManifestOrders", objParam);
            }
            catch
            {
                MessageBox.Show("Error occurred during cancel FedEx orders", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
