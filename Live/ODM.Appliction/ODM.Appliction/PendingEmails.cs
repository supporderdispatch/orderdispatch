﻿using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Linq;
using System.Timers;
using System.Runtime.CompilerServices;
using System.Configuration;
using System.Threading;
namespace ODM.Appliction
{
    public partial class PendingEmails : Form
    {
        bool blHasCheckboxAdded = false;
        List<SendEmailEntity> lstEmails = new List<SendEmailEntity>();
        SendEmailEntity objSendEmail = new SendEmailEntity();
        Email objEmail = new Email();
        System.Timers.Timer timer = new System.Timers.Timer();
        string strIdsToDeleteOrUpdate = "", strNumOrderIds = "", strOrderIds = "";
        DataTable objDtBindGrid = new DataTable();
        public PendingEmails()
        {
            InitializeComponent();
            BindGrid();
        }
        private void BindGrid()
        {
            try
            {
                DataTable objDt = GlobalVariablesAndMethods.GetPendingOrSentEmail("Pending");
                if (objDt.Rows.Count > 0)
                {
                    for (int i = 0; i < objDtBindGrid.Rows.Count; i++)
                    {
                        DataView dv = new DataView(objDt);
                        dv.RowFilter = "OrderId <> '" + Convert.ToString(objDtBindGrid.Rows[i]["OrderId"]) + "'";
                        objDt = dv.ToTable();
                    }
                    if (objDt.Rows.Count > 0)
                    {
                        objDtBindGrid.Merge(CreateCustomDataSource(objDt));
                    }
                    lblMsg.Hide();
                    grdPendingEmails.Show();
                    lblTotalRecord.Text = "Total Records: " + objDtBindGrid.Rows.Count.ToString();
                    grdPendingEmails.AllowUserToAddRows = false;
                    grdPendingEmails.DataSource = objDtBindGrid;
                    if (!blHasCheckboxAdded)
                    {
                        DataGridViewCheckBoxColumn chkColumn = new DataGridViewCheckBoxColumn();
                        chkColumn.HeaderText = "";
                        chkColumn.Width = 30;
                        chkColumn.Name = "chkSendEmail";
                        grdPendingEmails.Columns.Insert(0, chkColumn);
                        blHasCheckboxAdded = true;
                    }
                    grdPendingEmails.Columns["OrderId"].Visible = false;
                    grdPendingEmails.Columns["ID"].Visible = false;
                    grdPendingEmails.Columns["Source"].Visible = false;
                    grdPendingEmails.Columns["SubSource"].Visible = false;
                    grdPendingEmails.Columns["SentMailBody"].Visible = false;
                    for (int i = 1; i < grdPendingEmails.Columns.Count; i++)
                    {
                        grdPendingEmails.Columns[i].ReadOnly = true;
                    }
                    grdPendingEmails.Columns["To Mail"].Width = 230;
                    grdPendingEmails.Columns["To Name"].Width = 230;
                    grdPendingEmails.Columns["Subject"].Width = 230;
                }
                else
                {
                    grdPendingEmails.Hide();
                    //grdPendingEmails.DataSource = objDtBindGrid;
                    lblMsg.Show();
                    lblTotalRecord.Text = "Total Records: 0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private DataTable CreateCustomDataSource(DataTable objDtPendingEmails)
        {
            DataTable objDtCondition = new DataTable();
            DataView dvFilterCondition = new DataView();
            DataTable objDtTemplateDetails = new DataTable();
            DataTable objDtDataToShowInTemplate = new DataTable();
            Guid OrderId = new Guid();
            int intRowCount = 0;
            if (objDtCondition.Rows.Count == 0)
            {
                objDtCondition = GlobalVariablesAndMethods.GetEmailConditions();
            }
            string strIDs = "", strMailID = "";
        ExecuteIfErrorInMainLoop:
            try
            {
                for (int i = intRowCount; i < objDtPendingEmails.Rows.Count; i++)
                {
                    string strDataColumns = "", strDataColumnsForSubject = "", strDataColumnsForDatabaseUse = "", strSubject = "";
                    string[] strArrColumnsInSubject = new string[] { };
                    dvFilterCondition = new DataView(objDtCondition);
                    Guid ConditionID = new Guid();
                    Guid TemplateID = new Guid();
                    string strSource = Convert.ToString(objDtPendingEmails.Rows[i]["Source"]);
                    string strSubSource = Convert.ToString(objDtPendingEmails.Rows[i]["SubSource"]);
                    OrderId = new Guid(Convert.ToString(objDtPendingEmails.Rows[i]["OrderId"]));
                    strMailID = Convert.ToString(objDtPendingEmails.Rows[i]["ID"]);
                    ConditionID = objEmail.GetConditionIDByConditionText(strSource, strSubSource, dvFilterCondition);

                ExecuteAgainIfTemplateIsDisabled:
                    objDtTemplateDetails = GlobalVariablesAndMethods.GetTemplateDetailsByTemplateId(TemplateID, ConditionID);
                    bool blIsEnabled = false;
                    if (objDtTemplateDetails.Rows.Count > 0)
                    {
                        blIsEnabled = Convert.ToBoolean(objDtTemplateDetails.Rows[0]["IsEnabled"]);
                    }
                    else
                    {
                        ConditionID = new Guid();
                        goto ExecuteAgainIfTemplateIsDisabled;
                    }
                    /*ConditionID can only be equal to 00000000-0000-0000-0000-000000000000 when either matched template is disabled 
                     * or don't have any matched template
                     * So in that case we need to skip those emails
                     */
                    if (ConditionID.ToString() == "00000000-0000-0000-0000-000000000000" && !blIsEnabled)
                    {
                        strIDs = strIDs == "" ? strMailID : strIDs + "," + strMailID;
                    }
                    /*Checking if this template is enabled or not, if not then setting ConditionID to default value of guid,
                     and executing GetTemplateDetailsByTemplateId again to get default template and settings
                     */
                    if (blIsEnabled)
                    {
                        strSubject = Convert.ToString(objDtTemplateDetails.Rows[0]["Subject"]);
                        if (strSubject.Contains("[{"))
                        {
                            objEmail.GetColumnNamesToGetDataFromDB(strSubject, ref strDataColumnsForDatabaseUse, ref strDataColumnsForSubject, ref strDataColumns, ref strArrColumnsInSubject, true);
                        ExecuteIf207Error:
                            try
                            {
                                //strDataColumnsForDatabaseUse ="A.XYZ AS [A.XYZ],"+ strDataColumnsForDatabaseUse + ",A.ABC AS [A.ABC]";
                                objDtDataToShowInTemplate = objEmail.GetDataToShowInTemplate(strDataColumnsForDatabaseUse, Convert.ToString(objDtPendingEmails.Rows[i]["OrderId"]));
                            }
                            catch (SqlException sqlEx)
                            {
                                /*If 207 error(means invalid column call) then RemoveInvalidColumnIfExistsInString method is
                                 *removing that column from list of text and database column string as well.
                                 * Then executing again with valid columns
                                 */
                                if (sqlEx.Number == 207)
                                {
                                    string strStringToCompare = strDataColumns;
                                    GlobalVariablesAndMethods.RemoveInvalidColumnIfExistsInString(ref strDataColumnsForSubject, ref strDataColumnsForDatabaseUse, ref strArrColumnsInSubject, sqlEx.Message);
                                    goto ExecuteIf207Error;
                                }
                            }
                            if (objDtDataToShowInTemplate.Rows.Count > 0)
                            {
                                strSubject = objEmail.ReplaceColumnWithData(strDataColumns, strDataColumnsForSubject, strSubject, objDtDataToShowInTemplate, true, strArrColumnsInSubject);
                            }
                        }
                        objDtPendingEmails.Rows[i]["Subject"] = strSubject;
                        intRowCount++;
                    }
                    else if (ConditionID.ToString() != "00000000-0000-0000-0000-000000000000")
                    {
                        ConditionID = new Guid();//Setting condition id to default
                        goto ExecuteAgainIfTemplateIsDisabled;
                    }
                }
            }
            catch (Exception ex)
            {
                intRowCount++;
                strIDs = strIDs == "" ? strMailID : strIDs + "," + strMailID;
                GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
                GlobalVariablesAndMethods.OrderIdToLog = OrderId;
                GlobalVariablesAndMethods.LogHistory("Error occurred while creating custom data source to bind pending email screen&&" + ex.Message);
                goto ExecuteIfErrorInMainLoop;
            }
            finally
            {
                if (strIDs != "")
                {
                    string[] strArrMailsToSkip = strIDs.Split(',');
                    for (int i = 0; i < strArrMailsToSkip.Length; i++)
                    {
                        DataView dv = new DataView(objDtPendingEmails);
                        dv.RowFilter = "ID <> '" + strArrMailsToSkip[i] + "'";
                        objDtPendingEmails = dv.ToTable();
                    }
                }
            }
            // blLoadAllData = false;
            return objDtPendingEmails;
        }
        private void btnAutoSend_Click(object sender, EventArgs e)
        {
            btnAutoSend.Enabled = false;
            //btnSendSelected.Enabled = false;
            //btnDeleteSelected.Enabled = false;
            lstEmails = new List<SendEmailEntity>();
            timer = new System.Timers.Timer();
            timer.Elapsed += new ElapsedEventHandler(AutoSendEmailsEvent);
            timer.Interval = Convert.ToDouble(ConfigurationManager.AppSettings["TimeIntervalInMin"]);
            //timer.Interval = 600;
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Start();
            Thread SendEmail = new Thread(AutoSendEmails);
            SendEmail.Start();
        }
        private void AutoSendEmailsEvent(object sender, ElapsedEventArgs e)
        {
            //AutoSendEmails();
            Thread SendEmail = new Thread(AutoSendEmails);
            SendEmail.Start();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void AutoSendEmails()
        {
            DataTable objDt = GlobalVariablesAndMethods.GetPendingOrSentEmail("Pending");
            CheckForIllegalCrossThreadCalls = false;
            if (objDt.Rows.Count > 0)
            {
                objDt = CreateCustomDataSource(objDt);
                for (int i = 0; i < objDt.Rows.Count; i++)
                {
                    try
                    {
                        string strSelectedEmailID = "", strNumOrderID = "", strOrderID = "", strSubject = "", strID = "";
                        strIdsToDeleteOrUpdate = ""; strNumOrderIds = "";
                        objSendEmail = new SendEmailEntity();
                        strSelectedEmailID = Convert.ToString(objDt.Rows[i]["To Mail"]);
                        strNumOrderID = Convert.ToString(objDt.Rows[i]["NumOrderId"]);
                        strOrderID = Convert.ToString(objDt.Rows[i]["OrderId"]);
                        strSubject = Convert.ToString(objDt.Rows[i]["Subject"]);
                        strID = Convert.ToString(objDt.Rows[i]["ID"]);

                        strIdsToDeleteOrUpdate = strIdsToDeleteOrUpdate == "" ? strID : strIdsToDeleteOrUpdate + "," + strID;
                        strNumOrderIds = strNumOrderIds == "" ? strNumOrderID : strNumOrderIds + "," + strNumOrderID;
                        strOrderIds = strOrderIds == "" ? strOrderID : strOrderIds + "," + strOrderID;

                        //If its not an email call then we dont need other details then id to delete and numOrderIds to log
                        if (!string.IsNullOrEmpty(strSelectedEmailID))
                        {
                            objSendEmail.EmailID = strSelectedEmailID;
                            objSendEmail.NumberOrderID = strNumOrderID;
                            objSendEmail.OrderID = strOrderID;
                            objSendEmail.SentSubject = strSubject;
                            objSendEmail.ID = strID;
                            objSendEmail.Source = Convert.ToString(objDt.Rows[i]["Source"]);
                            objSendEmail.SubSource = Convert.ToString(objDt.Rows[i]["SubSource"]);
                            lstEmails.Add(objSendEmail);
                        }
                    }
                    catch { throw; }
                }
                if (lstEmails.Count > 0)
                {
                    objEmail.SendEmailForDispatchedOrders(lstEmails, ref objDtBindGrid);
                    lstEmails = new List<SendEmailEntity>();
                    BindGrid();
                }
            }
            else
            {
                lstEmails = new List<SendEmailEntity>();
                btnAutoSend.Enabled = true;
                //btnSendSelected.Enabled = false;
                //btnDeleteSelected.Enabled = false;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDeleteSelected_Click(object sender, EventArgs e)
        {
            if (GetSelectedEmailIds(false))
            {
                DialogResult ConfirmDelete = MessageBox.Show("Are you sure, you want to delete selected email(s)", "Delete Mails", MessageBoxButtons.YesNo);
                if (ConfirmDelete == DialogResult.Yes)
                {
                    objSendEmail.IDs = strIdsToDeleteOrUpdate;
                    objSendEmail.NumOrderIDs = strNumOrderIds;
                    objSendEmail.OrderIDs = strOrderIds;
                    objSendEmail.Date = DateTime.Now;
                    objSendEmail.Mode = "DELETE";
                    GlobalVariablesAndMethods.InsertUpdateDeleteDataForPendingEmail(objSendEmail);
                    BindGrid();
                    MessageBox.Show("Mail(s) Deleted Successfully !", "Email Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
                MessageBox.Show("Select at least one order to send email", "Make Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            //blLoadAllData = true;
            BindGrid();
        }

        private void btnSendSelected_Click(object sender, EventArgs e)
        {
            if (GetSelectedEmailIds(true))
            {
                DialogResult ConfirmSendMail = MessageBox.Show("Are you sure, you want to send selected email(s)", "Send Mails", MessageBoxButtons.YesNo);
                if (ConfirmSendMail == DialogResult.Yes)
                {
                    objEmail.SendEmailForDispatchedOrders(lstEmails, ref objDtBindGrid);
                    BindGrid();
                    MessageBox.Show("Mail(s) sent Successfully !", "Email Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
                MessageBox.Show("Select at least one order to send email", "Make Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// To get the details of selected email either to send email or to delete those from db
        /// </summary>
        /// <param name="blIsSendEmailCall">True means details for send email, False means detials to delete selected rows from db</param>
        /// <returns>True if any row selected otherwise False</returns>
        private bool GetSelectedEmailIds(bool blIsSendEmailCall)
        {
            bool blIsAnyRowSelected = false;
            lstEmails = new List<SendEmailEntity>();
            try
            {
                string strSelectedEmailID = "", strNumOrderID = "", strOrderID = "", strSubject = "", strID = "", strNumOrderIds = "";
                strOrderIds = ""; strIdsToDeleteOrUpdate = "";
                foreach (DataGridViewRow row in grdPendingEmails.Rows)
                {
                    bool IsSelected = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells[0]).Value);
                    if (IsSelected)
                    {
                        objSendEmail = new SendEmailEntity();
                        blIsAnyRowSelected = true;
                        strSelectedEmailID = Convert.ToString(row.Cells["To Mail"].Value);
                        strNumOrderID = Convert.ToString(row.Cells["NumOrderId"].Value);
                        strOrderID = Convert.ToString(row.Cells["OrderId"].Value);
                        strSubject = Convert.ToString(row.Cells["Subject"].Value);
                        strID = Convert.ToString(row.Cells["ID"].Value);

                        strIdsToDeleteOrUpdate = strIdsToDeleteOrUpdate == "" ? strID : strIdsToDeleteOrUpdate + "," + strID;
                        strNumOrderIds = strNumOrderIds == "" ? strNumOrderID : strNumOrderIds + "," + strNumOrderID;
                        strOrderIds = strOrderIds == "" ? strOrderID : strOrderIds + "," + strOrderID;

                        //If its not an email call then we dont need other details then id to delete and numOrderIds to log
                        if (!string.IsNullOrEmpty(strSelectedEmailID) && blIsSendEmailCall)
                        {
                            objSendEmail.EmailID = strSelectedEmailID;
                            objSendEmail.NumberOrderID = strNumOrderID;
                            objSendEmail.OrderID = strOrderID;
                            objSendEmail.SentSubject = strSubject;
                            objSendEmail.ID = strID;
                            objSendEmail.Source = Convert.ToString(row.Cells["Source"].Value);
                            objSendEmail.SubSource = Convert.ToString(row.Cells["SubSource"].Value);
                            //objSendEmail.MailTemplate = "This is a test mail from OD, Ignore that ;)";
                            lstEmails.Add(objSendEmail);
                        }
                    }
                }
            }
            catch { throw; }
            return blIsAnyRowSelected;
        }

        private void emailConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmEmailNotification = Application.OpenForms["EmailConfiguration"];
            if (frmEmailNotification == null)
            {
                EmailConfiguration obj = new EmailConfiguration();
                obj.Show();
            }
        }

        private void sentEmailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmSentEmail = Application.OpenForms["SentEmail"];
            if (frmSentEmail == null)
            {
                SentEmail obj = new SentEmail();
                obj.Show();
            }
        }
    }
}
