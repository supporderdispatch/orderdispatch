﻿namespace ODM.Appliction
{
    partial class UrgentOrderSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UrgentOrderSetting));
            this.grdPostalServices = new System.Windows.Forms.DataGridView();
            this.chkUrgentOrder = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdPostalServices)).BeginInit();
            this.SuspendLayout();
            // 
            // grdPostalServices
            // 
            this.grdPostalServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPostalServices.Location = new System.Drawing.Point(1, 50);
            this.grdPostalServices.Name = "grdPostalServices";
            this.grdPostalServices.Size = new System.Drawing.Size(453, 364);
            this.grdPostalServices.TabIndex = 0;
            this.grdPostalServices.Visible = false;
            // 
            // chkUrgentOrder
            // 
            this.chkUrgentOrder.AutoSize = true;
            this.chkUrgentOrder.Location = new System.Drawing.Point(12, 27);
            this.chkUrgentOrder.Name = "chkUrgentOrder";
            this.chkUrgentOrder.Size = new System.Drawing.Size(137, 17);
            this.chkUrgentOrder.TabIndex = 1;
            this.chkUrgentOrder.Text = "Dispatch Urgent Orders";
            this.chkUrgentOrder.UseVisualStyleBackColor = true;
            this.chkUrgentOrder.CheckedChanged += new System.EventHandler(this.chkUrgentOrder_CheckedChanged);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(178, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(135, 32);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save Changes";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(319, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(135, 32);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // UrgentOrderSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 415);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkUrgentOrder);
            this.Controls.Add(this.grdPostalServices);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UrgentOrderSetting";
            this.Text = "Urgent Order Shipping Services";
            ((System.ComponentModel.ISupportInitialize)(this.grdPostalServices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdPostalServices;
        private System.Windows.Forms.CheckBox chkUrgentOrder;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}