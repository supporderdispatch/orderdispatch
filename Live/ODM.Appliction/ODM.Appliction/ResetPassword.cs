﻿using ODM.Data.Service;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class ResetPassword : BaseForm
    {
        public ResetPassword()
        {
            InitializeComponent();
        }

        private void ResetPassword_Load(object sender, EventArgs e)
        {
            IUserService userService = new UserService(ConfigString);
            List<Data.Entity.User> userList = userService.GetAllUsers();

            if (userList != null)
            {
                cboUser.ValueMember = "Userid";
                cboUser.DisplayMember = "Loginname";
                cboUser.DataSource = userList;

                //cboUser.SelectedValue = LoginedUser.Userid;
                //cboUser.SelectedText = LoginedUser.Loginname;
            }

            //GetAllUsers
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtConfirmPassword.Text))
            {
                if (validate())
                {
                    DialogResult result = MessageBox.Show("Are you sure you want to reset Password?", "Password Reset", MessageBoxButtons.YesNo);
                    if (result.Equals(DialogResult.Yes))
                    {
                        IUserService userService = new UserService(ConfigString);
                        Guid userid;
                        Guid.TryParse(cboUser.SelectedValue.ToString(), out userid);

                        userService.ChangePassword(userid, txtPassword.Text);
                        MessageBox.Show("Password  has been changed  sucessfully");
                        this.Close();
                    }
                }
                else
                {
                    lblError.Text = "Password and Confirm Password should be same";
                }
            }
            else
            {
                lblError.Text = "Password is required";
            }


        }

        bool validate()
        {
            if (txtConfirmPassword.Text == txtPassword.Text) return true;
            else return false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
