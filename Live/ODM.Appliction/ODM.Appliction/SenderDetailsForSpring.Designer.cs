﻿namespace ODM.Appliction
{
    partial class SenderDetailsForSpring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SenderDetailsForSpring));
            this.lblCompanyRequired = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblPostCodeRequired = new System.Windows.Forms.Label();
            this.txtPostCode = new System.Windows.Forms.TextBox();
            this.lblPostalCode = new System.Windows.Forms.Label();
            this.lblAddress2Required = new System.Windows.Forms.Label();
            this.txtStreet2 = new System.Windows.Forms.TextBox();
            this.lblStreet2 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.ddlCountry = new System.Windows.Forms.ComboBox();
            this.lblTownRequired = new System.Windows.Forms.Label();
            this.lblAddress1Required = new System.Windows.Forms.Label();
            this.lblPhoneRequired = new System.Windows.Forms.Label();
            this.lblFirstNameRequired = new System.Windows.Forms.Label();
            this.lblCountry = new System.Windows.Forms.Label();
            this.txtTown = new System.Windows.Forms.TextBox();
            this.lblRegion = new System.Windows.Forms.Label();
            this.txtStreet1 = new System.Windows.Forms.TextBox();
            this.lblStreet1 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblLastNameRequired = new System.Windows.Forms.Label();
            this.lblStreetNumber = new System.Windows.Forms.Label();
            this.txtStreetNumber = new System.Windows.Forms.TextBox();
            this.lblStreetNumberRequired = new System.Windows.Forms.Label();
            this.lblStreetNumberSuffix = new System.Windows.Forms.Label();
            this.txtStreeNumberSuffix = new System.Windows.Forms.TextBox();
            this.lblStreetNumberSuffixRequired = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblCityRequired = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCompanyRequired
            // 
            this.lblCompanyRequired.AutoSize = true;
            this.lblCompanyRequired.ForeColor = System.Drawing.Color.Red;
            this.lblCompanyRequired.Location = new System.Drawing.Point(640, 91);
            this.lblCompanyRequired.Name = "lblCompanyRequired";
            this.lblCompanyRequired.Size = new System.Drawing.Size(50, 13);
            this.lblCompanyRequired.TabIndex = 97;
            this.lblCompanyRequired.Text = "Required";
            this.lblCompanyRequired.Visible = false;
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(481, 88);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(153, 20);
            this.txtCompany.TabIndex = 96;
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(396, 88);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 95;
            this.lblCompany.Text = "Company";
            // 
            // lblPostCodeRequired
            // 
            this.lblPostCodeRequired.AutoSize = true;
            this.lblPostCodeRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPostCodeRequired.Location = new System.Drawing.Point(640, 26);
            this.lblPostCodeRequired.Name = "lblPostCodeRequired";
            this.lblPostCodeRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPostCodeRequired.TabIndex = 94;
            this.lblPostCodeRequired.Text = "Required";
            this.lblPostCodeRequired.Visible = false;
            // 
            // txtPostCode
            // 
            this.txtPostCode.Location = new System.Drawing.Point(481, 26);
            this.txtPostCode.Name = "txtPostCode";
            this.txtPostCode.Size = new System.Drawing.Size(153, 20);
            this.txtPostCode.TabIndex = 93;
            // 
            // lblPostalCode
            // 
            this.lblPostalCode.AutoSize = true;
            this.lblPostalCode.Location = new System.Drawing.Point(396, 26);
            this.lblPostalCode.Name = "lblPostalCode";
            this.lblPostalCode.Size = new System.Drawing.Size(64, 13);
            this.lblPostalCode.TabIndex = 92;
            this.lblPostalCode.Text = "Postal Code";
            // 
            // lblAddress2Required
            // 
            this.lblAddress2Required.AutoSize = true;
            this.lblAddress2Required.ForeColor = System.Drawing.Color.Red;
            this.lblAddress2Required.Location = new System.Drawing.Point(279, 141);
            this.lblAddress2Required.Name = "lblAddress2Required";
            this.lblAddress2Required.Size = new System.Drawing.Size(50, 13);
            this.lblAddress2Required.TabIndex = 91;
            this.lblAddress2Required.Text = "Required";
            this.lblAddress2Required.Visible = false;
            // 
            // txtStreet2
            // 
            this.txtStreet2.Location = new System.Drawing.Point(120, 141);
            this.txtStreet2.Name = "txtStreet2";
            this.txtStreet2.Size = new System.Drawing.Size(153, 20);
            this.txtStreet2.TabIndex = 90;
            // 
            // lblStreet2
            // 
            this.lblStreet2.AutoSize = true;
            this.lblStreet2.Location = new System.Drawing.Point(30, 141);
            this.lblStreet2.Name = "lblStreet2";
            this.lblStreet2.Size = new System.Drawing.Size(67, 13);
            this.lblStreet2.TabIndex = 89;
            this.lblStreet2.Text = "Street Line 2";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(369, 295);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(107, 29);
            this.btnCancel.TabIndex = 88;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(241, 296);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 29);
            this.btnSave.TabIndex = 87;
            this.btnSave.Text = "Save && Close";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ddlCountry
            // 
            this.ddlCountry.FormattingEnabled = true;
            this.ddlCountry.Location = new System.Drawing.Point(480, 117);
            this.ddlCountry.Name = "ddlCountry";
            this.ddlCountry.Size = new System.Drawing.Size(153, 21);
            this.ddlCountry.TabIndex = 86;
            // 
            // lblTownRequired
            // 
            this.lblTownRequired.AutoSize = true;
            this.lblTownRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTownRequired.Location = new System.Drawing.Point(640, 57);
            this.lblTownRequired.Name = "lblTownRequired";
            this.lblTownRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTownRequired.TabIndex = 85;
            this.lblTownRequired.Text = "Required";
            this.lblTownRequired.Visible = false;
            // 
            // lblAddress1Required
            // 
            this.lblAddress1Required.AutoSize = true;
            this.lblAddress1Required.ForeColor = System.Drawing.Color.Red;
            this.lblAddress1Required.Location = new System.Drawing.Point(278, 115);
            this.lblAddress1Required.Name = "lblAddress1Required";
            this.lblAddress1Required.Size = new System.Drawing.Size(50, 13);
            this.lblAddress1Required.TabIndex = 84;
            this.lblAddress1Required.Text = "Required";
            this.lblAddress1Required.Visible = false;
            // 
            // lblPhoneRequired
            // 
            this.lblPhoneRequired.AutoSize = true;
            this.lblPhoneRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPhoneRequired.Location = new System.Drawing.Point(278, 84);
            this.lblPhoneRequired.Name = "lblPhoneRequired";
            this.lblPhoneRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPhoneRequired.TabIndex = 83;
            this.lblPhoneRequired.Text = "Required";
            this.lblPhoneRequired.Visible = false;
            // 
            // lblFirstNameRequired
            // 
            this.lblFirstNameRequired.AutoSize = true;
            this.lblFirstNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblFirstNameRequired.Location = new System.Drawing.Point(279, 26);
            this.lblFirstNameRequired.Name = "lblFirstNameRequired";
            this.lblFirstNameRequired.Size = new System.Drawing.Size(50, 13);
            this.lblFirstNameRequired.TabIndex = 82;
            this.lblFirstNameRequired.Text = "Required";
            this.lblFirstNameRequired.Visible = false;
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(396, 119);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(43, 13);
            this.lblCountry.TabIndex = 81;
            this.lblCountry.Text = "Country";
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(481, 54);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(153, 20);
            this.txtTown.TabIndex = 80;
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Location = new System.Drawing.Point(396, 54);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(41, 13);
            this.lblRegion.TabIndex = 79;
            this.lblRegion.Text = "Region";
            // 
            // txtStreet1
            // 
            this.txtStreet1.Location = new System.Drawing.Point(119, 111);
            this.txtStreet1.Name = "txtStreet1";
            this.txtStreet1.Size = new System.Drawing.Size(153, 20);
            this.txtStreet1.TabIndex = 78;
            // 
            // lblStreet1
            // 
            this.lblStreet1.AutoSize = true;
            this.lblStreet1.Location = new System.Drawing.Point(29, 111);
            this.lblStreet1.Name = "lblStreet1";
            this.lblStreet1.Size = new System.Drawing.Size(67, 13);
            this.lblStreet1.TabIndex = 77;
            this.lblStreet1.Text = "Street Line 1";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(119, 81);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(153, 20);
            this.txtPhone.TabIndex = 76;
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(29, 81);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(78, 13);
            this.lblPhoneNumber.TabIndex = 75;
            this.lblPhoneNumber.Text = "Phone Number";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(120, 23);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(153, 20);
            this.txtFirstName.TabIndex = 74;
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(30, 23);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(57, 13);
            this.lblFirstName.TabIndex = 73;
            this.lblFirstName.Text = "First Name";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(30, 52);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(58, 13);
            this.lblLastName.TabIndex = 73;
            this.lblLastName.Text = "Last Name";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(120, 52);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(153, 20);
            this.txtLastName.TabIndex = 74;
            // 
            // lblLastNameRequired
            // 
            this.lblLastNameRequired.AutoSize = true;
            this.lblLastNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblLastNameRequired.Location = new System.Drawing.Point(279, 55);
            this.lblLastNameRequired.Name = "lblLastNameRequired";
            this.lblLastNameRequired.Size = new System.Drawing.Size(50, 13);
            this.lblLastNameRequired.TabIndex = 82;
            this.lblLastNameRequired.Text = "Required";
            this.lblLastNameRequired.Visible = false;
            // 
            // lblStreetNumber
            // 
            this.lblStreetNumber.AutoSize = true;
            this.lblStreetNumber.Location = new System.Drawing.Point(29, 173);
            this.lblStreetNumber.Name = "lblStreetNumber";
            this.lblStreetNumber.Size = new System.Drawing.Size(75, 13);
            this.lblStreetNumber.TabIndex = 89;
            this.lblStreetNumber.Text = "Street Number";
            // 
            // txtStreetNumber
            // 
            this.txtStreetNumber.Location = new System.Drawing.Point(119, 173);
            this.txtStreetNumber.Name = "txtStreetNumber";
            this.txtStreetNumber.Size = new System.Drawing.Size(153, 20);
            this.txtStreetNumber.TabIndex = 90;
            // 
            // lblStreetNumberRequired
            // 
            this.lblStreetNumberRequired.AutoSize = true;
            this.lblStreetNumberRequired.ForeColor = System.Drawing.Color.Red;
            this.lblStreetNumberRequired.Location = new System.Drawing.Point(278, 173);
            this.lblStreetNumberRequired.Name = "lblStreetNumberRequired";
            this.lblStreetNumberRequired.Size = new System.Drawing.Size(50, 13);
            this.lblStreetNumberRequired.TabIndex = 91;
            this.lblStreetNumberRequired.Text = "Required";
            this.lblStreetNumberRequired.Visible = false;
            // 
            // lblStreetNumberSuffix
            // 
            this.lblStreetNumberSuffix.AutoSize = true;
            this.lblStreetNumberSuffix.Location = new System.Drawing.Point(0, 210);
            this.lblStreetNumberSuffix.Name = "lblStreetNumberSuffix";
            this.lblStreetNumberSuffix.Size = new System.Drawing.Size(104, 13);
            this.lblStreetNumberSuffix.TabIndex = 89;
            this.lblStreetNumberSuffix.Text = "Street Number Suffix";
            // 
            // txtStreeNumberSuffix
            // 
            this.txtStreeNumberSuffix.Location = new System.Drawing.Point(119, 207);
            this.txtStreeNumberSuffix.Name = "txtStreeNumberSuffix";
            this.txtStreeNumberSuffix.Size = new System.Drawing.Size(153, 20);
            this.txtStreeNumberSuffix.TabIndex = 90;
            // 
            // lblStreetNumberSuffixRequired
            // 
            this.lblStreetNumberSuffixRequired.AutoSize = true;
            this.lblStreetNumberSuffixRequired.ForeColor = System.Drawing.Color.Red;
            this.lblStreetNumberSuffixRequired.Location = new System.Drawing.Point(278, 207);
            this.lblStreetNumberSuffixRequired.Name = "lblStreetNumberSuffixRequired";
            this.lblStreetNumberSuffixRequired.Size = new System.Drawing.Size(50, 13);
            this.lblStreetNumberSuffixRequired.TabIndex = 91;
            this.lblStreetNumberSuffixRequired.Text = "Required";
            this.lblStreetNumberSuffixRequired.Visible = false;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(396, 148);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(24, 13);
            this.lblCity.TabIndex = 95;
            this.lblCity.Text = "City";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(481, 148);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(153, 20);
            this.txtCity.TabIndex = 96;
            // 
            // lblCityRequired
            // 
            this.lblCityRequired.AutoSize = true;
            this.lblCityRequired.ForeColor = System.Drawing.Color.Red;
            this.lblCityRequired.Location = new System.Drawing.Point(640, 151);
            this.lblCityRequired.Name = "lblCityRequired";
            this.lblCityRequired.Size = new System.Drawing.Size(50, 13);
            this.lblCityRequired.TabIndex = 97;
            this.lblCityRequired.Text = "Required";
            this.lblCityRequired.Visible = false;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(396, 178);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(32, 13);
            this.lblEmail.TabIndex = 95;
            this.lblEmail.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(481, 178);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(153, 20);
            this.txtEmail.TabIndex = 96;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(640, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 97;
            this.label2.Text = "Required";
            this.label2.Visible = false;
            // 
            // SenderDetailsForSpring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 336);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCityRequired);
            this.Controls.Add(this.lblCompanyRequired);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtCity);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.lblCity);
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.lblPostCodeRequired);
            this.Controls.Add(this.txtPostCode);
            this.Controls.Add(this.lblPostalCode);
            this.Controls.Add(this.lblStreetNumberSuffixRequired);
            this.Controls.Add(this.txtStreeNumberSuffix);
            this.Controls.Add(this.lblStreetNumberRequired);
            this.Controls.Add(this.txtStreetNumber);
            this.Controls.Add(this.lblStreetNumberSuffix);
            this.Controls.Add(this.lblAddress2Required);
            this.Controls.Add(this.lblStreetNumber);
            this.Controls.Add(this.txtStreet2);
            this.Controls.Add(this.lblStreet2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ddlCountry);
            this.Controls.Add(this.lblTownRequired);
            this.Controls.Add(this.lblAddress1Required);
            this.Controls.Add(this.lblPhoneRequired);
            this.Controls.Add(this.lblLastNameRequired);
            this.Controls.Add(this.lblFirstNameRequired);
            this.Controls.Add(this.lblCountry);
            this.Controls.Add(this.txtTown);
            this.Controls.Add(this.lblRegion);
            this.Controls.Add(this.txtStreet1);
            this.Controls.Add(this.lblStreet1);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.lblPhoneNumber);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.lblFirstName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SenderDetailsForSpring";
            this.Text = "SenderDetailsForSpring";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCompanyRequired;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.Label lblPostCodeRequired;
        private System.Windows.Forms.TextBox txtPostCode;
        private System.Windows.Forms.Label lblPostalCode;
        private System.Windows.Forms.Label lblAddress2Required;
        private System.Windows.Forms.TextBox txtStreet2;
        private System.Windows.Forms.Label lblStreet2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox ddlCountry;
        private System.Windows.Forms.Label lblTownRequired;
        private System.Windows.Forms.Label lblAddress1Required;
        private System.Windows.Forms.Label lblPhoneRequired;
        private System.Windows.Forms.Label lblFirstNameRequired;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.TextBox txtTown;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.TextBox txtStreet1;
        private System.Windows.Forms.Label lblStreet1;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label lblLastNameRequired;
        private System.Windows.Forms.Label lblStreetNumber;
        private System.Windows.Forms.TextBox txtStreetNumber;
        private System.Windows.Forms.Label lblStreetNumberRequired;
        private System.Windows.Forms.Label lblStreetNumberSuffix;
        private System.Windows.Forms.TextBox txtStreeNumberSuffix;
        private System.Windows.Forms.Label lblStreetNumberSuffixRequired;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label lblCityRequired;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label2;
    }
}