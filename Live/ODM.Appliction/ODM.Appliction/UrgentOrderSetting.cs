﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
namespace ODM.Appliction
{
    public partial class UrgentOrderSetting : Form
    {
        public UrgentOrderSetting()
        {
            InitializeComponent();
            BindGrid(true);
        }

        private void BindGrid(bool IsCallFromLoad = false)
        {
            try
            {
                DataTable ObjPostalServices = new DataTable();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                ObjPostalServices = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetAllPostalServices").Tables[0];

                //Find if any data are under urgent selection
                var data = ObjPostalServices.AsEnumerable().Where(x => x.Field<bool>("Is Urgent") == true);
                DataTable objdt = new DataTable();
                foreach (var v in data)
                {
                    objdt = data.CopyToDataTable<DataRow>();
                    break;
                }
                if (objdt != null && objdt.Rows.Count > 0 && IsCallFromLoad)
                {
                    chkUrgentOrder.Checked = true;
                }
                //End of Find the if any data are under urgent selection

                if (ObjPostalServices.Rows.Count > 0 && chkUrgentOrder.Checked == true)
                {
                    grdPostalServices.DataSource = ObjPostalServices;
                    grdPostalServices.AllowUserToAddRows = false;
                    grdPostalServices.Columns["pkPostalServiceId"].Visible = false;
                    grdPostalServices.Columns["IsCN22_Required"].Visible = false;
                    grdPostalServices.Columns["Postal Service Name"].Width = 290;
                    grdPostalServices.Visible = true;
                }
            }
            catch
            {
                MessageBox.Show("An Error Occurred, Contact the Administrator");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SavePostalServiceIdsForUrgendOrders();
        }

        private void SavePostalServiceIdsForUrgendOrders()
        {
            if (chkUrgentOrder.Checked)
            {
                bool IsAnyCheckboxChecked = false;
                string strSelectedPostalIds = "";
                foreach (DataGridViewRow gvr in grdPostalServices.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)gvr.Cells[0];
                    if (Convert.ToBoolean(chk.Value) == true)
                    {
                        IsAnyCheckboxChecked = true;
                        if (strSelectedPostalIds == "")
                        {
                            strSelectedPostalIds = grdPostalServices.Rows[gvr.Index].Cells["pkPostalServiceId"].Value.ToString();
                        }
                        else
                        {
                            strSelectedPostalIds = strSelectedPostalIds + "," + grdPostalServices.Rows[gvr.Index].Cells["pkPostalServiceId"].Value.ToString();
                        }
                    }
                }
                if (!IsAnyCheckboxChecked)
                {
                    MessageBox.Show("Please select a Shipping Service");
                }
                else
                {
                    UpdateUrgentOrderStatus(strSelectedPostalIds);
                    MessageBox.Show("Selected Shipping Services has been set for urgent order dispatch process !");
                    this.Close();
                }
            }
            else
            {
                UpdateUrgentOrderStatus("");
                this.Close();
            }
        }
        private void UpdateUrgentOrderStatus(string strSelectedPostalIds)
        {
            //If strSelectedPostalIds=="" then we want to set IsUrgent flag to false to all postal services
            SqlConnection connection = new SqlConnection();
            try
            {
                SqlParameter[] objPostalParams = new SqlParameter[1];

                objPostalParams[0] = new SqlParameter("@PostalServiceIds", SqlDbType.VarChar, 8000);
                objPostalParams[0].Value = strSelectedPostalIds;

                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateUrgentOrderStatus", objPostalParams);
            }
            finally
            {
                connection.Close();
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkUrgentOrder_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUrgentOrder.Checked)
            {
                BindGrid();
            }
            else
            {
                grdPostalServices.Hide();
            }
        }
    }
}
