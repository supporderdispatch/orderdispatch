﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction.Customers
{
    public partial class Customers : Form
    {
        public Customers()
        {
            InitializeComponent();
            LoadCustomers();
            SetGridSettings();
            RegisterEvents();
        }

        private void RegisterEvents()
        {
            grdCustomers.CellContentClick += grdCustomers_CellContentClick;
        }

        void grdCustomers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                var row = senderGrid.Rows[e.RowIndex];
                var buttonText = row.Cells[e.ColumnIndex].Value.ToString();
                var customerId = row.Cells["CustomerId"].Value.ToString();
                var CustomerName = row.Cells["CustomerName"].Value.ToString();
                switch (buttonText)
                {
                    case "Edit":
                        EditCondition(customerId);
                        break;

                    case "Delete":
                        DeleteCondition(customerId, CustomerName);
                        break;

                    default:
                        break;
                }
            }
        }

        private void DeleteCondition(string customerId, string CustomerName)
        {
            DialogResult result = MessageBox.Show("Do You Want To Delete \" " + CustomerName + " " + "\"?",
              "Delete Customer",
          MessageBoxButtons.YesNo,
          MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                ODM.Core.FulFilmenCustomers.GetInstance().DeleteCustomer(customerId);
                LoadCustomers();
            }
            else
            {
                LoadCustomers();
            }
        }

        private void EditCondition(string customerId)
        {
            var frmAddCustomer = new AddCustomer(customerId);
            if (frmAddCustomer.ShowDialog() != null)
            {
                LoadCustomers();
            }
        }
        private void SetGridSettings()
        {
            grdCustomers.Columns["CustomerId"].Visible = false;
            grdCustomers.DoubleBuffered(true);
            grdCustomers.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdCustomers.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdCustomers.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdCustomers.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdCustomers.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdCustomers.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdCustomers.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdCustomers.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdCustomers.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdCustomers.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdCustomers.ScrollBars = ScrollBars.Both;
            grdCustomers.RowHeadersVisible = false;
            grdCustomers.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdCustomers.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdCustomers.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdCustomers.EnableHeadersVisualStyles = false;
            grdCustomers.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdCustomers.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdCustomers.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdCustomers.DefaultCellStyle.Padding = new Padding(5, 5, 5, 5);
            grdCustomers.AllowUserToAddRows = false;


            var buttonsToadd = new List<string>() 
            {
                "Edit",
                "Delete"
            };

            foreach (var button in buttonsToadd)
            {
                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                btn.HeaderText = string.Empty;
                btn.Name = "btn" + button + "Customer";
                btn.Text = button;
                btn.UseColumnTextForButtonValue = true;
                btn.Width = 30;
                btn.FlatStyle = FlatStyle.Flat;
                grdCustomers.Columns.Add(btn);
            }
        }

        private void LoadCustomers()
        {
            grdCustomers.DataSource = ODM.Core.FulFilmenCustomers.GetInstance().GetCustomers();
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            var frmAddCustomer = new AddCustomer();
             if (frmAddCustomer.ShowDialog() != null)
             {
                 LoadCustomers();
             }
        }
    }
}
