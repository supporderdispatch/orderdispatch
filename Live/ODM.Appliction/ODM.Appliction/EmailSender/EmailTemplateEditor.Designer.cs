﻿namespace ODM.Appliction.EmailSender
{
    partial class EmailTemplateEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailTemplateEditor));
            this.pnlEmailTemplateSettings = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnEmailConditions = new System.Windows.Forms.Button();
            this.ddlEmailAccount = new System.Windows.Forms.ComboBox();
            this.lblAccount = new System.Windows.Forms.Label();
            this.grpAttachement = new System.Windows.Forms.GroupBox();
            this.chkOrderImage = new System.Windows.Forms.CheckBox();
            this.lblOrderImage = new System.Windows.Forms.Label();
            this.chkInvoice = new System.Windows.Forms.CheckBox();
            this.lblInvoice = new System.Windows.Forms.Label();
            this.btnExitWithoutSave = new System.Windows.Forms.Button();
            this.btnSaveAndExit = new System.Windows.Forms.Button();
            this.grpTemplate = new System.Windows.Forms.GroupBox();
            this.chkHtml = new System.Windows.Forms.CheckBox();
            this.lblHtml = new System.Windows.Forms.Label();
            this.chkEnabled = new System.Windows.Forms.CheckBox();
            this.lblEnabled = new System.Windows.Forms.Label();
            this.txtTemplateName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblCC = new System.Windows.Forms.Label();
            this.txtCC = new System.Windows.Forms.TextBox();
            this.lblSubject = new System.Windows.Forms.Label();
            this.lblBCC = new System.Windows.Forms.Label();
            this.txtBCC = new System.Windows.Forms.TextBox();
            this.lblBody = new System.Windows.Forms.Label();
            this.lblInsertDBField = new System.Windows.Forms.Label();
            this.wbMailBody = new System.Windows.Forms.WebBrowser();
            this.lblTemplateSavedToolbar = new System.Windows.Forms.ToolStripLabel();
            this.btnTextForeColor = new System.Windows.Forms.ToolStripButton();
            this.btnCut = new System.Windows.Forms.ToolStripButton();
            this.btnCopy = new System.Windows.Forms.ToolStripButton();
            this.btnPaste = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.fontComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.fontSizeComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.btnBold = new System.Windows.Forms.ToolStripButton();
            this.btnItalic = new System.Windows.Forms.ToolStripButton();
            this.btnUnderLine = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnJustifyLeft = new System.Windows.Forms.ToolStripButton();
            this.btnJustifyCenter = new System.Windows.Forms.ToolStripButton();
            this.btnJustifyFull = new System.Windows.Forms.ToolStripButton();
            this.btnJustifyRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnIndent = new System.Windows.Forms.ToolStripButton();
            this.btnOutdent = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.wbMailSubject = new System.Windows.Forms.WebBrowser();
            this.lblEditToggle = new System.Windows.Forms.Label();
            this.txtBody = new System.Windows.Forms.RichTextBox();
            this.lblAddItemsTable = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlEmailTemplateSettings.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpAttachement.SuspendLayout();
            this.grpTemplate.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlEmailTemplateSettings
            // 
            this.pnlEmailTemplateSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlEmailTemplateSettings.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pnlEmailTemplateSettings.Controls.Add(this.groupBox1);
            this.pnlEmailTemplateSettings.Controls.Add(this.grpAttachement);
            this.pnlEmailTemplateSettings.Controls.Add(this.btnExitWithoutSave);
            this.pnlEmailTemplateSettings.Controls.Add(this.btnSaveAndExit);
            this.pnlEmailTemplateSettings.Controls.Add(this.grpTemplate);
            this.pnlEmailTemplateSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlEmailTemplateSettings.Location = new System.Drawing.Point(3, 1);
            this.pnlEmailTemplateSettings.Name = "pnlEmailTemplateSettings";
            this.pnlEmailTemplateSettings.Size = new System.Drawing.Size(1037, 116);
            this.pnlEmailTemplateSettings.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.groupBox1.Controls.Add(this.btnEmailConditions);
            this.groupBox1.Controls.Add(this.ddlEmailAccount);
            this.groupBox1.Controls.Add(this.lblAccount);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(392, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(243, 109);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Other Settings";
            // 
            // btnEmailConditions
            // 
            this.btnEmailConditions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmailConditions.Location = new System.Drawing.Point(75, 62);
            this.btnEmailConditions.Name = "btnEmailConditions";
            this.btnEmailConditions.Size = new System.Drawing.Size(153, 23);
            this.btnEmailConditions.TabIndex = 27;
            this.btnEmailConditions.Text = "Email Conditions";
            this.btnEmailConditions.UseVisualStyleBackColor = true;
            this.btnEmailConditions.Click += new System.EventHandler(this.btnEmailConditions_Click);
            // 
            // ddlEmailAccount
            // 
            this.ddlEmailAccount.FormattingEnabled = true;
            this.ddlEmailAccount.Location = new System.Drawing.Point(75, 22);
            this.ddlEmailAccount.Name = "ddlEmailAccount";
            this.ddlEmailAccount.Size = new System.Drawing.Size(153, 23);
            this.ddlEmailAccount.TabIndex = 26;
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Location = new System.Drawing.Point(6, 25);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(62, 15);
            this.lblAccount.TabIndex = 25;
            this.lblAccount.Text = "Mail from ";
            // 
            // grpAttachement
            // 
            this.grpAttachement.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.grpAttachement.Controls.Add(this.chkOrderImage);
            this.grpAttachement.Controls.Add(this.lblOrderImage);
            this.grpAttachement.Controls.Add(this.chkInvoice);
            this.grpAttachement.Controls.Add(this.lblInvoice);
            this.grpAttachement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpAttachement.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpAttachement.Location = new System.Drawing.Point(234, 3);
            this.grpAttachement.Name = "grpAttachement";
            this.grpAttachement.Size = new System.Drawing.Size(152, 109);
            this.grpAttachement.TabIndex = 8;
            this.grpAttachement.TabStop = false;
            this.grpAttachement.Text = "Attachement";
            // 
            // chkOrderImage
            // 
            this.chkOrderImage.AutoSize = true;
            this.chkOrderImage.Checked = true;
            this.chkOrderImage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOrderImage.Location = new System.Drawing.Point(87, 31);
            this.chkOrderImage.Name = "chkOrderImage";
            this.chkOrderImage.Size = new System.Drawing.Size(15, 14);
            this.chkOrderImage.TabIndex = 5;
            this.chkOrderImage.UseVisualStyleBackColor = true;
            // 
            // lblOrderImage
            // 
            this.lblOrderImage.AutoSize = true;
            this.lblOrderImage.Location = new System.Drawing.Point(5, 30);
            this.lblOrderImage.Name = "lblOrderImage";
            this.lblOrderImage.Size = new System.Drawing.Size(76, 15);
            this.lblOrderImage.TabIndex = 4;
            this.lblOrderImage.Text = "Order Image";
            // 
            // chkInvoice
            // 
            this.chkInvoice.AutoSize = true;
            this.chkInvoice.Checked = true;
            this.chkInvoice.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInvoice.Location = new System.Drawing.Point(87, 65);
            this.chkInvoice.Name = "chkInvoice";
            this.chkInvoice.Size = new System.Drawing.Size(15, 14);
            this.chkInvoice.TabIndex = 3;
            this.chkInvoice.UseVisualStyleBackColor = true;
            // 
            // lblInvoice
            // 
            this.lblInvoice.AutoSize = true;
            this.lblInvoice.Location = new System.Drawing.Point(6, 64);
            this.lblInvoice.Name = "lblInvoice";
            this.lblInvoice.Size = new System.Drawing.Size(45, 15);
            this.lblInvoice.TabIndex = 2;
            this.lblInvoice.Text = "Invoice";
            // 
            // btnExitWithoutSave
            // 
            this.btnExitWithoutSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExitWithoutSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExitWithoutSave.Location = new System.Drawing.Point(858, 43);
            this.btnExitWithoutSave.Name = "btnExitWithoutSave";
            this.btnExitWithoutSave.Size = new System.Drawing.Size(128, 27);
            this.btnExitWithoutSave.TabIndex = 7;
            this.btnExitWithoutSave.Text = "Exit Without Saving";
            this.btnExitWithoutSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExitWithoutSave.UseVisualStyleBackColor = true;
            this.btnExitWithoutSave.Click += new System.EventHandler(this.btnExitWithoutSave_Click);
            // 
            // btnSaveAndExit
            // 
            this.btnSaveAndExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAndExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveAndExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveAndExit.Location = new System.Drawing.Point(858, 14);
            this.btnSaveAndExit.Name = "btnSaveAndExit";
            this.btnSaveAndExit.Size = new System.Drawing.Size(128, 25);
            this.btnSaveAndExit.TabIndex = 6;
            this.btnSaveAndExit.Text = "Save and Exit";
            this.btnSaveAndExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveAndExit.UseVisualStyleBackColor = true;
            this.btnSaveAndExit.Click += new System.EventHandler(this.btnSaveAndExit_Click);
            // 
            // grpTemplate
            // 
            this.grpTemplate.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.grpTemplate.Controls.Add(this.chkHtml);
            this.grpTemplate.Controls.Add(this.lblHtml);
            this.grpTemplate.Controls.Add(this.chkEnabled);
            this.grpTemplate.Controls.Add(this.lblEnabled);
            this.grpTemplate.Controls.Add(this.txtTemplateName);
            this.grpTemplate.Controls.Add(this.lblName);
            this.grpTemplate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpTemplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTemplate.Location = new System.Drawing.Point(7, 4);
            this.grpTemplate.Name = "grpTemplate";
            this.grpTemplate.Size = new System.Drawing.Size(223, 108);
            this.grpTemplate.TabIndex = 0;
            this.grpTemplate.TabStop = false;
            this.grpTemplate.Text = "Template";
            // 
            // chkHtml
            // 
            this.chkHtml.AutoSize = true;
            this.chkHtml.Checked = true;
            this.chkHtml.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHtml.Location = new System.Drawing.Point(149, 65);
            this.chkHtml.Name = "chkHtml";
            this.chkHtml.Size = new System.Drawing.Size(15, 14);
            this.chkHtml.TabIndex = 5;
            this.chkHtml.UseVisualStyleBackColor = true;
            // 
            // lblHtml
            // 
            this.lblHtml.AutoSize = true;
            this.lblHtml.Location = new System.Drawing.Point(108, 64);
            this.lblHtml.Name = "lblHtml";
            this.lblHtml.Size = new System.Drawing.Size(33, 15);
            this.lblHtml.TabIndex = 4;
            this.lblHtml.Text = "Html";
            // 
            // chkEnabled
            // 
            this.chkEnabled.AutoSize = true;
            this.chkEnabled.Checked = true;
            this.chkEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEnabled.Location = new System.Drawing.Point(62, 65);
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.Size = new System.Drawing.Size(15, 14);
            this.chkEnabled.TabIndex = 3;
            this.chkEnabled.UseVisualStyleBackColor = true;
            // 
            // lblEnabled
            // 
            this.lblEnabled.AutoSize = true;
            this.lblEnabled.Location = new System.Drawing.Point(2, 64);
            this.lblEnabled.Name = "lblEnabled";
            this.lblEnabled.Size = new System.Drawing.Size(53, 15);
            this.lblEnabled.TabIndex = 2;
            this.lblEnabled.Text = "Enabled";
            // 
            // txtTemplateName
            // 
            this.txtTemplateName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTemplateName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTemplateName.Location = new System.Drawing.Point(59, 27);
            this.txtTemplateName.Name = "txtTemplateName";
            this.txtTemplateName.Size = new System.Drawing.Size(143, 20);
            this.txtTemplateName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(1, 27);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 15);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            // 
            // lblCC
            // 
            this.lblCC.AutoSize = true;
            this.lblCC.Location = new System.Drawing.Point(30, 195);
            this.lblCC.Name = "lblCC";
            this.lblCC.Size = new System.Drawing.Size(26, 13);
            this.lblCC.TabIndex = 35;
            this.lblCC.Text = "Cc :";
            // 
            // txtCC
            // 
            this.txtCC.Location = new System.Drawing.Point(62, 192);
            this.txtCC.Name = "txtCC";
            this.txtCC.Size = new System.Drawing.Size(185, 20);
            this.txtCC.TabIndex = 34;
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(7, 222);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(49, 13);
            this.lblSubject.TabIndex = 37;
            this.lblSubject.Text = "Subject :";
            // 
            // lblBCC
            // 
            this.lblBCC.AutoSize = true;
            this.lblBCC.Location = new System.Drawing.Point(272, 195);
            this.lblBCC.Name = "lblBCC";
            this.lblBCC.Size = new System.Drawing.Size(32, 13);
            this.lblBCC.TabIndex = 39;
            this.lblBCC.Text = "Bcc :";
            // 
            // txtBCC
            // 
            this.txtBCC.Location = new System.Drawing.Point(304, 192);
            this.txtBCC.Name = "txtBCC";
            this.txtBCC.Size = new System.Drawing.Size(185, 20);
            this.txtBCC.TabIndex = 38;
            // 
            // lblBody
            // 
            this.lblBody.AutoSize = true;
            this.lblBody.Location = new System.Drawing.Point(15, 275);
            this.lblBody.Name = "lblBody";
            this.lblBody.Size = new System.Drawing.Size(37, 13);
            this.lblBody.TabIndex = 41;
            this.lblBody.Text = "Body :";
            // 
            // lblInsertDBField
            // 
            this.lblInsertDBField.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblInsertDBField.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblInsertDBField.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblInsertDBField.Location = new System.Drawing.Point(509, 246);
            this.lblInsertDBField.Name = "lblInsertDBField";
            this.lblInsertDBField.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.lblInsertDBField.Size = new System.Drawing.Size(100, 23);
            this.lblInsertDBField.TabIndex = 42;
            this.lblInsertDBField.Text = "Insert DB Field";
            this.lblInsertDBField.Click += new System.EventHandler(this.lblInsertDBField_Click);
            // 
            // wbMailBody
            // 
            this.wbMailBody.Location = new System.Drawing.Point(62, 277);
            this.wbMailBody.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbMailBody.Name = "wbMailBody";
            this.wbMailBody.Size = new System.Drawing.Size(967, 305);
            this.wbMailBody.TabIndex = 44;
            // 
            // lblTemplateSavedToolbar
            // 
            this.lblTemplateSavedToolbar.AutoSize = false;
            this.lblTemplateSavedToolbar.Name = "lblTemplateSavedToolbar";
            this.lblTemplateSavedToolbar.Size = new System.Drawing.Size(82, 26);
            this.lblTemplateSavedToolbar.Text = "                         ";
            // 
            // btnTextForeColor
            // 
            this.btnTextForeColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTextForeColor.Image = ((System.Drawing.Image)(resources.GetObject("btnTextForeColor.Image")));
            this.btnTextForeColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTextForeColor.Name = "btnTextForeColor";
            this.btnTextForeColor.Size = new System.Drawing.Size(23, 26);
            this.btnTextForeColor.Text = "Text Color";
            this.btnTextForeColor.Click += new System.EventHandler(this.btnTextForeColor_Click);
            // 
            // btnCut
            // 
            this.btnCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCut.Image = ((System.Drawing.Image)(resources.GetObject("btnCut.Image")));
            this.btnCut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCut.Name = "btnCut";
            this.btnCut.Size = new System.Drawing.Size(23, 26);
            this.btnCut.Text = "C&ut";
            // 
            // btnCopy
            // 
            this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnCopy.Image")));
            this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(23, 26);
            this.btnCopy.Text = "&Copy";
            // 
            // btnPaste
            // 
            this.btnPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPaste.Image = ((System.Drawing.Image)(resources.GetObject("btnPaste.Image")));
            this.btnPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPaste.Name = "btnPaste";
            this.btnPaste.Size = new System.Drawing.Size(23, 26);
            this.btnPaste.Text = "&Paste";
            // 
            // toolStripBtnRefresh
            // 
            this.toolStripBtnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBtnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnRefresh.Image")));
            this.toolStripBtnRefresh.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripBtnRefresh.Name = "toolStripBtnRefresh";
            this.toolStripBtnRefresh.Size = new System.Drawing.Size(23, 26);
            this.toolStripBtnRefresh.Text = "toolStripButton1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 29);
            // 
            // fontComboBox
            // 
            this.fontComboBox.AutoSize = false;
            this.fontComboBox.Name = "fontComboBox";
            this.fontComboBox.Size = new System.Drawing.Size(200, 23);
            // 
            // fontSizeComboBox
            // 
            this.fontSizeComboBox.Name = "fontSizeComboBox";
            this.fontSizeComboBox.Size = new System.Drawing.Size(75, 29);
            // 
            // btnBold
            // 
            this.btnBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBold.Image = ((System.Drawing.Image)(resources.GetObject("btnBold.Image")));
            this.btnBold.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBold.Name = "btnBold";
            this.btnBold.Size = new System.Drawing.Size(23, 26);
            this.btnBold.Text = "Bold";
            this.btnBold.Click += new System.EventHandler(this.btnBold_Click);
            // 
            // btnItalic
            // 
            this.btnItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnItalic.Image = ((System.Drawing.Image)(resources.GetObject("btnItalic.Image")));
            this.btnItalic.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnItalic.Name = "btnItalic";
            this.btnItalic.Size = new System.Drawing.Size(23, 26);
            this.btnItalic.Text = "toolStripButton2";
            this.btnItalic.Click += new System.EventHandler(this.btnItalic_Click);
            // 
            // btnUnderLine
            // 
            this.btnUnderLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUnderLine.Image = ((System.Drawing.Image)(resources.GetObject("btnUnderLine.Image")));
            this.btnUnderLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUnderLine.Name = "btnUnderLine";
            this.btnUnderLine.Size = new System.Drawing.Size(23, 26);
            this.btnUnderLine.Text = "toolStripButton3";
            this.btnUnderLine.Click += new System.EventHandler(this.btnUnderLine_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 29);
            // 
            // btnJustifyLeft
            // 
            this.btnJustifyLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnJustifyLeft.Image = ((System.Drawing.Image)(resources.GetObject("btnJustifyLeft.Image")));
            this.btnJustifyLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJustifyLeft.Name = "btnJustifyLeft";
            this.btnJustifyLeft.Size = new System.Drawing.Size(23, 26);
            this.btnJustifyLeft.Text = "Justify Left";
            this.btnJustifyLeft.Click += new System.EventHandler(this.btnJustifyLeft_Click);
            // 
            // btnJustifyCenter
            // 
            this.btnJustifyCenter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnJustifyCenter.Image = ((System.Drawing.Image)(resources.GetObject("btnJustifyCenter.Image")));
            this.btnJustifyCenter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJustifyCenter.Name = "btnJustifyCenter";
            this.btnJustifyCenter.Size = new System.Drawing.Size(23, 26);
            this.btnJustifyCenter.Text = "Justify center";
            this.btnJustifyCenter.Click += new System.EventHandler(this.btnJustifyCenter_Click);
            // 
            // btnJustifyFull
            // 
            this.btnJustifyFull.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnJustifyFull.Image = ((System.Drawing.Image)(resources.GetObject("btnJustifyFull.Image")));
            this.btnJustifyFull.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJustifyFull.Name = "btnJustifyFull";
            this.btnJustifyFull.Size = new System.Drawing.Size(23, 26);
            this.btnJustifyFull.Text = "Justify Full";
            this.btnJustifyFull.Click += new System.EventHandler(this.btnJustifyFull_Click);
            // 
            // btnJustifyRight
            // 
            this.btnJustifyRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnJustifyRight.Image = ((System.Drawing.Image)(resources.GetObject("btnJustifyRight.Image")));
            this.btnJustifyRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJustifyRight.Name = "btnJustifyRight";
            this.btnJustifyRight.Size = new System.Drawing.Size(23, 26);
            this.btnJustifyRight.Text = "Justify Right";
            this.btnJustifyRight.Click += new System.EventHandler(this.btnJustifyRight_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 29);
            // 
            // btnIndent
            // 
            this.btnIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnIndent.Image = ((System.Drawing.Image)(resources.GetObject("btnIndent.Image")));
            this.btnIndent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnIndent.Name = "btnIndent";
            this.btnIndent.Size = new System.Drawing.Size(23, 26);
            this.btnIndent.Text = "Indent ";
            this.btnIndent.Click += new System.EventHandler(this.btnIndent_Click);
            // 
            // btnOutdent
            // 
            this.btnOutdent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnOutdent.Image = ((System.Drawing.Image)(resources.GetObject("btnOutdent.Image")));
            this.btnOutdent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOutdent.Name = "btnOutdent";
            this.btnOutdent.Size = new System.Drawing.Size(23, 26);
            this.btnOutdent.Text = "Outdent";
            this.btnOutdent.Click += new System.EventHandler(this.btnOutdent_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(76, 26);
            this.toolStripLabel1.Text = "                       ";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblTemplateSavedToolbar,
            this.btnCut,
            this.btnCopy,
            this.btnPaste,
            this.toolStripBtnRefresh,
            this.toolStripSeparator1,
            this.fontComboBox,
            this.fontSizeComboBox,
            this.btnBold,
            this.btnItalic,
            this.btnUnderLine,
            this.btnTextForeColor,
            this.toolStripSeparator2,
            this.btnJustifyLeft,
            this.btnJustifyCenter,
            this.btnJustifyFull,
            this.btnJustifyRight,
            this.toolStripSeparator3,
            this.btnIndent,
            this.btnOutdent,
            this.toolStripLabel1});
            this.toolStrip1.Location = new System.Drawing.Point(3, 120);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(789, 29);
            this.toolStrip1.TabIndex = 46;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // wbMailSubject
            // 
            this.wbMailSubject.Location = new System.Drawing.Point(62, 222);
            this.wbMailSubject.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbMailSubject.Name = "wbMailSubject";
            this.wbMailSubject.ScrollBarsEnabled = false;
            this.wbMailSubject.Size = new System.Drawing.Size(427, 47);
            this.wbMailSubject.TabIndex = 47;
            // 
            // lblEditToggle
            // 
            this.lblEditToggle.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblEditToggle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblEditToggle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblEditToggle.Location = new System.Drawing.Point(929, 246);
            this.lblEditToggle.Name = "lblEditToggle";
            this.lblEditToggle.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.lblEditToggle.Size = new System.Drawing.Size(100, 23);
            this.lblEditToggle.TabIndex = 48;
            this.lblEditToggle.Text = "Edit as Html";
            this.lblEditToggle.Click += new System.EventHandler(this.lblEditToggle_Click);
            // 
            // txtBody
            // 
            this.txtBody.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBody.Location = new System.Drawing.Point(62, 279);
            this.txtBody.Name = "txtBody";
            this.txtBody.Size = new System.Drawing.Size(967, 305);
            this.txtBody.TabIndex = 49;
            this.txtBody.Text = "";
            this.txtBody.Visible = false;
            // 
            // lblAddItemsTable
            // 
            this.lblAddItemsTable.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblAddItemsTable.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblAddItemsTable.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblAddItemsTable.Image = ((System.Drawing.Image)(resources.GetObject("lblAddItemsTable.Image")));
            this.lblAddItemsTable.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lblAddItemsTable.Location = new System.Drawing.Point(618, 246);
            this.lblAddItemsTable.Name = "lblAddItemsTable";
            this.lblAddItemsTable.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.lblAddItemsTable.Size = new System.Drawing.Size(110, 23);
            this.lblAddItemsTable.TabIndex = 50;
            this.lblAddItemsTable.Text = "        Add Item Table";
            this.lblAddItemsTable.Click += new System.EventHandler(this.lblAddItemsTable_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(69, 593);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(397, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "* To insert db fields as url parameter use @@[\'dbfield\']  eg. href=\'/@@NumOrderId" +
    "\'";
            // 
            // EmailTemplateEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 615);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblAddItemsTable);
            this.Controls.Add(this.txtBody);
            this.Controls.Add(this.lblEditToggle);
            this.Controls.Add(this.wbMailSubject);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.wbMailBody);
            this.Controls.Add(this.lblInsertDBField);
            this.Controls.Add(this.lblBody);
            this.Controls.Add(this.lblBCC);
            this.Controls.Add(this.txtBCC);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.lblCC);
            this.Controls.Add(this.txtCC);
            this.Controls.Add(this.pnlEmailTemplateSettings);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmailTemplateEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EmailTemplateEditor";
            this.pnlEmailTemplateSettings.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpAttachement.ResumeLayout(false);
            this.grpAttachement.PerformLayout();
            this.grpTemplate.ResumeLayout(false);
            this.grpTemplate.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlEmailTemplateSettings;
        private System.Windows.Forms.Button btnExitWithoutSave;
        private System.Windows.Forms.Button btnSaveAndExit;
        private System.Windows.Forms.GroupBox grpTemplate;
        private System.Windows.Forms.CheckBox chkEnabled;
        private System.Windows.Forms.Label lblEnabled;
        private System.Windows.Forms.TextBox txtTemplateName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.GroupBox grpAttachement;
        private System.Windows.Forms.CheckBox chkInvoice;
        private System.Windows.Forms.Label lblInvoice;
        private System.Windows.Forms.CheckBox chkOrderImage;
        private System.Windows.Forms.Label lblOrderImage;
        private System.Windows.Forms.CheckBox chkHtml;
        private System.Windows.Forms.Label lblHtml;
        private System.Windows.Forms.Label lblCC;
        private System.Windows.Forms.TextBox txtCC;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label lblBCC;
        private System.Windows.Forms.TextBox txtBCC;
        private System.Windows.Forms.Label lblBody;
        private System.Windows.Forms.Label lblInsertDBField;
        private System.Windows.Forms.WebBrowser wbMailBody;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblAccount;
        private System.Windows.Forms.ComboBox ddlEmailAccount;
        private System.Windows.Forms.Button btnEmailConditions;
        private System.Windows.Forms.ToolStripLabel lblTemplateSavedToolbar;
        private System.Windows.Forms.ToolStripButton btnTextForeColor;
        private System.Windows.Forms.ToolStripButton btnCut;
        private System.Windows.Forms.ToolStripButton btnCopy;
        private System.Windows.Forms.ToolStripButton btnPaste;
        private System.Windows.Forms.ToolStripButton toolStripBtnRefresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripComboBox fontComboBox;
        private System.Windows.Forms.ToolStripComboBox fontSizeComboBox;
        private System.Windows.Forms.ToolStripButton btnBold;
        private System.Windows.Forms.ToolStripButton btnItalic;
        private System.Windows.Forms.ToolStripButton btnUnderLine;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnJustifyLeft;
        private System.Windows.Forms.ToolStripButton btnJustifyCenter;
        private System.Windows.Forms.ToolStripButton btnJustifyFull;
        private System.Windows.Forms.ToolStripButton btnJustifyRight;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnIndent;
        private System.Windows.Forms.ToolStripButton btnOutdent;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.WebBrowser wbMailSubject;
        private System.Windows.Forms.Label lblEditToggle;
        private System.Windows.Forms.RichTextBox txtBody;
        private System.Windows.Forms.Label lblAddItemsTable;
        private System.Windows.Forms.Label label1;
    }
}