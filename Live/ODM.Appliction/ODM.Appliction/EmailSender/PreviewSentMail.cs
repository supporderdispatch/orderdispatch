﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction.EmailSender
{
    public partial class PreviewSentMail : Form
    {

        public PreviewSentMail()
        {
            InitializeComponent();
        }

        public PreviewSentMail(Entities.Emails.Email email)
        {
            InitializeComponent();
            LoadPreviewEmailAsync(email);            
            
        }

        private async  void LoadPreviewEmailAsync(Entities.Emails.Email email)
        {
           var emailTemplate=new Entities.Emails.EmailTemplate();
           await Task.Run(() => { emailTemplate = ODM.Core.Emails.EmailTemplates.GetInstance().GetEmailTemplate(email.EmailTemplateId); }).ContinueWith((r) =>
           {
               LoadSentMailIntoControls(email, emailTemplate);
           });
        }

        private void LoadSentMailIntoControls(Entities.Emails.Email email,ODM.Entities.Emails.EmailTemplate emailTemplate)
        {            

            txtCC.Text = emailTemplate.Cc;
            txtBCC.Text = emailTemplate.Bcc;
            chkInvoice.Checked = emailTemplate.WithImage;
            chkOrderImage.Checked = emailTemplate.WithPdf;
            wbMailBody.DocumentText = email.SentMailBody.Replace("whitesmoke","inherit");
            wbMailSubject.DocumentText = email.SentSubject.Replace("whitesmoke", "inherit");

            txtBCC.Enabled = false;
            txtCC.Enabled = false;
            chkOrderImage.Enabled = false;
            chkInvoice.Enabled = false;
        }
    }
}
