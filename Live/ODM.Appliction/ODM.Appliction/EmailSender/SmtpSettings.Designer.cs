﻿namespace ODM.Appliction.EmailSender
{
    partial class SmtpSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SmtpSettings));
            this.gboxSenderSettings = new System.Windows.Forms.GroupBox();
            this.lblSendToCopyRequired = new System.Windows.Forms.Label();
            this.lblFromNameRequired = new System.Windows.Forms.Label();
            this.lblReplyToEmailRequired = new System.Windows.Forms.Label();
            this.txtSendToCopy = new System.Windows.Forms.TextBox();
            this.lblSentCopyTo = new System.Windows.Forms.Label();
            this.txtFromName = new System.Windows.Forms.TextBox();
            this.lblFromName = new System.Windows.Forms.Label();
            this.txtReplyToEmail = new System.Windows.Forms.TextBox();
            this.lblReplyToEmail = new System.Windows.Forms.Label();
            this.gboxSMTPSettings = new System.Windows.Forms.GroupBox();
            this.lblPasswordRequired = new System.Windows.Forms.Label();
            this.lblUserNameRequired = new System.Windows.Forms.Label();
            this.lblPortRequired = new System.Windows.Forms.Label();
            this.lblSMTPRequired = new System.Windows.Forms.Label();
            this.chkSSL = new System.Windows.Forms.CheckBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.chkSMTP_Auth = new System.Windows.Forms.CheckBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.lblPort = new System.Windows.Forms.Label();
            this.txtSMTPServer = new System.Windows.Forms.TextBox();
            this.lblSMTPServer = new System.Windows.Forms.Label();
            this.chkEnabled = new System.Windows.Forms.CheckBox();
            this.ddlEmaiAccount = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnNewAccount = new System.Windows.Forms.Button();
            this.btbDelete = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblEmailAccRequired = new System.Windows.Forms.Label();
            this.gboxSenderSettings.SuspendLayout();
            this.gboxSMTPSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // gboxSenderSettings
            // 
            this.gboxSenderSettings.Controls.Add(this.lblSendToCopyRequired);
            this.gboxSenderSettings.Controls.Add(this.lblFromNameRequired);
            this.gboxSenderSettings.Controls.Add(this.lblReplyToEmailRequired);
            this.gboxSenderSettings.Controls.Add(this.txtSendToCopy);
            this.gboxSenderSettings.Controls.Add(this.lblSentCopyTo);
            this.gboxSenderSettings.Controls.Add(this.txtFromName);
            this.gboxSenderSettings.Controls.Add(this.lblFromName);
            this.gboxSenderSettings.Controls.Add(this.txtReplyToEmail);
            this.gboxSenderSettings.Controls.Add(this.lblReplyToEmail);
            this.gboxSenderSettings.Location = new System.Drawing.Point(411, 61);
            this.gboxSenderSettings.Name = "gboxSenderSettings";
            this.gboxSenderSettings.Size = new System.Drawing.Size(367, 145);
            this.gboxSenderSettings.TabIndex = 4;
            this.gboxSenderSettings.TabStop = false;
            this.gboxSenderSettings.Text = "Sender ";
            // 
            // lblSendToCopyRequired
            // 
            this.lblSendToCopyRequired.AutoSize = true;
            this.lblSendToCopyRequired.ForeColor = System.Drawing.Color.Red;
            this.lblSendToCopyRequired.Location = new System.Drawing.Point(104, 106);
            this.lblSendToCopyRequired.Name = "lblSendToCopyRequired";
            this.lblSendToCopyRequired.Size = new System.Drawing.Size(11, 13);
            this.lblSendToCopyRequired.TabIndex = 17;
            this.lblSendToCopyRequired.Tag = "";
            this.lblSendToCopyRequired.Text = "*";
            this.lblSendToCopyRequired.Visible = false;
            // 
            // lblFromNameRequired
            // 
            this.lblFromNameRequired.AutoSize = true;
            this.lblFromNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblFromNameRequired.Location = new System.Drawing.Point(104, 71);
            this.lblFromNameRequired.Name = "lblFromNameRequired";
            this.lblFromNameRequired.Size = new System.Drawing.Size(11, 13);
            this.lblFromNameRequired.TabIndex = 16;
            this.lblFromNameRequired.Text = "*";
            this.lblFromNameRequired.Visible = false;
            // 
            // lblReplyToEmailRequired
            // 
            this.lblReplyToEmailRequired.AutoSize = true;
            this.lblReplyToEmailRequired.ForeColor = System.Drawing.Color.Red;
            this.lblReplyToEmailRequired.Location = new System.Drawing.Point(104, 32);
            this.lblReplyToEmailRequired.Name = "lblReplyToEmailRequired";
            this.lblReplyToEmailRequired.Size = new System.Drawing.Size(11, 13);
            this.lblReplyToEmailRequired.TabIndex = 15;
            this.lblReplyToEmailRequired.Text = "*";
            this.lblReplyToEmailRequired.Visible = false;
            // 
            // txtSendToCopy
            // 
            this.txtSendToCopy.Location = new System.Drawing.Point(123, 103);
            this.txtSendToCopy.Name = "txtSendToCopy";
            this.txtSendToCopy.Size = new System.Drawing.Size(201, 20);
            this.txtSendToCopy.TabIndex = 8;
            // 
            // lblSentCopyTo
            // 
            this.lblSentCopyTo.AutoSize = true;
            this.lblSentCopyTo.Location = new System.Drawing.Point(7, 104);
            this.lblSentCopyTo.Name = "lblSentCopyTo";
            this.lblSentCopyTo.Size = new System.Drawing.Size(75, 13);
            this.lblSentCopyTo.TabIndex = 7;
            this.lblSentCopyTo.Text = "Send Copy To";
            // 
            // txtFromName
            // 
            this.txtFromName.Location = new System.Drawing.Point(123, 68);
            this.txtFromName.Name = "txtFromName";
            this.txtFromName.Size = new System.Drawing.Size(201, 20);
            this.txtFromName.TabIndex = 6;
            // 
            // lblFromName
            // 
            this.lblFromName.AutoSize = true;
            this.lblFromName.Location = new System.Drawing.Point(7, 69);
            this.lblFromName.Name = "lblFromName";
            this.lblFromName.Size = new System.Drawing.Size(61, 13);
            this.lblFromName.TabIndex = 5;
            this.lblFromName.Text = "From Name";
            // 
            // txtReplyToEmail
            // 
            this.txtReplyToEmail.Location = new System.Drawing.Point(121, 29);
            this.txtReplyToEmail.Name = "txtReplyToEmail";
            this.txtReplyToEmail.Size = new System.Drawing.Size(201, 20);
            this.txtReplyToEmail.TabIndex = 4;
            // 
            // lblReplyToEmail
            // 
            this.lblReplyToEmail.AutoSize = true;
            this.lblReplyToEmail.Location = new System.Drawing.Point(5, 30);
            this.lblReplyToEmail.Name = "lblReplyToEmail";
            this.lblReplyToEmail.Size = new System.Drawing.Size(78, 13);
            this.lblReplyToEmail.TabIndex = 3;
            this.lblReplyToEmail.Text = "Reply To Email";
            // 
            // gboxSMTPSettings
            // 
            this.gboxSMTPSettings.Controls.Add(this.lblPasswordRequired);
            this.gboxSMTPSettings.Controls.Add(this.lblUserNameRequired);
            this.gboxSMTPSettings.Controls.Add(this.lblPortRequired);
            this.gboxSMTPSettings.Controls.Add(this.lblSMTPRequired);
            this.gboxSMTPSettings.Controls.Add(this.chkSSL);
            this.gboxSMTPSettings.Controls.Add(this.txtPassword);
            this.gboxSMTPSettings.Controls.Add(this.lblPassword);
            this.gboxSMTPSettings.Controls.Add(this.txtUserName);
            this.gboxSMTPSettings.Controls.Add(this.lblUserName);
            this.gboxSMTPSettings.Controls.Add(this.chkSMTP_Auth);
            this.gboxSMTPSettings.Controls.Add(this.txtPort);
            this.gboxSMTPSettings.Controls.Add(this.lblPort);
            this.gboxSMTPSettings.Controls.Add(this.txtSMTPServer);
            this.gboxSMTPSettings.Controls.Add(this.lblSMTPServer);
            this.gboxSMTPSettings.Controls.Add(this.chkEnabled);
            this.gboxSMTPSettings.Location = new System.Drawing.Point(12, 61);
            this.gboxSMTPSettings.Name = "gboxSMTPSettings";
            this.gboxSMTPSettings.Size = new System.Drawing.Size(393, 242);
            this.gboxSMTPSettings.TabIndex = 5;
            this.gboxSMTPSettings.TabStop = false;
            this.gboxSMTPSettings.Text = "SMTP Server";
            // 
            // lblPasswordRequired
            // 
            this.lblPasswordRequired.AutoSize = true;
            this.lblPasswordRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPasswordRequired.Location = new System.Drawing.Point(105, 166);
            this.lblPasswordRequired.Name = "lblPasswordRequired";
            this.lblPasswordRequired.Size = new System.Drawing.Size(11, 13);
            this.lblPasswordRequired.TabIndex = 14;
            this.lblPasswordRequired.Text = "*";
            this.lblPasswordRequired.Visible = false;
            // 
            // lblUserNameRequired
            // 
            this.lblUserNameRequired.AutoSize = true;
            this.lblUserNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblUserNameRequired.Location = new System.Drawing.Point(106, 131);
            this.lblUserNameRequired.Name = "lblUserNameRequired";
            this.lblUserNameRequired.Size = new System.Drawing.Size(11, 13);
            this.lblUserNameRequired.TabIndex = 13;
            this.lblUserNameRequired.Text = "*";
            this.lblUserNameRequired.Visible = false;
            // 
            // lblPortRequired
            // 
            this.lblPortRequired.AutoSize = true;
            this.lblPortRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPortRequired.Location = new System.Drawing.Point(105, 62);
            this.lblPortRequired.Name = "lblPortRequired";
            this.lblPortRequired.Size = new System.Drawing.Size(11, 13);
            this.lblPortRequired.TabIndex = 12;
            this.lblPortRequired.Text = "*";
            this.lblPortRequired.Visible = false;
            // 
            // lblSMTPRequired
            // 
            this.lblSMTPRequired.AutoSize = true;
            this.lblSMTPRequired.ForeColor = System.Drawing.Color.Red;
            this.lblSMTPRequired.Location = new System.Drawing.Point(106, 29);
            this.lblSMTPRequired.Name = "lblSMTPRequired";
            this.lblSMTPRequired.Size = new System.Drawing.Size(11, 13);
            this.lblSMTPRequired.TabIndex = 11;
            this.lblSMTPRequired.Text = "*";
            this.lblSMTPRequired.Visible = false;
            // 
            // chkSSL
            // 
            this.chkSSL.AutoSize = true;
            this.chkSSL.Location = new System.Drawing.Point(237, 219);
            this.chkSSL.Name = "chkSSL";
            this.chkSSL.Size = new System.Drawing.Size(68, 17);
            this.chkSSL.TabIndex = 10;
            this.chkSSL.Text = "Use SSL";
            this.chkSSL.UseVisualStyleBackColor = true;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(122, 163);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(201, 20);
            this.txtPassword.TabIndex = 9;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(6, 168);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 8;
            this.lblPassword.Text = "Password";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(123, 128);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(201, 20);
            this.txtUserName.TabIndex = 7;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(7, 133);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(60, 13);
            this.lblUserName.TabIndex = 6;
            this.lblUserName.Text = "User Name";
            // 
            // chkSMTP_Auth
            // 
            this.chkSMTP_Auth.AutoSize = true;
            this.chkSMTP_Auth.Location = new System.Drawing.Point(123, 97);
            this.chkSMTP_Auth.Name = "chkSMTP_Auth";
            this.chkSMTP_Auth.Size = new System.Drawing.Size(152, 17);
            this.chkSMTP_Auth.TabIndex = 5;
            this.chkSMTP_Auth.Text = "Use SMTP Authentication ";
            this.chkSMTP_Auth.UseVisualStyleBackColor = true;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(123, 59);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(201, 20);
            this.txtPort.TabIndex = 4;
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(7, 64);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(26, 13);
            this.lblPort.TabIndex = 3;
            this.lblPort.Text = "Port";
            // 
            // txtSMTPServer
            // 
            this.txtSMTPServer.Location = new System.Drawing.Point(123, 26);
            this.txtSMTPServer.Name = "txtSMTPServer";
            this.txtSMTPServer.Size = new System.Drawing.Size(201, 20);
            this.txtSMTPServer.TabIndex = 2;
            // 
            // lblSMTPServer
            // 
            this.lblSMTPServer.AutoSize = true;
            this.lblSMTPServer.Location = new System.Drawing.Point(7, 27);
            this.lblSMTPServer.Name = "lblSMTPServer";
            this.lblSMTPServer.Size = new System.Drawing.Size(71, 13);
            this.lblSMTPServer.TabIndex = 1;
            this.lblSMTPServer.Text = "SMTP Server";
            // 
            // chkEnabled
            // 
            this.chkEnabled.AutoSize = true;
            this.chkEnabled.Location = new System.Drawing.Point(311, 219);
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.Size = new System.Drawing.Size(76, 17);
            this.chkEnabled.TabIndex = 0;
            this.chkEnabled.Text = "Is Enabled";
            this.chkEnabled.UseVisualStyleBackColor = true;
            // 
            // ddlEmaiAccount
            // 
            this.ddlEmaiAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEmaiAccount.FormattingEnabled = true;
            this.ddlEmaiAccount.Location = new System.Drawing.Point(135, 12);
            this.ddlEmaiAccount.Name = "ddlEmaiAccount";
            this.ddlEmaiAccount.Size = new System.Drawing.Size(201, 21);
            this.ddlEmaiAccount.TabIndex = 7;
            this.ddlEmaiAccount.SelectedIndexChanged += new System.EventHandler(this.ddlEmaiAccount_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "SMTP Account";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(708, 402);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(627, 402);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 14;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnNewAccount
            // 
            this.btnNewAccount.Location = new System.Drawing.Point(523, 402);
            this.btnNewAccount.Name = "btnNewAccount";
            this.btnNewAccount.Size = new System.Drawing.Size(98, 23);
            this.btnNewAccount.TabIndex = 13;
            this.btnNewAccount.Text = "New Account";
            this.btnNewAccount.UseVisualStyleBackColor = true;
            this.btnNewAccount.Click += new System.EventHandler(this.btnNewAccount_Click);
            // 
            // btbDelete
            // 
            this.btbDelete.Location = new System.Drawing.Point(399, 404);
            this.btbDelete.Name = "btbDelete";
            this.btbDelete.Size = new System.Drawing.Size(118, 23);
            this.btbDelete.TabIndex = 12;
            this.btbDelete.Text = "Delete Account";
            this.btbDelete.UseVisualStyleBackColor = true;
            this.btbDelete.Click += new System.EventHandler(this.btbDelete_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(460, -40);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(201, 21);
            this.comboBox1.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(346, -36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Email Account";
            // 
            // lblEmailAccRequired
            // 
            this.lblEmailAccRequired.AutoSize = true;
            this.lblEmailAccRequired.ForeColor = System.Drawing.Color.Red;
            this.lblEmailAccRequired.Location = new System.Drawing.Point(118, 15);
            this.lblEmailAccRequired.Name = "lblEmailAccRequired";
            this.lblEmailAccRequired.Size = new System.Drawing.Size(11, 13);
            this.lblEmailAccRequired.TabIndex = 17;
            this.lblEmailAccRequired.Text = "*";
            this.lblEmailAccRequired.Visible = false;
            // 
            // SmtpSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 435);
            this.Controls.Add(this.lblEmailAccRequired);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.btnNewAccount);
            this.Controls.Add(this.btbDelete);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ddlEmaiAccount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gboxSMTPSettings);
            this.Controls.Add(this.gboxSenderSettings);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SmtpSettings";
            this.Text = "SmtpSettings";
            this.gboxSenderSettings.ResumeLayout(false);
            this.gboxSenderSettings.PerformLayout();
            this.gboxSMTPSettings.ResumeLayout(false);
            this.gboxSMTPSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gboxSenderSettings;
        private System.Windows.Forms.TextBox txtSendToCopy;
        private System.Windows.Forms.Label lblSentCopyTo;
        private System.Windows.Forms.TextBox txtFromName;
        private System.Windows.Forms.Label lblFromName;
        private System.Windows.Forms.TextBox txtReplyToEmail;
        private System.Windows.Forms.Label lblReplyToEmail;
        private System.Windows.Forms.GroupBox gboxSMTPSettings;
        private System.Windows.Forms.CheckBox chkSSL;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.CheckBox chkSMTP_Auth;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.TextBox txtSMTPServer;
        private System.Windows.Forms.Label lblSMTPServer;
        private System.Windows.Forms.CheckBox chkEnabled;
        private System.Windows.Forms.ComboBox ddlEmaiAccount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnNewAccount;
        private System.Windows.Forms.Button btbDelete;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblEmailAccRequired;
        private System.Windows.Forms.Label lblSMTPRequired;
        private System.Windows.Forms.Label lblPortRequired;
        private System.Windows.Forms.Label lblUserNameRequired;
        private System.Windows.Forms.Label lblPasswordRequired;
        private System.Windows.Forms.Label lblReplyToEmailRequired;
        private System.Windows.Forms.Label lblFromNameRequired;
        private System.Windows.Forms.Label lblSendToCopyRequired;
    }
}