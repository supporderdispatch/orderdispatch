﻿using Microsoft.ApplicationBlocks.Data;
using mshtml;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction.EmailSender
{
    public partial class EmailTemplateEditor : Form
    {
        private DataSet Tables;
        private bool ViewOutputInBrowser=false;
        private Guid EmailTemplateId;
        private Guid EmailConditionId;
        private Guid SelectedMailFrom;
        private string MailBody = string.Empty;
        private string MailSubject = string.Empty;
        private string ActiveBrowser = string.Empty;
        private string SampleHtmlForEditors = "<!DOCTYPE HTML><HTML><HEAD><STYLE> body { margin: 0pt; margin-right: 0pt !important; padding: 0pt; } </STYLE></HEAD><BODY></BODY></HTML>";
        private bool EditAsHtml=true;
        public static string ItemsTable = string.Empty;

        public EmailTemplateEditor()
        {
            InitializeComponent();
            PopulateDataSetOfDBFields();
            RegisterEvents();
            BindDropdownLists();
            EmailTemplateId = Guid.NewGuid();
            EmailConditionId = Guid.NewGuid();
            LoadEditorFunctions();
        }

        public EmailTemplateEditor(Guid templateId)
        {
            InitializeComponent();
            PopulateDataSetOfDBFields();
            RegisterEvents();
            InitializeEditorFor(templateId);
            BindDropdownLists();
            LoadEditorFunctions();
        }

        private void LoadEditorFunctions()
        {
            LoadWebBasedEditors();
            LoadFonts();
            LoadFontSizes();

        }

        private void LoadFontSizes()
        {
            for (int x = 1; x <= 7; x++)
            {
                fontSizeComboBox.Items.Add(x.ToString());
            }
            fontSizeComboBox.SelectedIndexChanged += new EventHandler(fontSizeComboBox_TextChanged);
            fontSizeComboBox.SelectedIndex = 2;
        }

        private void fontSizeComboBox_TextChanged(object sender, EventArgs e)
        {
            var activeControl = GetActiveWebControl();
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {                
                var webBrowser=activeControl as WebBrowser;
                webBrowser.Document.ExecCommand("FontSize", false, (Convert.ToInt32((string)fontSizeComboBox.SelectedItem ?? "1") - 1).ToString());
                webBrowser.DocumentText = webBrowser.Document.GetElementsByTagName("HTML")[0].OuterHtml;
                webBrowser.Focus();
            }
        }

        private object GetActiveWebControl()
        {
            Object activeWebControl = null;
            if (ActiveBrowser.Equals(wbMailSubject.Name))
            {
                activeWebControl = wbMailSubject;
            }

            if (ActiveBrowser.Equals(wbMailBody.Name))
            {
                activeWebControl = wbMailBody;
            }
            return activeWebControl;
        }

        private void LoadFonts()
        {
            AutoCompleteStringCollection ac = new AutoCompleteStringCollection();
            foreach (FontFamily fam in FontFamily.Families)
            {
                fontComboBox.Items.Add(fam.Name);
                ac.Add(fam.Name);
            }
            fontComboBox.SelectedIndexChanged += new EventHandler(fontComboBox_TextChanged);
            fontComboBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            fontComboBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            fontComboBox.AutoCompleteCustomSource = ac;
            fontComboBox.SelectedIndex = 3;
        }


        private void fontComboBox_TextChanged(object sender, EventArgs e)
        {
            FontFamily  ff = new FontFamily(fontComboBox.Text);
            var activeControl = GetActiveWebControl();
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                var webBrowser=activeControl as WebBrowser;
                webBrowser.Document.ExecCommand("FontName", false, ff.Name);
                webBrowser.DocumentText = webBrowser.Document.GetElementsByTagName("HTML")[0].OuterHtml;

            }
        }

        private void LoadWebBasedEditors()
        {
            wbMailBody.DocumentText = SampleHtmlForEditors;
            wbMailSubject.DocumentText = SampleHtmlForEditors;

            wbMailBody.DocumentText = SampleHtmlForEditors.Replace("<BODY>","<BODY contentEditable='true'>"+MailBody);
            wbMailSubject.DocumentText = SampleHtmlForEditors.Replace("<BODY>", "<BODY contentEditable='true'>" + MailSubject);

            (wbMailBody.Document.DomDocument as IHTMLDocument2).designMode = "On";
            (wbMailSubject.Document.DomDocument as IHTMLDocument2).designMode = "On";

            wbMailBody.DocumentCompleted += wbMailBody_DocumentCompleted;
           
        }

        void wbMailBody_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            wbMailBody.Document.Body.Click += (s, ev) => 
            {
                ActiveBrowser = wbMailBody.Name;
                wbMailBody.Document.Body.SetAttribute("contentEditable", "true");
                wbMailSubject.Document.Body.SetAttribute("contentEditable", "false");

            };

            wbMailSubject.Document.Body.Click += (s, ev) => 
            {
                ActiveBrowser = wbMailSubject.Name;
                wbMailBody.Document.Body.SetAttribute("contentEditable", "false");
                wbMailSubject.Document.Body.SetAttribute("contentEditable", "true");
            };
        }



        private void InitializeEditorFor(Guid templateId)
        {
            EmailTemplateId = templateId;
            var emailTemplate = ODM.Core.Emails.EmailTemplates.GetInstance().GetEmailTemplate(EmailTemplateId);
            txtTemplateName.Text = emailTemplate.EmailTemplateName;
            txtCC.Text = emailTemplate.Cc;
            txtBCC.Text = emailTemplate.Bcc;
            MailBody = emailTemplate.Body;
            MailSubject = emailTemplate.Subject;
            chkHtml.Checked = emailTemplate.IsHtml;
            chkEnabled.Checked = emailTemplate.IsEnabled;
            chkInvoice.Checked = emailTemplate.WithPdf;
            chkOrderImage.Checked = emailTemplate.WithImage;
            EmailConditionId = emailTemplate.EmailTemplateConditionId;
            SelectedMailFrom = emailTemplate.MailFrom;
        }

        private void BindDropdownLists()
        {
            ddlEmailAccount.DataSource = GlobalVariablesAndMethods.GetEmailAccounts();
            ddlEmailAccount.ValueMember = "ID";
            ddlEmailAccount.DisplayMember = "EmailAccount";
            ddlEmailAccount.SelectedValue = SelectedMailFrom;
        }


        private void RegisterEvents()
        {
            
            lblEditToggle.MouseEnter += MakeLabelBackgroundGrayForButtonAppearance;
            lblInsertDBField.MouseEnter += MakeLabelBackgroundGrayForButtonAppearance;
            lblAddItemsTable.MouseEnter += MakeLabelBackgroundGrayForButtonAppearance;

            lblEditToggle.MouseLeave += ResetLabelBackgroundForButtonAppearance;
            lblInsertDBField.MouseLeave += ResetLabelBackgroundForButtonAppearance;
            lblAddItemsTable.MouseLeave += ResetLabelBackgroundForButtonAppearance;


        }

        private void ResetLabelBackgroundForButtonAppearance(object sender, EventArgs e)
        {
            (sender as Label).BackColor = SystemColors.ControlLight;
        }

        private void MakeLabelBackgroundGrayForButtonAppearance(object sender, EventArgs e)
        {
            ( sender as Label).BackColor = Color.DarkGray;
        }


        private void PopulateDataSetOfDBFields()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            Tables = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDataAndColumnNamesForMenu");
            connection.Close();
        }
     

        private void MenuOfColumnsClick(object sender, EventArgs e)
        {
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                var wbEditor = activeControl as WebBrowser;
                var newElement = wbEditor.Document.CreateElement("span");
                newElement.SetAttribute("type", "dbField");
                newElement.SetAttribute("contentEditable", "false");
                newElement.Style = "background-color:whitesmoke;padding: 1px; margin-top:1px;";
                newElement.InnerText = sender.ToString();
                wbEditor.DocumentText = wbEditor.Document.GetElementsByTagName("HTML")[0].OuterHtml.Replace("</BODY>", newElement.OuterHtml + "&nbsp;" + "</BODY>");
            }
            if (activeControl != null && activeControl.GetType().Equals(typeof(RichTextBox)))
            {
                var intSelection = txtBody.SelectionStart;
                var valToInsert = "<span contentEditable='false' style='background-color:whitesmoke;padding: 1px; margin-top:1px; type='dbField' '>&nbsp;" + sender.ToString() + "</span>";
                txtBody.Text = txtBody.Text.Insert(intSelection, valToInsert);
                txtBody.SelectionStart = intSelection + valToInsert.Length;
            }
        }

        private void SetLabelInActive()
        {
            var g=lblInsertDBField.CreateGraphics();
            g.DrawRectangle(Pens.WhiteSmoke,lblInsertDBField.ClientRectangle);
        }

        private void lblInsertDBField_Click(object sender, EventArgs e)
        {
            SetLabelActive();
            ContextMenuStrip MnuStripItemMain = new ContextMenuStrip();
            if (Tables != null && Tables.Tables.Count > 0)
            {
                for (int i = 0; i < Tables.Tables.Count; i++)
                {
                    DataTable objDt = new DataTable();
                    objDt = Tables.Tables[i];
                    string strTableName = Convert.ToString(objDt.Rows[i]["Table_Name"]);

                    ToolStripMenuItem MenuTable = new ToolStripMenuItem(strTableName);
                    MnuStripItemMain.Items.Add(MenuTable);
                    foreach (DataRow dr in objDt.Rows)
                    {
                        ToolStripMenuItem MenuColumnName = new ToolStripMenuItem(dr["Column_Name"].ToString(), null, MenuOfColumnsClick);
                        MenuTable.DropDownItems.Add(MenuColumnName);
                    }
                }
            }
            MnuStripItemMain.Show(this,lblInsertDBField.Location);
            SetLabelInActive();
        }

        private void SetLabelActive()
        {
            var g = lblInsertDBField.CreateGraphics();
            g.DrawRectangle(Pens.Black, lblInsertDBField.ClientRectangle);
        }

        private void lblViewOutput_Click(object sender, EventArgs e)
        {
            
        }

        private void btnSaveAndExit_Click(object sender, EventArgs e)
        {
            SaveTemplate();
            this.Close();
        }

        private bool SaveTemplate()
        {
            if (string.IsNullOrEmpty(txtTemplateName.Text))
            {
                MessageBox.Show("Please Enter Template Name");
                return false;
            }

            var emailTemplate = new ODM.Entities.Emails.EmailTemplate();
            emailTemplate.IsEnabled = chkEnabled.Checked;
            emailTemplate.IsHtml = chkHtml.Checked;
            emailTemplate.EmailTemplateId = EmailTemplateId;
            emailTemplate.EmailTemplateConditionId = EmailConditionId;
            emailTemplate.EmailTemplateName = txtTemplateName.Text;
            emailTemplate.WithImage = chkOrderImage.Checked;
            emailTemplate.WithPdf = chkInvoice.Checked;
            emailTemplate.Cc = txtCC.Text;
            emailTemplate.Bcc = txtBCC.Text;
            emailTemplate.Subject = GetMailSubject();
            emailTemplate.Body = GetMailBody();
            emailTemplate.MailFrom = Guid.Parse(ddlEmailAccount.SelectedValue.ToString());

            ODM.Core.Emails.EmailTemplates.GetInstance().SaveEmailTemplate(emailTemplate);
            return true;
        }

        private string GetMailBody()
        {
            var mailBody = string.Empty;
            if (wbMailBody.Document != null && wbMailBody.Document.Body != null)
            {
                mailBody = wbMailBody.Document.Body.InnerHtml ?? string.Empty;
            }

            return mailBody;
        }

        private string GetMailSubject()
        {
            var mailSubject = string.Empty;
            if (wbMailSubject.Document != null && wbMailSubject.Document.Body != null)
            {
                mailSubject = wbMailSubject.Document.Body.InnerHtml ?? string.Empty;
            }

            return mailSubject;
        }

        private void btnExitWithoutSave_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEmailConditions_Click(object sender, EventArgs e)
        {
            if (SaveTemplate())
            {
                if (EmailConditionId == null || EmailConditionId.Equals(Guid.Empty))
                {
                    MessageBox.Show("Please save template first");
                    return;
                }
                else
                {
                    var emailConditions = new EmailConditions(EmailConditionId);
                    emailConditions.Show();
                }
            }           
        }

        private void btnBold_Click(object sender, EventArgs e)
        {
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                (activeControl as WebBrowser).Document.ExecCommand("Bold", false, null);
            }
        }

        private void btnItalic_Click(object sender, EventArgs e)
        {
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                (activeControl as WebBrowser).Document.ExecCommand("Italic", false, null);
            }
        }

        private void btnUnderLine_Click(object sender, EventArgs e)
        {
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                (activeControl as WebBrowser).Document.ExecCommand("Underline", false, null);
            }
        }

        private void btnIndent_Click(object sender, EventArgs e)
        {
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                (activeControl as WebBrowser).Document.ExecCommand("Indent", false, null);
            }
        }

        private void btnOutdent_Click(object sender, EventArgs e)
        {
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                (activeControl as WebBrowser).Document.ExecCommand("Outdent", false, null);
            }
        }

        private void btnJustifyLeft_Click(object sender, EventArgs e)
        {
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                (activeControl as WebBrowser).Document.ExecCommand("JustifyLeft", false, null);
            }
        }

        private void btnJustifyCenter_Click(object sender, EventArgs e)
        {
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                (activeControl as WebBrowser).Document.ExecCommand("JustifyCenter", false, null);
            }
        }

        private void btnJustifyFull_Click(object sender, EventArgs e)
        {
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                (activeControl as WebBrowser).Document.ExecCommand("JustifyFull", false, null);
            }
        }

        private void btnJustifyRight_Click(object sender, EventArgs e)
        {
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                (activeControl as WebBrowser).Document.ExecCommand("JustifyRight", false, null);
            }
        }

        private void lblEditToggle_Click(object sender, EventArgs e)
        {
            EditAsHtml = EditAsHtml ? false : true;

            if(EditAsHtml)
            {
                wbMailBody.Visible = true;
                lblEditToggle.Text = "Edit as Html";
                txtBody.Visible = false;
                wbMailBody.DocumentText = txtBody.Text;
            }
            else
            {
                wbMailBody.Visible = false;
                lblEditToggle.Text = "Edit as Text";
                txtBody.Visible = true;
                txtBody.Text = wbMailBody.Document.GetElementsByTagName("HTML")[0].OuterHtml;
            }
        }

        private void btnTextForeColor_Click(object sender, EventArgs e)
        {
            Color color;
            var colorDlg = new ColorDialog();
            colorDlg.ShowDialog();
            color = colorDlg.Color;
            string colorstr =
                    string.Format("#{0:X2}{1:X2}{2:X2}", color.R, color.G, color.B);
            //webBrowser1.Document.ExecCommand("ForeColor", false, colorstr);
            var activeControl = this.ActiveControl;
            if (activeControl != null && activeControl.GetType().Equals(typeof(WebBrowser)))
            {
                (activeControl as WebBrowser).Document.ExecCommand("ForeColor", false, colorstr);
            }
        }

        private void lblAddItemsTable_Click(object sender, EventArgs e)
        {
            var addItemsToTablePrompt = new AddItemsIntoItemTable();
            if (addItemsToTablePrompt.ShowDialog() != null)
            {
                if (!string.IsNullOrEmpty(ItemsTable))
                {
                    wbMailBody.DocumentText = wbMailBody.Document.GetElementsByTagName("HTML")[0].OuterHtml.Replace("</BODY>", ItemsTable + "</BODY>");
                    ItemsTable = string.Empty;
                }
            }
        }

    }
}
