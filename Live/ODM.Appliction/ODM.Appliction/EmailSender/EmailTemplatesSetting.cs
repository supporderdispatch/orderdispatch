﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction.EmailSender
{
    public partial class EmailTemplatesSetting : Form
    {
        public EmailTemplatesSetting()
        {
            InitializeComponent();
            GetAndBindEmailTemplates();
            RenderGridPhysicalStyle();
            RegisterEvents();
        }

        private void RegisterEvents()
        {
            grdEmailTemplates.CellContentClick+=grdEmailTemplates_CellContentClick;
        }

        private void grdEmailTemplates_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                var row = senderGrid.Rows[e.RowIndex];
                var buttonText = row.Cells[e.ColumnIndex].Value.ToString();
                var templateId = row.Cells["EmailTemplateId"].Value.ToString();
                var templateName = row.Cells["EmailTemplateName"].Value.ToString();
                switch (buttonText)
                {
                    case "Edit":
                        EditTemplate(templateId);
                        break;

                    case "Delete":
                        DeleteTemplate(templateId, templateName);
                        break;

                    case "Copy":
                        CopyTemplate(templateId);
                        break;

                    default:
                        break;
                }
            }
        }

        private void CopyTemplate(string templateId)
        {
            ODM.Core.Emails.EmailTemplates.GetInstance().CopyTemplate(templateId);
            btnRefresh.PerformClick();
        }

        private void DeleteTemplate(string templateId, string templateName)
        {
            DialogResult result=MessageBox.Show("Do You Want To Delete \" "+templateName+" "+"\"?","Delete Email Template",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                ODM.Core.Emails.EmailTemplates.GetInstance().DeleteEmailTemplate(Guid.Parse(templateId));
                btnRefresh.PerformClick();
            }
        }

        private void EditTemplate(string templateId)
        {
            Cursor = Cursors.WaitCursor;

            var emailTemplateEditorForm = new EmailTemplateEditor(Guid.Parse(templateId));
            var dialogResult = emailTemplateEditorForm.ShowDialog();
            if (dialogResult != null)
            {
                btnRefresh.PerformClick();
                Cursor = Cursors.Default;
            }
        }

        private void GetAndBindEmailTemplates()
        {
            grdEmailTemplates.DataSource = ODM.Core.Emails.EmailTemplates.GetInstance().GetAllEmailTemplates();
        }

        private void RenderGridPhysicalStyle()
        {
            //set grid physical style :)
            grdEmailTemplates.Columns["EmailTemplateName"].MinimumWidth = 250;

            grdEmailTemplates.DoubleBuffered(true);
            grdEmailTemplates.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdEmailTemplates.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdEmailTemplates.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdEmailTemplates.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdEmailTemplates.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdEmailTemplates.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdEmailTemplates.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdEmailTemplates.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdEmailTemplates.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdEmailTemplates.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdEmailTemplates.ScrollBars = ScrollBars.Both;
            grdEmailTemplates.RowHeadersVisible = false;
            grdEmailTemplates.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdEmailTemplates.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdEmailTemplates.Columns["EmailTemplateName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            grdEmailTemplates.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdEmailTemplates.EnableHeadersVisualStyles = false;
            grdEmailTemplates.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdEmailTemplates.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdEmailTemplates.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdEmailTemplates.DefaultCellStyle.Padding = new Padding(3, 3, 3, 3);
            grdEmailTemplates.AllowUserToAddRows = false;

            var buttonsToadd = new List<string>() 
            {
                "Edit",
                "Copy",
                "Delete"
            };

            foreach (var button in buttonsToadd)
            {
                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                btn.HeaderText = string.Empty;
                btn.Name = "btn" + button + "EmailTemplate";
                btn.Text = button;
                btn.UseColumnTextForButtonValue = true;
                btn.Width = 30;
                btn.FlatStyle = FlatStyle.Flat;
                grdEmailTemplates.Columns.Add(btn);
            }

            grdEmailTemplates.Columns["EmailTemplateId"].Visible = false;
            grdEmailTemplates.Columns["EmailTemplateConditionId"].Visible = false;
            grdEmailTemplates.Columns["EmailTemplateName"].HeaderText = "Name";
            grdEmailTemplates.Columns["EmailTemplateName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            grdEmailTemplates.Columns["IsEnabled"].HeaderText = "Enabled";
            grdEmailTemplates.Columns["IsHtml"].HeaderText = "Html";
            grdEmailTemplates.Columns["WithImage"].HeaderText = "Img Attachment";
            grdEmailTemplates.Columns["WithPdf"].HeaderText = "Pdf Attachment";
    
            grdEmailTemplates.Columns["EmailTemplateName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        private void btnAddEmailTemplate_Click(object sender, EventArgs e)
        {
            var emailTemplateEditor = Application.OpenForms["EmailTemplateEditor"];
            if (emailTemplateEditor == null)
            {
                var templateEditor = new EmailSender.EmailTemplateEditor();
                var result = templateEditor.ShowDialog();
                if (result != null)
                {
                    btnRefresh.PerformClick();
                }
            }
            else
            {
                emailTemplateEditor.Focus();
            }            
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            GetAndBindEmailTemplates();
        }

    }
}
