﻿namespace ODM.Appliction.EmailSender
{
    partial class EmailConditions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailConditions));
            this.grdCriteria = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAddCondition = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.chkMatchAll = new System.Windows.Forms.CheckBox();
            this.chkMatchAny = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grdCriteria)).BeginInit();
            this.SuspendLayout();
            // 
            // grdCriteria
            // 
            this.grdCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdCriteria.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grdCriteria.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdCriteria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCriteria.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdCriteria.Location = new System.Drawing.Point(12, 79);
            this.grdCriteria.Name = "grdCriteria";
            this.grdCriteria.Size = new System.Drawing.Size(661, 257);
            this.grdCriteria.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(168, 37);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(150, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSavePrintConditions_Click);
            // 
            // btnAddCondition
            // 
            this.btnAddCondition.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCondition.Location = new System.Drawing.Point(12, 37);
            this.btnAddCondition.Name = "btnAddCondition";
            this.btnAddCondition.Size = new System.Drawing.Size(150, 23);
            this.btnAddCondition.TabIndex = 4;
            this.btnAddCondition.Text = "Add Criteria";
            this.btnAddCondition.UseVisualStyleBackColor = true;
            this.btnAddCondition.Click += new System.EventHandler(this.btnAddCondition_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(456, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Validate condition if match";
            // 
            // chkMatchAll
            // 
            this.chkMatchAll.AutoSize = true;
            this.chkMatchAll.Checked = true;
            this.chkMatchAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMatchAll.Location = new System.Drawing.Point(459, 43);
            this.chkMatchAll.Name = "chkMatchAll";
            this.chkMatchAll.Size = new System.Drawing.Size(37, 17);
            this.chkMatchAll.TabIndex = 6;
            this.chkMatchAll.Text = "All";
            this.chkMatchAll.UseVisualStyleBackColor = true;
            // 
            // chkMatchAny
            // 
            this.chkMatchAny.AutoSize = true;
            this.chkMatchAny.Location = new System.Drawing.Point(513, 43);
            this.chkMatchAny.Name = "chkMatchAny";
            this.chkMatchAny.Size = new System.Drawing.Size(44, 17);
            this.chkMatchAny.TabIndex = 7;
            this.chkMatchAny.Text = "Any";
            this.chkMatchAny.UseVisualStyleBackColor = true;
            // 
            // EmailConditions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 348);
            this.Controls.Add(this.chkMatchAny);
            this.Controls.Add(this.chkMatchAll);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAddCondition);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.grdCriteria);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmailConditions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Email Conditions";
            ((System.ComponentModel.ISupportInitialize)(this.grdCriteria)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdCriteria;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAddCondition;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkMatchAll;
        private System.Windows.Forms.CheckBox chkMatchAny;
    }
}