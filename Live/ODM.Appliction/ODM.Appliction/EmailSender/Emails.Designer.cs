﻿namespace ODM.Appliction.EmailSender
{
    partial class Emails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Emails));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolstripButtonRotate = new System.Windows.Forms.ToolStripDropDownButton();
            this.templateSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smtpSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSentMails = new System.Windows.Forms.ToolStripLabel();
            this.grdPendingEmails = new System.Windows.Forms.DataGridView();
            this.lblPendingEmails = new System.Windows.Forms.Label();
            this.btnSendSelected = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnDeleteSelected = new System.Windows.Forms.Button();
            this.lblSelectedRows = new System.Windows.Forms.Label();
            this.picLoading = new System.Windows.Forms.PictureBox();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPendingEmails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolstripButtonRotate,
            this.btnSentMails});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(865, 25);
            this.toolStrip1.TabIndex = 26;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolstripButtonRotate
            // 
            this.toolstripButtonRotate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.templateSettingToolStripMenuItem,
            this.smtpSettingsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.toolstripButtonRotate.Image = ((System.Drawing.Image)(resources.GetObject("toolstripButtonRotate.Image")));
            this.toolstripButtonRotate.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolstripButtonRotate.ImageTransparentColor = System.Drawing.Color.White;
            this.toolstripButtonRotate.Name = "toolstripButtonRotate";
            this.toolstripButtonRotate.Size = new System.Drawing.Size(110, 22);
            this.toolstripButtonRotate.Text = "Email Settings";
            // 
            // templateSettingToolStripMenuItem
            // 
            this.templateSettingToolStripMenuItem.Name = "templateSettingToolStripMenuItem";
            this.templateSettingToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.templateSettingToolStripMenuItem.Text = "Template Setting";
            this.templateSettingToolStripMenuItem.Click += new System.EventHandler(this.templateSettingToolStripMenuItem_Click);
            // 
            // smtpSettingsToolStripMenuItem
            // 
            this.smtpSettingsToolStripMenuItem.Name = "smtpSettingsToolStripMenuItem";
            this.smtpSettingsToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.smtpSettingsToolStripMenuItem.Text = "Smtp Settings";
            this.smtpSettingsToolStripMenuItem.Click += new System.EventHandler(this.smtpSettingsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // btnSentMails
            // 
            this.btnSentMails.Name = "btnSentMails";
            this.btnSentMails.Size = new System.Drawing.Size(61, 22);
            this.btnSentMails.Text = "Sent Mails";
            this.btnSentMails.Click += new System.EventHandler(this.btnSentMails_Click);
            // 
            // grdPendingEmails
            // 
            this.grdPendingEmails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdPendingEmails.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grdPendingEmails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdPendingEmails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPendingEmails.Location = new System.Drawing.Point(12, 102);
            this.grdPendingEmails.Name = "grdPendingEmails";
            this.grdPendingEmails.Size = new System.Drawing.Size(841, 254);
            this.grdPendingEmails.TabIndex = 27;
            // 
            // lblPendingEmails
            // 
            this.lblPendingEmails.AutoSize = true;
            this.lblPendingEmails.Location = new System.Drawing.Point(12, 83);
            this.lblPendingEmails.Name = "lblPendingEmails";
            this.lblPendingEmails.Size = new System.Drawing.Size(79, 13);
            this.lblPendingEmails.TabIndex = 28;
            this.lblPendingEmails.Text = "Pending Emails";
            // 
            // btnSendSelected
            // 
            this.btnSendSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendSelected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendSelected.Location = new System.Drawing.Point(627, 66);
            this.btnSendSelected.Name = "btnSendSelected";
            this.btnSendSelected.Size = new System.Drawing.Size(110, 23);
            this.btnSendSelected.TabIndex = 32;
            this.btnSendSelected.Text = "Send Selected";
            this.btnSendSelected.UseVisualStyleBackColor = true;
            this.btnSendSelected.Click += new System.EventHandler(this.btnSendSelected_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Location = new System.Drawing.Point(546, 66);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 31;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnDeleteSelected
            // 
            this.btnDeleteSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteSelected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteSelected.Location = new System.Drawing.Point(743, 66);
            this.btnDeleteSelected.Name = "btnDeleteSelected";
            this.btnDeleteSelected.Size = new System.Drawing.Size(111, 23);
            this.btnDeleteSelected.TabIndex = 30;
            this.btnDeleteSelected.Text = "Delete Selected";
            this.btnDeleteSelected.UseVisualStyleBackColor = true;
            this.btnDeleteSelected.Click += new System.EventHandler(this.btnDeleteSelected_Click);
            // 
            // lblSelectedRows
            // 
            this.lblSelectedRows.AutoSize = true;
            this.lblSelectedRows.Location = new System.Drawing.Point(228, 86);
            this.lblSelectedRows.Name = "lblSelectedRows";
            this.lblSelectedRows.Size = new System.Drawing.Size(79, 13);
            this.lblSelectedRows.TabIndex = 33;
            this.lblSelectedRows.Text = "Selected 0 of 0";
            // 
            // picLoading
            // 
            this.picLoading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picLoading.Image = ((System.Drawing.Image)(resources.GetObject("picLoading.Image")));
            this.picLoading.InitialImage = ((System.Drawing.Image)(resources.GetObject("picLoading.InitialImage")));
            this.picLoading.Location = new System.Drawing.Point(513, 70);
            this.picLoading.Name = "picLoading";
            this.picLoading.Size = new System.Drawing.Size(25, 24);
            this.picLoading.TabIndex = 34;
            this.picLoading.TabStop = false;
            this.picLoading.Visible = false;
            // 
            // Emails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 368);
            this.Controls.Add(this.picLoading);
            this.Controls.Add(this.lblSelectedRows);
            this.Controls.Add(this.btnSendSelected);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnDeleteSelected);
            this.Controls.Add(this.lblPendingEmails);
            this.Controls.Add(this.grdPendingEmails);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Emails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Emails";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPendingEmails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolstripButtonRotate;
        private System.Windows.Forms.ToolStripMenuItem templateSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel btnSentMails;
        private System.Windows.Forms.DataGridView grdPendingEmails;
        private System.Windows.Forms.Label lblPendingEmails;
        private System.Windows.Forms.Button btnSendSelected;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnDeleteSelected;
        private System.Windows.Forms.ToolStripMenuItem smtpSettingsToolStripMenuItem;
        private System.Windows.Forms.Label lblSelectedRows;
        private System.Windows.Forms.PictureBox picLoading;
    }
}