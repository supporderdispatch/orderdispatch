﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction.EmailSender
{
    public partial class Emails : Form
    {
        public Emails()
        {
            InitializeComponent();
            LoadPendingMails();
            RegisterEvents();
            CheckForIllegalCrossThreadCalls = false;
        }

        private void RegisterEvents()
        {
            grdPendingEmails.MouseClick += grdPendingEmails_MouseClick;
            grdPendingEmails.MouseMove += grdPendingEmails_MouseMove;

            btnSentMails.MouseEnter += btnSentMails_MouseEnter;
            btnSentMails.MouseLeave += btnSentMails_MouseLeave;
        }

        void btnSentMails_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        void btnSentMails_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }


        void grdPendingEmails_MouseMove(object sender, MouseEventArgs e)
        {
            UpdateSelectedRowStatus();
        }

        void grdPendingEmails_MouseClick(object sender, MouseEventArgs e)
        {
            UpdateSelectedRowStatus();
        }

        private void LoadPendingMails()
        {
            GetPendingMailsAndBindWithGrid();
            SetGridPhysicalStyle();
        }
        private void UpdateSelectedRowStatus()
        {
            lblSelectedRows.Text = "Selected : " + grdPendingEmails.SelectedRows.Count + " of " + grdPendingEmails.Rows.Count;
        }
        private void SetGridPhysicalStyle()
        {
            grdPendingEmails.DoubleBuffered(true);
            grdPendingEmails.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdPendingEmails.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdPendingEmails.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdPendingEmails.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdPendingEmails.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdPendingEmails.ScrollBars = ScrollBars.Both;
            grdPendingEmails.RowHeadersVisible = false;
            grdPendingEmails.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            grdPendingEmails.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdPendingEmails.EnableHeadersVisualStyles = false;
            grdPendingEmails.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdPendingEmails.DefaultCellStyle.Padding = new Padding(1, 1, 1, 1);
            grdPendingEmails.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            grdPendingEmails.AllowUserToAddRows = false;

            grdPendingEmails.Columns["SentMailBody"].Visible = false;
            grdPendingEmails.Columns["IsMailSent"].Visible = false;
            grdPendingEmails.Columns["OrderId"].Visible = false;
            grdPendingEmails.Columns["EmailTemplateId"].Visible = false;
            grdPendingEmails.Columns["MailSentOn"].Visible = false;
            grdPendingEmails.Columns["IsProcessed"].Visible = false; 
            grdPendingEmails.Columns["NumOrderId"].HeaderText = "Order Number";
            grdPendingEmails.Columns["NumOrderId"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdPendingEmails.EditMode = DataGridViewEditMode.EditProgrammatically;
            grdPendingEmails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            grdPendingEmails.ClearSelection();
            UpdateSelectedRowStatus();
        }

        private void GetPendingMailsAndBindWithGrid()
        {
            var pendingMails = ODM.Core.Emails.Mails.GetInstance().GetAllPendingMails();
            foreach (DataRow row in pendingMails.Rows)
            {
                var createdOn = row["CreatedOn"].ToString();
                row["CreatedOn"] = ODM.Core.Helpers.Conversions.GetLocalDateFromUtc(createdOn);
            }
            pendingMails.AcceptChanges();
            grdPendingEmails.DataSource = pendingMails;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void templateSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmEmailTemplatesSetting = Application.OpenForms["EmailTemplatesSetting"];
            if (frmEmailTemplatesSetting == null)
            {
                var emailTemplatesSetting = new EmailSender.EmailTemplatesSetting();
                emailTemplatesSetting.Show();
            }
            else
            {
                frmEmailTemplatesSetting.Focus();
            }
        }

        private void smtpSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var smtpSettings = Application.OpenForms["SmtpSettings"];
            if (smtpSettings == null)
            {
                var smtpSettingsForm = new SmtpSettings();
                smtpSettingsForm.Show();
            }
            else
            {
                smtpSettings.Focus();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            GetPendingMailsAndBindWithGrid();
        }

        private void btnDeleteSelected_Click(object sender, EventArgs e)
        {
            var orderIdsSelected = string.Empty;
            foreach (DataGridViewRow row in grdPendingEmails.SelectedRows)
            {
                orderIdsSelected += row.Cells["OrderId"].Value.ToString() + ",";
            }
            ODM.Core.Emails.Mails.GetInstance().DeleteMails(orderIdsSelected);
            btnRefresh.PerformClick();
        }

        private async void btnSendSelected_Click(object sender, EventArgs e)
        {

            picLoading.Visible = true;

            await Task.Run(() =>
            {
                foreach (DataGridViewRow row in grdPendingEmails.SelectedRows)
                {
                    ODM.Data.Util.GlobalVariablesAndMethods.OrderIdToLog = (Guid)row.Cells["OrderId"].Value;
                    try
                    {
                        var email = new ODM.Entities.Emails.Email();
                        email.OrderId = (Guid)row.Cells["OrderId"].Value;
                        email.NumOrderId = row.Cells["NumOrderId"].Value.ToString();
                        email.MailToAddress = row.Cells["MailToAddress"].Value.ToString();
                        email.SentMailBody = row.Cells["SentMailBody"].Value.ToString();
                        email.EmailTemplateId = (Guid)row.Cells["EmailTemplateId"].Value;
                        email.CreatedOn = Convert.ToDateTime(row.Cells["CreatedOn"].Value.ToString());
                        email.SentSubject = row.Cells["SentSubject"].Value.ToString();
                        ODM.Core.Emails.Mails.GetInstance().SendMail(email);
                        ODM.Data.Util.GlobalVariablesAndMethods.LogHistory("Mail sent for order : " + email.NumOrderId);
                    }
                    catch (Exception ex)
                    {
                        ODM.Data.Util.GlobalVariablesAndMethods.LogHistory(ex.Message);
                    }
                }
            }).ContinueWith((result) => 
            {
                picLoading.Visible = false;
                LoadPendingMails();
            });
        }

        private void btnSentMails_Click(object sender, EventArgs e)
        {
            var sentMails = Application.OpenForms["SentMails"];
            if (sentMails != null)
            {
                sentMails.Close();
            }
            var sentMailsForm = new SentMails();
            sentMailsForm.Show();
        }
    }
}
