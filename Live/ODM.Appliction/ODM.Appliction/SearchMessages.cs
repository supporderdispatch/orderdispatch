﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class SearchMessages : Form
    {
        private int _intTotalMessages = 0, _intReadMessages = 0, _intUnreadMessages = 0;
        private int _intPageSize = 0;
        private int _intPageCount = 0;
        private int _intCurrentPage = 1;
        int _PageSize = 50;
        MessageFolders objMessageFolders = new MessageFolders("GET");
        public SearchMessages()
        {
            InitializeComponent();

        }
        private void btnSearchMessages_Click(object sender, EventArgs e)
        {
            // FilterMessageGridOnTextSearch();
            if (chkMessageBody.Checked == true || chkMessageSubject.Checked == true || chkEmailAddress.Checked == true)
            {
                if (!string.IsNullOrEmpty(txtSearch.Text.TrimEnd()))
                {
                    lblRequiredError.Visible = false;
                    fillGrid();

                }
                else
                {
                    lblRequiredError.Visible = true;
                }

            }
            else
            {
                lblRequiredError.Visible = true;
            }
        }
        private void PopulateControls(bool blIsEnabled)
        {
            btnFirst.Visible = blIsEnabled;
            btnLast.Visible = blIsEnabled;
            btnNext.Visible = blIsEnabled;
            btnPrevious.Visible = blIsEnabled;
            lblStatus.Visible = blIsEnabled;
            pnlPaging.Visible = blIsEnabled;
            chkShowAllMessages.Checked = blIsEnabled;
            chkShowAllMessages.Visible = blIsEnabled;
            chkShowReadMessages.Visible = blIsEnabled;
            lblPageRecords.Visible = blIsEnabled;
            lblPageRecordsValue.Visible = blIsEnabled;
        }
        private void FilterMessageGridOnReadUnRead(ref DataGridView grdMessageList, bool IsRead)
        {
            if (grdMessageList.DataSource != null)
            {
                if (chkShowAllMessages.Checked == false)
                {
                    (grdMessageList.DataSource as DataTable).DefaultView.RowFilter = string.Format("IsRead = {0}", IsRead);
                    lblPageRecordsValue.Text = grdMessageList.Rows.Count.ToString();
                    objMessageFolders.MarkReadUnreadSentMessages(ref grdMessageList);
                }
                if (chkShowAllMessages.Checked == true)
                {
                    (grdMessageList.DataSource as DataTable).DefaultView.RowFilter = null;
                    objMessageFolders.MarkReadUnreadSentMessages(ref grdMessageList);
                    lblPageRecordsValue.Text = grdMessageList.Rows.Count.ToString();
                }
            }
        }


        //else
        //{
        //    fillGrid();
        //}



        public void fillGrid()
        {
            // For Page view.
            this._intPageSize = this._PageSize;
            //this.mintTotalRecords = getCount();
            //if (chkShowReadMessages.Checked == true)
            //{
            //    this._intPageCount = this._intReadMessages / this._intPageSize;
            //    if (this._intReadMessages % this._intPageSize > 0)
            //        this._intPageCount++;
            //}
            //else
            //{
            //    this._intPageCount = this._intUnreadMessages / this._intPageSize;
            //    if (this._intUnreadMessages % this._intPageSize > 0)
            //        this._intPageCount++;
            //}


            // Adjust page count if the last page contains partial page.


            this._intCurrentPage = 0;

            LoadPage();

        }
        private void GetTotalMessages(int intSkip)
        {

            DataTable objDt = new DataTable();
            objDt = GetMessagesBasedOnSearch("TOTAL", _intCurrentPage, _intPageSize, intSkip);
            int Length = Convert.ToInt32(objDt.Rows[0]["TotalMessages"].ToString());
            this._intPageCount = Length / this._intPageSize;
            if (Length % this._intPageSize > 0)
                this._intPageCount++;
            lblTotalRecordsValue.Text = Length.ToString();

        }
        public void BindGridAndPopulateControls(int intCurrentPage, int intPageSize, int intSkip)
        {
            DataTable objDt = new DataTable();
            objDt = GetMessagesBasedOnSearch("SEARCH", _intCurrentPage, _intPageSize, intSkip);
            GetTotalMessages(intSkip);
            if (objDt.Rows.Count > 0)
            {
                grdMessageList.Show();
                PopulateControls(true);
                lblNoRecordError.Visible = false;
                grdMessageList.EditMode = DataGridViewEditMode.EditProgrammatically;
                grdMessageList.AllowUserToAddRows = false;
                grdMessageList.DataSource = objDt;
                grdMessageList = objMessageFolders.PopulateColumns(ref grdMessageList);
                //grdMessageList = objMessageFolders.PopulateColumns();
            }
            else
            {
                lblNoRecordError.Visible = true;
                grdMessageList.Hide();
                PopulateControls(false);
            }

        }
        public void LoadPage()
        {
            int intSkip = 0;
            intSkip = (this._intCurrentPage * this._intPageSize);
            BindGridAndPopulateControls(_intCurrentPage, _intPageSize, intSkip);
            lblPageRecordsValue.Text = grdMessageList.Rows.Count.ToString();
            // Show Status
            this.lblStatus.Text = (this._intCurrentPage + 1).ToString() +
              " / " + this._intPageCount.ToString();

        }
        private DataTable GetMessagesBasedOnSearch(string strMode, int intCurrentPage, int intPageSize, int intSkip)
        {
            DataTable objDt = new DataTable();
            DataSet objDs = new DataSet();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[7];
                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objParam[1] = new SqlParameter("@SearchText", SqlDbType.VarChar, txtSearch.Text.Length);
                objParam[1].Value = txtSearch.Text;

                objParam[2] = new SqlParameter("@IsMessageBody", SqlDbType.Bit);
                objParam[2].Value = chkMessageBody.Checked;

                objParam[3] = new SqlParameter("@IsSubject", SqlDbType.Bit);
                objParam[3].Value = chkMessageSubject.Checked;

                objParam[4] = new SqlParameter("@IsEmail", SqlDbType.Bit);
                objParam[4].Value = chkEmailAddress.Checked;

                objParam[5] = new SqlParameter("@PageSize", SqlDbType.VarChar, 10);
                objParam[5].Value = intPageSize.ToString();

                objParam[6] = new SqlParameter("@Skip", SqlDbType.Int);
                objParam[6].Value = intSkip;

                objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetMessagesBasedOnSearch", objParam);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    objDt = objDs.Tables[0];

                }
                return objDt;

            }
            catch (Exception)
            {

                throw;
            }
            finally { conn.Close(); }
        }
        public void goFirst()
        {
            this._intCurrentPage = 0;

            LoadPage();
        }

        private void goPrevious()
        {
            if (this._intCurrentPage == this._intPageCount)
                this._intCurrentPage = this._intPageCount - 1;

            this._intCurrentPage--;

            if (this._intCurrentPage < 1)
                this._intCurrentPage = 0;

            LoadPage();
        }

        private void goNext()
        {
            this._intCurrentPage++;

            if (this._intCurrentPage > (this._intPageCount - 1))
                this._intCurrentPage = this._intPageCount - 1;

            LoadPage();
        }

        private void goLast()
        {
            this._intCurrentPage = this._intPageCount - 1;

            LoadPage();
        }
        private void btnFirst_Click(object sender, EventArgs e)
        {
            goFirst();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            goPrevious();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            goNext();
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            goLast();
        }

        private void chkShowReadMessages_CheckedChanged(object sender, EventArgs e)
        {
            if (chkShowReadMessages.Checked == true)
            {
                chkShowAllMessages.Checked = false;
                FilterMessageGridOnReadUnRead(ref grdMessageList, chkShowReadMessages.Checked);
            }
            if (chkShowReadMessages.Checked == false && chkShowAllMessages.Checked == false)
            {
                FilterMessageGridOnReadUnRead(ref grdMessageList, chkShowReadMessages.Checked);
            }

        }

        private void chkShowAllMessages_CheckedChanged(object sender, EventArgs e)
        {
            if (chkShowAllMessages.Checked == false && chkShowReadMessages.Checked == false)
            {
                FilterMessageGridOnReadUnRead(ref grdMessageList, chkShowReadMessages.Checked);
            }
            if (chkShowAllMessages.Checked == true)
            {
                chkShowReadMessages.Checked = false;
                FilterMessageGridOnReadUnRead(ref grdMessageList, chkShowAllMessages.Checked);
            }

        }

        private void grdMessageList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Form frmMessageBody = Application.OpenForms["frmMessageBody"];
            if (frmMessageBody == null)
            {
                string[] strArrGrdSelectedValues = new string[]
                {
                    grdMessageList.Rows[e.RowIndex].Cells["MessageUID"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["CustomerEmail"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["MessageType"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["Subject"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["MessageBody"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["IsAttachment"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["IsOpen"].Value.ToString(),
                   // grdMessageList.Rows[e.RowIndex].Cells["IsProcessed"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["CreatedOn"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["AccountID"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["FolderID"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["ExtMessageID"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["ItemID"].Value.ToString(),
                      grdMessageList.Rows[e.RowIndex].Cells["EbayUserID"].Value.ToString(),
                      grdMessageList.Rows[e.RowIndex].Cells["IsReplySent"].Value.ToString(),
                      grdMessageList.Rows[e.RowIndex].Cells["SentMessageBody"].Value.ToString()
                };

                if (grdMessageList.Rows[e.RowIndex].Cells["IsOpen"].Value.ToString().ToUpper() == "FALSE")
                {
                    objMessageFolders.UpdateIsOpenandReadStatus("N", strArrGrdSelectedValues[0], new Guid(strArrGrdSelectedValues[9]), new Guid(strArrGrdSelectedValues[8]));
                    MessageBody objMessageBody = new MessageBody(strArrGrdSelectedValues);
                    objMessageBody.ShowDialog();
                    objMessageFolders.UpdateIsOpenandReadStatus("Y", strArrGrdSelectedValues[0], new Guid(strArrGrdSelectedValues[9]), new Guid(strArrGrdSelectedValues[8]));


                }
                else
                {
                    MessageBox.Show("Already Opened by other user", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //BindGridForMessageList(grdMessageFolders.SelectedCells[0].Value.ToString(), grdMessageFolders.CurrentRow.Cells["ID"].Value.ToString());
                    //fillGrid();
                }
                fillGrid();
            }
        }
    }
}
