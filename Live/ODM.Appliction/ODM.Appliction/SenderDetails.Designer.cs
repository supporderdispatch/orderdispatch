﻿namespace ODM.Appliction
{
    partial class SenderDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SenderDetails));
            this.lblSenderName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.lblAddress1 = new System.Windows.Forms.Label();
            this.txtTown = new System.Windows.Forms.TextBox();
            this.lblTown = new System.Windows.Forms.Label();
            this.lblCounty = new System.Windows.Forms.Label();
            this.lblNameRequired = new System.Windows.Forms.Label();
            this.lblPhoneRequired = new System.Windows.Forms.Label();
            this.lblAddress1Required = new System.Windows.Forms.Label();
            this.lblTownRequired = new System.Windows.Forms.Label();
            this.ddlCountry = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblAddress2Required = new System.Windows.Forms.Label();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.lblAddress2 = new System.Windows.Forms.Label();
            this.lblPostCodeRequired = new System.Windows.Forms.Label();
            this.txtPostCode = new System.Windows.Forms.TextBox();
            this.lblPostCode = new System.Windows.Forms.Label();
            this.lblCompanyRequired = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.lblCompany = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblSenderName
            // 
            this.lblSenderName.AutoSize = true;
            this.lblSenderName.Location = new System.Drawing.Point(13, 13);
            this.lblSenderName.Name = "lblSenderName";
            this.lblSenderName.Size = new System.Drawing.Size(72, 13);
            this.lblSenderName.TabIndex = 0;
            this.lblSenderName.Text = "Sender Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(103, 13);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(153, 20);
            this.txtName.TabIndex = 1;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(102, 45);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(153, 20);
            this.txtPhone.TabIndex = 3;
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(12, 45);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(78, 13);
            this.lblPhoneNumber.TabIndex = 2;
            this.lblPhoneNumber.Text = "Phone Number";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(102, 75);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(153, 20);
            this.txtAddress1.TabIndex = 5;
            // 
            // lblAddress1
            // 
            this.lblAddress1.AutoSize = true;
            this.lblAddress1.Location = new System.Drawing.Point(12, 75);
            this.lblAddress1.Name = "lblAddress1";
            this.lblAddress1.Size = new System.Drawing.Size(77, 13);
            this.lblAddress1.TabIndex = 4;
            this.lblAddress1.Text = "Address Line 1";
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(464, 48);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(153, 20);
            this.txtTown.TabIndex = 9;
            // 
            // lblTown
            // 
            this.lblTown.AutoSize = true;
            this.lblTown.Location = new System.Drawing.Point(379, 48);
            this.lblTown.Name = "lblTown";
            this.lblTown.Size = new System.Drawing.Size(75, 13);
            this.lblTown.TabIndex = 8;
            this.lblTown.Text = "Address Town";
            // 
            // lblCounty
            // 
            this.lblCounty.AutoSize = true;
            this.lblCounty.Location = new System.Drawing.Point(379, 112);
            this.lblCounty.Name = "lblCounty";
            this.lblCounty.Size = new System.Drawing.Size(81, 13);
            this.lblCounty.TabIndex = 10;
            this.lblCounty.Text = "Address County";
            // 
            // lblNameRequired
            // 
            this.lblNameRequired.AutoSize = true;
            this.lblNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblNameRequired.Location = new System.Drawing.Point(262, 16);
            this.lblNameRequired.Name = "lblNameRequired";
            this.lblNameRequired.Size = new System.Drawing.Size(50, 13);
            this.lblNameRequired.TabIndex = 55;
            this.lblNameRequired.Text = "Required";
            this.lblNameRequired.Visible = false;
            // 
            // lblPhoneRequired
            // 
            this.lblPhoneRequired.AutoSize = true;
            this.lblPhoneRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPhoneRequired.Location = new System.Drawing.Point(261, 48);
            this.lblPhoneRequired.Name = "lblPhoneRequired";
            this.lblPhoneRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPhoneRequired.TabIndex = 56;
            this.lblPhoneRequired.Text = "Required";
            this.lblPhoneRequired.Visible = false;
            // 
            // lblAddress1Required
            // 
            this.lblAddress1Required.AutoSize = true;
            this.lblAddress1Required.ForeColor = System.Drawing.Color.Red;
            this.lblAddress1Required.Location = new System.Drawing.Point(261, 79);
            this.lblAddress1Required.Name = "lblAddress1Required";
            this.lblAddress1Required.Size = new System.Drawing.Size(50, 13);
            this.lblAddress1Required.TabIndex = 57;
            this.lblAddress1Required.Text = "Required";
            this.lblAddress1Required.Visible = false;
            // 
            // lblTownRequired
            // 
            this.lblTownRequired.AutoSize = true;
            this.lblTownRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTownRequired.Location = new System.Drawing.Point(623, 51);
            this.lblTownRequired.Name = "lblTownRequired";
            this.lblTownRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTownRequired.TabIndex = 59;
            this.lblTownRequired.Text = "Required";
            this.lblTownRequired.Visible = false;
            // 
            // ddlCountry
            // 
            this.ddlCountry.FormattingEnabled = true;
            this.ddlCountry.Location = new System.Drawing.Point(464, 110);
            this.ddlCountry.Name = "ddlCountry";
            this.ddlCountry.Size = new System.Drawing.Size(153, 21);
            this.ddlCountry.TabIndex = 61;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(264, 150);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 29);
            this.btnSave.TabIndex = 62;
            this.btnSave.Text = "Save && Close";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(392, 149);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(107, 29);
            this.btnCancel.TabIndex = 63;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblAddress2Required
            // 
            this.lblAddress2Required.AutoSize = true;
            this.lblAddress2Required.ForeColor = System.Drawing.Color.Red;
            this.lblAddress2Required.Location = new System.Drawing.Point(262, 105);
            this.lblAddress2Required.Name = "lblAddress2Required";
            this.lblAddress2Required.Size = new System.Drawing.Size(50, 13);
            this.lblAddress2Required.TabIndex = 66;
            this.lblAddress2Required.Text = "Required";
            this.lblAddress2Required.Visible = false;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(103, 105);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(153, 20);
            this.txtAddress2.TabIndex = 65;
            // 
            // lblAddress2
            // 
            this.lblAddress2.AutoSize = true;
            this.lblAddress2.Location = new System.Drawing.Point(13, 105);
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Size = new System.Drawing.Size(77, 13);
            this.lblAddress2.TabIndex = 64;
            this.lblAddress2.Text = "Address Line 2";
            // 
            // lblPostCodeRequired
            // 
            this.lblPostCodeRequired.AutoSize = true;
            this.lblPostCodeRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPostCodeRequired.Location = new System.Drawing.Point(623, 16);
            this.lblPostCodeRequired.Name = "lblPostCodeRequired";
            this.lblPostCodeRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPostCodeRequired.TabIndex = 69;
            this.lblPostCodeRequired.Text = "Required";
            this.lblPostCodeRequired.Visible = false;
            // 
            // txtPostCode
            // 
            this.txtPostCode.Location = new System.Drawing.Point(464, 16);
            this.txtPostCode.Name = "txtPostCode";
            this.txtPostCode.Size = new System.Drawing.Size(153, 20);
            this.txtPostCode.TabIndex = 68;
            // 
            // lblPostCode
            // 
            this.lblPostCode.AutoSize = true;
            this.lblPostCode.Location = new System.Drawing.Point(379, 16);
            this.lblPostCode.Name = "lblPostCode";
            this.lblPostCode.Size = new System.Drawing.Size(56, 13);
            this.lblPostCode.TabIndex = 67;
            this.lblPostCode.Text = "Post Code";
            // 
            // lblCompanyRequired
            // 
            this.lblCompanyRequired.AutoSize = true;
            this.lblCompanyRequired.ForeColor = System.Drawing.Color.Red;
            this.lblCompanyRequired.Location = new System.Drawing.Point(623, 81);
            this.lblCompanyRequired.Name = "lblCompanyRequired";
            this.lblCompanyRequired.Size = new System.Drawing.Size(50, 13);
            this.lblCompanyRequired.TabIndex = 72;
            this.lblCompanyRequired.Text = "Required";
            this.lblCompanyRequired.Visible = false;
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(464, 78);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(153, 20);
            this.txtCompany.TabIndex = 71;
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(379, 78);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 70;
            this.lblCompany.Text = "Company";
            // 
            // SenderDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 213);
            this.Controls.Add(this.lblCompanyRequired);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.lblPostCodeRequired);
            this.Controls.Add(this.txtPostCode);
            this.Controls.Add(this.lblPostCode);
            this.Controls.Add(this.lblAddress2Required);
            this.Controls.Add(this.txtAddress2);
            this.Controls.Add(this.lblAddress2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ddlCountry);
            this.Controls.Add(this.lblTownRequired);
            this.Controls.Add(this.lblAddress1Required);
            this.Controls.Add(this.lblPhoneRequired);
            this.Controls.Add(this.lblNameRequired);
            this.Controls.Add(this.lblCounty);
            this.Controls.Add(this.txtTown);
            this.Controls.Add(this.lblTown);
            this.Controls.Add(this.txtAddress1);
            this.Controls.Add(this.lblAddress1);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.lblPhoneNumber);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblSenderName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SenderDetails";
            this.Text = "Sender Details";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSenderName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.Label lblAddress1;
        private System.Windows.Forms.TextBox txtTown;
        private System.Windows.Forms.Label lblTown;
        private System.Windows.Forms.Label lblCounty;
        private System.Windows.Forms.Label lblNameRequired;
        private System.Windows.Forms.Label lblPhoneRequired;
        private System.Windows.Forms.Label lblAddress1Required;
        private System.Windows.Forms.Label lblTownRequired;
        private System.Windows.Forms.ComboBox ddlCountry;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblAddress2Required;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label lblAddress2;
        private System.Windows.Forms.Label lblPostCodeRequired;
        private System.Windows.Forms.TextBox txtPostCode;
        private System.Windows.Forms.Label lblPostCode;
        private System.Windows.Forms.Label lblCompanyRequired;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label lblCompany;
    }
}