﻿namespace ODM.Appliction
{
    partial class ProcessOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcessOrder));
            this.btnbarcodeScan = new System.Windows.Forms.Button();
            this.btnprintlabel = new System.Windows.Forms.Button();
            this.btnprintinvoice = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSkip = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.gbxOrderItem = new System.Windows.Forms.GroupBox();
            this.gridItemDetail = new System.Windows.Forms.DataGridView();
            this.gbxShipping = new System.Windows.Forms.GroupBox();
            this.PicRight = new System.Windows.Forms.PictureBox();
            this.PicError = new System.Windows.Forms.PictureBox();
            this.PicLoading = new System.Windows.Forms.PictureBox();
            this.txtTotalWeight = new System.Windows.Forms.TextBox();
            this.lblTotalWeight = new System.Windows.Forms.Label();
            this.txtItemWeight = new System.Windows.Forms.TextBox();
            this.lblItemsWeight = new System.Windows.Forms.Label();
            this.txtTrackingNumber = new System.Windows.Forms.TextBox();
            this.lblTrackingNumber = new System.Windows.Forms.Label();
            this.lblPackagingControl = new System.Windows.Forms.Label();
            this.ddlPackagingType = new System.Windows.Forms.ComboBox();
            this.ddlPackaging = new System.Windows.Forms.ComboBox();
            this.ddlShippingMethod = new System.Windows.Forms.ComboBox();
            this.lblPackaging = new System.Windows.Forms.Label();
            this.lblShippingMethod = new System.Windows.Forms.Label();
            this.gbxOrderNotes = new System.Windows.Forms.GroupBox();
            this.gbxInfo = new System.Windows.Forms.GroupBox();
            this.ddlCountry = new System.Windows.Forms.ComboBox();
            this.lblOrderNum = new System.Windows.Forms.Label();
            this.txtOrderNum = new System.Windows.Forms.TextBox();
            this.panelUserInfo = new System.Windows.Forms.Panel();
            this.lblCountryInfo = new System.Windows.Forms.Label();
            this.lblPostCode = new System.Windows.Forms.Label();
            this.lblTown = new System.Windows.Forms.Label();
            this.lblcompany = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblCntry = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.txtPostageCost = new System.Windows.Forms.TextBox();
            this.txttax = new System.Windows.Forms.TextBox();
            this.txtsubtotal = new System.Windows.Forms.TextBox();
            this.txttaxrate = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblPostageCost = new System.Windows.Forms.Label();
            this.lblTax = new System.Windows.Forms.Label();
            this.lblSubTotal = new System.Windows.Forms.Label();
            this.lblTaxRate = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblCountry = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtreference = new System.Windows.Forms.TextBox();
            this.txtsource = new System.Windows.Forms.TextBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblRefrence = new System.Windows.Forms.Label();
            this.lblOrderId = new System.Windows.Forms.Label();
            this.txtOrderId = new System.Windows.Forms.TextBox();
            this.btnEditAddress = new System.Windows.Forms.Button();
            this.richTextInstructions = new System.Windows.Forms.RichTextBox();
            this.picImageInstruction = new System.Windows.Forms.PictureBox();
            this.richTxtOrderNotes = new System.Windows.Forms.RichTextBox();
            this.gbxOrderItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridItemDetail)).BeginInit();
            this.gbxShipping.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).BeginInit();
            this.gbxOrderNotes.SuspendLayout();
            this.gbxInfo.SuspendLayout();
            this.panelUserInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImageInstruction)).BeginInit();
            this.SuspendLayout();
            // 
            // btnbarcodeScan
            // 
            this.btnbarcodeScan.Location = new System.Drawing.Point(758, 9);
            this.btnbarcodeScan.Name = "btnbarcodeScan";
            this.btnbarcodeScan.Size = new System.Drawing.Size(86, 23);
            this.btnbarcodeScan.TabIndex = 0;
            this.btnbarcodeScan.Text = "Scan Barcode";
            this.btnbarcodeScan.UseVisualStyleBackColor = true;
            // 
            // btnprintlabel
            // 
            this.btnprintlabel.Location = new System.Drawing.Point(566, 9);
            this.btnprintlabel.Name = "btnprintlabel";
            this.btnprintlabel.Size = new System.Drawing.Size(93, 23);
            this.btnprintlabel.TabIndex = 1;
            this.btnprintlabel.Text = "Save && Print";
            this.btnprintlabel.UseVisualStyleBackColor = true;
            this.btnprintlabel.Click += new System.EventHandler(this.btnprintlabel_Click);
            // 
            // btnprintinvoice
            // 
            this.btnprintinvoice.Location = new System.Drawing.Point(850, 9);
            this.btnprintinvoice.Name = "btnprintinvoice";
            this.btnprintinvoice.Size = new System.Drawing.Size(90, 23);
            this.btnprintinvoice.TabIndex = 2;
            this.btnprintinvoice.Text = "Print Invoice";
            this.btnprintinvoice.UseVisualStyleBackColor = true;
            this.btnprintinvoice.Click += new System.EventHandler(this.btnprintinvoice_Click);
            // 
            // btnexit
            // 
            this.btnexit.Location = new System.Drawing.Point(955, 9);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(52, 23);
            this.btnexit.TabIndex = 3;
            this.btnexit.Text = "Exit";
            this.btnexit.UseVisualStyleBackColor = true;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(108, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Order Details";
            // 
            // btnSkip
            // 
            this.btnSkip.Location = new System.Drawing.Point(4, 585);
            this.btnSkip.Name = "btnSkip";
            this.btnSkip.Size = new System.Drawing.Size(100, 35);
            this.btnSkip.TabIndex = 27;
            this.btnSkip.Text = "Skip";
            this.btnSkip.UseVisualStyleBackColor = true;
            this.btnSkip.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(652, 585);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(101, 35);
            this.btnProcess.TabIndex = 28;
            this.btnProcess.Text = "Dispatch";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // gbxOrderItem
            // 
            this.gbxOrderItem.Controls.Add(this.gridItemDetail);
            this.gbxOrderItem.Location = new System.Drawing.Point(4, 454);
            this.gbxOrderItem.Name = "gbxOrderItem";
            this.gbxOrderItem.Size = new System.Drawing.Size(1003, 115);
            this.gbxOrderItem.TabIndex = 25;
            this.gbxOrderItem.TabStop = false;
            this.gbxOrderItem.Text = "Order Items";
            // 
            // gridItemDetail
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridItemDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.gridItemDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridItemDetail.DefaultCellStyle = dataGridViewCellStyle11;
            this.gridItemDetail.Location = new System.Drawing.Point(3, 19);
            this.gridItemDetail.Name = "gridItemDetail";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridItemDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.gridItemDetail.Size = new System.Drawing.Size(996, 90);
            this.gridItemDetail.TabIndex = 26;
            // 
            // gbxShipping
            // 
            this.gbxShipping.Controls.Add(this.PicRight);
            this.gbxShipping.Controls.Add(this.PicError);
            this.gbxShipping.Controls.Add(this.PicLoading);
            this.gbxShipping.Controls.Add(this.txtTotalWeight);
            this.gbxShipping.Controls.Add(this.lblTotalWeight);
            this.gbxShipping.Controls.Add(this.txtItemWeight);
            this.gbxShipping.Controls.Add(this.lblItemsWeight);
            this.gbxShipping.Controls.Add(this.txtTrackingNumber);
            this.gbxShipping.Controls.Add(this.lblTrackingNumber);
            this.gbxShipping.Controls.Add(this.lblPackagingControl);
            this.gbxShipping.Controls.Add(this.ddlPackagingType);
            this.gbxShipping.Controls.Add(this.ddlPackaging);
            this.gbxShipping.Controls.Add(this.ddlShippingMethod);
            this.gbxShipping.Controls.Add(this.lblPackaging);
            this.gbxShipping.Controls.Add(this.lblShippingMethod);
            this.gbxShipping.Location = new System.Drawing.Point(5, 332);
            this.gbxShipping.Name = "gbxShipping";
            this.gbxShipping.Size = new System.Drawing.Size(748, 117);
            this.gbxShipping.TabIndex = 18;
            this.gbxShipping.TabStop = false;
            this.gbxShipping.Text = "Shipping";
            // 
            // PicRight
            // 
            this.PicRight.Image = ((System.Drawing.Image)(resources.GetObject("PicRight.Image")));
            this.PicRight.Location = new System.Drawing.Point(384, 25);
            this.PicRight.Name = "PicRight";
            this.PicRight.Size = new System.Drawing.Size(25, 18);
            this.PicRight.TabIndex = 27;
            this.PicRight.TabStop = false;
            // 
            // PicError
            // 
            this.PicError.Image = ((System.Drawing.Image)(resources.GetObject("PicError.Image")));
            this.PicError.Location = new System.Drawing.Point(383, 25);
            this.PicError.Name = "PicError";
            this.PicError.Size = new System.Drawing.Size(26, 20);
            this.PicError.TabIndex = 26;
            this.PicError.TabStop = false;
            // 
            // PicLoading
            // 
            this.PicLoading.Image = ((System.Drawing.Image)(resources.GetObject("PicLoading.Image")));
            this.PicLoading.Location = new System.Drawing.Point(383, 25);
            this.PicLoading.Name = "PicLoading";
            this.PicLoading.Size = new System.Drawing.Size(26, 20);
            this.PicLoading.TabIndex = 25;
            this.PicLoading.TabStop = false;
            // 
            // txtTotalWeight
            // 
            this.txtTotalWeight.Location = new System.Drawing.Point(524, 84);
            this.txtTotalWeight.Name = "txtTotalWeight";
            this.txtTotalWeight.Size = new System.Drawing.Size(100, 20);
            this.txtTotalWeight.TabIndex = 24;
            // 
            // lblTotalWeight
            // 
            this.lblTotalWeight.AutoSize = true;
            this.lblTotalWeight.Location = new System.Drawing.Point(425, 87);
            this.lblTotalWeight.Name = "lblTotalWeight";
            this.lblTotalWeight.Size = new System.Drawing.Size(68, 13);
            this.lblTotalWeight.TabIndex = 22;
            this.lblTotalWeight.Text = "Total Weight";
            // 
            // txtItemWeight
            // 
            this.txtItemWeight.Enabled = false;
            this.txtItemWeight.Location = new System.Drawing.Point(524, 52);
            this.txtItemWeight.Name = "txtItemWeight";
            this.txtItemWeight.Size = new System.Drawing.Size(100, 20);
            this.txtItemWeight.TabIndex = 22;
            // 
            // lblItemsWeight
            // 
            this.lblItemsWeight.AutoSize = true;
            this.lblItemsWeight.Location = new System.Drawing.Point(425, 55);
            this.lblItemsWeight.Name = "lblItemsWeight";
            this.lblItemsWeight.Size = new System.Drawing.Size(69, 13);
            this.lblItemsWeight.TabIndex = 20;
            this.lblItemsWeight.Text = "Items Weight";
            // 
            // txtTrackingNumber
            // 
            this.txtTrackingNumber.Location = new System.Drawing.Point(524, 21);
            this.txtTrackingNumber.Name = "txtTrackingNumber";
            this.txtTrackingNumber.Size = new System.Drawing.Size(207, 20);
            this.txtTrackingNumber.TabIndex = 20;
            // 
            // lblTrackingNumber
            // 
            this.lblTrackingNumber.AutoSize = true;
            this.lblTrackingNumber.Location = new System.Drawing.Point(425, 24);
            this.lblTrackingNumber.Name = "lblTrackingNumber";
            this.lblTrackingNumber.Size = new System.Drawing.Size(89, 13);
            this.lblTrackingNumber.TabIndex = 18;
            this.lblTrackingNumber.Text = "Tracking Number";
            // 
            // lblPackagingControl
            // 
            this.lblPackagingControl.AutoSize = true;
            this.lblPackagingControl.Location = new System.Drawing.Point(83, 83);
            this.lblPackagingControl.Name = "lblPackagingControl";
            this.lblPackagingControl.Size = new System.Drawing.Size(85, 13);
            this.lblPackagingControl.TabIndex = 17;
            this.lblPackagingControl.Text = "Packaging Type";
            this.lblPackagingControl.Visible = false;
            // 
            // ddlPackagingType
            // 
            this.ddlPackagingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPackagingType.FormattingEnabled = true;
            this.ddlPackagingType.Location = new System.Drawing.Point(186, 80);
            this.ddlPackagingType.Name = "ddlPackagingType";
            this.ddlPackagingType.Size = new System.Drawing.Size(121, 21);
            this.ddlPackagingType.TabIndex = 23;
            this.ddlPackagingType.Visible = false;
            // 
            // ddlPackaging
            // 
            this.ddlPackaging.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPackaging.FormattingEnabled = true;
            this.ddlPackaging.Location = new System.Drawing.Point(186, 52);
            this.ddlPackaging.Name = "ddlPackaging";
            this.ddlPackaging.Size = new System.Drawing.Size(121, 21);
            this.ddlPackaging.TabIndex = 21;
            // 
            // ddlShippingMethod
            // 
            this.ddlShippingMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlShippingMethod.FormattingEnabled = true;
            this.ddlShippingMethod.Location = new System.Drawing.Point(186, 24);
            this.ddlShippingMethod.Name = "ddlShippingMethod";
            this.ddlShippingMethod.Size = new System.Drawing.Size(191, 21);
            this.ddlShippingMethod.TabIndex = 19;
            // 
            // lblPackaging
            // 
            this.lblPackaging.AutoSize = true;
            this.lblPackaging.Location = new System.Drawing.Point(91, 55);
            this.lblPackaging.Name = "lblPackaging";
            this.lblPackaging.Size = new System.Drawing.Size(58, 13);
            this.lblPackaging.TabIndex = 13;
            this.lblPackaging.Text = "Packaging";
            // 
            // lblShippingMethod
            // 
            this.lblShippingMethod.AutoSize = true;
            this.lblShippingMethod.Location = new System.Drawing.Point(83, 27);
            this.lblShippingMethod.Name = "lblShippingMethod";
            this.lblShippingMethod.Size = new System.Drawing.Size(87, 13);
            this.lblShippingMethod.TabIndex = 12;
            this.lblShippingMethod.Text = "Shipping Method";
            // 
            // gbxOrderNotes
            // 
            this.gbxOrderNotes.Controls.Add(this.richTxtOrderNotes);
            this.gbxOrderNotes.Location = new System.Drawing.Point(538, 176);
            this.gbxOrderNotes.Name = "gbxOrderNotes";
            this.gbxOrderNotes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbxOrderNotes.Size = new System.Drawing.Size(197, 104);
            this.gbxOrderNotes.TabIndex = 16;
            this.gbxOrderNotes.TabStop = false;
            this.gbxOrderNotes.Text = "Order Notes";
            // 
            // gbxInfo
            // 
            this.gbxInfo.Controls.Add(this.gbxOrderNotes);
            this.gbxInfo.Controls.Add(this.ddlCountry);
            this.gbxInfo.Controls.Add(this.lblOrderNum);
            this.gbxInfo.Controls.Add(this.txtOrderNum);
            this.gbxInfo.Controls.Add(this.panelUserInfo);
            this.gbxInfo.Controls.Add(this.txttotal);
            this.gbxInfo.Controls.Add(this.txtPostageCost);
            this.gbxInfo.Controls.Add(this.txttax);
            this.gbxInfo.Controls.Add(this.txtsubtotal);
            this.gbxInfo.Controls.Add(this.txttaxrate);
            this.gbxInfo.Controls.Add(this.lblTotal);
            this.gbxInfo.Controls.Add(this.lblPostageCost);
            this.gbxInfo.Controls.Add(this.lblTax);
            this.gbxInfo.Controls.Add(this.lblSubTotal);
            this.gbxInfo.Controls.Add(this.lblTaxRate);
            this.gbxInfo.Controls.Add(this.lblEmail);
            this.gbxInfo.Controls.Add(this.lblCountry);
            this.gbxInfo.Controls.Add(this.txtEmail);
            this.gbxInfo.Controls.Add(this.txtreference);
            this.gbxInfo.Controls.Add(this.txtsource);
            this.gbxInfo.Controls.Add(this.lblSource);
            this.gbxInfo.Controls.Add(this.lblRefrence);
            this.gbxInfo.Controls.Add(this.lblOrderId);
            this.gbxInfo.Controls.Add(this.txtOrderId);
            this.gbxInfo.Location = new System.Drawing.Point(4, 38);
            this.gbxInfo.Name = "gbxInfo";
            this.gbxInfo.Size = new System.Drawing.Size(749, 288);
            this.gbxInfo.TabIndex = 4;
            this.gbxInfo.TabStop = false;
            this.gbxInfo.Text = "Info";
            // 
            // ddlCountry
            // 
            this.ddlCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCountry.FormattingEnabled = true;
            this.ddlCountry.Location = new System.Drawing.Point(337, 110);
            this.ddlCountry.Name = "ddlCountry";
            this.ddlCountry.Size = new System.Drawing.Size(144, 21);
            this.ddlCountry.TabIndex = 11;
            // 
            // lblOrderNum
            // 
            this.lblOrderNum.AutoSize = true;
            this.lblOrderNum.Location = new System.Drawing.Point(235, 22);
            this.lblOrderNum.Name = "lblOrderNum";
            this.lblOrderNum.Size = new System.Drawing.Size(73, 13);
            this.lblOrderNum.TabIndex = 61;
            this.lblOrderNum.Text = "Order Number";
            // 
            // txtOrderNum
            // 
            this.txtOrderNum.Enabled = false;
            this.txtOrderNum.Location = new System.Drawing.Point(337, 22);
            this.txtOrderNum.Name = "txtOrderNum";
            this.txtOrderNum.Size = new System.Drawing.Size(144, 20);
            this.txtOrderNum.TabIndex = 60;
            this.txtOrderNum.WordWrap = false;
            // 
            // panelUserInfo
            // 
            this.panelUserInfo.Controls.Add(this.lblCountryInfo);
            this.panelUserInfo.Controls.Add(this.lblPostCode);
            this.panelUserInfo.Controls.Add(this.lblTown);
            this.panelUserInfo.Controls.Add(this.lblcompany);
            this.panelUserInfo.Controls.Add(this.lblAddress);
            this.panelUserInfo.Controls.Add(this.lblCntry);
            this.panelUserInfo.Controls.Add(this.lblName);
            this.panelUserInfo.Location = new System.Drawing.Point(6, 22);
            this.panelUserInfo.Name = "panelUserInfo";
            this.panelUserInfo.Size = new System.Drawing.Size(213, 219);
            this.panelUserInfo.TabIndex = 5;
            // 
            // lblCountryInfo
            // 
            this.lblCountryInfo.AutoSize = true;
            this.lblCountryInfo.Location = new System.Drawing.Point(9, 111);
            this.lblCountryInfo.Name = "lblCountryInfo";
            this.lblCountryInfo.Size = new System.Drawing.Size(43, 13);
            this.lblCountryInfo.TabIndex = 8;
            this.lblCountryInfo.Text = "Country";
            // 
            // lblPostCode
            // 
            this.lblPostCode.AutoSize = true;
            this.lblPostCode.Location = new System.Drawing.Point(9, 83);
            this.lblPostCode.Name = "lblPostCode";
            this.lblPostCode.Size = new System.Drawing.Size(53, 13);
            this.lblPostCode.TabIndex = 7;
            this.lblPostCode.Text = "PostCode";
            // 
            // lblTown
            // 
            this.lblTown.AutoSize = true;
            this.lblTown.Location = new System.Drawing.Point(9, 56);
            this.lblTown.Name = "lblTown";
            this.lblTown.Size = new System.Drawing.Size(34, 13);
            this.lblTown.TabIndex = 6;
            this.lblTown.Text = "Town";
            // 
            // lblcompany
            // 
            this.lblcompany.AutoSize = true;
            this.lblcompany.Location = new System.Drawing.Point(3, 52);
            this.lblcompany.Name = "lblcompany";
            this.lblcompany.Size = new System.Drawing.Size(0, 13);
            this.lblcompany.TabIndex = 5;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(8, 31);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(45, 13);
            this.lblAddress.TabIndex = 4;
            this.lblAddress.Text = "Address";
            // 
            // lblCntry
            // 
            this.lblCntry.AutoSize = true;
            this.lblCntry.Location = new System.Drawing.Point(45, 88);
            this.lblCntry.Name = "lblCntry";
            this.lblCntry.Size = new System.Drawing.Size(0, 13);
            this.lblCntry.TabIndex = 2;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(8, 7);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            // 
            // txttotal
            // 
            this.txttotal.Enabled = false;
            this.txttotal.Location = new System.Drawing.Point(632, 146);
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(100, 20);
            this.txttotal.TabIndex = 15;
            // 
            // txtPostageCost
            // 
            this.txtPostageCost.Location = new System.Drawing.Point(632, 112);
            this.txtPostageCost.Name = "txtPostageCost";
            this.txtPostageCost.Size = new System.Drawing.Size(100, 20);
            this.txtPostageCost.TabIndex = 13;
            // 
            // txttax
            // 
            this.txttax.Enabled = false;
            this.txttax.Location = new System.Drawing.Point(632, 80);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(100, 20);
            this.txttax.TabIndex = 11;
            // 
            // txtsubtotal
            // 
            this.txtsubtotal.Enabled = false;
            this.txtsubtotal.Location = new System.Drawing.Point(632, 49);
            this.txtsubtotal.Name = "txtsubtotal";
            this.txtsubtotal.Size = new System.Drawing.Size(100, 20);
            this.txtsubtotal.TabIndex = 9;
            // 
            // txttaxrate
            // 
            this.txttaxrate.Enabled = false;
            this.txttaxrate.Location = new System.Drawing.Point(632, 19);
            this.txttaxrate.Name = "txttaxrate";
            this.txttaxrate.Size = new System.Drawing.Size(100, 20);
            this.txttaxrate.TabIndex = 7;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(541, 149);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(31, 13);
            this.lblTotal.TabIndex = 59;
            this.lblTotal.Text = "Total";
            // 
            // lblPostageCost
            // 
            this.lblPostageCost.AutoSize = true;
            this.lblPostageCost.Location = new System.Drawing.Point(541, 115);
            this.lblPostageCost.Name = "lblPostageCost";
            this.lblPostageCost.Size = new System.Drawing.Size(70, 13);
            this.lblPostageCost.TabIndex = 58;
            this.lblPostageCost.Text = "Postage Cost";
            // 
            // lblTax
            // 
            this.lblTax.AutoSize = true;
            this.lblTax.Location = new System.Drawing.Point(541, 80);
            this.lblTax.Name = "lblTax";
            this.lblTax.Size = new System.Drawing.Size(25, 13);
            this.lblTax.TabIndex = 57;
            this.lblTax.Text = "Tax";
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.AutoSize = true;
            this.lblSubTotal.Location = new System.Drawing.Point(541, 52);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(50, 13);
            this.lblSubTotal.TabIndex = 56;
            this.lblSubTotal.Text = "SubTotal";
            // 
            // lblTaxRate
            // 
            this.lblTaxRate.AutoSize = true;
            this.lblTaxRate.Location = new System.Drawing.Point(540, 22);
            this.lblTaxRate.Name = "lblTaxRate";
            this.lblTaxRate.Size = new System.Drawing.Size(51, 13);
            this.lblTaxRate.TabIndex = 55;
            this.lblTaxRate.Text = "Tax Rate";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(235, 143);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(35, 13);
            this.lblEmail.TabIndex = 54;
            this.lblEmail.Text = "E-mail";
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(234, 119);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(43, 13);
            this.lblCountry.TabIndex = 53;
            this.lblCountry.Text = "Country";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(337, 140);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(144, 20);
            this.txtEmail.TabIndex = 14;
            // 
            // txtreference
            // 
            this.txtreference.Enabled = false;
            this.txtreference.Location = new System.Drawing.Point(337, 52);
            this.txtreference.Name = "txtreference";
            this.txtreference.Size = new System.Drawing.Size(178, 20);
            this.txtreference.TabIndex = 8;
            // 
            // txtsource
            // 
            this.txtsource.Enabled = false;
            this.txtsource.Location = new System.Drawing.Point(337, 81);
            this.txtsource.Name = "txtsource";
            this.txtsource.Size = new System.Drawing.Size(144, 20);
            this.txtsource.TabIndex = 10;
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(234, 87);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(97, 13);
            this.lblSource.TabIndex = 48;
            this.lblSource.Text = "Source/Subsource";
            // 
            // lblRefrence
            // 
            this.lblRefrence.AutoSize = true;
            this.lblRefrence.Location = new System.Drawing.Point(235, 54);
            this.lblRefrence.Name = "lblRefrence";
            this.lblRefrence.Size = new System.Drawing.Size(57, 13);
            this.lblRefrence.TabIndex = 47;
            this.lblRefrence.Text = "Reference";
            // 
            // lblOrderId
            // 
            this.lblOrderId.AutoSize = true;
            this.lblOrderId.Location = new System.Drawing.Point(235, 169);
            this.lblOrderId.Name = "lblOrderId";
            this.lblOrderId.Size = new System.Drawing.Size(42, 13);
            this.lblOrderId.TabIndex = 46;
            this.lblOrderId.Text = "OrderId";
            this.lblOrderId.Visible = false;
            // 
            // txtOrderId
            // 
            this.txtOrderId.Location = new System.Drawing.Point(337, 169);
            this.txtOrderId.Name = "txtOrderId";
            this.txtOrderId.Size = new System.Drawing.Size(144, 20);
            this.txtOrderId.TabIndex = 6;
            this.txtOrderId.Visible = false;
            // 
            // btnEditAddress
            // 
            this.btnEditAddress.Location = new System.Drawing.Point(665, 9);
            this.btnEditAddress.Name = "btnEditAddress";
            this.btnEditAddress.Size = new System.Drawing.Size(88, 23);
            this.btnEditAddress.TabIndex = 62;
            this.btnEditAddress.Text = "Edit Address";
            this.btnEditAddress.UseVisualStyleBackColor = true;
            this.btnEditAddress.Click += new System.EventHandler(this.btnEditAddress_Click);
            // 
            // richTextInstructions
            // 
            this.richTextInstructions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextInstructions.BackColor = System.Drawing.SystemColors.Control;
            this.richTextInstructions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextInstructions.Location = new System.Drawing.Point(759, 205);
            this.richTextInstructions.Name = "richTextInstructions";
            this.richTextInstructions.ReadOnly = true;
            this.richTextInstructions.Size = new System.Drawing.Size(248, 244);
            this.richTextInstructions.TabIndex = 67;
            this.richTextInstructions.Text = "";
            // 
            // picImageInstruction
            // 
            this.picImageInstruction.Location = new System.Drawing.Point(757, 44);
            this.picImageInstruction.Name = "picImageInstruction";
            this.picImageInstruction.Size = new System.Drawing.Size(250, 155);
            this.picImageInstruction.TabIndex = 66;
            this.picImageInstruction.TabStop = false;
            // 
            // richTxtOrderNotes
            // 
            this.richTxtOrderNotes.Location = new System.Drawing.Point(6, 19);
            this.richTxtOrderNotes.Name = "richTxtOrderNotes";
            this.richTxtOrderNotes.Size = new System.Drawing.Size(185, 75);
            this.richTxtOrderNotes.TabIndex = 62;
            this.richTxtOrderNotes.Text = "";
            // 
            // ProcessOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 633);
            this.Controls.Add(this.richTextInstructions);
            this.Controls.Add(this.picImageInstruction);
            this.Controls.Add(this.btnEditAddress);
            this.Controls.Add(this.gbxShipping);
            this.Controls.Add(this.gbxOrderItem);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.btnSkip);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.btnprintinvoice);
            this.Controls.Add(this.btnprintlabel);
            this.Controls.Add(this.btnbarcodeScan);
            this.Controls.Add(this.gbxInfo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProcessOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Process Order";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProcessOrder_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbxOrderItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridItemDetail)).EndInit();
            this.gbxShipping.ResumeLayout(false);
            this.gbxShipping.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).EndInit();
            this.gbxOrderNotes.ResumeLayout(false);
            this.gbxInfo.ResumeLayout(false);
            this.gbxInfo.PerformLayout();
            this.panelUserInfo.ResumeLayout(false);
            this.panelUserInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImageInstruction)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnbarcodeScan;
        private System.Windows.Forms.Button btnprintlabel;
        private System.Windows.Forms.Button btnprintinvoice;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSkip;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.GroupBox gbxOrderItem;
        private System.Windows.Forms.DataGridView gridItemDetail;
        private System.Windows.Forms.GroupBox gbxShipping;
        private System.Windows.Forms.TextBox txtTotalWeight;
        private System.Windows.Forms.Label lblTotalWeight;
        private System.Windows.Forms.TextBox txtItemWeight;
        private System.Windows.Forms.Label lblItemsWeight;
        private System.Windows.Forms.TextBox txtTrackingNumber;
        private System.Windows.Forms.Label lblTrackingNumber;
        private System.Windows.Forms.Label lblPackagingControl;
        private System.Windows.Forms.ComboBox ddlPackagingType;
        private System.Windows.Forms.ComboBox ddlPackaging;
        private System.Windows.Forms.ComboBox ddlShippingMethod;
        private System.Windows.Forms.Label lblPackaging;
        private System.Windows.Forms.Label lblShippingMethod;
        private System.Windows.Forms.GroupBox gbxOrderNotes;
        private System.Windows.Forms.GroupBox gbxInfo;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.TextBox txtPostageCost;
        private System.Windows.Forms.TextBox txttax;
        private System.Windows.Forms.TextBox txtsubtotal;
        private System.Windows.Forms.TextBox txttaxrate;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblPostageCost;
        private System.Windows.Forms.Label lblTax;
        private System.Windows.Forms.Label lblSubTotal;
        private System.Windows.Forms.Label lblTaxRate;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtreference;
        private System.Windows.Forms.TextBox txtsource;
        private System.Windows.Forms.Label lblSource;
        private System.Windows.Forms.Label lblRefrence;
        private System.Windows.Forms.Label lblOrderId;
        private System.Windows.Forms.TextBox txtOrderId;
        private System.Windows.Forms.Label lblOrderNum;
        private System.Windows.Forms.TextBox txtOrderNum;
        private System.Windows.Forms.Panel panelUserInfo;
        private System.Windows.Forms.Label lblCntry;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox ddlCountry;
        private System.Windows.Forms.Label lblcompany;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblCountryInfo;
        private System.Windows.Forms.Label lblPostCode;
        private System.Windows.Forms.Label lblTown;
        private System.Windows.Forms.Button btnEditAddress;
        private System.Windows.Forms.PictureBox PicLoading;
        private System.Windows.Forms.PictureBox PicError;
        private System.Windows.Forms.PictureBox PicRight;
        private System.Windows.Forms.RichTextBox richTextInstructions;
        private System.Windows.Forms.PictureBox picImageInstruction;
        private System.Windows.Forms.RichTextBox richTxtOrderNotes;
    }
}