﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Entity;
using ODM.Data.Util;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Security.Authentication;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class MessageBody : Form
    {
        bool IsFirstCall = false;
        string _strExtMessageID = string.Empty, _strItemID = string.Empty, _strAppKey, _strCertKey, _strDevKey, _strEbayAuthToken, _strSentMessageBody = string.Empty, _strCustomerName;
        string _strSelectedEmailID = "", _strNumOrderID = "", _strOrderID = "", _strSubject = "", _strID = "", _strNumOrderIds = "", _strOrderIds = "",_strSource="",_strSubSource="";
        int _intIsSandBox = 0;
        bool _IsReplySent = false;
        string _strEbayMessageType = string.Empty;
        string MessageUID;
        string strTemplateText = null, _strMessageType = string.Empty;
        Guid AccountID = new Guid();
        string EbayUserID = string.Empty;
        Guid OrderId = new Guid();
        DataTable objDtGetTokenAndAppKeys = new DataTable();
        Email objEmail = new Email();
        DataTable objDtOrderDetailsWithAddress = new DataTable();
        private int IsMailSent;
        public MessageBody()
        {
            InitializeComponent();
            IsFirstCall = true;
        }
        public MessageBody(string[] strArrGrdSelectedValues)
        {
            InitializeComponent();
            IsFirstCall = true;
            PopulateControls(strArrGrdSelectedValues);
            BindDropDownList();
            GetTemplateDetails();
            CheckIsReplySent();
            if (_strMessageType.ToUpper() != "EMAIL")
            {
                GetTokenAndKeys();
                chkDisplayReplyToPublic.Visible = true;
                ReviseMessageEbayApiCall("true");
            }


            
            PopulateOrderDetailsWithAddress();
            IsFirstCall = false;
        }
        public void PopulateOrderDetailsWithAddress()
        {
            DataTable objDtOrderDetailsWithAddress = new DataTable();
            DataTable objDtOrderItemDetailsByOrderId = new DataTable();
            objDtOrderDetailsWithAddress = GlobalVariablesAndMethods.GetOrderDetailsWithAddress(txtCustomerName.Text.ToString(), _strMessageType.ToUpper());
            if (objDtOrderDetailsWithAddress.Rows.Count > 0)
            {
                ddlNumOrderId.DataSource = objDtOrderDetailsWithAddress;
                ddlNumOrderId.DisplayMember = "NumOrderId";
                ddlNumOrderId.ValueMember = "NumOrderId";
                ddlNumOrderId.SelectedIndex = 0;
                //tblOrderItemDetails.
                //save 
                objDtOrderItemDetailsByOrderId = GlobalVariablesAndMethods.OrderItemDetailsByOrderId(_strMessageType, OrderId);
                string OrderItems = string.Empty;
                if (objDtOrderItemDetailsByOrderId.Rows.Count > 0)
                {
                    grdOrderItemDetails.EditMode = DataGridViewEditMode.EditProgrammatically;
                    grdOrderItemDetails.DataSource = objDtOrderItemDetailsByOrderId;

                    grdOrderItemDetails.Columns["Discount"].Visible = false;
                    grdOrderItemDetails.Columns["Tax"].Visible = false;
                    grdOrderItemDetails.Columns["TaxRate"].Visible = false;
                    grdOrderItemDetails.Columns["Cost"].Visible = false;
                    grdOrderItemDetails.Columns["CostIncTax"].Visible = false;
                    grdOrderItemDetails.Columns["HasImage"].Visible = false;
                    grdOrderItemDetails.Columns["ImageId"].Visible = false;
                    grdOrderItemDetails.Columns["PricePerUnit"].Visible = false;
                    grdOrderItemDetails.Columns["Quantity"].Width = 50;
                    grdOrderItemDetails.Columns["Title"].Width = 225;
                    //to add the order items
                    grdOrderItemDetails.Columns["UnitCost"].Width = 50;

                    //"     $" + objDtOrderDetailsWithAddress.Rows[RowCount]["UnitCost"].ToString();

                    //txtOrderItems.Text = OrderItems;
                }



                // to add address for the user.
                int RowCount = ddlNumOrderId.SelectedIndex;
                string strAddress = objDtOrderDetailsWithAddress.Rows[RowCount]["FullName"].ToString() + Environment.NewLine + objDtOrderDetailsWithAddress.Rows[RowCount]["Address1"].ToString() + Environment.NewLine + objDtOrderDetailsWithAddress.Rows[RowCount]["Address2"].ToString() + Environment.NewLine + objDtOrderDetailsWithAddress.Rows[RowCount]["Address3"].ToString() + Environment.NewLine + objDtOrderDetailsWithAddress.Rows[RowCount]["Town"].ToString() + " " + objDtOrderDetailsWithAddress.Rows[RowCount]["Region"].ToString() + Environment.NewLine + objDtOrderDetailsWithAddress.Rows[RowCount]["Country"].ToString();
                txtAddress.Text = strAddress;

                //email address
                _strSelectedEmailID=objDtOrderDetailsWithAddress.Rows[RowCount]["EmailAddress"].ToString();

                //to add couriername and Tracking Number
                lblTrackingNumber.Text = objDtOrderDetailsWithAddress.Rows[RowCount]["TrackingNumber"].ToString();
                lblCourierName.Text = objDtOrderDetailsWithAddress.Rows[RowCount]["ServiceName"].ToString();
                lblTotalValue.Text = objDtOrderDetailsWithAddress.Rows[RowCount]["TotalCharge"].ToString() + " " + objDtOrderDetailsWithAddress.Rows[RowCount]["Currency"].ToString().ToUpper();

                //Order Received Date and Order Shipped Date
                if (OrderId != new Guid())
                {
                    lblOrderReceivedDate.Text = objDtOrderDetailsWithAddress.Rows[RowCount]["ReceivedDate"].ToString();
                    DateTime dt = Convert.ToDateTime(objDtOrderDetailsWithAddress.Rows[RowCount]["ReceivedDate"].ToString());
                }

                //check if the order is processed or not
                int IsProcessed = 0;
                IsProcessed = Convert.ToInt16(Convert.ToBoolean(objDtOrderDetailsWithAddress.Rows[RowCount]["Processed"].ToString()));
                if (IsProcessed == 1)
                {
                    lblOrderShippedDate.Text = objDtOrderDetailsWithAddress.Rows[RowCount]["DespatchByDate"].ToString();
                }
                else
                {
                    lblOrderShippedDate.Text = "Not Dispatched Yet";
                }
            }

        }
        private void ReviseMessageEbayApiCall(string strIsRead)
        {

            var client = new RestClient("https://www.google.com");
            if (Convert.ToBoolean(_intIsSandBox))
            {
                client = new RestClient("https://api.sandbox.ebay.com/ws/api.dll");
            }
            else
            {
                client = new RestClient("https://api.ebay.com/ws/api.dll");
            }



            var request = new RestRequest(Method.POST);

            request.AddHeader("X-EBAY-API-SITEID", "3");
            request.AddHeader("X-EBAY-API-COMPATIBILITY-LEVEL", "1073");
            request.AddHeader("X-EBAY-API-CALL-NAME", "ReviseMyMessages");
            request.AddHeader("X-EBAY-API-APP-NAME", _strAppKey);
            request.AddHeader("X-EBAY-API-DEV-NAME", _strDevKey);
            request.AddHeader("X-EBAY-API-CERT-NAME", _strCertKey);
            string strxml = @"<?xml version=""1.0"" encoding=""utf - 8""?>
<ReviseMyMessagesRequest xmlns=""urn:ebay:apis:eBLBaseComponents"">
  <RequesterCredentials>
    <eBayAuthToken>" + _strEbayAuthToken + @"</eBayAuthToken>
  </RequesterCredentials>
  <WarningLevel>High</WarningLevel>
  <MessageIDs>
    <MessageID>" + MessageUID + @"</MessageID>
  </MessageIDs>
  <Read>" + strIsRead + @"</Read>
  <Flagged>" + strIsRead + @"</Flagged>
</ReviseMyMessagesRequest>";
            request.AddParameter("text/xml", strxml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string strResponseData = response.Content;
            string strReviseMessageResult = GlobalVariablesAndMethods.GetSubString(strResponseData, "<Ack>", "</Ack>");
            if (strReviseMessageResult.ToUpper() == "SUCCESS")
            {

            }
        }

        private void CheckIsReplySent()
        {
            if (_IsReplySent)
            {
                if (!string.IsNullOrEmpty(_strSentMessageBody))
                {
                    txtReplyMessageBody.Text = _strSentMessageBody;
                    lblReplyMessageBodyShow.Visible = true;
                }

            }
            else
            {
                lblReplyMessageBodyShow.Visible = false;
            }

        }

        private void HTMLToTextConversion()
        {
            // var doc = new HtmlDocument();
            //doc.LoadHtml()
        }
        private void ResendEmailWithInvoiceAndImageUsingNewTemplate()
        {
            List<SendEmailEntity> lstEmails = new List<SendEmailEntity>();
            SendEmailEntity objSendEmail = new SendEmailEntity();
            

            if (OrderId.ToString() != new Guid().ToString())
            {
             //   _strSelectedEmailID = _strCustomerName;
                _strNumOrderID = ddlNumOrderId.SelectedValue.ToString();
                _strOrderID = OrderId.ToString();

                if (!string.IsNullOrEmpty(_strSelectedEmailID))
                {
                    objSendEmail.EmailID = _strSelectedEmailID;
                    objSendEmail.NumberOrderID = _strNumOrderID;
                    objSendEmail.OrderID = _strOrderID;
                    objSendEmail.SentSubject = _strSubject;
                    objSendEmail.ID = _strID;
                    objSendEmail.Source = _strSource;
                    objSendEmail.SubSource = _strSubSource;
                    lstEmails.Add(objSendEmail);
                    DialogResult ConfirmSendMail = MessageBox.Show("Are you sure, you want to resend email ?", "ReSend Mail", MessageBoxButtons.YesNo);
                    if (ConfirmSendMail == DialogResult.Yes)
                    {
                        objEmail.SendEmailForDispatchedOrdersForMessages(lstEmails);
                       // BindGrid();
                        MessageBox.Show("Mail(s) sent Successfully !", "Email Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                
            }

        }
        public void PopulateControls(string[] strArrGrdSelectedValues)
        {
            _strCustomerName = strArrGrdSelectedValues.ElementAt(1).ToString();
            MessageUID = strArrGrdSelectedValues.ElementAt(0).ToString();

            _strMessageType = strArrGrdSelectedValues.ElementAt(2).ToString().ToUpper();
            // webbrowser for ebay messages 
            if (_strMessageType.ToUpper() != "EMAIL")
            {
                wbMessageBody.Visible = true;

                wbMessageBody.AllowNavigation = false;
                txtMessageBody.Visible = false;
                wbMessageBody.DocumentText = strArrGrdSelectedValues.ElementAt(4).ToString();


            }
            // webbrowser for email messages containing '@members.ebay.co.uk'
            else if (_strCustomerName.ToUpper().Contains("EBAY") && _strCustomerName.ToUpper() != "EBAY")
            {
                wbMessageBody.Visible = true;
                wbMessageBody.AllowNavigation = false;
                txtMessageBody.Visible = false;

                wbMessageBody.DocumentText = strArrGrdSelectedValues.ElementAt(4).ToString();
            }
            else if (_strCustomerName.ToUpper().Contains("@MARKETPLACE.AMAZON"))
            {
                wbMessageBody.Visible = true;
                wbMessageBody.AllowNavigation = false;
                txtMessageBody.Visible = false;
                wbMessageBody.DocumentText = strArrGrdSelectedValues.ElementAt(4).ToString();
            }
            else
            {
                txtMessageBody.Visible = true;
                txtMessageBody.ReadOnly = true;

                wbMessageBody.Visible = false;
                txtMessageBody.Text = GetPlainTextFromHtml(strArrGrdSelectedValues.ElementAt(4).ToString());
            }
            _strExtMessageID = strArrGrdSelectedValues.ElementAt(10).ToString();
            _strItemID = strArrGrdSelectedValues.ElementAt(11).ToString();
            EbayUserID = strArrGrdSelectedValues.ElementAt(12).ToString();
            _IsReplySent = Convert.ToBoolean(strArrGrdSelectedValues.ElementAt(13).ToString());
            _strSentMessageBody = strArrGrdSelectedValues.ElementAt(14).ToString();

            //if (strCustomerName.Contains("<"))
            //{
            //    txtCustomerName.Text = Regex.Split(strCustomerName, @"(\<(?:.*?)\>)").ElementAt(1).ToString().Replace("<", "").Replace(">", "");
            //}
            //else {
            txtCustomerName.Text = _strCustomerName;
            //}
            txtSubject.Text = strArrGrdSelectedValues.ElementAt(3).ToString();
            //   HTMLToTextConversion();            

            AccountID = new Guid(strArrGrdSelectedValues.ElementAt(8).ToString());
        }
        private string GetPlainTextFromHtml(string strHtml)
        {
            string htmlTagPattern = "<.*?>";
            var regexCss = new Regex("(\\<script(.+?)\\</script\\>)|(\\<style(.+?)\\</style\\>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            //  htmlString = regexCss.Replace(htmlString, string.Empty);
            strHtml = Regex.Replace(strHtml, regexCss.ToString(), "", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            //htmlString = htmlString.Replace("\\s","");
            strHtml = Regex.Replace(strHtml, htmlTagPattern, string.Empty, RegexOptions.IgnorePatternWhitespace);
            strHtml = Regex.Replace(strHtml, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
            strHtml = strHtml.Replace("&nbsp;", Environment.NewLine);
            char tab = '\u0009';
            strHtml = strHtml.Replace(tab.ToString(), "");
            strHtml = strHtml.Replace("&gt;", ">");
            return strHtml;
        }
        private void BindDropDownList()
        {
            DataTable objTemplateSubject = GlobalVariablesAndMethods.GetAllMessageTemplateSubject();
            ddlTemplates.DataSource = objTemplateSubject;
            if(objTemplateSubject.Rows.Count>0)
            {
                ddlTemplates.ValueMember = "ID";
                ddlTemplates.DisplayMember = "Text";
                ddlTemplates.SelectedIndex = 0;
            }
           
        }

        private void ddlTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!IsFirstCall)
            {
                GetTemplateDetails();
            }
        }
        public DataTable GetDataToShowInTemplate(string strColumnList, Guid strOrderId)
        {

            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@ColumnsToSelect", SqlDbType.VarChar, Int32.MaxValue);
                objParam[0].Value = strColumnList;

                objParam[1] = new SqlParameter("@OrderID", SqlDbType.VarChar, 40);
                objParam[1].Value = Convert.ToString(strOrderId);

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDataToShowInEmailTemplate", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                //GlobalVariablesAndMethods.LogHistory("Error occurred while getting data from db &&" + ex.Message);
                throw;
            }
            finally
            {
                connection.Close();
            }

            return objDt;
        }

        private void GetTemplateDetails()
        {
            DataTable objDt = new DataTable();
            string strSubjectConditionId = string.Empty;
            if (ddlTemplates.SelectedValue==null)
            {
                strSubjectConditionId = new Guid().ToString();
            }
            else
            {
                strSubjectConditionId = ddlTemplates.SelectedValue.ToString();
            }
            objDt = GlobalVariablesAndMethods.GetAllMessageTemplates(strSubjectConditionId);
            int Index = ddlTemplates.SelectedIndex;
            string strDataColumnsForDatabaseUse = "", strDataColumns = "", strUserDetails = "";
            string[] strArrColumnsInSubject = new string[] { };
            bool IsEnabled = false;
            DataTable objDtDataToShowInTemplate = new DataTable();
            DataTable objUserDataToShowInTemplate = new DataTable();
            if (objDt.Rows.Count > 0)
            {
                IsEnabled = Convert.ToBoolean(objDt.Rows[0]["IsEnabled"]);
                if (IsEnabled)
                {
                    string strNewCustomerEmailToMatch = txtCustomerName.Text;
                    strTemplateText = Convert.ToString(objDt.Rows[0]["TemplateText"]);
                    if (txtCustomerName.Text.ToUpper().Contains("AMAZON"))
                    {
                        int intStartIndex = txtCustomerName.Text.IndexOf('+');
                        int intEndIndex = txtCustomerName.Text.IndexOf('@');
                        if (intStartIndex > 0)
                        {
                            strNewCustomerEmailToMatch = txtCustomerName.Text.Remove(intStartIndex, (intEndIndex - intStartIndex));
                        }
                    }
                    //get Order ID from email address if present in Pending Emails
                    GetOrderIdFromEmail(strNewCustomerEmailToMatch, _strMessageType);
                    // get user details for the template


                    //proceed if orderId != {00000000-0000-0000-0000-000000000000}

                    GetColumnNamesToGetDataFromDB(strTemplateText, ref strDataColumnsForDatabaseUse, ref strDataColumns, ref strArrColumnsInSubject, ref strUserDetails);
                ExecuteIf207Error:
                    try
                    {
                        if (!string.IsNullOrEmpty(strUserDetails))
                        {
                            objUserDataToShowInTemplate = GetUserDetailsToShowInTemplate(strUserDetails, GlobalVariablesAndMethods.UserDetails.Userid);
                        }
                        if (!string.IsNullOrEmpty(strDataColumnsForDatabaseUse))
                        {
                            objDtDataToShowInTemplate = GetDataToShowInTemplate(strDataColumnsForDatabaseUse, OrderId);
                        }

                    }
                    catch (SqlException sqlEx)
                    {
                        /*If 207 error(means invalid column call) then RemoveInvalidColumnIfExistsInString method is
                         *removing that column from list of text and database column string as well.
                         *Then executing again with valid columns
                         */
                        if (sqlEx.Number == 207)
                        {
                            // strDataColumnsForSubject = "[{Adddress.XYZ}]," + strDataColumnsForSubject + ",[{Adddress.ABC}]";
                            GlobalVariablesAndMethods.RemoveInvalidColumnIfExistsInString(ref strDataColumns, ref strDataColumnsForDatabaseUse, ref strArrColumnsInSubject, sqlEx.Message);

                            goto ExecuteIf207Error;
                        }

                    }

                    //           strTemplateText = ReplaceColumnWithData(strDataColumns, strTemplateText, objDtDataToShowInTemplate);
                    if (OrderId.ToString() != "00000000-0000-0000-0000-000000000000")
                    {
                        ddlTemplates.Enabled = true;
                        if (objUserDataToShowInTemplate.Rows.Count > 0 && objDtDataToShowInTemplate.Rows.Count > 0)
                        {
                            DataTable objDtAfterMerging = new DataTable();
                            objDtAfterMerging = MergeTablesByIndex(objDtDataToShowInTemplate, objUserDataToShowInTemplate);
                            strTemplateText = ReplaceColumnWithData(strDataColumns, strTemplateText, objDtAfterMerging);
                        }
                        else if (objDtDataToShowInTemplate.Rows.Count > 0)
                        {
                            strTemplateText = ReplaceColumnWithData(strDataColumns, strTemplateText, objDtDataToShowInTemplate);
                        }
                        else if (objUserDataToShowInTemplate.Rows.Count > 0)
                        {
                            strTemplateText = ReplaceColumnWithData(strDataColumns, strTemplateText, objUserDataToShowInTemplate);
                        }
                        txtReplyMessageBody.Text = strTemplateText;
                        txtReplyMessageBody.Enabled = true;
                        btnSendMessage.Enabled = true;
                    }
                    else
                    {
                        ddlTemplates.Enabled = false;


                    }
                }
                else
                {
                    txtReplyMessageBody.Enabled = false;
                    btnSendMessage.Enabled = false;
                }
            }
            else
            {
                txtReplyMessageBody.Text = null;
            }
        }
        public static DataTable MergeTablesByIndex(DataTable objDt1, DataTable objDt2)
        {
            DataTable objDtAfterMerge = objDt1.Clone();  // first add columns from table1
            foreach (DataColumn col in objDt2.Columns)
            {
                string newColumnName = col.ColumnName;
                int colNum = 1;
                while (objDtAfterMerge.Columns.Contains(newColumnName))
                {
                    newColumnName = string.Format("{0}_{1}", col.ColumnName, ++colNum);
                }
                objDtAfterMerge.Columns.Add(newColumnName, col.DataType);
            }
            var mergedRows = objDt1.AsEnumerable().Zip(objDt2.AsEnumerable(),
                (r1, r2) => r1.ItemArray.Concat(r2.ItemArray).ToArray());
            foreach (object[] rowFields in mergedRows)
                objDtAfterMerge.Rows.Add(rowFields);

            return objDtAfterMerge;
        }
        public DataTable GetUserDetailsToShowInTemplate(string strUserDetailsToSelect, Guid UserId)
        {
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@UserId", SqlDbType.VarChar, 50);
                objParam[0].Value = Convert.ToString(GlobalVariablesAndMethods.UserDetails.Userid);

                objParam[1] = new SqlParameter("@ColumnsToSelect", SqlDbType.VarChar, Int32.MaxValue);
                objParam[1].Value = strUserDetailsToSelect;

                objDt = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetUserDataToShowInTemplate", objParam).Tables[0];

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                conn.Close();
            }
            return objDt;
        }

        //public void ReplaceUserDetailsWithData(string strUserColumns,string strTextToReplace,DataTable objDt)
        //{
        //    string strResult = strTextToReplace;
        //    if (strUserColumns.Contains(",")) {
        //        string[] ArrColumns = strUserColumns.Split(',');
        //        for (int x = 0; x < ArrColumns.Length; x++) {
        //            strResult = strResult.Replace(ArrColumns[x], Convert.ToString(objDt.Rows[0][x]).Trim());
        //        }
        //    }

        //}

        public string ReplaceColumnWithData(string strDataColumns, string strTextToReplace, DataTable objDt)
        {
            string strResult = strTextToReplace;
            if (strDataColumns.Contains(","))
            {

                string[] ArrColumns = strDataColumns.Split(',');
                for (int x = 0; x < ArrColumns.Length; x++)
                {
                    strResult = strResult.Replace(ArrColumns[x], Convert.ToString(objDt.Rows[0][x]).Trim());
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(strDataColumns))
                {
                    strResult = strResult.Replace(strDataColumns, Convert.ToString(objDt.Rows[0][0]).Trim());
                }
            }
            return strResult;
        }
        public void GetOrderIdFromEmail(string strEmailAddress, string MessageType)
        {
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@Email", SqlDbType.VarChar, 50);
                objParam[0].Value = strEmailAddress;
                objParam[1] = new SqlParameter("@MessageType", SqlDbType.VarChar, 50);
                objParam[1].Value = MessageType.ToUpper();
                objDt = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrderIdForMessages", objParam).Tables[0];
                if (objDt.Rows.Count > 0)
                {
                    OrderId = new Guid(Convert.ToString(objDt.Rows[0]["Orderid"]));
                    _strSubject = objDt.Rows[0]["Source"].ToString();
                    _strSubSource = objDt.Rows[0]["SubSource"].ToString();
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        public void GetColumnNamesToGetDataFromDB(string strText, ref string strDataColumnsForDatabaseUse, ref string strDataColumns, ref string[] strArrColumnsInMessage, ref string strUserDetails)
        {

            string strStringToReplace = strText.ToUpper();
            string strExactColumsToReplace = strText;
            //Replacing table names with alias, because we are using those in database.
            strStringToReplace = Regex.Replace(strStringToReplace, "\\[{ORDERITEM.", "[{OI.");
            strStringToReplace = Regex.Replace(strStringToReplace, "\\[{ORDER.", "[{O.");
            strStringToReplace = Regex.Replace(strStringToReplace, "\\[{ADDRESS.", "[{A.");
            strStringToReplace = Regex.Replace(strStringToReplace, "\\[{USER.", "[{U.");
            //End of replacing table names with alias

            List<string> lstSubjectColumns = new List<string>();
        ExecuteAgainIfMoreCondition:
            if (strStringToReplace.Contains("[{") && strStringToReplace.Contains("}]"))
            {
                //Getting substring and concating strings for the columns we want to get from database
                string strSubStringWithUpperCase = GlobalVariablesAndMethods.GetSubString(strStringToReplace, "[{", "}]");
                if (strSubStringWithUpperCase.Contains("U."))
                {
                    strUserDetails = strUserDetails == "" ? strSubStringWithUpperCase + " AS [" + strSubStringWithUpperCase + "]"
                        : (!strUserDetails.Contains(strSubStringWithUpperCase)) ? strUserDetails + "," + strSubStringWithUpperCase + " AS [" + strSubStringWithUpperCase + "]"
                        : strUserDetails;
                }
                else
                {
                    strDataColumnsForDatabaseUse = strDataColumnsForDatabaseUse == "" ? strSubStringWithUpperCase + " AS [" + strSubStringWithUpperCase + "]"
                        : (!strDataColumnsForDatabaseUse.Contains(strSubStringWithUpperCase)) ? strDataColumnsForDatabaseUse + "," + strSubStringWithUpperCase + " AS [" + strSubStringWithUpperCase + "]"
                        : strDataColumnsForDatabaseUse;
                }

                //End of concatinating columns for database

                //Getting substring and concating strings for the columns exists in string to send email, so that we could find those to replace
                string strSubStringWithExactCase = strExactColumsToReplace.Substring(strExactColumsToReplace.IndexOf("[{"), strExactColumsToReplace.IndexOf("}]") - strExactColumsToReplace.IndexOf("[{") + 2);
                //End of concating strings to replace for mail

                //IF its a call for subject text for mail, then getting only columns used in subject
                //if (blIsSubjectCall)
                //{
                //    strDataColumnsForSubject = strDataColumnsForSubject == "" ? strSubStringWithExactCase
                //    : (!strDataColumnsForSubject.Contains(strSubStringWithExactCase)) ? strDataColumnsForSubject + "," + strSubStringWithExactCase
                //    : strDataColumnsForSubject;
                //    lstSubjectColumns.Add(strSubStringWithUpperCase);
                //}
                //else
                //{
                strDataColumns = strDataColumns == "" ? strSubStringWithExactCase
                                    : (!strDataColumns.Contains(strSubStringWithExactCase)) ? strDataColumns + "," + strSubStringWithExactCase
                                    : strDataColumns;
                //}
                //End of subject call test

                //Overwriting strSubStringWithUpperCase to replace that from temp string "strStringToReplace" to get next column existence
                strSubStringWithUpperCase = strStringToReplace.Substring(strStringToReplace.IndexOf("[{"), strStringToReplace.IndexOf("}]") - strStringToReplace.IndexOf("[{") + 2);
                //End of getting substring to replace temp string "strStringToReplace"

                //Replacing temp string "strStringToReplace" so that we could get next existence of column
                strStringToReplace = strStringToReplace.Replace(strSubStringWithUpperCase, "");

                /*This is the copy of exact string we are going to send as mail, we are using that
                 *so that we could get exact name of columns exists in a string.
                 */
                strExactColumsToReplace = strExactColumsToReplace.Replace(strSubStringWithExactCase, "");
                goto ExecuteAgainIfMoreCondition;
            }

            /*Converting column list to array so that we could get required columns from dataset returned by database
             * check use of that array in ReplaceColumnWithData method
            */
            strArrColumnsInMessage = lstSubjectColumns.ToArray();

            //End of to Get column names from template
        }

        private void btnSendMessage_Click(object sender, EventArgs e)
        {
            btnSendMessage.Enabled = false;
            DialogResult objDialog = MessageBox.Show("Are you sure, you want to send this message ?", "Send Message Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (objDialog == DialogResult.Yes)
            {



                if (_strMessageType.ToUpper() != "EMAIL")
                {
                    _strEbayMessageType = _strMessageType.ToUpper();

                    if (OrderId != new Guid())
                    {
                        string strSentMessageResult = SentEbayReplyToUserWithOrderPlaced();
                        if (strSentMessageResult.ToUpper() == "SUCCESS")
                        {
                            IsMailSent = 1;
                            UpdateDataForSentMessage();
                        }
                        else
                        {
                            MessageBox.Show("Some Error occurred while sending Reply to this message.", "Send Reply To Ebay Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        string strSentMessageResult = SentEbayReplyToUserWithNoOrderPlaced();
                        if (strSentMessageResult.ToUpper() == "SUCCESS")
                        {
                            IsMailSent = 1;
                            UpdateDataForSentMessage();
                        }
                        else
                        {
                            MessageBox.Show("Some Error occurred while sending Reply to this message.", "Send Reply To Ebay Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }


                }
                else
                {


                    if (objDialog.Equals(DialogResult.Yes))
                    {
                        IsMailSent = SentMessage();
                        if (IsMailSent == 1)
                        {
                            UpdateDataForSentMessage();
                        }
                        this.Close();
                    }
                }
            }
            btnSendMessage.Enabled = true;

            //Form frmMessageFolders = Application.OpenForms["MessageFolders"];
            //if (frmMessageFolders != null)
            //{
            //    frmMessageFolders.Close();
            //    MessageFolders objMessageFolders = new MessageFolders();
            //    objMessageFolders.Show();
            //}




        }

        private void GetTokenAndKeys()
        {
            DataTable objDt = new DataTable();
            objDt = GetEbayUserTokenWithAppKeys("GET");
            DataView dv = new DataView(objDt);
            dv.RowFilter = "EbayUserID='" + EbayUserID.ToString() + "'";
            DataTable objDtGetTokenAndAppKeys = dv.ToTable();

            _strAppKey = objDtGetTokenAndAppKeys.Rows[0]["AppKey"].ToString();
            _strCertKey = objDtGetTokenAndAppKeys.Rows[0]["CertKey"].ToString();
            _strDevKey = objDtGetTokenAndAppKeys.Rows[0]["DevKey"].ToString();
            _strEbayAuthToken = objDtGetTokenAndAppKeys.Rows[0]["EbayAuthToken"].ToString();
            _intIsSandBox = Convert.ToInt16(Convert.ToBoolean(objDtGetTokenAndAppKeys.Rows[0]["IsEbaySandbox"].ToString()));

        }
        //reply api call to those users who has not placed any order
        private string SentEbayReplyToUserWithOrderPlaced()
        {
            // GetTokenAndKeys();
            //objDtGetTokenAndAppKeys.Rows
            //SentEbayReplyToUserWithOrderPlaced();
            var client = new RestClient("https://www.google.com");
            if (Convert.ToBoolean(_intIsSandBox))
            {
                client = new RestClient("https://api.sandbox.ebay.com/ws/api.dll");
            }
            else
            {
                client = new RestClient("https://api.ebay.com/ws/api.dll");
            }



            var request = new RestRequest(Method.POST);

            request.AddHeader("X-EBAY-API-SITEID", "3");
            request.AddHeader("X-EBAY-API-COMPATIBILITY-LEVEL", "1073");
            request.AddHeader("X-EBAY-API-CALL-NAME", "AddMemberMessageRTQ");
            request.AddHeader("X-EBAY-API-APP-NAME", _strAppKey);
            request.AddHeader("X-EBAY-API-DEV-NAME", _strDevKey);
            request.AddHeader("X-EBAY-API-CERT-NAME", _strCertKey);
            string strxml = @"<?xml version=""1.0"" encoding=""utf - 8""?>  
<AddMemberMessageAAQToPartnerRequest xmlns=""urn:ebay:apis:eBLBaseComponents"">
  <RequesterCredentials>
    <eBayAuthToken>" + _strEbayAuthToken + @"</eBayAuthToken>
  </RequesterCredentials>
  
  <ItemID>" + _strItemID + @"</ItemID>
  <MemberMessage>
    <Subject>" + txtSubject.Text + @"</Subject>
    <Body>" + txtReplyMessageBody.Text + @"</Body>
   
    <QuestionType>General</QuestionType>
   
    <RecipientID>" + txtCustomerName.Text + @"</RecipientID>
  </MemberMessage>
</AddMemberMessageAAQToPartnerRequest>";
            request.AddParameter("text/xml", strxml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string strResponseData = response.Content;
            string strSentMessageResult = GlobalVariablesAndMethods.GetSubString(strResponseData, "<Ack>", "</Ack>");
            return strSentMessageResult;
        }
        // Reply api call to those users who has placed an order.
        private string SentEbayReplyToUserWithNoOrderPlaced()
        {

            // GetTokenAndKeys();
            var client = new RestClient("https://www.google.com");
            if (Convert.ToBoolean(_intIsSandBox))
            {
                client = new RestClient("https://api.sandbox.ebay.com/ws/api.dll");
            }
            else
            {
                client = new RestClient("https://api.ebay.com/ws/api.dll");
            }



            var request = new RestRequest(Method.POST);

            request.AddHeader("X-EBAY-API-SITEID", "3");
            request.AddHeader("X-EBAY-API-COMPATIBILITY-LEVEL", "1073");
            request.AddHeader("X-EBAY-API-CALL-NAME", "AddMemberMessageRTQ");
            request.AddHeader("X-EBAY-API-APP-NAME", _strAppKey);
            request.AddHeader("X-EBAY-API-DEV-NAME", _strDevKey);
            request.AddHeader("X-EBAY-API-CERT-NAME", _strCertKey);
            string strxml = @"<?xml version=""1.0"" encoding=""utf - 8""?>
    <AddMemberMessageRTQRequest xmlns = ""urn:ebay:apis:eBLBaseComponents"">
    
      <RequesterCredentials>
    
        <eBayAuthToken>" + _strEbayAuthToken + @"</eBayAuthToken>
       
         </RequesterCredentials>
    
       
         <ItemID>" + _strItemID + @"</ItemID>
       
         <MemberMessage>
       
           <Body>" +
       txtReplyMessageBody.Text
    + @"</Body>
    <DisplayToPublic>" + chkDisplayReplyToPublic.Checked.ToString() + @"</DisplayToPublic>
    <EmailCopyToSender>true</EmailCopyToSender>
   
    <ParentMessageID>" + _strExtMessageID + @"</ParentMessageID>

   
 
     <RecipientID>" + txtCustomerName.Text + @"</RecipientID>
 0
   </MemberMessage>
 </AddMemberMessageRTQRequest> ";
            request.AddParameter("text/xml", strxml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string strResponseData = response.Content;
            string strSentMessageResult = GlobalVariablesAndMethods.GetSubString(strResponseData, "<Ack>", "</Ack>");
            return strSentMessageResult;
        }

        private DataTable GetEbayUserTokenWithAppKeys(string strMode, string strUserName = "", string strEbayAuthToken = "")
        {

            DataTable objDt = new DataTable();
            DataSet objDs = new DataSet();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[6];
                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();
                objParam[1] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = new Guid();
                objParam[2] = new SqlParameter("@AccountID", SqlDbType.UniqueIdentifier);
                objParam[2].Value = new Guid();
                objParam[3] = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
                objParam[3].Value = strUserName;
                objParam[4] = new SqlParameter("@EbayAuthToken", SqlDbType.VarChar, int.MaxValue);
                objParam[4].Value = strEbayAuthToken;
                objParam[5] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[5].Direction = ParameterDirection.Output;

                objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetEbayTokenWithUserName", objParam);
                string strResult = objParam[5].Value.ToString();
                if (strResult == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return objDt;
        }
        private int SentMessage()
        {
            DataTable objDt = new DataTable();
            objDt = GlobalVariablesAndMethods.GetMessageAccount("ALL");
            DataTable objDtAdditionalEmailAccount = new DataTable();
            objDtAdditionalEmailAccount = GlobalVariablesAndMethods.GetEmailAccounts();
            DataView dvAdditionalAccount = new DataView(objDtAdditionalEmailAccount);
            //need to change this filter to default instead of defaulttest
            dvAdditionalAccount.RowFilter = "UserName= 'oliver@electrolve.co.uk'";
            DataTable objDtSmtpDetails = dvAdditionalAccount.ToTable();
            if (objDt.Rows.Count > 0)
            {
                //DataView dv = new DataView();
                objDt = objDt.AsEnumerable().Where(x => x.Field<Guid>("ID") == AccountID).CopyToDataTable();

                string strMessageBody = txtMessageBody.Text.ToString();
                try
                {
                    MailMessage Mail = new MailMessage();
                    // SmtpClient SmtpServer = new SmtpClient("mail.electrolve.co.uk", 587);

                    SmtpClient SmtpServer = new SmtpClient(Convert.ToString(objDtSmtpDetails.Rows[0]["SMTP_Server"]), Convert.ToInt32(objDtSmtpDetails.Rows[0]["Port"]));
                    Mail.Sender = new MailAddress(Convert.ToString(objDtSmtpDetails.Rows[0]["UserName"]));
                    //SmtpClient SmtpServer = new SmtpClient(objDt.Rows[0]["SmtpHostName"].ToString(),587);
                    Mail.From = new MailAddress(objDt.Rows[0]["UserName"].ToString());
                    Mail.To.Add(txtCustomerName.Text.ToString());
                    Mail.Subject = txtSubject.Text;
                    //Mail.ReplyTo = ;
                    //Mail

                    Mail.Body = txtReplyMessageBody.Text + Environment.NewLine + Environment.NewLine + "-----------------------------###############-----------------------------" + Environment.NewLine + Environment.NewLine + strMessageBody;
                    //  Mail.Sender = new MailAddress("oliver@electrolve.co.uk");
                    //Mail.From = new MailAddress("customerservice@etwist.co.uk");

                    //SmtpServer.UseDefaultCredentials = true;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(Convert.ToString(objDtSmtpDetails.Rows[0]["UserName"]), Convert.ToString(objDtSmtpDetails.Rows[0]["Password"]));
                    //SmtpServer.Credentials = new System.Net.NetworkCredential("supporderdispatch@gmail.com", "OrderDispatch@2018");
                    SmtpServer.EnableSsl = Convert.ToBoolean(objDtSmtpDetails.Rows[0]["IsSSL"]);
                    //  SmtpServer.Credentials = new System.Net.NetworkCredential("oliver@electrolve.co.uk", "FUJnd78BPH321!");
                    //SmtpServer.Credentials = new System.Net.NetworkCredential(objDt.Rows[0]["UserName"].ToString(), objDt.Rows[0]["Password"].ToString());
                    // SmtpServer.EnableSsl = false;
                    //SmtpServer.UseDefaultCredentials = true;
                    //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                    SmtpServer.Send(Mail);
                    IsMailSent = 1;
                    return IsMailSent;
                }
                catch (SmtpException smtpEx)
                {
                    //Log the exception and again execute to send rest of the email
                    GlobalVariablesAndMethods.LogHistory("SmtpException Error occrred while sending mail to Email:-" + txtCustomerName.Text + " MessageUID:-" + MessageUID.ToString() + "&&Error :- " + smtpEx.Message);
                    MessageBox.Show("SmtpException Error occrred while sending message", "Error While Sending Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    IsMailSent = 0;
                    return IsMailSent;
                }
                catch (AuthenticationException AuthEx)
                {
                    GlobalVariablesAndMethods.LogHistory("AuthenticationException Error occrred while sending mail to Email:-" + txtCustomerName.Text + " MessageUID:-" + MessageUID.ToString() + "&&Error :- " + AuthEx.Message);
                    MessageBox.Show("AuthenticationException Error occrred while sending message", "Error While Sending Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    IsMailSent = 0;
                    return IsMailSent;
                }
                catch (Exception)
                {
                    IsMailSent = 0;
                    return IsMailSent;
                }
            }
            else
            {
                MessageBox.Show("Account Not Found", "Error while Sending Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                return -1;
        }

        private void UpdateDataForSentMessage()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[4];
                objParam[0] = new SqlParameter("@MessageUID", SqlDbType.VarChar, 50);
                objParam[0].Value = MessageUID.ToString();

                objParam[1] = new SqlParameter("@IsReplySent", SqlDbType.Bit);
                objParam[1].Value = Convert.ToByte(IsMailSent);

                objParam[2] = new SqlParameter("@SentMessageBody", SqlDbType.VarChar, Int32.MaxValue);
                objParam[2].Value = txtReplyMessageBody.Text.ToString();

                objParam[3] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[3].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "UpdateDataForSentMessage", objParam);

                string strResult = objParam[3].Value.ToString().ToUpper();
                if (strResult.Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                else
                {
                    MessageBox.Show("Message Sent", "Sent Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                conn.Close();
            }
        }
        
        private void btnEmailInvoice_Click(object sender, EventArgs e)
        {
            CreateCustomDataSource();
            ResendEmailWithInvoiceAndImageUsingNewTemplate();
        }
        private void CreateCustomDataSource()
        {
            DataTable objDtCondition = new DataTable();
            DataView dvFilterCondition = new DataView();
            DataTable objDtTemplateDetails = new DataTable();
            DataTable objDtDataToShowInTemplate = new DataTable();
            Guid OrderId = new Guid();
            int intRowCount = 0;
            if (objDtCondition.Rows.Count == 0)
            {
                objDtCondition = GlobalVariablesAndMethods.GetEmailConditionsForTemplateOnMessageBody();
            }
            string strIDs = "", strMailID = "";
        ExecuteIfErrorInMainLoop:
            try
            {
               
                
                    string strDataColumns = "", strDataColumnsForSubject = "", strDataColumnsForDatabaseUse = "", strSubject = "";
                    string[] strArrColumnsInSubject = new string[] { };
                    dvFilterCondition = new DataView(objDtCondition);
                    Guid ConditionID = new Guid();
                    Guid TemplateID = new Guid();
                    //string strSource = Convert.ToString(objDtPendingEmails.Rows[i]["Source"]);
                    //string strSubSource = Convert.ToString(objDtPendingEmails.Rows[i]["SubSource"]);
                    //OrderId = new Guid(Convert.ToString(objDtPendingEmails.Rows[i]["OrderId"]));
                   //strMailID = Convert.ToString(objDtPendingEmails.Rows[i]["ID"]);
                    ConditionID = objEmail.GetConditionIDByConditionText(_strSource, _strSubSource, dvFilterCondition);
                ExecuteAgainIfTemplateIsDisabled:
                    objDtTemplateDetails = GlobalVariablesAndMethods.GetTemplateDetailsByTemplateIdForMessages(TemplateID, ConditionID);
                    bool blIsEnabled = false;
                    if (objDtTemplateDetails.Rows.Count > 0)
                    {
                        blIsEnabled = Convert.ToBoolean(objDtTemplateDetails.Rows[0]["IsEnabled"]);
                    }
                    else
                    {
                        ConditionID = new Guid();
                        goto ExecuteAgainIfTemplateIsDisabled;
                    }
                    /* ConditionID can only be equal to 00000000-0000-0000-0000-000000000000 when either matched template is disabled 
                     * or don't have any matched template
                     * So in that case we need to skip those emails
                     */
                    if (ConditionID.ToString() == "00000000-0000-0000-0000-000000000000" && !blIsEnabled)
                    {
                        strIDs = strIDs == "" ? strMailID : strIDs + "," + strMailID;
                    }
                    /*Checking if this template is enabled or not, if not then setting ConditionID to default value of guid,
                      and executing GetTemplateDetailsByTemplateId again to get default template and settings
                     */
                    if (blIsEnabled)
                    {
                        strSubject = Convert.ToString(objDtTemplateDetails.Rows[0]["Subject"]);
                        if (strSubject.Contains("[{"))
                        {
                            objEmail.GetColumnNamesToGetDataFromDB(strSubject, ref strDataColumnsForDatabaseUse, ref strDataColumnsForSubject, ref strDataColumns, ref strArrColumnsInSubject, true);
                        ExecuteIf207Error:
                            try
                            {
                                //strDataColumnsForDatabaseUse ="A.XYZ AS [A.XYZ],"+ strDataColumnsForDatabaseUse + ",A.ABC AS [A.ABC]";
                                objDtDataToShowInTemplate = objEmail.GetDataToShowInTemplate(strDataColumnsForDatabaseUse, OrderId.ToString());
                            }
                            catch (SqlException sqlEx)
                            {
                                /*If 207 error(means invalid column call) then RemoveInvalidColumnIfExistsInString method is
                                 *removing that column from list of text and database column string as well.
                                 * Then executing again with valid columns
                                 */
                                if (sqlEx.Number == 207)
                                {
                                    string strStringToCompare = strDataColumns;
                                    GlobalVariablesAndMethods.RemoveInvalidColumnIfExistsInString(ref strDataColumnsForSubject, ref strDataColumnsForDatabaseUse, ref strArrColumnsInSubject, sqlEx.Message);
                                    goto ExecuteIf207Error;
                                }
                            }
                            if (objDtDataToShowInTemplate.Rows.Count > 0)
                            {
                                strSubject = objEmail.ReplaceColumnWithData(strDataColumns, strDataColumnsForSubject, strSubject, objDtDataToShowInTemplate, true, strArrColumnsInSubject);
                            }
                        }
                        _strSubject= strSubject;
                        intRowCount++;
                    }
                    else if (ConditionID.ToString() != "00000000-0000-0000-0000-000000000000")
                    {
                        ConditionID = new Guid();//Setting condition id to default
                        goto ExecuteAgainIfTemplateIsDisabled;
                    }
                
            }
            catch (Exception ex)
            {
                intRowCount++;
                strIDs = strIDs == "" ? strMailID : strIDs + "," + strMailID;
                GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
                GlobalVariablesAndMethods.OrderIdToLog = OrderId;
                GlobalVariablesAndMethods.LogHistory("Error occurred while creating custom data source to bind pending email screen&&" + ex.Message);
                goto ExecuteIfErrorInMainLoop;
            }
            finally
            {
                if (strIDs != "")
                {
                    //string[] strArrMailsToSkip = strIDs.Split(',');
                    //for (int i = 0; i < strArrMailsToSkip.Length; i++)
                    //{
                    //    DataView dv = new DataView(objDtPendingEmails);
                    //    dv.RowFilter = "ID <> '" + strArrMailsToSkip[i] + "'";
                    //    objDtPendingEmails = dv.ToTable();
                    //}
                }
            }
            // blLoadAllData = false;
            
        }
        private void btnTemplateSettings_Click(object sender, EventArgs e)
        {
            

        }

    }

}
