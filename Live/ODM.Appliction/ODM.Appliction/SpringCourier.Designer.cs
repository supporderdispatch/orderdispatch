﻿namespace ODM.Appliction
{
    partial class SpringSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpringSettings));
            this.lblLiveLoginName = new System.Windows.Forms.Label();
            this.txtLiveLoginName = new System.Windows.Forms.TextBox();
            this.txtLivePswd = new System.Windows.Forms.TextBox();
            this.lblLivePswd = new System.Windows.Forms.Label();
            this.txtLiveKey = new System.Windows.Forms.TextBox();
            this.lblLiveKey = new System.Windows.Forms.Label();
            this.txtTestLoginName = new System.Windows.Forms.TextBox();
            this.lblTestLoginName = new System.Windows.Forms.Label();
            this.txtTestKey = new System.Windows.Forms.TextBox();
            this.lblTestKey = new System.Windows.Forms.Label();
            this.txtTestPswd = new System.Windows.Forms.TextBox();
            this.lblTestPswd = new System.Windows.Forms.Label();
            this.chkIsLive = new System.Windows.Forms.CheckBox();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.ddlPrinter = new System.Windows.Forms.ComboBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpBoxShippingSettings = new System.Windows.Forms.GroupBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSaveClose = new System.Windows.Forms.Button();
            this.grdMappedServices = new System.Windows.Forms.DataGridView();
            this.lblLiveLoginRequired = new System.Windows.Forms.Label();
            this.lblLivePswdRequired = new System.Windows.Forms.Label();
            this.lblLiveKeyRequired = new System.Windows.Forms.Label();
            this.lblTestLoginRequired = new System.Windows.Forms.Label();
            this.lblTestPswdRequired = new System.Windows.Forms.Label();
            this.lblTestKeyRequired = new System.Windows.Forms.Label();
            this.lblWeightInvalid = new System.Windows.Forms.Label();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblWeightRequired = new System.Windows.Forms.Label();
            this.chkIsBoseLogin = new System.Windows.Forms.CheckBox();
            this.btnManifest = new System.Windows.Forms.Button();
            this.ddlManifestPrinter = new System.Windows.Forms.ComboBox();
            this.lblSpringManifestPrinter = new System.Windows.Forms.Label();
            this.grpBoxShippingSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLiveLoginName
            // 
            this.lblLiveLoginName.AutoSize = true;
            this.lblLiveLoginName.Location = new System.Drawing.Point(13, 13);
            this.lblLiveLoginName.Name = "lblLiveLoginName";
            this.lblLiveLoginName.Size = new System.Drawing.Size(87, 13);
            this.lblLiveLoginName.TabIndex = 0;
            this.lblLiveLoginName.Text = "Live Login Name";
            // 
            // txtLiveLoginName
            // 
            this.txtLiveLoginName.Location = new System.Drawing.Point(116, 13);
            this.txtLiveLoginName.Name = "txtLiveLoginName";
            this.txtLiveLoginName.Size = new System.Drawing.Size(191, 20);
            this.txtLiveLoginName.TabIndex = 1;
            // 
            // txtLivePswd
            // 
            this.txtLivePswd.Location = new System.Drawing.Point(116, 41);
            this.txtLivePswd.Name = "txtLivePswd";
            this.txtLivePswd.Size = new System.Drawing.Size(191, 20);
            this.txtLivePswd.TabIndex = 3;
            // 
            // lblLivePswd
            // 
            this.lblLivePswd.AutoSize = true;
            this.lblLivePswd.Location = new System.Drawing.Point(12, 44);
            this.lblLivePswd.Name = "lblLivePswd";
            this.lblLivePswd.Size = new System.Drawing.Size(76, 13);
            this.lblLivePswd.TabIndex = 2;
            this.lblLivePswd.Text = "Live Password";
            // 
            // txtLiveKey
            // 
            this.txtLiveKey.Location = new System.Drawing.Point(116, 69);
            this.txtLiveKey.Name = "txtLiveKey";
            this.txtLiveKey.Size = new System.Drawing.Size(191, 20);
            this.txtLiveKey.TabIndex = 5;
            // 
            // lblLiveKey
            // 
            this.lblLiveKey.AutoSize = true;
            this.lblLiveKey.Location = new System.Drawing.Point(12, 75);
            this.lblLiveKey.Name = "lblLiveKey";
            this.lblLiveKey.Size = new System.Drawing.Size(98, 13);
            this.lblLiveKey.TabIndex = 4;
            this.lblLiveKey.Text = "Live Activation Key";
            // 
            // txtTestLoginName
            // 
            this.txtTestLoginName.Location = new System.Drawing.Point(514, 10);
            this.txtTestLoginName.Name = "txtTestLoginName";
            this.txtTestLoginName.Size = new System.Drawing.Size(191, 20);
            this.txtTestLoginName.TabIndex = 9;
            // 
            // lblTestLoginName
            // 
            this.lblTestLoginName.AutoSize = true;
            this.lblTestLoginName.Location = new System.Drawing.Point(395, 13);
            this.lblTestLoginName.Name = "lblTestLoginName";
            this.lblTestLoginName.Size = new System.Drawing.Size(88, 13);
            this.lblTestLoginName.TabIndex = 8;
            this.lblTestLoginName.Text = "Test Login Name";
            // 
            // txtTestKey
            // 
            this.txtTestKey.Location = new System.Drawing.Point(514, 76);
            this.txtTestKey.Name = "txtTestKey";
            this.txtTestKey.Size = new System.Drawing.Size(191, 20);
            this.txtTestKey.TabIndex = 13;
            // 
            // lblTestKey
            // 
            this.lblTestKey.AutoSize = true;
            this.lblTestKey.Location = new System.Drawing.Point(395, 76);
            this.lblTestKey.Name = "lblTestKey";
            this.lblTestKey.Size = new System.Drawing.Size(99, 13);
            this.lblTestKey.TabIndex = 12;
            this.lblTestKey.Text = "Test Activation Key";
            // 
            // txtTestPswd
            // 
            this.txtTestPswd.Location = new System.Drawing.Point(514, 44);
            this.txtTestPswd.Name = "txtTestPswd";
            this.txtTestPswd.Size = new System.Drawing.Size(191, 20);
            this.txtTestPswd.TabIndex = 11;
            // 
            // lblTestPswd
            // 
            this.lblTestPswd.AutoSize = true;
            this.lblTestPswd.Location = new System.Drawing.Point(395, 44);
            this.lblTestPswd.Name = "lblTestPswd";
            this.lblTestPswd.Size = new System.Drawing.Size(71, 13);
            this.lblTestPswd.TabIndex = 10;
            this.lblTestPswd.Text = "Test Pssword";
            // 
            // chkIsLive
            // 
            this.chkIsLive.AutoSize = true;
            this.chkIsLive.Location = new System.Drawing.Point(116, 106);
            this.chkIsLive.Name = "chkIsLive";
            this.chkIsLive.Size = new System.Drawing.Size(57, 17);
            this.chkIsLive.TabIndex = 15;
            this.chkIsLive.Text = "Is Live";
            this.chkIsLive.UseVisualStyleBackColor = true;
            // 
            // lblPrinter
            // 
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Location = new System.Drawing.Point(395, 136);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(70, 13);
            this.lblPrinter.TabIndex = 16;
            this.lblPrinter.Text = "Select Printer";
            // 
            // ddlPrinter
            // 
            this.ddlPrinter.FormattingEnabled = true;
            this.ddlPrinter.Location = new System.Drawing.Point(514, 133);
            this.ddlPrinter.Name = "ddlPrinter";
            this.ddlPrinter.Size = new System.Drawing.Size(191, 21);
            this.ddlPrinter.TabIndex = 17;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(602, 232);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(164, 29);
            this.btnDelete.TabIndex = 19;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(416, 232);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(180, 29);
            this.btnAdd.TabIndex = 18;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grpBoxShippingSettings
            // 
            this.grpBoxShippingSettings.Controls.Add(this.lblMessage);
            this.grpBoxShippingSettings.Controls.Add(this.btnCancel);
            this.grpBoxShippingSettings.Controls.Add(this.btnSaveClose);
            this.grpBoxShippingSettings.Controls.Add(this.grdMappedServices);
            this.grpBoxShippingSettings.Location = new System.Drawing.Point(12, 267);
            this.grpBoxShippingSettings.Name = "grpBoxShippingSettings";
            this.grpBoxShippingSettings.Size = new System.Drawing.Size(787, 229);
            this.grpBoxShippingSettings.TabIndex = 20;
            this.grpBoxShippingSettings.TabStop = false;
            this.grpBoxShippingSettings.Text = "Shipping Mapping";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(339, 75);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(92, 13);
            this.lblMessage.TabIndex = 15;
            this.lblMessage.Text = "No Record Found";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(590, 193);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(180, 29);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSaveClose
            // 
            this.btnSaveClose.Location = new System.Drawing.Point(404, 194);
            this.btnSaveClose.Name = "btnSaveClose";
            this.btnSaveClose.Size = new System.Drawing.Size(180, 29);
            this.btnSaveClose.TabIndex = 13;
            this.btnSaveClose.Text = "Save && Close";
            this.btnSaveClose.UseVisualStyleBackColor = true;
            this.btnSaveClose.Click += new System.EventHandler(this.btnSaveClose_Click);
            // 
            // grdMappedServices
            // 
            this.grdMappedServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMappedServices.Location = new System.Drawing.Point(0, 19);
            this.grdMappedServices.Name = "grdMappedServices";
            this.grdMappedServices.Size = new System.Drawing.Size(781, 150);
            this.grdMappedServices.TabIndex = 12;
            // 
            // lblLiveLoginRequired
            // 
            this.lblLiveLoginRequired.AutoSize = true;
            this.lblLiveLoginRequired.ForeColor = System.Drawing.Color.Red;
            this.lblLiveLoginRequired.Location = new System.Drawing.Point(313, 13);
            this.lblLiveLoginRequired.Name = "lblLiveLoginRequired";
            this.lblLiveLoginRequired.Size = new System.Drawing.Size(50, 13);
            this.lblLiveLoginRequired.TabIndex = 21;
            this.lblLiveLoginRequired.Text = "Required";
            this.lblLiveLoginRequired.Visible = false;
            // 
            // lblLivePswdRequired
            // 
            this.lblLivePswdRequired.AutoSize = true;
            this.lblLivePswdRequired.ForeColor = System.Drawing.Color.Red;
            this.lblLivePswdRequired.Location = new System.Drawing.Point(313, 40);
            this.lblLivePswdRequired.Name = "lblLivePswdRequired";
            this.lblLivePswdRequired.Size = new System.Drawing.Size(50, 13);
            this.lblLivePswdRequired.TabIndex = 22;
            this.lblLivePswdRequired.Text = "Required";
            this.lblLivePswdRequired.Visible = false;
            // 
            // lblLiveKeyRequired
            // 
            this.lblLiveKeyRequired.AutoSize = true;
            this.lblLiveKeyRequired.ForeColor = System.Drawing.Color.Red;
            this.lblLiveKeyRequired.Location = new System.Drawing.Point(313, 72);
            this.lblLiveKeyRequired.Name = "lblLiveKeyRequired";
            this.lblLiveKeyRequired.Size = new System.Drawing.Size(50, 13);
            this.lblLiveKeyRequired.TabIndex = 23;
            this.lblLiveKeyRequired.Text = "Required";
            this.lblLiveKeyRequired.Visible = false;
            // 
            // lblTestLoginRequired
            // 
            this.lblTestLoginRequired.AutoSize = true;
            this.lblTestLoginRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTestLoginRequired.Location = new System.Drawing.Point(711, 13);
            this.lblTestLoginRequired.Name = "lblTestLoginRequired";
            this.lblTestLoginRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTestLoginRequired.TabIndex = 24;
            this.lblTestLoginRequired.Text = "Required";
            this.lblTestLoginRequired.Visible = false;
            // 
            // lblTestPswdRequired
            // 
            this.lblTestPswdRequired.AutoSize = true;
            this.lblTestPswdRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTestPswdRequired.Location = new System.Drawing.Point(711, 45);
            this.lblTestPswdRequired.Name = "lblTestPswdRequired";
            this.lblTestPswdRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTestPswdRequired.TabIndex = 25;
            this.lblTestPswdRequired.Text = "Required";
            this.lblTestPswdRequired.Visible = false;
            // 
            // lblTestKeyRequired
            // 
            this.lblTestKeyRequired.AutoSize = true;
            this.lblTestKeyRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTestKeyRequired.Location = new System.Drawing.Point(711, 79);
            this.lblTestKeyRequired.Name = "lblTestKeyRequired";
            this.lblTestKeyRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTestKeyRequired.TabIndex = 26;
            this.lblTestKeyRequired.Text = "Required";
            this.lblTestKeyRequired.Visible = false;
            // 
            // lblWeightInvalid
            // 
            this.lblWeightInvalid.AutoSize = true;
            this.lblWeightInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblWeightInvalid.Location = new System.Drawing.Point(711, 107);
            this.lblWeightInvalid.Name = "lblWeightInvalid";
            this.lblWeightInvalid.Size = new System.Drawing.Size(65, 13);
            this.lblWeightInvalid.TabIndex = 32;
            this.lblWeightInvalid.Text = "Invalid Input";
            this.lblWeightInvalid.Visible = false;
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(514, 104);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(191, 20);
            this.txtWeight.TabIndex = 30;
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Location = new System.Drawing.Point(397, 103);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(78, 13);
            this.lblWeight.TabIndex = 31;
            this.lblWeight.Text = "Default Weight";
            // 
            // lblWeightRequired
            // 
            this.lblWeightRequired.AutoSize = true;
            this.lblWeightRequired.ForeColor = System.Drawing.Color.Red;
            this.lblWeightRequired.Location = new System.Drawing.Point(711, 107);
            this.lblWeightRequired.Name = "lblWeightRequired";
            this.lblWeightRequired.Size = new System.Drawing.Size(50, 13);
            this.lblWeightRequired.TabIndex = 33;
            this.lblWeightRequired.Text = "Required";
            this.lblWeightRequired.Visible = false;
            // 
            // chkIsBoseLogin
            // 
            this.chkIsBoseLogin.AutoSize = true;
            this.chkIsBoseLogin.Location = new System.Drawing.Point(116, 130);
            this.chkIsBoseLogin.Name = "chkIsBoseLogin";
            this.chkIsBoseLogin.Size = new System.Drawing.Size(90, 17);
            this.chkIsBoseLogin.TabIndex = 34;
            this.chkIsBoseLogin.Text = "Is Bose Login";
            this.chkIsBoseLogin.UseVisualStyleBackColor = true;
            // 
            // btnManifest
            // 
            this.btnManifest.Location = new System.Drawing.Point(230, 232);
            this.btnManifest.Name = "btnManifest";
            this.btnManifest.Size = new System.Drawing.Size(180, 29);
            this.btnManifest.TabIndex = 35;
            this.btnManifest.Text = "Manifest";
            this.btnManifest.UseVisualStyleBackColor = true;
            this.btnManifest.Click += new System.EventHandler(this.btnManifest_Click);
            // 
            // ddlManifestPrinter
            // 
            this.ddlManifestPrinter.FormattingEnabled = true;
            this.ddlManifestPrinter.Location = new System.Drawing.Point(514, 169);
            this.ddlManifestPrinter.Name = "ddlManifestPrinter";
            this.ddlManifestPrinter.Size = new System.Drawing.Size(191, 21);
            this.ddlManifestPrinter.TabIndex = 37;
            // 
            // lblSpringManifestPrinter
            // 
            this.lblSpringManifestPrinter.AutoSize = true;
            this.lblSpringManifestPrinter.Location = new System.Drawing.Point(395, 172);
            this.lblSpringManifestPrinter.Name = "lblSpringManifestPrinter";
            this.lblSpringManifestPrinter.Size = new System.Drawing.Size(113, 13);
            this.lblSpringManifestPrinter.TabIndex = 36;
            this.lblSpringManifestPrinter.Text = "Select Manifest Printer";
            // 
            // SpringSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 498);
            this.Controls.Add(this.ddlManifestPrinter);
            this.Controls.Add(this.lblSpringManifestPrinter);
            this.Controls.Add(this.btnManifest);
            this.Controls.Add(this.chkIsBoseLogin);
            this.Controls.Add(this.lblWeightRequired);
            this.Controls.Add(this.lblWeightInvalid);
            this.Controls.Add(this.txtWeight);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.lblTestKeyRequired);
            this.Controls.Add(this.lblTestPswdRequired);
            this.Controls.Add(this.lblTestLoginRequired);
            this.Controls.Add(this.lblLiveKeyRequired);
            this.Controls.Add(this.lblLivePswdRequired);
            this.Controls.Add(this.lblLiveLoginRequired);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpBoxShippingSettings);
            this.Controls.Add(this.ddlPrinter);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.chkIsLive);
            this.Controls.Add(this.txtTestKey);
            this.Controls.Add(this.lblTestKey);
            this.Controls.Add(this.txtTestPswd);
            this.Controls.Add(this.lblTestPswd);
            this.Controls.Add(this.txtTestLoginName);
            this.Controls.Add(this.lblTestLoginName);
            this.Controls.Add(this.txtLiveKey);
            this.Controls.Add(this.lblLiveKey);
            this.Controls.Add(this.txtLivePswd);
            this.Controls.Add(this.lblLivePswd);
            this.Controls.Add(this.txtLiveLoginName);
            this.Controls.Add(this.lblLiveLoginName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SpringSettings";
            this.Text = "Spring Courier Settings";
            this.grpBoxShippingSettings.ResumeLayout(false);
            this.grpBoxShippingSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLiveLoginName;
        private System.Windows.Forms.TextBox txtLiveLoginName;
        private System.Windows.Forms.TextBox txtLivePswd;
        private System.Windows.Forms.Label lblLivePswd;
        private System.Windows.Forms.TextBox txtLiveKey;
        private System.Windows.Forms.Label lblLiveKey;
        private System.Windows.Forms.TextBox txtTestLoginName;
        private System.Windows.Forms.Label lblTestLoginName;
        private System.Windows.Forms.TextBox txtTestKey;
        private System.Windows.Forms.Label lblTestKey;
        private System.Windows.Forms.TextBox txtTestPswd;
        private System.Windows.Forms.Label lblTestPswd;
        private System.Windows.Forms.CheckBox chkIsLive;
        private System.Windows.Forms.Label lblPrinter;
        private System.Windows.Forms.ComboBox ddlPrinter;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grpBoxShippingSettings;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSaveClose;
        private System.Windows.Forms.DataGridView grdMappedServices;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblLiveLoginRequired;
        private System.Windows.Forms.Label lblLivePswdRequired;
        private System.Windows.Forms.Label lblLiveKeyRequired;
        private System.Windows.Forms.Label lblTestLoginRequired;
        private System.Windows.Forms.Label lblTestPswdRequired;
        private System.Windows.Forms.Label lblTestKeyRequired;
        private System.Windows.Forms.Label lblWeightInvalid;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblWeightRequired;
        private System.Windows.Forms.CheckBox chkIsBoseLogin;
        private System.Windows.Forms.Button btnManifest;
        private System.Windows.Forms.ComboBox ddlManifestPrinter;
        private System.Windows.Forms.Label lblSpringManifestPrinter;
    }
}