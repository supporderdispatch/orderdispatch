﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using ODM.Entities;
using ODM.Core;

namespace ODM.Appliction
{
    public partial class GetOrderData : Form
    {
        public string BarCode;

        public GlobalVariablesAndMethods GlobalVars = new GlobalVariablesAndMethods();
        public DataTable objDt = new DataTable();
        public List<int> ConsolidatedOrders;
        public List<PickListBatch> PickingBatchList;

        public static bool consolidation = false;
        public static Item CmbPickListBatchSelected = GlobalVariablesAndMethods.CmbPickListBatchSelected;



        #region Constructors
        public GetOrderData()
        {
            InitializeComponent();
            GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
            CheckUrgentOrders();
            chkfirst.Checked = true;
            chkscanning.Checked = true;
            btnScan.Enabled = false;
            txtSearch.Focus();
            LoadPickingBatch();
            LoadScreenFunctions();
        }


        public GetOrderData(string strBarCode)
        {
            InitializeComponent();
            GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
            CheckUrgentOrders();
            BarCode = strBarCode;
            txtSearch.Text = strBarCode;
            txtSearch.Focus();
            chkfirst.Checked = true;
            chkscanning.Checked = true;
            btnScan.Enabled = false;
            LoadPickingBatch();
            GetDataToBindGird();
            chkConsolidateOrderItems.Checked = consolidation;
            BindGrid();
            LoadScreenFunctions();
        }

        private void LoadScreenFunctions()
        {
            RegisterEvents();
            LoadBins();
        }

        private void LoadPickingBatch()
        {
            if (PickingBatchList == null)
            {
                PickingBatchList = new List<PickListBatch>();
            }
            if (PickingBatchList.Count == 0)
            {
                PickingBatchList = Activator.CreateInstance<PickList>().GetPickListBatches();
                cmbPickingBatches.Items.Add(new Item("All", PickingBatchList.Sum(x => x.OrdersRemaining).ToString()));
                PickingBatchList.ForEach(x =>
                    cmbPickingBatches.Items.Add(new Item(x.BatchName, x.OrdersRemaining.ToString()))
                    );
            }

            var currentSelectedBatch = new PickListBatch()
            {
                BatchName = CmbPickListBatchSelected.Name,
                OrdersRemaining = Convert.ToInt32(string.IsNullOrEmpty(CmbPickListBatchSelected.Value) ? "0" : CmbPickListBatchSelected.Value)
            };

            if (currentSelectedBatch.BatchName.Equals("All") || string.IsNullOrEmpty(currentSelectedBatch.BatchName))
            {
                if (!PickingBatchList.Any(x => x.BatchName.Equals("none")))
                {
                    cmbPickingBatches.Items.Insert(0, new Item("none", PickingBatchList.Sum(x => x.OrdersRemaining).ToString()));
                    cmbPickingBatches.SelectedIndex = 0;
                    return;
                }

            }

            var batches = PickingBatchList.Select(x => x.BatchName.ToString()).ToList();

            if (batches.Contains(currentSelectedBatch.BatchName)) cmbPickingBatches.SelectedIndex = batches.IndexOf(currentSelectedBatch.BatchName) + 1;
            else cmbPickingBatches.SelectedIndex = 0;
        }
        private void LoadBins()
        {
            cmbOccupiedBins.Items.Clear();
            var maxNumberOfBins = new Bins().GetNumberOfBins();
            var bins = new Bins().GetBins();
            cmbOccupiedBins.Items.Insert(0, String.Format("Occupied Bins ({0}/{1})", bins.Count, maxNumberOfBins));
            cmbOccupiedBins.SelectedIndex = 0;
            cmbOccupiedBins.ItemHeight = 40;
            bins = bins.OrderBy(x => x.BinNumber).ToList();
            Action<List<Bin>> iterateBins = (listOfBins) =>
            {
                foreach (var bin in listOfBins)
                {
                    var itemText = String.Format("Bin {0} ,Order No. :{1},\n Items Added : {2}/{3}",
                       bin.BinNumber, bin.OrderNumber, bin.NumberOfItemsAlreadyAddedInBin, bin.TotalNumberofItems);
                    this.cmbOccupiedBins.Items.Add(new Item(itemText, bin.OrderNumber.ToString()));
                }
            };
            if (bins.Any())
            {
                cmbOccupiedBins.Items.Add("Consolidation Completed--");
                iterateBins(bins.Where(x => x.NumberOfItemsAlreadyAddedInBin == x.TotalNumberofItems).ToList());
                cmbOccupiedBins.Items.Add("Consolidation Pending--");
                iterateBins(bins.Where(x => x.TotalNumberofItems != x.NumberOfItemsAlreadyAddedInBin).ToList());
            }
        }
        #endregion

        #region Methods
        private void CheckUrgentOrders()
        {
            try
            {
                if (GlobalVariablesAndMethods.DispatchType != null)
                {
                    if (GlobalVariablesAndMethods.DispatchType.ToUpper() == "ALL")
                    {
                        rbtnUrgent.Checked = false;
                        rbtnAllOrders.Checked = true;
                    }
                    else if (GlobalVariablesAndMethods.DispatchType.ToUpper() == "URGENT")
                    {
                        rbtnUrgent.Checked = true;
                        rbtnAllOrders.Checked = false;
                    }
                }
                else
                {
                    GlobalVariablesAndMethods.DispatchType = "All";
                    rbtnAllOrders.Checked = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void GetDataToBindGird()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:

            DataTable ObjdtOrders = new DataTable();
            SqlConnection connection = new SqlConnection();
            DataTable objDtData = new DataTable();
            string strProcToGetOrderData;
            strProcToGetOrderData = "GetConsolidatedOrderDispatchConsole";
            try
            {
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                BarCode = txtSearch.Text.Trim();
                SqlParameter[] objParam = new SqlParameter[4];
                objParam[0] = new SqlParameter("@Barcode", SqlDbType.NVarChar, 200);
                objParam[0].Value = BarCode;

                objParam[1] = new SqlParameter("@DisptachType", SqlDbType.NVarChar, 200);
                objParam[1].Value = GlobalVariablesAndMethods.DispatchType;

                objParam[2] = new SqlParameter("@Consolidation", SqlDbType.Bit, 1);
                objParam[2].Value = chkConsolidateOrderItems.Checked;

                objParam[3] = new SqlParameter("@PickingBatch", SqlDbType.VarChar, 200);
                objParam[3].Value = cmbPickingBatches.SelectedItem.ToString();

                objDtData = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, strProcToGetOrderData, objParam).Tables[0];
                objDt = objDtData.DefaultView.ToTable(true);
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    MessageBox.Show("Error occurred please try again after sometime", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch
            {
                MessageBox.Show("Error occurred please try again after sometime", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void ProceedToScan()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtSearch.Text))
                {
                    MessageBox.Show("Please Enter Barcode.");
                }
                else if (grdOrder.CurrentCell == null && chkfirst.Checked == false && chkscanning.Checked == false)
                {
                    MessageBox.Show("Please Select An Order.");
                }
                else
                {
                    int RowIndex = 0;
                    if (chkscanning.Checked)
                    {
                        if (objDt != null && objDt.Rows.Count > 0)
                        {
                            string SelectedOrderID = objDt.AsEnumerable().Select(x => x.Field<Guid>("OrderId").ToString()).FirstOrDefault() ?? string.Empty;
                            Scan_Items objOrderData = new Scan_Items(txtSearch.Text.Trim(), SelectedOrderID, true);
                            objOrderData.Show();
                            btnScan.Enabled = false;
                            btnFind.Enabled = false;
                            txtSearch.Enabled = false;
                        }
                        else
                        {
                            lblerror.Text = "No Record Found.";
                            lblerror.Visible = true;
                            grdOrder.Visible = false;
                            btnScan.Enabled = false;
                        }
                    }
                    else
                    {
                        if (objDt != null && objDt.Rows.Count > 0)
                        {
                            if (!chkfirst.Checked)
                            {
                                RowIndex = Convert.ToInt32(grdOrder.CurrentCell.RowIndex);
                            }
                            string SelectedOrderID = grdOrder.Rows[RowIndex].Cells[0].Value.ToString();

                            //Check is selected order opened or not
                            if (CheckIsOpenStatus(SelectedOrderID))
                            {
                                Scan_Items objOrderData = new Scan_Items(txtSearch.Text.Trim(), SelectedOrderID, false);
                                objOrderData.Show();
                                btnScan.Enabled = false;
                                btnFind.Enabled = false;
                                txtSearch.Enabled = false;
                            }
                            //end of checking isopen status
                        }
                        else
                        {
                            lblerror.Text = "No Record Found.";
                            lblerror.Visible = true;
                            grdOrder.Visible = false;
                            btnScan.Enabled = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private bool CheckIsOpenStatus(String OrderID)
        {
            DataTable dt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            Guid Orderid = new Guid(OrderID);
            SqlParameter[] objorderParams = new SqlParameter[1];
            objorderParams[0] = new SqlParameter("@OrderID", SqlDbType.UniqueIdentifier);
            objorderParams[0].Value = Orderid;
            dt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetCurrentOpenStatusByOrderID", objorderParams).Tables[0];
            if (dt.Rows.Count > 0)
            {
                bool IsOpened = Convert.ToBoolean(Convert.ToString(dt.Rows[0]["IsOpened"]));
                if (IsOpened)
                {
                    MessageBox.Show("This order is already opened, press find button to reload the data and try again with another order");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        private void BindGrid()
        {
            var selectedPickingBatch = (Item)cmbPickingBatches.SelectedItem;
            if (selectedPickingBatch.Name.Equals("none"))
            {
                MessageBox.Show("Please choose any Picking Batch");
                return;
            }
            try
            {
                string BarCode = txtSearch.Text.Trim();
                #region "Grid data"
                objDt = FilterConsolidatedOrders(objDt);

                if (objDt != null & objDt.Rows.Count > 0)
                {
                    grdOrder.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
                    grdOrder.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    grdOrder.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                    grdOrder.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
                    grdOrder.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                    grdOrder.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    grdOrder.ScrollBars = ScrollBars.Both;
                    grdOrder.AllowUserToAddRows = false;
                    grdOrder.Visible = true;
                    grdOrder.DataSource = objDt;
                    FormatDataGridColumns();
                    lblerror.Visible = false;
                    btnScan.Enabled = true;
                    btnFind.Enabled = true;
                    txtSearch.Enabled = true;
                    chkfirst.Enabled = true;
                    chkscanning.Enabled = true;
                    foreach (DataGridViewColumn column in grdOrder.Columns)
                    {
                        column.MinimumWidth = 120;
                        if (column.Name.Equals("Consolidation Status"))
                        {
                            column.MinimumWidth = 150;
                        }
                    }
                    LoadBins();
                }
                else
                {
                    lblerror.Text = "No Record Found.";
                    lblerror.Visible = true;
                    grdOrder.Visible = false;
                    btnScan.Enabled = false;
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred, Error: {0} ", ex.Message);
            }
        }

        private void FormatDataGridColumns()
        {
            int retry = 0;
        retry_to_format_grid:
            try
            {
                grdOrder.Columns["OrderId"].Visible = false;
                grdOrder.Columns["Received Date"].Width = 130;
                grdOrder.Columns["Received Date"].DefaultCellStyle.Format = "dd/MM/yyyy hh:mm:ss";
                grdOrder.Columns["Despatch By Date"].DefaultCellStyle.Format = "dd/MM/yyyy hh:mm:ss";
                grdOrder.Columns["Despatch By Date"].Width = 130;
                grdOrder.Columns["Consolidation status"].Width = 210;
                grdOrder.Columns["Consolidation status"].Visible = ConsolidatedOrders.Count > 0 ? true : false;
                grdOrder.Columns["Remaining Items to add in bin"].Visible = false;
                grdOrder.Columns["currentitemquantity"].Visible = false;
                grdOrder.Columns["itemstatus"].Visible = false;
                grdOrder.Sort(grdOrder.Columns["Order Number"], ListSortDirection.Ascending);
                grdOrder.DoubleBuffered(true);
            }
            catch
            {
                if (retry < 2)
                {
                    ++retry;
                    goto retry_to_format_grid;
                }
            }
        }
        private DataTable FilterConsolidatedOrders(DataTable objdt)
        {
            #region copying objdt table to new column added table ,and Add Bin number, items consolidated
            ConsolidatedOrders = null;
            ConsolidatedOrders = new List<int>();
            DataTable filteredOrders = new DataTable();
            filteredOrders.Columns.Add("OrderId");
            filteredOrders.Columns.Add("Order Number");
            filteredOrders.Columns.Add("Name");
            filteredOrders.Columns.Add("Source");
            filteredOrders.Columns.Add("# SKU's");
            filteredOrders.Columns.Add("# Of Items");
            filteredOrders.Columns.Add("Consolidation status");
            filteredOrders.Columns.Add("Remaining Items to add in bin");
            filteredOrders.Columns.Add("Received Date");
            filteredOrders.Columns.Add("Despatch By Date");
            filteredOrders.Columns.Add("itemstatus");
            filteredOrders.Columns.Add("currentitemquantity");
            foreach (DataRow row in objdt.Rows)
            {

                var addedToBin = row["IsAddedToBin"].ToString();
                bool isAddedToBin = !String.IsNullOrEmpty(addedToBin) ? Convert.ToBoolean(addedToBin) : false;
                DataRow filteredOrder = filteredOrders.NewRow();
                filteredOrder["OrderId"] = row["OrderId"];
                filteredOrder["Order Number"] = row["Order Number"];
                filteredOrder["Name"] = row["Name"];
                filteredOrder["Source"] = row["Source"];
                filteredOrder["# SKU's"] = row["# SKU's"];
                filteredOrder["# Of Items"] = row["# Of Items"];
                filteredOrder["currentitemquantity"] = row["currentitemquantity"];
                if (isAddedToBin)
                {
                    try
                    {
                        ConsolidatedOrders.Add(Convert.ToInt32(row["Order Number"]));
                        var binString = row["BinNumber"].ToString() ?? "0";
                        var binNumber = Convert.ToInt32(binString);
                        var itemPendingForConsolidation = Convert.ToInt32(row["TotalItems"]) - Convert.ToInt32(row["ConsolidatedNumberOfItems"]);
                        var consolidationStatus = "";
                        var itemStatus = ItemStatus.NotAddedInBin;
                        var consolidatedItems = GetListOfConsolidatedItems(row["ConsolidatedItems"].ToString());
                        #region Create status text consolidated order
                        if (consolidatedItems.Contains(BarCode))
                        {
                            consolidationStatus = "Item added to Bin" + binNumber;
                            itemStatus = ItemStatus.AlreadyAddedToBin;

                            if (itemPendingForConsolidation == 0)
                            {
                                consolidationStatus = String.Format("Consolidation completed,items are available for scan in Bin {0}", binNumber);
                                itemStatus = ItemStatus.ConsolidationCompleted;
                            }
                        }
                        else
                        {
                            if (itemPendingForConsolidation == 1)
                            {
                                consolidationStatus = String.Format("this item completes this order,Items available in Bin {0}", binNumber);
                                itemStatus = ItemStatus.CompletesOrder;
                            }
                            if (itemPendingForConsolidation > 1)
                            {
                                consolidationStatus = String.Format("Add item to Bin {0}, Pending Consolidation", binNumber);
                                itemStatus = ItemStatus.NotAddedInBin;
                            }
                        }
                        #endregion
                        filteredOrder["itemStatus"] = (int)itemStatus;
                        filteredOrder["Consolidation status"] = consolidationStatus;
                        filteredOrder["Remaining Items to add in bin"] = Convert.ToInt32(row["TotalItems"]) - Convert.ToInt32(row["ConsolidatedNumberOfItems"]) - 1;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Conversion error \n" + ex.Message);
                    }
                }
                else
                {
                    filteredOrder["itemStatus"] = 0;
                    filteredOrder["Consolidation status"] = "";
                    filteredOrder["Remaining Items to add in bin"] = "";
                }
                filteredOrder["Received Date"] = row["Received Date"];
                filteredOrder["Despatch By Date"] = row["Despatch By Date"];
                filteredOrders.Rows.Add(filteredOrder);
            }
            #endregion

            return filteredOrders;
        }
        private List<string> GetListOfConsolidatedItems(string allItemsAddedToBin)
        {
            List<string> itemsInBin = new List<string>();
            var itemArray = allItemsAddedToBin.Split(',');
            foreach (var item in itemArray)
            {
                if (!String.IsNullOrEmpty(item))
                {
                    itemsInBin.Add(item);
                }
            }
            return itemsInBin;
        }
        private void RegisterEvents()
        {
            this.grdOrder.CellMouseEnter += grdOrder_CellMouseEnter;
            this.grdOrder.CellMouseLeave += grdOrder_CellMouseLeave;
            this.grdOrder.CellMouseClick += grdOrder_CellMouseClick;
            this.grdOrder.ColumnHeaderMouseClick += FormatRows;
            this.cmbOccupiedBins.DrawItem += cmbOccupiedBins_DrawItem;
            this.cmbPickingBatches.DrawItem += cmbOccupiedBins_DrawItem;
            this.grdOrder.DataBindingComplete += FormatRows;
            this.chkConsolidateOrderItems.CheckedChanged += (Sender, evnt)
                =>
            {
                consolidation = chkConsolidateOrderItems.Checked;
            };
            new Bins().RemoveBinsOfProcessedOrders();
            ViewUpdateUserStatistics();
        }

        #endregion

        #region  Events
        private void btnFind_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtSearch.Text))
            {
                GetDataToBindGird();
                lblerror.Text = "";
                BindGrid();
            }
            else
            {
                MessageBox.Show("Please Enter Barcode.");
            }
        }
        private void btnScan_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtSearch.Text))
            {
                GetDataToBindGird();
                ProceedToScan();
            }
            else
            {
                MessageBox.Show("Please Enter Barcode");
            }
        }
        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (!string.IsNullOrWhiteSpace(txtSearch.Text))
                {
                    bool ignoreSecondLoad = false;
                    this.Focus();
                    if (chkConsolidateOrderItems.Checked)
                    {
                        GetDataToBindGird();
                        ignoreSecondLoad = true;
                    }

                    var checkBarcodeIsPartOfAnyConsolidatedOrder = new Bins().CheckBarcodeIsPresentInAnyBin(txtSearch.Text);
                    if (!checkBarcodeIsPartOfAnyConsolidatedOrder)
                    {
                        if (!ignoreSecondLoad)
                            GetDataToBindGird();
                        if (chkscanning.Checked)
                        {
                            ProceedToScan();
                        }
                        else
                        {
                            lblerror.Text = "";
                            BindGrid();
                        }
                    }
                    else
                    {
                        btnFind.PerformClick();
                    }
                }
                else
                {
                    MessageBox.Show("Please Enter Barcode");
                }
            }
        }
        private void txtSearch_TextAlignChanged(object sender, EventArgs e)
        {
            string key = e.ToString();
        }
        private void rbtnUrgent_CheckedChanged(object sender, EventArgs e)
        {
            GlobalVariablesAndMethods.DispatchType = "Urgent";
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                GetDataToBindGird();
                lblerror.Text = "";
                BindGrid();
            }
        }
        private void rbtnAllOrders_CheckedChanged(object sender, EventArgs e)
        {
            GlobalVariablesAndMethods.DispatchType = "All";
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                GetDataToBindGird();
                lblerror.Text = "";
                BindGrid();
            }
        }
        private void grdOrder_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var consolidationStatusColumn = grdOrder.Columns["Consolidation status"];
            if (e.RowIndex >= 0 && e.ColumnIndex == consolidationStatusColumn.Index)
            {
                var currentRow = grdOrder.Rows[e.RowIndex];
                var itemStatusValue = Convert.ToInt32(currentRow.Cells["itemstatus"].Value);
                if (
                    itemStatusValue == (int)ItemStatus.NotAddedInBin ||
                    itemStatusValue == (int)ItemStatus.CompletesOrder
                    )
                {
                    var statusCellValue = currentRow.Cells[e.ColumnIndex].Value.ToString();
                    if (!String.IsNullOrWhiteSpace(statusCellValue))
                    {
                        var binDetails = new Bins().GetIndividualBin(Convert.ToInt32(currentRow.Cells["Order Number"].Value.ToString()));
                        int itemsRequired = binDetails.TotalNumberofItems - binDetails.NumberOfItemsAlreadyAddedInBin - 1;
                        var binNumberPhrase = itemsRequired == 0 ? String.Format("The item completes the order") : String.Format("Add item to Bin {0}", binDetails.BinNumber);
                        if (itemsRequired >= 0)
                        {
                            //choosing what item status phrase we show on add to bin prompt
                            var itemRequiredstring = itemsRequired > 1 ?
                                String.Format("{0} more items required to complete the order {1} ", itemsRequired, binDetails.OrderNumber) : itemsRequired == 1 ?
                                String.Format("{0} more item is required to complete the order {1} ", itemsRequired, binDetails.OrderNumber) : String.Format("This is the only item left to complete order {0}, get the rest of the items from bin {1}", binDetails.OrderNumber, binDetails.BinNumber);

                            var qnty = 1;
                            #region creating prompt to add item into bin
                            Func<string, bool, BinPromptResult> addToBinPrompt = (Caption, ProceedToScanEnabled) =>
                            {

                                Form prompt = new Form();
                                prompt.Text = Caption;
                                prompt.Width = 420;
                                prompt.Height = 180;
                                prompt.StartPosition = FormStartPosition.CenterScreen;
                                prompt.ShowIcon = false;
                                int quantity = 1;
                                try { quantity = Convert.ToInt32(currentRow.Cells["currentitemquantity"].Value.ToString()); }
                                catch { };

                                prompt.BackColor = Color.White;
                                var selectedOption = BinPromptResult.Cancel;
                                //

                                Label addToBinNumPhrase = new Label() { Top = 20, Left = 20, Width = 300, Height = 18 };
                                addToBinNumPhrase.Text = binNumberPhrase;

                                Label qntyPhrase = new Label() { Top = 38, Left = 20, Width = 100, Height = 18 };
                                qntyPhrase.Text = "Quantity : " + qnty.ToString();

                                Label remainingItemsPhrase = new Label() { Top = 60, Left = 20, Width = 300, Height = 26 };
                                remainingItemsPhrase.Text = itemRequiredstring;
                                //
                                Button btnYes = new Button() { Top = 100, Width = 80, Left = 210 };
                                Button btnProceedToScan = new Button() { Top = 100, Width = 160, Left = 130 };
                                Button btnCancel = new Button() { Top = 100, Width = 80, Left = 300 };


                                TextBox txtQuantity = new TextBox() { Top = 35, Width = 40, Left = 97, Height = 20 };
                                txtQuantity.Text = qnty.ToString();

                                Label remainingQty = new Label() { Top = 38, Left = 142 };
                                remainingQty.ForeColor = Color.Red;
                                remainingQty.Text = String.Format("({0} Remaining)", quantity - qnty);

                                btnYes.Text = "Yes";
                                btnProceedToScan.Text = "Proceed To Scan";
                                btnCancel.Text = "Cancel";
                                //
                                //adding control to prompt
                                //

                                Action refreshProceedToScan = () =>
                                {
                                    if (ProceedToScanEnabled)
                                    {
                                        var tempEnb = ProceedToScanEnabled && quantity - qnty == 0 ? true : false;
                                        remainingItemsPhrase.ForeColor = Color.Black;
                                        btnProceedToScan.Visible = tempEnb;
                                        btnYes.Visible = !tempEnb;
                                        if (quantity - qnty != 0)
                                        {
                                            remainingItemsPhrase.ForeColor = Color.DarkGray;
                                        }
                                    }
                                    qnty = qnty > quantity ? quantity : qnty;
                                    qntyPhrase.Text = "Quantity : " + qnty.ToString();
                                    btnYes.Enabled = qnty == 0 ? false : true;
                                    remainingQty.Text = String.Format("({0} Remaining)", quantity - qnty);

                                };
                                Action<string> QuantityChanged = (action) =>
                                {
                                    switch (action)
                                    {
                                        case "increaseQnty":
                                            {
                                                if (qnty < quantity)
                                                {
                                                    try { qnty = Convert.ToInt32(txtQuantity.Text); }
                                                    catch { }
                                                    refreshProceedToScan.Invoke();
                                                }
                                            }
                                            break;

                                        case "decreaseQnty":
                                            {
                                                if (qnty > 0)
                                                {
                                                    try { qnty = Convert.ToInt32(txtQuantity.Text); }
                                                    catch { }
                                                    refreshProceedToScan.Invoke();
                                                }
                                            }
                                            break;

                                        default:
                                            break;
                                    }
                                };
                                Action qntyTxtChanged = () =>
                                {
                                    var txtQntyValue = txtQuantity.Text;
                                    if (!String.IsNullOrEmpty(txtQntyValue))
                                    {
                                        try
                                        {
                                            if (qnty != Convert.ToInt32(txtQntyValue))
                                            {
                                                if (Convert.ToInt32(txtQntyValue) > qnty)
                                                {
                                                    QuantityChanged.Invoke("increaseQnty");
                                                }
                                                else
                                                {
                                                    QuantityChanged.Invoke("decreaseQnty");
                                                }
                                            }
                                        }
                                        catch
                                        {
                                            txtQuantity.Text = string.Empty;
                                        }
                                    }
                                };
                                //
                                txtQuantity.KeyUp += (s, evnt) =>
                                {
                                    qntyTxtChanged.Invoke();
                                };
                                txtQuantity.KeyDown += (s, evnt) =>
                                {
                                    qntyTxtChanged.Invoke();
                                };
                                btnYes.Click += (Sender, evnt) => { selectedOption = BinPromptResult.Yes; prompt.Close(); };
                                btnProceedToScan.Click += (Sender, evnt) => { selectedOption = BinPromptResult.ProceedToScan; prompt.Close(); };
                                btnCancel.Click += (Sender, evnt) => { prompt.Close(); };
                                //
                                btnProceedToScan.Visible = false;
                                refreshProceedToScan.Invoke();
                                //
                                //
                                prompt.Controls.Add(txtQuantity);
                                prompt.Controls.Add(btnYes);
                                prompt.Controls.Add(btnProceedToScan);
                                prompt.Controls.Add(btnCancel);
                                prompt.Controls.Add(addToBinNumPhrase);
                                prompt.Controls.Add(qntyPhrase);
                                prompt.Controls.Add(remainingItemsPhrase);
                                prompt.Controls.Add(remainingQty);
                                prompt.ShowDialog();
                                //

                                return selectedOption;
                            };
                            #endregion
                            var result = addToBinPrompt("Add Item", itemsRequired == 0);
                            //delegate to add item into bin
                            Func<BinPromptResult, bool> addItemToBin = (addToBinResult) =>
                            {
                                var orderNumber = currentRow.Cells["Order Number"].Value.ToString();
                                var bin = new Bins();
                                if (!(String.IsNullOrEmpty(orderNumber) && String.IsNullOrEmpty(BarCode)) && qnty > 0)
                                {
                                    var obj = bin.AddItemToBin(orderNumber, BarCode, qnty);
                                    var itemAddedInBin = obj.GetType().GetProperty("Status").GetValue(obj, null) ?? false;
                                    return (bool)itemAddedInBin;
                                }
                                return false;
                            };
                            if (result == BinPromptResult.Yes)
                            {
                                if (addItemToBin.Invoke(BinPromptResult.Yes))
                                {
                                    GetDataToBindGird();
                                    lblerror.Text = "";
                                    BindGrid();
                                    LoadBins();
                                    txtSearch.Clear();
                                    txtSearch.Focus();
                                }
                            }
                            if (result == BinPromptResult.ProceedToScan)
                            {
                                addItemToBin.Invoke(BinPromptResult.ProceedToScan);
                                var barcode = txtSearch.Text;
                                if (!String.IsNullOrEmpty(barcode))
                                {
                                    var orderId = currentRow.Cells["OrderId"].Value.ToString();
                                    MoveToScan(barcode, orderId, false, false);
                                }
                            }
                        }
                    }
                }
            }
        }
        private void grdOrder_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                grdOrder.Cursor = Cursors.Default;
                var consolidationStatusColumn = grdOrder.Columns["Consolidation status"];
                if (e.RowIndex >= 0 && e.ColumnIndex == consolidationStatusColumn.Index)
                {
                    var currentRow = grdOrder.Rows[e.RowIndex];
                    var itemStatusValue = Convert.ToInt32(currentRow.Cells["itemstatus"].Value);
                    if (itemStatusValue == (int)ItemStatus.NotAddedInBin ||
                        itemStatusValue == (int)ItemStatus.CompletesOrder
                        )
                        currentRow.Cells[e.ColumnIndex].Style.ForeColor = Color.Red;
                }
            }
        }
        private void grdOrder_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            var consolidationStatusColumn = grdOrder.Columns["Consolidation status"];
            if (e.RowIndex >= 0 && e.ColumnIndex == consolidationStatusColumn.Index)
            {
                var currentRow = grdOrder.Rows[e.RowIndex];
                if (!String.IsNullOrWhiteSpace(currentRow.Cells[e.ColumnIndex].Value.ToString() ?? String.Empty))
                {
                    grdOrder.Cursor = Cursors.Hand;
                    var itemStatusValue = Convert.ToInt32(currentRow.Cells["itemstatus"].Value);
                    if (itemStatusValue == (int)ItemStatus.NotAddedInBin)
                        currentRow.Cells[e.ColumnIndex].Style.ForeColor = Color.Blue;
                    if (itemStatusValue == (int)ItemStatus.CompletesOrder)
                        currentRow.Cells[e.ColumnIndex].Style.ForeColor = Color.Green;
                }
            }
        }
        private void FormatRows(object sender, EventArgs e)
        {
            try
            {
                int rowIndex = 0;
                foreach (DataGridViewRow row in grdOrder.Rows)
                {
                    grdOrder.Columns["Consolidation status"].DefaultCellStyle.ForeColor = Color.Red;
                    var ordernumber = Convert.ToInt32(grdOrder.Rows[rowIndex].Cells["Order Number"].Value);
                    if (ConsolidatedOrders.Contains(ordernumber))
                    {
                        var currentRow = grdOrder.Rows[rowIndex];
                        var statusCell = currentRow.Cells["Consolidation status"];
                        var currentRowItemStatusValue = currentRow.Cells["itemstatus"].Value.ToString() ?? String.Empty;
                        if (!String.IsNullOrEmpty(currentRowItemStatusValue))
                        {
                            var itemStatus = (ItemStatus)Enum.Parse(typeof(ItemStatus), currentRow.Cells["itemstatus"].Value.ToString());
                            if (itemStatus == ItemStatus.ConsolidationCompleted)
                            {
                                statusCell.Style.ForeColor = Color.Green;
                            }
                            if (itemStatus == ItemStatus.AlreadyAddedToBin)
                            {
                                statusCell.Style.ForeColor = Color.Black;
                            }
                        }
                    }
                    rowIndex++;
                }
            }
            catch { }
        }
        private void cmbOccupiedBins_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            ComboBox box = sender as ComboBox;

            if (Object.ReferenceEquals(null, box))
            {
                return;
            }
            e.DrawBackground();
            if (e.Index >= 0)
            {
                Graphics g = e.Graphics;
                using (Brush brush = ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                      ? new SolidBrush(SystemColors.Highlight)
                      : new SolidBrush(Color.WhiteSmoke))
                {
                    using (Brush textBrush = new SolidBrush(e.ForeColor))
                    {
                        g.FillRectangle(brush, e.Bounds);

                        g.DrawString(box.Items[e.Index].ToString(),
                                     e.Font,
                                     textBrush,
                                     e.Bounds,
                                     StringFormat.GenericDefault);
                    }
                }

            }
            e.DrawFocusRectangle();
        }
        #endregion

        private void cmbOccupiedBins_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
            //items on which this event should ignored
            string[] itemsOnWhichClickIgnore = { "Consolidation Completed--", "Consolidation Pending--" };
            //
            var selectedText = cmbOccupiedBins.SelectedItem.ToString();
            var ignore = itemsOnWhichClickIgnore.Contains(selectedText);
            if (cmbOccupiedBins.SelectedIndex > 0 && !ignore)
            {
                var item = (Item)cmbOccupiedBins.SelectedItem;
                var currentBinDetails = new Bins().GetBinDetails(Convert.ToInt32(item.Value));
                var bin = (Bin)currentBinDetails.GetType().GetProperty("Bin").GetValue(currentBinDetails, null);
                var orderItems = (DataTable)currentBinDetails.GetType().GetProperty("DataTable").GetValue(currentBinDetails, null) ?? new DataTable();
                //getting order id of current order

                if (orderItems.Rows.Count == 0)
                {
                    MessageBox.Show("Order is processed");
                    return;
                }
                var currentOrderId = orderItems.Rows[0][0].ToString();
                orderItems.Columns.RemoveAt(0);

                if (bin.OrderNumber > 0)
                {
                    var readyToScan = bin.NumberOfItemsAlreadyAddedInBin == bin.TotalNumberofItems ? true : false;
                    var dialogueResult = BinDialogue("Bin " + bin.BinNumber, orderItems, readyToScan, bin);
                    var dialogueSelectedOption = (BinPromptResult)Enum.Parse(typeof(BinPromptResult), dialogueResult[0]);
                    //if adding items having no barcode to bin
                    if (dialogueSelectedOption == BinPromptResult.AddItemsHavingNoBacode)
                    {
                        //add items not having barcode to bin 
                        if (new Bins().AddItemToBinHavingNoBarcode(bin.OrderNumber, Convert.ToInt32(dialogueResult[1])))
                        {
                            LoadBins();
                        }
                    }
                    //if delete bin option clicked
                    if (dialogueSelectedOption == BinPromptResult.DeleteBin)
                    {
                        var consolidation = chkConsolidateOrderItems.Checked;
                        if (consolidation)
                            chkConsolidateOrderItems.Checked = false;

                        //removing the bin
                        if (new Bins().RemoveOrderFromBin(bin.OrderNumber))
                        {
                            if (!String.IsNullOrEmpty(txtSearch.Text))
                                btnFind.PerformClick();
                            LoadBins();
                        }
                        chkConsolidateOrderItems.Checked = consolidation;
                    }
                    //if proceed to scan clicked
                    if (dialogueSelectedOption == BinPromptResult.ProceedToScan)
                    {
                        string barcode = string.IsNullOrEmpty(txtSearch.Text) ? dialogueResult[2] : txtSearch.Text;
                        string strOrderId = currentOrderId;
                        bool blIsCallFromProcessScreen = true;
                        bool IsScanningChecked = false;
                        //proceed to scan
                        MoveToScan(barcode, strOrderId, blIsCallFromProcessScreen, IsScanningChecked);
                    }
                    grdOrder.Focus();
                }
            }
        }
        private void MoveToScan(string barcode, string strOrderId, bool blIsCallFromProcessScreen, bool isScannignChecked)
        {
            Scan_Items objOrderData = new Scan_Items(barcode, strOrderId, blIsCallFromProcessScreen, isScannignChecked);
            objOrderData.Show();
        }

        private void GetOrderData_Load(object sender, EventArgs e)
        {

        }

        private void cmbPickingBatches_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedBatch = ((Item)cmbPickingBatches.SelectedItem);
            CmbPickListBatchSelected = selectedBatch;
            GlobalVariablesAndMethods.CmbPickListBatchSelected = selectedBatch;
            if (!string.IsNullOrEmpty(txtSearch.Text)) btnFind.PerformClick();
            lblRemaningOrdersCount.Text = String.Format("{0} Orders Remaining", selectedBatch.Value);

            if (selectedBatch.Value.Equals("1"))
            {
                lblRemaningOrdersCount.Text = String.Format("{0} Order Remaining", selectedBatch.Value);
            }
        }

    }
}