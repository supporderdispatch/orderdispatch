﻿using ODM.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Entities;
using ODM.Data.Util;
using System.Data.SqlClient;
using ODM.Data.ShippingLabel;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using ODM.Core.UserTracking;
using ODM.Data.Entity;
using System.Threading;
using ODM.Appliction.Extensions;

namespace ODM.Appliction
{
    public partial class AllOpenOrders
    {
        #region events to run gif in grid cell
        bool currentlyAnimating = true;
        public ProcessState _currentProcessState { get; set; }
        public Tuple<ProcessState, int, bool> currentProcessState
        {
            get
            {
                return Tuple.Create(_currentProcessState, 0, true);
            }
            set
            {
                _currentProcessState = value.Item1;
                ChangeStatus(Convert.ToInt32(value.Item2), _currentProcessState, Convert.ToBoolean(value.Item3));
            }
        }


        private void RegisterEvents()
        {
            this.Load += AllOpenOrders_Load;
            grdOrders.DoubleBuffered(true);
            grdOrders.DataError += grdOrders_DataError;
            grdOrders.CellMouseClick += grdOrders_CellMouseClick;
            grdOrders.MouseMove += grdOrders_MouseMove;
            this.FormClosed += AllOpenOrders_FormClosed;
        }

        void AllOpenOrders_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (AutoRefreshTimer != null)
                AutoRefreshTimer.Dispose();
        }
        void grdOrders_MouseMove(object sender, EventArgs e)
        {
            UpdateSelectedRowsStatus();
        }

        void grdOrders_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            UpdateSelectedRowsStatus();
        }

        private void UpdateSelectedRowsStatus()
        {
            var selectedRowCount = grdOrders.SelectedRows.Count;
            var totalRowCount = grdOrders.Rows.Count;
            if (grdOrders.CheckIfFilterActivatedFor() && grdOrders.Rows.Count > 0)
            {
                grdOrders.Rows[0].Selected = false;
                totalRowCount = grdOrders.Rows.Count - 1;
            }
            lblSelectedOrdersStatus.Text = string.Format("Selected {0} of {1} Orders", selectedRowCount, totalRowCount);
        }


        private void grdOrders_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //
        }
        private void AllOpenOrders_Load(object sender, EventArgs e)
        {
            this.grdOrders.Paint += new PaintEventHandler(grdOrders_Paint);
            cmbAutoRefreshInterval.Items.Clear();
            cmbAutoRefreshInterval.Items.AddRange(new string[] { "3", "5", "10", "15", "20", "30", "60" });
            cmbAutoRefreshInterval.SelectedIndex = 1;
            this.chkAutoRefresh.CheckedChanged += chkAutoRefresh_CheckedChanged;
            this.cmbAutoRefreshInterval.SelectedIndexChanged += cmbAutoRefreshInterval_SelectedIndexChanged;
            SetUnsetAutoRefresh(false);
        }
        private void chkAutoRefresh_CheckedChanged(object sender, EventArgs e)
        {
            SetUnsetAutoRefresh(true);
        }

        private void cmbAutoRefreshInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetUnsetAutoRefresh(true);
        }

        private void SetUnsetAutoRefresh(bool run)
        {
            CheckForIllegalCrossThreadCalls = false;
            if (AutoRefreshTimer != null)
            {
                AutoRefreshTimer.Dispose();
            }

            AutoRefreshTimer = new System.Timers.Timer();
            AutoRefreshTimer.Interval = TimeSpan.FromMinutes(Convert.ToInt32(cmbAutoRefreshInterval.SelectedItem.ToString())).TotalMilliseconds;
            AutoRefreshTimer.Elapsed += (s, e) => { SetUnsetAutoRefresh(true); };

            if (chkAutoRefresh.Checked)
            {
                AutoRefreshTimer.Start();
                Action act = () =>
                {
                    if (run)
                    {
                        btnRefresh.PerformClick();
                        lblLastAutoUpdate.Text = "Last updated at : " + DateTime.Now.ToLocalTime();
                    }
                    else
                    {
                        lblLastAutoUpdate.Text = string.Empty;
                    }

                };
                this.BeginInvoke(act);


            }
            else
            {
                AutoRefreshTimer.Stop();
                lblLastAutoUpdate.Text = string.Empty;
            }
        }
        private void grdOrders_Paint(object sender, PaintEventArgs e)
        {
            AnimateImage();
            ImageAnimator.UpdateFrames();
        }
        public void AnimateImage()
        {
            if (!currentlyAnimating)
            {
                foreach (DataGridViewRow row in grdOrders.Rows)
                {
                    Image img = row.Cells["process status"].Value as Image;
                    if (img != null)
                    {
                        ImageAnimator.Animate(img, new EventHandler(this.OnFrameChanged));
                    }
                }
                currentlyAnimating = true;
            }
        }
        private void OnFrameChanged(object sender, EventArgs e)
        {
            this.grdOrders.Invalidate();
        }
        #endregion

        private void ChangeStatus(int rowIndex, ProcessState processState, bool shallRun)
        {
            if (!shallRun)
                return;

            Func<string, int, bool> changeIconOfProcessStateCellIcon = (imageName, index) =>
            {
                try
                {
                    var imagePath = Application.StartupPath + "\\Images\\Processing\\";
                    Bitmap bmp = new Bitmap(imagePath + imageName);
                    PictureBox cellIcon = new PictureBox();
                    Graphics g = Graphics.FromImage(bmp);
                    g.DrawRectangle(Pens.LightGray, 0, 0, bmp.Width - 1, bmp.Height - 1);
                    cellIcon.Image = bmp;
                    ((DataGridViewImageCell)grdOrders.Rows[index].Cells["process status"]).Value = (System.Drawing.Image)cellIcon.Image;
                    return true;
                }
                catch { return false; }
            };
            switch (processState)
            {
                case ProcessState.None:
                    {
                        changeIconOfProcessStateCellIcon("Initial.png", rowIndex);
                    }
                    break;
                case ProcessState.Loading:
                    {
                        changeIconOfProcessStateCellIcon("Processing_Main.gif", rowIndex);
                    }
                    break;

                case ProcessState.Printing:
                    {
                        changeIconOfProcessStateCellIcon("Printing.png", rowIndex);
                    }
                    break;

                case ProcessState.Completed:
                    {
                        changeIconOfProcessStateCellIcon("done.png", rowIndex);
                    }
                    break;

                case ProcessState.Failed:
                    {
                        changeIconOfProcessStateCellIcon("failed.png", rowIndex);
                    }
                    break;

                default:
                    break;
            }
        }

        public void ShowLogsOfSelectedOrders(List<string> selectedOrders)
        {
            var prompt = new Form();
            prompt.Text = "View Logs";
            prompt.Width = 860;
            prompt.Height = 560;
            prompt.ShowIcon = false;
            prompt.Show();
            Label currentOrder = new Label() { Left = 10, Top = 40, Width = 150, Height = 20 };
            DataGridView grdLogs = new DataGridView() { Top = 60 };
            TabControl control = new TabControl();
            control.Name = String.Empty;
            control.Width = 740;
            control.Height = 20;
            prompt.FormBorderStyle = FormBorderStyle.Sizable;
            prompt.Controls.Add(control);
            prompt.Controls.Add(currentOrder);
            prompt.Controls.Add(grdLogs);
            Action<string> viewLogOfOrder = (numOrderId) =>
            {
                var logs = Activator.CreateInstance<Logger>().GetLogsOfOrder(numOrderId);
                grdLogs.Invalidate();
                grdLogs.Size = new System.Drawing.Size(prompt.Width - 20, prompt.Height - 100);
                grdLogs.DataSource = logs;
                grdLogs.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
                try
                {
                    grdLogs.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                    grdLogs.DoubleBuffered(true);
                    grdLogs.BackgroundColor = Color.WhiteSmoke;
                    grdLogs.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    grdLogs.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                    grdLogs.RowTemplate.MinimumHeight = 60;
                    grdLogs.Columns["ID"].Visible = false;
                    grdLogs.Columns["OrderId"].Visible = false;
                    grdLogs.Columns["NumOrderId"].Visible = false;
                    grdLogs.Columns["LoggedOn"].DefaultCellStyle.Format = "dd-MM-yyyy hh:mm:ss tt";
                    grdLogs.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
                    grdLogs.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                    grdLogs.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    grdLogs.ScrollBars = ScrollBars.Both;
                    currentOrder.Text = String.Format("Total Records : {0}", grdLogs.Rows.Count - 1);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            };
            foreach (var order in selectedOrders)
            {
                TabPage tabPage = new TabPage();
                tabPage.Name = order.ToString();
                tabPage.Text = order.ToString();
                control.Controls.Add(tabPage);
            }
            control.SelectedIndexChanged += (sender, events) =>
            {
                viewLogOfOrder(control.SelectedTab.Name.ToString());
            };

            viewLogOfOrder(control.SelectedTab.Name.ToString());
        }

        public void UpdateProcessStateOfOrder(Guid orderId, ProcessCompleted state)
        {
            Activator.CreateInstance<ODM.Core.ProcessStateOfOrder>()
                .UpdateProcessesStatusOfOrder(orderId, state);
        }
        private string ParseCellValue(string cellValue)
        {
            var result = String.Empty;
            try
            {
                var processes = cellValue.Split(',');
                foreach (var process in processes.Distinct().ToList())
                {
                    result += "* " + (ProcessCompleted)Convert.ToInt32(process) + "\n";
                }
            }
            catch
            {
                result = cellValue;
            }

            return result;
        }
        private void CheckCourierAndPrintForMultipleOrders(int currentRowIndex)
        {
            //Start :- This is because we can't show the hidden(when we hide element before start of thread) element and vice versa during threading process 
            Task task = null;
            PicLoading.Visible = true;
            PicRight.Visible = true;
            PicError.Visible = true;
            PicRight.Visible = false;
            PicError.Visible = false;
            //End
            // Thread PrintLabelThread;
            orderProcessed = false;
            switch (strCourierType.Trim().ToUpper())
            {
                case "MYHERMES":
                    {
                        task = new Task(() =>
                        {
                            MyHermesPrintLabelProcess(currentRowIndex);
                            orderProcessed = true;
                        });
                        task.Start();
                    }
                    break;

                case "HERMES":
                    {
                        task = new Task(() =>
                        {
                            HermesPrintLabelProcess(currentRowIndex);
                            orderProcessed = true;
                        });
                        task.Start();
                    }
                    break;

                case "SPRING":
                    {
                        task = new Task(() =>
                        {
                            SpringPrintLabelProcess_New(currentRowIndex);
                            orderProcessed = true;
                        });
                        task.Start();
                    }
                    break;
                case "DPD UK":
                case "DPD":
                    {
                        task = new Task(() =>
                        {
                            DPDPrintLabelProcess(currentRowIndex);
                            orderProcessed = true;
                        });
                        task.Start();
                    }
                    break;

                case "ROYAL MAIL":
                case "OBA":
                    {
                        task = new Task(() =>
                        {
                            RoyalMailPrintLabelProcess(currentRowIndex);
                            orderProcessed = true;
                        });
                        task.Start();
                    }
                    break;

                case "FEDEX":
                    {
                        task = new Task(() =>
                        {
                            FedExPrintLabelProcess(currentRowIndex);
                            orderProcessed = true;
                        });
                        task.Start();
                    }
                    break;

                case "AMAZON":
                    {
                        task = new Task(() =>
                        {
                            AmazonPrintLabelProcess(currentRowIndex);
                            orderProcessed = true;
                        });
                        task.Start();
                    }
                    break;

                default:
                    orderProcessed = true;
                    break;
            }
        }
        #region "Print Label Process"
        private void HermesPrintLabelProcess(int currentRowIndex)
        {
            try
            {
                currentProcessState = Tuple.Create(ProcessState.Loading, currentRowIndex, true);
                HermesPrintLabel objPrintLabel = new HermesPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                strTrackingNumber = objPrintLabel.ExecuteMyHermesShippingAPI(objOrder, Orderid);
                currentProcessState = Tuple.Create(ProcessState.Printing, currentRowIndex, true);
                if (strTrackingNumber != "")
                {
                    UpdateProcessStateOfOrder(Orderid, ProcessCompleted.Label_Printed);
                    currentProcessState = Tuple.Create(ProcessState.Completed, currentRowIndex, true);
                    UpdateTrackingNumber();
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                }
            }
            catch (Exception ex)
            {
                currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void MyHermesPrintLabelProcess(int currentRowIndex)
        {
            try
            {
                currentProcessState = Tuple.Create(ProcessState.Loading, currentRowIndex, true);
                MyHermesPrintLabel objPrintLabel = new MyHermesPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                strTrackingNumber = objPrintLabel.ExecuteMyHermesShippingAPI(objOrder, Orderid);
                currentProcessState = Tuple.Create(ProcessState.Printing, currentRowIndex, true);
                if (strTrackingNumber != "")
                {
                    UpdateProcessStateOfOrder(Orderid, ProcessCompleted.Label_Printed);
                    currentProcessState = Tuple.Create(ProcessState.Completed, currentRowIndex, true);
                    UpdateTrackingNumber();
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                }
            }
            catch (Exception ex)
            {
                currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void SpringPrintLabelProcess_New(int currentRowIndex)
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            try
            {
                currentProcessState = Tuple.Create(ProcessState.Loading, currentRowIndex, true);
                //SpringPrintLabel objPrintLabel = new SpringPrintLabel();
                SpringPrintLabel_New objPrintLabel = new SpringPrintLabel_New();
                CheckForIllegalCrossThreadCalls = false;
                PicRight.Visible = false;
                PicError.Visible = false;
                strTrackingNumber = objPrintLabel.ExecuteSpringShippingAPI_New(objOrder, Orderid, objDt);
                currentProcessState = Tuple.Create(ProcessState.Printing, currentRowIndex, true);
                if (strTrackingNumber != "")
                {
                    UpdateProcessStateOfOrder(Orderid, ProcessCompleted.Label_Printed);
                    currentProcessState = Tuple.Create(ProcessState.Completed, currentRowIndex, true);
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    UpdateTrackingNumber();
                }
                else if (strTrackingNumber.ToUpper() != "ERROR")
                {
                    currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                    PicLoading.Visible = false;
                    PicRight.Visible = false;
                    PicError.Visible = true;
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error in the API");
                    MessageBox.Show("Can't process because of error in the API", "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                PicLoading.Visible = false;
                PicRight.Visible = false;
                // PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SpringPrintLabelProcess(int currentRowIndex)
        {
            try
            {
                SpringPrintLabel objPrintLabel = new SpringPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                strTrackingNumber = objPrintLabel.ExecuteSpringShippingAPI(objOrder, Orderid);
                if (strTrackingNumber != "")
                {
                    UpdateTrackingNumber();
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                }
            }
            catch (Exception ex)
            {
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void DPDPrintLabelProcess(int currentRowIndex)
        {
            try
            {
                currentProcessState = Tuple.Create(ProcessState.Loading, currentRowIndex, true);
                DPDPrintLabel objPrintLabel = new DPDPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                strTrackingNumber = objPrintLabel.ExecuteDPDShippingAPI(objOrder, Orderid);
                currentProcessState = Tuple.Create(ProcessState.Printing, currentRowIndex, true);
                if (strTrackingNumber != "")
                {
                    UpdateProcessStateOfOrder(Orderid, ProcessCompleted.Label_Printed);
                    currentProcessState = Tuple.Create(ProcessState.Completed, currentRowIndex, true);
                    UpdateTrackingNumber();
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                }
            }
            catch (Exception ex)
            {
                currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RoyalMailPrintLabelProcess(int currentRowIndex)
        {
            try
            {
                currentProcessState = Tuple.Create(ProcessState.Loading, currentRowIndex, true);
                RoyalMailPrintLabel objPrintLabel = new RoyalMailPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                strTrackingNumber = objPrintLabel.ExecuteRoyalMailShippingAPI(objOrder, Orderid);
                currentProcessState = Tuple.Create(ProcessState.Printing, currentRowIndex, true);
                if (strTrackingNumber != "")
                {
                    UpdateProcessStateOfOrder(Orderid, ProcessCompleted.Label_Printed);
                    currentProcessState = Tuple.Create(ProcessState.Completed, currentRowIndex, true);
                    UpdateTrackingNumber();
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                }
            }
            catch (Exception ex)
            {
                currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void FedExPrintLabelProcess(int currentRowIndex)
        {
            try
            {
                currentProcessState = Tuple.Create(ProcessState.Loading, currentRowIndex, true);
                FedExPrintLabel objPrintLabel = new FedExPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                strTrackingNumber = objPrintLabel.ExecuteFedExShippingAPI(objOrder, Orderid);
                currentProcessState = Tuple.Create(ProcessState.Printing, currentRowIndex, true);
                if (strTrackingNumber != "")
                {
                    UpdateProcessStateOfOrder(Orderid, ProcessCompleted.Label_Printed);
                    currentProcessState = Tuple.Create(ProcessState.Completed, currentRowIndex, true);
                    UpdateTrackingNumber();
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                }
            }
            catch (Exception ex)
            {
                currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void AmazonPrintLabelProcess(int currentRowIndex)
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            try
            {
                currentProcessState = Tuple.Create(ProcessState.Loading, currentRowIndex, true);
                AmazonPrintLabel objPrintLabel = new AmazonPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                PicRight.Visible = false;
                PicError.Visible = false;
                Guid ShipmentID = new Guid();
                strTrackingNumber = objPrintLabel.ExecuteAmazonShippingAPI(objDt, Orderid, ref ShipmentID);
                currentProcessState = Tuple.Create(ProcessState.Printing, currentRowIndex, true);
                if (strTrackingNumber != "")
                {
                    UpdateProcessStateOfOrder(Orderid, ProcessCompleted.Label_Printed);
                    currentProcessState = Tuple.Create(ProcessState.Completed, currentRowIndex, true);
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    GlobalVariablesAndMethods.LogHistory("Got Tracking number in all open order screen:- " + strTrackingNumber);
                    UpdateTrackingNumber();
                    GlobalVariablesAndMethods.GetAddDeleteShipmentDetails(ShipmentID, "ADD", Orderid);
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region event helpers
        private bool populateVariablesToPrintLabelOrProcess(string Order_ID)
        {
            var result = false;
            try
            {

                Orderid = new Guid(Order_ID);
                if (Orderid != GlobalVariablesAndMethods.OrderIdToLog)
                {
                   return false;
                }

                objDt = new DataTable();
                objDt = GlobalVariablesAndMethods.GetOrderDetailsByOrderId(Orderid);
                if (objDt != null & objDt.Rows.Count > 0)
                {
                    //Code to populate the fields
                    LocationID = new Guid(Convert.ToString(objDt.Rows[0]["Location"]));
                    strCourierType = Convert.ToString(objDt.Rows[0]["vendor"]);
                    strTrackingNumber = Convert.ToString(objDt.Rows[0]["OrderTrackingNumber"]);
                    strServiceName = Convert.ToString(objDt.Rows[0]["PostalServiceName"]);
                    pkPostalServiceID = new Guid(Convert.ToString(objDt.Rows[0]["pkPostalServiceId"]));
                    ShippingID = new Guid(Convert.ToString(objDt.Rows[0]["ShippingId"]));
                    //End of Code to populate the fields
                    //Code to map the Order Object for the API Integration.
                    List<OrderItem> objOrderItems = new List<OrderItem>();
                    OrderItem objOrderItem = new OrderItem();
                    objOrder.Orderid = Orderid;
                    objOrder.NumOrderId = Convert.ToInt32(objDt.Rows[0]["NumOrderId"]);
                    objOrder.PostCode = Convert.ToString(objDt.Rows[0]["PostCode"]);
                    objOrder.Address1 = Convert.ToString(objDt.Rows[0]["CompanyAddress"]);
                    objOrder.Address2 = Convert.ToString(objDt.Rows[0]["Address2"]);
                    objOrder.Address3 = Convert.ToString(objDt.Rows[0]["Address3"]);
                    objOrder.Town = Convert.ToString(objDt.Rows[0]["Town"]);
                    objOrder.FullName = Convert.ToString(objDt.Rows[0]["CustomerFullName"]);
                    objOrder.EmailAddress = Convert.ToString(objDt.Rows[0]["EmailAddress"]);
                    objOrder.BuyerPhoneNumber = Convert.ToString(objDt.Rows[0]["PhoneNumber"]);
                    objOrder.Pack_TotalWeight = Convert.ToDouble(objDt.Rows[0]["TotalWeight"]);
                    objOrder.ShippingMethod = Convert.ToString(objDt.Rows[0]["PostalServiceName"]);
                    objOrder.PostalTrackingNumber = Convert.ToString(objDt.Rows[0]["TrackingNumber"]);
                    objOrder.CountryCode = Convert.ToString(objDt.Rows[0]["CountryCode"]);
                    objOrder.Country = Convert.ToString(objDt.Rows[0]["CountryName"]);
                    objOrder.ExternalReferenceNum = Convert.ToString(objDt.Rows[0]["ExternalReferenceNum"]);
                    objOrder.Company = Convert.ToString(objDt.Rows[0]["Company"]);
                    objOrder.Region = Convert.ToString(objDt.Rows[0]["Region"]);
                    objOrder.ReferenceNum = Convert.ToString(objDt.Rows[0]["ReferenceNum"]);
                    objOrderItem.Qty = Convert.ToInt32(objDt.Rows[0]["Quantity"]);
                    objOrderItem.Title = Convert.ToString(objDt.Rows[0]["Title"]);
                    objOrderItems.Add(objOrderItem);
                    objOrder.OrderItems = objOrderItems;
                    //End of code to integrate API.

                    //Mapping for email functionality 
                    objSendEmail.OrderID = Order_ID;
                    objSendEmail.NumberOrderID = Convert.ToString(objDt.Rows[0]["NumOrderId"]);
                    objSendEmail.Date = Convert.ToDateTime(Convert.ToString(objDt.Rows[0]["DespatchByDate"]));
                    objSendEmail.FullName = Convert.ToString(objDt.Rows[0]["FullName"]);
                    objSendEmail.EmailID = Convert.ToString(objDt.Rows[0]["EmailAddress"]);
                    objSendEmail.Mode = "INSERT";
                    //End of Mapping for email functionality
                    result = true;
                }
            }
            catch (Exception Ex)
            {
                GlobalVariablesAndMethods.LogHistory("Error occurred in populateVariablesToPrintLabelOrProcess:- " + Ex.Message);
                MessageBox.Show(Ex.Message, "Error", MessageBoxButtons.OK);
                result = false;
            }
            return result;
        }

        private void UpdateTrackingNumber()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objManifest = new SqlParameter[5];

                objManifest[0] = new SqlParameter("@OrderID", SqlDbType.UniqueIdentifier);
                objManifest[0].Value = Orderid;

                objManifest[1] = new SqlParameter("@TrackingNumber", SqlDbType.VarChar, 200);
                objManifest[1].Value = strTrackingNumber;

                objManifest[2] = new SqlParameter("@ServiceName", SqlDbType.VarChar, 200);
                objManifest[2].Value = strServiceName;

                objManifest[3] = new SqlParameter("@Manifested", SqlDbType.Bit);
                objManifest[3].Value = false;

                objManifest[4] = new SqlParameter("@Result", SqlDbType.Int);
                objManifest[4].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateTrackingNumberForManifest", objManifest);
                int intResult = Convert.ToInt32(objManifest[4].Value);
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Error occurred in UpdateTrackingNumber" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
                CN22Form.PrintForOrder(Orderid);
            }
        }
        private void PrintInvoiceForMultiple()
        {
            currentProcessState = Tuple.Create(ProcessState.Loading, grdOrders.SelectedRows[0].Index, true);
            var rowDataWithNumOrderId = checkAndIterate("NumOrderId");
            var rowDataWithOrderId = checkAndIterate("OrderId");
            foreach (var data in rowDataWithNumOrderId.OrderBy(x => x.RowIndex).ToList())
            {
                try
                {
                    var orderId = rowDataWithOrderId.Where(x => x.RowIndex == data.RowIndex).FirstOrDefault().FieldData.ToString();
                    var currentRowIndex = data.RowIndex;
                    currentProcessState = Tuple.Create(ProcessState.Printing, currentRowIndex, true);
                    CheckForIllegalCrossThreadCalls = false;
                    var threadFinished = false;
                    var invoicePrintThread = new Thread(new ThreadStart(() =>
                    {
                        try
                        {
                            Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().PrintInvoiceForOrder(Guid.Parse(orderId),false);
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message); }
                        threadFinished = true;
                    }));
                    invoicePrintThread.SetApartmentState(ApartmentState.STA);
                    invoicePrintThread.Start();
                    while (!threadFinished)
                    {
                        Task t = new Task(() => { });
                        t.Wait(500);
                        t.Start();

                    }
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    currentProcessState = Tuple.Create(ProcessState.Completed, currentRowIndex, true);
                    UpdateProcessStateOfOrder(new Guid(orderId), ProcessCompleted.Invoice_Printed);
                }
                catch (Exception ex)
                {
                    currentProcessState = Tuple.Create(ProcessState.Failed, data.RowIndex, true);
                    PicLoading.Visible = false;
                    PicRight.Visible = false;
                    PicError.Visible = true;
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void PrintInvoiceProcess()
        {
            currentProcessState = Tuple.Create(ProcessState.Loading, grdOrders.SelectedRows[0].Index, true);
            var rowDataWithNumOrderId = checkAndIterate("NumOrderId");
            var rowDataWithOrderId = checkAndIterate("OrderId");
            foreach (var data in rowDataWithNumOrderId.OrderBy(x => x.RowIndex).ToList())
            {
                try
                {
                    var orderId = rowDataWithOrderId.Where(x => x.RowIndex == data.RowIndex).FirstOrDefault().FieldData.ToString();
                    OrderTracker.BeginTrackerForOrder(orderId);
                    var currentRowIndex = data.RowIndex;
                    currentProcessState = Tuple.Create(ProcessState.Loading, currentRowIndex, true);
                    CheckForIllegalCrossThreadCalls = false;

                    var numOrderId = data.FieldData;
                    currentProcessState = Tuple.Create(ProcessState.Printing, currentRowIndex, true);
                    objPdf.CreateInvoiceDetails(orderId, true, numOrderId);
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    currentProcessState = Tuple.Create(ProcessState.Completed, currentRowIndex, true);
                   // UpdateProcessStateOfOrder(new Guid(orderId), ProcessCompleted.Invoice_Printed);
                }
                catch (Exception ex)
                {
                    currentProcessState = Tuple.Create(ProcessState.Failed, data.RowIndex, true);
                    PicLoading.Visible = false;
                    PicRight.Visible = false;
                    PicError.Visible = true;
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                OrderTracker.EndTracking();
            }
        }
        private void UpdateProcessStatusInDB()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@OrderID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = Orderid;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "ProcessOrderFromContextMenu", objParam);
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Error occurred in UpdateProcessStatusInDb:- " + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        private void RemoveSelectedOrder(string strOrderID)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@OrderID", SqlDbType.VarChar, Int32.MaxValue);
                objParam[0].Value = strOrderID;

                objParam[1] = new SqlParameter("@Result", SqlDbType.Int);
                objParam[1].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "RemoveCancelledAndProcessedOrders_New", objParam);
                int intResult = Convert.ToInt32(objParam[1].Value);
                if (intResult == 1)
                {
                    blIsOrderDeleted = true;
                    MessageBox.Show("Order Removed Successfully", "Order Removed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Error occured while deleting orders " + Environment.NewLine + "Please try after some time." + Environment.NewLine + "Error code : - " + intResult.ToString());
                    GlobalVariablesAndMethods.LogHistory("Error returned from Database, Error Number :-" + intResult.ToString());
                }
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Error occurred in RemoveSelectedOrder:- " + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        private DataTable SetDataSet(DataTable objDt)
        {
            try
            {
                objDt.Columns.Add("TotalNumberOfItems", typeof(Int32));
                DataTable FinalDataSet = new DataTable();
                DataView objDv = new DataView(objDt);
                DataTable DistinctRows = objDv.ToTable(true, "NumOrderId");
                FinalDataSet = objDt.Clone();
                int intOrderNum, intCount;
                for (int i = 0; i < DistinctRows.Rows.Count; i++)
                {
                    intOrderNum = Convert.ToInt32(DistinctRows.Rows[i]["NumOrderId"]);
                    intCount = 0;

                    DataView DvFiltered = new DataView(objDt);
                    DvFiltered.RowFilter = "NumOrderId =" + intOrderNum;
                    DataTable DtFiltered = new DataTable();
                    DtFiltered = DvFiltered.ToTable();
                    var sku = string.Empty;
                    foreach (DataRow MainRow in DtFiltered.Rows)
                    {
                        if (intCount == 0)
                        {
                            FinalDataSet.ImportRow(MainRow);
                        }
                        else
                        {
                            FinalDataSet.Rows[i]["Items"] = Convert.ToString(FinalDataSet.Rows[i]["Items"]) + "\n\n" + Convert.ToString(MainRow["Items"]);
                        }
                        var totalItems = FinalDataSet.Rows[i]["TotalNumberOfItems"].GetType().Equals(typeof(DBNull)) ? 0 : FinalDataSet.Rows[i]["TotalNumberOfItems"];
                        var numberOfItems = Convert.ToInt32(MainRow["NumOfDistinctItem"].ToString());
                        FinalDataSet.Rows[i]["TotalNumberOfItems"] = (Convert.ToInt32(totalItems) + numberOfItems);
                        sku += MainRow["SKU"].ToString().TrimEnd() + ",";
                        intCount++;
                    }
                    FinalDataSet.Rows[i]["NumOfDistinctItem"] = DtFiltered.Rows.Count;
                    FinalDataSet.Rows[i]["SKU"] = sku;
                }
                return FinalDataSet;
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Error occurred in SetDataSet:- " + ex.Message);
                throw ex;
            }
        }
        #endregion
    }
}
