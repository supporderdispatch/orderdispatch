﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ODM.Appliction.Extensions
{
    public class ColumnWithDisplayIndex
    {
        public string ColumnName { get; set; }
        public int DisplayIndex { get; set; }
    }
}
