﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ODM.Appliction.Extensions
{
    public class CellAndValue
    {
        public string CellColumnName { get; set; }
        public string CellValue { get; set; }
    }
}
