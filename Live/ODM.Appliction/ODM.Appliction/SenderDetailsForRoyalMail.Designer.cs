﻿namespace ODM.Appliction
{
    partial class SenderDetailsForRoyalMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SenderDetailsForRoyalMail));
            this.lblBuildingName = new System.Windows.Forms.Label();
            this.txtBuildingName = new System.Windows.Forms.TextBox();
            this.txtBuildingNumber = new System.Windows.Forms.TextBox();
            this.lblBuildingNumber = new System.Windows.Forms.Label();
            this.txtAddressLine1 = new System.Windows.Forms.TextBox();
            this.lblAddressLine1 = new System.Windows.Forms.Label();
            this.txtAddressLine2 = new System.Windows.Forms.TextBox();
            this.lblAddressLine2 = new System.Windows.Forms.Label();
            this.txtAddressLine3 = new System.Windows.Forms.TextBox();
            this.lblAddressLine3 = new System.Windows.Forms.Label();
            this.txtPostTown = new System.Windows.Forms.TextBox();
            this.lblPostTown = new System.Windows.Forms.Label();
            this.txtPostCode = new System.Windows.Forms.TextBox();
            this.lblPostCode = new System.Windows.Forms.Label();
            this.lblCountryID = new System.Windows.Forms.Label();
            this.ddlCountry = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblBuildingNameRequired = new System.Windows.Forms.Label();
            this.lblBuildingNumberRequired = new System.Windows.Forms.Label();
            this.lblAddressLine1Required = new System.Windows.Forms.Label();
            this.lblAddressLine2Required = new System.Windows.Forms.Label();
            this.lblAddressLine3Required = new System.Windows.Forms.Label();
            this.lblPostTownRequired = new System.Windows.Forms.Label();
            this.lblPostCodeRequired = new System.Windows.Forms.Label();
            this.lblCountryRequired = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblBuildingName
            // 
            this.lblBuildingName.AutoSize = true;
            this.lblBuildingName.Location = new System.Drawing.Point(12, 13);
            this.lblBuildingName.Name = "lblBuildingName";
            this.lblBuildingName.Size = new System.Drawing.Size(75, 13);
            this.lblBuildingName.TabIndex = 0;
            this.lblBuildingName.Text = "Building Name";
            // 
            // txtBuildingName
            // 
            this.txtBuildingName.Location = new System.Drawing.Point(104, 13);
            this.txtBuildingName.Name = "txtBuildingName";
            this.txtBuildingName.Size = new System.Drawing.Size(168, 20);
            this.txtBuildingName.TabIndex = 1;
            // 
            // txtBuildingNumber
            // 
            this.txtBuildingNumber.Location = new System.Drawing.Point(104, 46);
            this.txtBuildingNumber.Name = "txtBuildingNumber";
            this.txtBuildingNumber.Size = new System.Drawing.Size(168, 20);
            this.txtBuildingNumber.TabIndex = 3;
            // 
            // lblBuildingNumber
            // 
            this.lblBuildingNumber.AutoSize = true;
            this.lblBuildingNumber.Location = new System.Drawing.Point(12, 46);
            this.lblBuildingNumber.Name = "lblBuildingNumber";
            this.lblBuildingNumber.Size = new System.Drawing.Size(84, 13);
            this.lblBuildingNumber.TabIndex = 2;
            this.lblBuildingNumber.Text = "Building Number";
            // 
            // txtAddressLine1
            // 
            this.txtAddressLine1.Location = new System.Drawing.Point(104, 82);
            this.txtAddressLine1.Name = "txtAddressLine1";
            this.txtAddressLine1.Size = new System.Drawing.Size(168, 20);
            this.txtAddressLine1.TabIndex = 5;
            // 
            // lblAddressLine1
            // 
            this.lblAddressLine1.AutoSize = true;
            this.lblAddressLine1.Location = new System.Drawing.Point(12, 82);
            this.lblAddressLine1.Name = "lblAddressLine1";
            this.lblAddressLine1.Size = new System.Drawing.Size(71, 13);
            this.lblAddressLine1.TabIndex = 4;
            this.lblAddressLine1.Text = "AddressLine1";
            // 
            // txtAddressLine2
            // 
            this.txtAddressLine2.Location = new System.Drawing.Point(104, 118);
            this.txtAddressLine2.Name = "txtAddressLine2";
            this.txtAddressLine2.Size = new System.Drawing.Size(168, 20);
            this.txtAddressLine2.TabIndex = 7;
            // 
            // lblAddressLine2
            // 
            this.lblAddressLine2.AutoSize = true;
            this.lblAddressLine2.Location = new System.Drawing.Point(12, 118);
            this.lblAddressLine2.Name = "lblAddressLine2";
            this.lblAddressLine2.Size = new System.Drawing.Size(71, 13);
            this.lblAddressLine2.TabIndex = 6;
            this.lblAddressLine2.Text = "AddressLine2";
            // 
            // txtAddressLine3
            // 
            this.txtAddressLine3.Location = new System.Drawing.Point(494, 10);
            this.txtAddressLine3.Name = "txtAddressLine3";
            this.txtAddressLine3.Size = new System.Drawing.Size(168, 20);
            this.txtAddressLine3.TabIndex = 9;
            // 
            // lblAddressLine3
            // 
            this.lblAddressLine3.AutoSize = true;
            this.lblAddressLine3.Location = new System.Drawing.Point(402, 13);
            this.lblAddressLine3.Name = "lblAddressLine3";
            this.lblAddressLine3.Size = new System.Drawing.Size(71, 13);
            this.lblAddressLine3.TabIndex = 8;
            this.lblAddressLine3.Text = "AddressLine3";
            // 
            // txtPostTown
            // 
            this.txtPostTown.Location = new System.Drawing.Point(494, 43);
            this.txtPostTown.Name = "txtPostTown";
            this.txtPostTown.Size = new System.Drawing.Size(168, 20);
            this.txtPostTown.TabIndex = 11;
            // 
            // lblPostTown
            // 
            this.lblPostTown.AutoSize = true;
            this.lblPostTown.Location = new System.Drawing.Point(402, 46);
            this.lblPostTown.Name = "lblPostTown";
            this.lblPostTown.Size = new System.Drawing.Size(58, 13);
            this.lblPostTown.TabIndex = 10;
            this.lblPostTown.Text = "Post Town";
            // 
            // txtPostCode
            // 
            this.txtPostCode.Location = new System.Drawing.Point(494, 76);
            this.txtPostCode.Name = "txtPostCode";
            this.txtPostCode.Size = new System.Drawing.Size(168, 20);
            this.txtPostCode.TabIndex = 13;
            // 
            // lblPostCode
            // 
            this.lblPostCode.AutoSize = true;
            this.lblPostCode.Location = new System.Drawing.Point(402, 79);
            this.lblPostCode.Name = "lblPostCode";
            this.lblPostCode.Size = new System.Drawing.Size(56, 13);
            this.lblPostCode.TabIndex = 12;
            this.lblPostCode.Text = "Post Code";
            // 
            // lblCountryID
            // 
            this.lblCountryID.AutoSize = true;
            this.lblCountryID.Location = new System.Drawing.Point(402, 115);
            this.lblCountryID.Name = "lblCountryID";
            this.lblCountryID.Size = new System.Drawing.Size(74, 13);
            this.lblCountryID.TabIndex = 14;
            this.lblCountryID.Text = "Country Name";
            // 
            // ddlCountry
            // 
            this.ddlCountry.FormattingEnabled = true;
            this.ddlCountry.Location = new System.Drawing.Point(494, 112);
            this.ddlCountry.Name = "ddlCountry";
            this.ddlCountry.Size = new System.Drawing.Size(168, 21);
            this.ddlCountry.TabIndex = 62;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(390, 168);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(107, 29);
            this.btnCancel.TabIndex = 65;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(262, 169);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 29);
            this.btnSave.TabIndex = 64;
            this.btnSave.Text = "Save && Close";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblBuildingNameRequired
            // 
            this.lblBuildingNameRequired.AutoSize = true;
            this.lblBuildingNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblBuildingNameRequired.Location = new System.Drawing.Point(278, 17);
            this.lblBuildingNameRequired.Name = "lblBuildingNameRequired";
            this.lblBuildingNameRequired.Size = new System.Drawing.Size(50, 13);
            this.lblBuildingNameRequired.TabIndex = 66;
            this.lblBuildingNameRequired.Text = "Required";
            this.lblBuildingNameRequired.Visible = false;
            // 
            // lblBuildingNumberRequired
            // 
            this.lblBuildingNumberRequired.AutoSize = true;
            this.lblBuildingNumberRequired.ForeColor = System.Drawing.Color.Red;
            this.lblBuildingNumberRequired.Location = new System.Drawing.Point(278, 50);
            this.lblBuildingNumberRequired.Name = "lblBuildingNumberRequired";
            this.lblBuildingNumberRequired.Size = new System.Drawing.Size(50, 13);
            this.lblBuildingNumberRequired.TabIndex = 67;
            this.lblBuildingNumberRequired.Text = "Required";
            this.lblBuildingNumberRequired.Visible = false;
            // 
            // lblAddressLine1Required
            // 
            this.lblAddressLine1Required.AutoSize = true;
            this.lblAddressLine1Required.ForeColor = System.Drawing.Color.Red;
            this.lblAddressLine1Required.Location = new System.Drawing.Point(278, 82);
            this.lblAddressLine1Required.Name = "lblAddressLine1Required";
            this.lblAddressLine1Required.Size = new System.Drawing.Size(50, 13);
            this.lblAddressLine1Required.TabIndex = 68;
            this.lblAddressLine1Required.Text = "Required";
            this.lblAddressLine1Required.Visible = false;
            // 
            // lblAddressLine2Required
            // 
            this.lblAddressLine2Required.AutoSize = true;
            this.lblAddressLine2Required.ForeColor = System.Drawing.Color.Red;
            this.lblAddressLine2Required.Location = new System.Drawing.Point(278, 118);
            this.lblAddressLine2Required.Name = "lblAddressLine2Required";
            this.lblAddressLine2Required.Size = new System.Drawing.Size(50, 13);
            this.lblAddressLine2Required.TabIndex = 69;
            this.lblAddressLine2Required.Text = "Required";
            this.lblAddressLine2Required.Visible = false;
            // 
            // lblAddressLine3Required
            // 
            this.lblAddressLine3Required.AutoSize = true;
            this.lblAddressLine3Required.ForeColor = System.Drawing.Color.Red;
            this.lblAddressLine3Required.Location = new System.Drawing.Point(677, 13);
            this.lblAddressLine3Required.Name = "lblAddressLine3Required";
            this.lblAddressLine3Required.Size = new System.Drawing.Size(50, 13);
            this.lblAddressLine3Required.TabIndex = 70;
            this.lblAddressLine3Required.Text = "Required";
            this.lblAddressLine3Required.Visible = false;
            // 
            // lblPostTownRequired
            // 
            this.lblPostTownRequired.AutoSize = true;
            this.lblPostTownRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPostTownRequired.Location = new System.Drawing.Point(677, 46);
            this.lblPostTownRequired.Name = "lblPostTownRequired";
            this.lblPostTownRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPostTownRequired.TabIndex = 71;
            this.lblPostTownRequired.Text = "Required";
            this.lblPostTownRequired.Visible = false;
            // 
            // lblPostCodeRequired
            // 
            this.lblPostCodeRequired.AutoSize = true;
            this.lblPostCodeRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPostCodeRequired.Location = new System.Drawing.Point(677, 79);
            this.lblPostCodeRequired.Name = "lblPostCodeRequired";
            this.lblPostCodeRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPostCodeRequired.TabIndex = 72;
            this.lblPostCodeRequired.Text = "Required";
            this.lblPostCodeRequired.Visible = false;
            // 
            // lblCountryRequired
            // 
            this.lblCountryRequired.AutoSize = true;
            this.lblCountryRequired.ForeColor = System.Drawing.Color.Red;
            this.lblCountryRequired.Location = new System.Drawing.Point(677, 115);
            this.lblCountryRequired.Name = "lblCountryRequired";
            this.lblCountryRequired.Size = new System.Drawing.Size(50, 13);
            this.lblCountryRequired.TabIndex = 73;
            this.lblCountryRequired.Text = "Required";
            this.lblCountryRequired.Visible = false;
            // 
            // SenderDetailsForRoyalMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 231);
            this.Controls.Add(this.lblCountryRequired);
            this.Controls.Add(this.lblPostCodeRequired);
            this.Controls.Add(this.lblPostTownRequired);
            this.Controls.Add(this.lblAddressLine3Required);
            this.Controls.Add(this.lblAddressLine2Required);
            this.Controls.Add(this.lblAddressLine1Required);
            this.Controls.Add(this.lblBuildingNumberRequired);
            this.Controls.Add(this.lblBuildingNameRequired);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ddlCountry);
            this.Controls.Add(this.lblCountryID);
            this.Controls.Add(this.txtPostCode);
            this.Controls.Add(this.lblPostCode);
            this.Controls.Add(this.txtPostTown);
            this.Controls.Add(this.lblPostTown);
            this.Controls.Add(this.txtAddressLine3);
            this.Controls.Add(this.lblAddressLine3);
            this.Controls.Add(this.txtAddressLine2);
            this.Controls.Add(this.lblAddressLine2);
            this.Controls.Add(this.txtAddressLine1);
            this.Controls.Add(this.lblAddressLine1);
            this.Controls.Add(this.txtBuildingNumber);
            this.Controls.Add(this.lblBuildingNumber);
            this.Controls.Add(this.txtBuildingName);
            this.Controls.Add(this.lblBuildingName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SenderDetailsForRoyalMail";
            this.Text = "Sender Details For Royal Mail";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBuildingName;
        private System.Windows.Forms.TextBox txtBuildingName;
        private System.Windows.Forms.TextBox txtBuildingNumber;
        private System.Windows.Forms.Label lblBuildingNumber;
        private System.Windows.Forms.TextBox txtAddressLine1;
        private System.Windows.Forms.Label lblAddressLine1;
        private System.Windows.Forms.TextBox txtAddressLine2;
        private System.Windows.Forms.Label lblAddressLine2;
        private System.Windows.Forms.TextBox txtAddressLine3;
        private System.Windows.Forms.Label lblAddressLine3;
        private System.Windows.Forms.TextBox txtPostTown;
        private System.Windows.Forms.Label lblPostTown;
        private System.Windows.Forms.TextBox txtPostCode;
        private System.Windows.Forms.Label lblPostCode;
        private System.Windows.Forms.Label lblCountryID;
        private System.Windows.Forms.ComboBox ddlCountry;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblBuildingNameRequired;
        private System.Windows.Forms.Label lblBuildingNumberRequired;
        private System.Windows.Forms.Label lblAddressLine1Required;
        private System.Windows.Forms.Label lblAddressLine2Required;
        private System.Windows.Forms.Label lblAddressLine3Required;
        private System.Windows.Forms.Label lblPostTownRequired;
        private System.Windows.Forms.Label lblPostCodeRequired;
        private System.Windows.Forms.Label lblCountryRequired;
    }
}