﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class AdditionalEmailAccount : Form
    {
        DataTable objDtMailAccDetails = new DataTable();
        DataView DvMailDetails = new DataView();
        bool blIsFirstCall = false;
        Email objEmail = new Email();
        public AdditionalEmailAccount()
        {
            InitializeComponent();
            blIsFirstCall = true;
            BindEmaiAccounts();
        }

        private void btbDelete_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            btnNewAccount.Enabled = false;

            Form frmDeleteEmail = Application.OpenForms["DeleteEmailAccount"];
            if (frmDeleteEmail != null)
            {
                frmDeleteEmail.Close();
            }
            DeleteEmailAccount objDeleteEmailAccount = new DeleteEmailAccount(GlobalVariablesAndMethods.GetEmailAccounts());
            objDeleteEmailAccount.Show();
        }

        private void btnNewAccount_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            btbDelete.Enabled = false;

            Form frmAddEmailAccount = Application.OpenForms["AddEmailAccount"];
            if (frmAddEmailAccount == null)
            {
                AddEmailAccount objAddEmailAccount = new AddEmailAccount();
                objAddEmailAccount.Show();
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateControls())
                {
                    string strMsgToShow = "";
                    bool blIsValidSMTP = objEmail.ValidateSMTP(txtSMTPServer.Text, Convert.ToInt32(txtPort.Text));
                    strMsgToShow = blIsValidSMTP ? "Looks fine" : "Invalid SMTP";
                    MessageBox.Show(strMsgToShow, "Validate SMTP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                AddUpdateDeleteEmailAccount(strMode: "UPDATE");
                MessageBox.Show("Record Updated Successfully", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private bool ValidateControls()
        {
            bool IsValid = true;
            if (ddlEmaiAccount.Text != "" && ddlEmaiAccount.SelectedValue == null)
            {
                lblEmailAccRequired.Visible = true;
                lblEmailAccRequired.Text = "Invalid Email Account";
                IsValid = false;
            }
            else if (ddlEmaiAccount.Text == "" && ddlEmaiAccount.SelectedValue == null)
            {
                lblEmailAccRequired.Visible = true;
                lblEmailAccRequired.Text = "Required";
                IsValid = false;
            }
            else
            {
                lblEmailAccRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtSMTPServer.Text))
            {
                lblSMTPRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblSMTPRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPort.Text))
            {
                lblPortRequired.Visible = true;
                lblPortRequired.Text = "Required";
                IsValid = false;
            }                                           
            else
            {
                int intPortNumber = 0;
                bool blIsValidInt = int.TryParse(txtPort.Text, out intPortNumber);
                if (blIsValidInt)
                {
                    lblPortRequired.Visible = false;
                }
                else
                {
                    lblPortRequired.Visible = true;
                    lblPortRequired.Text = "Invalid Port";
                    IsValid = false;
                }
            }

            if (string.IsNullOrWhiteSpace(txtUserName.Text))
            {
                lblUserNameRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblUserNameRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPassword.Text))
            {
                lblPasswordRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPasswordRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtReplyToEmail.Text))
            {
                lblReplyToEmailRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblReplyToEmailRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtFromName.Text))
            {
                lblFromNameRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblFromNameRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtSendToCopy.Text))
            {
                lblSendToCopyRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblSendToCopyRequired.Visible = false;
            }
            return IsValid;
        }

        /// <summary>
        /// To Add, Update and Delete email account, we are using that as a generic method
        /// </summary>
        /// <param name="strMode">Mode can be ADD, UPATE or DELETE, 
        /// we need values of all other fileds except ID and Email account only for UPDATE</param>
        /// <param name="strEmailAccount">This is required while adding email account from AddEmailAccount Screen</param>
        public string AddUpdateDeleteEmailAccount(string strMode, string strEmailAccount = "", string strAccountIdsToDelete = "")
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            string strResult = string.Empty;
            try
            {
                connection.Open();
                Guid ID = new Guid();
                if (ddlEmaiAccount.SelectedValue != null && strEmailAccount.ToUpper() != "DEFAULT" && strMode.ToUpper()!="ADD")
                {
                    ID = new Guid(ddlEmaiAccount.SelectedValue.ToString());
                }
                else if (strEmailAccount.ToUpper() != "DEFAULT" && strMode.ToUpper() == "ADD")
                {
                    ID = Guid.NewGuid();
                }
                SqlParameter[] objParam = new SqlParameter[15];

                objParam[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = ID;

                objParam[1] = new SqlParameter("@EmailAccount", SqlDbType.VarChar, 50);
                objParam[1].Value = strEmailAccount != "" ? strEmailAccount : ddlEmaiAccount.Text;

                objParam[2] = new SqlParameter("@IsEnabled", SqlDbType.Bit);
                objParam[2].Value = strMode == "UPDATE" ? chkEnabled.Checked : true;

                objParam[3] = new SqlParameter("@SMTP_Server", SqlDbType.VarChar, 50);
                objParam[3].Value = strMode == "UPDATE" ? txtSMTPServer.Text : "";

                objParam[4] = new SqlParameter("@Port", SqlDbType.VarChar, 10);
                objParam[4].Value = strMode == "UPDATE" ? txtPort.Text : "";

                objParam[5] = new SqlParameter("@SMTP_Auth", SqlDbType.Bit);
                objParam[5].Value = strMode == "UPDATE" ? chkSMTP_Auth.Checked : false;

                objParam[6] = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
                objParam[6].Value = strMode == "UPDATE" ? txtUserName.Text : "";

                objParam[7] = new SqlParameter("@Password", SqlDbType.VarChar, 50);
                objParam[7].Value = strMode == "UPDATE" ? txtPassword.Text : "";

                objParam[8] = new SqlParameter("@IsSSL", SqlDbType.Bit);
                objParam[8].Value = strMode == "UPDATE" ? chkSSL.Checked : false;

                objParam[9] = new SqlParameter("@ReplyToEmail", SqlDbType.VarChar, 50);
                objParam[9].Value = strMode == "UPDATE" ? txtReplyToEmail.Text : "";

                objParam[10] = new SqlParameter("@FromName", SqlDbType.VarChar, 50);
                objParam[10].Value = strMode == "UPDATE" ? txtFromName.Text : "";

                objParam[11] = new SqlParameter("@SendCopyTo", SqlDbType.VarChar, 50);
                objParam[11].Value = strMode == "UPDATE" ? txtSendToCopy.Text : "";

                objParam[12] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[12].Value = strMode;

                objParam[13] = new SqlParameter("@IdsToDelete", SqlDbType.VarChar, 8000);
                objParam[13].Value = strMode == "DELETE" ? strAccountIdsToDelete : "";

                objParam[14] = new SqlParameter("@Result", SqlDbType.VarChar, 50);
                objParam[14].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteAdditionalEmail", objParam);
                strResult = objParam[14].Value.ToString();
            }
            catch (Exception)
            {
                string strError = "Error occurred while Updating Additional Email Account";
                switch (strMode.ToUpper())
                {
                    case "ADD":
                        strError += "Adding";
                        break;

                    case "UPDATE":
                        strError += "Updating";
                        break;

                    case "DELETE":
                        strError += "Deleting";
                        break;
                }
                strError += " Additional Email Account";
                MessageBox.Show(strError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            return strResult;
        }
        private void BindEmaiAccounts()
        {
            objDtMailAccDetails = GlobalVariablesAndMethods.GetEmailAccounts();
            ddlEmaiAccount.DataSource = objDtMailAccDetails;
            ddlEmaiAccount.ValueMember = "ID";
            ddlEmaiAccount.DisplayMember = "EmailAccount";
            ddlEmaiAccount.SelectedValue = "00000000-0000-0000-0000-000000000000";
            chkEnabled.Enabled = false;
            blIsFirstCall = false;
        }

        private void PopulateControls(DataView DvEmailAccount)
        {
            DataTable objDt = new DataTable();
            if (DvEmailAccount.ToTable().Rows.Count > 0)
            {
                objDt = DvEmailAccount.ToTable();
                if (ddlEmaiAccount.SelectedValue != null && ddlEmaiAccount.SelectedValue.ToString() == "00000000-0000-0000-0000-000000000000")
                {
                    chkEnabled.Enabled = false;
                }
                else
                {
                    chkEnabled.Enabled = true;
                }
                string strIsEnabled = Convert.ToString(objDt.Rows[0]["IsEnabled"]);
                string strAuth = Convert.ToString(objDt.Rows[0]["SMTP_Auth"]);
                string strSSL = Convert.ToString(objDt.Rows[0]["IsSSL"]);

                chkEnabled.Checked = string.IsNullOrEmpty(strIsEnabled) ? false : Convert.ToBoolean(strIsEnabled);
                txtSMTPServer.Text = Convert.ToString(objDt.Rows[0]["SMTP_Server"]);
                txtPort.Text = Convert.ToString(objDt.Rows[0]["Port"]);
                chkSMTP_Auth.Checked = string.IsNullOrEmpty(strAuth) ? false : Convert.ToBoolean(strAuth);
                txtUserName.Text = Convert.ToString(objDt.Rows[0]["UserName"]);
                txtPassword.Text = Convert.ToString(objDt.Rows[0]["Password"]);
                chkSSL.Checked = string.IsNullOrEmpty(strSSL) ? false : Convert.ToBoolean(strSSL);
                txtReplyToEmail.Text = Convert.ToString(objDt.Rows[0]["ReplyToEmail"]);
                txtFromName.Text = Convert.ToString(objDt.Rows[0]["FromName"]);
                txtSendToCopy.Text = Convert.ToString(objDt.Rows[0]["SendCopyTo"]);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ddlEmaiAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            DvMailDetails = new DataView(GlobalVariablesAndMethods.GetEmailAccounts());
            if (DvMailDetails.ToTable().Rows.Count > 0)
            {
                if (blIsFirstCall)
                {
                    string strID = Convert.ToString(DvMailDetails.ToTable().Rows[0]["ID"]);
                    DvMailDetails.RowFilter = "ID ='" + strID + "'";
                }
                else
                {
                    DvMailDetails.RowFilter = "ID ='" + ddlEmaiAccount.SelectedValue + "'";
                }
            }
            PopulateControls(DvMailDetails);
        }
    }
}
