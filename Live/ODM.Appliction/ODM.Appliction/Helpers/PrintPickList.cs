﻿using ODM.Core;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction.Helpers
{
    public partial class PrintPickList : Form
    {
        private string commaSepratedselectedOrderIds;
        public static string batchName;
        private int totalNumberOfItems;
        private int skuCount;

        public PrintPickList(string batchNameParam, string commaSepratedselectedOrderIds,int totalNumberOfItems, int totalSku)
        {
            InitializeComponent();
            this.commaSepratedselectedOrderIds = commaSepratedselectedOrderIds;
            batchName = batchNameParam;
            this.skuCount = totalSku;
            this.totalNumberOfItems = totalNumberOfItems;
            SetDefaults();
            RegisterEvents();
            PicLoading.Visible = false;
        }

        private void SetDefaults()
        {
            txtBatchName.Text = batchName;
            lblTotalNumberOfItems.Text = totalNumberOfItems.ToString();
            lblTotalSkuCount.Text = this.skuCount.ToString();
        }

        private  void RegisterEvents()
        {
            
            btnPrint.Click += (sender, e) =>
            {
                CheckForIllegalCrossThreadCalls = false;
                PicLoading.Visible = true;
                new Thread(new ThreadStart(() => {
                    var batchName = txtBatchName.Text;
                    if (!string.IsNullOrEmpty(batchName))
                    {
                        Activator.CreateInstance<PickList>().CreateAndPrintPickListForBatch(batchName, commaSepratedselectedOrderIds);

                    }
                    batchName = txtBatchName.Text;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                
                })).Start();
            };

            txtBatchName.KeyPress += (sender, e) =>
            {
                if (e.KeyChar == '_')
                {
                    e.KeyChar = '-';
                }
            };
        }

    }
}
