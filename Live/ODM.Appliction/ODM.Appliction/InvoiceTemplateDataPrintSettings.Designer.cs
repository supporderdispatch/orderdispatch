﻿namespace ODM.Appliction
{
    partial class InvoiceTemplateDataPrintSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InvoiceTemplateDataPrintSettings));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabDataPrintSettings = new System.Windows.Forms.TabControl();
            this.tabTableContentSettings = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.chkTryAdjustTemplateBlock = new System.Windows.Forms.CheckBox();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.grdDescription = new System.Windows.Forms.DataGridView();
            this.cmbTemplates = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTableContentExceedOptions = new System.Windows.Forms.ComboBox();
            this.tabDataPrintSettings.SuspendLayout();
            this.tabTableContentSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDescription)).BeginInit();
            this.SuspendLayout();
            // 
            // tabDataPrintSettings
            // 
            this.tabDataPrintSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabDataPrintSettings.Controls.Add(this.tabTableContentSettings);
            this.tabDataPrintSettings.Location = new System.Drawing.Point(12, 20);
            this.tabDataPrintSettings.Name = "tabDataPrintSettings";
            this.tabDataPrintSettings.SelectedIndex = 0;
            this.tabDataPrintSettings.Size = new System.Drawing.Size(769, 330);
            this.tabDataPrintSettings.TabIndex = 1;
            // 
            // tabTableContentSettings
            // 
            this.tabTableContentSettings.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabTableContentSettings.Controls.Add(this.label2);
            this.tabTableContentSettings.Controls.Add(this.chkTryAdjustTemplateBlock);
            this.tabTableContentSettings.Controls.Add(this.btnSaveSettings);
            this.tabTableContentSettings.Controls.Add(this.picHelp);
            this.tabTableContentSettings.Controls.Add(this.grdDescription);
            this.tabTableContentSettings.Controls.Add(this.cmbTemplates);
            this.tabTableContentSettings.Controls.Add(this.label1);
            this.tabTableContentSettings.Controls.Add(this.cmbTableContentExceedOptions);
            this.tabTableContentSettings.Location = new System.Drawing.Point(4, 22);
            this.tabTableContentSettings.Name = "tabTableContentSettings";
            this.tabTableContentSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabTableContentSettings.Size = new System.Drawing.Size(761, 304);
            this.tabTableContentSettings.TabIndex = 2;
            this.tabTableContentSettings.Text = "Table Content Settings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(206, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "if not work ";
            // 
            // chkTryAdjustTemplateBlock
            // 
            this.chkTryAdjustTemplateBlock.AutoSize = true;
            this.chkTryAdjustTemplateBlock.Checked = true;
            this.chkTryAdjustTemplateBlock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTryAdjustTemplateBlock.Location = new System.Drawing.Point(168, 41);
            this.chkTryAdjustTemplateBlock.Name = "chkTryAdjustTemplateBlock";
            this.chkTryAdjustTemplateBlock.Size = new System.Drawing.Size(155, 17);
            this.chkTryAdjustTemplateBlock.TabIndex = 6;
            this.chkTryAdjustTemplateBlock.Text = "Try Adjust Template Blocks";
            this.chkTryAdjustTemplateBlock.UseVisualStyleBackColor = true;
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveSettings.Location = new System.Drawing.Point(29, 264);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(99, 23);
            this.btnSaveSettings.TabIndex = 5;
            this.btnSaveSettings.Text = "Save Settings";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // picHelp
            // 
            this.picHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picHelp.Image = ((System.Drawing.Image)(resources.GetObject("picHelp.Image")));
            this.picHelp.Location = new System.Drawing.Point(367, 86);
            this.picHelp.Name = "picHelp";
            this.picHelp.Size = new System.Drawing.Size(19, 17);
            this.picHelp.TabIndex = 4;
            this.picHelp.TabStop = false;
            this.picHelp.Click += new System.EventHandler(this.picHelp_Click);
            // 
            // grdDescription
            // 
            this.grdDescription.AllowUserToAddRows = false;
            this.grdDescription.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grdDescription.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.grdDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdDescription.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDescription.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(1);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdDescription.DefaultCellStyle = dataGridViewCellStyle1;
            this.grdDescription.GridColor = System.Drawing.SystemColors.Control;
            this.grdDescription.Location = new System.Drawing.Point(433, 52);
            this.grdDescription.Name = "grdDescription";
            this.grdDescription.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.grdDescription.RowHeadersVisible = false;
            this.grdDescription.Size = new System.Drawing.Size(325, 153);
            this.grdDescription.TabIndex = 3;
            this.grdDescription.Visible = false;
            // 
            // cmbTemplates
            // 
            this.cmbTemplates.Enabled = false;
            this.cmbTemplates.FormattingEnabled = true;
            this.cmbTemplates.Location = new System.Drawing.Point(144, 113);
            this.cmbTemplates.Name = "cmbTemplates";
            this.cmbTemplates.Size = new System.Drawing.Size(205, 21);
            this.cmbTemplates.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Content Exceed Options";
            // 
            // cmbTableContentExceedOptions
            // 
            this.cmbTableContentExceedOptions.FormattingEnabled = true;
            this.cmbTableContentExceedOptions.ItemHeight = 13;
            this.cmbTableContentExceedOptions.Location = new System.Drawing.Point(144, 86);
            this.cmbTableContentExceedOptions.Name = "cmbTableContentExceedOptions";
            this.cmbTableContentExceedOptions.Size = new System.Drawing.Size(204, 21);
            this.cmbTableContentExceedOptions.TabIndex = 0;
            this.cmbTableContentExceedOptions.SelectedIndexChanged += new System.EventHandler(this.cmbTableContentExceedOptions_SelectedIndexChanged);
            // 
            // InvoiceTemplateDataPrintSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 362);
            this.Controls.Add(this.tabDataPrintSettings);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InvoiceTemplateDataPrintSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InvoiceTemplateDataPrintSettings";
            this.tabDataPrintSettings.ResumeLayout(false);
            this.tabTableContentSettings.ResumeLayout(false);
            this.tabTableContentSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDescription)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabDataPrintSettings;
        private System.Windows.Forms.TabPage tabTableContentSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbTableContentExceedOptions;
        private System.Windows.Forms.ComboBox cmbTemplates;
        private System.Windows.Forms.DataGridView grdDescription;
        private System.Windows.Forms.PictureBox picHelp;
        private System.Windows.Forms.Button btnSaveSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkTryAdjustTemplateBlock;

    }
}