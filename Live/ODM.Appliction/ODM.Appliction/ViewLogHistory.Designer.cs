﻿namespace ODM.Appliction
{
    partial class ViewLogHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewLogHistory));
            this.grdViewLogHistory = new System.Windows.Forms.DataGridView();
            this.lblMsg = new System.Windows.Forms.Label();
            this.txtOrderId = new System.Windows.Forms.TextBox();
            this.lblOrderID = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblRequired = new System.Windows.Forms.Label();
            this.btnErrorLogs = new System.Windows.Forms.Button();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.btnSearchByDate = new System.Windows.Forms.Button();
            this.chkFromDate = new System.Windows.Forms.CheckBox();
            this.chkToDate = new System.Windows.Forms.CheckBox();
            this.lblRecordCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewLogHistory)).BeginInit();
            this.SuspendLayout();
            // 
            // grdViewLogHistory
            // 
            this.grdViewLogHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdViewLogHistory.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grdViewLogHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdViewLogHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdViewLogHistory.Location = new System.Drawing.Point(-1, 92);
            this.grdViewLogHistory.Name = "grdViewLogHistory";
            this.grdViewLogHistory.Size = new System.Drawing.Size(1082, 593);
            this.grdViewLogHistory.TabIndex = 0;
            this.grdViewLogHistory.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdViewLogHistory_CellFormatting);
            this.grdViewLogHistory.MouseClick += new System.Windows.Forms.MouseEventHandler(this.grdViewLogHistory_MouseClick);
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Location = new System.Drawing.Point(431, 241);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(92, 13);
            this.lblMsg.TabIndex = 1;
            this.lblMsg.Text = "No Record Found";
            this.lblMsg.Visible = false;
            // 
            // txtOrderId
            // 
            this.txtOrderId.Location = new System.Drawing.Point(284, 26);
            this.txtOrderId.Name = "txtOrderId";
            this.txtOrderId.Size = new System.Drawing.Size(303, 20);
            this.txtOrderId.TabIndex = 2;
            this.txtOrderId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOrderId_KeyPress);
            // 
            // lblOrderID
            // 
            this.lblOrderID.AutoSize = true;
            this.lblOrderID.Location = new System.Drawing.Point(181, 29);
            this.lblOrderID.Name = "lblOrderID";
            this.lblOrderID.Size = new System.Drawing.Size(84, 13);
            this.lblOrderID.TabIndex = 3;
            this.lblOrderID.Text = "Filter by Order Id";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(599, 24);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(680, 24);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(761, 24);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblRequired
            // 
            this.lblRequired.AutoSize = true;
            this.lblRequired.ForeColor = System.Drawing.Color.Red;
            this.lblRequired.Location = new System.Drawing.Point(417, 7);
            this.lblRequired.Name = "lblRequired";
            this.lblRequired.Size = new System.Drawing.Size(50, 13);
            this.lblRequired.TabIndex = 8;
            this.lblRequired.Text = "Required";
            this.lblRequired.Visible = false;
            // 
            // btnErrorLogs
            // 
            this.btnErrorLogs.BackColor = System.Drawing.Color.BurlyWood;
            this.btnErrorLogs.ForeColor = System.Drawing.Color.Black;
            this.btnErrorLogs.Location = new System.Drawing.Point(842, 24);
            this.btnErrorLogs.Name = "btnErrorLogs";
            this.btnErrorLogs.Size = new System.Drawing.Size(73, 23);
            this.btnErrorLogs.TabIndex = 9;
            this.btnErrorLogs.Text = "Error Logs";
            this.btnErrorLogs.UseVisualStyleBackColor = false;
            this.btnErrorLogs.Click += new System.EventHandler(this.btnErrorLogs_Click);
            // 
            // dtFrom
            // 
            this.dtFrom.Enabled = false;
            this.dtFrom.Location = new System.Drawing.Point(284, 52);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(129, 20);
            this.dtFrom.TabIndex = 10;
            // 
            // dtTo
            // 
            this.dtTo.Enabled = false;
            this.dtTo.Location = new System.Drawing.Point(458, 52);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(129, 20);
            this.dtTo.TabIndex = 11;
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Location = new System.Drawing.Point(235, 57);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(30, 13);
            this.lblFromDate.TabIndex = 12;
            this.lblFromDate.Text = "From";
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(424, 56);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 13;
            this.lblTo.Text = "To";
            // 
            // btnSearchByDate
            // 
            this.btnSearchByDate.Location = new System.Drawing.Point(599, 50);
            this.btnSearchByDate.Name = "btnSearchByDate";
            this.btnSearchByDate.Size = new System.Drawing.Size(156, 23);
            this.btnSearchByDate.TabIndex = 14;
            this.btnSearchByDate.Text = "Search By Date";
            this.btnSearchByDate.UseVisualStyleBackColor = true;
            this.btnSearchByDate.Click += new System.EventHandler(this.btnSearchByDate_Click);
            // 
            // chkFromDate
            // 
            this.chkFromDate.AutoSize = true;
            this.chkFromDate.Location = new System.Drawing.Point(347, 74);
            this.chkFromDate.Name = "chkFromDate";
            this.chkFromDate.Size = new System.Drawing.Size(15, 14);
            this.chkFromDate.TabIndex = 15;
            this.chkFromDate.UseVisualStyleBackColor = true;
            this.chkFromDate.CheckedChanged += new System.EventHandler(this.chkFromDate_CheckedChanged);
            // 
            // chkToDate
            // 
            this.chkToDate.AutoSize = true;
            this.chkToDate.Location = new System.Drawing.Point(528, 74);
            this.chkToDate.Name = "chkToDate";
            this.chkToDate.Size = new System.Drawing.Size(15, 14);
            this.chkToDate.TabIndex = 16;
            this.chkToDate.UseVisualStyleBackColor = true;
            this.chkToDate.CheckedChanged += new System.EventHandler(this.chkToDate_CheckedChanged);
            // 
            // lblRecordCount
            // 
            this.lblRecordCount.AutoSize = true;
            this.lblRecordCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecordCount.Location = new System.Drawing.Point(957, 25);
            this.lblRecordCount.Name = "lblRecordCount";
            this.lblRecordCount.Size = new System.Drawing.Size(0, 13);
            this.lblRecordCount.TabIndex = 17;
            // 
            // ViewLogHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 697);
            this.Controls.Add(this.lblRecordCount);
            this.Controls.Add(this.chkToDate);
            this.Controls.Add(this.chkFromDate);
            this.Controls.Add(this.btnSearchByDate);
            this.Controls.Add(this.lblTo);
            this.Controls.Add(this.lblFromDate);
            this.Controls.Add(this.dtTo);
            this.Controls.Add(this.dtFrom);
            this.Controls.Add(this.btnErrorLogs);
            this.Controls.Add(this.lblRequired);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblOrderID);
            this.Controls.Add(this.txtOrderId);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.grdViewLogHistory);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewLogHistory";
            this.Text = "View Log History";
            ((System.ComponentModel.ISupportInitialize)(this.grdViewLogHistory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdViewLogHistory;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.TextBox txtOrderId;
        private System.Windows.Forms.Label lblOrderID;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblRequired;
        private System.Windows.Forms.Button btnErrorLogs;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Button btnSearchByDate;
        private System.Windows.Forms.CheckBox chkFromDate;
        private System.Windows.Forms.CheckBox chkToDate;
        private System.Windows.Forms.Label lblRecordCount;
    }
}