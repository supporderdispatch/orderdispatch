﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Core;
using ODM.Core.UserTracking;
using ODM.Data.Entity;
using ODM.Data.ShippingLabel;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class ProcessOrder : Form
    {
        #region "Global Variables"
        public string OrderID;
        public string OrderItemID;
        public string BarCode;
        bool blIsScannedChecked = false;
        public Guid LocationID;
        public bool IsItemsDispatched = false;
        public Guid ShippingID;
        GlobalVariablesAndMethods GlobalVars = new GlobalVariablesAndMethods();
        PDFLib objPdf = new PDFLib();
        DataTable objDtCountry = new DataTable();
        DataTable objDt = new DataTable();
        Order objOrder = new Order();
        Guid Orderid = new Guid();
        string strPrintLabelError = "";
        string strTrackingNumber = "";
        string strServiceName = "";
        string strCourierType = "";
        string strSource = "";
        bool OpenGetDataScreen = true;
        SendEmailEntity objSendEmail = new SendEmailEntity();
        #endregion

        #region "Constructors"
        public ProcessOrder()
        {
            InitializeComponent();
        }
        public ProcessOrder(string strOrderId, string strOrderItemID, string strBarCode, bool IsScannedChecked)
        {
            InitializeComponent();
            OrderID = strOrderId;
            OrderItemID = strOrderItemID;
            BarCode = strBarCode;
            blIsScannedChecked = IsScannedChecked;
            BeginTrackingForOrder(strOrderId);
            this.FormClosed += ProcessOrder_FormClosed;
            GetAndBindInstruction();
        }
        public ProcessOrder(string strOrderId)
        {
            InitializeComponent();
            OrderID = strOrderId;
            OrderItemID = string.Empty;
            BarCode = string.Empty;
            blIsScannedChecked = false;
            BeginTrackingForOrder(strOrderId);
            OpenGetDataScreen = false;
            this.FormClosed += ProcessOrder_FormClosed;
            GetAndBindInstruction();
        }
        public async void GetAndBindInstruction()
        {
            CheckForIllegalCrossThreadCalls = false;
            var result = new ODM.Entities.ProcessOrderInstrunctions.CriteriaResult();
            await Task.Run(() =>
            {
                result = new ODM.Core.ProcessOrderInstructions.ProcessOrderInstructions().GetInstructionsForOrder(Guid.Parse(OrderID));
            }).ContinueWith((r) =>
            {

                richTextInstructions.Text = result.TextResult;
                if (!string.IsNullOrEmpty(result.ImageResult))
                {
                    SetInPicBox(result.ImageResult);
                }
                else
                {
                    picImageInstruction.Height = 1;
                }
                richTextInstructions.Top = picImageInstruction.Top + picImageInstruction.Height;
            });

        }
        private void SetInPicBox(string base64String)
        {
            Image Image = ODM.Core.Helpers.Conversions.Base64StringToImage(base64String);
            int height = Image.Height;
            int width = Image.Width;
            int picboxheight = picImageInstruction.Height;
            int picboxwidth = picImageInstruction.Width;
            float scaleHeight = (float)picboxheight / (float)height;
            float scaleWidth = (float)picboxwidth / (float)width;
            float scale = Math.Min(scaleHeight, scaleWidth);
            Image resizedImage = new Bitmap(Image, (int)(Image.Width * scale), (int)(Image.Height * scale));
            picImageInstruction.Image = resizedImage;
        }
        private void BeginTrackingForOrder(string strOrderId)
        {
            try
            { 
                OrderTracker.BeginTrackerForOrder(strOrderId);
            }
            catch(Exception ex)
            {
                GlobalVariablesAndMethods.OrderIdToLog = Guid.Parse(OrderID);
                GlobalVariablesAndMethods.LogHistory(String.Format("Exception while tracking order {0} on Process screen : {1}", strOrderId, ex.Message));
            }
        }
        private void ProcessOrder_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                OrderTracker.EndTracking();
            }
            catch(Exception ex)
            {
                GlobalVariablesAndMethods.OrderIdToLog = Guid.Parse(OrderID);
                GlobalVariablesAndMethods.LogHistory(String.Format("Exception while tracking order {0} on Process screen : {1}", OrderID, ex.Message));
            }
        }
        #endregion

        #region "Events"
        private void Form1_Load(object sender, EventArgs e)
        {
            populateControls();
            BindGrid();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtTrackingNumber.Text))
            {
                MessageBox.Show("Tracking number required");
            }
            else
            {

                CaptureImage objCaptureImage = new CaptureImage();
                objCaptureImage.ShowDialog();
                if (objCaptureImage.blIsImageCaptured)
                {

                    //showing message box for testing that image has been captured successfully.
                    //    MessageBox.Show("Image captured successfully", "Capture Image", MessageBoxButtons.OK);
                   
                    // needs too be uncommented

                     updatepartshipped();
                }

            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnprintlabel_Click(object sender, EventArgs e)
        {
            UpdateShippingSettingsAndPrint();
            CheckCourierAndPrint();
        }

        private void ProcessOrder_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsItemsDispatched)
            {
                SetScanStatusOfOrderItem();
                GlobalVars.setIsOpenStatus(OrderID, "Y");
            }
            CloseThisWindow();
            e.Cancel = false;
        }

        private void btnEditAddress_Click(object sender, EventArgs e)
        {
            Form frmEditAddress = Application.OpenForms["EditAddress"];
            if (frmEditAddress == null)
            {
                EditAddress objEditAddress = new EditAddress(objDt, objDtCountry, OrderID, OrderItemID, BarCode, blIsScannedChecked);
                objEditAddress.Show();
            }
        }

        private void btnSkip_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PicErrorCustomEvent_MouseHover(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(strPrintLabelError))
            {
                ToolTip ErrorToolTip = new ToolTip();
                ErrorToolTip.SetToolTip(PicError, strPrintLabelError);
            }
        }
        private async void PrintInvoice(bool autoPrint)
        {
            try
            {
                await Task.Run(() =>
                {
                    Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().PrintInvoiceForOrder(Guid.Parse(OrderID),autoPrint);
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void btnprintinvoice_Click(object sender, EventArgs e)
        {
           // objPdf.CreateInvoiceDetails(OrderID, true, txtOrderNum.Text);
            PrintInvoice(false);

        }
        #endregion

        #region "Methods"

        #region "Toogle Windows"
        private void CloseThisWindow()
        {
            Form frmScanItems = Application.OpenForms["GetOrderData"];
            if (frmScanItems != null)
            {
                frmScanItems.Close();
            }
            if (OpenGetDataScreen)
            {
                GetOrderData objOrderData = new GetOrderData(BarCode);
                objOrderData.Show();
            }
        }
        #endregion

        #region "Populate All Contorls except grid"
        private void populateControls()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            //SqlCommand cmd = new SqlCommand();
            try
            {
                Orderid = new Guid(OrderID);

                string strOrderTrackingNumber = "";
                DataTable objDtShippingMethods = new DataTable();
                DataTable objDtPackageCategory = new DataTable();
                DataTable objDtPackageTypes = new DataTable();

                objDt = GlobalVariablesAndMethods.GetOrderDetailsByOrderId(Orderid);

                objDtShippingMethods = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetAllShippingMethods_sp").Tables[0];
                objDtPackageCategory = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetAllPackageCategories_sp").Tables[0];
                objDtCountry = GlobalVars.GetAllCountries();
                if (objDtCountry.Rows.Count > 0)
                {
                    ddlCountry.ValueMember = "CountryId";
                    ddlCountry.DisplayMember = "CountryName";
                    ddlCountry.DataSource = objDtCountry;
                }
                if (objDtShippingMethods.Rows.Count > 0)
                {
                    ddlShippingMethod.ValueMember = "pkPostalServiceId";
                    ddlShippingMethod.DisplayMember = "PostalServiceName";
                    ddlShippingMethod.DataSource = objDtShippingMethods;
                }
                if (objDtPackageCategory.Rows.Count > 0)
                {
                    ddlPackaging.ValueMember = "PackageCategoryID";
                    ddlPackaging.DisplayMember = "PackageCategory";
                    ddlPackaging.DataSource = objDtPackageCategory;
                }
                if (objDt != null & objDt.Rows.Count > 0)
                {
                    #region "Binding Package Type"
                    Guid PackageCategory = new Guid(Convert.ToString(objDt.Rows[0]["PackageCategoryID"]));
                    SqlParameter[] objParms = new SqlParameter[1];
                    objParms[0] = new SqlParameter("PackageCategoryID", SqlDbType.UniqueIdentifier);
                    objParms[0].Value = PackageCategory;
                    objDtPackageTypes = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetPackageTypeByPageCategoryID_SP", objParms).Tables[0];
                    if (objDtPackageTypes.Rows.Count > 0)
                    {
                        ddlPackagingType.ValueMember = "PackageTypeID";
                        ddlPackagingType.DisplayMember = "PackageTitle";
                        ddlPackagingType.DataSource = objDtPackageTypes;
                        ddlPackagingType.SelectedValue = Convert.ToString(objDt.Rows[0]["PackageTypeID"]);
                    }
                    #endregion

                    strSource = Convert.ToString(objDt.Rows[0]["Source"]);

                    lblName.Text = Convert.ToString(objDt.Rows[0]["FullName"]);
                    //Code to populate the fields
                    lblAddress.Text = Convert.ToString(objDt.Rows[0]["CompanyAddress"]);
                    lblTown.Text = Convert.ToString(objDt.Rows[0]["Town"]);
                    lblPostCode.Text = Convert.ToString(objDt.Rows[0]["PostCode"]);
                    lblCountryInfo.Text = Convert.ToString(objDt.Rows[0]["CountryName"]);
                    lblcompany.Text = Convert.ToString(objDt.Rows[0]["Company"]);
                    txtOrderId.Text = Convert.ToString(Orderid);
                    txtreference.Text = Convert.ToString(objDt.Rows[0]["ReferenceNum"]);
                    txtsource.Text = strSource;
                    txttaxrate.Text = Convert.ToString(objDt.Rows[0]["CountryTaxRate"]);
                    txtsubtotal.Text = Convert.ToString(objDt.Rows[0]["Subtotal"]);
                    txttax.Text = Convert.ToString(objDt.Rows[0]["Tax"]);
                    txtPostageCost.Text = Convert.ToString(objDt.Rows[0]["PostageCost"]);
                    txttotal.Text = Convert.ToString(Math.Round(Convert.ToDouble(objDt.Rows[0]["TotalCharge"]), 2));
                    txtEmail.Text = Convert.ToString(objDt.Rows[0]["EmailAddress"]);
                    txtItemWeight.Text = Convert.ToString(objDt.Rows[0]["ItemWeight"]);
                    txtTotalWeight.Text = Convert.ToString(objDt.Rows[0]["TotalWeight"]);
                    richTxtOrderNotes.Text = Convert.ToString(objDt.Rows[0]["Notes"]);
                    LocationID = new Guid(Convert.ToString(objDt.Rows[0]["Location"]));
                    txtOrderNum.Text = Convert.ToString(objDt.Rows[0]["NumOrderId"]);
                    ddlCountry.SelectedValue = Convert.ToString(objDt.Rows[0]["CountryId"]);
                    ddlShippingMethod.SelectedValue = Convert.ToString(objDt.Rows[0]["pkPostalServiceId"]);
                    ddlPackaging.SelectedValue = Convert.ToString(objDt.Rows[0]["PackageCategoryID"]);
                    strCourierType = Convert.ToString(objDt.Rows[0]["vendor"]);
                    strOrderTrackingNumber = Convert.ToString(objDt.Rows[0]["OrderTrackingNumber"]);

                    //Mapping for email functionality 
                    objSendEmail.OrderID = OrderID;
                    objSendEmail.NumberOrderID = txtOrderNum.Text;
                    objSendEmail.Date = Convert.ToDateTime(Convert.ToString(objDt.Rows[0]["DespatchByDate"]));
                    objSendEmail.FullName = lblName.Text;
                    objSendEmail.EmailID = txtEmail.Text;
                    objSendEmail.Mode = "INSERT";
                    //End of Mapping for email functionality

                    strServiceName = Convert.ToString(objDt.Rows[0]["PostalServiceName"]);
                    ShippingID = new Guid(Convert.ToString(objDt.Rows[0]["ShippingId"]));

                    //End of Code to populate the fields

                    PopulateOrderAndOrderItemClassForAPIs();

                    if (string.IsNullOrWhiteSpace(strOrderTrackingNumber))
                    {
                        PrintInvoice(true);
                       // objPdf.CreateInvoiceDetails(OrderID, false, txtOrderNum.Text);
                        CheckCourierAndPrint();
                    }
                    else
                    {
                        txtTrackingNumber.Text = strOrderTrackingNumber;
                        GlobalVariablesAndMethods.LogHistory("Label already printed&&Tracking number already retrieved '" + strOrderTrackingNumber + "'");
                    }
                }
            }
            catch (Exception Ex)
            {
                string strError = Ex.Message;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// To populate order/order item class so that we could use those to pass values(order details) to Courier APIs
        /// MYHERMES,HERMES and SPRING are definitely using this and other Courier API may or may not use that.
        /// </summary>
        private void PopulateOrderAndOrderItemClassForAPIs()
        {
            //Code to map the Order Object for the API Integration.
            List<OrderItem> objOrderItems = new List<OrderItem>();
            OrderItem objOrderItem = new OrderItem();
            objOrder.Orderid = Orderid;
            objOrder.NumOrderId = Convert.ToInt32(objDt.Rows[0]["NumOrderId"]);
            objOrder.PostCode = Convert.ToString(objDt.Rows[0]["PostCode"]);
            objOrder.Address1 = Convert.ToString(objDt.Rows[0]["CompanyAddress"]);
            objOrder.Address2 = Convert.ToString(objDt.Rows[0]["Address2"]);
            objOrder.Address3 = Convert.ToString(objDt.Rows[0]["Address3"]);
            objOrder.Town = Convert.ToString(objDt.Rows[0]["Town"]);
            objOrder.FullName = Convert.ToString(objDt.Rows[0]["CustomerFullName"]);
            objOrder.EmailAddress = Convert.ToString(objDt.Rows[0]["EmailAddress"]);
            objOrder.BuyerPhoneNumber = Convert.ToString(objDt.Rows[0]["PhoneNumber"]);
            objOrder.Pack_TotalWeight = Convert.ToDouble(objDt.Rows[0]["TotalWeight"]);
            objOrder.ShippingMethod = Convert.ToString(objDt.Rows[0]["PostalServiceName"]);
            objOrder.PostalTrackingNumber = Convert.ToString(objDt.Rows[0]["TrackingNumber"]);
            objOrder.CountryCode = Convert.ToString(objDt.Rows[0]["CountryCode"]);
            objOrder.Country = Convert.ToString(objDt.Rows[0]["CountryName"]);
            objOrder.ExternalReferenceNum = Convert.ToString(objDt.Rows[0]["ExternalReferenceNum"]);
            objOrder.Company = Convert.ToString(objDt.Rows[0]["Company"]);
            objOrder.Region = Convert.ToString(objDt.Rows[0]["Region"]);
            objOrder.ReferenceNum = Convert.ToString(objDt.Rows[0]["ReferenceNum"]);
            objOrderItem.Qty = Convert.ToInt32(objDt.Rows[0]["Quantity"]);
            objOrderItem.Title = Convert.ToString(objDt.Rows[0]["Title"]);
            objOrderItems.Add(objOrderItem);
            objOrder.OrderItems = objOrderItems;
            //End of code to integrate API.
        }

        private void CheckCourierAndPrint()
        {
            Thread PrintLabelThread;
            switch (strCourierType.Trim().ToUpper())
            {
                case "MYHERMES":
                    {
                        PrintLabelThread = new Thread(MyHermesPrintLabelProcess);
                        PrintLabelThread.Start();
                    }
                    break;
                case "HERMES":
                    {
                        PrintLabelThread = new Thread(HermesPrintLabelProcess);
                        PrintLabelThread.Start();
                    }
                    break;
                case "SPRING":
                    {
                        PrintLabelThread = new Thread(SpringPrintLabelProcess_New);
                        PrintLabelThread.Start();
                    }
                    break;
                case "DPD UK":
                case "DPD":
                    {
                        PrintLabelThread = new Thread(DPDPrintLabelProcess);
                        PrintLabelThread.Start();
                    }
                    break;
                case "ROYAL MAIL":
                case "OBA":
                    {
                        PrintLabelThread = new Thread(RoyalMailPrintLabelProcess);
                        PrintLabelThread.Start();
                    }
                    break;
                case "FEDEX":
                    {
                        PrintLabelThread = new Thread(FedExPrintLabelProcess);
                        PrintLabelThread.Start();
                    }
                    break;
                case "AMAZON":
                    {
                        PrintLabelThread = new Thread(AmazonPrintLabelProcess);
                        PrintLabelThread.Start();
                    }
                    break;
            }
        }
        #endregion

        #region "Get Item Detail"
        public void BindGrid()
        {
            try
            {
                Guid orderID = new Guid(OrderID);
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();

                SqlParameter[] objorderParams = new SqlParameter[2];
                objorderParams[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objorderParams[0].Value = orderID;

                objorderParams[1] = new SqlParameter("@OrderItemId", SqlDbType.NVarChar, 1000);
                objorderParams[1].Value = OrderItemID;

                DataTable objDt = new DataTable();
                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetItemDetail", objorderParams).Tables[0];
                gridItemDetail.DataSource = objDt;
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }
        #endregion

        #region "Update Selected order items after dispatched"
        public void updatepartshipped()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            SqlConnection connection = new SqlConnection();
            try
            {
                Guid orderID = new Guid(OrderID);
                bool blIsOrderProcessedByAPI = false;
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                try
                {
                    GlobalVariablesAndMethods.LogHistory("API calls for processing order begin");
                    blIsOrderProcessedByAPI = ProcessOrderAndUpdateAPI.UpdateOrderAPI(orderID, true, LocationID, txtTrackingNumber.Text);
                    if (blIsOrderProcessedByAPI)
                        GlobalVariablesAndMethods.LogHistory("API calls for processing order completed successfully!");
                    else
                        GlobalVariablesAndMethods.LogHistory("API calls for processing order completed with error!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error occurred in Process API call \nError:-" + ex.Message + "\nPlease keep this order to one side for verification", "API call Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //API call Test:-  Update database if order is set processed on the linnwork API(blIsOrderProcessedByAPI ==TRUE)
                if (blIsOrderProcessedByAPI)
                {
                    //UpdateTrackingNumberForManifest();
                    IsItemsDispatched = true;

                    SqlParameter[] objorderParams = new SqlParameter[3];

                    objorderParams[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                    objorderParams[0].Value = orderID;

                    objorderParams[1] = new SqlParameter("@OrderItemId", SqlDbType.NVarChar, 1000);
                    objorderParams[1].Value = OrderItemID;

                    objorderParams[2] = new SqlParameter("@IsAllItemShipped", SqlDbType.NVarChar, 1);
                    objorderParams[2].Direction = ParameterDirection.Output;

                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdatePartShipped", objorderParams);

                    string IsAllItemShipped = Convert.ToString(objorderParams[2].Value);
                    if (IsAllItemShipped == "Y")
                    {
                        GlobalVariablesAndMethods.LogHistory("Process status 'processed' updated in Database");
                       // string strResult = GlobalVariablesAndMethods.InsertUpdateDeleteDataForPendingEmail(objSendEmail);
                     //   GlobalVariablesAndMethods.LogHistory("Moving to Log database");
                     //   GlobalVariablesAndMethods.MoveOrderToLogDatabase(orderID);
                        ODM.Core.UserTracking.OrderTracker.MarkOrderProcessed(orderID.ToString());
                        ODM.Core.Emails.CreatePendingEmail.CreatePendingEmailForOrder(orderID);
                    }
                    else
                    {
                        GlobalVariablesAndMethods.LogHistory("Something went wrong while updating process status in Database");
                    }

                    Form frmScanItems = Application.OpenForms["GetOrderData"];
                    if (frmScanItems != null)
                    {
                        frmScanItems.Close();
                    }
                    GetOrderData objOrderData = new GetOrderData(BarCode);
                    objOrderData.Show();
                    this.Close();
                }
                //End of API call Test
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    MessageBox.Show("Error occurred please try again after sometime", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred while updating process status of order\nError:-" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region "Print Label Process"
        private void HermesPrintLabelProcess()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:

            try
            {
                HermesPrintLabel objPrintLabel = new HermesPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                PicRight.Visible = false;
                PicError.Visible = false;
                strTrackingNumber = objPrintLabel.ExecuteMyHermesShippingAPI(objOrder, Orderid);
                if (strTrackingNumber != "")
                {
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    txtTrackingNumber.Text = strTrackingNumber;
                    GlobalVariablesAndMethods.LogHistory("Got Tracking number in process order screen:- " + strTrackingNumber);
                    UpdateTrackingNumberForManifest();
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void MyHermesPrintLabelProcess()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            try
            {
                MyHermesPrintLabel objPrintLabel = new MyHermesPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                PicRight.Visible = false;
                PicError.Visible = false;
                strTrackingNumber = objPrintLabel.ExecuteMyHermesShippingAPI(objOrder, Orderid);
                if (strTrackingNumber != "")
                {
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    txtTrackingNumber.Text = strTrackingNumber;
                    GlobalVariablesAndMethods.LogHistory("Got Tracking number in process order screen:- " + strTrackingNumber);
                    UpdateTrackingNumberForManifest();
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void SpringPrintLabelProcess_New()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            try
            {
                SpringPrintLabel_New objPrintLabel = new SpringPrintLabel_New();
                CheckForIllegalCrossThreadCalls = false;
                PicRight.Visible = false;
                PicError.Visible = false;
                strTrackingNumber = objPrintLabel.ExecuteSpringShippingAPI_New(objOrder, Orderid, objDt);
                if (strTrackingNumber != "")
                {
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    txtTrackingNumber.Text = strTrackingNumber;
                    GlobalVariablesAndMethods.LogHistory("Got Tracking number in process order screen:- " + strTrackingNumber);
                    UpdateTrackingNumberForManifest();
                }
                else if (strTrackingNumber.ToUpper() != "ERROR")
                {
                    PicLoading.Visible = false;
                    PicRight.Visible = false;
                    PicError.Visible = true;
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error in the API");
                    MessageBox.Show("Can't process because of error in the API", "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void SpringPrintLabelProcess()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            try
            {
                SpringPrintLabel objPrintLabel = new SpringPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                PicRight.Visible = false;
                PicError.Visible = false;
                strTrackingNumber = objPrintLabel.ExecuteSpringShippingAPI(objOrder, Orderid);
                if (strTrackingNumber != "")
                {
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    txtTrackingNumber.Text = strTrackingNumber;
                    GlobalVariablesAndMethods.LogHistory("Got Tracking number in process order screen:- " + strTrackingNumber);
                    UpdateTrackingNumberForManifest();
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void DPDPrintLabelProcess()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            try
            {
                DPDPrintLabel objPrintLabel = new DPDPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                PicRight.Visible = false;
                PicError.Visible = false;
                strTrackingNumber = objPrintLabel.ExecuteDPDShippingAPI(objOrder, Orderid);
                if (strTrackingNumber != "")
                {
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    txtTrackingNumber.Text = strTrackingNumber;
                    GlobalVariablesAndMethods.LogHistory("Got Tracking number in process order screen:- " + strTrackingNumber);
                    UpdateTrackingNumberForManifest();
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RoyalMailPrintLabelProcess()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            try
            {
                RoyalMailPrintLabel objPrintLabel = new RoyalMailPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                PicRight.Visible = false;
                PicError.Visible = false;
                strTrackingNumber = objPrintLabel.ExecuteRoyalMailShippingAPI(objOrder, Orderid);
                if (strTrackingNumber != "")
                {
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    txtTrackingNumber.Text = strTrackingNumber;
                    GlobalVariablesAndMethods.LogHistory("Got Tracking number in process order screen:- " + strTrackingNumber);
                    UpdateTrackingNumberForManifest();
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void FedExPrintLabelProcess()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            try
            {
                FedExPrintLabel objPrintLabel = new FedExPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                PicRight.Visible = false;
                PicError.Visible = false;
                strTrackingNumber = objPrintLabel.ExecuteFedExShippingAPI(objOrder, Orderid);
                if (strTrackingNumber != "")
                {
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    txtTrackingNumber.Text = strTrackingNumber;
                    GlobalVariablesAndMethods.LogHistory("Got Tracking number in process order screen:- " + strTrackingNumber);
                    UpdateTrackingNumberForManifest();
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void AmazonPrintLabelProcess()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            try
            {
                AmazonPrintLabel objPrintLabel = new AmazonPrintLabel();
                CheckForIllegalCrossThreadCalls = false;
                PicRight.Visible = false;
                PicError.Visible = false;
                Guid ShipmentID = new Guid();
                strTrackingNumber = objPrintLabel.ExecuteAmazonShippingAPI(objDt, Orderid, ref ShipmentID);
                if (strTrackingNumber != "")
                {
                    PicLoading.Visible = false;
                    PicRight.Visible = true;
                    txtTrackingNumber.Text = strTrackingNumber;
                    GlobalVariablesAndMethods.LogHistory("Got Tracking number in process order screen:- " + strTrackingNumber);
                    UpdateTrackingNumberForManifest();
                    GlobalVariablesAndMethods.GetAddDeleteShipmentDetails(ShipmentID, "ADD", Orderid);
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = ex.Message;
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                PicLoading.Visible = false;
                PicRight.Visible = false;
                PicError.Visible = true;
                strPrintLabelError = ex.Message;
                PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + ex.Message);
                MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void UpdateShippingSettingsAndPrint()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                Guid orderID = new Guid(OrderID);
                string strSelectedPostalID = ddlShippingMethod.SelectedValue.ToString();
                Guid PostalServiceID = new Guid(strSelectedPostalID);
                SqlParameter[] objorderParams = new SqlParameter[7];

                objorderParams[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objorderParams[0].Value = orderID;

                objorderParams[1] = new SqlParameter("@ShippingID", SqlDbType.UniqueIdentifier);
                objorderParams[1].Value = ShippingID;

                objorderParams[2] = new SqlParameter("@PostalServiceID", SqlDbType.UniqueIdentifier);
                objorderParams[2].Value = PostalServiceID;

                objorderParams[3] = new SqlParameter("@PostalServiceName", SqlDbType.VarChar, 30);
                objorderParams[3].Value = ddlShippingMethod.Text;

                objorderParams[4] = new SqlParameter("@TotalWeight", SqlDbType.Decimal);
                objorderParams[4].Value = Convert.ToDecimal(txtTotalWeight.Text);

                objorderParams[5] = new SqlParameter("@CourierVendor", SqlDbType.VarChar, 50);
                objorderParams[5].Direction = ParameterDirection.Output;

                objorderParams[6] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objorderParams[6].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateProcessScreenSettings", objorderParams);
                if (Convert.ToString(objorderParams[6].Value) == "1")
                {
                    //Setting back courier type after updating shipping method and total weight.
                    strCourierType = Convert.ToString(objorderParams[5].Value);

                    /*This is to get the order details again from db after updating shipping method and total weight
                     *So that we could pass updated values for respected courier integration 
                     */
                    objDt = GlobalVariablesAndMethods.GetOrderDetailsByOrderId(Orderid);
                    PopulateOrderAndOrderItemClassForAPIs();
                }
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = "Error occurred while updating shipping status";
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        private void SetScanStatusOfOrderItem()
        {
            Guid orderID = new Guid(OrderID);

            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:

            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objParams = new SqlParameter[4];
                objParams[0] = new SqlParameter("@BarCodeNumber", SqlDbType.VarChar, 5000);
                objParams[0].Value = "";

                objParams[1] = new SqlParameter("@OrderID", SqlDbType.UniqueIdentifier);
                objParams[1].Value = orderID;

                objParams[2] = new SqlParameter("@IsReset", SqlDbType.VarChar, 2);
                objParams[2].Value = "Y";

                objParams[3] = new SqlParameter("@IsAllItemScanned", SqlDbType.VarChar, 50);
                objParams[3].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateIsScannedStatusOfOrderItem", objParams);
                string Result = Convert.ToString(objParams[3].Value);
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = "Error occurred while updating shipping status";
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                string Excp = ex.Message;
            }
        }
        private void UpdateTrackingNumberForManifest()
        {
            int intErrorCount = 0;
        ExecuteAgainIfDeadLock:
            GlobalVariablesAndMethods.LogHistory("Updating tracking number in database");
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objManifest = new SqlParameter[5];

                objManifest[0] = new SqlParameter("@OrderID", SqlDbType.UniqueIdentifier);
                objManifest[0].Value = Orderid;

                objManifest[1] = new SqlParameter("@TrackingNumber", SqlDbType.VarChar, 200);
                objManifest[1].Value = txtTrackingNumber.Text;

                objManifest[2] = new SqlParameter("@ServiceName", SqlDbType.VarChar, 200);
                objManifest[2].Value = strServiceName;

                objManifest[3] = new SqlParameter("@Manifested", SqlDbType.Bit);
                objManifest[3].Value = false;

                objManifest[4] = new SqlParameter("@Result", SqlDbType.Int);
                objManifest[4].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateTrackingNumberForManifest", objManifest);
                int intResult = Convert.ToInt32(objManifest[4].Value);
                if (intResult == 1)
                {
                    GlobalVariablesAndMethods.LogHistory(string.Format("Tracking number {0} saved in database", txtTrackingNumber.Text));
                }
                CN22Form.PrintForOrder(Orderid);
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strPrintLabelError = "Error occurred while updating tracking number in database";
                    PicError.MouseHover += new EventHandler(PicErrorCustomEvent_MouseHover);
                    MessageBox.Show(ex.Message, "Courier Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion
    }
}
