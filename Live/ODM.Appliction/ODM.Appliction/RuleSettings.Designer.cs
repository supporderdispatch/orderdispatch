﻿namespace ODM.Appliction
{
    partial class RuleSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RuleSettings));
            this.txtRuleName = new System.Windows.Forms.TextBox();
            this.lblRuleName = new System.Windows.Forms.Label();
            this.chkIsActive = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblConditionText = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblAssignToFolder = new System.Windows.Forms.Label();
            this.ddlFolders = new System.Windows.Forms.ComboBox();
            this.txtRuleCondition = new System.Windows.Forms.TextBox();
            this.txtPosition = new System.Windows.Forms.TextBox();
            this.lblPosition = new System.Windows.Forms.Label();
            this.lblCondition = new System.Windows.Forms.Label();
            this.ddlRuleConditions = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtRuleName
            // 
            this.txtRuleName.Location = new System.Drawing.Point(180, 42);
            this.txtRuleName.Name = "txtRuleName";
            this.txtRuleName.Size = new System.Drawing.Size(350, 20);
            this.txtRuleName.TabIndex = 14;
            // 
            // lblRuleName
            // 
            this.lblRuleName.AutoSize = true;
            this.lblRuleName.Location = new System.Drawing.Point(129, 43);
            this.lblRuleName.Name = "lblRuleName";
            this.lblRuleName.Size = new System.Drawing.Size(41, 13);
            this.lblRuleName.TabIndex = 13;
            this.lblRuleName.Text = "Name :";
            // 
            // chkIsActive
            // 
            this.chkIsActive.AutoSize = true;
            this.chkIsActive.Location = new System.Drawing.Point(183, 290);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(64, 17);
            this.chkIsActive.TabIndex = 12;
            this.chkIsActive.Text = "IsActive";
            this.chkIsActive.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(181, 334);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(74, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblConditionText
            // 
            this.lblConditionText.AutoSize = true;
            this.lblConditionText.Location = new System.Drawing.Point(112, 84);
            this.lblConditionText.Name = "lblConditionText";
            this.lblConditionText.Size = new System.Drawing.Size(57, 13);
            this.lblConditionText.TabIndex = 9;
            this.lblConditionText.Text = "Condition :";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(318, 334);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblAssignToFolder
            // 
            this.lblAssignToFolder.AutoSize = true;
            this.lblAssignToFolder.Location = new System.Drawing.Point(78, 203);
            this.lblAssignToFolder.Name = "lblAssignToFolder";
            this.lblAssignToFolder.Size = new System.Drawing.Size(92, 13);
            this.lblAssignToFolder.TabIndex = 16;
            this.lblAssignToFolder.Text = "Assign To Folder :";
            // 
            // ddlFolders
            // 
            this.ddlFolders.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlFolders.FormattingEnabled = true;
            this.ddlFolders.Location = new System.Drawing.Point(182, 200);
            this.ddlFolders.Name = "ddlFolders";
            this.ddlFolders.Size = new System.Drawing.Size(348, 21);
            this.ddlFolders.TabIndex = 17;
            // 
            // txtRuleCondition
            // 
            this.txtRuleCondition.Location = new System.Drawing.Point(180, 84);
            this.txtRuleCondition.Multiline = true;
            this.txtRuleCondition.Name = "txtRuleCondition";
            this.txtRuleCondition.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRuleCondition.Size = new System.Drawing.Size(351, 57);
            this.txtRuleCondition.TabIndex = 18;
            // 
            // txtPosition
            // 
            this.txtPosition.Location = new System.Drawing.Point(183, 245);
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.Size = new System.Drawing.Size(347, 20);
            this.txtPosition.TabIndex = 20;
            // 
            // lblPosition
            // 
            this.lblPosition.AutoSize = true;
            this.lblPosition.Location = new System.Drawing.Point(120, 248);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(50, 13);
            this.lblPosition.TabIndex = 19;
            this.lblPosition.Text = "Position :";
            // 
            // lblCondition
            // 
            this.lblCondition.AutoSize = true;
            this.lblCondition.Location = new System.Drawing.Point(88, 161);
            this.lblCondition.Name = "lblCondition";
            this.lblCondition.Size = new System.Drawing.Size(81, 13);
            this.lblCondition.TabIndex = 21;
            this.lblCondition.Text = "Condition Text :";
            // 
            // ddlRuleConditions
            // 
            this.ddlRuleConditions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlRuleConditions.FormattingEnabled = true;
            this.ddlRuleConditions.Location = new System.Drawing.Point(182, 161);
            this.ddlRuleConditions.Name = "ddlRuleConditions";
            this.ddlRuleConditions.Size = new System.Drawing.Size(350, 21);
            this.ddlRuleConditions.TabIndex = 22;
            // 
            // RuleSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 363);
            this.Controls.Add(this.ddlRuleConditions);
            this.Controls.Add(this.lblCondition);
            this.Controls.Add(this.txtPosition);
            this.Controls.Add(this.lblPosition);
            this.Controls.Add(this.txtRuleCondition);
            this.Controls.Add(this.ddlFolders);
            this.Controls.Add(this.lblAssignToFolder);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtRuleName);
            this.Controls.Add(this.lblRuleName);
            this.Controls.Add(this.chkIsActive);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblConditionText);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RuleSettings";
            this.Text = "RuleSettings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRuleName;
        private System.Windows.Forms.Label lblRuleName;
        private System.Windows.Forms.CheckBox chkIsActive;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblConditionText;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblAssignToFolder;
        private System.Windows.Forms.ComboBox ddlFolders;
        private System.Windows.Forms.TextBox txtRuleCondition;
        private System.Windows.Forms.TextBox txtPosition;
        private System.Windows.Forms.Label lblPosition;
        private System.Windows.Forms.Label lblCondition;
        private System.Windows.Forms.ComboBox ddlRuleConditions;
    }
}