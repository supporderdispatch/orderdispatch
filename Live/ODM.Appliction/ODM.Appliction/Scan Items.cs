﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Core.UserTracking;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;

namespace ODM.Appliction
{
    public partial class Scan_Items : Form
    {
        #region "Global Variables"
        public string BarCode;
        public string StrOrderId;
        string strOrderItemIds = "";
        bool blIsProcessScreenCall = false;
        bool blIsScanningChecked = false;
        bool blIsAllItemScanned = false;
        DataTable objCustomDT = new DataTable();
        GlobalVariablesAndMethods GlobalVars = new GlobalVariablesAndMethods();
        bool blIsAnyItemHaveMoreThanOneQuantity = false;
        bool blIsFirstCall = true;
        Guid RowID = new Guid();
        int intErrorCount = 0;
        int totalItems = 0;
        int scannedItems = 0;

        #endregion

        #region "Constructors"
        public Scan_Items()
        {
            InitializeComponent();
            btndispatch.Enabled = true;
            btnScan.Enabled = true;
            txtBarCodeSearch.Enabled = true;
        }
        public Scan_Items(string barcode, string strOrderID, bool blIsCallFromProcessScreen, bool IsScanningChecked = false)
        {
            InitializeComponent();
            BarCode = barcode;
            StrOrderId = strOrderID;
            blIsProcessScreenCall = blIsCallFromProcessScreen;
            blIsScanningChecked = IsScanningChecked;
            if (blIsCallFromProcessScreen)
            {
                if (blIsScanningChecked)
                {
                    if (!BindGridForScanningChecked())
                    {
                        lblmessage.Text = "No record found under selected bar code, Click on back button and try with another order id ";
                    }
                }
                else
                {
                    if (!BindGrid())
                    {
                        lblmessage.Text = "No record found under selected bar code and order id click on back button and try with another order id ";
                    }
                }
            }
            this.FormClosed += Scan_Items_FormClosed;
            ApplyGridViewSettings();
            lblScannedItemStatus.Paint += lblScannedItemStatus_Paint;
        }

        void lblScannedItemStatus_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            var width = Convert.ToInt32(e.ClipRectangle.Width * (Convert.ToDouble(scannedItems) / Convert.ToDouble((totalItems)) * 100) / 100);
            g.FillRectangle(Brushes.WhiteSmoke, e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width, e.ClipRectangle.Height);
            g.FillRectangle(Brushes.DarkSeaGreen, e.ClipRectangle.X, e.ClipRectangle.Y, width, e.ClipRectangle.Height);
            g.DrawString((sender as Label).Text, lblScannedItemStatus.Font, Brushes.Black, new PointF(e.ClipRectangle.X, e.ClipRectangle.Y));

        }

        private void ApplyGridViewSettings()
        {
            grdScan.DoubleBuffered(true);
            grdScan.CellBorderStyle = DataGridViewCellBorderStyle.Single;       
            grdScan.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdScan.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;          
            grdScan.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdScan.EnableHeadersVisualStyles = false;
            grdScan.DefaultCellStyle.SelectionForeColor = Color.Black;     
            grdScan.ScrollBars = ScrollBars.Both;
        }
                


        void Scan_Items_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!string.IsNullOrEmpty(StrOrderId))
            {
                OrderTracker.EndTracking();
            }
        }
        #endregion

        #region "Methods"
        private void CreateCustomDataSource(DataTable objDataTbl)
        {
            blIsFirstCall = false;
            try
            {
                objCustomDT = objDataTbl;
                GlobalVars.OpenedOrderIds = "";
                int intTotalRowCount = objDataTbl.Rows.Count;
                for (int i = 0; i < intTotalRowCount; i++)
                {
                    StrOrderId = Convert.ToString(objDataTbl.Rows[i]["OrderId"]);
                    GlobalVariablesAndMethods.OrderIdToLog = new Guid(StrOrderId);
                    GlobalVars.OpenedOrderIds = StrOrderId;
                    strOrderItemIds = strOrderItemIds == "" ? Convert.ToString(objDataTbl.Rows[i]["ItemId"]) : strOrderItemIds + "," + Convert.ToString(objDataTbl.Rows[i]["ItemId"]);
                    int intQuantity = Convert.ToInt32(objDataTbl.Rows[i]["Quantity"]);
                    if (intQuantity > 1)
                    {
                        blIsAnyItemHaveMoreThanOneQuantity = true;
                        for (int j = 1; j < intQuantity; j++)
                        {
                            objCustomDT.ImportRow(objDataTbl.Rows[i]);
                        }
                    }
                }
                if (!String.IsNullOrEmpty(StrOrderId))
                {
                    OrderTracker.BeginTrackerForOrder(StrOrderId);
                }
            }
            catch
            {
                MessageBox.Show("Some Error Occurred during binding of list of order items");
            }
            if (!blIsAnyItemHaveMoreThanOneQuantity)
            {
                objCustomDT = objDataTbl;
            }
            else
            {
                //Sort the order items by barcode so that we could get all the items after each other with same barcode.
                DataView dv = new DataView(objDataTbl);
                dv.Sort = "BarcodeNumber, RowId asc";
                objCustomDT = dv.ToTable();
            }
            if (GlobalVars.OpenedOrderIds != null && GlobalVars.OpenedOrderIds != "")
            {
                GlobalVars.setIsOpenStatus(GlobalVars.OpenedOrderIds, "N");
            }
        }

        public bool BindGridForScanningChecked()
        {
            bool isHaveData = true;
            try
            {
                #region "Scan Screen"
                DataTable objDt = new DataTable();
                if (blIsFirstCall)
                {
                    objDt = GetDataForBindGrid();
                    if (objDt.Rows.Count > 0)
                    {
                        CreateCustomDataSource(objDt);
                    }
                }
                if (objCustomDT.Rows.Count > 0)
                {
                    grdScan.DataSource = objCustomDT;
                    grdScan.AllowUserToAddRows = false;
                    try
                    {
                        HideColumnsOfGrid();
                        grdScan.Columns["SKU"].Width = 120;
                        grdScan.Columns["Title"].Width = 353;
                        grdScan.Columns["SKU"].ReadOnly = true;
                        grdScan.Columns["Title"].ReadOnly = true;
                        int TotalNumberOfItems = objCustomDT.Rows.Count;
                        int intNumberOfScannedItems = 0;
                        foreach (DataGridViewRow row in this.grdScan.Rows)
                        {
                            bool IsScanned = Convert.ToBoolean(row.Cells["IsScanned"].Value.ToString());
                            if (IsScanned)
                            {
                                intNumberOfScannedItems++;
                                row.DefaultCellStyle.BackColor = Color.FromName("DarkSeaGreen");
                                row.Cells["SKU"].ToolTipText = "All Good";
                            }
                            else
                            {
                                string strBarCodeNumber = Convert.ToString(row.Cells["BarcodeNumber"].Value);
                                row.Cells["SKU"].ToolTipText = string.Format("Requires barcode {0} scan before order can be processed", strBarCodeNumber);
                            }
                            if (TotalNumberOfItems == intNumberOfScannedItems)
                            {
                                blIsAllItemScanned = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }
                else
                {
                    grdScan.Visible = false;
                    btndispatch.Visible = false;
                    isHaveData = false;
                }
                DisplayOrderStatus();
                #endregion
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
            return isHaveData;
        }

        private void HideColumnsOfGrid()
        {
            for (int i = 2; i < objCustomDT.Columns.Count; i++)
            {
                grdScan.Columns[i].Visible = false;
            }
        }

        private DataTable GetDataForBindGrid()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            SqlCommand cmd = new SqlCommand();
            connection.Open();
            DataTable objDt = new DataTable();
            try
            {
                if (blIsScanningChecked)
                {
                    SqlParameter[] objorderParams = new SqlParameter[2];

                    objorderParams[0] = new SqlParameter("@BarcodeNumber", SqlDbType.NVarChar, 200);
                    objorderParams[0].Value = BarCode;

                    objorderParams[1] = new SqlParameter("@DisptachType", SqlDbType.NVarChar, 200);
                    objorderParams[1].Value = GlobalVariablesAndMethods.DispatchType;

                    objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "AfterScnning", objorderParams).Tables[0];
                }
                else
                {
                    Guid OrderId = new Guid(StrOrderId);
                    SqlParameter[] objorderParams = new SqlParameter[2];
                    objorderParams[0] = new SqlParameter("@Barcode", SqlDbType.NVarChar, 200);
                    objorderParams[0].Value = BarCode;
                    objorderParams[1] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                    objorderParams[1].Value = OrderId;
                    objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "ScanOrder", objorderParams).Tables[0];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }

        public bool BindGrid()
        {
            bool IsHaveData = true;
            try
            {
                #region "Scan Screen"
                DataTable objDt = new DataTable();
                if (blIsFirstCall)
                {
                    objDt = GetDataForBindGrid();
                    if (objDt.Rows.Count > 0)
                    {
                        CreateCustomDataSource(objDt);
                    }
                }
                if (!blIsAnyItemHaveMoreThanOneQuantity)
                {
                    if (objDt.Rows.Count == 0)
                    {
                        objDt = GetDataForBindGrid();
                        objCustomDT = objDt;
                    }
                }
                if (objCustomDT.Rows.Count > 0)
                {
                    grdScan.DataSource = objCustomDT;
                    grdScan.AllowUserToAddRows = false;
                    try
                    {
                        HideColumnsOfGrid();
                        grdScan.Columns["SKU"].Width = 120;
                        grdScan.Columns["Title"].Width = 353;
                        grdScan.Columns["SKU"].ReadOnly = true;
                        grdScan.Columns["Title"].ReadOnly = true;
                        int TotalNumberOfItems = objCustomDT.Rows.Count;
                        int intNumberOfScannedItems = 0;
                        foreach (DataGridViewRow row in this.grdScan.Rows)
                        {
                            bool IsScanned = Convert.ToBoolean(row.Cells["IsScanned"].Value.ToString());
                            if (IsScanned)
                            {
                                intNumberOfScannedItems++;
                                row.DefaultCellStyle.BackColor = Color.FromName("DarkSeaGreen");
                                row.Cells["SKU"].ToolTipText = "All Good";
                            }
                            else
                            {
                                string strBarCodeNumber = Convert.ToString(row.Cells["BarcodeNumber"].Value);
                                row.Cells["SKU"].ToolTipText = string.Format("Requires barcode {0} scan before order can be processed", strBarCodeNumber);
                            }
                            if (TotalNumberOfItems == intNumberOfScannedItems)
                            {
                                blIsAllItemScanned = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }
                else
                {
                    grdScan.Visible = false;
                    btndispatch.Visible = false;
                    IsHaveData = false;
                }
                DisplayOrderStatus();
                #endregion
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
            return IsHaveData;
        }

        public void dispatchorder()
        {
            if (!String.IsNullOrEmpty(StrOrderId))
            {
                OrderTracker.BeginTrackerForOrder(StrOrderId);
            }

            Guid orderID = new Guid(StrOrderId);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                #region "Validate"
                SqlParameter[] objParams = new SqlParameter[3];
                objParams[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParams[0].Value = orderID;

                objParams[1] = new SqlParameter("@OrderItemId", SqlDbType.VarChar, 5000);
                objParams[1].Value = strOrderItemIds;

                objParams[2] = new SqlParameter("@returnval", SqlDbType.NVarChar, 3);
                objParams[2].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "ValidateDispatchItem", objParams);
                string Result = Convert.ToString(objParams[2].Value);
                if (Result == "0")
                {
                    grdScan.Visible = true;
                    btndispatch.Enabled = true;
                    btnScan.Enabled = true;
                    txtBarCodeSearch.Enabled = true;
                    this.Close();
                    ProcessOrder objProcessOrder = new ProcessOrder(StrOrderId, strOrderItemIds, BarCode, blIsScanningChecked);
                    objProcessOrder.Show();
                }
                else
                {
                    lblmessage.Text = "Item has already been dispatched";
                    grdScan.Visible = false;
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        private void SetScanStatusOfOrderItem(string strIsReset)
        {
            Guid orderID = new Guid();
            if (!string.IsNullOrWhiteSpace(StrOrderId))
            {
                orderID = new Guid(StrOrderId);
            }
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objParams = new SqlParameter[5];
                objParams[0] = new SqlParameter("@BarCodeNumber", SqlDbType.VarChar, 5000);
                objParams[0].Value = txtBarCodeSearch.Text;

                objParams[1] = new SqlParameter("@OrderID", SqlDbType.UniqueIdentifier);
                objParams[1].Value = orderID;

                objParams[2] = new SqlParameter("@IsReset", SqlDbType.VarChar, 2);
                objParams[2].Value = strIsReset;

                objParams[3] = new SqlParameter("@RowId", SqlDbType.UniqueIdentifier);
                objParams[3].Value = RowID;

                objParams[4] = new SqlParameter("@IsAllItemScanned", SqlDbType.VarChar, 1000);
                objParams[4].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateIsScannedStatusOfOrderItem", objParams);
                string Result = Convert.ToString(objParams[4].Value);
                if (!blIsAnyItemHaveMoreThanOneQuantity)
                {
                    UpdateBarcode(txtBarCodeSearch.Text);
                }
                if (Result == "Y")
                {
                    if (blIsScanningChecked)
                    {
                        BindGridForScanningChecked();
                    }
                    else
                    {
                        BindGrid();
                    }
                    dispatchorder();

                    //So that user will not be able to change the order item to dispatch after open the process order window
                    btndispatch.Enabled = false;
                    btnScan.Enabled = false;
                    txtBarCodeSearch.Enabled = false;
                    //
                }
                else if (Result != "N" && Result != "Y")
                {
                    if (blIsScanningChecked)
                    {
                        BindGridForScanningChecked();
                    }
                    else
                    {
                        BindGrid();
                    }
                    MessageBox.Show(Result);
                }
                else
                {
                    if (blIsScanningChecked)
                    {
                        BindGridForScanningChecked();
                    }
                    else
                    {
                        BindGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                string Excp = ex.Message;
            }
            finally
            {
                connection.Close();
            }
        }

        private bool IsItemCanUpdateDatabase()
        {
            bool IsItemCanScan = false;
            try
            {

                int intNumberOfScannedItems = 0;
                int intTotalNumbersOfScannedItems = 1;
                bool isBarCodeExists = false;
                for (int i = 0; i < objCustomDT.Rows.Count; i++)
                {
                    string strBarcode = Convert.ToString(objCustomDT.Rows[i]["BarcodeNumber"]);
                    if (strBarcode.ToUpper() == txtBarCodeSearch.Text.ToString().ToUpper())
                    {
                        isBarCodeExists = true;
                        DataView dv = new DataView(objCustomDT);
                        dv.RowFilter = "BarcodeNumber=" + "'" + strBarcode + "'";
                        int intTotalQuantity = Convert.ToInt32(dv.ToTable().Rows.Count);
                        if (intTotalQuantity > 1)
                        {
                            bool IsItemScanned = Convert.ToBoolean(Convert.ToString(objCustomDT.Rows[i]["IsScanned"]));
                            if (IsItemScanned)
                            {
                                if (intTotalNumbersOfScannedItems == intTotalQuantity)
                                {
                                    MessageBox.Show("You have already scanned all item(s) of this SKU, please scan the next required item(s)");
                                    break;
                                }
                                else
                                {
                                    intTotalNumbersOfScannedItems++;
                                    intNumberOfScannedItems++;
                                }
                            }
                            else
                            {
                                objCustomDT.Rows[i]["IsScanned"] = true;
                                GlobalVariablesAndMethods.LogHistory("Scanning Done for Barcode:- " + strBarcode);
                                RowID = new Guid(Convert.ToString(objCustomDT.Rows[i]["RowId"]));
                                //Because that can be the last item to scan
                                if (intTotalQuantity == intNumberOfScannedItems + 1)
                                {
                                    IsItemCanScan = true;
                                }
                                break;
                            }
                        }
                        else
                        {
                            objCustomDT.Rows[i]["IsScanned"] = true;
                            GlobalVariablesAndMethods.LogHistory("Scanning Done for Barcode:- " + strBarcode);
                            IsItemCanScan = true;
                            break;
                        }
                    }
                }
                if (!isBarCodeExists)
                {
                    MessageBox.Show("Invalid Barcode number");
                }
            }
            catch (Exception ex)
            {
                if (intErrorCount < 2 && ex.Message.ToLower().Contains("cannot find column"))
                {
                    MessageBox.Show("Some error occurred please try again to scan items or close this window and select that order again from find order screen");
                    intErrorCount++;
                    BindGridForScanningChecked();
                }
                else
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return IsItemCanScan;
        }

        //That will set the next available barcode in to the textbox to scan.
        private void SetNextBarcodeToScan()
        {
            DataView objDv = new DataView(objCustomDT);
            int intTotalItems = objCustomDT.Rows.Count;
            objDv.RowFilter = "IsScanned = True";
            int intTotalScannedItems = objDv.ToTable().Rows.Count;
            if (intTotalScannedItems > 0 && intTotalScannedItems != intTotalItems)
            {
                objDv = new DataView(objCustomDT);
                objDv.RowFilter = "IsScanned = False";
                txtBarCodeSearch.Text = Convert.ToString(objDv.ToTable().Rows[0]["BarcodeNumber"]);
                txtBarCodeSearch.SelectAll();
                txtBarCodeSearch.Focus();
            }
        }

        /*That will update the IsScanned status to true 
        (in case when quantity is not more than one, the method IsItemCanUpdateDatabase() doing same task but when any of item have more than one quantity)*/
        private void UpdateBarcode(string strBarcode)
        {
            for (int i = 0; i < objCustomDT.Rows.Count; i++)
            {
                string BarcodeNumber = Convert.ToString(objCustomDT.Rows[i]["BarcodeNumber"]);
                if (strBarcode.Trim() == BarcodeNumber.Trim())
                {
                    objCustomDT.Rows[i]["IsScanned"] = true;
                }
            }
        }
        #endregion

        #region "Events"
        private void Scan_Items_Load(object sender, EventArgs e)
        {
            txtBarCodeSearch.Text = BarCode;
            if (blIsScanningChecked)
            {
                BindGridForScanningChecked();
            }
            else
            {
                BindGrid();
            }
        }

        private void btndispatch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtBarCodeSearch.Text))
            {
                MessageBox.Show("Barcode Required");
            }
            else
            {
                if (blIsAllItemScanned)
                {
                    dispatchorder();
                    //So that user will not be able to change the order item to dispatch after open the process order window
                    btndispatch.Enabled = false;
                    btnScan.Enabled = false;
                    txtBarCodeSearch.Enabled = false;
                    //
                }
                else
                {
                    MessageBox.Show("Please scan all items before proceed");
                }
            }
        }

        private void btnback_Click(object sender, EventArgs e)
        {
            SetScanStatusOfOrderItem("Y");
            if (GlobalVars.OpenedOrderIds != null && GlobalVars.OpenedOrderIds != "")
            {
                GlobalVars.setIsOpenStatus(GlobalVars.OpenedOrderIds, "Y");
            }
            this.Close();

            //This is because we to close the "GetOrderData" window if it's already open and rebind the grid.
            Form frmGetOrderData = Application.OpenForms["GetOrderData"];
            Form frmProcessOrder = Application.OpenForms["ProcessOrder"];
            if (frmGetOrderData != null)
            {
                frmGetOrderData.Close();
            }
            if (frmProcessOrder != null)
            {
                frmProcessOrder.Close();
            }
            GetOrderData objGetOrderData = new GetOrderData(BarCode);
            objGetOrderData.Show();
            // End of new rebind code.

        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            if (blIsAnyItemHaveMoreThanOneQuantity)
            {
                if (IsItemCanUpdateDatabase())
                {
                    SetScanStatusOfOrderItem("N");
                }
                else
                {
                    if (blIsScanningChecked)
                    {
                        BindGridForScanningChecked();
                    }
                    else
                    {
                        BindGrid();
                    }
                }
            }
            else
            {
                SetScanStatusOfOrderItem("N");
            }

            txtBarCodeSearch.Text = "";
            txtBarCodeSearch.Focus();

            //Uncomment that code when you want to show the next available barcode on scan textbox
            //SetNextBarcodeToScan();
        }

        private void txtBarCodeSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                btnScan.PerformClick();
                //if (IsItemCanUpdateDatabase())
                //{
                //    SetScanStatusOfOrderItem("N");
                //}
                //else
                //{
                //    if (blIsScanningChecked)
                //    {
                //        BindGridForScanningChecked();
                //    }
                //    else
                //    {
                //        BindGrid();
                //    }
                //}

                //txtBarCodeSearch.Text = "";
                //txtBarCodeSearch.Focus();

                //Uncomment that code when you want to show the next available barcode on scan textbox
                //SetNextBarcodeToScan();
            }
        }
        #endregion
        private void DisplayOrderStatus()
        {
            var statusFiels = new DataTable();
            statusFiels.Columns.Add("SKU");
            statusFiels.Columns.Add("Scanned/Remaining");


            var skuList = new List<SkuDetails>();
           
            foreach (DataGridViewRow row in this.grdScan.Rows)
            {
                var skuDetail = new SkuDetails();
                skuDetail.Sku=row.Cells["SKU"].Value.ToString();
                skuDetail.IsScanned = Convert.ToBoolean(row.Cells["IsScanned"].Value.ToString());
                skuList.Add(skuDetail);
               
            }
            var distinctSkuList = skuList.Select(x => x.Sku).Distinct().ToList();
            totalItems = 0;
            scannedItems = 0;
            foreach (var sku in distinctSkuList)
            {
                var rowvalue = statusFiels.NewRow();
                rowvalue["SKU"] = sku.Trim().ToString();
                scannedItems = scannedItems + skuList.Where(x => x.Sku.Equals(sku) && x.IsScanned).Count();
                totalItems = totalItems + skuList.Where(x => x.Sku.Equals(sku)).Count();
                rowvalue["Scanned/Remaining"] = skuList.Where(x => x.Sku.Equals(sku) && x.IsScanned).Count() +"/" + skuList.Where(x => x.Sku.Equals(sku)).Count();
                statusFiels.Rows.Add(rowvalue);

            }           
            ///
            lblScannedItemStatus.Text = "Total : " + scannedItems + "/" + totalItems;
            grdOrderItemDetails.DataSource = statusFiels;          
            grdOrderItemDetails.DoubleBuffered(true);
            grdOrderItemDetails.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdOrderItemDetails.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdOrderItemDetails.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdOrderItemDetails.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdOrderItemDetails.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdOrderItemDetails.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdOrderItemDetails.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdOrderItemDetails.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdOrderItemDetails.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdOrderItemDetails.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdOrderItemDetails.ScrollBars = ScrollBars.Both;
            grdOrderItemDetails.RowHeadersVisible = false;       
            grdOrderItemDetails.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdOrderItemDetails.EnableHeadersVisualStyles = false;
            grdOrderItemDetails.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdOrderItemDetails.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdOrderItemDetails.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdOrderItemDetails.DefaultCellStyle.Padding = new Padding(1,1,1,1);
            grdOrderItemDetails.AllowUserToAddRows = false;
            grdOrderItemDetails.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            grdOrderItemDetails.Columns["Scanned/Remaining"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdOrderItemDetails.Columns["Scanned/Remaining"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdOrderItemDetails.Columns["Scanned/Remaining"].Width = 105;
            grdOrderItemDetails.ClearSelection();
            grdScan.ClearSelection();
            lblScannedItemStatus.Invalidate();

            
        }
    }
    public class SkuDetails
    {
        public string Sku { get; set; }
        public bool IsScanned { get; set; }
    }
    
}

