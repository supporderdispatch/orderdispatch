﻿namespace ODM.Appliction
{
    partial class myHermesSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(myHermesSettings));
            this.lblApiKey = new System.Windows.Forms.Label();
            this.txtApiKey = new System.Windows.Forms.TextBox();
            this.txtParams = new System.Windows.Forms.TextBox();
            this.lblParams = new System.Windows.Forms.Label();
            this.txtApiUserId = new System.Windows.Forms.TextBox();
            this.lblApiUserId = new System.Windows.Forms.Label();
            this.pnlPrintSettings = new System.Windows.Forms.Panel();
            this.ddlLabelType = new System.Windows.Forms.ComboBox();
            this.lblLabelType = new System.Windows.Forms.Label();
            this.chkInvoiceLandscape = new System.Windows.Forms.CheckBox();
            this.chkLandscape = new System.Windows.Forms.CheckBox();
            this.lblMarginSetting = new System.Windows.Forms.Label();
            this.txtMarginTop = new System.Windows.Forms.TextBox();
            this.txtMarginLeft = new System.Windows.Forms.TextBox();
            this.ddlPaper = new System.Windows.Forms.ComboBox();
            this.ddlPrinter = new System.Windows.Forms.ComboBox();
            this.lblMargin = new System.Windows.Forms.Label();
            this.lblPaper = new System.Windows.Forms.Label();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.grpBoxShippingSettings = new System.Windows.Forms.GroupBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSaveClose = new System.Windows.Forms.Button();
            this.grdMappedServices = new System.Windows.Forms.DataGridView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblWeight = new System.Windows.Forms.Label();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.chkIsBoseLogIn = new System.Windows.Forms.CheckBox();
            this.lblApiKeyRequired = new System.Windows.Forms.Label();
            this.lblUserIdRequired = new System.Windows.Forms.Label();
            this.lblParamsRequired = new System.Windows.Forms.Label();
            this.lblWeightRequired = new System.Windows.Forms.Label();
            this.lblWeightInvalid = new System.Windows.Forms.Label();
            this.pnlPrintSettings.SuspendLayout();
            this.grpBoxShippingSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).BeginInit();
            this.SuspendLayout();
            // 
            // lblApiKey
            // 
            this.lblApiKey.AutoSize = true;
            this.lblApiKey.Location = new System.Drawing.Point(7, 11);
            this.lblApiKey.Name = "lblApiKey";
            this.lblApiKey.Size = new System.Drawing.Size(45, 13);
            this.lblApiKey.TabIndex = 0;
            this.lblApiKey.Text = "API Key";
            // 
            // txtApiKey
            // 
            this.txtApiKey.Location = new System.Drawing.Point(69, 11);
            this.txtApiKey.Name = "txtApiKey";
            this.txtApiKey.Size = new System.Drawing.Size(217, 20);
            this.txtApiKey.TabIndex = 1;
            // 
            // txtParams
            // 
            this.txtParams.Location = new System.Drawing.Point(68, 64);
            this.txtParams.Name = "txtParams";
            this.txtParams.Size = new System.Drawing.Size(218, 20);
            this.txtParams.TabIndex = 3;
            // 
            // lblParams
            // 
            this.lblParams.AutoSize = true;
            this.lblParams.Location = new System.Drawing.Point(6, 67);
            this.lblParams.Name = "lblParams";
            this.lblParams.Size = new System.Drawing.Size(42, 13);
            this.lblParams.TabIndex = 2;
            this.lblParams.Text = "Params";
            // 
            // txtApiUserId
            // 
            this.txtApiUserId.Location = new System.Drawing.Point(68, 37);
            this.txtApiUserId.Name = "txtApiUserId";
            this.txtApiUserId.Size = new System.Drawing.Size(218, 20);
            this.txtApiUserId.TabIndex = 2;
            // 
            // lblApiUserId
            // 
            this.lblApiUserId.AutoSize = true;
            this.lblApiUserId.Location = new System.Drawing.Point(6, 37);
            this.lblApiUserId.Name = "lblApiUserId";
            this.lblApiUserId.Size = new System.Drawing.Size(63, 13);
            this.lblApiUserId.TabIndex = 4;
            this.lblApiUserId.Text = "API User ID";
            // 
            // pnlPrintSettings
            // 
            this.pnlPrintSettings.Controls.Add(this.ddlLabelType);
            this.pnlPrintSettings.Controls.Add(this.lblLabelType);
            this.pnlPrintSettings.Controls.Add(this.chkInvoiceLandscape);
            this.pnlPrintSettings.Controls.Add(this.chkLandscape);
            this.pnlPrintSettings.Controls.Add(this.lblMarginSetting);
            this.pnlPrintSettings.Controls.Add(this.txtMarginTop);
            this.pnlPrintSettings.Controls.Add(this.txtMarginLeft);
            this.pnlPrintSettings.Controls.Add(this.ddlPaper);
            this.pnlPrintSettings.Controls.Add(this.ddlPrinter);
            this.pnlPrintSettings.Controls.Add(this.lblMargin);
            this.pnlPrintSettings.Controls.Add(this.lblPaper);
            this.pnlPrintSettings.Controls.Add(this.lblPrinter);
            this.pnlPrintSettings.Location = new System.Drawing.Point(363, 11);
            this.pnlPrintSettings.Name = "pnlPrintSettings";
            this.pnlPrintSettings.Size = new System.Drawing.Size(404, 230);
            this.pnlPrintSettings.TabIndex = 6;
            // 
            // ddlLabelType
            // 
            this.ddlLabelType.FormattingEnabled = true;
            this.ddlLabelType.Items.AddRange(new object[] {
            "Thermal",
            "Default"});
            this.ddlLabelType.Location = new System.Drawing.Point(69, 181);
            this.ddlLabelType.Name = "ddlLabelType";
            this.ddlLabelType.Size = new System.Drawing.Size(255, 21);
            this.ddlLabelType.TabIndex = 12;
            // 
            // lblLabelType
            // 
            this.lblLabelType.AutoSize = true;
            this.lblLabelType.Location = new System.Drawing.Point(3, 184);
            this.lblLabelType.Name = "lblLabelType";
            this.lblLabelType.Size = new System.Drawing.Size(60, 13);
            this.lblLabelType.TabIndex = 10;
            this.lblLabelType.Text = "Label Type";
            // 
            // chkInvoiceLandscape
            // 
            this.chkInvoiceLandscape.AutoSize = true;
            this.chkInvoiceLandscape.Location = new System.Drawing.Point(69, 143);
            this.chkInvoiceLandscape.Name = "chkInvoiceLandscape";
            this.chkInvoiceLandscape.Size = new System.Drawing.Size(168, 17);
            this.chkInvoiceLandscape.TabIndex = 11;
            this.chkInvoiceLandscape.Text = "Integrated Invoice Landscape";
            this.chkInvoiceLandscape.UseVisualStyleBackColor = true;
            // 
            // chkLandscape
            // 
            this.chkLandscape.AutoSize = true;
            this.chkLandscape.Location = new System.Drawing.Point(69, 119);
            this.chkLandscape.Name = "chkLandscape";
            this.chkLandscape.Size = new System.Drawing.Size(79, 17);
            this.chkLandscape.TabIndex = 10;
            this.chkLandscape.Text = "Landscape";
            this.chkLandscape.UseVisualStyleBackColor = true;
            // 
            // lblMarginSetting
            // 
            this.lblMarginSetting.AutoSize = true;
            this.lblMarginSetting.Location = new System.Drawing.Point(216, 78);
            this.lblMarginSetting.Name = "lblMarginSetting";
            this.lblMarginSetting.Size = new System.Drawing.Size(56, 13);
            this.lblMarginSetting.TabIndex = 7;
            this.lblMarginSetting.Text = "(Left, Top)";
            // 
            // txtMarginTop
            // 
            this.txtMarginTop.Location = new System.Drawing.Point(140, 75);
            this.txtMarginTop.Name = "txtMarginTop";
            this.txtMarginTop.Size = new System.Drawing.Size(60, 20);
            this.txtMarginTop.TabIndex = 9;
            // 
            // txtMarginLeft
            // 
            this.txtMarginLeft.Location = new System.Drawing.Point(69, 75);
            this.txtMarginLeft.Name = "txtMarginLeft";
            this.txtMarginLeft.Size = new System.Drawing.Size(65, 20);
            this.txtMarginLeft.TabIndex = 8;
            // 
            // ddlPaper
            // 
            this.ddlPaper.FormattingEnabled = true;
            this.ddlPaper.Items.AddRange(new object[] {
            "Thermal",
            "Default"});
            this.ddlPaper.Location = new System.Drawing.Point(69, 34);
            this.ddlPaper.Name = "ddlPaper";
            this.ddlPaper.Size = new System.Drawing.Size(255, 21);
            this.ddlPaper.TabIndex = 7;
            // 
            // ddlPrinter
            // 
            this.ddlPrinter.FormattingEnabled = true;
            this.ddlPrinter.Items.AddRange(new object[] {
            "ZDesigner GC420d(EPL)",
            "Brother DCP-1510 series"});
            this.ddlPrinter.Location = new System.Drawing.Point(69, 7);
            this.ddlPrinter.Name = "ddlPrinter";
            this.ddlPrinter.Size = new System.Drawing.Size(255, 21);
            this.ddlPrinter.TabIndex = 6;
            // 
            // lblMargin
            // 
            this.lblMargin.AutoSize = true;
            this.lblMargin.Location = new System.Drawing.Point(3, 75);
            this.lblMargin.Name = "lblMargin";
            this.lblMargin.Size = new System.Drawing.Size(39, 13);
            this.lblMargin.TabIndex = 2;
            this.lblMargin.Text = "Margin";
            // 
            // lblPaper
            // 
            this.lblPaper.AutoSize = true;
            this.lblPaper.Location = new System.Drawing.Point(3, 37);
            this.lblPaper.Name = "lblPaper";
            this.lblPaper.Size = new System.Drawing.Size(35, 13);
            this.lblPaper.TabIndex = 1;
            this.lblPaper.Text = "Paper";
            // 
            // lblPrinter
            // 
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Location = new System.Drawing.Point(3, 10);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(37, 13);
            this.lblPrinter.TabIndex = 0;
            this.lblPrinter.Text = "Printer";
            // 
            // grpBoxShippingSettings
            // 
            this.grpBoxShippingSettings.Controls.Add(this.lblMessage);
            this.grpBoxShippingSettings.Controls.Add(this.btnCancel);
            this.grpBoxShippingSettings.Controls.Add(this.btnSaveClose);
            this.grpBoxShippingSettings.Controls.Add(this.grdMappedServices);
            this.grpBoxShippingSettings.Location = new System.Drawing.Point(3, 292);
            this.grpBoxShippingSettings.Name = "grpBoxShippingSettings";
            this.grpBoxShippingSettings.Size = new System.Drawing.Size(788, 229);
            this.grpBoxShippingSettings.TabIndex = 7;
            this.grpBoxShippingSettings.TabStop = false;
            this.grpBoxShippingSettings.Text = "Shipping Mapping";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(344, 86);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(92, 13);
            this.lblMessage.TabIndex = 18;
            this.lblMessage.Text = "No Record Found";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(590, 193);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(180, 29);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSaveClose
            // 
            this.btnSaveClose.Location = new System.Drawing.Point(404, 194);
            this.btnSaveClose.Name = "btnSaveClose";
            this.btnSaveClose.Size = new System.Drawing.Size(180, 29);
            this.btnSaveClose.TabIndex = 16;
            this.btnSaveClose.Text = "Save && Close";
            this.btnSaveClose.UseVisualStyleBackColor = true;
            this.btnSaveClose.Click += new System.EventHandler(this.btnSaveClose_Click);
            // 
            // grdMappedServices
            // 
            this.grdMappedServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMappedServices.Location = new System.Drawing.Point(6, 19);
            this.grdMappedServices.Name = "grdMappedServices";
            this.grdMappedServices.Size = new System.Drawing.Size(781, 150);
            this.grdMappedServices.TabIndex = 15;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(407, 265);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(180, 31);
            this.btnAdd.TabIndex = 13;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(593, 265);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(180, 31);
            this.btnDelete.TabIndex = 14;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Location = new System.Drawing.Point(6, 96);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(78, 13);
            this.lblWeight.TabIndex = 13;
            this.lblWeight.Text = "Default Weight";
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(90, 94);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(196, 20);
            this.txtWeight.TabIndex = 4;
            // 
            // chkIsBoseLogIn
            // 
            this.chkIsBoseLogIn.AutoSize = true;
            this.chkIsBoseLogIn.Location = new System.Drawing.Point(10, 130);
            this.chkIsBoseLogIn.Name = "chkIsBoseLogIn";
            this.chkIsBoseLogIn.Size = new System.Drawing.Size(102, 17);
            this.chkIsBoseLogIn.TabIndex = 5;
            this.chkIsBoseLogIn.Text = "Is Bose Logging";
            this.chkIsBoseLogIn.UseVisualStyleBackColor = true;
            // 
            // lblApiKeyRequired
            // 
            this.lblApiKeyRequired.AutoSize = true;
            this.lblApiKeyRequired.ForeColor = System.Drawing.Color.Red;
            this.lblApiKeyRequired.Location = new System.Drawing.Point(292, 12);
            this.lblApiKeyRequired.Name = "lblApiKeyRequired";
            this.lblApiKeyRequired.Size = new System.Drawing.Size(50, 13);
            this.lblApiKeyRequired.TabIndex = 15;
            this.lblApiKeyRequired.Text = "Required";
            this.lblApiKeyRequired.Visible = false;
            // 
            // lblUserIdRequired
            // 
            this.lblUserIdRequired.AutoSize = true;
            this.lblUserIdRequired.ForeColor = System.Drawing.Color.Red;
            this.lblUserIdRequired.Location = new System.Drawing.Point(292, 44);
            this.lblUserIdRequired.Name = "lblUserIdRequired";
            this.lblUserIdRequired.Size = new System.Drawing.Size(50, 13);
            this.lblUserIdRequired.TabIndex = 16;
            this.lblUserIdRequired.Text = "Required";
            this.lblUserIdRequired.Visible = false;
            // 
            // lblParamsRequired
            // 
            this.lblParamsRequired.AutoSize = true;
            this.lblParamsRequired.ForeColor = System.Drawing.Color.Red;
            this.lblParamsRequired.Location = new System.Drawing.Point(292, 71);
            this.lblParamsRequired.Name = "lblParamsRequired";
            this.lblParamsRequired.Size = new System.Drawing.Size(50, 13);
            this.lblParamsRequired.TabIndex = 17;
            this.lblParamsRequired.Text = "Required";
            this.lblParamsRequired.Visible = false;
            // 
            // lblWeightRequired
            // 
            this.lblWeightRequired.AutoSize = true;
            this.lblWeightRequired.ForeColor = System.Drawing.Color.Red;
            this.lblWeightRequired.Location = new System.Drawing.Point(292, 101);
            this.lblWeightRequired.Name = "lblWeightRequired";
            this.lblWeightRequired.Size = new System.Drawing.Size(50, 13);
            this.lblWeightRequired.TabIndex = 18;
            this.lblWeightRequired.Text = "Required";
            this.lblWeightRequired.Visible = false;
            // 
            // lblWeightInvalid
            // 
            this.lblWeightInvalid.AutoSize = true;
            this.lblWeightInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblWeightInvalid.Location = new System.Drawing.Point(292, 101);
            this.lblWeightInvalid.Name = "lblWeightInvalid";
            this.lblWeightInvalid.Size = new System.Drawing.Size(65, 13);
            this.lblWeightInvalid.TabIndex = 19;
            this.lblWeightInvalid.Text = "Invalid Input";
            this.lblWeightInvalid.Visible = false;
            // 
            // myHermesSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 533);
            this.Controls.Add(this.lblWeightInvalid);
            this.Controls.Add(this.lblWeightRequired);
            this.Controls.Add(this.lblParamsRequired);
            this.Controls.Add(this.lblUserIdRequired);
            this.Controls.Add(this.lblApiKeyRequired);
            this.Controls.Add(this.chkIsBoseLogIn);
            this.Controls.Add(this.txtWeight);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpBoxShippingSettings);
            this.Controls.Add(this.pnlPrintSettings);
            this.Controls.Add(this.txtApiUserId);
            this.Controls.Add(this.lblApiUserId);
            this.Controls.Add(this.txtParams);
            this.Controls.Add(this.lblParams);
            this.Controls.Add(this.txtApiKey);
            this.Controls.Add(this.lblApiKey);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "myHermesSettings";
            this.Text = "myHermes Settings";
            this.pnlPrintSettings.ResumeLayout(false);
            this.pnlPrintSettings.PerformLayout();
            this.grpBoxShippingSettings.ResumeLayout(false);
            this.grpBoxShippingSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblApiKey;
        private System.Windows.Forms.TextBox txtApiKey;
        private System.Windows.Forms.TextBox txtParams;
        private System.Windows.Forms.Label lblParams;
        private System.Windows.Forms.TextBox txtApiUserId;
        private System.Windows.Forms.Label lblApiUserId;
        private System.Windows.Forms.Panel pnlPrintSettings;
        private System.Windows.Forms.Label lblMargin;
        private System.Windows.Forms.Label lblPaper;
        private System.Windows.Forms.Label lblPrinter;
        private System.Windows.Forms.CheckBox chkInvoiceLandscape;
        private System.Windows.Forms.CheckBox chkLandscape;
        private System.Windows.Forms.Label lblMarginSetting;
        private System.Windows.Forms.TextBox txtMarginTop;
        private System.Windows.Forms.TextBox txtMarginLeft;
        private System.Windows.Forms.ComboBox ddlPaper;
        private System.Windows.Forms.ComboBox ddlPrinter;
        private System.Windows.Forms.ComboBox ddlLabelType;
        private System.Windows.Forms.Label lblLabelType;
        private System.Windows.Forms.GroupBox grpBoxShippingSettings;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridView grdMappedServices;
        private System.Windows.Forms.Button btnSaveClose;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.CheckBox chkIsBoseLogIn;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblApiKeyRequired;
        private System.Windows.Forms.Label lblUserIdRequired;
        private System.Windows.Forms.Label lblParamsRequired;
        private System.Windows.Forms.Label lblWeightRequired;
        private System.Windows.Forms.Label lblWeightInvalid;
    }
}