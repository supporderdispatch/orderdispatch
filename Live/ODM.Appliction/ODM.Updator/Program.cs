﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Updator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                CloseOdIfRunning();
                UninstallOldVersion();
                InstallNewVersion();
                RunInstalledApp();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured while updating Application :"+Environment.NewLine+ex.Message);
            }
        }

        private static void RunInstalledApp()
        {
            var links = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu)).ToList();
            links.AddRange(Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Desktop)).ToList());
            links.AddRange(Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)).ToList());
            var odLink = links.Where(x => x.ToLower().Contains("orderdispatch.lnk")).FirstOrDefault();

            if (odLink != null)
            {
                Process proc = new Process();
                proc.StartInfo.FileName = odLink;
                proc.Start();
            }
        }

        private static void CloseOdIfRunning()
        {
            var processes = System.Diagnostics.Process.GetProcesses();
            var od = processes.Where(x => x.ProcessName.Equals("ODM.Appliction")).FirstOrDefault();
            if (od != null)
            {
                try
                {
                    od.Kill();
                }
                catch {     }
            }
        }

        private static void InstallNewVersion()
        {
            var updateFile = Path.GetTempPath() + "OdUpdate.zip";
            var tempDir = Path.GetTempPath() + "ODsetup-temp";
            if (Directory.Exists(tempDir))
            {
                Directory.Delete(tempDir,true);
            }
            Directory.CreateDirectory(tempDir);
            if (File.Exists(updateFile))
            {
                System.IO.Compression.ZipFile.ExtractToDirectory(updateFile, tempDir);
                DirectoryInfo dir = new DirectoryInfo(tempDir);
                var setupPath = dir.FullName;

                if(!dir.GetFiles().Any(x=>x.ToString().ToLower().Equals("setup")))
                {
                   var insideZip=dir.GetFiles().FirstOrDefault().ToString();
                   System.IO.Compression.ZipFile.ExtractToDirectory(tempDir+"\\"+insideZip,tempDir);
                   setupPath += "\\" + insideZip.Replace(".zip","");
                }
                var setup = setupPath+"\\" + dir.GetDirectories().FirstOrDefault().GetFiles().Where(x => x.Name.ToLower().Equals("setup.exe")).FirstOrDefault().ToString();
                Process process = new Process();
                process.StartInfo.FileName = setup;
                process.StartInfo.Verb = "runas";
                process.StartInfo.UseShellExecute = false;
                process.Start();
                process.WaitForExit();
            }
        }

        private static void UninstallOldVersion()
        {
            using (var localMachine = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32))
            {
                var key = localMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", false);
                foreach (String keyName in key.GetSubKeyNames())
                {
                    RegistryKey subkey = key.OpenSubKey(keyName);
                    var displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.ToLower().Equals("odsetup"))
                    {
                        var data = key.OpenSubKey(keyName.ToString());
                        var uninstall = data.GetValue("UninstallString").ToString();
                        string temp = "/x{" + uninstall.Split("/".ToCharArray())[1].Split("I{".ToCharArray())[2];

                        Process process = new Process();
                        process.StartInfo.FileName = uninstall.Split("/".ToCharArray())[0];
                        process.StartInfo.Arguments = "/qn " + temp;
                   //     process.StartInfo.Arguments =  temp;
                        process.StartInfo.Verb = "runas";
                        process.StartInfo.UseShellExecute = false;
                        process.Start();
                        process.WaitForExit();
                        break;
                    }

                }
            }
        }
    }
}
