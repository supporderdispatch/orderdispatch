﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Web.Services;

namespace ConsumeWcf
{
    public partial class ODispatch : System.Web.UI.Page
    {
        /*
         Service references details
         * * 1.OrderDispatch:- This service reference is used to test the service that we hosted in console
         * to test that the console host must be in running mode
         
         * * 2.HostedOrderDispatchService:- This service reference is used to test the serive with local iis
         * to test that the wcf service must be available and it's application pool must have start status under iis
         
         * * 3.OrderDispatchLive:- This service reference is used to control the live wcf of Our Server(104.238.81.200).
         * 
         * * 4.OD_Live:- This service reference is used to control the live wcf of the Oliver's Server.
         */

        //OrderDispatch.OrderDispatchClient client = new OrderDispatch.OrderDispatchClient();
        //HostedOrderDispatchService.OrderDispatchClient client = new HostedOrderDispatchService.OrderDispatchClient();
        //OrderDispatchLive.OrderDispatchClient client = new OrderDispatchLive.OrderDispatchClient();
        OD_Live.OrderDispatchClient client = new OD_Live.OrderDispatchClient();

        public static string IntervalTime;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    UpdateWebServiceStatus("");
                    client.StartServiceOnLoad();
                }
            }
            //Ignore if we are getting "TimeoutException" because wcf is working behind the screen.
            catch (TimeoutException TimeEx)
            {
                string strError = TimeEx.Message;
            }
            catch (Exception ex)
            {
                //Log exception in txt if it is not a "TimeoutException".
                Utill.LogError(ex);
            }
        }
        protected void btnStop_Click(object sender, EventArgs e)
        {
            UpdateWebServiceStatus("N");
            try
            {
                string StopService = client.StopService();
            }
            catch (Exception ex)
            {
                Utill.LogError(ex);
            }
        }

        protected void btnStart_Click(object sender, EventArgs e)
        {
            UpdateWebServiceStatus("Y");
            try
            {
                string strStartData = client.StartService();
            }
            catch (Exception ex)
            {
                Utill.LogError(ex);
            }
        }

        private void UpdateWebServiceStatus(string strServiceStatus)
        {
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            try
            {
                con.ConnectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["OrderDispatchConnection"]);
                cmd.CommandText = "UpdateWebServiceStatus_sp";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;

                cmd.Parameters.Add("@Status", SqlDbType.VarChar, 1).Value = strServiceStatus;

                cmd.Parameters.Add("@Result", SqlDbType.Int);
                cmd.Parameters["@Result"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@CurrentStatus", SqlDbType.VarChar, 2);
                cmd.Parameters["@CurrentStatus"].Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();

                //Using this flag to know what user set the service flag in database by pressing on/off button from UI
                int intResult = Convert.ToInt32(cmd.Parameters["@Result"].Value);

                //Using this flag to know what is the current status of service on the load of page.
                string strCurrentStatus = Convert.ToString(cmd.Parameters["@CurrentStatus"].Value);

                if (Convert.ToString(strCurrentStatus) != null && Convert.ToString(strCurrentStatus).ToUpper() == "Y")
                {
                    btnStart.Visible = false;
                    btnStop.Visible = true;
                    lblCurrentState.ForeColor = Color.Green;
                    lblCurrentState.Text = "Running";
                }
                else if (Convert.ToString(strCurrentStatus) != null && Convert.ToString(strCurrentStatus).ToUpper() == "N")
                {
                    btnStop.Visible = false;
                    btnStart.Visible = true;
                    lblCurrentState.ForeColor = Color.Red;
                    lblCurrentState.Text = "Stopped";
                }

                if (intResult == 0)
                {
                    trmsg.Visible = true;
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Text = "Service has been stopped";
                }
                else if (intResult == 1)
                {
                    trmsg.Visible = true;
                    lblMessage.ForeColor = Color.Green;
                    lblMessage.Text = "Service has been started";
                }
            }
            catch (Exception ex)
            {
                Utill.LogError(ex);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
        }

        protected void btnClearData_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            try
            {
                con.ConnectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["OrderDispatchConnection"]);
                con.Open();
                cmd.Connection = con;
                SqlParameter parmResult = new SqlParameter("@Result", SqlDbType.Int);
                parmResult.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parmResult);
                cmd.CommandText = "ClearAllTables";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                int DeleteResult = Convert.ToInt32(parmResult.Value);
                if (DeleteResult == 1)
                {
                    trmsg.Visible = true;
                    lblMessage.ForeColor = Color.Green;
                    lblMessage.Text = "Records cleared successfully!";
                }
            }
            catch (Exception ex)
            {
                Utill.LogError(ex);
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Get the last and next execution time details(Executing by ajax call on that page)
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.Synchronized)]// To prevent the random calls from Ajax.
        [WebMethod(EnableSession = true)]
        public static string GetServiceRunningDetails()
        {
            string strRestult = "";
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            try
            {
                con.ConnectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["OrderDispatchConnection"]);
                con.Open();
                cmd.Connection = con;
                cmd.CommandText = "GetWebServiceRunningDetails";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    string strLastCallTime = Convert.ToString(dt.Rows[0]["HostStartedOn"]);// The time when execution started last time(last execution time)
                    IntervalTime = Convert.ToString(dt.Rows[0]["NextCall"]);//Time interval(in milliseconds) to execute service.
                    DateTime dtLastDate = Convert.ToDateTime(strLastCallTime);
                    DateTime dtNextDate = dtLastDate.AddMilliseconds(Convert.ToDouble(IntervalTime));//Calculating next execution time
                    DateTime dtCurrentDate = Convert.ToDateTime(dt.Rows[0]["CurrentDateTime"]);//Current datetime of server(client server)

                    /*if that condition is true that means service has been stopped automatically,
                     *we need to reload the page to start that behind the scene.
                     *Otherwise send last and next execution time to show on website.
                     *Note:- Reason behind "dtCurrentDate.AddMinutes(1)" is the last service execution may take time to complete,
                     *that's why we are giving 1 minute to check, if still current date is greater than the last execution time
                     *then we need to reload the page.
                    */
                    if (dtCurrentDate.AddMinutes(1) > dtNextDate)
                    {
                        strRestult = "Reload";
                    }
                    else
                    {
                        //string strNetCallTime = Convert.ToString(Convert.ToDateTime(strLastCallTime).AddMilliseconds(Convert.ToDouble(dt.Rows[0]["NextCall"])));
                        strRestult = dtLastDate.ToString("yyyy-MMM-dd hh:mm:ss tt") + "," + dtNextDate.ToString("yyyy-MMM-dd hh:mm:ss tt");
                    }
                }
            }
            catch (Exception ex)
            {
                Utill.LogError(ex);
            }
            finally
            {
                con.Close();
            }
            return strRestult;
        }
    }
}