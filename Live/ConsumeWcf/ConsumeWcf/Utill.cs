﻿using System;
using System.IO;

namespace ConsumeWcf
{
    public static class Utill
    {
        public static void LogError(Exception ex)
        {
            try
            {
                string strBaseDir = AppDomain.CurrentDomain.BaseDirectory;
                string strPathToLogError = strBaseDir + "OrderDispatchErrorLog\\";
                if (!Directory.Exists(strPathToLogError))
                {
                    Directory.CreateDirectory(strPathToLogError);
                }
                string strErrorTitle = DateTime.Now.ToString("dd-MM-yyyy");
                string message = string.Format("Time: {0} ", DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "--------------------------------------------------------------------------------";
                message += Environment.NewLine;
                message += string.Format("Inner Exception: {0}", ex.InnerException);
                message += Environment.NewLine;
                message += string.Format("Message: {0}", ex.Message);
                message += Environment.NewLine;
                message += string.Format("StackTrace: {0}", ex.StackTrace);
                message += Environment.NewLine;
                message += string.Format("Source: {0}", ex.Source);
                message += Environment.NewLine;
                message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
                message += Environment.NewLine;
                message += "--------------------------------------------------------------------------------";
                message += Environment.NewLine;
                strPathToLogError = strPathToLogError + strErrorTitle + ".txt";
                if (!File.Exists(strPathToLogError))
                {
                    File.Create(strPathToLogError).Dispose();
                    using (StreamWriter writer = new StreamWriter(strPathToLogError, true))
                    {
                        writer.WriteLine(message);
                        writer.Close();
                    }
                }
                else
                {
                    using (StreamWriter writer = File.AppendText(strPathToLogError))
                    {
                        writer.WriteLine(message);
                        writer.Close();
                    }
                }
            }
            catch (Exception Ex)
            { }
        }

        public static void LogServiceStatus(int intTotalOrders = 0)
        {
            try
            {
                string strBaseDir = AppDomain.CurrentDomain.BaseDirectory;
                string strPathToServiceStatus = "";

                if (strBaseDir.Contains("bin\\Debug"))
                {
                    strPathToServiceStatus = strBaseDir.Replace("\\bin\\Debug", "\\OrderDispatchWebServiceStatus");
                }
                else
                {
                    strPathToServiceStatus = strBaseDir + "OrderDispatchWebServiceStatus\\";
                }
                if (!Directory.Exists(strPathToServiceStatus))
                {
                    Directory.CreateDirectory(strPathToServiceStatus);
                }
                //string strErrorTitle = DateTime.Now.ToShortDateString();
                string message = string.Format("Time: {0} ", DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "--------------------------------------------------------------------------------";
                message += Environment.NewLine;
                message += string.Format("Total Number Of Orders: {0}", intTotalOrders);
                message += Environment.NewLine;
                message += string.Format("Last Datetime when service started: {0}", DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "--------------------------------------------------------------------------------";
                message += Environment.NewLine;
                strPathToServiceStatus = strPathToServiceStatus + "WebServiceStatusLog.txt";
                if (!File.Exists(strPathToServiceStatus))
                {
                    File.Create(strPathToServiceStatus).Dispose();
                    using (StreamWriter writer = new StreamWriter(strPathToServiceStatus, true))
                    {
                        writer.WriteLine(message);
                        writer.Close();
                    }
                }
                else
                {
                    long LogLength = new FileInfo(strPathToServiceStatus).Length;
                    if ((LogLength / 1024) > 5)
                    {
                        File.Delete(strPathToServiceStatus);
                        File.Create(strPathToServiceStatus).Dispose();
                        using (StreamWriter writer = new StreamWriter(strPathToServiceStatus, true))
                        {
                            writer.WriteLine(message);
                            writer.Close();
                        }
                    }
                    else
                    {
                        using (StreamWriter writer = File.AppendText(strPathToServiceStatus))
                        {
                            writer.WriteLine(message);
                            writer.Close();
                        }
                    }
                }
            }
            catch (Exception exLog)
            {
                LogError(exLog);
            }
        }
    }
}