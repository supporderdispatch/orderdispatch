import { AuthService } from './../auth.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {
title = '';

  constructor(
    public authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {

  }

  logOutClick(){
    this.authService.logOut();
    this.router.navigate(['/']).finally(() => {
      window.location.reload();
    });
  }

  @HostListener('window:unload', ['$event'])
unloadHandler(event) {
}
}
