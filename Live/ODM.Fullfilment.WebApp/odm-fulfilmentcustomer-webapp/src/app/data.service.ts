import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  header = {
    headers: new HttpHeaders()
      .set('Authorization',  `Bearer ${this.authService.getToken()}`)
  };

  GetOrderItems(orderNumber: string) {
    const url = 'http://localhost:59143/Api/Orders/GetOrderItems?orderNumber=' + orderNumber;

    return this.http.get(url, this.header);
  }

  GetOrderImage(orderNumber) {
    const url = 'http://localhost:59143/Api/OrderImage/GetOrderImage?orderNumber=' + orderNumber;

    return this.http.get(url, this.header);
  }

  getOrderCustomerDetails(orderNumber) {
    const url = 'http://localhost:59143/Api/Customer/GetOrderCustomerDetails?orderNumber=' + orderNumber;
    return this.http.get(url, this.header);
  }

  constructor(
    private authService: AuthService,
    private http: HttpClient,
  ) { }

  GetOrders(fromDate, toDate): Observable<any> {
    const url = 'http://localhost:59143/Api/Orders/GetOrders?fromDate=' + fromDate + '&toDate=' + toDate;
    return this.http.get(url, this.header);
  }
}
