import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  loadUserDetails(): void{
    const url = 'http://localhost:59143/Api/Customer/GetCustomerName';
    const header = {
      headers: new HttpHeaders()
        .set('Authorization',  `Bearer ${this.authService.getToken()}`)
    };

    this.http.get(url, header).subscribe(x => {
      this.authService.logedInUserName = x.toString();
      this.authService.userLoggedIn = true;
    });
  }

}
