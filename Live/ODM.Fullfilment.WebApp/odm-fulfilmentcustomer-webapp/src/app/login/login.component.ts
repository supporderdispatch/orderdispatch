import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginForm;
showInvalidCredentialsError = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
            username: '',
            password: ''
        });
  }

  ngOnInit(): void {
    if (this.authService.getToken()){
      this.router.navigate(['/dashboard']);
    }
  }

  onSubmit(loginForm){
   this.authService.logIn(loginForm).subscribe(token => {
     if (token) {
       this.authService.setToken(token);
       this.showInvalidCredentialsError = false;
       this.router.navigate(['/dashboard']);
     }
     else {
       this.showInvalidCredentialsError = true;
     }
   });
   this.loginForm.reset();
  }
}
