import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { CustomerDashboardComponent } from './customer-dashboard/customer-dashboard.component';
import { CookieService } from 'ngx-cookie-service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyMatModule } from './my-mat/my-mat.module';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {MatNativeDateModule} from '@angular/material/core';
import { OrdersComponent } from './customer-dashboard/dashboard-features/orders/orders.component';
import { CustomerDetailsComponent } from './customer-dashboard/dashboard-features/customer-details/customer-details.component';
import { OrderDetailsComponent } from './customer-dashboard/dashboard-features/order-details/order-details.component';
import { OrderImageComponent } from './customer-dashboard/dashboard-features/order-image/order-image.component';
import { FormsModule } from '@angular/forms';
import { OrderItemsComponent } from './customer-dashboard/dashboard-features/order-items/order-items.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TopBarComponent,
    CustomerDashboardComponent,
    OrdersComponent,
    CustomerDetailsComponent,
    OrderDetailsComponent,
    OrderImageComponent,
    OrderItemsComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MyMatModule,
    FormsModule,
  ],
  providers: [CookieService, { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } }],
  bootstrap: [AppComponent]
})
export class AppModule { }
