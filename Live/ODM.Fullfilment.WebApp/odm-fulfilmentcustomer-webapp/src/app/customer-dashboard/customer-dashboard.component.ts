import { DashboardService } from './../dashboard.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrdersComponent } from './dashboard-features/orders/orders.component';

@Component({
  selector: 'app-customer-dashboard',
  templateUrl: './customer-dashboard.component.html',
  styleUrls: ['./customer-dashboard.component.css']
})
export class CustomerDashboardComponent implements OnInit {
userDetails;
public components = [OrdersComponent];
  constructor(
    public authService: AuthService,
    private router: Router,
    private dashboardService: DashboardService
  ) { }

  ngOnInit(): void {
    if (this.authService.getToken()) {
      this.dashboardService.loadUserDetails();
    }
    else{
      this.router.navigate(['login']);
    }
  }

}
