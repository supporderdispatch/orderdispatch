import { DataService } from './../../../data.service';
import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
@Output() showDetail = new EventEmitter();
fromDate = new FormControl(this.getPreviousMonthDate());
toDate = new FormControl(new Date());
selected = 'All';
searchValue = '';

displayedColumns: string[] = [ 'NumOrderId' , 'FullName' , 'PickingBatch', 'PostalServiceName' , 'TrackingNumber' , 'ReferenceNum' , 'Processed', 'ProcessedDateTime', 'customColumn1'];
dataSource = new MatTableDataSource();
showLoader = false;

constructor(
    private dataService: DataService
  ) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  ngOnInit(): void {
    this.loadSelectedOrders();
  }
  getPreviousMonthDate(): Date {
    const today = new Date();
    today.setDate(today.getDate() - 14);
    return today;
  }
  showOrderTypeChanged() {
    const filterPredicate = this.dataSource.filterPredicate;
    this.searchValue = '';
    this.dataSource.filterPredicate = function customFilter(data: any , filter: string ): boolean {
      return filter === 'All' ? true : data.Processed === true && filter === 'Processed' || data.Processed === false && filter === 'UnProcessed' ;
    };
    this.dataSource.filter = this.selected;
    this.dataSource.filterPredicate = filterPredicate;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getOrders(fromDateValue, toDateValue) {
    this.showLoader = true;
    this.dataService.GetOrders(fromDateValue, toDateValue).subscribe(orders => {
      this.dataSource.data = orders;
      this.dataSource.paginator = this.paginator;
      this.showLoader = false;
      this.dataSource.filter = '';
      this.selected = 'All';
    });
  }

      loadSelectedOrders() {
        this.getOrders(new Date(this.fromDate.value).toISOString() , new Date(this.toDate.value).toISOString());
    }
}
