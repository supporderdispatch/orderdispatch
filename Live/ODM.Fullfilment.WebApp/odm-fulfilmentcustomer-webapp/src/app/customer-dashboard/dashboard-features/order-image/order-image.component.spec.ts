import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderImageComponent } from './order-image.component';

describe('OrderImageComponent', () => {
  let component: OrderImageComponent;
  let fixture: ComponentFixture<OrderImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
