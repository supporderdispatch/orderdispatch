import { DataService } from './../../../data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-order-image',
  templateUrl: './order-image.component.html',
  styleUrls: ['./order-image.component.css']
})
export class OrderImageComponent implements OnInit {
orderImage;
showLoader;

  constructor(
    private route: ActivatedRoute,
    private data: DataService
    ) { }

  ngOnInit(): void {
    this.loadOrderImage();
  }

  loadOrderImage() {
    this.route.paramMap.subscribe(params => {
      this.showLoader = true;
      this.data.GetOrderImage(params.get('orderNumber')).subscribe(image => {
        this.orderImage = image;
        this.showLoader = false;
      });
    });
  }
}
