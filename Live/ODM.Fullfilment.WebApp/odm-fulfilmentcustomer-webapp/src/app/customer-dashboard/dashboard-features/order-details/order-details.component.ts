import { DashboardService } from './../../../dashboard.service';
import { AuthService } from './../../../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
orderNumber;
imageExists: boolean;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private dashboardService: DashboardService
  ) { }

  ngOnInit(): void {
    if (this.authService.getToken()) {
      this.loadOrderDetails();
      this.dashboardService.loadUserDetails();
    }
    else{
      this.router.navigate(['login']);
    }
  }

  loadOrderDetails() {
    this.route.paramMap.subscribe(params => {
      this.orderNumber = params.get('orderNumber');
      this.imageExists = Boolean(JSON.parse(params.get('imageExists')));
    });
  }

}
