import { DataService } from './../../../data.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-items',
  templateUrl: './order-items.component.html',
  styleUrls: ['./order-items.component.css']
})
export class OrderItemsComponent implements OnInit {
OrderItems;
displayedColumns: string[] = [ 'title' , 'sku' , 'qnt'];

  constructor(
    private route: ActivatedRoute,
    private data: DataService
  ) { }

  ngOnInit(): void {
    this.loadItems();
  }
  loadItems() {
    this.route.paramMap.subscribe(params => {
      this.data.GetOrderItems(params.get('orderNumber')).subscribe(items => {
        this.OrderItems = items;
      });
    });
  }

}
