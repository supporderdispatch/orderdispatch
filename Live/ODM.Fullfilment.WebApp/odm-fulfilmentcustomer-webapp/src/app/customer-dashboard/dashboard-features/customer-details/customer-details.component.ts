import { DataService } from './../../../data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {
customerDetails;
showLoader;

  constructor(
    private data: DataService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getCustomerDetails();

  }

  getCustomerDetails() {
    this.route.paramMap.subscribe(params => {
      this.showLoader = true;
      this.data.getOrderCustomerDetails(params.get('orderNumber')).subscribe(customerData => {
        this.customerDetails = customerData;
        this.showLoader = false;
      });
    });
  }

}
