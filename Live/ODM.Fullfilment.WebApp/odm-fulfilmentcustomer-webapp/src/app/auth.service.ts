import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userLoggedIn = false;
  logedInUserName = '';

  logOut() {
    localStorage.removeItem('_uIdToken_live');
    localStorage.clear();
    this.userLoggedIn = false;
  }

setToken(token) {
  localStorage.setItem('_uIdToken_live', token);
}

getToken(): any {
  if (localStorage.getItem('_uIdToken_live')) {
    this.CheckTokenIfValid();
  }
  return localStorage.getItem('_uIdToken_live');
}
  CheckTokenIfValid() {
    const header = {
      headers: new HttpHeaders()
        .set('Authorization',  `Bearer ${localStorage.getItem('_uIdToken_live')}`)
    };
    const url = 'http://localhost:59143/Api/Auth/CheckTokenIfValid';
    this.http.get(url, header).subscribe(x => {
      if (x === false){
        this.logOut();
        this.route.navigate(['/login']);
      }
    });
  }

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private route: Router
  ) { }

  logIn(loginFormData): Observable<string> {
    const url = 'http://localhost:59143/Api/Auth/ValidateUser';
    return this.http.post<string>(url, loginFormData);
  }
}
