﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Owin;
using Microsoft.Owin.Security.Jwt;
using ODM.FulfilmentCustomer.WebApp.Models;

[assembly: OwinStartup(typeof(ODM.FulfilmentCustomer.WebApp.Startup))]

namespace ODM.FulfilmentCustomer.WebApp
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidAudience = ConfigHelper.GetAudience(),
                        ValidIssuer = ConfigHelper.GetIssuer(),
                        IssuerSigningKey = ConfigHelper.GetSymmetricSecurityKey(),
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true
                    }
                });
        }
    }
}
