﻿using ODM.FulfilmentCustomer.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace ODM.FulfilmentCustomer.WebApp.Controllers
{
    public class UserDetails
    {
        public static UserCredentials GetUserDetails(HttpRequestMessage request)
        {
            var result = new UserCredentials();
            var token = request.Headers.Authorization.Parameter;

            var simplePrinciple = JwtManager.GetPrincipal(token);
            var identity = simplePrinciple?.Identity as ClaimsIdentity;

            if (identity.IsAuthenticated)
            {
                var usernameClaim = identity.FindFirst(ClaimTypes.Name);
                result.username = usernameClaim?.Value;
            }
            return result;
        }
    }
}