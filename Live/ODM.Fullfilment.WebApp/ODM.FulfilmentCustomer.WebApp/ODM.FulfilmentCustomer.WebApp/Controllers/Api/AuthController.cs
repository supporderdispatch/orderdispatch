﻿using ODM.FulfilmentCustomer.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ODM.FulfilmentCustomer.WebApp.Models.JwtSettings;

namespace ODM.FulfilmentCustomer.WebApp.Controllers.Api
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AuthController : ApiController
    {
        [HttpPost]
        [AllowAnonymous]
        public string ValidateUser([FromBody]UserCredentials credentials)
        {
            if(Validation.ValidateUser(credentials.username, credentials.password))
            {
                return  JwtManager.GenerateToken(credentials.username);
                /*System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(DateTime.Now.Date.ToString() + AppConfig.AppSecret+"." +credentials.username + ":" + credentials.password))+AppConfig.AppSecret;*/
            }
            return string.Empty;
        }

        [HttpGet]
        [JwtAuthentication]
        public bool CheckTokenIfValid()
        {
            string userName = string.Empty;
            try
            {
                userName = UserDetails.GetUserDetails(this.Request).username;
            }
            catch { }

            return !string.IsNullOrEmpty(userName);
        }

    }
}