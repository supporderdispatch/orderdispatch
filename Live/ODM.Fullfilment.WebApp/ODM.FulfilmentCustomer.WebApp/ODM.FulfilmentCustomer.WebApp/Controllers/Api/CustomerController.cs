﻿using ODM.FulfilmentCustomer.WebApp.Models;
using ODM.FulfilmentCustomer.WebApp.Models.JwtSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Data;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ODM.FulfilmentCustomer.WebApp.Controllers.Api
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CustomerController : ApiController
    {
        [HttpGet]
        [JwtAuthentication]
        public string GetCustomerName()
        {
            return new DataLayer().GetCustomerName(UserDetails.GetUserDetails(this.Request));
        }

        [HttpGet]
        [JwtAuthentication]
        public CustomerAddress GetOrderCustomerDetails(string orderNumber)
        {
            var data = new DataLayer().GetOrderCustomerDetail(orderNumber, UserDetails.GetUserDetails(this.Request).username).
                AsEnumerable().Select(x => new Address()
                {
                    FullName=x.Field<string>("FullName"),
                    Address1= x.Field<string>("Address1"),
                    Address2= x.Field<string>("Address2"),
                    Address3= x.Field<string>("Address3"),
                    EmailAddress= x.Field<string>("EmailAddress"),
                    Country= x.Field<string>("Country"),
                    IsBilling= x.Field<bool>("IsBilling"),
                    IsShipping= x.Field<bool>("IsShipping"),
                    PhoneNumber= x.Field<string>("PhoneNumber"),
                    PostCode= x.Field<string>("PostCode"),
                    Region= x.Field<string>("Region"),
                    Town= x.Field<string>("Town")
                }).ToList();
            var result = new CustomerAddress();
            if(data.Count > 0)
            {
                result.FullName = data.FirstOrDefault().FullName;
                result.PhoneNumber = data.FirstOrDefault().PhoneNumber;
                result.EmailAddress = data.FirstOrDefault().EmailAddress;
                result.ShippingAddress = data.Where(x => x.IsShipping).FirstOrDefault();
                result.BillingAddress = data.Where(x => x.IsBilling).FirstOrDefault();
            }
            return result;
        }

        public class CustomerAddress
        {
            public string FullName { get; set; }
            public string PhoneNumber { get; set; }
            public string EmailAddress { get; set; }
            public Address BillingAddress { get; set; }
            public Address ShippingAddress { get; set; }
        }
    }
}