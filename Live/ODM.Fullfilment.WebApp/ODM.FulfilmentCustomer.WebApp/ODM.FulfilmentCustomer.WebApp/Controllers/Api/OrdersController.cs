﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using ODM.FulfilmentCustomer.WebApp.Models;
using System.Data;
using ODM.FulfilmentCustomer.WebApp.Models.JwtSettings;

namespace ODM.FulfilmentCustomer.WebApp.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OrdersController : ApiController
    {

        [HttpGet]
        [JwtAuthentication]
        public IEnumerable<OrderDetails> GetOrders(string fromDate, string toDate)
        {
            var fromDateValue = DateTime.Now.AddMonths(-1);
            var toDateValue = DateTime.Now;
            DateTime.TryParse(fromDate, out fromDateValue);
            DateTime.TryParse(toDate, out toDateValue);

            return new DataLayer().GetOrders(UserDetails.GetUserDetails(this.Request), fromDateValue, toDateValue).AsEnumerable()
                .Select(x => new OrderDetails()
                {
                    PostalServiceName = x.Field<string>("PostalServiceName"),
                    TrackingNumber = x.Field<string>("TrackingNumber"),
                    FullName = x.Field<string>("FullName"),
                    ProcessedDateTime = x.Field<DateTime?>("ProcessedDateTime"),
                    NumOrderId = x.Field<string>("NumOrderId"),
                    PickingBatch = Convert.ToBoolean(x.Field<int>("PickingBatch")),
                    Processed = Convert.ToBoolean(x.Field<int>("Processed")),
                    ImageExists = Convert.ToBoolean(x.Field<int>("ImageExists")),
                    ReferenceNum = GetSlicedString(x.Field<string>("ReferenceNum"))

                }).ToList();
        }

        [HttpGet]
        [JwtAuthentication]
        public IEnumerable<OrderItem> GetOrderItems(string orderNumber)
        {
            if (!string.IsNullOrEmpty(UserDetails.GetUserDetails(this.Request).username))
            {
                return new DataLayer().GetOrderItems(orderNumber,UserDetails.GetUserDetails(this.Request).username)
                    .AsEnumerable().Select(x => new OrderItem()
                    {
                        qnt=x.Field<int>("Quantity"),
                        sku=x.Field<string>("SKU"),
                        title=x.Field<string>("ItemTitle")
                    });
            }
            throw new UnauthorizedAccessException();
        }
        private string GetSlicedString(string v)
        {
            if (!string.IsNullOrEmpty(v))
            {
                return v.Length > 20 ? v.Substring(0, 19) : v;
            }
            return v;
        }
    }
}