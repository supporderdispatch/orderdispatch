﻿using ODM.FulfilmentCustomer.WebApp.Models;
using ODM.FulfilmentCustomer.WebApp.Models.JwtSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ODM.FulfilmentCustomer.WebApp.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OrderImageController : ApiController
    {
        [HttpGet]
        [JwtAuthentication]
        public string  GetOrderImage(string orderNumber)
        {
            return new DataLayer().GetOrderImage(orderNumber, UserDetails.GetUserDetails(this.Request).username);
        }   
    }
}