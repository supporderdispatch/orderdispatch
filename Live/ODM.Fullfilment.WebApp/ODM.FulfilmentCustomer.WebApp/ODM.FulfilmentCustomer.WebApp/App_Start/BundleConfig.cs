﻿using System.Web;
using System.Web.Optimization;

namespace ODM.FulfilmentCustomer.WebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts").IncludeDirectory(
                "~/NgApp","*.js",true
                ));



            bundles.Add(new StyleBundle("~/Styles").Include(
                      "~/NgApp/*.css"));
        }
    }
}
