﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ODM.FulfilmentCustomer.WebApp.Models
{
    public class Validation
    {
        public static bool ValidateUser(string username,string password)
        {
            return (bool)new DataLayer().CheckIfUserExists(username, password);
        }
    }
}