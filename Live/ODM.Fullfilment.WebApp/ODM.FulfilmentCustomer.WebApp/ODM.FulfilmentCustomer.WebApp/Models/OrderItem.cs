﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ODM.FulfilmentCustomer.WebApp.Models
{
    public class OrderItem
    {
        public string title { get; set; }
        public string sku { get; set; }
        public int qnt { get; set; }
    }
}