﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ODM.FulfilmentCustomer.WebApp.Models
{
    public class Context
    {
        public SqlConnection conn;

        public Context()
        {
            conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
        }
        public void PostOperationEvents()
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
    }
    public class MakeConnection
    {
        public static string GetConnection()
        {
            var database = "OrderDispatch";
            var userName = "ettu_orderdispatch";
            var password = "Ettuod@2099";

            return string.Format("Data Source=77.68.11.228,1433;Initial Catalog={0};User Id={1};Password={2}",
                database,
                userName,
                password
                );
        }
    }

    public class DataLayer:Context
    {

        public bool CheckIfUserExists(string username, string password)
        {
            var result = false;
            try
            {
                SqlParameter[] objParam = new SqlParameter[3];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "CHECK_IF_USER_EXISTS";

                objParam[1] = new SqlParameter("@username", SqlDbType.VarChar, 100);
                objParam[1].Value = username;

                objParam[2] = new SqlParameter("@password", SqlDbType.VarChar, 100);
                objParam[2].Value = password;

                result = Convert.ToBoolean(SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetFulfilementCustomerData", objParam).Tables[0].Rows[0]["UserFound"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public DataTable GetOrderItems(string orderNumber, string username)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[3];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ORDER_ITEMS";

                objParam[1] = new SqlParameter("@OrderNumber", SqlDbType.VarChar, 100);
                objParam[1].Value = orderNumber;


                objParam[2] = new SqlParameter("@username", SqlDbType.VarChar, 100);
                objParam[2].Value = username;

                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetFulfilementCustomerData", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public string GetCustomerName(UserCredentials userCredentials)
        {
            var result = string.Empty;
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_CUSTOMER_NAME";

                objParam[1] = new SqlParameter("@username", SqlDbType.VarChar, 100);
                objParam[1].Value = userCredentials.username;

                var data = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetFulfilementCustomerData", objParam).Tables[0];
                if (data.Rows.Count > 0)
                {
                    result = data.Rows[0]["CustomerName"].ToString(); 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public DataTable GetOrders(UserCredentials userCredentials,DateTime fromDate, DateTime toDate)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ORDERS";

                objParam[1] = new SqlParameter("@username", SqlDbType.VarChar, 100);
                objParam[1].Value = userCredentials.username;

                objParam[2] = new SqlParameter("@fromDate", SqlDbType.DateTime);
                objParam[2].Value = fromDate.Date.ToUniversalTime();

                objParam[3] = new SqlParameter("@toDate", SqlDbType.DateTime);
                objParam[3].Value = toDate.Date.ToUniversalTime();

                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetFulfilementCustomerData", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public string GetOrderImage(string orderNumber, string username)
        {
            var result = string.Empty;
            try
            {
                SqlParameter[] objParam = new SqlParameter[5];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_BY_OrderNumber";

                objParam[1] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = Guid.NewGuid();

                objParam[2] = new SqlParameter("@orderNumber", SqlDbType.VarChar, 100);
                objParam[2].Value = orderNumber;

                objParam[3] = new SqlParameter("@username", SqlDbType.VarChar, 100);
                objParam[3].Value = username;

                objParam[4] = new SqlParameter("@result", SqlDbType.VarChar, 2000);
                objParam[4].Value = ParameterDirection.Output;


                var data = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateDeleteImagesByOrderId", objParam).Tables[0];
                if(data.Rows.Count > 0)
                {
                    result = data.Rows[0]["Image"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public DataTable GetOrderCustomerDetail(string orderNumber,string username)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[3];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ORDER_CUSTOMER_DETAILS";

                objParam[1] = new SqlParameter("@OrderNumber", SqlDbType.VarChar, 100);
                objParam[1].Value = orderNumber;

                objParam[2] = new SqlParameter("@username", SqlDbType.VarChar, 100);
                objParam[2].Value = username;


                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetFulfilementCustomerData", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }
    }
}