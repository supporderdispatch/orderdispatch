﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ODM.FulfilmentCustomer.WebApp.Models
{
    public class OrderDetails
    {
        public string NumOrderId { get; set; }
        public string FullName { get; set; }
        public bool PickingBatch { get; set; }
        public string PostalServiceName { get; set; }
        public string TrackingNumber { get; set; }
        public bool Processed { get; set; }
        public DateTime? ProcessedDateTime { get; set; }
        public bool ImageExists { get; set; }
        public string ReferenceNum { get; set; }
    }
}