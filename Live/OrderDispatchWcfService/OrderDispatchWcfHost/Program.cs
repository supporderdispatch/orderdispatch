﻿using System;
using System.ServiceModel;
namespace OrderDispatchWcfHost
{
    class Program
    {
        static void Main()
        {
            try
            {
                using (ServiceHost host = new ServiceHost(typeof(OrderDispatchWcfService.OrderDispatch)))
                {
                    host.Open();
                    Console.WriteLine("Service started @ " + DateTime.Now.ToString());
                    //OrderDispatchWcfService.OrderDispatch obj = new OrderDispatchWcfService.OrderDispatch();
                    //string strdata = obj.SyncData();
                    //Console.WriteLine(strdata);

                    OrderDispatchWcfService.OrderDispatch disp = new OrderDispatchWcfService.OrderDispatch();
                    disp.StartServiceOnLoad();
                    //disp.SyncData();
                    //disp.AutoSendEmailsForProcessedOrders();
                    Console.ReadLine();
                }
            }
            catch(Exception ex)
            {
                string sterError = ex.Message;
            }
        }
    }
}
