﻿using System;
using System.Configuration;
namespace OrderDispatchWcfService
{
    public class MakeConnection
    {
        public static string GetConnection()
        {
            string strConnection = "";
            try
            {
                strConnection = ConfigurationManager.ConnectionStrings["OrderDispatchConnection"].ToString();
                //strConnection = @"Data Source=VAIO\SQLEXPRESS;Initial Catalog=OrderDispatch;Integrated Security=True";
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return strConnection;
        }
    }
}
