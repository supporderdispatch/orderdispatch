﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderDispatchWcfService;

namespace OrderDispatchWcfService
{
    public class clsPostalServices
    {
        public System.Guid id { get; set; }
        public Nullable<bool> hasMappedShippingService { get; set; }
        public List<Channel> Channels { get; set; }
        public List<ShippingService> ShippingServices { get; set; }
        public string PostalServiceName { get; set; }
        public string PostalServiceTag { get; set; }
        public string ServiceCountry { get; set; }
        public string PostalServiceCode { get; set; }
        public string Vendor { get; set; }
        public string PrintModule { get; set; }
        public string PrintModuleTitle { get; set; }
        public Nullable<System.Guid> pkPostalServiceId { get; set; }
        public Nullable<bool> TrackingNumberRequired { get; set; }
        public Nullable<bool> WeightRequired { get; set; }
        public Nullable<bool> IgnorePackagingGroup { get; set; }
        public string fkShippingAPIConfigId { get; set; }
        public string IntegratedServiceId { get; set; }
    }

    public class clsCancelledAndProcessedOrders
    {
        public Guid pkOrderId { get; set; }
    }
}
