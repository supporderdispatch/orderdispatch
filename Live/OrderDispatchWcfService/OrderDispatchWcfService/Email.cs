﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Drawing;
using ODM.Data.Util;
using System.Drawing.Imaging;


namespace OrderDispatchWcfService
{
    class Email
    {
        public Guid GuidForLog = new Guid();
        public Guid OrderIdToLog = new Guid();
        PDFLib objPdfLib = new PDFLib();
        // public static System.Collections.Generic.List<System.Drawing.Image> lstimgInvoice = new System.Collections.Generic.List<System.Drawing.Image>();
        DataTable objDtCondition = new DataTable();
        DataTable objDtTemplateDetails = new DataTable();
        //    StoreDataInLocaldb objdb = new StoreDataInLocaldb();
        DataView dvEmailAccount = new DataView();
        string strDataColumns = "", strDataColumnsForSubject = "", strDataColumnsForDatabaseUse = "";
        string[] strArrColumnsInSubject = new string[] { };
        DataView DvExcludeMailSentOrders = new DataView();

        // conversion of base64 string to image
        public static Image Base64StringToImage(string strBase64String)
        {
            //edit string to check response
            //strBase64String = "";

            byte[] bytImageBytes = Convert.FromBase64String(strBase64String);
            MemoryStream ms = new MemoryStream(bytImageBytes, 0, bytImageBytes.Length);
            ms.Write(bytImageBytes, 0, bytImageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }



        public string SendEmailForDispatchedOrders(List<SendEmailEntity> lstEmails, ref DataTable objDtAllDataFromPendingMailScreen)
        {

            string strIdsToDeleteOrUpdate = "";
            string strNumOrderIds = "";
            string strOrderIds = "";
            string strText = "";
            string strSubject = "";
            DataView dvFilterCondition = new DataView();
            SendEmailEntity objEmail = new SendEmailEntity();
            string strEmailTo = string.Empty, strInvoicePath = string.Empty, strOrderNumberId = string.Empty, strOrderID = string.Empty;
            int intNextCountToSentMailIfError = 0;
        ExecuteAgainIfError:
            try
            {
                for (int i = intNextCountToSentMailIfError; i < lstEmails.Count; i++)
                {
                    Guid ConditionID = new Guid();
                    Guid TemplateID = new Guid();
                    string strEmailAccID = "", strBase64String = "";
                    Boolean blIsPDF = false, blIsHtml = false, blIsEnabled = false, blIsImage = false;
                    DataTable objDtDataToShowInTemplate = new DataTable();
                    if (objDtCondition.Rows.Count == 0)
                    {
                        objDtCondition = Utill.GetEmailConditions();
                    }

                    string strSource = lstEmails[i].Source;
                    string strSubSource = lstEmails[i].SubSource;
                    dvFilterCondition = new DataView(objDtCondition);

                    ConditionID = GetConditionIDByConditionText(strSource, strSubSource, dvFilterCondition);

                ExecuteAgainIfTemplateIsDisabled:
                    objDtTemplateDetails = Utill.GetTemplateDetailsByTemplateId(TemplateID, ConditionID);
                    if (objDtTemplateDetails.Rows.Count > 0)
                    {
                        blIsEnabled = Convert.ToBoolean(objDtTemplateDetails.Rows[0]["IsEnabled"]);
                    }


                    /*Checking if this template is enabled or not, if not then setting ConditionID to default value of guid,
                      and executing GetTemplateDetailsByTemplateId again to get default template and settings
                     */
                    if (blIsEnabled)
                    {
                        strSubject = Convert.ToString(objDtTemplateDetails.Rows[0]["Subject"]);
                        strText = Convert.ToString(objDtTemplateDetails.Rows[0]["Text"]);
                        blIsPDF = Convert.ToBoolean(objDtTemplateDetails.Rows[0]["IsPDF"]);
                        blIsImage = Convert.ToBoolean(objDtTemplateDetails.Rows[0]["IsImage"]);
                        blIsHtml = Convert.ToBoolean(objDtTemplateDetails.Rows[0]["IsHtml"]);
                        strEmailAccID = Convert.ToString(objDtTemplateDetails.Rows[0]["EmailAccId"]);

                        // string strStringToReplace = strText.ToUpper();
                        //string strExactColumsToReplace = strText;
                        GetColumnNamesToGetDataFromDB(strText, ref strDataColumnsForDatabaseUse, ref strDataColumnsForSubject, ref strDataColumns, ref strArrColumnsInSubject, false);

                        //strStringToReplace = strSubject.ToUpper();
                        //strExactColumsToReplace = strSubject;
                        //strDataColumnsForDatabaseUse = "A.XYZ AS [A.XYZ]," + strDataColumnsForDatabaseUse + ",A.ABC AS [A.ABC]";
                        GetColumnNamesToGetDataFromDB(strSubject, ref strDataColumnsForDatabaseUse, ref strDataColumnsForSubject, ref strDataColumns, ref strArrColumnsInSubject, true);
                    ExecuteIf207Error:
                        try
                        {
                            objDtDataToShowInTemplate = GetDataToShowInTemplate(strDataColumnsForDatabaseUse, lstEmails[i].OrderID);
                        }
                        catch (SqlException sqlEx)
                        {
                            /*If 207 error(means invalid column call) then RemoveInvalidColumnIfExistsInString method is
                             *removing that column from list of text and database column string as well.
                             *Then executing again with valid columns
                             */
                            if (sqlEx.Number == 207)
                            {
                                // strDataColumnsForSubject = "[{Adddress.XYZ}]," + strDataColumnsForSubject + ",[{Adddress.ABC}]";
                                Utill.RemoveInvalidColumnIfExistsInString(ref strDataColumns, ref strDataColumnsForDatabaseUse, ref strArrColumnsInSubject, sqlEx.Message);
                                Utill.RemoveInvalidColumnIfExistsInString(ref strDataColumnsForSubject, ref strDataColumnsForDatabaseUse, ref strArrColumnsInSubject, sqlEx.Message);
                                goto ExecuteIf207Error;
                            }
                        }
                        strText = ReplaceColumnWithData(strDataColumns, strDataColumnsForSubject, strText, objDtDataToShowInTemplate, false, strArrColumnsInSubject);
                        strSubject = ReplaceColumnWithData(strDataColumns, strDataColumnsForSubject, strSubject, objDtDataToShowInTemplate, true, strArrColumnsInSubject);

                        dvEmailAccount = new DataView(Utill.GetEmailAccounts());
                        dvEmailAccount.RowFilter = "ID='" + strEmailAccID + "'";
                        intNextCountToSentMailIfError++;
                        strEmailTo = lstEmails[i].EmailID;
                        strOrderNumberId = lstEmails[i].NumberOrderID;
                        Guid OrderID = new Guid(lstEmails[i].OrderID);

                        MailMessage mail = new MailMessage();

                        //SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);
                        //mail.From = new MailAddress("supporderdispatch@gmail.com");

                        SmtpClient SmtpServer = new SmtpClient(Convert.ToString(dvEmailAccount.ToTable().Rows[0]["SMTP_Server"]), Convert.ToInt32(dvEmailAccount.ToTable().Rows[0]["Port"]));
                        //  mail.Sender = new MailAddress(Convert.ToString(dvEmailAccount.ToTable().Rows[0]["UserName"]));
                        mail.Sender = new MailAddress("customerservice@etwist.co.uk");
                        //mail.From = Convert.ToString(dvEmailAccount.ToTable().Rows[0]["ReplyToEmail"]);
                        //mail.From = new MailAddress("creativecoder4u@gmail.com");
                        mail.From = new MailAddress(Convert.ToString(dvEmailAccount.ToTable().Rows[0]["ReplyToEmail"]), Convert.ToString(dvEmailAccount.ToTable().Rows[0]["FromName"]));
                        //mail.Subject = lstEmails[i].Subject;
                        //mail.Body = lstEmails[i].MailTemplate;

                        mail.To.Add(strEmailTo);
                        mail.Subject = strSubject;
                        mail.Body = strText;
                        //  mail.ReplyToList.Add(new MailAddress("test@dispostable.com"));
                        mail.ReplyToList.Add(new MailAddress(Convert.ToString(dvEmailAccount.ToTable().Rows[0]["ReplyToEmail"])));
                        if (blIsPDF)
                        {
                            strInvoicePath = System.IO.Path.GetTempPath() + strOrderNumberId + ".pdf";
                            strOrderID = lstEmails[i].OrderID;
                            if (!File.Exists(strInvoicePath))
                            {
                                strInvoicePath = objPdfLib.CreateInvoiceDetails(strOrderID, false, strOrderNumberId);
                            }
                            Attachment attachment;
                            attachment = new Attachment(strInvoicePath);
                            mail.Attachments.Add(attachment);
                        }
                        //for attaching order image in email as attachment
                        if (blIsImage)
                        {

                            DataTable objDt = new DataTable();
                            objDt = Utill.GetAddUpdateDeleteImageByOrderId("GET", OrderID);
                            if (objDt.Rows.Count > 0)
                            {
                                strBase64String = objDt.Rows[0]["Image"].ToString(); ;
                            }
                            // converting base64string to image using method: GlobalVariablesAndMethods.Base64StringToImage
                            if (!string.IsNullOrEmpty(strBase64String))
                            {

                                Image image = Utill.Base64StringToImage(strBase64String);
                                MemoryStream ms = new MemoryStream();
                                image.Save(ms, ImageFormat.Jpeg);
                                ms.Position = 0;
                                mail.Attachments.Add(new Attachment(ms, "OrderedPackage.jpg"));
                            }
                            else
                            {
                                Utill.LogHistory("No Image to attach for Order Num Id :- " + strOrderNumberId, Guid.NewGuid());
                            }
                        }
                        mail.IsBodyHtml = blIsHtml;
                        SmtpServer.Credentials = new System.Net.NetworkCredential(Convert.ToString(dvEmailAccount.ToTable().Rows[0]["UserName"]), Convert.ToString(dvEmailAccount.ToTable().Rows[0]["Password"]));
                        //SmtpServer.Credentials = new System.Net.NetworkCredential("supporderdispatch@gmail.com", "OrderDispatch@2018");
                        SmtpServer.EnableSsl = Convert.ToBoolean(dvEmailAccount.ToTable().Rows[0]["IsSSL"]);

                        DataTable objDtToCheckIfMailIsSentAlready = new DataTable();
                        objDtToCheckIfMailIsSentAlready = GetDataForPendingEmailsBasedOnOrderId(strOrderID);
                        if (objDtToCheckIfMailIsSentAlready.Rows.Count > 0)
                        {
                            if (!Convert.ToBoolean(Convert.ToString(objDtToCheckIfMailIsSentAlready.Rows[0]["IsMailSent"])))
                            {
                                SmtpServer.Send(mail);
                                strIdsToDeleteOrUpdate = lstEmails[i].ID;
                                strNumOrderIds = lstEmails[i].NumberOrderID;
                                strOrderIds = lstEmails[i].OrderID;
                                intNextCountToSentMailIfError++;
                                objEmail.IDs = strIdsToDeleteOrUpdate;
                                objEmail.NumOrderIDs = strNumOrderIds;
                                objEmail.OrderIDs = strOrderIds;
                                objEmail.SentText = strText;
                                objEmail.SentSubject = strSubject;
                                objEmail.Date = DateTime.Now;
                                objEmail.Mode = "SENT";
                                string strResult = InsertUpdateDeleteDataForPendingEmail(objEmail);
                                if (strResult == "UPDATED")
                                {
                                    Utill.LogHistory("Mail sent successfullt for OrderId: " + strNumOrderIds, Guid.NewGuid());
                                    //DvExcludeMailSentOrders = new DataView(objDtAllDataFromPendingMailScreen);
                                    //DvExcludeMailSentOrders.RowFilter = "ID <> '" + strIdsToDeleteOrUpdate + "'";
                                    //objDtAllDataFromPendingMailScreen = DvExcludeMailSentOrders.ToTable();
                                }
                            }
                            else
                            {
                                Utill.LogHistory("Mail is sent Already for this OrderID: " + strOrderID, Guid.NewGuid());
                            }
                        }
                        



                    }
                    else if (ConditionID.ToString() != "00000000-0000-0000-0000-000000000000")
                    {
                        ConditionID = new Guid();//Setting condition id to default
                        goto ExecuteAgainIfTemplateIsDisabled;
                    }
                    //End of enabled check
                }
                return "Mail(s) Sent";
            }
            catch (SmtpException smtpEx)
            {
                //Log the exception and again execute to send rest of the email
                Utill.LogHistory("SmtpException Error occrred while sending mail to Email:-" + strEmailTo + " Order Num Id :-" + strOrderNumberId + "&&Error :- " + smtpEx.Message, Guid.NewGuid());
                intNextCountToSentMailIfError++;
                goto ExecuteAgainIfError;
            }
            catch (AuthenticationException AuthEx)
            {
                Utill.LogHistory("AuthenticationException Error occrred while sending mail to Email:-" + strEmailTo + " Order Num Id :-" + strOrderNumberId + "&&Error :- " + AuthEx.Message, Guid.NewGuid());
                intNextCountToSentMailIfError++;
                goto ExecuteAgainIfError;
            }
            catch (Exception ex)
            {
                //We may get error at the last count of email
                if (intNextCountToSentMailIfError == lstEmails.Count)
                {
                    //To update the flag of sent email in tblPending email
                    if (strIdsToDeleteOrUpdate != "" && strNumOrderIds != "")
                    {
                        objEmail.IDs = strIdsToDeleteOrUpdate;
                        objEmail.NumOrderIDs = strNumOrderIds;
                        objEmail.Date = DateTime.Now;
                        objEmail.SentText = strText;
                        objEmail.SentSubject = strSubject;
                        objEmail.Mode = "SENT";
                        string strResult = InsertUpdateDeleteDataForPendingEmail(objEmail);
                    }
                    //End of flag updation
                }
                //End of check last count of email

                //Log the exception and again execute to send rest of the email
                Utill.LogHistory("Error occrred while sending mail to Email:-" + strEmailTo + " Order Num Id :-" + strOrderNumberId + "&&Error :- " + ex.Message, Guid.NewGuid());
                goto ExecuteAgainIfError;
            }
        }

        /// <summary>
        /// To get the condition id of matched condition
        /// </summary>
        /// <param name="strSource">Source value of order</param>
        /// <param name="strSubSource">SubSource value of order</param>
        /// <param name="dvFilterCondition">Dataview having dataset/list of all conditions stored in db</param>
        /// <returns></returns>
        public Guid GetConditionIDByConditionText(string strSource, string strSubSource, DataView dvFilterCondition)
        {
            Guid ConditionID = new Guid();
            try
            {
                //Making a search string to find/filter matching conditions
                string strSearchText = strSubSource != "" ? "Text like '%" + strSource + "%' or Text like '%" + strSubSource + "%'" : "Text like '%" + strSource + "%'";
                dvFilterCondition.RowFilter = strSearchText;

                foreach (DataRowView dr in dvFilterCondition)
                {
                    string strData = Convert.ToString(dr["Text"]);//Text of condition
                    string strID = Convert.ToString(dr["ID"]);//Id of that condition
                    strData = Regex.Replace(strData, @"\""+", "'").Trim().ToUpper();
                    if (strSubSource != "")
                    {
                        if (strData.Contains(" AND ") && !strData.Contains(" OR "))
                        {
                            if (strData == "SOURCE='" + strSource.ToUpper() + "' AND SUBSOURCE='" + strSubSource.ToUpper() + "'")
                            {
                                ConditionID = new Guid(strID);
                                break;
                            }
                        }
                        else if (strData.Contains(" OR ") && !strData.Contains(" AND "))
                        {
                            if (strData.Contains("SOURCE='" + strSource.ToUpper() + "'") || strData.Contains("SUBSOURCE='" + strSubSource.ToUpper() + "'"))
                            {
                                ConditionID = new Guid(strID);
                                break;
                            }
                        }
                        else if (!strData.Contains(" AND ") && !strData.Contains(" OR "))
                        {
                            if (Regex.Replace(strData, " ", "").Contains("SOURCE='" + strSource.ToUpper() + "'") || Regex.Replace(strData, " ", "").Contains("SUBSOURCE='" + strSubSource.ToUpper() + "'"))
                            {
                                ConditionID = new Guid(strID);
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (strData.Contains(" OR "))
                        {
                            if (strData.Contains("SOURCE='" + strSource.ToUpper() + "'"))
                            {
                                ConditionID = new Guid(strID);
                                break;
                            }
                        }
                        if (!strData.Contains(" OR ") && !strData.Contains(" AND "))
                        {
                            if (Regex.Replace(strData, " ", "") == "SOURCE='" + strSource.ToUpper() + "'")
                            {
                                ConditionID = new Guid(strID);
                                break;
                            }
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return ConditionID;
        }
        public bool ValidateSMTP(string strHostName, int intPortNum)
        {
            bool blIsValidSMTP = false;
            try
            {
                using (var client = new TcpClient())
                {
                    client.Connect(strHostName, intPortNum);
                    if (client.Connected)
                    {
                        blIsValidSMTP = true;
                    }
                }
            }
            catch
            {
                //GlobalVariablesAndMethods.LogHistory("Error occurred while validating SMTP&&" + ex.Message);
            }
            return blIsValidSMTP;
        }

        /// <summary>
        /// Get the data for selected columns
        /// </summary>
        /// <param name="strColumnList">List of columns those we want to select from db</param>
        /// <param name="strOrderId">Order id for which we want to select record</param>
        /// <returns>Record set of columns we sent to get execute</returns>
        public DataTable GetDataToShowInTemplate(string strColumnList, string strOrderId)
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@ColumnsToSelect", SqlDbType.VarChar, Int32.MaxValue);
                objParam[0].Value = strColumnList;

                objParam[1] = new SqlParameter("@OrderID", SqlDbType.VarChar, 40);
                objParam[1].Value = strOrderId;

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDataToShowInEmailTemplate", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                //GlobalVariablesAndMethods.LogHistory("Error occurred while getting data from db &&" + ex.Message);
                throw;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }

        /// <summary>
        /// Get the comma separated list of columns those we want to select from db to replace those in text of subject,mail body etc
        /// </summary>  
        /// <param name="strText">Text in which we want to find and replace the column names with data</param>
        /// <param name="strDataColumnsForDatabaseUse">Comma separated string of columns those we want to get from db</param>
        /// <param name="strDataColumnsForSubject">Comma separated string of columns used in subject text those we want to get from db</param>
        /// <param name="strDataColumns">Exact column names exists in string so that we could find those and replace</param>
        /// <param name="strArrColumnsInSubject">Array of columns exists in subject text</param>
        /// <param name="blIsSubjectCall">To determine whether its a call for subject or mail body</param>
        public void GetColumnNamesToGetDataFromDB(string strText, ref string strDataColumnsForDatabaseUse, ref string strDataColumnsForSubject, ref string strDataColumns, ref string[] strArrColumnsInSubject, bool blIsSubjectCall)
        {
            string strStringToReplace = strText.ToUpper();
            string strExactColumsToReplace = strText;
            //Replacing table names with alias, because we are using those in database.
            strStringToReplace = Regex.Replace(strStringToReplace, "\\[{ORDERITEM.", "[{OI.");
            strStringToReplace = Regex.Replace(strStringToReplace, "\\[{ORDER.", "[{O.");
            strStringToReplace = Regex.Replace(strStringToReplace, "\\[{ADDRESS.", "[{A.");
            //End of replacing table names with alias

            List<string> lstSubjectColumns = new List<string>();
        ExecuteAgainIfMoreCondition:
            if (strStringToReplace.Contains("[{") && strStringToReplace.Contains("}]"))
            {
                //Getting substring and concating strings for the columns we want to get from database
                string strSubStringWithUpperCase = Utill.GetSubString(strStringToReplace, "[{", "}]");
                strDataColumnsForDatabaseUse = strDataColumnsForDatabaseUse == "" ? strSubStringWithUpperCase + " AS [" + strSubStringWithUpperCase + "]"
                    : (!strDataColumnsForDatabaseUse.Contains(strSubStringWithUpperCase)) ? strDataColumnsForDatabaseUse + "," + strSubStringWithUpperCase + " AS [" + strSubStringWithUpperCase + "]"
                    : strDataColumnsForDatabaseUse;
                //End of concating columns for database

                //Getting substring and concating strings for the columns exists in string to send email, so that we could find those to replace
                string strSubStringWithExactCase = strExactColumsToReplace.Substring(strExactColumsToReplace.IndexOf("[{"), strExactColumsToReplace.IndexOf("}]") - strExactColumsToReplace.IndexOf("[{") + 2);
                //End of concating strings to replace for mail

                //IF its a call for subject text for mail, then getting only columns used in subject
                if (blIsSubjectCall)
                {
                    strDataColumnsForSubject = strDataColumnsForSubject == "" ? strSubStringWithExactCase
                    : (!strDataColumnsForSubject.Contains(strSubStringWithExactCase)) ? strDataColumnsForSubject + "," + strSubStringWithExactCase
                    : strDataColumnsForSubject;
                    lstSubjectColumns.Add(strSubStringWithUpperCase);
                }
                else
                {
                    strDataColumns = strDataColumns == "" ? strSubStringWithExactCase
                                        : (!strDataColumns.Contains(strSubStringWithExactCase)) ? strDataColumns + "," + strSubStringWithExactCase
                                        : strDataColumns;
                }
                //End of subject call test

                //Overwriting strSubStringWithUpperCase to replace that from temp string "strStringToReplace" to get next column existence
                strSubStringWithUpperCase = strStringToReplace.Substring(strStringToReplace.IndexOf("[{"), strStringToReplace.IndexOf("}]") - strStringToReplace.IndexOf("[{") + 2);
                //End of getting substring to replace temp string "strStringToReplace"

                //Replacing temp string "strStringToReplace" so that we could get next existence of column
                strStringToReplace = strStringToReplace.Replace(strSubStringWithUpperCase, "");

                /*This is the copy of exact string we are going to send as mail, we are using that
                 *so that we could get exact name of columns exists in a string.
                 */
                strExactColumsToReplace = strExactColumsToReplace.Replace(strSubStringWithExactCase, "");
                goto ExecuteAgainIfMoreCondition;
            }

            /*Converting column list to array so that we could get required columns from dataset returned by database
             * check use of that array in ReplaceColumnWithData method
            */
            strArrColumnsInSubject = lstSubjectColumns.ToArray();

            //End of to Get column names from template
        }

        public string ReplaceColumnWithData(string strDataColumns, string strDataColumnsForSubject, string strTextToReplace, DataTable objDt, bool blIsSubjectCall, string[] strArrColumnsInSubject)
        {
            string strResult = strTextToReplace;

            //To extract the columns used in subject only so that we could replace those only
            if (blIsSubjectCall)
            {
                DataView dv = new DataView(objDt);
                objDt = dv.ToTable(true, strArrColumnsInSubject);
                strDataColumns = strDataColumnsForSubject;
            }
            //End to extract subject columns

            if (strDataColumns.Contains(","))
            {
                string[] ArrColumns = strDataColumns.Split(',');
                for (int x = 0; x < ArrColumns.Length; x++)
                {
                    strResult = strResult.Replace(ArrColumns[x], Convert.ToString(objDt.Rows[0][x]).Trim());
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(strDataColumns))
                {
                    strResult = strResult.Replace(strDataColumns, Convert.ToString(objDt.Rows[0][0]).Trim());
                }
            }
            return strResult;
        }

        #region "Add/Update/Delete data under pending email table for send email purpose"
        /// <summary>
        /// Add/Update/Delete data under pending email table for send email purpose
        /// </summary>
        /// <param name="objSendEmail">Object of class SendEmailEntity for mail purpose</param>
        /// <returns>
        /// INSERTED:- When data saved after processing for send email purpose.
        /// UPDATED:- When mail have been sent to user.
        /// DELETED:- When mail have been deleted from pending email list
        /// <returns>Execution result</returns>
        public string InsertUpdateDeleteDataForPendingEmail(SendEmailEntity objSendEmail)
        {
            string strLogText = "Making database call for details of pending email screen";
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                Guid OrderID = new Guid();
                if (objSendEmail.Mode == "INSERT")
                {
                    OrderID = new Guid(objSendEmail.OrderID);
                }
                SqlParameter[] objParam = new SqlParameter[10];

                objParam[0] = new SqlParameter("@IDs", SqlDbType.VarChar, int.MaxValue);
                objParam[0].Value = objSendEmail.IDs;

                objParam[1] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = OrderID;

                objParam[2] = new SqlParameter("@NumOrderId", SqlDbType.Int);
                objParam[2].Value = Convert.ToInt32(objSendEmail.NumberOrderID);

                objParam[3] = new SqlParameter("@Date", SqlDbType.DateTime);
                objParam[3].Value = objSendEmail.Date;

                objParam[4] = new SqlParameter("@FullName", SqlDbType.VarChar, 50);
                objParam[4].Value = objSendEmail.FullName;

                objParam[5] = new SqlParameter("@EmailAddress", SqlDbType.VarChar, 50);
                objParam[5].Value = objSendEmail.EmailID;

                objParam[6] = new SqlParameter("@SentMailBody", SqlDbType.VarChar, Int32.MaxValue);
                objParam[6].Value = objSendEmail.SentText;

                objParam[7] = new SqlParameter("@SentSubject", SqlDbType.VarChar, Int32.MaxValue);
                objParam[7].Value = objSendEmail.SentSubject;


                /* Values for Mode parameter
                 * INSERT:- When we want to store data in db for send email purpose
                 * SENT:- When we are sending email to user
                 * DELETE:- When we want to delete emails from pending email list
                 */
                objParam[8] = new SqlParameter("@Mode", SqlDbType.VarChar, 20);
                objParam[8].Value = objSendEmail.Mode;

                objParam[9] = new SqlParameter("@Result", SqlDbType.VarChar, int.MaxValue);
                objParam[9].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeletePendingEmail", objParam);
                string strResult = Convert.ToString(objParam[9].Value);

                //Loggin pending email status in db
                switch (strResult)
                {
                    //Means we have stored the details for mail purpose
                    case "INSERTED":
                        {
                            strLogText = strLogText + "&&Email Details save in database for NumOrderId:- " + objSendEmail.NumberOrderID;
                            Utill.LogHistory(strLogText, Guid.NewGuid());
                            break;
                        }

                    case "UPDATED":
                    case "DELETED":
                        {
                            /*these cases are for sent mail for delete mails
                             Logging details for each order ids for which we sent or deleted mail(s)
                             */
                            string[] strDeletedOrderIdsMail = objSendEmail.OrderIDs.Split(',');
                            for (int i = 0; i < strDeletedOrderIdsMail.Length; i++)
                            {
                                Guid DeletedOrderID = new Guid(strDeletedOrderIdsMail[i]);
                                GuidForLog = Guid.NewGuid();
                                OrderIdToLog = DeletedOrderID;
                                string strLogWithResult = strResult == "UPDATED" ? string.Format("Email sent for order id {0}", strDeletedOrderIdsMail[i]) :
                                    string.Format("Order id {0} deleted from pending email list", strDeletedOrderIdsMail[i]);
                                Utill.LogHistory(strLogWithResult, Guid.NewGuid());
                            }
                            break;
                        }
                }
                //End of logging email status in db
                return strResult;
            }
            catch (Exception ex)
            {
                Utill.LogHistory(strLogText + "&&Exception while save email details:-" + ex.Message, Guid.NewGuid());
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion
        #region Check If Mail is sent Already or not
        public DataTable GetDataForPendingEmailsBasedOnOrderId(string strOrderId)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            DataTable objDt = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[0].Value = new Guid(strOrderId);

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDataForPendingEmailsBasedOnOrderId", objParam).Tables[0];
            }
            catch (Exception ex)
            {

                Utill.LogHistory(string.Format("Exception occurred while checking data if mail is already sent or not  for {0}&&Exception:- {1}", strOrderId, ex.Message), Guid.NewGuid());
            }
            finally { connection.Close(); }
            return objDt;
        }
        #endregion
    }
}
