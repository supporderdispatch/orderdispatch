﻿using GhostscriptSharp;
using GhostscriptSharp.Settings;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Microsoft.ApplicationBlocks.Data;
using OrderDispatchWcfService;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;

namespace ODM.Data.Util
{
    public class PDFLib
    {
        public static System.Collections.Generic.List<System.Drawing.Image> lstimgInvoice = new System.Collections.Generic.List<System.Drawing.Image>();
        public static System.Drawing.Image imgInvoice { get; set; }
      //  GlobalVariablesAndMethods objGlobalVar = new GlobalVariablesAndMethods();
       // StoreDataInLocaldb objdb = new StoreDataInLocaldb();
       
        public string CreateInvoiceDetails(string OrderID, bool blIsPrintCall, string strNumberOrderID = "")
        {
            lstimgInvoice = new System.Collections.Generic.List<System.Drawing.Image>();
            double Total = 0.00, UnitCost = 0.00, SubTotal = 0.00;
            string strSubTotal = "", strVat = "", strPostage = "", strTotal = "", strOrderID = "", strDespatchByDate = "";
            string strCompany = "", strFullName = "", strAddress1 = "", strTown = "", strRegion = "", strPostCode = "", strCountry = "";
            try
            {
                string pdffilename = System.IO.Path.GetTempPath() + strNumberOrderID + ".pdf";
                string imageFilename = System.IO.Path.GetTempPath();
                if (!File.Exists(pdffilename))
                {
                    DataTable ObjDt = new DataTable();
                    Guid GuOrderID = new Guid(OrderID);
                    SqlConnection Conn = new SqlConnection();
                    Conn.ConnectionString = MakeConnection.GetConnection();
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("@OrderID", SqlDbType.UniqueIdentifier);
                    param[0].Value = GuOrderID;

                    ObjDt = SqlHelper.ExecuteDataset(Conn, CommandType.StoredProcedure, "GetDetailsForInvoice", param).Tables[0];

                    StringBuilder strMainDataRow = new StringBuilder();
                    if (ObjDt.Rows.Count > 0)
                    {
                        strSubTotal = Convert.ToString(ObjDt.Rows[0]["Subtotal"]);
                        strVat = Convert.ToString(ObjDt.Rows[0]["VAT"]);
                        strPostage = Convert.ToString(ObjDt.Rows[0]["PostageCost"]);
                        strTotal = Convert.ToString(ObjDt.Rows[0]["Total"]);
                        strOrderID = Convert.ToString(ObjDt.Rows[0]["NumOrderId"]);

                        strCompany = Convert.ToString(ObjDt.Rows[0]["Company"]);
                        strFullName = Convert.ToString(ObjDt.Rows[0]["FullName"]);
                        strAddress1 = Convert.ToString(ObjDt.Rows[0]["Address1"]);
                        strTown = Convert.ToString(ObjDt.Rows[0]["Town"]);
                        strRegion = Convert.ToString(ObjDt.Rows[0]["Region"]);
                        strPostCode = Convert.ToString(ObjDt.Rows[0]["PostCode"]);
                        strCountry = Convert.ToString(ObjDt.Rows[0]["Country"]);

                        strDespatchByDate = Convert.ToDateTime(ObjDt.Rows[0]["DespatchByDate"]).ToString("yyyy MMM dd");

                        for (int i = 0; i < ObjDt.Rows.Count; i++)
                        {
                            if (ObjDt.Rows.Count > 1 && ObjDt.Rows.Count != i + 1)
                            {
                                strMainDataRow.Append("<tr>");
                                strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["SKU"]) + "</td>");
                                strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["Title"]) + "</td>");
                                strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["Quantity"]) + "</td>");
                                if (Convert.ToString(ObjDt.Rows[i]["UnitCost"]) == "0.00")
                                {
                                    UnitCost = 1.00;
                                    strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(string.Format("{0:0.00}", UnitCost)) + "</td>");
                                }
                                else
                                {
                                    strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["UnitCost"]) + "</td>");
                                }
                                //strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["UnitCost"]) + "</td>");
                                strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["TaxRate"]) + "</td>");
                                strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["Tax"]) + "</td>");
                                strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["Cost"]) + "</td>");
                                if (Convert.ToString(ObjDt.Rows[i]["CostIncTax"]) == "0.00")
                                {
                                    strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(string.Format("{0:0.00}", UnitCost * Convert.ToDouble(ObjDt.Rows[i]["Quantity"]))) + "</td>");
                                    if (strSubTotal == "0.00")
                                    {
                                        if (!(SubTotal > 0.00))
                                        {
                                            SubTotal = Convert.ToDouble(strSubTotal);
                                        }
                                        SubTotal = SubTotal + (UnitCost * Convert.ToDouble(ObjDt.Rows[i]["Quantity"]));
                                        //strSubTotal = Convert.ToString(string.Format("{0:0.00}", SubTotal));
                                        Total = SubTotal;
                                        strTotal = Convert.ToString(string.Format("{0:0.00}", Total));
                                    }
                                }
                                else
                                {
                                    strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["CostIncTax"]) + "</td>");
                                }

                                //  strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["CostIncTax"]) + "</td>");
                                strMainDataRow.Append("</tr>");
                            }
                            else
                            {
                                strMainDataRow.Append("<tr>");
                                strMainDataRow.Append("<td>" + Convert.ToString(ObjDt.Rows[i]["SKU"]) + "</td>");
                                strMainDataRow.Append("<td>" + Convert.ToString(ObjDt.Rows[i]["Title"]) + "</td>");
                                strMainDataRow.Append("<td>" + Convert.ToString(ObjDt.Rows[i]["Quantity"]) + "</td>");
                                if (Convert.ToString(ObjDt.Rows[i]["UnitCost"]) == "0.00")
                                {
                                    UnitCost = 1.00;
                                    strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(string.Format("{0:0.00}", UnitCost)) + "</td>");

                                }
                                else
                                {
                                    strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["UnitCost"]) + "</td>");
                                }

                                //strMainDataRow.Append("<td>" + Convert.ToString(ObjDt.Rows[i]["UnitCost"]) + "</td>");
                                strMainDataRow.Append("<td>" + Convert.ToString(ObjDt.Rows[i]["TaxRate"]) + "</td>");
                                strMainDataRow.Append("<td>" + Convert.ToString(ObjDt.Rows[i]["Tax"]) + "</td>");
                                strMainDataRow.Append("<td>" + Convert.ToString(ObjDt.Rows[i]["Cost"]) + "</td>");
                                if (Convert.ToString(ObjDt.Rows[i]["CostIncTax"]) == "0.00")
                                {
                                    strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(string.Format("{0:0.00}", UnitCost * Convert.ToDouble(ObjDt.Rows[i]["Quantity"]))) + "</td>");
                                    if (strSubTotal == "0.00")
                                    {
                                        if (!(SubTotal > 0.00))
                                        {
                                            SubTotal = Convert.ToDouble(strSubTotal);
                                        }

                                        SubTotal = SubTotal + (UnitCost * Convert.ToDouble(ObjDt.Rows[i]["Quantity"]));
                                        strSubTotal = Convert.ToString(string.Format("{0:0.00}", SubTotal));
                                        Total = SubTotal;
                                        strTotal = Convert.ToString(string.Format("{0:0.00}", Total));
                                    }
                                }
                                else
                                {
                                    strMainDataRow.Append("<td style='border-bottom: 1px solid #000;'>" + Convert.ToString(ObjDt.Rows[i]["CostIncTax"]) + "</td>");
                                }


                                //strMainDataRow.Append("<td>" + Convert.ToString(ObjDt.Rows[i]["CostIncTax"]) + "</td>");
                                strMainDataRow.Append("</tr>");
                            }
                        }
                    }
                    string strHtml = @"<html><body>
              <div style='float: right'>
                        <img src='http://77.68.11.228:9090/images/Invoice_Logo.png' height='75px' width='235px' />
              </div>

              <table>
                        <tr>
                            <td colspan='2' style='font-size: 20px; font-weight: 110'>" + Utill.RepeatString("&nbsp;", 4) + @"INVOICE</td>
                        </tr>

                        <tr>
                            <td rowspan='2' style='height: 100px; width: 200px; background-color: lightgrey; border-bottom: 0.5px solid black; border-right: 0.5px solid black; border-left: 1px solid black; border-top: 1px solid black;'>
                                <table>
                                    <tr>
                                        <td style='font-size:13px'>Order No:</td>
                                        <td><b>@@OrderNumID</b></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style='font-size:13px'>Date:@@DespatchByDate</td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td style='width: 200px'>
                                                        <table>
                                                            <tr>
                                                                <td style='font-size: 14px; font-weight: bold; text-decoration: underline'>Invoice To:</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@InvoiceCompany</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@InvoiceFullName</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@InvoiceAddress1</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@InvoiceTown</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@InvoiceRegion</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@InvoicePost</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@InvoiceCountry</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td style='font-size: 14px; font-weight: bold; text-decoration: underline'>Deliver To:</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@DeliverCompany</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@DeliverFullName</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@DeliverAddress1</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@DeliverTown</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@DeliverRegion</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@DeliverPost</td>
                                                            </tr>
                                                            <tr>
                                                                <td style='font-size: 13px; font-weight: bold'>@@DeliverCountry</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
              </table>

              <br/>
              
              <table style='border: 1px solid #000; font-size: 12px; width: 100%; text-align: center;' cellspacing='0'>
                     <tr style='background-color: lightgrey; height: 33px;'>
                                <td style='border-bottom: 1px solid #000; border-right: 1px solid #000; width: 10%'>SKU</td>
                                <td style='border-bottom: 1px solid #000; border-right: 1px solid #000; width: 25%'>Original Item Name</td>
                                <td style='border-bottom: 1px solid #000; border-right: 1px solid #000;'>Quantity</td>
                                <td style='border-bottom: 1px solid #000; border-right: 1px solid #000;'>Unit Cost</td>
                                <td style='border-bottom: 1px solid #000; border-right: 1px solid #000;'>VAT Rate</td>
                                <td style='border-bottom: 1px solid #000; border-right: 1px solid #000;'>VAT</td>
                                <td style='border-bottom: 1px solid #000; border-right: 1px solid #000;'>Cost (ex. Tax)</td>
                                <td style='border-bottom: 1px solid #000;'>Line Cost</td>
                     </tr>
                     @@strMainDataRow
              </table>
              <table style='text-align: right; width: 100%; font-size: 12px'>
                     <tr>
                                <td style='width: 94%'>SUB TOTAL:</td>
                                <td style='width: 16%'> @@SubTotal GBP </td>
                      </tr>
                      <tr>
                                <td style='width: 94%'>VAT:</td>
                                <td style='width: 16%'> @@Vat GBP</td>
                      </tr>
                      <tr>
                                <td style='width: 94%'>POSTAGE:</td>
                                <td style='width: 16%'> @@Postage GBP</td>
                      </tr>
                      <tr style='font-weight: bold'>
                                <td style='width: 94%'>TOTAL:</td>
                                <td style='width: 16%'> @@Total GBP</td>
                      </tr>
              </table>

              <br />
              <br />
              <br />
              <br />

              <table style='width: 100%'>
                    <tr>
                                <td colspan='2'>" + Utill.RepeatString("-", 128) + @"</td>
                    </tr>
                    <tr>
                                <td style='text-align: left; width: 30%; font-size: 25px; font-weight: bold'>" + Utill.RepeatString("&nbsp;", 4) + @"Return Slip</td>
                                <td style='text-align: left; width: 70%; font-size: 12px'>If you would like to return your order, please include
                                                        this slip in the package.<br />
                                                        Please contact us before
                                                        returning your item.
                                </td>
                    </tr>
              </table>

              <p style='font-size: 15px; font-weight: bold; text-decoration: underline;'>Return to:</p>

                    <table style='font-size: 12px;'>
                        <tr>
                            <td></td>
                            <td colspan='2' style='text-align: left'>Return reason:
                            </td>
                            <td colspan='3' style='text-align: left'>" + Utill.RepeatString("&nbsp;", 4) + @"I would like:
                            </td>
                        </tr>

                        <tr>
                            <td style='width: 25%; text-align: left'>Electrolve Ltd<br />
                                PO Box 610<br />
                                Grimsby<br />
                                DN31 9FS<br />
                                UNITED KINGDOM
                            </td>
                            <td style='width: 20%; text-align: left'>Faulty (Please specify fault)
                            </td>
                            <td style='width: 15px;text-align: left'>
                                <table>
                                    <tr>
                                        <td style='border: 1px solid black; height: 15px; width: 15px'></td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 25%; text-align: left'>" + Utill.RepeatString("&nbsp;", 4) + @"A replacement:
                            </td>
                            <td style='width: 15px;text-align: left'>
                                <table>
                                    <tr>
                                        <td style='border: 1px solid black; height: 15px; width: 15px'></td>
                                    </tr>
                                </table>
                            </td>
                            <td ></td>
                        </tr>

                        <tr>
                            <td>
                            </td>
                            <td style='width: 20%; text-align: left'>" + Utill.RepeatString(".", 42) + @"
                            </td>
                            <td style='width: 15px'>
                            </td>
                            <td style='width: 25%; text-align: left'>" + Utill.RepeatString("&nbsp;", 4) + @"A refund:
                            </td>
                            <td style='width: 15px;text-align: left'>
                                <table>
                                    <tr>
                                        <td style='border: 1px solid black; height: 15px; width: 15px'></td>
                                    </tr>
                                </table>
                            </td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>
                            </td>
                            <td style='width: 20%; text-align: left'>
                            </td>
                            <td style='text-align: left;width: 40%;' colspan='4'>
                                 <br/>" + Utill.RepeatString("&nbsp;", 9) + @"An exchange for another item (please specify):
                            </td>
                        </tr>

                        <tr>
                            <td style='width: 25%; text-align: left'>
                            </td>
                            <td style='width: 20%; text-align: left'>Incorrect Item
                            </td>
                            <td style='width: 15px; text-align: left'>
                                <table>
                                    <tr>
                                        <td style='border: 1px solid black; height: 15px; width: 15px'></td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 25%; text-align: left' colspan='3'><br/>" + Utill.RepeatString("&nbsp;", 4) + Utill.RepeatString(".", 72) + @"
                            </td>
                        </tr>

                        <tr>
                            <td style='width: 25%; text-align: left'>
                            </td>
                            <td style='width: 20%; text-align: left'>No longer required
                            </td>
                            <td style='width: 15px; text-align: left'>
                                <table>
                                    <tr>
                                        <td style='border: 1px solid black; height: 15px; width: 15px'></td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 25%; text-align: left' colspan='3'>
                            </td>
                        </tr>

                        <tr>
                            <td style='width: 25%; text-align: left'>
                            </td>
                            <td style='width: 20%; text-align: left'>Not as expected
                            </td>
                            <td style='width: 15px; text-align: left'>
                                <table>
                                    <tr>
                                        <td style='border: 1px solid black; height: 15px; width: 15px'></td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 25%; text-align: left' colspan='3'>
                            </td>
                        </tr>

                        <tr>
                            <td style='width: 25%; text-align: left'>
                            </td>
                            <td style='width: 20%; text-align: left'>Other
                            </td>
                            <td style='width: 15px; text-align: left'>
                                <table>
                                    <tr>
                                        <td style='border: 1px solid black; height: 15px; width: 15px'></td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 25%; text-align: left' colspan='3'>" + Utill.RepeatString("&nbsp;", 9) + @"Date: __/__/____
                            </td>
                        </tr>
                    </table>

                    <p style='font-size:15px;font-weight:bold'>@@BottomOrderNumID</p>
                     
                    <table style='width:100%;text-align:center; font-size:12px'>
                         <tr>
                                  <td>&nbsp;</td>
                         </tr>
                         <tr>
                                  <td style='width:30px'>
                                                    eTwist.co.uk is a trading name of Electrolve Ltd, registered in England and Wales under Company Number 06776120.<br/>
                                                    VAT Registration: GB 948042612.<br/>
                                                    www.etwist.co.uk - customerservice@etwist.co.uk
                                 </td>
                       </tr>
                    </table>
         </body></html>";

                    //Replacing for top section
                    strHtml = strHtml.Replace("@@OrderNumID", strOrderID);
                    strHtml = strHtml.Replace("@@DespatchByDate", strDespatchByDate);

                    //Replacing for table data and total details
                    strHtml = strHtml.Replace("@@strMainDataRow", strMainDataRow.ToString());
                    strHtml = strHtml.Replace("@@BottomOrderNumID", strOrderID);
                    strHtml = strHtml.Replace("@@SubTotal", strSubTotal);
                    strHtml = strHtml.Replace("@@Vat", strVat);
                    strHtml = strHtml.Replace("@@Postage", strPostage);
                    strHtml = strHtml.Replace("@@Total", strTotal);

                    //Replacing for Invoice To
                    strHtml = strHtml.Replace("@@InvoiceCompany", strCompany);
                    strHtml = strHtml.Replace("@@InvoiceFullName", strFullName);
                    strHtml = strHtml.Replace("@@InvoiceAddress1", strAddress1);
                    strHtml = strHtml.Replace("@@InvoiceTown", strTown);
                    strHtml = strHtml.Replace("@@InvoiceRegion", strRegion);
                    strHtml = strHtml.Replace("@@InvoicePost", strPostCode);
                    strHtml = strHtml.Replace("@@InvoiceCountry", strCountry);

                    //Replacing for Deliver To
                    strHtml = strHtml.Replace("@@DeliverCompany", strCompany);
                    strHtml = strHtml.Replace("@@DeliverFullName", strFullName);
                    strHtml = strHtml.Replace("@@DeliverAddress1", strAddress1);
                    strHtml = strHtml.Replace("@@DeliverTown", strTown);
                    strHtml = strHtml.Replace("@@DeliverRegion", strRegion);
                    strHtml = strHtml.Replace("@@DeliverPost", strPostCode);
                    strHtml = strHtml.Replace("@@DeliverCountry", strCountry);

                    //string pdffilename = System.IO.Path.GetTempPath() + strOrderID + ".pdf";
                    //string imageFilename = System.IO.Path.GetTempPath();
                    Document document = new Document(PageSize.A4);
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(pdffilename, FileMode.Create));
                    document.Open();
                    HTMLWorker hw = new HTMLWorker(document);
                    StringReader sr = new StringReader(strHtml);
                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
                    document.Close();
                }
                if (blIsPrintCall)
                {
                    LoadImagePdf(pdffilename, imageFilename);
                    //PrintLabel();
                }
                //System.IO.File.Delete(pdffilename);
                return pdffilename;
            }
            catch
            {
                throw;
            }
        }

        public static void LoadImagePdf(string InputPDFFile, string OutPath)
        {
            try
            {
                PdfSharp.Pdf.PdfDocument pdf = PdfSharp.Pdf.IO.PdfReader.Open(InputPDFFile, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                for (int i = 0; i < pdf.Pages.Count; i++)
                {
                    string outImageName = Path.GetFileNameWithoutExtension(InputPDFFile);
                    string str1 = Guid.NewGuid().ToString();
                    outImageName = outImageName + str1 + ".png";
                    string strOutputPath = OutPath + outImageName;
                    int point1 = (int)pdf.Pages[0].Height.Point;
                    int point2 = (int)pdf.Pages[0].Width.Point;
                    GhostscriptSettings settings = new GhostscriptSettings();
                    settings.Device = GhostscriptDevices.png256;
                    settings.Page.Start = i + 1;
                    settings.Page.End = pdf.Pages.Count;
                    settings.Size = new GhostscriptPageSize()
                    {
                        Manual = new Size(point2, point1)
                    };
                    Size size = new Size(400, 400);
                    settings.Resolution = size;
                    GhostscriptWrapper.GenerateOutput(InputPDFFile, strOutputPath, settings);
                    Bitmap bitmap = new Bitmap(strOutputPath);
                    System.Drawing.Image imgLabel = System.Drawing.Image.FromFile(strOutputPath);
                    lstimgInvoice.Add(imgLabel);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
