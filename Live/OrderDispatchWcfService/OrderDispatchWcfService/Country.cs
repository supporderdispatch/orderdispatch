//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OrderDispatchWcfService
{
    using System;
    using System.Collections.Generic;
    
    public partial class Country
    {
        public Country()
        {
            this.tblAddresses = new HashSet<Address>();
        }
    
        public System.Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string cContinent { get; set; }
        public string cCurrency { get; set; }
        public Nullable<double> nPostageCostPerKg { get; set; }
        public System.Guid rowguid { get; set; }
        public Nullable<double> TaxRate { get; set; }
        public Nullable<bool> bLogicalDelete { get; set; }
        public string AddressFormat { get; set; }
        public Nullable<bool> CustomsRequired { get; set; }
    
        public virtual ICollection<Address> tblAddresses { get; set; }
    }
}
