﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
namespace OrderDispatchWcfService
{
    public static class Utill
    {
        /// <summary>
        /// Log error details in txt file 
        /// </summary>
        /// <param name="ex">Exception object of Exception block</param>
        /// <param name="strExceptionBlock">Block/Method name where exception occurred</param>
        /// <param name="intTotalOrders">Total count of orders</param>
        /// <param name="CountWhenError">Count where error occurred(under count of total orders)</param>
        public static void LogError(Exception ex, string strExceptionBlock, int intTotalOrders = 0, int CountWhenError = 0)
        {
            try
            {
                string strBaseDir = AppDomain.CurrentDomain.BaseDirectory;
                string strPathToLogError = "";
                if (strBaseDir.Contains("bin\\Debug"))
                {
                    strPathToLogError = strBaseDir.Replace("\\bin\\Debug", "\\OrderDispatchErrorLog");
                }
                else
                {
                    strPathToLogError = strBaseDir + "OrderDispatchErrorLog\\";
                }
                if (!Directory.Exists(strPathToLogError))
                {
                    Directory.CreateDirectory(strPathToLogError);
                }
                string strErrorTitle = DateTime.Now.ToString("dd-MM-yyyy");
                string message = string.Format("Time: {0} | Exception Block: {1} ", DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"), strExceptionBlock);
                message += Environment.NewLine;
                message += "-----------------------------------------------------------------------------------------------------------------";
                message += Environment.NewLine;
                message += "Custom Block" + strExceptionBlock;
                message += Environment.NewLine;
                message += string.Format("Total number of orders: {0} | Error occurred on count : {1}", intTotalOrders, CountWhenError);
                message += Environment.NewLine;
                message += string.Format("Message: {0} ", ex.Message);
                message += Environment.NewLine;
                message += string.Format("Inner Exception: {0} ", ex.InnerException);
                message += Environment.NewLine;
                message += string.Format("StackTrace: {0} ", ex.StackTrace);
                message += Environment.NewLine;
                message += string.Format("Source: {0} ", ex.Source);
                message += Environment.NewLine;
                message += string.Format("TargetSite: {0} ", ex.TargetSite.ToString());
                message += Environment.NewLine;
                message += "-----------------------------------------------------------------------------------------------------------------";
                message += Environment.NewLine;
                strPathToLogError = strPathToLogError + strErrorTitle + ".txt";
                if (!File.Exists(strPathToLogError))
                {
                    File.Create(strPathToLogError).Dispose();
                    using (StreamWriter writer = new StreamWriter(strPathToLogError, true))
                    {
                        writer.WriteLine(message);
                        writer.Close();
                    }
                }
                else
                {
                    long LogLength = new FileInfo(strPathToLogError).Length;
                    if ((LogLength / 1024) > 7)
                    {
                        File.Delete(strPathToLogError);
                        File.Create(strPathToLogError).Dispose();
                        using (StreamWriter writer = new StreamWriter(strPathToLogError, true))
                        {
                            writer.WriteLine(message);
                            writer.Close();
                        }
                    }
                    else
                    {
                        using (StreamWriter writer = File.AppendText(strPathToLogError))
                        {
                            writer.WriteLine(message);
                            writer.Close();
                        }
                    }
                }
            }
            catch (Exception exLog)
            {
                string strError = exLog.Message;
            }
        }

        /// <summary>
        /// Log service status, like when sync service ran last time and what is current state(stopped=N or running=Y)
        /// </summary>
        /// <param name="intTotalOrders">Total number or orders at the logged time</param>
        public static void LogServiceStatus(int intTotalOrders = 0)
        {
            try
            {
                string strBaseDir = AppDomain.CurrentDomain.BaseDirectory;
                string strPathToServiceStatus = "";

                if (strBaseDir.Contains("bin\\Debug"))
                {
                    strPathToServiceStatus = strBaseDir.Replace("\\bin\\Debug", "\\OrderDispatchWebServiceStatus");
                }
                else
                {
                    strPathToServiceStatus = strBaseDir + "OrderDispatchWebServiceStatus\\";
                }
                if (!Directory.Exists(strPathToServiceStatus))
                {
                    Directory.CreateDirectory(strPathToServiceStatus);
                }
                //string strErrorTitle = DateTime.Now.ToShortDateString();
                string message = string.Format("Time: {0} ", DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "--------------------------------------------------------------------------------";
                message += Environment.NewLine;
                message += "Custom text";
                message += Environment.NewLine;
                message += string.Format("Total Number Of Orders: {0}", intTotalOrders);
                message += Environment.NewLine;
                message += string.Format("Last Datetime when service started: {0}", DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "--------------------------------------------------------------------------------";
                message += Environment.NewLine;
                strPathToServiceStatus = strPathToServiceStatus + "WebServiceStatusLog.txt";
                if (!File.Exists(strPathToServiceStatus))
                {
                    File.Create(strPathToServiceStatus).Dispose();
                    using (StreamWriter writer = new StreamWriter(strPathToServiceStatus, true))
                    {
                        writer.WriteLine(message);
                        writer.Close();
                    }
                }
                else
                {
                    long LogLength = new FileInfo(strPathToServiceStatus).Length;
                    if ((LogLength / 1024) > 5)
                    {
                        File.Delete(strPathToServiceStatus);
                        File.Create(strPathToServiceStatus).Dispose();
                        using (StreamWriter writer = new StreamWriter(strPathToServiceStatus, true))
                        {
                            writer.WriteLine(message);
                            writer.Close();
                        }
                    }
                    else
                    {
                        using (StreamWriter writer = File.AppendText(strPathToServiceStatus))
                        {
                            writer.WriteLine(message);
                            writer.Close();
                        }
                    }
                }
            }
            catch (Exception exLog)
            {
                LogError(exLog, "While Creating Web Service Status txt");
            }
        }

        /// <summary>
        /// To lot the details of order while getting that from linnwork and storing in our db
        /// </summary>
        /// <param name="strLogText">Text to log in database</param>
        /// <param name="OrderIdToLog">Order id for which we are storing log</param>
        /// <param name="GuidForLog">Unique id for each log row in db</param>
        public static void LogHistory(string strLogText, Guid GuidForLog, Guid OrderIdToLog = new Guid())
        {
            /*
             * Note:- IF log text containing charecters '&&' then that means we want to break line here on log history grid
             * we are replacing '&&' with \n to break logs in newlines
             */
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            Guid ID = new Guid();
            Guid guDummyUserID = new Guid();
            ID = GuidForLog;
            int intErrorCount = 0;
        ExecuteAgainToLogThisMethodToo:
            try
            {
                SqlParameter[] objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = ID;//Unique id for each log row

                objParam[1] = new SqlParameter("@OrderID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = OrderIdToLog;//Order id for which we are storing log

                objParam[2] = new SqlParameter("@LogText", SqlDbType.VarChar, int.MaxValue);
                objParam[2].Value = strLogText;//Log text to store for order

                objParam[3] = new SqlParameter("@LoggedByID", SqlDbType.UniqueIdentifier);
                objParam[3].Value = guDummyUserID;//The user id who is logging that log(its dummy for log of sync service because none of user is handling that)

                objParam[4] = new SqlParameter("@LoggedByName", SqlDbType.VarChar);
                objParam[4].Value = "Sync Service"; //The user name who is logging that log 

                objParam[5] = new SqlParameter("@Result", SqlDbType.VarChar, int.MaxValue);
                objParam[5].Direction = ParameterDirection.Output;//Output parameter to trace the operation of sp and that will return error if there is any in sp
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "LogHistory", objParam);

                string strResult = Convert.ToString(objParam[5].Value);

                /*
                 * IF result is nither updated nor inserted that means we got error while execution of "LogHistory" sp
                 * then we are storing that error/exception in log as well
                 */
                if (strResult.ToUpper() != "UPDATED" && strResult.ToUpper() != "INSERTED")
                {
                    if (intErrorCount == 0)
                    {
                        ID = Guid.NewGuid();
                        strLogText = "Error occurred in LogHistory method:- " + strResult;
                        intErrorCount++;
                        goto ExecuteAgainToLogThisMethodToo;
                    }
                    else
                        throw new CustomException(strResult);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        #region "Get Email Condition"

        public static DataTable GetEmailConditions()
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            try
            {
                connection.Open();
                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetConditionData").Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }
        #endregion
        #region "RemoveInvalidColumnIfExistsInString"
        public static void RemoveInvalidColumnIfExistsInString(ref string strDataColumnsText, ref string strDataColumnsForDatabaseUse, ref string[] strArrColumnsInSubject, string sqlEx)
        {
            try
            {
                string[] strArrErrorList = sqlEx.Split('\n');
                List<string> lstDataBaseColums = new List<string>(strDataColumnsForDatabaseUse.Split(','));
                List<string> lstDataColumnInText = new List<string>(strDataColumnsText.Split(','));
                List<string> lstArrColumnsInSubject = new List<string>(strArrColumnsInSubject);
                for (int x = 0; x < strArrErrorList.Length; x++)
                {
                    string strExactError = GetSubString(strArrErrorList[x], "'", "'.");
                    string strColumnToRemoveFromDbList = lstDataBaseColums.Where(P => P.Contains(strExactError + "]")).FirstOrDefault();
                    if (strColumnToRemoveFromDbList != null)
                    {
                        int intIndexOfErrorColumn = lstDataBaseColums.IndexOf(strColumnToRemoveFromDbList);
                        /*
                         * We are using loop instead of lambda here because we can't change case of strDataColumnsText because
                         * this is the exact text which we got from template editor screen and we need to send that exact to client
                         */
                        for (int i = 0; i < lstDataColumnInText.Count; i++)
                        {
                            if (lstDataColumnInText[i].ToUpper().Contains(strExactError + "}]"))
                            {
                                lstDataColumnInText.RemoveAt(i);
                                lstArrColumnsInSubject.RemoveAt(i);
                                break;
                            }
                        }
                        lstDataBaseColums.RemoveAt(intIndexOfErrorColumn);
                        strDataColumnsForDatabaseUse = string.Join(",", lstDataBaseColums);
                        strDataColumnsText = string.Join(",", lstDataColumnInText);
                        strArrColumnsInSubject = lstArrColumnsInSubject.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                Utill.LogHistory("Error occurred in RemoveInvalidColumnIfExistsInString method&&" + ex.Message, Guid.NewGuid());
                throw ex;
            }
        }
        #endregion
        #region "Get Substring"
        public static string GetSubString(string strCompleteString, string strStartString, string strEndString)
        {
            try
            {
                string strData = string.Empty;
                int intStartOfString = strCompleteString.IndexOf(strStartString) + strStartString.Length;
                int intEndOfString = strCompleteString.IndexOf(strEndString);
                strData = strCompleteString.Substring(intStartOfString, intEndOfString - intStartOfString);
                return strData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region "Get Template ID"
        public static DataTable GetTemplateDetailsByTemplateId(Guid TemplateId, Guid ConditionID)
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@TemplateID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = TemplateId;

                objParam[1] = new SqlParameter("@ConditionID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = ConditionID;

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDetailsByTemplateId", objParam).Tables[0];
            }
            catch
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }
        #endregion
        #region "Get Email Accounts"
        /// <summary>
        /// Get details ot all eamil accounts.
        /// </summary>
        /// <returns>DataTable of details of email account</returns>
        public static DataTable GetEmailAccounts()
        {
            DataTable objDt = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            try
            {
                connection.Open();
                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetAdditionalEmailAccountDetails").Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            return objDt;
        }
        #endregion

        #region "Get images and delete images stored in DB with respect to order id"
        // get images and delete images stored in DB with respect to order id
        public static dynamic GetAddUpdateDeleteImageByOrderId(string strMode, Guid OrderId, string strImagetoBase64 = "")
        {

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            string strResult;
            try
            {
                SqlParameter[] objParam = new SqlParameter[5];

                objParam[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[0].Value = OrderId;

                objParam[1] = new SqlParameter("@Mode", SqlDbType.VarChar, 100);
                objParam[1].Value = strMode.ToUpper();

                objParam[2] = new SqlParameter("@Image", SqlDbType.VarChar, strImagetoBase64.Length);
                objParam[2].Value = strImagetoBase64;

                objParam[3] = new SqlParameter("@CreatedBy", SqlDbType.VarChar, 100);
                objParam[3].Value = string.Empty;

                objParam[4] = new SqlParameter("@Result", SqlDbType.VarChar, 100);
                objParam[4].Direction = ParameterDirection.Output;
                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateDeleteImagesByOrderId", objParam);
                }
                else if (strMode.ToUpper() == "DELETE" || strMode.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateDeleteImagesByOrderId", objParam);
                }

                strResult = objParam[4].Value.ToString();

                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                else if (strResult.ToString() == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];

                    }
                    return objDt;
                }
                else
                {
                    return strResult.ToString();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }

        }
        #endregion
        #region "conversion of base64 string to image"
        // conversion of base64 string to image
        public static Image Base64StringToImage(string strBase64String)
        {
            //edit string to check response
            //strBase64String = "";

            byte[] bytImageBytes = Convert.FromBase64String(strBase64String);
            MemoryStream ms = new MemoryStream(bytImageBytes, 0, bytImageBytes.Length);
            ms.Write(bytImageBytes, 0, bytImageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        #endregion

        #region ""
        /// <summary>
        /// That will return a repeated string
        /// </summary>
        /// <param name="StringToRepeat">String that you want to repeat</param>
        /// <param name="NumberOfRepeat">Number of times that you want to repeat a string</param>
        /// <returns></returns>
        public static string RepeatString(string StringToRepeat, int NumberOfRepeat)
        {
            string strResult = "";
            try
            {
                for (int i = 0; i < NumberOfRepeat; i++)
                {
                    strResult = strResult + StringToRepeat;
                }
                return strResult;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
