﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderDispatchWcfService
{
    public partial class OrderItem
    {
        //Because "CompositeSubItems" is an array property
        public List<CompositeSubItems> lstCompositeSubItems { get; set; }

        //Because "AdditionalInfo" is an array property
        public List<AdditionalInfos> lstAdditionalInfo { get; set; }
    }
    //We need these two classe for the mapping of OrderItem class with json,
    //because "Items" node contains "CompositeSubItems" and "AdditionalInfo" properties as array
    public class AdditionalInfos
    {
        public Guid pkOptionId { get; set; }
        public string Property { get; set; }
        public string Value { get; set; }
    }
    public class CompositeSubItems
    {
        //Using temp property because given json is not returning any value for that 
        //and we need to pass value to stored procedure thats why we used that temporarily
        public string TempProperty { get; set; }
    }
    //End of additionaly classes
}
