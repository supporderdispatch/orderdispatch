﻿using System;
namespace ODM.Data.Entity
{
   public class Address
    {
        public System.Guid AddressId { get; set; }
        public bool IsBilling { get; set; }
        public Nullable<bool> IsShipping { get; set; }
        public string EmailAddress { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string Region { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public string FullName { get; set; }
        public string Company { get; set; }
        public string PhoneNumber { get; set; }
        public System.Guid CountryId { get; set; }
        public System.Guid CustomerId { get; set; }
    }
}
