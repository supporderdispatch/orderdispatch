﻿namespace ODM.Data.Entity
{
   public  class OrderData
    {
        public Order order { get; set; }
        public Shipping shipping { get; set; }
        public Customer customer { get; set; }
    }
}
