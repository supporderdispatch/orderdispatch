﻿using System;
namespace ODM.Data.Entity
{
    public class HermesCourier
    {
        public Guid CourierID { get; set; }
        public string CourierName { get; set; }
        public string APIUser { get; set; }
        public string APIPass { get; set; }
        public string APIUserTest { get; set; }
        public string APIPassTest { get; set; }
        public bool IsTestMode { get; set; }
        public string PrinterName { get; set; }
        public bool IsBoseLoggin { get; set; }
        public string DefaultWeightInKg { get; set; }
        public bool IsLandscape { get; set; }
    }
}
