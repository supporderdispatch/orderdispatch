﻿namespace ODM.Data.Entity
{
    public class Shipping
    {
        public System.Guid ShippingId { get; set; }
        public string Vendor { get; set; }
        public System.Guid PostalServiceId { get; set; }
        public string PostalServiceName { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal ItemWeight { get; set; }
        public System.Guid PackageCategoryId { get; set; }
        public string PackageCategory { get; set; }
        public System.Guid PackageTypeId { get; set; }
        public string PackageType { get; set; }
        public decimal PostageCost { get; set; }
        public decimal PostageCostExTax { get; set; }
        public string TrackingNumber { get; set; }
        public bool ManualAdjust { get; set; }
        public System.Guid OrderId { get; set; }
        public System.Guid AddressId { get; set; }
        
    }
}
