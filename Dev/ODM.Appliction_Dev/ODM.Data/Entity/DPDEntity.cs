﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Data.Entity
{
    public class DPDEntity
    {
        public string job_id { get; set; }
        public bool collectionOnDelivery { get; set; }
        public string invoice { get; set; }
        public string collectionDate { get; set; }
        public bool consolidate { get; set; }
        public List<Consignment> consignment { get; set; }
    }
    public static class PrintLabel
    {
        public static string TrackingNumber { get; set; }
        public static string LabelToPrint { get; set; }
    }
    public class Parcels
    {
        public string packageNumber { get; set; }
        public List<parcelProduct> parcelProduct { get; set; }
    }

    public class parcelProduct
    {
        public string productCode { get; set; }
        public string productTypeDescription { get; set; }
        public string productItemsDescription { get; set; }
        public string productFabricContent { get; set; }
        public string countryOfOrigin { get; set; }
        public string productHarmonisedCode { get; set; }
        public string unitWeight { get; set; }
        public string numberOfItems { get; set; }
        public string unitValue { get; set; }
    }

    public class Consignment
    {
        public string consignmentNumber { get; set; }
        public string consignmentRef { get; set; }
        public List<Parcels> parcel { get; set; }
        public CollectionDetails collectionDetails { get; set; }
        public DeliveryDetails deliveryDetails { get; set; }
        public string networkCode { get; set; }
        public int numberOfParcels { get; set; }
        public decimal totalWeight { get; set; }
        public string shippingRef1 { get; set; }
        public string shippingRef2 { get; set; }
        public string shippingRef3 { get; set; }
        public string customsValue { get; set; }
        public string deliveryInstructions { get; set; }
        public string parcelDescription { get; set; }
        public string liabilityValue { get; set; }
        public bool liability { get; set; }
    }

    public class ContactDetails
    {
        public string contactName { get; set; }
        public string telephone { get; set; }
    }

    public class SenderAddress
    {
        public string organisation { get; set; }
        public string countryCode { get; set; }
        public string postcode { get; set; }
        public string street { get; set; }
        public string locality { get; set; }
        public string town { get; set; }
        public string county { get; set; }
    }

    public class OrderAddress
    {
        public string organisation { get; set; }
        public string countryCode { get; set; }
        public string postcode { get; set; }
        public string street { get; set; }
        public string locality { get; set; }
        public string town { get; set; }
        public string county { get; set; }
    }

    public class CollectionDetails
    {
        public ContactDetails contactDetails { get; set; }
        public SenderAddress address { get; set; }
    }

    public class NotificationDetails
    {
        public string email { get; set; }
        public string mobile { get; set; }
    }

    public class DeliveryDetails
    {
        public ContactDetails contactDetails { get; set; }
        public OrderAddress address { get; set; }
        public NotificationDetails notificationDetails { get; set; }
    }
}
