﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;

namespace ODM.Data.ShippingLabel
{
    public class RoyalMailPrintLabel
    {
        public static List<System.Drawing.Image> lstimgLabelToPrints;
        public static System.Drawing.Image imgLabelToPrint;
        public static bool IsManifest;
        public static bool blIsShipmentCreated;
        // to clear image variable in case of error.
        public RoyalMailPrintLabel() {
            imgLabelToPrint = null;
        }
        private void PrintPage(object o, PrintPageEventArgs e)
        {
            float MarginLeft = 0.00f;
            float MarginTop = 0.00f;
            if (IsManifest)
            {
                MarginLeft = -26.00f;
                MarginTop = 0.00f;
            }
            e.Graphics.DrawImage(imgLabelToPrint, MarginLeft, MarginTop);
        }

        public void PrintLabelRoyalMail()
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                if (IsManifest)
                {
                    if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForRoyalMailManifest))
                    {
                        pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForRoyalMailManifest;
                    }
                    if (lstimgLabelToPrints != null)
                    {
                        for (int i = 0; i < lstimgLabelToPrints.Count; i++)
                        {
                            imgLabelToPrint = lstimgLabelToPrints[i];
                            pd.PrintPage += PrintPage;
                            pd.Print();
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForRoyalMail))
                    {
                        pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForRoyalMail;
                    }
                    pd.PrintPage += PrintPage;
                    pd.Print();
                    // to make image variable null when we get error in api call for getting tracking number.
                    imgLabelToPrint = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ExecuteRoyalMailShippingAPI(Order objOrder, Guid OrderID)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            DataTable objDt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@OrderId", SqlDbType.UniqueIdentifier).Value = OrderID;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPrintLabelDetailsForRoyalMail";
                cmd.CommandTimeout = 200;
                SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
                objAdapt.Fill(objDt);
                    
                SetVariableForApiCall();
                ShippingLabelRoyalMail objRoyalMail = new ShippingLabelRoyalMail();
                string strTrackingNumber = objRoyalMail.CreateShipment(objDt);
                if (!string.IsNullOrEmpty(strTrackingNumber))
                {
                    PrintLabelRoyalMail();
                }
                return strTrackingNumber;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Setting the Global variables that are required to make an API call for Royal Mail
        /// </summary>
        public void SetVariableForApiCall()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            DataTable objDt = new DataTable();
            try
            {
                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetRoyalMailSettingDetails").Tables[0];
                if (objDt.Rows.Count > 0)
                {
                    GlobalVariablesAndMethods.APIUser = Convert.ToString(objDt.Rows[0]["Username"]);
                    GlobalVariablesAndMethods.APIPass = Convert.ToString(objDt.Rows[0]["Password"]);
                    GlobalVariablesAndMethods.ClientID = Convert.ToString(objDt.Rows[0]["ClientID"]);
                    GlobalVariablesAndMethods.Secret = Convert.ToString(objDt.Rows[0]["Secret"]);
                    GlobalVariablesAndMethods.ApplicationID = Convert.ToString(objDt.Rows[0]["ApplicationID"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// That one is a Generic method, this will Add, Return, and Delete the data of Pending Label Table for Royal Mail.
        /// </summary>
        /// <param name="strBatchNumber">This value is required in case of Add or Delete data in pending label table for royal mail</param>
        /// <param name="strMode">That contains three values 
        /// 1)GET:-When we want to get data of Pending Manifest Labels of Royal Mail
        /// 2)ADD:-When we want to store Batch Number to Pending Manifest Labels of Royal Mail
        /// 3)DELETE:-When we want to delete data from to Pending Manifest Labels of Royal Mail by Batch Number, that will be get called when we will get the Pending Manifest Labels got printed 
        /// </param>
        /// <param name="IsHaveData">This is out parameter that will tell us whether we have Pending Manifest Labels to print or not</param>
        /// <returns>That will return DataTable with the pending manifes labels</returns>
        public static DataTable WorkWithPendingLabels(string strBatchNumber, string strMode, out bool IsHaveData)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            DataTable objDtPendingManifestLabels = new DataTable();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@BatchNumber", SqlDbType.VarChar, 1000);
                param[0].Value = strBatchNumber;

                param[1] = new SqlParameter("@Mode", SqlDbType.VarChar, 10);
                param[1].Value = strMode;

                if (strMode.ToUpper() == "GET")
                {
                    objDtPendingManifestLabels = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetAddDelete_PendingLabelForRM", param).Tables[0];
                }
                else
                {
                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GetAddDelete_PendingLabelForRM", param);
                }
                if (objDtPendingManifestLabels.Rows.Count > 0)
                {
                    IsHaveData = true;
                }
                else
                {
                    IsHaveData = false;
                }
                return objDtPendingManifestLabels;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
