﻿using Newtonsoft.Json;
using ODM.Data.Entity;
using ODM.Data.Util;
using RestSharp;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Diagnostics;
using System.Drawing.Imaging;
using GhostscriptSharp;
using GhostscriptSharp.Settings;

namespace ODM.Data.ShippingLabel
{
    public class SpringPrintLabel_New
    {
        public bool _IsLiveMode = true;
        public string _strBearerToken = string.Empty;
        string ServiceID = string.Empty;
        string _strTrackingNumber = string.Empty, _strError = string.Empty, _strResponseCode = string.Empty, _ShipmentID = string.Empty;

        public void PrintLabel(byte[] btLabel)
        {
            try
            {
                MemoryStream ms = new MemoryStream(btLabel);
                ms.Write(btLabel, 0, btLabel.Length);
                //PdfDocument pd = new PdfDocument();
                //if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForSpring))
                //{
                //    pd.PrintSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForSpring;
              //  //}
              ////  pd.LoadFromStream(ms);
              //  pd.Print();
                PrintDocument pd = new PrintDocument();
             
                if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForSpring_New))
                {
                    pd.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForSpring_New;
                }
                pd.PrintPage += (Sender, e) =>
                {
                    var path=Path.GetTempPath();
                    var imgLabelToPrint = LoadImagePdf(path+"Response.pdf", path);
                    float MarginLeft = -20.00f;
                    float MarginTop = 0.0f;
                    e.Graphics.DrawImage(imgLabelToPrint, MarginLeft, MarginTop);
                };
                pd.Print();
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.GuidForLog = new Guid();
                GlobalVariablesAndMethods.LogHistory(string.Format("Exception occurred in Label printing process,&&Exception:- {0}", ex.Message.ToString()));
            }
        }
        public static System.Drawing.Image LoadImagePdf(string InputPDFFile, string OutPath)
        {
            string outImageName = Path.GetFileNameWithoutExtension(InputPDFFile);
            outImageName = outImageName + ".png";
            string strOutputPath = OutPath + outImageName;
            GhostscriptSettings settings = new GhostscriptSettings();
            settings.Device = GhostscriptDevices.png256;
            //var pageSize = PageSize.A4;
            //settings.Size = new GhostscriptPageSize()
            //{
            //    Manual = new Size(Convert.ToInt32(pageSize.Height)-28, Convert.ToInt32(pageSize.Width)-24)
            //};
           // settings.Resolution = new Size(Convert.ToInt32(pageSize.Height)-28,Convert.ToInt32(pageSize.Width)-24);
            PdfSharp.Pdf.PdfDocument pdf = PdfSharp.Pdf.IO.PdfReader.Open(InputPDFFile, PdfDocumentOpenMode.Import);
            int point1 = (int)pdf.Pages[0].Height.Point;
            int point2 = (int)pdf.Pages[0].Width.Point;
            settings.Device = GhostscriptDevices.png256;
            settings.Size = new GhostscriptPageSize()
            {
                Manual = new Size(point2, point1)
            };
            Size size = new Size(400, 400);
            settings.Resolution = size;
            GhostscriptWrapper.GenerateOutput(InputPDFFile, strOutputPath, settings);
            Bitmap bitmap = new Bitmap(strOutputPath);
            System.Drawing.Image imgLabel = System.Drawing.Image.FromFile(strOutputPath);
            imgLabel.RotateFlip(RotateFlipType.Rotate90FlipXY);
            return imgLabel;
        }
        // To print Spring labels using new API for spring courier and commenting the old api.
        public string ExecuteSpringShippingAPI_New(Order objOrder, Guid OrderID, DataTable objDtOrderDetails)
        {
            GlobalVariablesAndMethods.OrderIdToLog = OrderID;
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            //List<linnworks.scripting.core.ReturnClass.ShippingLabel> lstShippingLabel = new List<linnworks.scripting.core.ReturnClass.ShippingLabel>();
            GlobalVariablesAndMethods.strCountryCode = Convert.ToString(objDtOrderDetails.Rows[0]["CountryCode"]);
            DataTable objDt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@OrderId", SqlDbType.UniqueIdentifier).Value = OrderID;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPrintLabelDetailsForSpring_New";
                cmd.CommandTimeout = 200;
                SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
                objAdapt.Fill(objDt);
                _IsLiveMode = Convert.ToBoolean(objDt.Rows[0]["IsLive"]);

                string strCourierName = Convert.ToString(objDt.Rows[0]["CourierName"]);
                if (_IsLiveMode)
                {
                    GlobalVariablesAndMethods.UseLive = true;
                    GlobalVariablesAndMethods.strClientID = Convert.ToString(objDt.Rows[0]["LiveClientID"]);
                    GlobalVariablesAndMethods.strSecret = Convert.ToString(objDt.Rows[0]["LiveSecret"]);
                    GlobalVariablesAndMethods.strShopID = Convert.ToString(objDt.Rows[0]["ShopID"]);
                    GlobalVariablesAndMethods.strContractID = Convert.ToString(objDt.Rows[0]["ContractID"]);
                }
                else
                {
                    GlobalVariablesAndMethods.UseLive = false;
                    GlobalVariablesAndMethods.strClientID = Convert.ToString(objDt.Rows[0]["TestClientID"]);
                    GlobalVariablesAndMethods.strSecret = Convert.ToString(objDt.Rows[0]["TestSecret"]);
                    GlobalVariablesAndMethods.strShopID = Convert.ToString(objDt.Rows[0]["TestShopID"]);
                    GlobalVariablesAndMethods.strContractID = Convert.ToString(objDt.Rows[0]["TestContractID"]);

                }
              
                GlobalVariablesAndMethods.strDefaultWeightInKg = Convert.ToString(objDt.Rows[0]["DefaultWeight"]);


                _strBearerToken = AuthCallForSpring_New();
                if (!string.IsNullOrEmpty(_strBearerToken))
                {
                    if (_strBearerToken.ToUpper() != "ERROR")
                    {
                        GetShippingServiceIDFromCountryCode(strCourierName, objDt.Rows[0]["CountryCode"].ToString());
                        CreateShipment(objDt, objDtOrderDetails);
                        //if (!string.IsNullOrEmpty(_ShipmentID))
                        //{
                        //    GetLabelAndTrackingNumberForSpring(_ShipmentID);    
                        //}

                    }
                    else
                    {
                        _strError = "ERROR";
                    }



                }

                //ShippingLabelMacroSpring objMacro = new ShippingLabelMacroSpring();
                //SqlConnection con = new SqlConnection();
                //linnworks.scripting.core.Debugger objDebug = new linnworks.scripting.core.Debugger();
                //linnworks.scripting.core.Classes.OptionCollection ServiceOptions = new linnworks.scripting.core.Classes.OptionCollection();
                //lstShippingLabel = objMacro.Initialize(objDebug, con, objOrder, ServiceOptions);
                //Shipping = lstShippingLabel;
                if (_strError.ToUpper() == "ERROR")
                {
                    return _strError;
                }
                else
                {
                    return _strTrackingNumber;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }

            //if (lstShippingLabel.Count > 0)
            //{
            //    return lstShippingLabel[0].TrackingNumber;
            //}
            //else
            //{
            //    return "";
            //}
        }
        public void CreateShipment(DataTable objDt, DataTable objDtOrderDetails)
        {


            string strHost = GetApiUrl();
            //if (_IsLiveMode)
            //{
            //    strHost = "";
            //}
            //else
            //{
            //    strHost = "https://sandbox-api.myparcel.com/shipments";
            //}

            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            // we need this time stamp to be in past so we'll subtract 1 minute from it.
            unixTimestamp = unixTimestamp - (1 * 60 * 1000);
            string strOrderItemDetails = string.Empty;


            for (int i = 0; i < objDtOrderDetails.Rows.Count; i++)
            {
                string strAmount;
                if (Convert.ToString(Convert.ToInt32(Convert.ToDecimal(objDtOrderDetails.Rows[i]["PricePerUnit"]) * 100)) == "0")
                {
                    strAmount = "1";
                }
                else
                {
                    strAmount = Convert.ToString(Convert.ToInt32(Convert.ToDecimal(objDtOrderDetails.Rows[i]["PricePerUnit"]) * 100));
                }

 
                strOrderItemDetails = strOrderItemDetails + @" { 
                                            ""sku"": """ + Convert.ToString(objDtOrderDetails.Rows[i]["SKU"]).TrimEnd() + @""",
                                            ""description"": """ + Convert.ToString(objDtOrderDetails.Rows[i]["Title"]).TrimEnd().RemoveCharacters("'\"")+ @""",
                                            ""item_value"": { 
                                                            ""amount"":" + strAmount + @",
                                                            ""currency"":""" + Convert.ToString(objDtOrderDetails.Rows[i]["Currency"]) + @"""
                                                            },
                                            ""quantity"": " + Convert.ToString(Convert.ToInt32(objDtOrderDetails.Rows[i]["Quantity"])) + @",
                                            ""hs_code"": ""8517.12.00"",
                                            ""origin_country_code"": """ + Convert.ToString(objDtOrderDetails.Rows[i]["CountryCode"]) + @""",
                                            ""item_weight"": " + Convert.ToString(Convert.ToInt32(Convert.ToDecimal(Convert.ToString(objDtOrderDetails.Rows[0]["ItemWeight"])) * 1000)) + @"

                                         }";
                if (!(i == objDtOrderDetails.Rows.Count - 1))
                {
                    strOrderItemDetails = strOrderItemDetails + ",";
                }
            }
            string[] fullName = Convert.ToString(objDtOrderDetails.Rows[0]["FullName"]).Split(' ');
            string first_Name=fullName.Where(x => !String.IsNullOrWhiteSpace(x)).FirstOrDefault();
            string last_Name = ".";
            if (fullName.Length > 1)
            {
                for (int i = 1; i < fullName.Length; i++)
                {
                    if (!String.IsNullOrWhiteSpace(fullName[i]))
                    {
                        last_Name = fullName[i];
                        break;
                    }
                }
            }
            string strEmail = Convert.ToString(objDtOrderDetails.Rows[0]["EmailAddress"]);
            strEmail = !string.IsNullOrEmpty(strEmail) ? @"""email""" + ":" +@""""+ strEmail + @""""+"," :  @"""email""" + ":" +@"""noreply@swiftecommerce.co.uk"""+",";

            string address2 = Convert.ToString(objDtOrderDetails.Rows[0]["Address2"]??string.Empty);
            string address3 = Convert.ToString(objDtOrderDetails.Rows[0]["Address3"] ?? string.Empty);

            address2 = !string.IsNullOrEmpty(address3) ? address2 + "," + address3 : address2;
            address2 = !string.IsNullOrEmpty(address2) ? @"""street_2"": """ + address2 + @"""" + "," : string.Empty;

            string company = Convert.ToString(objDtOrderDetails.Rows[0]["company"]);
            company = !string.IsNullOrEmpty(company) ? @"""company"": """ + company + @"""" + "," : string.Empty;

            string phoneNumber = Convert.ToString(objDtOrderDetails.Rows[0]["PhoneNumber"]).RemoveCharacters(")(,+- ");
            phoneNumber = string.IsNullOrEmpty(phoneNumber) ? "000000000000" : phoneNumber;

            string strRequestData = @"
{
  ""data"": {
    ""type"": ""shipments"",
    ""attributes"": {
      ""recipient_address"": {
        ""street_1"":""" + Convert.ToString(objDtOrderDetails.Rows[0]["CompanyAddress"]).RemoveCharacters("\\&") + @""",
        " + address2.RemoveCharacters("\\&") + @"
        ""postal_code"": """ + Convert.ToString(objDtOrderDetails.Rows[0]["PostCode"]).RemoveCharacters("\\&") + @""",
        ""city"": """ + Convert.ToString(objDtOrderDetails.Rows[0]["Town"]).RemoveCharacters("\\&") + @""",
        ""country_code"": """ + Convert.ToString(objDtOrderDetails.Rows[0]["CountryCode"]) + @""",
        " + company.RemoveCharacters("\\&") + @"
        ""first_name"": """ + first_Name.RemoveCharacters("\\&") + @""",
        ""last_name"": """ + last_Name.RemoveCharacters("\\&") + @""",
       " + strEmail + @"
        ""phone_number"": """ + phoneNumber + @"""
      },
      ""return_address"": {
        ""street_1"": """ + Convert.ToString(objDt.Rows[0]["SenderStreet1"]) + @""",
        ""street_2"":""" + Convert.ToString(objDt.Rows[0]["SenderStreet2"]) + @""",
        ""street_number"": " + Convert.ToString(objDt.Rows[0]["SenderStreetNumber"]) + @",
        ""street_number_suffix"": """ + Convert.ToString(objDt.Rows[0]["SenderStreetNumberSuffix"]) + @""",
        ""postal_code"":""" + Convert.ToString(objDt.Rows[0]["SenderPostalCode"]) + @""",
        ""city"":""" + Convert.ToString(objDt.Rows[0]["SenderCity"]) + @""",
        ""region_code"": """ + Convert.ToString(objDt.Rows[0]["SenderRegion"]) + @""",
        ""country_code"": """ + Convert.ToString(objDt.Rows[0]["CountryCode"]) + @""",
        ""first_name"": """ + Convert.ToString(objDt.Rows[0]["SenderFirstName"]) + @""",
        ""last_name"": """ + Convert.ToString(objDt.Rows[0]["SenderLastName"]) + @""",
        ""company"": """ + Convert.ToString(objDt.Rows[0]["SenderCompany"]) + @""",
        ""email"": """ + Convert.ToString(objDt.Rows[0]["SenderEmail"]) + @""",
        ""phone_number"": """ + Convert.ToString(objDt.Rows[0]["SenderPhoneNumber"]).RemoveCharacters("+- ") + @"""
      },
      ""sender_address"": {
        ""street_1"": """ + Convert.ToString(objDt.Rows[0]["SenderStreet1"]) + @""",
        ""street_2"":""" + Convert.ToString(objDt.Rows[0]["SenderStreet2"]) + @""",
        ""street_number"": " + Convert.ToString(objDt.Rows[0]["SenderStreetNumber"]) + @",
        ""street_number_suffix"": """ + Convert.ToString(objDt.Rows[0]["SenderStreetNumberSuffix"]) + @""",
        ""postal_code"":""" + Convert.ToString(objDt.Rows[0]["SenderPostalCode"]) + @""",
        ""city"":""" + Convert.ToString(objDt.Rows[0]["SenderCity"]) + @""",
        ""region_code"": """ + Convert.ToString(objDt.Rows[0]["SenderRegion"]) + @""",
        ""country_code"": """ + Convert.ToString(objDt.Rows[0]["CountryCode"]) + @""",
        ""first_name"": """ + Convert.ToString(objDt.Rows[0]["SenderFirstName"]) + @""",
        ""last_name"": """ + Convert.ToString(objDt.Rows[0]["SenderLastName"]) + @""",
        ""company"": """ + Convert.ToString(objDt.Rows[0]["SenderCompany"]) + @""",
        ""email"": """ + Convert.ToString(objDt.Rows[0]["SenderEmail"]) + @""",
        ""phone_number"": """ + Convert.ToString(objDt.Rows[0]["SenderPhoneNumber"]).RemoveCharacters("+- ") + @"""
      },
      ""description"": ""Order #" + Convert.ToString(objDtOrderDetails.Rows[0]["NumOrderId"]) + @""",
      ""physical_properties"": {
        ""height"": " + Convert.ToString(Convert.ToInt32(objDtOrderDetails.Rows[0]["Height"])) + @",
        ""width"": " + Convert.ToString(Convert.ToInt32(objDtOrderDetails.Rows[0]["Width"])) + @",
        ""length"": " + Convert.ToString(Convert.ToInt32(objDtOrderDetails.Rows[0]["Depth"])) + @",
        ""volume"": 300,
        ""weight"": " + Convert.ToString(Convert.ToInt32(Convert.ToDecimal(Convert.ToString(objDtOrderDetails.Rows[0]["TotalWeight"])) * 1000)) + @"
      },
      ""items"": [" + strOrderItemDetails + @"
        
      ],
	  ""customs"": {
        ""content_type"": ""merchandise"",
        ""invoice_number"": """ + Convert.ToString(objDtOrderDetails.Rows[0]["NumOrderId"]) + @""",
        ""non_delivery"": ""return"",
        ""incoterm"": ""DAP""
      },
        ""register_at"": " + unixTimestamp.ToString() +
    @"},
    ""relationships"": {
      ""shop"": {
        ""data"": {
          ""type"": ""shops"",
          ""id"": """ + GlobalVariablesAndMethods.strShopID + @"""
        }
      },
      ""service"": {
        ""data"": {
           ""type"": ""services"",
          ""id"": """ + ServiceID + @"""
        }
      },
      ""contract"": {
        ""data"": {
          ""type"": ""contracts"",
          ""id"": """ + GlobalVariablesAndMethods.strContractID + @"""
        }
      }
    }
  }
}";
            var client = new RestClient(strHost + "shipments");
            var request = new RestRequest(Method.POST);

            request.AddHeader("authorization", "Bearer " + _strBearerToken);
            request.AddHeader("accept", "application/vnd.api+json");
            request.AddParameter("text/plain", strRequestData, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string JsonData = response.Content;
            dynamic objCreateShipmentResponse = JsonConvert.DeserializeObject(JsonData);

            if (objCreateShipmentResponse["data"] != null)
            {
                if (Convert.ToString(objCreateShipmentResponse["data"]) != null)
                {
                    _ShipmentID = Convert.ToString(objCreateShipmentResponse["data"]["id"]);
                    _strTrackingNumber = GetTrackingNumberFromShipmentID(_ShipmentID);
                    GetLabelForSpring(_ShipmentID);
                }
            }
            else
            {
                GlobalVariablesAndMethods.GuidForLog = new Guid();
                GlobalVariablesAndMethods.LogHistory("Can't process because of error:- " + objCreateShipmentResponse["errors"]);

            }


        }
        public void GetLabelForSpring(string ShipmentID)
        {

            string strHost = GetApiUrl();
            string strLabelID, strLabelLink = string.Empty;
            //if (_IsLiveMode)
            //{
            //    strHost = "https://api.myparcel.com/shipments/";
            //}
            //else
            //{
            //    strHost = "https://sandbox-api.myparcel.com/shipments/";
            //}
            var client = new RestClient(strHost + "shipments/" + ShipmentID + "/files");
            var request = new RestRequest(Method.GET);
            request.AddHeader("authorization", "Bearer " + _strBearerToken);
            request.AddHeader("accept", "application/pdf");
            IRestResponse response = client.Execute(request);
            string JsonData = response.Content;
            dynamic objGetLabelForSpringResponse = JsonConvert.DeserializeObject(JsonData);


            if (objGetLabelForSpringResponse["data"].Count > 0)
            {
                foreach (var item in objGetLabelForSpringResponse["data"])
                {
                    if (item["attributes"]["document_type"] == "label")
                    {
                        strLabelID = Convert.ToString(item["id"]);
                        strLabelLink = Convert.ToString(item["links"]["self"]);
                        break;
                    }
                }
                if (!string.IsNullOrEmpty(strLabelLink))
                {

                    var clientForLabel = new RestClient(strLabelLink);
                    var requestForLabel = new RestRequest(Method.GET);
                    requestForLabel.AddHeader("authorization", "Bearer " + _strBearerToken);
                    requestForLabel.AddHeader("accept", "application/pdf");
                    IRestResponse Newresponse = clientForLabel.Execute(requestForLabel);
                    string JsonData1 = Newresponse.Content;
                    byte[] btLabel = clientForLabel.DownloadData(requestForLabel);
                    File.WriteAllBytes(Path.GetTempPath()+"Response.pdf", btLabel);
                    GlobalVariablesAndMethods.GuidForLog = new Guid();
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed successfully && Printing Label ", strLabelLink));
                    PrintLabel(btLabel);
                    // print label code 
                }


            }


        }

        // we'll call this function after 5 seconds gap, the tracking number takes up to 1 second to be available, maybe it will always be ready in time, or maybe sometimes we will execute the code quicker than it is created
        public string GetTrackingNumberFromShipmentID(string ShipmentID)
        {
            string strTrackingNumber = string.Empty;
            string strHost = GetApiUrl();
            //if (_IsLiveMode)
            //{
            //    strHost = "https://api.myparcel.com/shipments/";
            //}
            //else
            //{
            //    strHost = "https://sandbox-api.myparcel.com/shipments/";
            //}
            int retry = 0;
        ExecuteUntillTrackingNumber:

            var client = new RestClient(strHost + "shipments/"+ ShipmentID);
            var request = new RestRequest(Method.GET);
            request.AddHeader("authorization", "Bearer " + _strBearerToken);
            request.AddHeader("accept", "application/vnd.api+json");
            IRestResponse response = client.Execute(request);
            string JsonData = response.Content;
            dynamic objTrackingNumberResponse = JsonConvert.DeserializeObject(JsonData);
            if (objTrackingNumberResponse["data"] != null)
            {
                Action retryIfTrackingNumberNotFound = () => { };
                if (objTrackingNumberResponse["data"]["attributes"]["tracking_code"] != null)
                {
                    strTrackingNumber = Convert.ToString(objTrackingNumberResponse["data"]["attributes"]["tracking_code"]);
                    if (string.IsNullOrEmpty(strTrackingNumber))
                    {
                        if (retry < 10)
                        {
                            bool ActivateGotTo = false;
                            Task t = new Task(() =>
                            {
                                ActivateGotTo = true;
                            });
                            try { t.Wait(2000); }
                            catch { }
                            t.RunSynchronously();
                            retry++;
                            if (ActivateGotTo)
                            {
                                ActivateGotTo = false;
                                goto ExecuteUntillTrackingNumber;
                            }
                        }
                    }
                    else
                    {
                        GlobalVariablesAndMethods.GuidForLog = new Guid();
                        GlobalVariablesAndMethods.LogHistory(string.Format("Shipping Call completed successfully && Got Tracking number :- {0}",strTrackingNumber));
                    }
                }
                else
                {
                    if (retry < 10)
                    {
                        bool ActivateGotTo = false;
                        Task t = new Task(() =>
                        {
                            ActivateGotTo = true;
                        });
                        try { t.Wait(2000); }
                        catch { }
                        t.RunSynchronously();
                        retry++;
                        if (ActivateGotTo)
                        {
                            ActivateGotTo = false;
                            goto ExecuteUntillTrackingNumber;
                        }
                    }
                    else
                    {
                        GlobalVariablesAndMethods.GuidForLog = new Guid();
                        GlobalVariablesAndMethods.LogHistory(String.Format("Unable to get tracking number for order ,Retries count:-{0}",retry));
                    }
                }

            }
            else
            {
                _strError = "ERROR";
            }
            return strTrackingNumber;
        }
        public void GetShippingServiceIDFromCountryCode(string strCourierName, string senderCountry)
        {
            string strHost = GetApiUrl();

            //if (_IsLiveMode)
            //{
            //    strHost = "https://api.myparcel.com/services?";
            //}
            //else
            //{
            //    strHost = "https://sandbox-api.myparcel.com/services?";
            //}
            var client = new RestClient(strHost + "services?");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + _strBearerToken);
            request.AddHeader("accept", "application/vnd.api+json");
            request.AddParameter("filter[address_to][country_code]", GlobalVariablesAndMethods.strCountryCode);
            IRestResponse response = client.Execute(request);
            string JsonData = response.Content;
            dynamic objDataForAllServices = JsonConvert.DeserializeObject(JsonData);
            if (objDataForAllServices["errors"] != null)
            {
                string strResponseCode = Convert.ToString(objDataForAllServices["errors"][0]["status"]);
                GlobalVariablesAndMethods.LogHistory(string.Format("Error occurred in " + strHost + " api call,Status code:-{0}&&response:-{1}", strResponseCode, Convert.ToString(objDataForAllServices)));

            }
            else
            {
                if (objDataForAllServices["data"].Count > 0)
                {
                    for (int i = 0; i < objDataForAllServices["data"].Count; i++)
                    {
                        if (Convert.ToString(objDataForAllServices["data"][i]["attributes"]["name"]).ToUpper() == strCourierName.ToUpper())
                        {
                            var sender_country = senderCountry;
                            var customer_country = GlobalVariablesAndMethods.strCountryCode.ToString();
                            //getting allowed region from 
                            var region_from = objDataForAllServices["data"][i]["attributes"]["regions_from"][0]["country_code"].ToString();
                            //getting allowed region to
                            var region_to = objDataForAllServices["data"][i]["attributes"]["regions_to"][0]["country_code"].ToString();
                            if (Convert.ToString(region_from).ToUpper().Equals(sender_country) && Convert.ToString(region_to).ToUpper().Equals(customer_country))
                            {
                                ServiceID = Convert.ToString(objDataForAllServices["data"][i]["id"]);
                                GlobalVariablesAndMethods.GuidForLog = new Guid();
                                GlobalVariablesAndMethods.LogHistory(String.Format("Got service Id for {0} ", strCourierName.ToString()));
                                break;
                            }
                        }

                    }

                }
                else
                {
                    GlobalVariablesAndMethods.LogHistory(string.Format("Error occurred in " + strHost + " api call, response:-{0}", Convert.ToString(objDataForAllServices)));

                }

            }
        }

        // function to make Auth call to the Myparcel.com for getting Access Token for Spring Couriers
        public string AuthCallForSpring_New()
        {
            string strBearerToken = string.Empty;
            string strHost = String.Empty;
            if (_IsLiveMode)
            {
                strHost = "https://auth.myparcel.com/access-token";
            }
            else
            {
                strHost = "https://sandbox-auth.myparcel.com/access-token";
            }
            var client = new RestClient(strHost);
            var request = new RestRequest(Method.POST);

            //   request.AddHeader("content-type", "application/json");
            request.AddParameter("grant_type", "client_credentials", ParameterType.GetOrPost);
            request.AddParameter("client_id", GlobalVariablesAndMethods.strClientID, ParameterType.GetOrPost);
            request.AddParameter("client_secret", GlobalVariablesAndMethods.strSecret, ParameterType.GetOrPost);
            request.AddParameter("scope", "*", ParameterType.GetOrPost);
            IRestResponse response = client.Execute(request);
            string JsonData = response.Content;
            dynamic objData = JsonConvert.DeserializeObject(JsonData);
            if (objData["errors"] != null)
            {
                _strResponseCode = Convert.ToString(objData["errors"][0]["status"]);
                //string strErrorTitle = Convert.ToString(objData["errors"][0]["title"]);
                GlobalVariablesAndMethods.LogHistory(string.Format("Error occurred in " + strHost + " api call,Status code:-{0}&&response:-{1}", _strResponseCode, Convert.ToString(objData)));
                _strError = "ERROR";
            }
            else
            {
                strBearerToken = Convert.ToString(objData["access_token"]);

            }
            return strBearerToken;
        }
        public string GetApiUrl()
        {
            string strHost = "";
            if (_IsLiveMode)
            {
                strHost = "https://api.myparcel.com/";
            }
            else
            {
                strHost = "https://sandbox-api.myparcel.com/";
            }
            return strHost;
        }

    }
}
