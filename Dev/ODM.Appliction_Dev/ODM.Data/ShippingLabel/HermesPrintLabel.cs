﻿using linnworks.finaware.CommonData.Objects;
using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;

namespace ODM.Data.ShippingLabel
{
    public class HermesPrintLabel
    {
        public List<linnworks.scripting.core.ReturnClass.ShippingLabel> Shipping { get; set; }
        public bool IsLandscape { get; set; }
        public string ShippingMethod { get; set; }

        private void PrintPage(object o, PrintPageEventArgs e)
        {
            System.Drawing.Image img = Shipping[0].LabelImage;
            float MarginLeft = 0.00f;
            float MarginTop = 0.00f;
            e.Graphics.DrawImage(img, MarginLeft, MarginTop);
        }

        public void PrintLabel()
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForHermes))
                {
                    pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForHermes;
                }
                pd.DefaultPageSettings.Landscape = IsLandscape;
                //this is because "Hermes International" service shows label in different orientation than other hermes services
                //if (ShippingMethod.ToUpper() == "HERMES INTERNATIONAL")
                //{
                //    pd.DefaultPageSettings.Landscape = IsLandscape;
                //}
                //else
                //{
                //    pd.DefaultPageSettings.Landscape = IsLandscape ? false : true;
                //}
                //end of orientation check

                pd.PrintPage += PrintPage;
                pd.Print();
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }

        public string ExecuteMyHermesShippingAPI(Order objOrder, Guid OrderID)
        {

            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            List<linnworks.scripting.core.ReturnClass.ShippingLabel> lstShippingLabel = new List<linnworks.scripting.core.ReturnClass.ShippingLabel>();
            DataTable objDt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@OrderId", SqlDbType.UniqueIdentifier).Value = OrderID;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPrintLabelDetailsForHermes";
                cmd.CommandTimeout = 200;
                SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
                objAdapt.Fill(objDt);

                //Mapping MyHermesCourier for the printing settings
                HermesCourier objCourier = new HermesCourier();
                objCourier.PrinterName = Convert.ToString(objDt.Rows[0]["PrinterName"]);
                objCourier.APIUser = Convert.ToString(objDt.Rows[0]["APIUser"]);
                objCourier.APIPass = Convert.ToString(objDt.Rows[0]["APIPass"]);
                objCourier.APIUserTest = Convert.ToString(objDt.Rows[0]["APIUserTest"]);
                objCourier.APIPassTest = Convert.ToString(objDt.Rows[0]["APIPassTest"]);
                objCourier.DefaultWeightInKg = Convert.ToString(objDt.Rows[0]["DefaultWeightInKg"]);
                objCourier.IsBoseLoggin = Convert.ToBoolean(objDt.Rows[0]["IsBoseLoggin"]);
                objCourier.IsTestMode = Convert.ToBoolean(objDt.Rows[0]["IsTestMode"]);
                objCourier.IsLandscape = Convert.ToBoolean(objDt.Rows[0]["IsLandscape"]);
                IsLandscape = Convert.ToBoolean(objDt.Rows[0]["IsLandscape"]);
                bool IsTestMode = Convert.ToBoolean(objDt.Rows[0]["IsTestMode"]);
                if (IsTestMode)
                {
                    GlobalVariablesAndMethods.UseLive = false;
                    GlobalVariablesAndMethods.APIUser = Convert.ToString(objDt.Rows[0]["APIUserTest"]);
                    GlobalVariablesAndMethods.APIPass = Convert.ToString(objDt.Rows[0]["APIPassTest"]);
                }
                else
                {
                    GlobalVariablesAndMethods.UseLive = true;
                    GlobalVariablesAndMethods.APIUser = Convert.ToString(objDt.Rows[0]["APIUser"]);
                    GlobalVariablesAndMethods.APIPass = Convert.ToString(objDt.Rows[0]["APIPass"]);
                }
                GlobalVariablesAndMethods.blVerboseLogging = Convert.ToBoolean(objDt.Rows[0]["IsBoseLoggin"]);
                GlobalVariablesAndMethods.strDefaultWeightInKg = Convert.ToString(objDt.Rows[0]["DefaultWeightInKg"]);
                //End of MyHermesCourier class mapping.

                //sanitizing string length and characters
                objOrder.ReferenceNum = objOrder.ReferenceNum.LimitLengthTo(20);
                objOrder.Address1 = objOrder.Address1.RemoveCharacters("&");
                objOrder.Address2 = objOrder.Address2.RemoveCharacters("&");
                objOrder.Address3 = objOrder.Address3.RemoveCharacters("&");
                //

                ShippingLabelMacroHermes objMacro = new ShippingLabelMacroHermes();
                SqlConnection con = new SqlConnection();
                linnworks.scripting.core.Debugger objDebug = new linnworks.scripting.core.Debugger();
                linnworks.scripting.core.Classes.OptionCollection ServiceOptions = new linnworks.scripting.core.Classes.OptionCollection();
                lstShippingLabel = objMacro.Initialize(objDebug, con, objOrder, ServiceOptions);
                Shipping = lstShippingLabel;
                ShippingMethod = objOrder.ShippingMethod;
                PrintLabel();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            if (lstShippingLabel.Count > 0)
            {
                return lstShippingLabel[0].TrackingNumber;
            }
            else
            {
                return "";
            }
        }
    }
}
