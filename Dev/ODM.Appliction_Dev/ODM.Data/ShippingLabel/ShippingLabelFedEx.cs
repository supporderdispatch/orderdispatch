﻿using GhostscriptSharp;
using GhostscriptSharp.Settings;
using ODM.Data.Util;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using RestSharp;
using System;
using System.Data;
using System.Drawing;
using System.IO;

namespace ODM.Data.ShippingLabel
{
    public class ShippingLabelFedEx
    {
        #region "GlobalVariables"
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        string strResponseStatus = string.Empty;
        string strLogText = string.Empty;
        bool blIsCustomException = false;
        #endregion

        #region "Methods"
        /// <summary>
        /// Create shipment for an order 
        /// </summary>
        /// <param name="objDt">DataTable of Order details</param>
        /// <returns>Tracking Number</returns>
        public string CreateShipment(DataTable objDt)
        {
            string strTrackingNumber = string.Empty;
            try
            {
                string strCurrency = Convert.ToString(objDt.Rows[0]["Currency"]);
                strCurrency = strCurrency.ToUpper() == "GBP" ? "UKL" : strCurrency;

                string strOrderDetailsRegion = Convert.ToString(objDt.Rows[0]["Region"]);
                string strOrderDetailsCountry = Convert.ToString(objDt.Rows[0]["CountryName"]);

                string strOrderDetailsStateCode = GlobalVariablesAndMethods.getStateCode(strOrderDetailsRegion, strOrderDetailsCountry);

                string phone = Convert.ToString(objDt.Rows[0]["Phone"]).RemoveCharacters(")(,+- ");
                phone = string.IsNullOrEmpty(phone) ? "000000000000" : phone;

                var phoneNumber = Convert.ToString(objDt.Rows[0]["PhoneNumber"]).RemoveCharacters(")(,+- ");
                phoneNumber = string.IsNullOrEmpty(phoneNumber) ? "000000000000" : phoneNumber;

                string strRequestBody = @"<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:v17='http://fedex.com/ws/ship/v17'>
<soapenv:Header/>
    <soapenv:Body>
       <v17:ProcessShipmentRequest>
           <v17:WebAuthenticationDetail>
              <v17:ParentCredential>
                   <v17:Key>" + FedExPrintLabel.Key + @"</v17:Key>
                   <v17:Password>" + FedExPrintLabel.Password + @"</v17:Password>
               </v17:ParentCredential>
               <v17:UserCredential>
                   <v17:Key>" + FedExPrintLabel.Key + @"</v17:Key>
                   <v17:Password>" + FedExPrintLabel.Password + @"</v17:Password>
                </v17:UserCredential>
           </v17:WebAuthenticationDetail>
           <v17:ClientDetail>
                 <v17:AccountNumber>" + FedExPrintLabel.AccountNumber + @"</v17:AccountNumber>
                 <v17:MeterNumber>" + FedExPrintLabel.MeterNumber + @"</v17:MeterNumber>
           </v17:ClientDetail>
                  <v17:TransactionDetail>
                        <v17:CustomerTransactionId>ProcessShip_Basic</v17:CustomerTransactionId>
                  </v17:TransactionDetail>
                  <v17:Version>
                        <v17:ServiceId>ship</v17:ServiceId>
                        <v17:Major>17</v17:Major>
                        <v17:Intermediate>0</v17:Intermediate>
                        <v17:Minor>0</v17:Minor>
                  </v17:Version>
                  <v17:RequestedShipment>
                            <v17:ShipTimestamp>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + @"</v17:ShipTimestamp>
                            <v17:DropoffType>REGULAR_PICKUP</v17:DropoffType>
                            <v17:ServiceType>" + Convert.ToString(objDt.Rows[0]["MappedCourierName"]) + @"</v17:ServiceType>
                            <v17:PackagingType>" + Convert.ToString(objDt.Rows[0]["PackageTypeName"]) + @"</v17:PackagingType>
                      <v17:Shipper>
                            <v17:AccountNumber>" + FedExPrintLabel.AccountNumber + @"</v17:AccountNumber>
                          <v17:Contact>
                                <v17:PersonName>" + Convert.ToString(objDt.Rows[0]["Name"]) + @"</v17:PersonName>
                                <v17:CompanyName>" + Convert.ToString(objDt.Rows[0]["Company"]) + @"</v17:CompanyName>
                                <v17:PhoneNumber>" + phone + @"</v17:PhoneNumber>
                                <v17:EMailAddress>" + Convert.ToString(objDt.Rows[0]["Email"]) + @"</v17:EMailAddress>
                           </v17:Contact>
                           <v17:Address>
                                <v17:StreetLines>" + Convert.ToString(objDt.Rows[0]["AddressLine1"]) + @"</v17:StreetLines>
                                <v17:StreetLines>" + Convert.ToString(objDt.Rows[0]["AddressLine2"]) + @"</v17:StreetLines>
                                <v17:City>" + Convert.ToString(objDt.Rows[0]["City"]) + @"</v17:City>
                                <v17:StateOrProvinceCode>" + Convert.ToString(objDt.Rows[0]["SettingCountryCode"]) + @"</v17:StateOrProvinceCode>
                                <v17:PostalCode>" + Convert.ToString(objDt.Rows[0]["ZipCode"]) + @"</v17:PostalCode>
                                <v17:CountryCode>" + Convert.ToString(objDt.Rows[0]["SettingCountryCode"]) + @"</v17:CountryCode>
                           </v17:Address>
                      </v17:Shipper>
                      <v17:Recipient>
                                <v17:Contact>
                                        <v17:PersonName>" + Convert.ToString(objDt.Rows[0]["FullName"]) + @"</v17:PersonName>
                                        <v17:CompanyName>" + Convert.ToString(objDt.Rows[0]["CompanyName"]) + @"</v17:CompanyName>
                                        <v17:PhoneNumber>" + phoneNumber + @"</v17:PhoneNumber>
                                        <v17:EMailAddress>" + Convert.ToString(objDt.Rows[0]["EmailAddress"]) + @"</v17:EMailAddress>
                                 </v17:Contact>
                                 <v17:Address>
                                        <v17:StreetLines>" + Convert.ToString(objDt.Rows[0]["Address1"]) + @"</v17:StreetLines>
                                        <v17:StreetLines>" + Convert.ToString(objDt.Rows[0]["Address2"]) + @"</v17:StreetLines>
                                        <v17:City>" + Convert.ToString(objDt.Rows[0]["Town"]) + @"</v17:City>
                                        <v17:StateOrProvinceCode>" + strOrderDetailsStateCode + @"</v17:StateOrProvinceCode>
                                        <v17:PostalCode>" + Convert.ToString(objDt.Rows[0]["PostCode"]) + @"</v17:PostalCode>
                                        <v17:CountryCode>" + Convert.ToString(objDt.Rows[0]["CountryCode"]) + @"</v17:CountryCode>
                                  </v17:Address>
                      </v17:Recipient>
                      <v17:Origin>
                                    <v17:Contact>
                                            <v17:PersonName>" + Convert.ToString(objDt.Rows[0]["Name"]) + @"</v17:PersonName>
                                            <v17:CompanyName>" + Convert.ToString(objDt.Rows[0]["Company"]) + @"</v17:CompanyName>
                                            <v17:PhoneNumber>" + phone + @"</v17:PhoneNumber>
                                            <v17:EMailAddress>" + Convert.ToString(objDt.Rows[0]["Email"]) + @"</v17:EMailAddress>
                                    </v17:Contact>
                                    <v17:Address>
                                            <v17:StreetLines>" + Convert.ToString(objDt.Rows[0]["AddressLine1"]) + @"</v17:StreetLines>
                                            <v17:StreetLines>" + Convert.ToString(objDt.Rows[0]["AddressLine2"]) + @"</v17:StreetLines>
                                            <v17:City>" + Convert.ToString(objDt.Rows[0]["City"]) + @"</v17:City>
                                            <v17:StateOrProvinceCode>" + Convert.ToString(objDt.Rows[0]["SettingCountryCode"]) + @"</v17:StateOrProvinceCode>
                                            <v17:PostalCode>" + Convert.ToString(objDt.Rows[0]["ZipCode"]) + @"</v17:PostalCode>
                                            <v17:CountryCode>" + Convert.ToString(objDt.Rows[0]["SettingCountryCode"]) + @"</v17:CountryCode>
                                    </v17:Address>
                      </v17:Origin>
                      <v17:ShippingChargesPayment>
                                    <v17:PaymentType>SENDER</v17:PaymentType>
                                    <v17:Payor>
                                        <v17:ResponsibleParty>
                                            <v17:AccountNumber>" + FedExPrintLabel.AccountNumber + @"</v17:AccountNumber>
                                                <v17:Contact>
                                                        <v17:PersonName>" + Convert.ToString(objDt.Rows[0]["Name"]) + @"</v17:PersonName>
                                                        <v17:CompanyName>" + Convert.ToString(objDt.Rows[0]["Company"]) + @"</v17:CompanyName>
                                                        <v17:PhoneNumber>" + phone + @"</v17:PhoneNumber>
                                                        <v17:EMailAddress>" + Convert.ToString(objDt.Rows[0]["Email"]) + @"</v17:EMailAddress>
                                                </v17:Contact>
                                                <v17:Address>
                                                        <v17:StreetLines>" + Convert.ToString(objDt.Rows[0]["AddressLine1"]) + @"</v17:StreetLines>
                                                        <v17:StreetLines>" + Convert.ToString(objDt.Rows[0]["AddressLine2"]) + @"</v17:StreetLines>
                                                        <v17:City>" + Convert.ToString(objDt.Rows[0]["City"]) + @"</v17:City>
                                                        <v17:StateOrProvinceCode>" + Convert.ToString(objDt.Rows[0]["SettingCountryCode"]) + @"</v17:StateOrProvinceCode>
                                                        <v17:PostalCode>" + Convert.ToString(objDt.Rows[0]["PostCode"]) + @"</v17:PostalCode>
                                                        <v17:CountryCode>" + Convert.ToString(objDt.Rows[0]["SettingCountryCode"]) + @"</v17:CountryCode>
                                                </v17:Address>
                                        </v17:ResponsibleParty>
                                    </v17:Payor>
                      </v17:ShippingChargesPayment>
                      <v17:CustomsClearanceDetail>
                           <v17:ImporterOfRecord>
                                <v17:Contact>
                                        <v17:PersonName>" + Convert.ToString(objDt.Rows[0]["FullName"]) + @"</v17:PersonName>
                                        <v17:CompanyName>" + Convert.ToString(objDt.Rows[0]["CompanyName"]) + @"</v17:CompanyName>
                                        <v17:PhoneNumber>" + phoneNumber + @"</v17:PhoneNumber>
                                        <v17:EMailAddress>" + Convert.ToString(objDt.Rows[0]["EmailAddress"]) + @"</v17:EMailAddress>
                                </v17:Contact>
                                <v17:Address>
                                        <v17:StreetLines>" + Convert.ToString(objDt.Rows[0]["Address1"]) + @"</v17:StreetLines>
                                        <v17:StreetLines>" + Convert.ToString(objDt.Rows[0]["Address2"]) + @"</v17:StreetLines>
                                        <v17:City>" + Convert.ToString(objDt.Rows[0]["Town"]) + @"</v17:City>
                                        <v17:StateOrProvinceCode>" + strOrderDetailsStateCode + @"</v17:StateOrProvinceCode>
                                        <v17:PostalCode>" + Convert.ToString(objDt.Rows[0]["PostCode"]) + @"</v17:PostalCode>
                                        <v17:CountryCode>" + Convert.ToString(objDt.Rows[0]["CountryCode"]) + @"</v17:CountryCode>
                                </v17:Address>
                           </v17:ImporterOfRecord>
                                 <v17:DutiesPayment>
                                      <v17:PaymentType>RECIPIENT</v17:PaymentType>
                                 </v17:DutiesPayment>
                                 <v17:DocumentContent>DOCUMENTS_ONLY</v17:DocumentContent>
                                 <v17:CustomsValue>
                                        <v17:Currency>" + strCurrency + @"</v17:Currency>
                                        <v17:Amount>" + Convert.ToString(objDt.Rows[0]["TotalCharge"]) + @"</v17:Amount>
                                 </v17:CustomsValue>
                                 <v17:CommercialInvoice>
                                        <v17:CustomerReferences>
                                                <v17:CustomerReferenceType>CUSTOMER_REFERENCE</v17:CustomerReferenceType>
                                                <v17:Value>" + Convert.ToString(objDt.Rows[0]["NumOrderId"]) + @"</v17:Value>
                                        </v17:CustomerReferences>
                                        <v17:TermsOfSale>FOB</v17:TermsOfSale>
                                 </v17:CommercialInvoice>
                                 <v17:Commodities>
                                         <v17:Name>ELECTRICAL ACCESSORIES</v17:Name>
                                         <v17:NumberOfPieces>1</v17:NumberOfPieces>
                                         <v17:Description>ELECTRICAL ACCESSORIES</v17:Description>
                                         <v17:CountryOfManufacture>CN</v17:CountryOfManufacture>
                                         <v17:Weight>
                                                      <v17:Units>KG</v17:Units>
                                                      <v17:Value>" + Convert.ToString(objDt.Rows[0]["TotalWeight"]) + @"</v17:Value>
                                         </v17:Weight>
                                         <v17:Quantity>1</v17:Quantity>
                                         <v17:QuantityUnits>EA</v17:QuantityUnits>
                                         <v17:UnitPrice>
                                                 <v17:Currency>" + strCurrency + @"</v17:Currency>
                                                 <v17:Amount>" + Convert.ToString(objDt.Rows[0]["TotalCharge"]) + @"</v17:Amount>
                                         </v17:UnitPrice>
                                         <v17:CustomsValue>
                                                 <v17:Currency>" + strCurrency + @"</v17:Currency>
                                                 <v17:Amount>" + Convert.ToString(objDt.Rows[0]["TotalCharge"]) + @"</v17:Amount>
                                         </v17:CustomsValue>
                                 </v17:Commodities>
                      </v17:CustomsClearanceDetail>
                      <v17:LabelSpecification>
                                <v17:LabelFormatType>COMMON2D</v17:LabelFormatType>
                                <v17:ImageType>PDF</v17:ImageType>
                                <v17:LabelStockType>" + FedExPrintLabel.LabelStockType + @"</v17:LabelStockType>
                      </v17:LabelSpecification>
                      <v17:RateRequestTypes>LIST</v17:RateRequestTypes>
                      <v17:PackageCount>1</v17:PackageCount>
                      <v17:RequestedPackageLineItems>
                                        <v17:SequenceNumber>1</v17:SequenceNumber>
                              <v17:Weight>
                                        <v17:Units>KG</v17:Units>
                                        <v17:Value>" + Convert.ToString(objDt.Rows[0]["TotalWeight"]) + @"</v17:Value>
                              </v17:Weight>
                              <v17:CustomerReferences>
                                        <v17:CustomerReferenceType>CUSTOMER_REFERENCE</v17:CustomerReferenceType>
                                        <v17:Value>" + Convert.ToString(objDt.Rows[0]["NumOrderId"]) + @"</v17:Value>
                              </v17:CustomerReferences>
                      </v17:RequestedPackageLineItems>
                  </v17:RequestedShipment>
          </v17:ProcessShipmentRequest>
       </soapenv:Body>
</soapenv:Envelope>";
                strLogText = string.Format("API call for {0} begins with meter number {1}, account number {2}, password {3} and key {4}",
                FedExPrintLabel.Url, FedExPrintLabel.MeterNumber, FedExPrintLabel.AccountNumber, FedExPrintLabel.Password, FedExPrintLabel.Key);
                var client = new RestClient(FedExPrintLabel.Url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("meternumber", FedExPrintLabel.MeterNumber);
                request.AddHeader("accountnumber", FedExPrintLabel.AccountNumber);
                request.AddHeader("password", FedExPrintLabel.Password);
                request.AddHeader("key", FedExPrintLabel.Key);
                request.AddParameter("text/xml", strRequestBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string strResponseData = response.Content;
                string strResult = string.Empty;
                strResponseStatus = response.StatusCode.ToString();
                strLogText = strLogText + "&&api call for " + FedExPrintLabel.Url + " completed with response " + strResponseStatus;

                //</v17:Severity> contains api internal errors like auth failer etc, rest we are getting in else block of this if
                if (strResponseData.ToUpper().Contains("</V17:SEVERITY>"))
                {
                    strResult = GlobalVariablesAndMethods.GetSubString(strResponseData, "<v17:Severity xmlns:v17=\"http://fedex.com/ws/ship/v17\">", "</v17:Severity>");
                }
                else
                {
                    strResult = GlobalVariablesAndMethods.GetSubString(strResponseData, "<Severity>", "</Severity>");
                }
                strLogText = strLogText + "&&Severity result " + strResult;

                if (strResult.Trim().ToUpper() == "SUCCESS" || strResult.Trim().ToUpper() == "WARNING")
                {
                    strTrackingNumber = GlobalVariablesAndMethods.GetSubString(strResponseData, "<TrackingNumber>", "</TrackingNumber>");
                    GlobalVariablesAndMethods.LogHistory(strLogText + "&&Got tracking number:- " + strTrackingNumber);
                    string strImage = GlobalVariablesAndMethods.GetSubString(strResponseData, "<Image>", "</Image>");
                    ConvertBase64ToPdf(strImage);
                }
                else
                {
                    strLogText = strLogText + "&&Error occurred in api call " + FedExPrintLabel.Url;
                    GetErrorInResponse(strResponseData);
                }
            }
            catch (Exception ex)
            {
                /*If it is not a custom exception then we will store that in log,
                 * otherwise not because for custom exception we are storing that log in GetErrorInResponse method
                 */
                if (!blIsCustomException)
                    GlobalVariablesAndMethods.LogHistory("Exception occurred in method CreateShipment, Error:- " + ex.Message);

                //Setting this flag off again so that other method can turn it on or of as per execution
                blIsCustomException = false;
                throw ex;
            }
            return strTrackingNumber;
        }

        /// <summary>
        /// Get error message from the response we got from FedEx apis
        /// </summary>
        /// <param name="strData">A complete FedEx response to find errors messages</param>
        private void GetErrorInResponse(string strData)
        {
            string strError = string.Empty;
            int i = 0;
        CheckErrorMessageAgain:
            if (strData.Contains("<Message>"))
            {
                i++;
                int intStartLength = strData.IndexOf("</Message>") + 10;
                int intEndLength = strData.IndexOf("</SOAP-ENV:Envelope>") + 20;
                string strErrorMessage = GlobalVariablesAndMethods.GetSubString(strData, "<Message>", "</Message>");
                strError = strError == string.Empty ? i.ToString() + ")" + strErrorMessage : strError + "\n" + i.ToString() + ")" + strErrorMessage;
                strLogText = strLogText + "&&" + (strError == string.Empty ? i.ToString() + ")" + strErrorMessage : strError + "&&" + i.ToString() + ")" + strErrorMessage);

                //Removing message node from api response data, so that we could read next message if we have
                strData = strData.Substring(intStartLength, intEndLength - intStartLength);
                goto CheckErrorMessageAgain;
            }
            else if (strData.Contains("<v17:Message"))
            {
                i++;
                int intStartLength = strData.IndexOf("</v17:Message>") + 14;
                int intEndLength = strData.IndexOf("</SOAP-ENV:Envelope>") + 20;
                string strErrorMessage = GlobalVariablesAndMethods.GetSubString(strData, "<v17:Message xmlns:v17=\"http://fedex.com/ws/ship/v17\">", "</v17:Message>");
                strError = strError == string.Empty ? i.ToString() + ")" + strErrorMessage : strError + "\n" + i.ToString() + ")" + strErrorMessage;
                strLogText = strLogText + "&&" + System.Text.RegularExpressions.Regex.Replace(strError, @"\\n", "&&");

                //Removing message node from api response data, so that we could read next message if we have
                strData = strData.Substring(intStartLength, intEndLength - intStartLength);
                goto CheckErrorMessageAgain;
            }
            GlobalVariablesAndMethods.LogHistory(strLogText);
            blIsCustomException = true;
            throw new CustomException(strError);
        }

        /// <summary>
        /// Cancel a FedEx order
        /// </summary>
        /// <param name="objDt">Details for order that we want to cancel</param>
        /// <param name="TrackingNumber">Tracking number of order that we want to cancel</param>
        /// <returns>Cancel API result</returns>
        public string CancelShipment(DataTable objDt, string TrackingNumber)
        {
            try
            {
                string strTrackingNumber = string.Empty;
                string strCurrency = Convert.ToString(objDt.Rows[0]["Currency"]);
                strCurrency = strCurrency.ToUpper() == "GBP" ? "UKL" : strCurrency;

                string strOrderDetailsRegion = Convert.ToString(objDt.Rows[0]["Region"]);
                string strOrderDetailsCountry = Convert.ToString(objDt.Rows[0]["CountryName"]);

                string strOrderDetailsStateCode = GlobalVariablesAndMethods.getStateCode(strOrderDetailsRegion, strOrderDetailsCountry);


                string strRequestBody = @"<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:v17='http://fedex.com/ws/ship/v17'>
        <soapenv:Header/>
            <soapenv:Body>
                     <v17:DeleteShipmentRequest>
                            <v17:WebAuthenticationDetail>
                                    <v17:ParentCredential>
                                            <v17:Key>" + FedExPrintLabel.Key + @"</v17:Key>
                                            <v17:Password>" + FedExPrintLabel.Password + @"</v17:Password>
                                     </v17:ParentCredential>
                                     <v17:UserCredential>
                                            <v17:Key>" + FedExPrintLabel.Key + @"</v17:Key>
                                            <v17:Password>" + FedExPrintLabel.Password + @"</v17:Password>
                                     </v17:UserCredential>
                            </v17:WebAuthenticationDetail>
                            <v17:ClientDetail>
                                            <v17:AccountNumber>" + FedExPrintLabel.AccountNumber + @"</v17:AccountNumber>
                                            <v17:MeterNumber>" + FedExPrintLabel.MeterNumber + @"</v17:MeterNumber>
                            </v17:ClientDetail>
                            <v17:TransactionDetail>
                                            <v17:CustomerTransactionId>Delete Shipment</v17:CustomerTransactionId>
                            </v17:TransactionDetail>
                            <v17:Version>
                                            <v17:ServiceId>ship</v17:ServiceId>
                                            <v17:Major>17</v17:Major>
                                            <v17:Intermediate>0</v17:Intermediate>
                                            <v17:Minor>0</v17:Minor>
                            </v17:Version>
                                            <v17:ShipTimestamp>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + @"</v17:ShipTimestamp>
                                            <v17:TrackingId>
                                                        <v17:TrackingIdType>EXPRESS</v17:TrackingIdType>
                                                        <v17:FormId>0201</v17:FormId>
                                                        <v17:TrackingNumber>" + TrackingNumber + @"</v17:TrackingNumber>
                                            </v17:TrackingId>
                                            <v17:DeletionControl>DELETE_ALL_PACKAGES</v17:DeletionControl>
                     </v17:DeleteShipmentRequest>
            </soapenv:Body>
        </soapenv:Envelope>";
                strLogText = string.Format("API call for {0} begins with meter number {1}, account number {2}, password {3} and key {4}",
                    FedExPrintLabel.Url, FedExPrintLabel.MeterNumber, FedExPrintLabel.AccountNumber, FedExPrintLabel.Password, FedExPrintLabel.Key);
                var client = new RestClient(FedExPrintLabel.Url);
                var request = new RestRequest(Method.POST);
                request.AddHeader("meternumber", FedExPrintLabel.MeterNumber);
                request.AddHeader("accountnumber", FedExPrintLabel.AccountNumber);
                request.AddHeader("password", FedExPrintLabel.Password);
                request.AddHeader("key", FedExPrintLabel.Key);
                request.AddParameter("text/xml", strRequestBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string strResponseData = response.Content;
                strResponseStatus = response.StatusCode.ToString();
                string strResult = string.Empty;
                strLogText = strLogText + "&&api call for " + FedExPrintLabel.Url + " completed with response " + strResponseStatus;

                //</v17:Severity> contains api internal errors like auth failer etc, rest we are getting in else block of this if
                if (strResponseData.ToUpper().Contains("</V17:SEVERITY>"))
                {
                    strResult = GlobalVariablesAndMethods.GetSubString(strResponseData, "<v17:Severity xmlns:v17=\"http://fedex.com/ws/ship/v17\">", "</v17:Severity>");
                }
                else
                {
                    strResult = GlobalVariablesAndMethods.GetSubString(strResponseData, "<Severity>", "</Severity>");
                }

                strLogText = strLogText + "&&Severity result:- " + strResult;
                //string strResult = GlobalVariablesAndMethods.GetSubString(strResponseData, "<Severity>", "</Severity>");
                if (strResult.Trim().ToUpper() != "SUCCESS" && strResult.Trim().ToUpper() != "WARNING")
                {
                    GetErrorInResponse(strResponseData);
                }
                GlobalVariablesAndMethods.LogHistory(strLogText + "&& CancelShipment completed successfully");
                return strResult.Trim().ToUpper();
            }
            catch (Exception ex)
            {
                /*If it is not a custom exception then we will store that in log,
                 * otherwise not because for custom exception we are storing that log in GetErrorInResponse method
                 */
                if (!blIsCustomException)
                    GlobalVariablesAndMethods.LogHistory("Exception occurred in method CancelShipment, Error:- " + ex.Message);

                //Setting this flag off again so that other method can turn it on or of as per execution
                blIsCustomException = false;
                throw ex;
            }
        }

        /// <summary>
        /// Convert Base64 bit label to pdf for the printing process
        /// </summary>
        /// <param name="barcode">Base64 label</param>
        public static void ConvertBase64ToPdf(string barcode)
        {
            byte[] bytes = Convert.FromBase64String(barcode);
            string filepath = System.IO.Path.GetTempPath(); //get temp folder path
            Random _r = new Random();
            int n = _r.Next();
            string file_location = filepath + n + ".pdf"; //image location
            System.IO.File.WriteAllBytes(file_location, bytes);
            LoadImagePdf(file_location, filepath);
            System.IO.File.Delete(file_location);
        }

        /// <summary>
        /// Convert pdf to image to pass to printer
        /// </summary>
        /// <param name="InputPDFFile">Location of pdf that we want to convert to image</param>
        /// <param name="OutPath">Location to save image after conversion from pdf</param>
        public static void LoadImagePdf(string InputPDFFile, string OutPath)
        {
            try
            {
                System.Collections.Generic.List<System.Drawing.Image> lstimgLabel = new System.Collections.Generic.List<Image>();
                RoyalMailPrintLabel.lstimgLabelToPrints = null;
                string outImageName = Path.GetFileNameWithoutExtension(InputPDFFile);
                PdfDocument pdf = PdfReader.Open(InputPDFFile, PdfDocumentOpenMode.Import);
                for (int i = 0; i < pdf.Pages.Count; i++)
                {
                    string str1 = Guid.NewGuid().ToString();
                    outImageName = outImageName + str1 + ".png";
                    string strOutputPath = OutPath + outImageName;
                    int point1 = (int)pdf.Pages[0].Height.Point;
                    int point2 = (int)pdf.Pages[0].Width.Point;
                    GhostscriptSettings settings = new GhostscriptSettings();
                    settings.Device = GhostscriptDevices.png256;
                    settings.Page.Start = i + 1;
                    settings.Page.End = pdf.Pages.Count;
                    settings.Size = new GhostscriptPageSize()
                    {
                        Manual = new Size(point2, point1)
                    };
                    Size size = new Size(400, 400);
                    settings.Resolution = size;
                    GhostscriptWrapper.GenerateOutput(InputPDFFile, strOutputPath, settings);
                    Bitmap bitmap = new Bitmap(strOutputPath);
                    try
                    {
                        //System.IO.File.Delete(InputPDFFile);
                        //System.IO.File.Delete(strOutputPath);
                    }
                    catch
                    {
                    }
                    System.Drawing.Image imgLabel = System.Drawing.Image.FromFile(strOutputPath);
                    lstimgLabel.Add(imgLabel);
                }
                FedExPrintLabel.lstLabel = lstimgLabel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
