﻿using Newtonsoft.Json;
using ODM.Data.Entity;
using ODM.Data.Util;
using RestSharp;
using System;
using System.Net.Http;

namespace ODM.Data.ShippingLabel
{
    public class ShippingLabelDPD
    {
        GlobalVariablesAndMethods globalVars = new GlobalVariablesAndMethods();

        static string UserName;
        static string Password;
        static string AccountNumber;
        static string SessionID;
        static string strShippingID;
        string strResponseCode = string.Empty;
        public ShippingLabelDPD()
        {
            UserName = GlobalVariablesAndMethods.APIUser;
            Password = GlobalVariablesAndMethods.APIPass;
            AccountNumber = GlobalVariablesAndMethods.AccountNumber;

            //UserName = "electrolve";
            //Password = "Electrolve321!";
        }

        public void LoginToDPDApi()
        {
            try
            {
                string strLoginCredentials = UserName + ":" + Password;
                string strBase64Value = globalVars.ConvertStringToBase64(strLoginCredentials);
                var client = new RestClient("https://api.dpd.co.uk/user/?action=login");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("authorization", "Basic " + strBase64Value);
                IRestResponse response = client.Execute(request);
                string JsonData = response.Content;
                strResponseCode = response.StatusCode.ToString();
                dynamic objData = JsonConvert.DeserializeObject(JsonData);
                if (JsonData.Contains("errorMessage"))
                {
                    GlobalVariablesAndMethods.LogHistory(string.Format("Error occurred in https://api.dpd.co.uk/user/?action=login api call,Status code:-{0}&&response:-{1}", strResponseCode, Convert.ToString(objData)));
                    GetErrorInResponse(objData);
                }
                SessionID = Convert.ToString(objData["data"]["geoSession"]);
                GlobalVariablesAndMethods.LogHistory(string.Format("api call https://api.dpd.co.uk/user/?action=login completed&&status code:- {0}&&got GeoSession id:- {1} ", strResponseCode, SessionID));
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Exception occurred in https://api.dpd.co.uk/user/?action=login api call, exception message:- "+ex.Message);
                throw;
            }
        }

        public void InsertShipment(string json)
        {
            try
            {
                LoginToDPDApi();
                var client = new RestClient("https://api.dpd.co.uk/shipping/shipment");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("content-length", "2416");
                request.AddHeader("host", "api.dpd.co.uk");
                request.AddHeader("geoclient", "account/" + AccountNumber);
                request.AddHeader("geosession", SessionID);
                request.AddParameter("application/json", json, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                strResponseCode = response.StatusCode.ToString();
                string JsonData = response.Content;
                dynamic objData = JsonConvert.DeserializeObject(JsonData);
                if (JsonData.Contains("errorMessage"))
                {
                    GlobalVariablesAndMethods.LogHistory(string.Format("Error occurred in https://api.dpd.co.uk/shipping/shipment api call,Status code:-{0}&&response:-{1}", response.StatusCode, Convert.ToString(objData)));
                    GetErrorInResponse(objData);
                }
                strShippingID = objData["data"]["shipmentId"];
                PrintLabel.TrackingNumber = Convert.ToString(objData["data"]["consignmentDetail"][0]["parcelNumbers"][0]);
                GlobalVariablesAndMethods.LogHistory(string.Format("api call https://api.dpd.co.uk/shipping/shipment completed&&Status code:- {0}&&got shipmentId id:-{1}&&Tracking number:- {2}",strResponseCode, strShippingID, PrintLabel.TrackingNumber));
                GetLabel(strShippingID);
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Exception occurred in https://api.dpd.co.uk/shipping/shipment api call, exception message:- " + ex.Message);
                throw;
            }
        }

        public void GetLabel(string strShipmentId)
        {
            try
            {
                using (var AuthClient = new HttpClient())
                {
                    var urls = "https://api.dpd.co.uk/shipping/shipment/" + strShippingID + "/label/";
                    //AuthClient.DefaultRequestHeaders.Add("cache-control", "no-cache");
                    AuthClient.DefaultRequestHeaders.Add("Accept", "text/vnd.citizen-clp");
                    AuthClient.DefaultRequestHeaders.Add("host", "api.dpd.co.uk");
                    AuthClient.DefaultRequestHeaders.Add("geoclient", "account/" + AccountNumber);
                    AuthClient.DefaultRequestHeaders.Add("geosession", SessionID);
                    AuthClient.BaseAddress = new Uri(urls);
                    HttpResponseMessage responsemsg = AuthClient.GetAsync(urls).Result;
                    strResponseCode = responsemsg.StatusCode.ToString();
                    string jsonData = responsemsg.Content.ReadAsStringAsync().Result;
                    GlobalVariablesAndMethods.LogHistory(string.Format("api call https://api.dpd.co.uk/shipping/shipment/ completed, Status code:- {0}",strResponseCode));
                    PrintLabel.LabelToPrint = jsonData;
                }
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Exception occurred in https://api.dpd.co.uk/shipping/shipment/ api call, exception message:- " + ex.Message);
                throw;
            }
        }
        private void GetErrorInResponse(dynamic objData)
        {
            string strError = string.Empty;
            for (int i = 0; i < objData["error"].Count; i++)
            {
                strError = strError == string.Empty ? (i + 1).ToString() + ")" + Convert.ToString(objData["error"][i]["errorMessage"]) : strError + "\n" + (i + 1).ToString() + ")" + Convert.ToString(objData["error"][i]["errorMessage"]);
            }
            throw new CustomException(strError);
        }
    }
}
