﻿using GhostscriptSharp;
using GhostscriptSharp.Settings;
using ODM.Data.Entity;
using ODM.Data.Util;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Xml;

namespace linnworks.finaware.CommonData.Objects                // leave untouched                            
{                                                                                               // leave untouched                            
    //public class ShippingLabelMacroHermes : linnworks.scripting.core.IShippingMacro                       // leave untouched                            
    public class ShippingLabelMacroHermes
    {                                                                                       // leave untouched                            
        // This is the weight in KG that will be used if the parcel weight cannot be determined from the order or is zero.
        // Every parcel must have a weight, and the weight affects the price of the parcel.
        // You should ensure that your orders and inventory correctly specify a weight so that this default will not be used.

        //static string defaultWeightInKg = "0.8";
        static string defaultWeightInKg;
        //static string APIuser = "ElectrolveLtd-Sit"; //test
        //static string APIPass = "0hFb6HTui87v";	//test

        //static string APIuser = "ElectrolveLtd";
        //static string APIPass = "0L07CrFDq5P7";
        //static bool UseLive = true;
        //// Change to false to switch off logging
        //static bool verboseLogging = true;

        static string APIuser;
        static string APIPass;
        static bool UseLive;
        // Change to false to switch off logging
        static bool verboseLogging;


        // -------------------------------------------------------------------------------------------------------------
        //
        // Initialisation
        //
        // -------------------------------------------------------------------------------------------------------------
        public List<linnworks.scripting.core.ReturnClass.ShippingLabel> Initialize(linnworks.scripting.core.Debugger debug, SqlConnection ActiveConnection, Order order, linnworks.scripting.core.Classes.OptionCollection ServiceOptions)           // leave untouched                            
        {                                                                                   // leave untouched                            
            // COPY FROM THIS LINE
            defaultWeightInKg = GlobalVariablesAndMethods.strDefaultWeightInKg;
            APIuser = GlobalVariablesAndMethods.APIUser;
            APIPass = GlobalVariablesAndMethods.APIPass;
            UseLive = GlobalVariablesAndMethods.UseLive;
            // Change to false to switch off logging
            verboseLogging = GlobalVariablesAndMethods.blVerboseLogging;
            initialise(debug);
            return getShippingLabel(order);
        }

        /**
        * Performs initialisation prior to script execution, eg loading preferences.
        *
        */
        public static void initialise(linnworks.scripting.core.Debugger debug)
        {
            createWorkingDirectory();
            clearLogFile();
            createLabelDirectory();
            LOG = new Logger(debug);
        }

        /**
        * Find the working directory and create it if not found.
        *
        */
        public static void createWorkingDirectory()
        {
            if (!verboseLogging) return;

            workingDirectory = getHomeDirectory() + "/linnworks";

            if (!System.IO.Directory.Exists(workingDirectory))
            {
                System.IO.Directory.CreateDirectory(workingDirectory);
            }
        }

        public static void createLabelDirectory()
        {
            if (!verboseLogging) return;
            labelDirectory = System.IO.Path.GetTempPath() + "/MyLabels";
            if (!System.IO.Directory.Exists(labelDirectory))
            {
                System.IO.Directory.CreateDirectory(labelDirectory);
            }
        }

        /**
        * Find the user's home directory.
        *
        */
        public static string getHomeDirectory()
        {
            return (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX)
                ? Environment.GetEnvironmentVariable("HOME")
                : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
        }

        // -------------------------------------------------------------------------------------------------------------
        //
        // API configuration
        //
        // -------------------------------------------------------------------------------------------------------------

        /**
        * Get the full URI for a parcels request.
        *
        */
        public static string getParcelsUri()
        {
            return root;
        }

        // -------------------------------------------------------------------------------------------------------------
        //
        // Get Labels
        //
        // -------------------------------------------------------------------------------------------------------------


        /**
        * Export an order to create a parcel in Hermes, returning shipping labels.
        *
        */
        public static List<linnworks.scripting.core.ReturnClass.ShippingLabel> getShippingLabel(Order order)
        {
            LOG.blankLines(2);
            LOG.Log("Executing with script version '" + scriptVersion + "'");
            string parcelSummaries = exportToParcels(order);
            return getShippingLabelFor(order, parcelSummaries);
        }


        /**
        * Convert Order to 'parcels' XML, submit to Hermes to create a parcel, obtaining tracking barcodes.
        *
        */
        public static string exportToParcels(Order order)
        {
            LOG.Log("Exporting order with id '" + order.NumOrderId + "' to myHermes parcels");
            string parcelXML = parcelXMLFor(order);
            LOG.Log("Converted order to XML:");
            LOG.Log(parcelXML);
            string parcelSummaries = postForString(getParcelsUri(), parcelXML);
            LOG.Log(parcelSummaries);
            LOG.Log("Finished exporting order with id '" + order.NumOrderId + "' to myHermes parcels");

            // This if block is used for custom error handling
            if (parcelSummaries.Contains("errorDescription"))
            {
                string strError = GlobalVariablesAndMethods.GetSubString(parcelSummaries, "<errorDescription>", "</errorDescription>");
                throw (new HermesCustomException(strError));
            }
            //End of custom error handling

            return parcelSummaries;
        }

        /**
        * Get a shipping label for the tracking barcode.
        *
        */
        public static List<linnworks.scripting.core.ReturnClass.ShippingLabel> getShippingLabelFor(Order order, string parcelSummaries)
        {
            LOG.Log("Retrieving shipping label for order with id '" + order.NumOrderId + "'");
            //string barcode = firstValueOf(parcelSummaries);
            string barcode = getBarcodeNumber(parcelSummaries);
            LOG.Log("Got TrackingNbr: " + barcode);
            string labelBase64 = getLabelString(parcelSummaries);
            LOG.Log("Got Label1"); //: " + labelBase64);
            LOG.Log("Got Label1 : " + labelBase64);
            List<linnworks.scripting.core.ReturnClass.ShippingLabel> labels = new List<linnworks.scripting.core.ReturnClass.ShippingLabel>();
            labels.Add(labelFor(barcode, labelBase64));
            LOG.Log("Finished retrieving shipping label for order with id '" + order.NumOrderId + "'");
            return labels;
        }

        /**
        * Get a label image for the given barcode and create the Linnworks shipping label.
        *
        */
        public static linnworks.scripting.core.ReturnClass.ShippingLabel labelFor(string tracking, string barcode)
        {
            byte[] bytes = Convert.FromBase64String(barcode);
            string filepath = System.IO.Path.GetTempPath(); //get temp folder path
            Random _r = new Random();
            int n = _r.Next();
            string file_location = filepath + n + ".pdf"; //image location
            LOG.Log("File Path: " + file_location);
            System.IO.File.WriteAllBytes(file_location, bytes);
            string folder = System.IO.Path.GetTempPath() + "/MyLabels";
            System.Drawing.Image image = LoadImagePdf(file_location, filepath);
            System.IO.File.Delete(file_location);
            return new linnworks.scripting.core.ReturnClass.ShippingLabel(tracking, "", image);
        }

        public static System.Drawing.Image LoadImagePdf(string InputPDFFile, string OutPath)
        {
            string outImageName = Path.GetFileNameWithoutExtension(InputPDFFile);
            outImageName = outImageName + ".png";
            string strOutputPath = OutPath + outImageName;
            PdfDocument pdf = PdfReader.Open(InputPDFFile, PdfDocumentOpenMode.Import);
            int point1 = (int)pdf.Pages[0].Height.Point;
            int point2 = (int)pdf.Pages[0].Width.Point;
            string str1 = Guid.NewGuid().ToString();
            GhostscriptSettings settings = new GhostscriptSettings();
            settings.Device = GhostscriptDevices.png256;
            settings.Size = new GhostscriptPageSize()
            {
                Manual = new Size(point2, point1)
            };
            Size size = new Size(300, 300);
            settings.Resolution = size;
            GhostscriptWrapper.GenerateOutput(InputPDFFile, strOutputPath, settings);
            Bitmap bitmap = new Bitmap(strOutputPath);
            try
            {
                System.IO.File.Delete(InputPDFFile);
                System.IO.File.Delete(strOutputPath);
            }
            catch
            {
            }
            System.Drawing.Image imgLabel = System.Drawing.Image.FromFile(strOutputPath);
            return imgLabel;
        }

        // -------------------------------------------------------------------------------------------------------------
        //
        // Data Mapping
        //
        // -------------------------------------------------------------------------------------------------------------


        /**
        * Convert an Order to XML.
        *
        */
        public static string parcelXMLFor(Order order)
        
        {
            Dictionary<String, String> parameters = new Dictionary<string, string>();
            parameters.Add("creationDate", DateTime.Now.ToString("yyyy-MM-dd")); //Bill Sender
            parameters.Add("userId", APIuser); // can be disabled?
            parameters.Add("sourceOfRequest", "CLIENTWS");

            //		getPhone(order.BuyerPhoneNumber.ToString())

            Dictionary<String, String> toAddress = new Dictionary<string, string>();
            toAddress.Add("title", "");
            toAddress.Add("firstName", "");
            toAddress.Add("lastName", order.FullName.ToString());
            toAddress.Add("houseNo", "");
            toAddress.Add("streetName", order.Address1.ToString());
            toAddress.Add("countryCode", order.CountryCode.ToString());
            //toAddress.Add("countryCode", "");
            toAddress.Add("postCode", order.PostCode.ToString().ToUpper());
            toAddress.Add("city", order.Town.ToString());
            toAddress.Add("addressLine1", order.Address2.ToString());
            toAddress.Add("addressLine2", order.Company.ToString());
            toAddress.Add("addressLine3", order.Address3.ToString());
            toAddress.Add("region", order.Country.ToString());

            Dictionary<String, String> toContact = new Dictionary<string, string>();
            toContact.Add("homePhoneNo", getPhone(order.BuyerPhoneNumber.ToString()));
            toContact.Add("workPhoneNo", getPhone(order.BuyerPhoneNumber.ToString()));
            toContact.Add("mobilePhoneNo", getPhone(order.BuyerPhoneNumber.ToString()));
            toContact.Add("faxNo", "");
            toContact.Add("email", order.EmailAddress.ToString());
            toContact.Add("customerReference1", order.NumOrderId.ToString());
            //toContact.Add("customerReference1", "HTEST");
            toContact.Add("customerReference2", order.ExternalReferenceNum.ToString());
            toContact.Add("customerAlertType", "");
            toContact.Add("customerAlertGroup", "");
            toContact.Add("deliveryMessage", "do not leave without instruction");
            toContact.Add("specialInstruction1", "");
            toContact.Add("specialInstruction2", "");

            Dictionary<String, String> parcel = new Dictionary<string, string>();
            parcel.Add("weight", getParcelWeightInKg(order).ToString());
            parcel.Add("length", "0");
            parcel.Add("width", "0");
            parcel.Add("depth", "0");
            parcel.Add("girth", "0");
            parcel.Add("combinedDimension", "0");
            parcel.Add("volume", "0");
            parcel.Add("currency", "GBP");
            parcel.Add("value", (order.TotalCharge * 100).ToString());
            parcel.Add("numberOfParts", "1");
            parcel.Add("numberOfItems", "1");
            parcel.Add("description", "eTwist Parcel");
            parcel.Add("originOfParcel", "UK");

            Dictionary<String, String> services = new Dictionary<string, string>();
            services.Add("nextDay", NextDay(order.ShippingMethod.ToString()));
            services.Add("signature", SignedFor(order.ShippingMethod.ToString()));
            //		services.Add("nextDay", "false");
            //		services.Add("signature", "false");


            //Please fill up the requrn address below
            Dictionary<String, String> returnAddress = new Dictionary<string, string>();
            returnAddress.Add("addressLine1", "Electrolve Ltd");
            returnAddress.Add("addressLine2", "148 Caistor Road");
            returnAddress.Add("addressLine3", "Laceby");
            returnAddress.Add("addressLine4", "DN37 7JG");

            Dictionary<String, String> closing = new Dictionary<string, string>();
            closing.Add("expectedDespatchDate", DateTime.Now.ToString("yyyy-MM-dd"));
            closing.Add("countryOfOrigin", "GB");

            XmlDocument soapEnvelopeXml = new XmlDocument();

            //		String xml = System.IO.File.ReadAllText("C:\\Users\\Oliver\\SkyDrive\\Business\\Electrolve\\Hermes\\Electrolve Ltd (1).txt");

            String xml = @"<APIuser>" + APIuser + @"</APIuser>
									<APIPass>" + APIPass + @"</APIPass>
									<useLive>" + UseLive.ToString().ToLower() + @"</useLive>
									<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v3=""http://v3.web.domain.routing.hermes.co.uk/"">
   									<soapenv:Header/>
   									<soapenv:Body>
      								<v3:routeDeliveryCreatePreadviceAndLabel>
         							<deliveryRoutingRequest>
            						<clientId>317</clientId>
            						<clientName>eTwist</clientName>
            						<batchNumber>1</batchNumber>";
            foreach (KeyValuePair<String, String> item in parameters)
            {
                xml += Environment.NewLine + "<" + item.Key + ">" + item.Value + @"</" + item.Key + ">";
            }

            xml += Environment.NewLine + @"<deliveryRoutingRequestEntries>
               						<deliveryRoutingRequestEntry>
               						<addressValidationRequired>false</addressValidationRequired>
               						<customer>
               						<address>";

            foreach (KeyValuePair<String, String> item in toAddress)
            {
                xml += Environment.NewLine + "<" + item.Key + ">" + item.Value + @"</" + item.Key + ">";
            }
            xml += Environment.NewLine + @"</address>";

            foreach (KeyValuePair<String, String> item in toContact)
            {
                xml += Environment.NewLine + "<" + item.Key + ">" + item.Value + @"</" + item.Key + ">";
            }
            xml += Environment.NewLine + @"</customer>
                  <parcel>";

            foreach (KeyValuePair<String, String> item in parcel)
            {
                xml += Environment.NewLine + "<" + item.Key + ">" + item.Value + @"</" + item.Key + ">";
            }
            xml += Environment.NewLine + @"</parcel>";

            //Exclude <service> node if the request if of "Hermes International"
            if (order.ShippingMethod.Trim().ToUpper() != "HERMES INTERNATIONAL")
            {
                xml += Environment.NewLine + @"<services>";

                foreach (KeyValuePair<String, String> item in services)
                {
                    xml += Environment.NewLine + "<" + item.Key + ">" + item.Value + @"</" + item.Key + ">";
                }
                xml += Environment.NewLine + @"</services>";
            }
            //End of Excluding <service> node if the request if of "Hermes International"

            xml += Environment.NewLine + @"<senderAddress>";

            foreach (KeyValuePair<String, String> item in returnAddress)
            {
                xml += Environment.NewLine + "<" + item.Key + ">" + item.Value + @"</" + item.Key + ">";
            }
            xml += Environment.NewLine + @"</senderAddress>";

            foreach (KeyValuePair<String, String> item in closing)
            {
                xml += Environment.NewLine + "<" + item.Key + ">" + item.Value + @"</" + item.Key + ">";
            }

            xml += @"</deliveryRoutingRequestEntry>
            </deliveryRoutingRequestEntries>
         </deliveryRoutingRequest>
      </v3:routeDeliveryCreatePreadviceAndLabel>
   </soapenv:Body>
</soapenv:Envelope>";
            return xml;
        }

        public static string getParcelWeightInKg(Order order)
        {
            if (order.Pack_TotalWeight == null || order.Pack_TotalWeight <= 0.0)
            {
                return useDefaultWeight();
            }
            return (order.Pack_TotalWeight * 1000).ToString();
        }

        /**
        * Checks that defaultWeightInKg is a number and returns the value as a string.
        *
        */
        public static string useDefaultWeight()
        {
            LOG.Log("Order total weight not set or is zero. Using defaultWeightInKg which is set to '" + defaultWeightInKg + "'");
            double defaultWeightNumber = double.Parse(defaultWeightInKg.Trim(), System.Globalization.NumberStyles.AllowDecimalPoint);
            return defaultWeightNumber.ToString();
        }

        /**
        * Item description includes the item title and the quantity, and the whole description is limited to a maximum length.
        * eg an order for 1 apple and 2 bananas will have a description "apple, banana x2". Two properties control this method:
        * minDescriptionQuantity sets the smallest quantity at which any description is shown.
        * minMultiplierQuantity sets the smallest quantity at which the multiplier is shown.
        * eg if minDescriptionQuantity=1 and an item has quantity 0, it will not appear in the description.
        * eg if minMultiplierQuantity=2 and an item has quantity 1, it will show as a title without multiplier.
        * eg if the above two settings are true and an item has quantity 2, it will show as 'title xQuantity'.
        * If either property is set to a negative value, quantity will not affect the output.
        */
        public static string getItemDescriptionFor(List<OrderItem> orderItems)
        {
            System.Text.StringBuilder descriptionBuilder = new System.Text.StringBuilder();
            foreach (OrderItem item in orderItems)
            {
                LOG.Log(item.Title + " - " + item.Qty);
                if (minDescriptionQuantity >= 0 && item.Qty < minDescriptionQuantity)
                {
                    continue;
                }
                string itemTitle = titleFor(item);
                string multiplier = multiplierFor(item);
                descriptionBuilder.Append(itemTitle + multiplier);
                descriptionBuilder.Append(", ");
            }
            string description = descriptionBuilder.ToString();
            if (description.Length > MAX_DESCRIPTION_LENGTH)
            {
                return description.Substring(0, MAX_DESCRIPTION_LENGTH);
            }
            else if (description.EndsWith(", "))
            {
                return description.Remove(description.Length - 2, 2).ToString();
            }

            if (description.Length <= 0)
            {
                return "Not specified";
            }
            return description;
        }

        /**
        * Generate the item title to use in the parcel description.
        *
        */
        public static string titleFor(OrderItem item)
        {

            if (item.Title.Length <= 0)
            {
                return "Unknown";
            }

            return item.Title;
        }

        /**
        * Generate the multiplier to use for the item in the parcel description.
        *
        */
        public static string multiplierFor(OrderItem item)
        {
            if (minMultiplierQuantity >= 0 && item.Qty < minMultiplierQuantity)
            {
                return "";
            }
            return " x" + item.Qty;
        }

        /**
        * Limits reference number to max allowed length.
        *
        */
        public static string getReference(Order order)
        {
            string refNum = order.ReferenceNum.ToString();
            if (refNum.Length > MAX_REFERENCE_LENGTH)
            {
                return refNum.Substring(0, MAX_REFERENCE_LENGTH);
            }
            return refNum;
        }

        /**
        * Attempts to find the value of the first occurrence of the given property.
        *
        */
        public static string firstValueOf(string XML)
        {
            System.Xml.XmlDocument responseXML = new System.Xml.XmlDocument();
            responseXML.LoadXml(XML);
            string data = "";
            data = responseXML.SelectSingleNode("barcodeNumber").InnerText;
            foreach (XmlNode n in responseXML.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes)
            {
                if (n.Name == "barcodeNumber")
                    data = n.InnerText;
            }
            return data;
        }

        public static string getBarcodeNumber(string xml)
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(Envelope));
            string barcode;
            using (System.IO.TextReader reader = new System.IO.StringReader(xml))
            {
                Envelope result = (Envelope)serializer.Deserialize(reader);
                barcode = result.Body.routeDeliveryCreatePreadviceAndLabelResponse.routingResponse.routingResponseEntries.routingResponseEntry.outboundCarriers.carrier1.barcode1.barcodeNumber;
            }
            return barcode;
        }

        public static string getLabelString(string xml)
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(Envelope));
            string label;
            using (System.IO.TextReader reader = new System.IO.StringReader(xml))
            {
                Envelope result = (Envelope)serializer.Deserialize(reader);
                label = result.Body.routeDeliveryCreatePreadviceAndLabelResponse.routingResponse.routingResponseEntries.routingResponseEntry.outboundCarriers.labelImage;
            }
            return label;
        }

        public static string GetLabel(string XML)
        {
            System.Xml.XmlDocument responseXML = new System.Xml.XmlDocument();
            responseXML.LoadXml(XML);
            string data = "";
            data = responseXML.SelectSingleNode("labelImage").InnerText;
            return data;
        }

        /**
        * Count how many times the given property appears in the XML.
        *
        */
        public static int countOccurrences(string key, string XML)
        {
            int count = 0;
            foreach (string s in XML.Split(','))
            {
                if (s.Contains(key))
                {
                    count++;
                }
            }
            return count;
        }




        // -------------------------------------------------------------------------------------------------------------
        //
        // Networking
        //
        // -------------------------------------------------------------------------------------------------------------



        /**
        * Post content to uri as contentType, returning response content as string if it is acceptable content type.
        *
        */
        public static string postForString(string uri, string content)
        {
            configureSsl();
            System.Net.HttpWebRequest request = createRequest(uri);

            request.Method = "POST";
            request.ContentType = "application/json";
            setRequestContent(request, content);

            System.Net.HttpWebResponse response = execute(request);

            string responseString = getResponseAsString(response);
            LOG.Log("Response Received"); //Content: " + responseString);					

            return responseString;
        }

        /**
        * Construct an http request for the given uri. Will fail if URI syntax is not valid, but does not try to access the URI.
        *
        */
        public static System.Net.HttpWebRequest createRequest(string uri)
        {

            LOG.Log("Creating request for uri: " + uri);
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(uri);
            LOG.Log("Created request");
            return request;

        }

        /**
        * Perform an http request. Will fail for unreachable host,not found, etc
        *
        */
        public static System.Net.HttpWebResponse execute(System.Net.HttpWebRequest request)
        {

            logRequest(request);
            System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
            LOG.Log("Response Status: " + response.StatusCode.ToString());

            return response;

        }


        /**
        * Put the content into the http request.
        *
        */
        public static void setRequestContent(System.Net.HttpWebRequest request, string content)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(content);
            request.ContentLength = bytes.Length;
            System.IO.Stream os = request.GetRequestStream();
            os.Write(bytes, 0, bytes.Length);
            os.Close();
        }

        /**
        * Read response content into a string.
        *
        */
        public static string getResponseAsString(System.Net.HttpWebResponse response)
        {
            try
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                return sr.ReadToEnd();

            }
            catch (Exception e)
            {
                failWithError("Failed to read content from http response", e);
                return null;
            }
        }

        /**
        * Apply configuration to avoid SSL errors.
        *
        */
        public static void configureSsl()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllSslCertificates);
        }

        /**
        * Ignore all SSL certificate errors.
        *
        */
        public static bool AcceptAllSslCertificates(
                object sender,
                System.Security.Cryptography.X509Certificates.X509Certificate certification,
                System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {

            return true;
        }



        // -------------------------------------------------------------------------------------------------------------
        //
        // Logging
        //
        // -------------------------------------------------------------------------------------------------------------

        /**
        * Get the current log file. Current log is appended to until full, then it is copied to the backup, replacing previous backup.
        *
        */
        public static string getLogFileName()
        {
            return workingDirectory + "/Hermes.txt";
        }

        /**
        * Get the backup log file. 
        *
        */
        public static string getBackupLogFileName()
        {
            return workingDirectory + "/log-backup.txt";
        }

        /**
        * Keep log file below max size.
        *
        */
        public static void clearLogFile()
        {
            if (!verboseLogging) return;
            if (System.IO.File.Exists(getLogFileName()))
            {
                System.IO.FileInfo logFileInfo = new System.IO.FileInfo(getLogFileName());
                if (logFileInfo.Length > LOG_FILE_LENGTH)
                {
                    if (System.IO.File.Exists(getBackupLogFileName()))
                    {
                        System.IO.File.Delete(getBackupLogFileName());
                    }
                    System.IO.File.Move(getLogFileName(), getBackupLogFileName());
                }
            }
        }

        /**
        * Log the details of an http request, including headers.
        *
        */
        public static void logRequest(System.Net.HttpWebRequest request)
        {

            LOG.Log("Executing request");

            System.Net.WebHeaderCollection headers = request.Headers;

            for (int i = 0; i < headers.Count; ++i)
            {
                string header = headers.GetKey(i);

                foreach (string value in headers.GetValues(i))
                {
                    LOG.Log("Request header <" + header + "> with value <" + value + ">");
                }
            }
        }


        /**
        * Log the given message with a date and time stamp, and report the same message to debug output.
        *
        */
        public class Logger
        {

            private linnworks.scripting.core.Debugger debug;

            public Logger(linnworks.scripting.core.Debugger debug)
            {
                this.debug = debug;
            }

            /**
            * Output a number of blank lines in the log for clear separation between activities.
            *
            */
            public void blankLines(int number)
            {
                for (int i = 0; i < number; i++)
                {
                    Log("");
                }
            }

            /**
            * Log the message with a date and timestamp, also reporting to debugger console.
            *
            */
            public void Log(string msg)
            {

                debug.AddEntry(msg);

                if (!verboseLogging) return;

                using (System.IO.StreamWriter w = System.IO.File.AppendText(getLogFileName()))
                {
                    DateTime now = DateTime.Now;
                    w.WriteLine("{0} {1} - {2}", now.ToLongDateString(), now.ToLongTimeString(), msg);
                    w.WriteLine("");
                }

                //Storing log messages of linnwork those have length less that 200 chars and don't have header
                if (!string.IsNullOrEmpty(msg) && msg.Length <= 200 && !msg.Contains("Request header <") && !msg.Contains(".pdf"))
                {
                    GlobalVariablesAndMethods.LogHistory(msg);
                }
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        //
        // Error Handling
        //
        // -------------------------------------------------------------------------------------------------------------

        /**
        * Helps distinguish our own exceptions from standard ones in debug output etc.
        *
        */
        public class HermesException : Exception
        {
            public HermesException(String msg) : base(msg) { }
        }

        /**
        * Report a message to the user and the log file. Report details and exception details to the log file.
        *
        */
        public static void failWithError(String msg, string details, Exception e)
        {
            LOG.Log(msg);
            LOG.Log(details);
            LOG.Log(e.Message);
            LOG.Log(e.StackTrace);
            throw new HermesException(msg);
        }

        /**
        * Report a message to the user and the log file. Report exception details to the log file.
        *
        */
        public static void failWithError(String msg, Exception e)
        {

            LOG.Log(msg);
            LOG.Log(e.GetType().ToString());
            LOG.Log(e.Message);
            LOG.Log(e.StackTrace);

            throw new HermesException(msg);
        }

        /**
        * Report a message to the user and the log file. Report details to the log file.
        *
        */
        public static void failWithError(String msg, string details)
        {
            LOG.Log(msg);
            LOG.Log(details);
            throw new HermesException(msg);
        }

        /**
        * Report a message to the user and the log file. Prefer other methods giving more details for the log file.
        *
        */
        public static void failWithError(String msg)
        {
            LOG.Log(msg);
            throw new HermesException(msg);
        }

        public static string SelectServiceCode(string ShippingMethod)
        {
            Dictionary<string, string> SelectServiceCode = new Dictionary<string, string>();
            SelectServiceCode.Add("Spring Packet", "1351");
            SelectServiceCode.Add("Spring Packet Plus", "1352");
            SelectServiceCode.Add("Spring Parcel", "1353");
            string serviceCode = SelectServiceCode[ShippingMethod];
            return serviceCode;
        }

        public static string getPhone(string phone)
        {
            if (phone == "")
            {
                return "07814994875";
            }
            else
            {
                return phone;
            }
        }

        public static string FTPUpload(string file, string name)
        {
            //	System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)System.Net.WebRequest.Create("ftp://ftp.grimsbywholesale.co.uk/" + name);
            System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)System.Net.WebRequest.Create("ftp://ftp.swiftecommerce.co.uk/" + name);
            request.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
            request.EnableSsl = true;
            request.UsePassive = true;
            //	request.Credentials = new System.Net.NetworkCredential ("hermes@grimsbywholesale.co.uk","4SKtaN-C$^=I");
            request.Credentials = new System.Net.NetworkCredential("hermes@swiftecommerce.co.uk", "@k(QFww5w6B=");
            //	request.Credentials = new System.Net.NetworkCredential ("oliver@electrolve.co.uk","FUJnd78BPH321!");
            System.IO.FileStream stream = System.IO.File.OpenRead(file);
            byte[] fileContents = new byte[stream.Length];
            stream.Read(fileContents, 0, fileContents.Length);
            stream.Close();
            System.IO.Stream requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();
            System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)request.GetResponse();
            string ftpresponse = response.StatusDescription.ToString();
            response.Close();
            return ftpresponse;
        }

        public static string DeleteFile(string name)
        {
            //		System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)System.Net.WebRequest.Create("ftp://ftp.grimsbywholesale.co.uk/" + name);
            System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)System.Net.WebRequest.Create("ftp://ftp.swiftecommerce.co.uk/" + name);
            request.Method = System.Net.WebRequestMethods.Ftp.DeleteFile;
            request.EnableSsl = true;
            request.UsePassive = true;
            //		request.Credentials = new System.Net.NetworkCredential("hermes@grimsbywholesale.co.uk","4SKtaN-C$^=I");
            request.Credentials = new System.Net.NetworkCredential("hermes@swiftecommerce.co.uk", "@k(QFww5w6B=");
            using (System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)request.GetResponse())
            {
                return response.StatusDescription.ToString();
            }
        }

        public static string NextDay(string ShippingMethod)
        {
            Dictionary<string, string> NextDay = new Dictionary<string, string>();
            NextDay.Add("Hermes UK Standard", "false");
            NextDay.Add("Hermes International", "false");
            NextDay.Add("Hermes UK SignedFor", "false");
            NextDay.Add("Hermes UK Next Day", "true");
            NextDay.Add("Hermes UK Next Day SignedFor", "true");
            string flag = NextDay[ShippingMethod];
            return flag;
        }

        public static string SignedFor(string ShippingMethod)
        {
            Dictionary<string, string> SignedFor = new Dictionary<string, string>();
            SignedFor.Add("Hermes UK Standard", "false");
            SignedFor.Add("Hermes International", "false");
            SignedFor.Add("Hermes UK SignedFor", "true");
            SignedFor.Add("Hermes UK Next Day", "false");
            SignedFor.Add("Hermes UK Next Day SignedFor", "true");
            string flag = SignedFor[ShippingMethod];
            return flag;
        }

        // -------------------------------------------------------------------------------------------------------------
        //
        // System Properties - DO NOT CHANGE
        //
        // -------------------------------------------------------------------------------------------------------------

        // SYSTEM EXECUTION PROPERTIES
        static string root = "http://hermeslabels.azurewebsites.net/api/hermeslabels";
        static string scriptVersion = "1";

        // DESCRIPTION PROPERTIES
        // Add item to parcel description only when item quantity is this value or more. Set to -1 to always show.
        static int minDescriptionQuantity = -1;
        // Add multiplier to this item in parcel description only when item quantity is this value or more.  Set to -1 to always show.
        static int minMultiplierQuantity = 2;

        // FIXED PROPERTIES						
        static int MAX_DESCRIPTION_LENGTH = 255;
        static int MAX_REFERENCE_LENGTH = 32;
        static int LOG_FILE_LENGTH = 500000;

        static Logger LOG;

        static string workingDirectory;
        static string labelDirectory;

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false)]
        public partial class Envelope
        {

            private EnvelopeBody bodyField;

            /// <remarks/>
            public EnvelopeBody Body
            {
                get
                {
                    return this.bodyField;
                }
                set
                {
                    this.bodyField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public partial class EnvelopeBody
        {
            private routeDeliveryCreatePreadviceAndLabelResponse routeDeliveryCreatePreadviceAndLabelResponseField;
            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://v3.web.domain.routing.hermes.co.uk/")]
            public routeDeliveryCreatePreadviceAndLabelResponse routeDeliveryCreatePreadviceAndLabelResponse
            {
                get
                {
                    return this.routeDeliveryCreatePreadviceAndLabelResponseField;
                }
                set
                {
                    this.routeDeliveryCreatePreadviceAndLabelResponseField = value;
                }
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://v3.web.domain.routing.hermes.co.uk/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://v3.web.domain.routing.hermes.co.uk/", IsNullable = false)]
        public partial class routeDeliveryCreatePreadviceAndLabelResponse
        {
            private routeDeliveryCreatePreadviceAndLabelResponseRoutingResponse routingResponseField;
            /// <remarks/>
            public routeDeliveryCreatePreadviceAndLabelResponseRoutingResponse routingResponse
            {
                get
                {
                    return this.routingResponseField;
                }
                set
                {
                    this.routingResponseField = value;
                }
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://v3.web.domain.routing.hermes.co.uk/")]
        public partial class routeDeliveryCreatePreadviceAndLabelResponseRoutingResponse
        {

            private ushort clientIdField;

            private string clientNameField;

            private string clientLogoRefField;

            private byte batchNumberField;

            private System.DateTime creationDateField;

            private routingResponseEntries routingResponseEntriesField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public ushort clientId
            {
                get
                {
                    return this.clientIdField;
                }
                set
                {
                    this.clientIdField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string clientName
            {
                get
                {
                    return this.clientNameField;
                }
                set
                {
                    this.clientNameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string clientLogoRef
            {
                get
                {
                    return this.clientLogoRefField;
                }
                set
                {
                    this.clientLogoRefField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public byte batchNumber
            {
                get
                {
                    return this.batchNumberField;
                }
                set
                {
                    this.batchNumberField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public System.DateTime creationDate
            {
                get
                {
                    return this.creationDateField;
                }
                set
                {
                    this.creationDateField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public routingResponseEntries routingResponseEntries
            {
                get
                {
                    return this.routingResponseEntriesField;
                }
                set
                {
                    this.routingResponseEntriesField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class routingResponseEntries
        {

            private routingResponseEntriesRoutingResponseEntry routingResponseEntryField;

            /// <remarks/>
            public routingResponseEntriesRoutingResponseEntry routingResponseEntry
            {
                get
                {
                    return this.routingResponseEntryField;
                }
                set
                {
                    this.routingResponseEntryField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntry
        {

            private routingResponseEntriesRoutingResponseEntryDestinationAddress destinationAddressField;

            private routingResponseEntriesRoutingResponseEntrySenderAddress senderAddressField;

            private routingResponseEntriesRoutingResponseEntryOutboundCarriers outboundCarriersField;

            private routingResponseEntriesRoutingResponseEntryServiceDescription[] serviceDescriptionsField;

            private decimal weightField;

            private string valueField;

            private string entity1ValueField;

            private decimal entity2ValueField;

            private string entity3ValueField;

            private string entity4ValueField;

            private routingResponseEntriesRoutingResponseEntryTitles titlesField;

            private string processField;

            /// <remarks/>
            public routingResponseEntriesRoutingResponseEntryDestinationAddress destinationAddress
            {
                get
                {
                    return this.destinationAddressField;
                }
                set
                {
                    this.destinationAddressField = value;
                }
            }

            /// <remarks/>
            public routingResponseEntriesRoutingResponseEntrySenderAddress senderAddress
            {
                get
                {
                    return this.senderAddressField;
                }
                set
                {
                    this.senderAddressField = value;
                }
            }

            /// <remarks/>
            public routingResponseEntriesRoutingResponseEntryOutboundCarriers outboundCarriers
            {
                get
                {
                    return this.outboundCarriersField;
                }
                set
                {
                    this.outboundCarriersField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("serviceDescription", IsNullable = false)]
            public routingResponseEntriesRoutingResponseEntryServiceDescription[] serviceDescriptions
            {
                get
                {
                    return this.serviceDescriptionsField;
                }
                set
                {
                    this.serviceDescriptionsField = value;
                }
            }

            /// <remarks/>
            public decimal weight
            {
                get
                {
                    return this.weightField;
                }
                set
                {
                    this.weightField = value;
                }
            }

            /// <remarks/>
            public string value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }

            /// <remarks/>
            public string entity1Value
            {
                get
                {
                    return this.entity1ValueField;
                }
                set
                {
                    this.entity1ValueField = value;
                }
            }

            /// <remarks/>
            public decimal entity2Value
            {
                get
                {
                    return this.entity2ValueField;
                }
                set
                {
                    this.entity2ValueField = value;
                }
            }

            /// <remarks/>
            public string entity3Value
            {
                get
                {
                    return this.entity3ValueField;
                }
                set
                {
                    this.entity3ValueField = value;
                }
            }

            /// <remarks/>
            public string entity4Value
            {
                get
                {
                    return this.entity4ValueField;
                }
                set
                {
                    this.entity4ValueField = value;
                }
            }

            /// <remarks/>
            public routingResponseEntriesRoutingResponseEntryTitles titles
            {
                get
                {
                    return this.titlesField;
                }
                set
                {
                    this.titlesField = value;
                }
            }

            /// <remarks/>
            public string process
            {
                get
                {
                    return this.processField;
                }
                set
                {
                    this.processField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntryServiceDescription
        {

            private string serviceLogoRefField;

            private string serviceDescriptionTextField;

            private byte servicePositionField;

            /// <remarks/>
            public string serviceLogoRef
            {
                get
                {
                    return this.serviceLogoRefField;
                }
                set
                {
                    this.serviceLogoRefField = value;
                }
            }

            /// <remarks/>
            public string serviceDescriptionText
            {
                get
                {
                    return this.serviceDescriptionTextField;
                }
                set
                {
                    this.serviceDescriptionTextField = value;
                }
            }

            /// <remarks/>
            public byte servicePosition
            {
                get
                {
                    return this.servicePositionField;
                }
                set
                {
                    this.servicePositionField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntryDestinationAddress
        {

            private string addressLine1Field;

            private string addressLine2Field;

            private string addressLine3Field;

            private string addressLine5Field;

            private string addressLine6Field;

            private string addressLine4Field;

            private uint customerReference1Field;

            private string customerReference2Field;

            /// <remarks/>
            public string addressLine1
            {
                get
                {
                    return this.addressLine1Field;
                }
                set
                {
                    this.addressLine1Field = value;
                }
            }

            /// <remarks/>
            public string addressLine2
            {
                get
                {
                    return this.addressLine2Field;
                }
                set
                {
                    this.addressLine2Field = value;
                }
            }

            /// <remarks/>
            public string addressLine3
            {
                get
                {
                    return this.addressLine3Field;
                }
                set
                {
                    this.addressLine3Field = value;
                }
            }

            /// <remarks/>
            public string addressLine5
            {
                get
                {
                    return this.addressLine5Field;
                }
                set
                {
                    this.addressLine5Field = value;
                }
            }

            /// <remarks/>
            public string addressLine6
            {
                get
                {
                    return this.addressLine6Field;
                }
                set
                {
                    this.addressLine6Field = value;
                }
            }

            /// <remarks/>
            public string addressLine4
            {
                get
                {
                    return this.addressLine4Field;
                }
                set
                {
                    this.addressLine4Field = value;
                }
            }

            /// <remarks/>
            public uint customerReference1
            {
                get
                {
                    return this.customerReference1Field;
                }
                set
                {
                    this.customerReference1Field = value;
                }
            }

            /// <remarks/>
            public string customerReference2
            {
                get
                {
                    return this.customerReference2Field;
                }
                set
                {
                    this.customerReference2Field = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntrySenderAddress
        {

            private string addressLine1Field;

            private string addressLine2Field;

            private string addressLine3Field;

            private string addressLine4Field;

            /// <remarks/>
            public string addressLine1
            {
                get
                {
                    return this.addressLine1Field;
                }
                set
                {
                    this.addressLine1Field = value;
                }
            }

            /// <remarks/>
            public string addressLine2
            {
                get
                {
                    return this.addressLine2Field;
                }
                set
                {
                    this.addressLine2Field = value;
                }
            }

            /// <remarks/>
            public string addressLine3
            {
                get
                {
                    return this.addressLine3Field;
                }
                set
                {
                    this.addressLine3Field = value;
                }
            }

            /// <remarks/>
            public string addressLine4
            {
                get
                {
                    return this.addressLine4Field;
                }
                set
                {
                    this.addressLine4Field = value;
                }
            }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntryOutboundCarriers
        {

            private routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier1 carrier1Field;

            private routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier2 carrier2Field;

            private string labelImageField;

            /// <remarks/>
            public routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier1 carrier1
            {
                get
                {
                    return this.carrier1Field;
                }
                set
                {
                    this.carrier1Field = value;
                }
            }

            /// <remarks/>
            public routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier2 carrier2
            {
                get
                {
                    return this.carrier2Field;
                }
                set
                {
                    this.carrier2Field = value;
                }
            }

            /// <remarks/>
            public string labelImage
            {
                get
                {
                    return this.labelImageField;
                }
                set
                {
                    this.labelImageField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier2
        {

            private string carrierIdField;

            private string carrierNameField;

            private string carrierLogoRefField;

            private routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier2Barcode1 barcode1Field;

            private routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier2Barcode2 barcode2Field;

            private string sortLevel1Field;

            private string sortLevel2Field;

            private string sortLevel3Field;

            private string sortLevel4Field;

            private string sortLevel5Field;

            /// <remarks/>
            public string carrierId
            {
                get
                {
                    return this.carrierIdField;
                }
                set
                {
                    this.carrierIdField = value;
                }
            }

            /// <remarks/>
            public string carrierName
            {
                get
                {
                    return this.carrierNameField;
                }
                set
                {
                    this.carrierNameField = value;
                }
            }

            /// <remarks/>
            public string carrierLogoRef
            {
                get
                {
                    return this.carrierLogoRefField;
                }
                set
                {
                    this.carrierLogoRefField = value;
                }
            }

            /// <remarks/>
            public routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier2Barcode1 barcode1
            {
                get
                {
                    return this.barcode1Field;
                }
                set
                {
                    this.barcode1Field = value;
                }
            }

            /// <remarks/>
            public routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier2Barcode2 barcode2
            {
                get
                {
                    return this.barcode2Field;
                }
                set
                {
                    this.barcode2Field = value;
                }
            }

            /// <remarks/>
            public string sortLevel1
            {
                get
                {
                    return this.sortLevel1Field;
                }
                set
                {
                    this.sortLevel1Field = value;
                }
            }

            /// <remarks/>
            public string sortLevel2
            {
                get
                {
                    return this.sortLevel2Field;
                }
                set
                {
                    this.sortLevel2Field = value;
                }
            }

            /// <remarks/>
            public string sortLevel3
            {
                get
                {
                    return this.sortLevel3Field;
                }
                set
                {
                    this.sortLevel3Field = value;
                }
            }

            /// <remarks/>
            public string sortLevel4
            {
                get
                {
                    return this.sortLevel4Field;
                }
                set
                {
                    this.sortLevel4Field = value;
                }
            }

            /// <remarks/>
            public string sortLevel5
            {
                get
                {
                    return this.sortLevel5Field;
                }
                set
                {
                    this.sortLevel5Field = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier2Barcode2
        {

            private string barcodeNumberField;

            private byte barcodeLengthField;

            private string barcodeSymbologyField;

            private string barcodeDisplayField;

            /// <remarks/>
            public string barcodeNumber
            {
                get
                {
                    return this.barcodeNumberField;
                }
                set
                {
                    this.barcodeNumberField = value;
                }
            }

            /// <remarks/>
            public byte barcodeLength
            {
                get
                {
                    return this.barcodeLengthField;
                }
                set
                {
                    this.barcodeLengthField = value;
                }
            }

            /// <remarks/>
            public string barcodeSymbology
            {
                get
                {
                    return this.barcodeSymbologyField;
                }
                set
                {
                    this.barcodeSymbologyField = value;
                }
            }

            /// <remarks/>
            public string barcodeDisplay
            {
                get
                {
                    return this.barcodeDisplayField;
                }
                set
                {
                    this.barcodeDisplayField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier2Barcode1
        {

            private string barcodeNumberField;

            private byte barcodeLengthField;

            private string barcodeSymbologyField;

            private string barcodeDisplayField;

            /// <remarks/>
            public string barcodeNumber
            {
                get
                {
                    return this.barcodeNumberField;
                }
                set
                {
                    this.barcodeNumberField = value;
                }
            }

            /// <remarks/>
            public byte barcodeLength
            {
                get
                {
                    return this.barcodeLengthField;
                }
                set
                {
                    this.barcodeLengthField = value;
                }
            }

            /// <remarks/>
            public string barcodeSymbology
            {
                get
                {
                    return this.barcodeSymbologyField;
                }
                set
                {
                    this.barcodeSymbologyField = value;
                }
            }

            /// <remarks/>
            public string barcodeDisplay
            {
                get
                {
                    return this.barcodeDisplayField;
                }
                set
                {
                    this.barcodeDisplayField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier1
        {

            private string carrierIdField;

            private string carrierNameField;

            private string carrierLogoRefField;

            private string deliveryMethodDescField;

            private routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier1Barcode1 barcode1Field;

            private byte sortLevel1Field;

            private string sortLevel2Field;

            /// <remarks/>
            public string carrierId
            {
                get
                {
                    return this.carrierIdField;
                }
                set
                {
                    this.carrierIdField = value;
                }
            }

            /// <remarks/>
            public string carrierName
            {
                get
                {
                    return this.carrierNameField;
                }
                set
                {
                    this.carrierNameField = value;
                }
            }

            /// <remarks/>
            public string carrierLogoRef
            {
                get
                {
                    return this.carrierLogoRefField;
                }
                set
                {
                    this.carrierLogoRefField = value;
                }
            }

            /// <remarks/>
            public string deliveryMethodDesc
            {
                get
                {
                    return this.deliveryMethodDescField;
                }
                set
                {
                    this.deliveryMethodDescField = value;
                }
            }

            /// <remarks/>
            public routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier1Barcode1 barcode1
            {
                get
                {
                    return this.barcode1Field;
                }
                set
                {
                    this.barcode1Field = value;
                }
            }

            /// <remarks/>
            public byte sortLevel1
            {
                get
                {
                    return this.sortLevel1Field;
                }
                set
                {
                    this.sortLevel1Field = value;
                }
            }

            /// <remarks/>
            public string sortLevel2
            {
                get
                {
                    return this.sortLevel2Field;
                }
                set
                {
                    this.sortLevel2Field = value;
                }
            }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntryOutboundCarriersCarrier1Barcode1
        {

            private string barcodeNumberField;

            private byte barcodeLengthField;

            private string barcodeSymbologyField;

            private string barcodeDisplayField;

            /// <remarks/>
            public string barcodeNumber
            {
                get
                {
                    return this.barcodeNumberField;
                }
                set
                {
                    this.barcodeNumberField = value;
                }
            }

            /// <remarks/>
            public byte barcodeLength
            {
                get
                {
                    return this.barcodeLengthField;
                }
                set
                {
                    this.barcodeLengthField = value;
                }
            }

            /// <remarks/>
            public string barcodeSymbology
            {
                get
                {
                    return this.barcodeSymbologyField;
                }
                set
                {
                    this.barcodeSymbologyField = value;
                }
            }

            /// <remarks/>
            public string barcodeDisplay
            {
                get
                {
                    return this.barcodeDisplayField;
                }
                set
                {
                    this.barcodeDisplayField = value;
                }
            }
        }


        /// <remarks/>
        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class routingResponseEntriesRoutingResponseEntryTitles
        {

            private string senderAddressTitleField;

            private string destinationAddressTitleField;

            private string entity1TitleField;

            private string entity2TitleField;

            private string entity3TitleField;

            private string entity4TitleField;

            /// <remarks/>
            public string senderAddressTitle
            {
                get
                {
                    return this.senderAddressTitleField;
                }
                set
                {
                    this.senderAddressTitleField = value;
                }
            }

            /// <remarks/>
            public string destinationAddressTitle
            {
                get
                {
                    return this.destinationAddressTitleField;
                }
                set
                {
                    this.destinationAddressTitleField = value;
                }
            }

            /// <remarks/>
            public string entity1Title
            {
                get
                {
                    return this.entity1TitleField;
                }
                set
                {
                    this.entity1TitleField = value;
                }
            }

            /// <remarks/>
            public string entity2Title
            {
                get
                {
                    return this.entity2TitleField;
                }
                set
                {
                    this.entity2TitleField = value;
                }
            }

            /// <remarks/>
            public string entity3Title
            {
                get
                {
                    return this.entity3TitleField;
                }
                set
                {
                    this.entity3TitleField = value;
                }
            }

            /// <remarks/>
            public string entity4Title
            {
                get
                {
                    return this.entity4TitleField;
                }
                set
                {
                    this.entity4TitleField = value;
                }
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        //
        // END - DO NOT EDIT BELOW HERE
        //
        // -------------------------------------------------------------------------------------------------------------						

        /**
        * This is just to keep the uneditable next line out of the way
        *
        */
        public class nothing
        {

            // COPY TO THIS LINE  
        }

        //Class to ganerate custom error if we got in response of Hermes api call
        public class HermesCustomException : Exception
        {
            public HermesCustomException(string strErrorMsg)
                : base(strErrorMsg)
            {

            }
        }
        // leave untouched                            
    }                                                                                           // leave untouched                            
}                                                                                               // leave untouched                            
