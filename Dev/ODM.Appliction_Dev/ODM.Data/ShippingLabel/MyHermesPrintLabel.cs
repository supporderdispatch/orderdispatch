﻿using linnworks.finaware.CommonData.Objects;
using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;

namespace ODM.Data.ShippingLabel
{
    public class MyHermesPrintLabel
    {
        public List<linnworks.scripting.core.ReturnClass.ShippingLabel> Shipping { get; set; }
        MyHermesCourier objMyHermesCourierSettings = new MyHermesCourier();
        GlobalVariablesAndMethods GlobalVars = new GlobalVariablesAndMethods();

        private void PrintPage(object o, PrintPageEventArgs e)
        {
            System.Drawing.Image img = Shipping[0].LabelImage;
            System.Drawing.Point loc = new System.Drawing.Point(100, 100);
            float MarginLeft = (float)objMyHermesCourierSettings.MarginLeft;
            float MarginTop = (float)objMyHermesCourierSettings.MarginTop;
            e.Graphics.DrawImage(img, MarginLeft, MarginTop);
        }

        public void PrintLabel(MyHermesCourier objMyHermesCourier)
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                if (objMyHermesCourier != null)
                {
                    objMyHermesCourierSettings = objMyHermesCourier;
                    pd.DefaultPageSettings.Landscape = objMyHermesCourier.IsLandScape;
                }
                if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForMyHermes))
                {
                    pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForMyHermes;
                }
                pd.PrintPage += PrintPage;
                pd.Print();
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }

        public string ExecuteMyHermesShippingAPI(Order objOrder, Guid OrderID)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            List<linnworks.scripting.core.ReturnClass.ShippingLabel> lstShippingLabel = new List<linnworks.scripting.core.ReturnClass.ShippingLabel>();
            DataTable objDt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@OrderId", SqlDbType.UniqueIdentifier).Value = OrderID;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPrintLabelDetailsForMyHermes";
                cmd.CommandTimeout = 200;
                SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
                objAdapt.Fill(objDt);

                //Mapping MyHermesCourier for the printing settings
                MyHermesCourier objCourier = new MyHermesCourier();
                objCourier.Printer = Convert.ToString(objDt.Rows[0]["Printer"]);
                objCourier.Paper = Convert.ToString(objDt.Rows[0]["Paper"]);
                objCourier.IsLandScape = Convert.ToBoolean(objDt.Rows[0]["IsLandScape"]);
                objCourier.IsInvoiceLandscape = Convert.ToBoolean(objDt.Rows[0]["IsInvoiceLandscape"]);
                objCourier.MarginLeft = Convert.ToDecimal(objDt.Rows[0]["MarginLeft"]);
                objCourier.MarginTop = Convert.ToDecimal(objDt.Rows[0]["MarginTop"]);
                GlobalVariablesAndMethods.blVerboseLogging = Convert.ToBoolean(objDt.Rows[0]["IsBoseLoggin"]);
                GlobalVariablesAndMethods.strApiToken = Convert.ToString(objDt.Rows[0]["ApiKey"]);
                GlobalVariablesAndMethods.strRequiredLabelFormat = Convert.ToString(objDt.Rows[0]["LabelType"]);
                GlobalVariablesAndMethods.strDefaultWeightInKg = Convert.ToString(objDt.Rows[0]["DefaultWeight"]);
                //End of MyHermesCourier class mapping.

                ShippingLabelMacro objMacro = new ShippingLabelMacro();
                SqlConnection con = new SqlConnection();
                linnworks.scripting.core.Debugger objDebug = new linnworks.scripting.core.Debugger();
                linnworks.scripting.core.Classes.OptionCollection ServiceOptions = new linnworks.scripting.core.Classes.OptionCollection();
                lstShippingLabel = objMacro.Initialize(objDebug, con, objOrder, ServiceOptions);
                Shipping = lstShippingLabel;
                PrintLabel(objCourier);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
            if (lstShippingLabel.Count > 0)
            {
                return lstShippingLabel[0].TrackingNumber;
            }
            else
            {
                return "";
            }
        }
    }
}
