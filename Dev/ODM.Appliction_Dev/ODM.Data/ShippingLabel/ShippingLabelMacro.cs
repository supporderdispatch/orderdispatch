﻿using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace linnworks.finaware.CommonData.Objects                // leave untouched                            
{                                                                                               // leave untouched                            
    // public class ShippingLabelMacro : linnworks.scripting.core.IShippingMacro                       // leave untouched                            
    public class ShippingLabelMacro
    {
        //GlobalVariablesAndMethods Global = new GlobalVariablesAndMethods();
        // leave untouched                            
        public List<linnworks.scripting.core.ReturnClass.ShippingLabel> Initialize(linnworks.scripting.core.Debugger debug, SqlConnection ActiveConnection, Order order, linnworks.scripting.core.Classes.OptionCollection ServiceOptions)           // leave untouched                            
        {                                                                                   // leave untouched                            
            // COPY FROM THIS LINE
            initialise(debug);
            return getShippingLabel(order);
        }


        // -------------------------------------------------------------------------------------------------------------
        //
        // NOTICE:
        //
        //
        // You have agreed that you must protect the information held within this macro and its subsequent use 
        // by third parties as any parcels generated using this code will be billed to your associated myHermes account.
        //
        //
        // Copyright Hermes Parcelnet Ltd myHermes 2014
        //
        //
        //
        // -------------------------------------------------------------------------------------------------------------


        // DO NOT CHANGE THIS SCRIPT


        // -------------------------------------------------------------------------------------------------------------
        //
        // Customisation Properties
        //
        // -------------------------------------------------------------------------------------------------------------			

        // NOTE - you should not need to change these properties. If you do need to change them, you can do so more easily
        // by accessing your myhermes account and regenerating the macro.
        // If you do change these settings, or the contents of this script, please note that it will make it difficult for
        // us to diagnose any errors that you experience. You should not need to modify this script.

        // Links to your myHermes account. Do not change. See notice about protecting access to this private information.
        //static string apiToken = "e9aeebc1-2dd6-4a73-81ea-1f25688029f4";
        static string apiToken = GlobalVariablesAndMethods.strApiToken;

        // The required label format. Available options are THERMAL (which gives labels suitable for thermal printers) and DEFAULT (for any other kind of printer).
        //static string requiredLabelFormat = "THERMAL";
        static string requiredLabelFormat = GlobalVariablesAndMethods.strRequiredLabelFormat;

        // This is the weight in KG that will be used if the parcel weight cannot be determined from the order or is zero.
        // Every parcel must have a weight, and the weight affects the price of the parcel.
        // You should ensure that your orders and inventory correctly specify a weight so that this default will not be used.
        //static string defaultWeightInKg = "0.8";
        static string defaultWeightInKg = GlobalVariablesAndMethods.strDefaultWeightInKg;

        // Change to false to switch off logging
        //static bool verboseLogging = true;
        static bool verboseLogging = GlobalVariablesAndMethods.blVerboseLogging;

        // -------------------------------------------------------------------------------------------------------------
        //
        // Initialisation
        //
        // -------------------------------------------------------------------------------------------------------------


        /**
        * Performs initialisation prior to script execution, eg loading preferences.
        *
        */
        public static void initialise(linnworks.scripting.core.Debugger debug)
        {
            createWorkingDirectory();
            clearLogFile();
            createShippingServiceSettings();

            LOG = new Logger(debug);
            messages = loadMessages();
        }

        /**
        * Find the working directory and create it if not found.
        *
        */
        public static void createWorkingDirectory()
        {
            if (!verboseLogging) return;
            workingDirectory = getHomeDirectory() + "/linnworks";
            if (!System.IO.Directory.Exists(workingDirectory))
            {
                System.IO.Directory.CreateDirectory(workingDirectory);
            }
        }

        /**
        * Find the user's home directory.
        *
        */
        public static string getHomeDirectory()
        {

            return (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX)
                ? Environment.GetEnvironmentVariable("HOME")
                : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
        }

        /**
        * Set up shipping service configurations.
        *
        */
        public static void createShippingServiceSettings()
        {

            availableShippingServiceSettings = new Dictionary<string, ShippingServiceSettings>();

            availableShippingServiceSettings.Add("MHSTANDARD", new ShippingServiceSettings(false, 0));
            availableShippingServiceSettings.Add("MHSIGNED50", new ShippingServiceSettings(true, 50));
            availableShippingServiceSettings.Add("MHSIGNED100", new ShippingServiceSettings(true, 100));
            availableShippingServiceSettings.Add("MHSIGNED250", new ShippingServiceSettings(true, 250));
        }


        // -------------------------------------------------------------------------------------------------------------
        //
        // API configuration
        //
        // -------------------------------------------------------------------------------------------------------------

        /**
        * Get the full URI for a parcels request.
        *exportToParcels
        */
        public static string getParcelsUri()
        {
            return root + "/parcels";
        }

        /**
        * Get the full URI for a labels request.
        *
        */
        public static string getLabelsUri()
        {
            return root + "/labels";
        }

        // -------------------------------------------------------------------------------------------------------------
        //
        // Get Labels
        //
        // -------------------------------------------------------------------------------------------------------------


        /**
        * Export an order to create a parcel in myHermes, returning shipping labels.
        *
        */
        public static List<linnworks.scripting.core.ReturnClass.ShippingLabel> getShippingLabel(Order order)
        {
            LOG.blankLines(2);
            LOG.Log("Executing with script version '" + scriptVersion + "'");

            validate(order);

            string parcelSummaries = exportToParcels(order);

            return getShippingLabelFor(order, parcelSummaries);
        }

        /**
        * We do only essential validation here for fundamental things like supported countries.
        * All other validation is left to the server so that the script does not duplicate validation logic, and does not become out of date.
        *
        */
        public static void validate(Order order)
        {
            if (order == null)
            {
                failWithError(messageFor("null-input"));
            }

            if (isMissingCountry(order))
            {
                failWithError(messageFor("unspecified-country"));
            }

            if (isUnsupportedCountry(order))
            {
                failWithError(messageFor("unsupported-country"));
            }

            if (isAlreadyTracked(order))
            {
                failWithError(messageFor("already-tracked"));
            }

            //if (order.OrderPackagingSplit.Count > 1)
            //{
            //    LOG.Log("Order is split into '" + order.OrderPackagingSplit.Count.ToString() + "' packages");
            //    failWithError(messageFor("split-packaging"));
            //}
        }

        /**
        * The country must be specified.
        *
        */
        public static bool isMissingCountry(Order order)
        {
            return order.Country.Length <= 0;
            //return true;
        }

        /**
        * myHermes currently does not support delivery to addresses outside UK.
        *
        */
        public static bool isUnsupportedCountry(Order order)
        {
            return !order.Country.ToUpper().Equals("UNITED KINGDOM");
        }

        /**
        * If the order already has a tracking number, you must delete the existing tracking number before resubmitting. 
        * This is to avoid accidentally creating multiple labels for one order.
        * Note that each new submission will result in a new parcel being created in your myHermes account.
        *
        */
        public static bool isAlreadyTracked(Order order)
        {
            return order.PostalTrackingNumber != null && order.PostalTrackingNumber.Length > 0;
        }

        /**
        * Convert Order to 'parcels' json, submit to myHermes to create a parcel, obtaining tracking barcodes.
        *
        */
        public static string exportToParcels(Order order)
        {
            LOG.Log("Exporting order with id '" + order.NumOrderId + "' to myHermes parcels");

            identifyServiceLevel(order.ShippingMethod);

            string parcelJson = parcelJsonFor(order);
            LOG.Log("Converted order to JSON:");
            LOG.Log(parcelJson);

            string parcelSummaries = postForString(getParcelsUri(), parcelJson, parcelMediaType, parcelSummaryMediaType);
            LOG.Log(parcelSummaries);

            checkForParcelSummaryErrors(parcelSummaries);

            LOG.Log("Finished exporting order with id '" + order.NumOrderId + "' to myHermes parcels");

            return parcelSummaries;
        }

        /**
        * Get a shipping label for the tracking barcode.
        *
        */
        public static List<linnworks.scripting.core.ReturnClass.ShippingLabel> getShippingLabelFor(Order order, string parcelSummaries)
        {
            LOG.Log("Retrieving shipping label for order with id '" + order.NumOrderId + "'");
            string barcode = firstValueOf("barcode", parcelSummaries);
            LOG.Log("Got barcode: " + barcode);
            List<linnworks.scripting.core.ReturnClass.ShippingLabel> labels = new List<linnworks.scripting.core.ReturnClass.ShippingLabel>();
            labels.Add(labelFor(barcode));
            LOG.Log("Finished retrieving shipping label for order with id '" + order.NumOrderId + "'");
            return labels;
        }

        /**
        * Get a label image for the given barcode and create the Linnworks shipping label.
        *
        */
        public static linnworks.scripting.core.ReturnClass.ShippingLabel labelFor(string barcode)
        {
            System.Drawing.Image labelImage = getForImage(getLabelsUri() + "/" + barcode + labelFormatParameter + requiredLabelFormat.Trim());
            return new linnworks.scripting.core.ReturnClass.ShippingLabel(barcode, "", labelImage);
        }


        /**
        * Find a matching shipping service.
        *
        */
        public static void identifyServiceLevel(string shippingServiceName)
        {
            string selectedService = shippingServiceName.ToUpper().Replace(" ", string.Empty);
            LOG.Log("Selected shipping service is '" + selectedService + "'");
            if (availableShippingServiceSettings.ContainsKey(selectedService))
            {
                shippingServiceSettings = availableShippingServiceSettings[selectedService];
            }
            else
            {
                failWithError(messageFor("missing-shipping-methods"));
            }
        }

        /**
        * Details of a shipping service level.
        *
        */
        public class ShippingServiceSettings
        {
            public bool trackingRequired;
            public int compensationlevel;
            public ShippingServiceSettings(bool trackingRequired, int compensationlevel)
            {
                this.trackingRequired = trackingRequired;
                this.compensationlevel = compensationlevel;
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        //
        // Data Mapping
        //
        // -------------------------------------------------------------------------------------------------------------


        /**
        * Convert an Order to JSON.
        *
        */
        //public static string parcelJsonFor(linnworks.finaware.CommonData.Objects.Order order)
        public static string parcelJsonFor(Order order)
        {
            System.Text.StringBuilder json = new System.Text.StringBuilder();

            if (order == null)
            {
                return "";
            }

            json.Append("{ ");
            json.Append(@"""parcels"":[ ");

            json.Append("{ ");

            json.Append(@"""clientUID"": """ + clean(order.NumOrderId.ToString()) + @""",");

            json.Append(@"""parcelDetails"":{");
            json.Append(@"""weightKg"": """ + getParcelWeightInKg(order) + @""",");
            json.Append(@"""itemDescription"": """ + clean(getItemDescriptionFor(order.OrderItems)) + @""",");
            json.Append(@"""deliveryReference"": """ + clean(order.NumOrderId.ToString()) + @""",");
            json.Append(@"""estimatedParcelValuePounds"": " + order.TotalCharge.ToString() + @",");
            json.Append(@"""compensationRequiredPounds"": " + shippingServiceSettings.compensationlevel);
            json.Append("}, ");


            json.Append(@"""deliveryDetails"":{");
            json.Append(@"""deliveryAddress"": {");
            json.Append(@"""postcode"": """ + findPostcode(order.PostCode.ToString().ToUpper()) + @""",");
            json.Append(@"""line1"": """ + clean(order.Address1.ToString()) + @""",");
            json.Append(@"""line2"": """ + clean(order.Address2.ToString()) + @""",");
            json.Append(@"""line3"": """ + clean(order.Address3.ToString()) + @""",");
            json.Append(@"""line4"": """ + clean(order.Town.ToString()) + @"""");
            json.Append("}, ");


            string[] names = parseName(order.FullName.ToString());

            json.Append(@"""firstName"": """ + clean(names[0]) + @""",");
            json.Append(@"""lastName"": """ + clean(names[1]) + @""",");
            json.Append(@"""email"": """ + clean(order.EmailAddress.ToString()) + @""",");
            json.Append(@"""telephone"": """ + order.BuyerPhoneNumber.ToString() + @""",");
            json.Append(@"""signatureRequired"": " + shippingServiceSettings.trackingRequired.ToString().ToLower());
            json.Append("} ");

            json.Append("} ");

            json.Append("]}");
            return json.ToString();
        }

        /**
        * Removes illegal characters that may break json formatting.
        *
        old code:
        private static string clean(string source){
            return source.Replace(@"""", "");
        }
        */

        private static string clean(string s)
        {
            if (s == null || s.Length == 0)
            {
                return "";
            }

            char c = '\0';
            int i;
            int len = s.Length;
            System.Text.StringBuilder sb = new System.Text.StringBuilder(len + 4);
            String t;

            for (i = 0; i < len; i += 1)
            {
                c = s[i];
                switch (c)
                {
                    case '\\':
                    case '"':
                        sb.Append('\\');
                        sb.Append(c);
                        break;
                    case '/':
                        sb.Append('\\');
                        sb.Append(c);
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    default:
                        if (c < ' ')
                        {
                            t = "000" + String.Format("X", c);
                            sb.Append("\\u" + t.Substring(t.Length - 4));
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            return sb.ToString();
        }

        /**
        * Removes spaces.
        *
        */
        private static string cleanSpaces(string source)
        {
            return source.Replace(@" ", "");
        }

        /**
        * Linnworks parcel weights are in grams. myHermes requires kg.
        * If the weight is not defined or is zero, the value of  "defaultWeightInKg", 
        * which was selected when generating the macro, will be used.
        *
        */
        public static string getParcelWeightInKg(Order order)
        {
            if (order.Pack_TotalWeight == null || order.Pack_TotalWeight <= 0.0)
            {
                return useDefaultWeight();
            }

            //		Random rnd = new Random();
            //		int weight = rnd.Next(950);
            //		return (650/1000).ToString();
            return (order.Pack_TotalWeight).ToString();
        }

        /**
        * Checks that defaultWeightInKg is a number and returns the value as a string.
        *
        */
        public static string useDefaultWeight()
        {
            try
            {
                LOG.Log("Order total weight not set or is zero. Using defaultWeightInKg which is set to '" + defaultWeightInKg + "'");
                double defaultWeightNumber = double.Parse(defaultWeightInKg.Trim(), System.Globalization.NumberStyles.AllowDecimalPoint);
                return defaultWeightNumber.ToString();
            }
            catch (Exception e)
            {
                failWithError(messageFor("invalid-default-weight"), e);
                return null;
            }
        }

        /**
        * Trim any leading and trailing spaces then split the fullname field at the first space and assume that the
        * first part is the first name, last parts are the last name. If there is no space, use the fullname as last name.
        * Because fullname is free-form text, we can't do much better.
        *
        * Eg Mr Bill Smith will generate firstName 'Mr', lastName 'Bill Smith'.
        * This is not great, but we can't deal with all the possible variations of honorifics and their spellings.
        *
        */
        public static string[] parseName(string incomingName)
        {
            string fullName = incomingName.Trim();

            if (fullName.Contains(" "))
            {

                string[] nameComponents = fullName.Split(' ');

                string first = nameComponents[0];
                string rest = fullName.Substring(first.Length + 1).Trim();

                return new string[] { first, rest };

            }
            else
            {
                LOG.Log("'fullname' field did not have a space so can't determine firstname / lastname");
                return new string[] { "", fullName };
            }
        }

        /**
        * Item description includes the item title and the quantity, and the whole description is limited to a maximum length.
        * eg an order for 1 apple and 2 bananas will have a description "apple, banana x2". Two properties control this method:
        * minDescriptionQuantity sets the smallest quantity at which any description is shown.
        * minMultiplierQuantity sets the smallest quantity at which the multiplier is shown.
        * eg if minDescriptionQuantity=1 and an item has quantity 0, it will not appear in the description.
        * eg if minMultiplierQuantity=2 and an item has quantity 1, it will show as a title without multiplier.
        * eg if the above two settings are true and an item has quantity 2, it will show as 'title xQuantity'.
        * If either property is set to a negative value, quantity will not affect the output.
        */
        public static string getItemDescriptionFor(List<OrderItem> orderItems)
        {

            System.Text.StringBuilder descriptionBuilder = new System.Text.StringBuilder();

            foreach (OrderItem item in orderItems)
            {

                LOG.Log(item.Title + " - " + item.Qty);

                if (minDescriptionQuantity >= 0 && item.Qty < minDescriptionQuantity)
                {
                    continue;
                }

                string itemTitle = titleFor(item);
                string multiplier = multiplierFor(item);

                descriptionBuilder.Append(itemTitle + multiplier);
                descriptionBuilder.Append(", ");
            }

            string description = descriptionBuilder.ToString();

            if (description.Length > MAX_DESCRIPTION_LENGTH)
            {
                return description.Substring(0, MAX_DESCRIPTION_LENGTH);

            }
            else if (description.EndsWith(", "))
            {
                return description.Remove(description.Length - 2, 2).ToString();
            }

            if (description.Length <= 0)
            {
                return "Not specified";
            }

            return description;
        }

        /**
        * Generate the item title to use in the parcel description.
        *
        */
        public static string titleFor(OrderItem item)
        {
            if (item.Title.Length <= 0)
            {
                return "Unknown";
            }
            return item.Title;
        }

        /**
        * Generate the multiplier to use for the item in the parcel description.
        *
        */
        public static string multiplierFor(OrderItem item)
        {
            if (minMultiplierQuantity >= 0 && item.Qty < minMultiplierQuantity)
            {
                return "";
            }
            return " x" + item.Qty;
        }



        /**
        * Limits reference number to max allowed length.
        *
        */
        public static string getReference(Order order)
        {

            string refNum = order.ReferenceNum.ToString();

            if (refNum.Length > MAX_REFERENCE_LENGTH)
            {
                return refNum.Substring(0, MAX_REFERENCE_LENGTH);
            }
            return refNum;
        }



        /**
        * Attempts to find the value of the first occurrence of the given property.
        *
        */
        public static string firstValueOf(string key, string json)
        {
            foreach (string s in json.Split(','))
            {
                if (s.Contains(key))
                {
                    return s.Split(':')[1].Replace('"', ' ').Replace('[', ' ').Replace(']', ' ').Replace('{', ' ').Replace('}', ' ').Trim();
                }
            }
            LOG.Log("Failed to find '" + key + "' in '" + json + "'");
            return "";
        }

        /**
        * Count how many times the given property appears in the json.
        *
        */
        public static int countOccurrences(string key, string json)
        {

            int count = 0;

            foreach (string s in json.Split(','))
            {
                if (s.Contains(key))
                {
                    count++;
                }
            }
            return count;
        }

        /**
        * Tests postcode for validity and formats it correctly.
        *
        */
        private static string findPostcode(string postcode)
        {

            string UK_POSTCODE_PATTERN = "\\b([A-PR-UWYZa-pr-uwyz]([0-9]{1,2}|([A-HK-Ya-hk-y][0-9]|[A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRV-Yabehmnprv-y]))|[0-9][A-HJKS-UWa-hjks-uw]))\\ {0,1}([0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}|([Gg][Ii][Rr]\\ 0[Aa][Aa])|([Ss][Aa][Nn]\\ {0,1}[Tt][Aa]1)|([Bb][Ff][Pp][Oo]\\ {0,1}([Cc]\\/[Oo]\\ )?[0-9]{1,4})|(([Aa][Ss][Cc][Nn]|[Bb][Bb][Nn][Dd]|[BFSbfs][Ii][Qq][Qq]|[Pp][Cc][Rr][Nn]|[Ss][Tt][Hh][Ll]|[Tt][Dd][Cc][Uu]|[Tt][Kk][Cc][Aa])\\ {0,1}1[Zz][Zz]))\\b[\\s]*";

            System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(cleanSpaces(postcode), UK_POSTCODE_PATTERN);

            if (match.Success)
            {
                return match.Groups[1].Value + " " + match.Groups[5].Value;
            }

            failWithError(messageFor("invalid-delivery-postcode"));
            return "";
        }



        // -------------------------------------------------------------------------------------------------------------
        //
        // Networking
        //
        // -------------------------------------------------------------------------------------------------------------



        /**
        * Post content to uri as contentType, returning response content as string if it is acceptable content type.
        *
        */
        public static string postForString(string uri, string content, string contentType, string acceptable)
        {
            configureSsl();
            System.Net.HttpWebRequest request = createRequest(uri);
            request.Method = "POST";
            request.ContentType = contentType;
            request.Accept = acceptable;
            setRequestSecurity(request);

            setRequestContent(request, content);

            System.Net.HttpWebResponse response = execute(request);

            string responseString = getResponseAsString(response);
            LOG.Log("Response Content: " + responseString);

            return responseString;
        }

        /**
        * Get the given uri and convert the response content into an Image.
        *
        */
        public static System.Drawing.Image getForImage(string uri)
        {
            configureSsl();
            System.Net.HttpWebRequest request = createRequest(uri);

            request.Method = "GET";
            request.Accept = labelImageType + "," + labelErrorMediaType;

            setRequestSecurity(request);

            System.Net.HttpWebResponse response = execute(request);

            try
            {
                return System.Drawing.Image.FromStream(response.GetResponseStream());
            }
            catch (Exception e)
            {
                failWithError(getMessageFor(e), e);
                return null;
            }
        }



        /**
        * Construct an http request for the given uri. Will fail if URI syntax is not valid, but does not try to access the URI.
        *
        */
        public static System.Net.HttpWebRequest createRequest(string uri)
        {

            try
            {
                LOG.Log("Creating request for uri: " + uri);
                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(uri);
                LOG.Log("Created request");
                return request;
            }
            catch (Exception e)
            {
                failWithError(getMessageFor(e), e);
                return null;
            }
        }

        /**
        * Perform an http request. Will fail for unreachable host,not found, etc
        *
        */
        public static System.Net.HttpWebResponse execute(System.Net.HttpWebRequest request)
        {
            try
            {
                logRequest(request);
                System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
                LOG.Log("Response Status: " + response.StatusCode.ToString());
                return response;
            }
            catch (System.Net.WebException e)
            {
                failWithError(getMessageForWebException(e), e);
                return null;
            }
        }

        /**
        * Add the user's security token details into the request.
        *
        */
        public static void setRequestSecurity(System.Net.HttpWebRequest request)
        {
            request.Headers.Add("Authorization", "Bearer " + apiToken);
        }

        /**
        * Put the content into the http request.
        *
        */
        public static void setRequestContent(System.Net.HttpWebRequest request, string content)
        {
            try
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(content);
                request.ContentLength = bytes.Length;
                System.IO.Stream os = request.GetRequestStream();
                os.Write(bytes, 0, bytes.Length);
                os.Close();

            }
            catch (Exception e)
            {
                failWithError(getMessageFor(e), e);
            }
        }

        /**
        * Read response content into a string.
        *
        */
        public static string getResponseAsString(System.Net.HttpWebResponse response)
        {
            try
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                return sr.ReadToEnd();
            }
            catch (Exception e)
            {
                failWithError("Failed to read content from http response", e);
                return null;
            }
        }

        /**
        * Apply configuration to avoid SSL errors.
        *
        */
        public static void configureSsl()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllSslCertificates);
        }

        /**
        * Ignore all SSL certificate errors.
        *
        */
        public static bool AcceptAllSslCertificates(
                object sender,
                System.Security.Cryptography.X509Certificates.X509Certificate certification,
                System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {

            return true;
        }



        // -------------------------------------------------------------------------------------------------------------
        //
        // Logging
        //
        // -------------------------------------------------------------------------------------------------------------

        /**
        * Get the current log file. Current log is appended to until full, then it is copied to the backup, replacing previous backup.
        *
        */
        public static string getLogFileName()
        {
            return workingDirectory + "/log.txt";
        }

        /**
        * Get the backup log file. 
        *
        */
        public static string getBackupLogFileName()
        {
            return workingDirectory + "/log-backup.txt";
        }

        /**
        * Keep log file below max size.
        *
        */
        public static void clearLogFile()
        {

            if (!verboseLogging) return;

            if (System.IO.File.Exists(getLogFileName()))
            {
                System.IO.FileInfo logFileInfo = new System.IO.FileInfo(getLogFileName());
                if (logFileInfo.Length > LOG_FILE_LENGTH)
                {
                    if (System.IO.File.Exists(getBackupLogFileName()))
                    {
                        System.IO.File.Delete(getBackupLogFileName());
                    }
                    System.IO.File.Move(getLogFileName(), getBackupLogFileName());
                }
            }
        }

        /**
        * Log the details of an http request, including headers.
        *
        */
        public static void logRequest(System.Net.HttpWebRequest request)
        {

            LOG.Log("Executing request");

            System.Net.WebHeaderCollection headers = request.Headers;

            for (int i = 0; i < headers.Count; ++i)
            {
                string header = headers.GetKey(i);

                foreach (string value in headers.GetValues(i))
                {
                    LOG.Log("Request header <" + header + "> with value <" + value + ">");
                }
            }
        }


        /**
        * Log the given message with a date and time stamp, and report the same message to debug output.
        *
        */
        public class Logger
        {

            private linnworks.scripting.core.Debugger debug;

            public Logger(linnworks.scripting.core.Debugger debug)
            {
                this.debug = debug;
            }

            /**
            * Output a number of blank lines in the log for clear separation between activities.
            *
            */
            public void blankLines(int number)
            {
                for (int i = 0; i < number; i++)
                {
                    Log("");
                }
            }

            /**
            * Log the message with a date and timestamp, also reporting to debugger console.
            *
            */
            public void Log(string msg)
            {

                debug.AddEntry(msg);

                if (!verboseLogging) return;

                using (System.IO.StreamWriter w = System.IO.File.AppendText(getLogFileName()))
                {
                    DateTime now = DateTime.Now;
                    w.WriteLine("{0} {1} - {2}", now.ToLongDateString(), now.ToLongTimeString(), msg);
                    w.WriteLine("");
                }

                //Storing log messages of linnwork those have length less that 200 chars and don't have header
                if (!string.IsNullOrEmpty(msg) && msg.Length <= 200 && !msg.Contains("Request header <") && !msg.Contains(".pdf"))
                {
                    GlobalVariablesAndMethods.LogHistory(msg);
                }
            }
        }


        // -------------------------------------------------------------------------------------------------------------
        //
        // Error Handling
        //
        // -------------------------------------------------------------------------------------------------------------

        /**
        * Helps distinguish our own exceptions from standard ones in debug output etc.
        *
        */
        public class HermesException : Exception
        {
            public HermesException(String msg) : base(msg) { }
        }

        /**
        * Report a message to the user and the log file. Report details and exception details to the log file.
        *
        */
        public static void failWithError(String msg, string details, Exception e)
        {

            LOG.Log(msg);
            LOG.Log(details);
            LOG.Log(e.Message);
            LOG.Log(e.StackTrace);

            throw new HermesException(msg);
        }

        /**
        * Report a message to the user and the log file. Report exception details to the log file.
        *
        */
        public static void failWithError(String msg, Exception e)
        {

            LOG.Log(msg);
            LOG.Log(e.GetType().ToString());
            LOG.Log(e.Message);
            LOG.Log(e.StackTrace);

            throw new HermesException(msg);
        }

        /**
        * Report a message to the user and the log file. Report details to the log file.
        *
        */
        public static void failWithError(String msg, string details)
        {

            LOG.Log(msg);
            LOG.Log(details);

            throw new HermesException(msg);
        }

        /**
        * Report a message to the user and the log file. Prefer other methods giving more details for the log file.
        *
        */
        public static void failWithError(String msg)
        {

            LOG.Log(msg);

            throw new HermesException(msg);
        }

        /**
        * Find a user-friendly error message for the given exception, by looking at the exception message.
        *
        */
        public static string getMessageFor(Exception exception)
        {

            if (exception.Message.StartsWith("The remote name could not be resolved"))
            {
                return messageFor("unknown-host");
            }

            if (exception.Message.StartsWith("The URI prefix is not recognised"))
            {
                return messageFor("bad-uri");
            }

            if (exception.Message.StartsWith("Unable to connect to the remote server"))
            {
                return messageFor("no-response");
            }

            return messageFor(exception.GetType().ToString());
        }

        /**
        * Find a user-friendly error message for the given web exception, by looking at the http response status code.
        *
        */
        public static string getMessageForWebException(System.Net.WebException webException)
        {

            try
            {
                System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)webException.Response;

                string status = response.StatusCode.ToString();
                return messageFor(status);

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /**
        * Report any errors found in the json.
        *
        */
        public static void checkForParcelSummaryErrors(string json)
        {

            if (json.Contains("errors"))
            {

                int errorCount = countOccurrences("error_description", json);

                string intro = errorCount > 1 ?
                    "Code 099: There were " + errorCount + " validation errors. First error"
                    :
                    "Code 099: There was " + errorCount + " validation error";

                failWithError(intro + " - Property: '"
                 + firstValueOf("error_paths", json)
                 + "' has error: '"
                 + firstValueOf("error_description", json)
                 + "'");
            }
        }

        // -------------------------------------------------------------------------------------------------------------
        //
        // Error Messages
        //
        // -------------------------------------------------------------------------------------------------------------

        /**
        * Get the user friendly error message for the given key.
        *
        */
        public static string messageFor(string key)
        {
            try
            {
                return messages[key];
            }
            catch (Exception)
            {
                LOG.Log("No error message defined for '" + key + "'");
                return "An error occurred with error code '" + key + "'";
            }
        }

        /**
        * Create the set of user friendly error messages.
        *
        */
        public static Dictionary<string, string> loadMessages()
        {
            Dictionary<string, string> messages = new Dictionary<string, string>();
            messages.Add("null-input", "Code 000: Invoked with null input");
            messages.Add("missing-shipping-methods", "Code 100: Please configure shipping service methods");
            messages.Add("invalid-default-weight", "Code 101: The defaultWeightInKg macro property is invalid");
            messages.Add("unsupported-country", "Code 103: myHermes delivery is currently only available inside the UK");
            messages.Add("unspecified-country", "Code 104: Country not specified. myHermes delivery is currently only available inside the UK");
            messages.Add("already-tracked", "Code 106: Order already has tracking number. Delete existing tracking number to re-submit");
            messages.Add("split-packaging", "Code 107: Split packaging orders cannot be processed. Please split the order, not just the packaging");
            messages.Add("invalid-delivery-postcode", "Code 108: The delivery postcode is invalid");

            messages.Add("unknown-host", "Code 200: The host could not be reached - check script settings and network connection");
            messages.Add("bad-uri", "Code 201: A URI was badly formed - check script settings and network connection");
            messages.Add("no-response", "Code 202: A URI request received no response - check script settings and network connection");

            messages.Add("Unauthorized", "Code 300: Unauthorised myHermes call - check for correct API token");
            messages.Add("NotFound", "Code 301: URI not found - check script settings and network connection");
            messages.Add("NotAcceptable", "Code 302: Unacceptable media type defined for response content - check script settings");
            messages.Add("UnsupportedMediaType", "Code 303: Unsupported media type defined for request content - check script settings");
            messages.Add("BadRequest", "Code 304: A URI request was badly formed - check the script and settings such as requiredLabelFormat ");

            return messages;
        }


        // -------------------------------------------------------------------------------------------------------------
        //
        // System Properties - DO NOT CHANGE
        //
        // -------------------------------------------------------------------------------------------------------------

        // SYSTEM EXECUTION PROPERTIES
        static string root = "https://api.myhermes.co.uk/api";
        static string labelFormatParameter = "?format=";
        static string scriptVersion = "6.0-20140922-172754";

        // DESCRIPTION PROPERTIES
        // Add item to parcel description only when item quantity is this value or more. Set to -1 to always show.
        static int minDescriptionQuantity = -1;
        // Add multiplier to this item in parcel description only when item quantity is this value or more.  Set to -1 to always show.
        static int minMultiplierQuantity = 2;

        // FIXED PROPERTIES						
        static string parcelSummaryMediaType = "application/vnd.myhermes.parcelsummaries-v1+json";
        static string parcelMediaType = "application/vnd.myhermes.parcels-v1+json";
        static string labelErrorMediaType = "application/json";
        static string labelImageType = "image/png";

        static int MAX_DESCRIPTION_LENGTH = 255;
        static int MAX_REFERENCE_LENGTH = 32;
        static int LOG_FILE_LENGTH = 500000;

        static Dictionary<string, string> messages;
        static Logger LOG;

        static string workingDirectory;

        static Dictionary<string, ShippingServiceSettings> availableShippingServiceSettings;
        static ShippingServiceSettings shippingServiceSettings;






        // -------------------------------------------------------------------------------------------------------------
        //
        // END - DO NOT EDIT BELOW HERE
        //
        // -------------------------------------------------------------------------------------------------------------						

        /**
        * This is just to keep the uneditable next line out of the way
        *
        */
        public class nothing
        {


            // COPY TO THIS LINE                                
        }                                                                                   // leave untouched                            
    }                                                                                           // leave untouched                            
}                                                                                               // leave untouched                            
