﻿using GhostscriptSharp;
using GhostscriptSharp.Settings;
using ODM.Data.Util;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using RestSharp;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace ODM.Data.ShippingLabel
{
    public class ShippingLabelRoyalMail
    {
        #region "Variables"
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        static string UserName;
        static string Password;
        static string ClientID;
        static string Secret;
        static string ApplicationID;
        string strShipmentNumber;
        string strResponseCode = string.Empty;
        #endregion

        #region "Constructor"
        public ShippingLabelRoyalMail()
        {
            UserName = GlobalVariablesAndMethods.APIUser;
            Password = GlobalVariablesAndMethods.APIPass;
            ClientID = GlobalVariablesAndMethods.ClientID;
            Secret = GlobalVariablesAndMethods.Secret;
            ApplicationID = GlobalVariablesAndMethods.ApplicationID;
        }
        #endregion

        #region "Methods"
        /// <summary>
        /// Method to create api call on RM server to create a shipment 
        /// </summary>
        /// <param name="objDt">Order Details</param>
        /// <returns>Tracking Number</returns>
        public string CreateShipment(DataTable objDt)
        {
            try
            {
                int TotalWeight = Convert.ToInt32(Convert.ToDecimal(objDt.Rows[0]["TotalWeight"]) * 1000);
                string Created = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                byte[] nonce = SOAP_Utills.GetNonce();
                string nonceStr = Convert.ToBase64String(nonce);
                string Password_Digest = SOAP_Utills.CreateHashedPassword(nonce, Created, Password);
                string strPhoneNumber = Convert.ToString(objDt.Rows[0]["PhoneNumber"]).RemoveCharacters(")(,+- ");
                strPhoneNumber = string.IsNullOrEmpty(strPhoneNumber) ? "11111" : Regex.Replace(strPhoneNumber, @"\s+", "");
                Guid TransactionId = Guid.NewGuid();

                RoyalMailPrintLabel.lstimgLabelToPrints = null;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var client = new RestClient("https://api.royalmail.net/shipping/v2");
                var request = new RestRequest(Method.POST);
                request.AddHeader("accept", "application/soap+xml");
                request.AddHeader("soapaction", "createShipment");
                request.AddHeader("x-ibm-client-secret", Secret);
                request.AddHeader("x-ibm-client-id", ClientID);
                request.AddHeader("Host", "api.royalmail.net");
                string strXMLBody = @"<soapenv:Envelope xmlns:oas=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://www.royalmailgroup.com/integration/core/V1"" xmlns:v2=""http://www.royalmailgroup.com/api/ship/V2"">
   <soapenv:Header><wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">
  <wsse:UsernameToken><wsse:Username>" + UserName + @"</wsse:Username>
 <wsse:Password Type=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest"">" + Password_Digest + @"</wsse:Password>
 <wsse:Nonce EncodingType=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"">" + nonceStr + @"</wsse:Nonce>
 <wsu:Created>" + Created + @"</wsu:Created></wsse:UsernameToken></wsse:Security>
   </soapenv:Header>
   <soapenv:Body>
      <v2:createShipmentRequest>
         <v2:integrationHeader>
            <v1:dateTime>" + Created + @"</v1:dateTime>
            <v1:version>2</v1:version>
            <v1:identification>
               <v1:applicationId>" + ApplicationID + @"</v1:applicationId>
               <v1:transactionId>" + TransactionId + @"</v1:transactionId>
            </v1:identification>
         </v2:integrationHeader>
          <v2:requestedShipment>
            <v2:shipmentType>
               <code>" + Convert.ToString(objDt.Rows[0]["ShipmentType"]) + @"</code>
            </v2:shipmentType>
            <v2:serviceOccurrence>1</v2:serviceOccurrence>
            <v2:serviceType>
               <code>" + Convert.ToString(objDt.Rows[0]["ServiceType"]) + @"</code>
               <name/>
            </v2:serviceType>
            <v2:serviceOffering>
               <serviceOfferingCode>
                  <identifier/>
                  <code>" + Convert.ToString(objDt.Rows[0]["ServiceOfferingCode"]) + @"</code>
               </serviceOfferingCode>
            </v2:serviceOffering>
            <v2:serviceFormat>
               <serviceFormatCode>
                  <code>" + Convert.ToString(objDt.Rows[0]["ServiceFormatCode"]) + @"</code>
               </serviceFormatCode>
            </v2:serviceFormat>
            <v2:bfpoFormat>
               <bFPOFormatCode>
                  <code/>
               </bFPOFormatCode>
            </v2:bfpoFormat>
            <v2:serviceEnhancements>
               <v2:enhancementType>
                  <serviceEnhancementCode>
                    <code>" + Convert.ToString(objDt.Rows[0]["Code"]) + @"</code>
                  </serviceEnhancementCode>
               </v2:enhancementType>
            </v2:serviceEnhancements>
            <v2:signature>" + Convert.ToString(objDt.Rows[0]["Signature"]) + @"</v2:signature>
            <v2:shippingDate>" + DateTime.Now.ToString("yyyy-MM-dd") + @"</v2:shippingDate>
            <v2:recipientContact>
               <v2:name>" + Convert.ToString(objDt.Rows[0]["FullName"]) + @"</v2:name>
               <v2:complementaryName>" + Convert.ToString(objDt.Rows[0]["Company"]) + @"</v2:complementaryName>
               <v2:telephoneNumber>
                  <countryCode>0044</countryCode>
                  <telephoneNumber>" + strPhoneNumber + @"</telephoneNumber>
               </v2:telephoneNumber>
               <v2:electronicAddress>
                  <electronicAddress>" + Convert.ToString(objDt.Rows[0]["EmailAddress"]) + @"</electronicAddress>
               </v2:electronicAddress>
            </v2:recipientContact>
            <v2:recipientAddress>
               <buildingName/>
               <buildingNumber>0</buildingNumber>
               <addressLine1>" + Convert.ToString(objDt.Rows[0]["Address1"]).RemoveCharacters("&") + @"</addressLine1>
               <addressLine2>" + Convert.ToString(objDt.Rows[0]["Address2"]).RemoveCharacters("&") + @"</addressLine2>
               <addressLine3>" + Convert.ToString(objDt.Rows[0]["Address3"]).RemoveCharacters("&") + @"</addressLine3>
               <addressLine4>" + Convert.ToString(objDt.Rows[0]["Region"]) + @"</addressLine4>
               <stateOrProvince>
                  <stateOrProvinceCode>
                     <!--Optional:-->
                     <identifier/>
                     <code/>
                     <name/>
                     <description/>
                  </stateOrProvinceCode>
               </stateOrProvince>
               <postTown>" + Convert.ToString(objDt.Rows[0]["PostTown"]) + @"</postTown>
               <postcode>" + Convert.ToString(objDt.Rows[0]["PostCode"]) + @"</postcode>
               <country>
                  <countryCode>
                     <code>" + Convert.ToString(objDt.Rows[0]["CountryCode"]) + @"</code>
                  </countryCode>
               </country>
            </v2:recipientAddress>
            <v2:items>
               <v2:item>
                  <v2:numberOfItems>1</v2:numberOfItems>
                  <v2:weight>
                     <unitOfMeasure>
                        <unitOfMeasureCode>
                           <code>g</code>
                        </unitOfMeasureCode>
                     </unitOfMeasure>
                     <value>" + TotalWeight + @"</value>
                  </v2:weight>
               </v2:item>
            </v2:items>
            <!--Customer Reference is an aggregate reference and impacts billing. 
                Check with the OBA team before populating this field -->
            <v2:customerReference/>
            <!--sendersReference is used for individal shipment reference-->
            <v2:senderReference>" + Convert.ToString(objDt.Rows[0]["NumOrderId"]) + @"</v2:senderReference>
            <v2:safePlace>0</v2:safePlace>
         </v2:requestedShipment>
      </v2:createShipmentRequest>
   </soapenv:Body>
</soapenv:Envelope>";
                request.AddParameter("text/xml", strXMLBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                strResponseCode = response.StatusCode.ToString();
                string strResponseData = response.Content;
                string strApiCallUrlToLog = "Api call for https://api.royalmail.net/shipping/v2 createShipment";
                GlobalVariablesAndMethods.LogHistory(string.Format("{0} created with Transaction id:- {1}&&Password Digest:- {2}&&Nonce:- {3}&&Status code:- {4}", strApiCallUrlToLog, TransactionId.ToString(), Password_Digest, nonceStr, strResponseCode));
                if (strResponseCode.ToUpper() != "OK")
                {
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed with error&&Response:- {1}", strApiCallUrlToLog, strResponseData));
                    throw new CustomException(strResponseData);
                }
                else if (strResponseData.Contains("<errorDescription>"))
                {
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed with error&&Response:- {1}", strApiCallUrlToLog, strResponseData));
                    ShowErrorForRoyalMail(strResponseData);
                }
                else if (strResponseData.Contains("<exceptionText>"))
                {
                    if (Convert.ToString(objDt.Rows[0]["ServiceCourierID"]) == "" && Convert.ToString(objDt.Rows[0]["ShipmentType"]) == "" && Convert.ToString(objDt.Rows[0]["ServiceType"]) == "")
                    {
                        string strPostalServiceName = Convert.ToString(objDt.Rows[0]["PostalServiceName"]);
                        string strError = string.Format(" '{0}' is not mapped with any courier, please map that on Royal Mail Setting screen", strPostalServiceName);
                        GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed with error :- {1}", strApiCallUrlToLog, strError));
                        throw new CustomException(strError);
                    }
                    else
                    {
                        string strError = GlobalVariablesAndMethods.GetSubString(strResponseData, "<exceptionText>", "</exceptionText>");
                        GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed with error :- {1}", strApiCallUrlToLog, strError));
                        throw new CustomException(strError);
                    }
                }
                else if (strResponseData.Contains("shipmentNumber"))
                {
                    strShipmentNumber = GlobalVariablesAndMethods.GetSubString(strResponseData, "<shipmentNumber>", "</shipmentNumber>");
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed successfully&&Got Tracking number :- {1}", strApiCallUrlToLog, strShipmentNumber));
                    PrintLabel();
                }
                return strShipmentNumber;
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory(string.Format("Exception occurred in create shipment method,&&Exception:- {0}", ex.Message));
                throw ex;
            }
        }

        /// <summary>
        /// Method to create api call on RM server to get label to print
        /// </summary>
        public void PrintLabel()
        {
            try
            {
                RoyalMailPrintLabel.IsManifest = false;
                RoyalMailPrintLabel.lstimgLabelToPrints = null;

                string Created = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                byte[] nonce = SOAP_Utills.GetNonce();
                string nonceStr = Convert.ToBase64String(nonce);
                string Password_Digest = SOAP_Utills.CreateHashedPassword(nonce, Created, Password);
                Guid TransactionId = Guid.NewGuid();

                var client = new RestClient("https://api.royalmail.net/shipping/v2");
                var request = new RestRequest(Method.POST);
                request.AddHeader("accept", "application/soap+xml");
                request.AddHeader("soapaction", "printLabel");
                request.AddHeader("x-ibm-client-secret", Secret);
                request.AddHeader("x-ibm-client-id", ClientID);
                request.AddHeader("Host", "api.royalmail.net");
                string strXMLBody = @"<soapenv:Envelope xmlns:oas=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://www.royalmailgroup.com/integration/core/V1"" xmlns:v2=""http://www.royalmailgroup.com/api/ship/V2"">
   <soapenv:Header><wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">
  <wsse:UsernameToken><wsse:Username>" + UserName + @"</wsse:Username>
 <wsse:Password Type=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest"">" + Password_Digest + @"</wsse:Password>
 <wsse:Nonce EncodingType=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"">" + nonceStr + @"</wsse:Nonce>
 <wsu:Created>" + Created + @"</wsu:Created></wsse:UsernameToken></wsse:Security>
   </soapenv:Header>
   <soapenv:Body>
      <v2:printLabelRequest>
         <v2:integrationHeader>
            <v1:dateTime>" + Created + @"</v1:dateTime>
            <v1:version>2</v1:version>
            <v1:identification>
               <v1:applicationId>" + ApplicationID + @"</v1:applicationId>
               <v1:transactionId>" + TransactionId + @"</v1:transactionId>
            </v1:identification>
         </v2:integrationHeader>
         <v2:shipmentNumber>" + strShipmentNumber + @"</v2:shipmentNumber>
         <v2:outputFormat>PDF</v2:outputFormat>
      </v2:printLabelRequest>
   </soapenv:Body>
</soapenv:Envelope>";
                request.AddParameter("text/xml", strXMLBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                strResponseCode = response.StatusCode.ToString();
                string strResponseData = response.Content;
                string strApiCallUrlToLog = "Api call for https://api.royalmail.net/shipping/v2 printLabel";
                GlobalVariablesAndMethods.LogHistory(string.Format("{0} created with Transaction id:- {1}&&Password Digest:- {2}&&Nonce:- {3}&&Status code:- {4}", strApiCallUrlToLog, TransactionId.ToString(), Password_Digest, nonceStr, strResponseCode));
                if (strResponseData.Contains("<errorDescription>"))
                {
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed with error&&Response:- {1}", strApiCallUrlToLog, strResponseData));
                    ShowErrorForRoyalMail(strResponseData);
                }
                else if (strResponseData.Contains("<label>"))
                {
                    string strLabel = GlobalVariablesAndMethods.GetSubString(strResponseData, "<label>", "</label>");
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed successfully&&Got Label to print&&Converting Base64Bit label to image to print", strApiCallUrlToLog));
                    ConvertBase64ToPdf(strLabel);
                    GlobalVariablesAndMethods.LogHistory("Label is ready to print");
                }
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory(string.Format("Exception occurred in PrintLabel method,&&Exception:- {0}", ex.Message));
                throw ex;
            }
        }

        /// <summary>
        /// Method to create api call on RM server to cancel a shipment
        /// </summary>
        /// <param name="strShipment">array of shipment ids that we want to cancel</param>
        public void CancelShipmet(string[] strShipment)
        {
            try
            {
                string Created = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                byte[] nonce = SOAP_Utills.GetNonce();
                string nonceStr = Convert.ToBase64String(nonce);
                string Password_Digest = SOAP_Utills.CreateHashedPassword(nonce, Created, Password);
                Guid TransactionId = Guid.NewGuid();

                string strShipmentNode = string.Empty;
                foreach (string Num in strShipment)
                {
                    strShipmentNode = strShipmentNode + "<v2:shipmentNumber>" + Num + "</v2:shipmentNumber>\n";
                }

                var client = new RestClient("https://api.royalmail.net/shipping/v2");
                var request = new RestRequest(Method.POST);
                request.AddHeader("accept", "application/soap+xml");
                request.AddHeader("soapaction", "cancelShipment");
                request.AddHeader("x-ibm-client-secret", Secret);
                request.AddHeader("x-ibm-client-id", ClientID);
                request.AddHeader("Host", "api.royalmail.net");
                string strXMLBody = @"<soapenv:Envelope xmlns:oas=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://www.royalmailgroup.com/integration/core/V1"" xmlns:v2=""http://www.royalmailgroup.com/api/ship/V2"">
   <soapenv:Header><wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">
  <wsse:UsernameToken><wsse:Username>" + UserName + @"</wsse:Username>
 <wsse:Password Type=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest"">" + Password_Digest + @"</wsse:Password>
 <wsse:Nonce EncodingType=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"">" + nonceStr + @"</wsse:Nonce>
 <wsu:Created>" + Created + @"</wsu:Created></wsse:UsernameToken></wsse:Security>
   </soapenv:Header>
   <soapenv:Body>
      <v2:cancelShipmentRequest>
         <v2:integrationHeader>
            <v1:dateTime>" + Created + @"</v1:dateTime>
            <v1:version>2</v1:version>
            <v1:identification>
               <v1:applicationId>" + ApplicationID + @"</v1:applicationId>
               <v1:transactionId>" + TransactionId + @"</v1:transactionId>
            </v1:identification>
         </v2:integrationHeader>
         <v2:cancelShipments>
            <!--1 or more repetitions:-->
            @SipmentNumberNode
         </v2:cancelShipments>
      </v2:cancelShipmentRequest>
   </soapenv:Body>
</soapenv:Envelope>";
                strXMLBody = strXMLBody.Replace(" @SipmentNumberNode", strShipmentNode);//this because we need to repeat the <shipmentNumber> node when we want to pass more than one shipment number
                request.AddParameter("text/xml", strXMLBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string strResponseData = response.Content;
                strResponseCode = response.StatusCode.ToString();
                string strApiCallUrlToLog = "Api call for https://api.royalmail.net/shipping/v2 cancelShipment";
                GlobalVariablesAndMethods.LogHistory(string.Format("{0} with Transaction id:- {1}&&Password Digest:- {2}&&Nonce:- {3}&&Status code:- {4}", strApiCallUrlToLog, TransactionId.ToString(), Password_Digest, nonceStr, strResponseCode));
                if (strResponseData.Contains("<errorDescription>"))
                {
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed with error&&Response:- {1}", strApiCallUrlToLog, strResponseData));
                    ShowErrorForRoyalMail(strResponseData);
                }
                GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed successfully&&Response:- {1}", strApiCallUrlToLog, strResponseCode));
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory(string.Format("Exception occurred in CancelShipmet method,&&Exception:- {0}", ex.Message));
                throw ex;
            }
        }

        /// <summary>
        /// Method to create api call on server to create a manifest request for order
        /// </summary>
        public void CreateManifestRequest()
        {
            try
            {
                string Created = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                byte[] nonce = SOAP_Utills.GetNonce();
                string nonceStr = Convert.ToBase64String(nonce);
                string Password_Digest = SOAP_Utills.CreateHashedPassword(nonce, Created, Password);
                Guid TransactionId = Guid.NewGuid();

                var client = new RestClient("https://api.royalmail.net/shipping/v2");
                var request = new RestRequest(Method.POST);
                request.AddHeader("accept", "application/soap+xml");
                request.AddHeader("soapaction", "createManifest");
                request.AddHeader("x-ibm-client-secret", Secret);
                request.AddHeader("x-ibm-client-id", ClientID);
                request.AddHeader("Host", "api.royalmail.net");
                string strXMLBody = @"<soapenv:Envelope xmlns:oas=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://www.royalmailgroup.com/integration/core/V1"" xmlns:v2=""http://www.royalmailgroup.com/api/ship/V2"">
               <soapenv:Header><wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">
              <wsse:UsernameToken><wsse:Username>" + UserName + @"</wsse:Username>
             <wsse:Password Type=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest"">" + Password_Digest + @"</wsse:Password>
             <wsse:Nonce EncodingType=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"">" + nonceStr + @"</wsse:Nonce>
             <wsu:Created>" + Created + @"</wsu:Created></wsse:UsernameToken></wsse:Security>
               </soapenv:Header>
               <soapenv:Body>
                  <v2:createManifestRequest>
                     <v2:integrationHeader>
                        <v1:dateTime>" + Created + @"</v1:dateTime>
                           <v1:version>2</v1:version>
                           <v1:identification>
                           <v1:applicationId>" + ApplicationID + @"</v1:applicationId>
                           <v1:transactionId>" + TransactionId + @"</v1:transactionId>
                      </v1:identification>
                    </v2:integrationHeader>
                       <v2:yourReference/>
                 </v2:createManifestRequest>
               </soapenv:Body>
              </soapenv:Envelope>";
                request.AddParameter("text/xml", strXMLBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string strResponseData = response.Content;

                strResponseCode = response.StatusCode.ToString();
                string strApiCallUrlToLog = "Api call for https://api.royalmail.net/shipping/v2 createManifest";
                GlobalVariablesAndMethods.LogHistory(string.Format("{0} with Transaction id:- {1}&&Password Digest:- {2}&&Nonce:- {3}&&Status code:- {4}", strApiCallUrlToLog, TransactionId.ToString(), Password_Digest, nonceStr, strResponseCode));
                if (strResponseCode.ToUpper() != "OK")
                {
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed with error&&Response:- {1}", strApiCallUrlToLog, strResponseData));
                    throw new CustomException(strResponseData);
                }
                else if (strResponseData.Contains("<errorDescription>"))
                {
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed with error&&Response:- {1}", strApiCallUrlToLog, strResponseData));
                    ShowErrorForRoyalMail(strResponseData);
                }
                else if (strResponseData.Contains("<manifestBatchNumber>"))
                {
                    string strBatchNumber = GlobalVariablesAndMethods.GetSubString(strResponseData, "<manifestBatchNumber>", "</manifestBatchNumber>");
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed successfully &&Got ManifestBatchNumber:- {1}", strApiCallUrlToLog, strBatchNumber));
                    RoyalMailPrintLabel.blIsShipmentCreated = true;
                    PrintManifestLabel(strBatchNumber);
                }
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory(string.Format("Exception occurred in CreateManifestRequest method,&&Exception:- {0}", ex.Message));
                throw ex;
            }
        }

        /// <summary>
        /// Method to create api call on RM server to print label for manifest
        /// </summary>
        /// <param name="strManifestBatchNumber">Batch number of which we want to print manifest label</param>
        public void PrintManifestLabel(string strManifestBatchNumber)
        {
            try
            {
                bool IsThereAnyPendingManifestLabel;
                RoyalMailPrintLabel.IsManifest = true;
                string Created = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                byte[] nonce = SOAP_Utills.GetNonce();
                string nonceStr = Convert.ToBase64String(nonce);
                string Password_Digest = SOAP_Utills.CreateHashedPassword(nonce, Created, Password);
                Guid TransactionId = Guid.NewGuid();

                var client = new RestClient("https://api.royalmail.net/shipping/v2");
                var request = new RestRequest(Method.POST);
                request.AddHeader("accept", "application/soap+xml");
                request.AddHeader("soapaction", "printManifest");
                request.AddHeader("x-ibm-client-secret", Secret);
                request.AddHeader("x-ibm-client-id", ClientID);
                request.AddHeader("Host", "api.royalmail.net");
                string strXMLBody = @"<soapenv:Envelope xmlns:oas=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://www.royalmailgroup.com/integration/core/V1"" xmlns:v2=""http://www.royalmailgroup.com/api/ship/V2"">
               <soapenv:Header><wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">
              <wsse:UsernameToken><wsse:Username>" + UserName + @"</wsse:Username>
             <wsse:Password Type=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest"">" + Password_Digest + @"</wsse:Password>
             <wsse:Nonce EncodingType=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"">" + nonceStr + @"</wsse:Nonce>
             <wsu:Created>" + Created + @"</wsu:Created></wsse:UsernameToken></wsse:Security>
               </soapenv:Header>
               <soapenv:Body>
                     <v2:printManifestRequest>
                         <v2:integrationHeader>
                             <v1:dateTime>" + Created + @"</v1:dateTime>
                             <v1:version>2</v1:version>
                             <v1:identification>
                                <v1:applicationId>" + ApplicationID + @"</v1:applicationId>
                                <v1:transactionId>" + TransactionId + @"</v1:transactionId>
                             </v1:identification>
                          </v2:integrationHeader>
                          <!--Enter the manifestBatchNumber returned in the createManifest response-->
                              <v2:manifestBatchNumber>" + strManifestBatchNumber + @"</v2:manifestBatchNumber>
                      </v2:printManifestRequest>
                </soapenv:Body>
             </soapenv:Envelope>";
                request.AddParameter("text/xml", strXMLBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string strResponseData = response.Content;

                strResponseCode = response.StatusCode.ToString();
                string strApiCallUrlToLog = "Api call for https://api.royalmail.net/shipping/v2 printManifest";
                GlobalVariablesAndMethods.LogHistory(string.Format("{0} with Transaction id:- {1}&&Password Digest:- {2}&&Nonce:- {3}&&Status code:- {4}", strApiCallUrlToLog, TransactionId.ToString(), Password_Digest, nonceStr, strResponseCode));
                if (strResponseData.Contains("<errorDescription>"))
                {
                    string strError = GlobalVariablesAndMethods.GetSubString(strResponseData, "<errorDescription>", "</errorDescription>");
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed with error&&Response:- {1}", strApiCallUrlToLog, strResponseData));
                    if (strError.Contains("not ready for printing"))
                    {
                        //If the label is not ready for now then we are storing that batch number in our database so that we could print later.
                        RoyalMailPrintLabel.WorkWithPendingLabels(strManifestBatchNumber, "ADD", out IsThereAnyPendingManifestLabel);
                    }
                    throw new CustomException(strError);
                }
                else if (strResponseData.Contains("<manifest>"))
                {
                    string strLabel = GlobalVariablesAndMethods.GetSubString(strResponseData, "<manifest>", "</manifest>");
                    GlobalVariablesAndMethods.LogHistory(string.Format("{0} completed successfully and got manifest label", strApiCallUrlToLog));
                    ConvertBase64ToPdf(strLabel);
                }
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory(string.Format("Exception occurred in PrintManifestLabel method,&&Exception:- {0}", ex.Message));
                throw ex;
            }
        }

        /// <summary>
        /// Method to get and return errors which we are getting under RM api responses
        /// </summary>
        /// <param name="strResponseData">xml response in string fromat from which we will find error text</param>
        private static void ShowErrorForRoyalMail(string strResponseData)
        {
            string strError = GlobalVariablesAndMethods.GetSubString(strResponseData, "<errorDescription>", "</errorDescription>");
            throw new CustomException(strError);
        }

        /// <summary>
        /// Method to convert the label we got in base 64 format to pdf so that we could print that
        /// </summary>
        /// <param name="barcode">Base64 format label</param>
        public static void ConvertBase64ToPdf(string barcode)
        {
            try
            {
                byte[] bytes = Convert.FromBase64String(barcode);
                string filepath = System.IO.Path.GetTempPath(); //get temp folder path
                Random _r = new Random();
                int n = _r.Next();
                string file_location = filepath + n + ".pdf"; //image location
                System.IO.File.WriteAllBytes(file_location, bytes);
                string folder = System.IO.Path.GetTempPath() + "/RoyalMailLabels";
                LoadImagePdf(file_location, filepath);
                System.IO.File.Delete(file_location);
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory(string.Format("Exception occurred in ConvertBase64ToPdf method,&&Exception:- {0}", ex.Message));
            }
        }

        /// <summary>
        /// Method to load converted pdf label to image, to pass that converted image to printer
        /// </summary>
        /// <param name="InputPDFFile">Location of converted pdf label</param>
        /// <param name="OutPath">Location where we want to store converted image(pdf to image)</param>
        public static void LoadImagePdf(string InputPDFFile, string OutPath)
        {
            try
            {
                System.Collections.Generic.List<System.Drawing.Image> lstimgLabel = new System.Collections.Generic.List<Image>();
                RoyalMailPrintLabel.lstimgLabelToPrints = null;
                string outImageName = Path.GetFileNameWithoutExtension(InputPDFFile);
                PdfDocument pdf = PdfReader.Open(InputPDFFile, PdfDocumentOpenMode.Import);
                if (pdf.Pages.Count == 1)
                {
                    outImageName = outImageName + ".png";
                    string strOutputPath = OutPath + outImageName;
                    int point1 = (int)pdf.Pages[0].Height.Point;
                    int point2 = (int)pdf.Pages[0].Width.Point;
                    GhostscriptSettings settings = new GhostscriptSettings();
                    settings.Device = GhostscriptDevices.png256;
                    settings.Size = new GhostscriptPageSize()
                    {
                        Manual = new Size(point2, point1)
                    };
                    Size size = new Size(400, 400);
                    settings.Resolution = size;
                    GhostscriptWrapper.GenerateOutput(InputPDFFile, strOutputPath, settings);
                    Bitmap bitmap = new Bitmap(strOutputPath);
                    try
                    {
                        System.IO.File.Delete(InputPDFFile);
                        //System.IO.File.Delete(strOutputPath);
                    }
                    catch
                    {
                    }
                    System.Drawing.Image imgLabel = System.Drawing.Image.FromFile(strOutputPath);
                    RoyalMailPrintLabel.imgLabelToPrint = imgLabel;
                }
                else
                {
                    for (int i = 0; i < pdf.Pages.Count; i++)
                    {
                        string str1 = Guid.NewGuid().ToString();
                        outImageName = outImageName + str1 + ".png";
                        string strOutputPath = OutPath + outImageName;
                        int point1 = (int)pdf.Pages[0].Height.Point;
                        int point2 = (int)pdf.Pages[0].Width.Point;
                        GhostscriptSettings settings = new GhostscriptSettings();
                        settings.Device = GhostscriptDevices.png256;
                        settings.Page.Start = i + 1;
                        settings.Page.End = pdf.Pages.Count;
                        settings.Size = new GhostscriptPageSize()
                        {
                            Manual = new Size(point2, point1)
                        };
                        Size size = new Size(300, 300);
                        settings.Resolution = size;
                        GhostscriptWrapper.GenerateOutput(InputPDFFile, strOutputPath, settings);
                        Bitmap bitmap = new Bitmap(strOutputPath);
                        try
                        {
                            //System.IO.File.Delete(InputPDFFile);
                            //System.IO.File.Delete(strOutputPath);
                        }
                        catch
                        {
                        }
                        System.Drawing.Image imgLabel = System.Drawing.Image.FromFile(strOutputPath);
                        lstimgLabel.Add(imgLabel);
                    }
                    RoyalMailPrintLabel.lstimgLabelToPrints = lstimgLabel;
                }
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory(string.Format("Exception occurred in LoadImagePdf method to load lable for printing process,&&Exception:- {0}", ex.Message));
                throw ex;
            }
        }
        #endregion
    }
}
