﻿using System;
using System.Security.Cryptography;
using System.Text;
namespace ODM.Data.Util
{
    public class SOAP_Utills
    {
        /// 
        /// Creates the hashed password.
        /// 
        public static string CreateHashedPassword(byte[] nonce, string created, string password)
        {
            byte[] createdBytes = Encoding.UTF8.GetBytes(created);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA1.Create().ComputeHash(passwordBytes);
            byte[] combined = new byte[nonce.Length + createdBytes.Length + passwordBytes.Length];
            Buffer.BlockCopy(nonce, 0, combined, 0, nonce.Length);
            Buffer.BlockCopy(createdBytes, 0, combined, nonce.Length, createdBytes.Length);
            Buffer.BlockCopy(passwordBytes, 0, combined, nonce.Length + createdBytes.Length, passwordBytes.Length);
            return Convert.ToBase64String(SHA1.Create().ComputeHash(combined));
        }

        /// 
        /// Create a Nonce
        /// returns a random nonce.
        public static byte[] GetNonce()
        {
            byte[] nonce = new byte[0x10];
            RandomNumberGenerator generator = new RNGCryptoServiceProvider();
            generator.GetBytes(nonce);
            return nonce;
        }
    }
}

