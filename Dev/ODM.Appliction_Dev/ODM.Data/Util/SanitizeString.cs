﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Data.Util
{
    public static class SanitizeString
    {
        private static string SubstituteString(string value, char charSet, string substituteWith)
        {
            var indexOfChar = value.IndexOf(charSet);
            var result = String.Empty;
            if (indexOfChar >= 0)
            {
                result = value.Substring(0, indexOfChar);
                result += substituteWith;
                var temp = value.Substring(indexOfChar + 1);
                while (temp.IndexOf(charSet) > 0)
                {
                    result += temp.Substring(0, temp.IndexOf(charSet));
                    result += substituteWith;
                    temp = temp.Substring(temp.IndexOf(charSet) + 1);

                }
                result += temp;
                temp = string.Empty;
            }
            else
            {
                result = value;
            }
            return result;

        }
        public static string RemoveCharacters(this string value, string characters)
        {
            string result = value;
            char[] chars = characters.ToCharArray();
            var charsToBeReplaceBy = new Dictionary<char, string>();
            foreach (var chr in chars)
            {
                switch (chr)
                {
                    case ')':
                        charsToBeReplaceBy.Add(')',string.Empty);
                        break;

                    case '(':
                        charsToBeReplaceBy.Add('(',string.Empty);
                        break;

                    case ',':
                        charsToBeReplaceBy.Add(',',string.Empty);
                        break;

                    case '&':
                        charsToBeReplaceBy.Add('&', "and");
                        break;

                    case '+':
                        charsToBeReplaceBy.Add('+', string.Empty);
                        break;

                    case '-':
                        charsToBeReplaceBy.Add('-', string.Empty);
                        break;

                    case '?':
                        charsToBeReplaceBy.Add('?', string.Empty);
                        break;

                    case '\'':
                        charsToBeReplaceBy.Add('\'', string.Empty);
                        break;

                    case '\"':
                        charsToBeReplaceBy.Add('\"', string.Empty);
                        break;

                    case ' ':
                        charsToBeReplaceBy.Add(' ', string.Empty);
                        break;

                        case '\\':
                        charsToBeReplaceBy.Add('\\',@"\\");
                        break;

                    default:
                        break;
                }
            }
            foreach (var chartoBeReplaceBy in charsToBeReplaceBy)
            {
                result = SubstituteString(result, chartoBeReplaceBy.Key, chartoBeReplaceBy.Value);
            }
            return result;
        }
        public static string LimitLengthTo(this string value, int length)
        {
            value = value.Trim();
            return value.Length < length ? value : value.Substring(0, length - 1);
        }
    }
}
