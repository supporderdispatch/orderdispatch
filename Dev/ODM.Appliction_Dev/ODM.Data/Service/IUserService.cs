﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.Entity;

namespace ODM.Data.Service
{
   public  interface IUserService
    {
        User CheckUserLogin(string loginName, string password);
        string AddUser(string loginName, string pass, string firstname, string lastname, string email, string phone, bool isAdmin, Guid createdBy);
        List<User> GetAllUsers();
        int ChangePassword(Guid userid, string password);

    }
}
