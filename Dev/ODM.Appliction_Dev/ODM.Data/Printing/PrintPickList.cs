﻿using GhostscriptSharp;
using GhostscriptSharp.Settings;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Drawing;


namespace ODM.Data.Printing
{
    public class PrintPickList
    {
        public System.Drawing.Image PickListImage { get; set; }
        public List<System.Drawing.Image> lstPickListImage = new List<System.Drawing.Image>();
        public string PageXofNFooter = string.Empty;

        public void printPickList(string batchName,string pickListPage)
        {
            string pdffilename = System.IO.Path.GetTempPath() + batchName + ".pdf";
            string imageFilename = System.IO.Path.GetTempPath();

            Document document = new Document(PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(pdffilename, FileMode.Create));
            document.Open();
            HTMLWorker hw = new HTMLWorker(document);
            StringReader sr = new StringReader(pickListPage);
            XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
            document.Close();
            LoadImagePdf(pdffilename, imageFilename);
            SendPickListToPrint();
        }
        private void PrintPage(object o, PrintPageEventArgs e)
        {
            System.Drawing.Image img = PickListImage;
            float MarginLeft = 0.00f;
            float MarginTop = 0.00f;
            e.Graphics.DrawImage(img, MarginLeft, MarginTop);
            e.Graphics.DrawString(PageXofNFooter, new System.Drawing.Font("Arial", 12.0f), Brushes.Black, e.PageBounds.Width - 210, e.PageBounds.Height - 60);
        }

        public void SendPickListToPrint()
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForPickList))
                {
                    pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForPickList;
                    if (lstPickListImage.Count>0)
                    {
                        for (int i = 0; i < lstPickListImage.Count; i++)
                        {
                            PickListImage = lstPickListImage[i];
                            PageXofNFooter = String.Format("Page {0} of {1}",i+1,lstPickListImage.Count);
                            pd.PrintPage += PrintPage;
                            pd.Print();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }
        public void LoadImagePdf(string InputPDFFile, string OutPath)
        {
            try
            {
                PdfSharp.Pdf.PdfDocument pdf = PdfSharp.Pdf.IO.PdfReader.Open(InputPDFFile, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                for (int i = 0; i < pdf.Pages.Count; i++)
                {
                    string outImageName = Path.GetFileNameWithoutExtension(InputPDFFile);
                    string str1 = Guid.NewGuid().ToString();
                    outImageName = outImageName + str1 + ".png";
                    string strOutputPath = OutPath + outImageName;
                    int point1 = (int)pdf.Pages[0].Height.Point;
                    int point2 = (int)pdf.Pages[0].Width.Point;
                    GhostscriptSettings settings = new GhostscriptSettings();
                    settings.Device = GhostscriptDevices.png256;
                    settings.Page.Start = i + 1;
                    settings.Page.End = pdf.Pages.Count;
                   
                    settings.Size = new GhostscriptPageSize()
                    {
                        Manual = new Size(point2, point1)
                    };
                    Size size = new Size(400, 400);
                    settings.Resolution = size;
                   
                    GhostscriptWrapper.GenerateOutput(InputPDFFile, strOutputPath, settings);
                    Bitmap bitmap = new Bitmap(strOutputPath);
                    System.Drawing.Image imgLabel = System.Drawing.Image.FromFile(strOutputPath);
                    lstPickListImage.Add(imgLabel);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
