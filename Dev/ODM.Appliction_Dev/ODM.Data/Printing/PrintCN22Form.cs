﻿using GhostscriptSharp;
using GhostscriptSharp.Settings;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Drawing;


namespace ODM.Data.Printing
{
    public class PrintCN22Form
    {
        public System.Drawing.Image CN22FormImage { get; set; }

        public void printCN22Form(string orderId, string filledCN22Form)
        {
            try
            {
                string pdffilename = System.IO.Path.GetTempPath() + orderId + ".pdf";
                string imageFilename = System.IO.Path.GetTempPath();

                Document document = new Document(PageSize.B5);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(pdffilename, FileMode.Create));
                document.Open();
                HTMLWorker hw = new HTMLWorker(document);
                StringReader sr = new StringReader(filledCN22Form);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
                document.Close();
                LoadImagePdf(pdffilename, imageFilename);
                SendFilledCN22FormToPrint();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void PrintPage(object o, PrintPageEventArgs e)
        {
            System.Drawing.Image img = CN22FormImage;
            float MarginLeft = -50.00f;
            float MarginTop = -48.00f;
            e.Graphics.DrawImage(img, MarginLeft, MarginTop);
        }

        public void SendFilledCN22FormToPrint()
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                if (!string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForCN22Form))
                {
                    pd.DefaultPageSettings.PrinterSettings.PrinterName = GlobalVariablesAndMethods.SelectedPrinterNameForCN22Form;
                    pd.PrintPage += PrintPage;
                    pd.Print();
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }
        public void LoadImagePdf(string InputPDFFile, string OutPath)
        {
            try
            {
                PdfSharp.Pdf.PdfDocument pdf = PdfSharp.Pdf.IO.PdfReader.Open(InputPDFFile, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                for (int i = 0; i < pdf.Pages.Count; i++)
                {
                    string outImageName = Path.GetFileNameWithoutExtension(InputPDFFile);
                    string str1 = Guid.NewGuid().ToString();
                    outImageName = outImageName + str1 + ".png";
                    string strOutputPath = OutPath + outImageName;
                    int point1 = (int)pdf.Pages[0].Height.Point;
                    int point2 = (int)pdf.Pages[0].Width.Point;
                    GhostscriptSettings settings = new GhostscriptSettings();
                    settings.Device = GhostscriptDevices.png256;
                    settings.Page.Start = i + 1;
                    settings.Page.End = pdf.Pages.Count;

                    settings.Size = new GhostscriptPageSize()
                    {
                        Manual = new Size(point2, point1-10)
                    };
                    Size size = new Size(300, 300);
                    settings.Resolution = size;

                    GhostscriptWrapper.GenerateOutput(InputPDFFile, strOutputPath, settings);
                    Bitmap bitmap = new Bitmap(strOutputPath);
                    System.Drawing.Image imgLabel = System.Drawing.Image.FromFile(strOutputPath);
                    imgLabel.RotateFlip(RotateFlipType.Rotate90FlipXY);
                    CN22FormImage = imgLabel;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
