﻿using GhostscriptSharp;
using GhostscriptSharp.Settings;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Drawing;


namespace ODM.Data.Printing
{
    public class PrintInvoiceDynamically
    {
        public System.Drawing.Image InvoiceImage { get; set; }
        public List<System.Drawing.Image> lstInvoiceImages = new List<System.Drawing.Image>();

        private void PrintPage(object o, PrintPageEventArgs e)
        {
            System.Drawing.Image img = InvoiceImage;
            float MarginLeft = 0.00f;
            float MarginTop = 0.00f;
            e.Graphics.DrawImage(img, MarginLeft, MarginTop);
        }

        public void SendInvoiceToPrint(string printer)
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                printer = string.IsNullOrEmpty(printer) ? GlobalVariablesAndMethods.SelectedPrinterNameForInvoice : printer;
                if (!string.IsNullOrWhiteSpace(printer))
                {
                    pd.DefaultPageSettings.PrinterSettings.PrinterName = printer;
                    if (lstInvoiceImages.Count > 0)
                    {
                        for (int i = 0; i < lstInvoiceImages.Count; i++)
                        {
                            InvoiceImage = lstInvoiceImages[i];
                            pd.PrintPage += PrintPage;
                            pd.Print();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }
        public void LoadImagePdf(string InputPDFFile, string OutPath,ODM.Entities.InvoiceTemplate.PageOrientation pageOrientation)
        {
            try
            {
                PdfSharp.Pdf.PdfDocument pdf = PdfSharp.Pdf.IO.PdfReader.Open(InputPDFFile, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                for (int i = 0; i < pdf.Pages.Count; i++)
                {
                    string outImageName = Path.GetFileNameWithoutExtension(InputPDFFile);
                    string str1 = Guid.NewGuid().ToString();
                    outImageName = outImageName + str1 + ".png";
                    string strOutputPath = OutPath + outImageName;
                    int point1 = (int)pdf.Pages[0].Height.Point;
                    int point2 = (int)pdf.Pages[0].Width.Point;
                    GhostscriptSettings settings = new GhostscriptSettings();
                    settings.Device = GhostscriptDevices.png256;
                    settings.Page.Start = i + 1;
                    settings.Page.End = pdf.Pages.Count;

                    settings.Size = new GhostscriptPageSize()
                    {
                        Manual = new Size(point2, point1)
                    };
                    Size size = new Size(400, 400);
                    settings.Resolution = size;

                    GhostscriptWrapper.GenerateOutput(InputPDFFile, strOutputPath, settings);
                    Bitmap bitmap = new Bitmap(strOutputPath);
                    System.Drawing.Image imgLabel = System.Drawing.Image.FromFile(strOutputPath);
                    if (pageOrientation == ODM.Entities.InvoiceTemplate.PageOrientation.Landscape)
                    {
                        imgLabel.RotateFlip(RotateFlipType.Rotate90FlipXY);
                    }
                    lstInvoiceImages.Add(imgLabel);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PrintInvoice(string pdffilename, string imageFilename, ODM.Entities.InvoiceTemplate.PageOrientation pageOrientation,string printer)
        {
            LoadImagePdf(pdffilename, imageFilename,pageOrientation);
            SendInvoiceToPrint(printer);
        }
    }
}
