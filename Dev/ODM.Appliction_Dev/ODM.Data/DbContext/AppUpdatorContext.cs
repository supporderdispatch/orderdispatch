﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace ODM.Data.DbContext
{
    public class AppUpdatorContext:Context
    {
        public DataTable GetLatestAppVersion()
        {
            var result = new DataTable();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "GET_LATEST_APP_VERSION";
            try
            {
                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetAppVersionDetails", param).Tables[0];
            }
            catch (Exception)
            {
                //throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }

        public void AddAppVersion(Entities.AppVersionDetails appVersion)
        {
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
            param[0].Value = "ADD_APP_VERSION";

            param[1] = new SqlParameter("@AppVersion", SqlDbType.VarChar, 10);
            param[1].Value = appVersion.AppVersion;

            param[2] = new SqlParameter("@Remark", SqlDbType.VarChar, 1000);
            param[2].Value = appVersion.Remark;

            param[3] = new SqlParameter("@DownloadUrl", SqlDbType.VarChar, 1000);
            param[3].Value = appVersion.DownloadUrl;


            param[4] = new SqlParameter("@Approved", SqlDbType.Bit);
            param[4].Value = false;

            try
            {
                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetAppVersionDetails", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
        }

        public void ApproveAppVersion(Entities.AppVersionDetails appVersion)
        {
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
            param[0].Value = "APPROVE_APP_VERSION";

            param[1] = new SqlParameter("@AppVersion", SqlDbType.VarChar, 10);
            param[1].Value = appVersion.AppVersion;

            try
            {
                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetAppVersionDetails", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
        }

        public void RejectAppVersion(Entities.AppVersionDetails appVersion)
        {
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
            param[0].Value = "REJECT_APP_VERSION";

            param[1] = new SqlParameter("@AppVersion", SqlDbType.VarChar, 10);
            param[1].Value = appVersion.AppVersion;

            try
            {
                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetAppVersionDetails", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
        }

        public DataTable GetLatestAppVersionToBeApprove()
        {
            var result = new DataTable();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
            param[0].Value = "GET_LATEST_APP_VERSION_TO_BE_APPROVE";
            try
            {
                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetAppVersionDetails", param).Tables[0];
            }
            catch (Exception)
            {
                //throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
    }
}
