﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ODM.Data.Util;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using ODM.Entities;


namespace ODM.Data.DbContext
{
    public class LoggerContext:Context
    {

        public DataTable GetLogsOfOrders(string orderNumber)
        {
            var logs = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@SearchValue", SqlDbType.VarChar, 40);
                objParam[0].Value = orderNumber;
                objParam[1] = new SqlParameter("@SearchMode", SqlDbType.VarChar, 30);
                objParam[1].Value = "ID";


                logs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetLogDetails", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return logs;
        }

        public void AddUserLog(UserStatics userStatistics, int ordersCount, double perOrderProcessRate,string orderLogs)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[8];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
                objParam[0].Value = "SET";
                objParam[1] = new SqlParameter("@UserId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = userStatistics.UserId;
                objParam[2]=new SqlParameter("@SessionId",SqlDbType.UniqueIdentifier);
                objParam[2].Value=userStatistics.SessionId;
                objParam[3]=new SqlParameter("@LoginTime",SqlDbType.DateTime);
                objParam[3].Value=userStatistics.LoginTime;
                objParam[4]=new SqlParameter("@OrdersProcessed",SqlDbType.Int);
                objParam[4].Value=ordersCount;
                objParam[5]=new SqlParameter("@PerOrderProcessRate",SqlDbType.Decimal);
                objParam[5].Value=Decimal.Round(Convert.ToDecimal(perOrderProcessRate),2);
                objParam[6]=new SqlParameter("@LogoutTime",SqlDbType.DateTime);
                objParam[6].Value=userStatistics.LogoutTime;
                objParam[7] = new SqlParameter("@OrderLogs", SqlDbType.VarChar, 1000);
                objParam[7].Value = orderLogs;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetUserStatisticsLogs", objParam);
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public DataTable GetUserLogs(Guid UserId,DateTime ofDate)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[3];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
                objParam[0].Value = "GET";
                objParam[1] = new SqlParameter("@UserId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = UserId;
                objParam[2] = new SqlParameter("@OfDate", SqlDbType.DateTime);
                objParam[2].Value = ofDate;

                var dataSet=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetUserStatisticsLogs", objParam);
                if (dataSet.Tables.Count > 0)
                {
                    result = dataSet.Tables[0];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public DataTable GetAllUserLogs(DateTime fromDate, DateTime toDate)
        {
             var result = new DataTable();
             try
             {
                 SqlParameter[] objParam = new SqlParameter[3];

                 objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
                 objParam[0].Value = "GET";
                 objParam[1] = new SqlParameter("@FromDate", SqlDbType.DateTime);
                 objParam[1].Value = fromDate;
                 objParam[2] = new SqlParameter("@ToDate", SqlDbType.DateTime);
                 objParam[2].Value = toDate;

                 var dataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetUserStatisticsLogs", objParam);
                 if (dataSet.Tables.Count > 0)
                 {
                     result = dataSet.Tables[0];
                 }
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally { PostOperationEvents(); }
             return result;
        }
    }
}
