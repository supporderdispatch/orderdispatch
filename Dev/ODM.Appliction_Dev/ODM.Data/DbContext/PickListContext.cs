﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Data.DbContext
{
    public class PickListContext:Context
    {
        private string StoredProcedureForPickList = "GetPickList";

        public DataTable CreateAndGetPickList(string batchName,string orderIds)
        {
            var result = new DataTable();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@Batch", SqlDbType.VarChar, 200);
            param[0].Value = batchName;
            param[1] = new SqlParameter("@OrderIds", SqlDbType.VarChar,Int32.MaxValue);
            param[1].Value = orderIds;

            try
            {
                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, StoredProcedureForPickList, param).Tables[0];
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }

        public DataTable GetPickingBatches()
        {
            var result = new DataTable();
            try
            {
                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetPickingBatches").Tables[0];
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
    }
}
