﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using ODM.Entities.InvoiceTemplate.InvoiceTemplateDataContent;
using System.Data.SqlClient;

namespace ODM.Data.DbContext
{
    public class InvoiceTemplateDataSettingContext:Context
    {
        public void SaveDataPrintSettings(DataPrintSettings dataPrintSettings)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[5];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "SET";

                objParam[1] = new SqlParameter("@TemplateId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = dataPrintSettings.TemplateId;

                objParam[2] = new SqlParameter("@DataExceedSelectedOption", SqlDbType.Int);
                objParam[2].Value = (int)dataPrintSettings.DataExceedActionSelected;

                objParam[3] = new SqlParameter("@TryAdjustBlock", SqlDbType.Bit);
                objParam[3].Value = dataPrintSettings.TryAdjustBlock;

                objParam[4] = new SqlParameter("@TemplateSelectedWhenDataExceed",SqlDbType.UniqueIdentifier);
                objParam[4].Value = dataPrintSettings.TemplateSelectedWhenDataExceed;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplateDataSettings", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }
        public DataTable GetDataPrintSettings(Guid templateId)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[5];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET";

                objParam[1] = new SqlParameter("@TemplateId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = templateId;


                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetInvoiceTemplateDataSettings", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }
    }
}
