﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Data.DbContext
{
    public class CN22FormContext:Context
    {

        public System.Data.DataTable GetDataOfPostalServicesMappedWithCN22()
        {
            var result = new DataTable();

            try
            {
                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAllPostalServices").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }

        public void UpdateCN22PostalServicesMappedSettingsFor(string strSelectedPostalIds)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@PostalIds", SqlDbType.VarChar, Int32.MaxValue);
                objParam[0].Value = strSelectedPostalIds;


                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "UpdateCN22PostalServicesMappings", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public DataTable GetItemDetailsOfOrder(Guid orderId)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[0].Value = orderId;


                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetCN22FormDetails", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }
    }
}
