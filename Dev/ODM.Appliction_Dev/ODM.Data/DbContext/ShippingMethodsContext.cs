﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.Util;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace ODM.Data.DbContext
{
    public class ShippingMethodsContext : Context
    {
        public ShippingMethodsContext() { }
        public DataTable GetShippingMethodAndData()
        {
            var result = new DataTable();
            try
            {
                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAllShippingMethodsWithVendor").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
        public bool ChangeShippingMethodOfOrderTo(string orderid, string shippingService)
        {
            var result = false;
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@orderid", SqlDbType.VarChar, 200);
            param[0].Value = orderid;
            param[1] = new SqlParameter("@postalServiceName", SqlDbType.VarChar, 200);
            param[1].Value = shippingService;
            try
            {
                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ChangeShippingMethodOfOrder", param);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
            return result;
        }
    }
}
