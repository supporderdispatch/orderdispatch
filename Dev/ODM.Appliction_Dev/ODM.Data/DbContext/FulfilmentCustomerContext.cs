﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Data.DbContext
{
    public class FulfilmentCustomerContext:Context
    {
        private readonly string SpForFulfilmentCustomer = "GetSetFulfilmentCustomer";
        private readonly string spForFulfilmentCustomerConditions = "GetSetFulfilmentCustomerConditions";

        public System.Data.DataTable GetCustomers()
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_All_CUSTOMERS";

                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, SpForFulfilmentCustomer, objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void SaveCustomer(Entities.FulFilmentCustomer customer)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[7];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "SAVE_CUSTOMER";

                objParam[1] = new SqlParameter("@CustomerId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = customer.CustomerId;

                objParam[2] = new SqlParameter("@CustomerName", SqlDbType.VarChar, 100);
                objParam[2].Value = customer.Name;

                objParam[3] = new SqlParameter("@Company", SqlDbType.VarChar, 100);
                objParam[3].Value = customer.Company;

                objParam[4] = new SqlParameter("@Username", SqlDbType.VarChar, 100);
                objParam[4].Value = customer.UserName;

                objParam[5] = new SqlParameter("@Password", SqlDbType.VarChar, 100);
                objParam[5].Value = customer.Password;

                objParam[6] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
                objParam[6].Value = customer.ConditionId;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, SpForFulfilmentCustomer, objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }


        public DataTable GetCustomer(Guid customerId)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_CUSTOMER";

                objParam[1] = new SqlParameter("@CustomerId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = customerId;

                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, SpForFulfilmentCustomer, objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void DeleteCustomer(Guid customerId)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "DELETE_CUSTOMER";

                objParam[1] = new SqlParameter("@CustomerId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = customerId;
              

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, SpForFulfilmentCustomer, objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public void SaveCondition(Entities.ConditionCriteria condition)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[7];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "INSERT_CONDITION";

                objParam[1] = new SqlParameter("ConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = condition.ConditionId;

                objParam[2] = new SqlParameter("@TableName", SqlDbType.VarChar, 100);
                objParam[2].Value = condition.TableName;

                objParam[3] = new SqlParameter("@ColumnName", SqlDbType.VarChar, 100);
                objParam[3].Value = condition.ColumnName;

                objParam[4] = new SqlParameter("@Creterion", SqlDbType.VarChar, 20);
                objParam[4].Value = condition.Creterion;

                objParam[5] = new SqlParameter("@CompareWith", SqlDbType.VarChar, 100);
                objParam[5].Value = condition.CompareWith;

                objParam[6] = new SqlParameter("@CriteriaDataType", SqlDbType.VarChar, 20);
                objParam[6].Value = string.IsNullOrEmpty(condition.CriteriaDataType) ? "Text" : condition.CriteriaDataType;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, spForFulfilmentCustomerConditions, objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public DataTable GetCustomerConditions(Guid conditionId)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_CUSTOMER_CONDITIONS";

                objParam[1] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = conditionId;


                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, spForFulfilmentCustomerConditions, objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void DeleteCustomerCondition(Guid conditionId)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "DELETE_CUSTOMER_CONDITIONS";

                objParam[1] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = conditionId;


                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, spForFulfilmentCustomerConditions, objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }
    }
}
