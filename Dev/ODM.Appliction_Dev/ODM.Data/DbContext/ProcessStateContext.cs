﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Entities;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace ODM.Data.DbContext
{
    public class ProcessStateContext:Context
    {
        public void UpdateProcessesStatusOfOrder(Guid orderId, ProcessCompleted state)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[3];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
                objParam[0].Value = "SET";
                objParam[1] = new SqlParameter("@OrderId", SqlDbType.VarChar,200);
                objParam[1].Value = orderId.ToString();
                objParam[2] = new SqlParameter("@ProcessedCompleted", SqlDbType.VarChar,200);
                objParam[2].Value = Convert.ToString((int)state);

                var data=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetCompletedProcessOfOrder", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
        }

        public void UnblockOrder(string orderId)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 200);
                objParam[0].Value = "UNBLOCK_ORDERS";
                objParam[1] = new SqlParameter("@NumOrderId", SqlDbType.VarChar, 200);
                objParam[1].Value = orderId;

                var data = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetCompletedProcessOfOrder", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PostOperationEvents();
            }
        }
    }
}
