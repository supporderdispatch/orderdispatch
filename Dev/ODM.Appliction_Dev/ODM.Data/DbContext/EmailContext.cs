﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using ODM.Entities.Emails;

namespace ODM.Data.DbContext
{
    public class EmailContext:Context
    {
        public EmailContext() { }

        public DataTable GetAllEmailTemplates()
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ALL_EMAIL_TEMPLATES";

                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplates", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void SaveEmailTemplate(EmailTemplate emailTemplate)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[13];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "ADD_UPDATE_EMAIL_TEMPLATE";

                objParam[1] = new SqlParameter("@EmailTemplateId ", SqlDbType.UniqueIdentifier);
                objParam[1].Value = emailTemplate.EmailTemplateId;

                objParam[2] = new SqlParameter("@EmailTemplateName", SqlDbType.VarChar, 200);
                objParam[2].Value = emailTemplate.EmailTemplateName;

                objParam[3] = new SqlParameter("@WithImage", SqlDbType.Bit);
                objParam[3].Value = emailTemplate.WithImage;

                objParam[4] = new SqlParameter("@WithPdf", SqlDbType.Bit);
                objParam[4].Value = emailTemplate.WithPdf;

                objParam[5] = new SqlParameter("@EmailTemplateConditionId", SqlDbType.UniqueIdentifier);
                objParam[5].Value = emailTemplate.EmailTemplateConditionId;

                objParam[6] = new SqlParameter("@IsEnabled", SqlDbType.Bit);
                objParam[6].Value = emailTemplate.IsEnabled;

                objParam[7] = new SqlParameter("@IsHtml", SqlDbType.Bit);
                objParam[7].Value = emailTemplate.IsHtml;

                objParam[8] = new SqlParameter("@Cc", SqlDbType.VarChar, 200);
                objParam[8].Value = emailTemplate.Cc;

                objParam[9] = new SqlParameter("@Bcc", SqlDbType.VarChar, 200);
                objParam[9].Value = emailTemplate.Bcc;

                objParam[10] = new SqlParameter("@Subject", SqlDbType.VarChar, 1000);
                objParam[10].Value = emailTemplate.Subject;

                objParam[11] = new SqlParameter("@Body", SqlDbType.VarChar, 3000);
                objParam[11].Value = emailTemplate.Body;

                objParam[12] = new SqlParameter("@MailFrom", SqlDbType.UniqueIdentifier);
                objParam[12].Value = emailTemplate.MailFrom;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplates", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public DataTable GetEmailTemplate(Guid emailTemplateId)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_EMAIL_TEMPLATE";

                objParam[1] = new SqlParameter("@EmailTemplateId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = emailTemplateId;

                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplates", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void DeleteEmailTemplate(Guid emailTemplateId)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "DELETE_EMAIL_TEMPLATE";

                objParam[1] = new SqlParameter("@EmailTemplateId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = emailTemplateId;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplates", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public void CopyEmailTemplate(string emailTemplateId)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "COPY_EMAIL_TEMPLATE";

                objParam[1] = new SqlParameter("@EmailTemplateId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = Guid.Parse(emailTemplateId);

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplates", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public void SaveEmailCondition(ODM.Entities.Emails.EmailCondition emailCondition)
        {
            var result = false;
            try
            {
                SqlParameter[] objParam = new SqlParameter[7];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "INSERT_EMAIL_CONDITION";

                objParam[1] = new SqlParameter("@EmailTemplateConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = emailCondition.EmailConditionId;

                objParam[2] = new SqlParameter("@TableName", SqlDbType.VarChar, 100);
                objParam[2].Value = emailCondition.TableName;

                objParam[3] = new SqlParameter("@ColumnName", SqlDbType.VarChar, 100);
                objParam[3].Value = emailCondition.ColumnName;

                objParam[4] = new SqlParameter("@Creterion", SqlDbType.VarChar, 20);
                objParam[4].Value = emailCondition.Creterion;

                objParam[5] = new SqlParameter("@CompareWith", SqlDbType.VarChar, 100);
                objParam[5].Value = emailCondition.CompareWith;

                objParam[6] = new SqlParameter("@CriteriaDataType", SqlDbType.VarChar, 20);
                objParam[6].Value = string.IsNullOrEmpty(emailCondition.CriteriaDataType) ? "Text" : emailCondition.CriteriaDataType;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplateConditions", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public object GetEmailCondtionsFor(Guid emailConditionId)
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_EMAIL_CONDITIONS";

                objParam[1] = new SqlParameter("@EmailTemplateConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = emailConditionId;


                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplateConditions", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void DeleteEmailConditionsOf(Guid emailConditionId)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "DELETE_EMAIL_CONDITIONS";

                objParam[1] = new SqlParameter("@EmailTemplateConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = emailConditionId;


                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplateConditions", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public void UpdateEmailConditionMatchForTemplate(Guid emailConditionId, EmailConditionCriteriaMatch selectedEmailCreterionMatch)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[3];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "UPDATE_TEMPLATE_CRETERION_MATCH";

                objParam[1] = new SqlParameter("@EmailTemplateConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = emailConditionId;

                objParam[2] = new SqlParameter("@EmailConditionCriteriaMatch", SqlDbType.Int);
                objParam[2].Value = (int)selectedEmailCreterionMatch;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplateConditions", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public string GetEmailConditionCriteriaMatch(Guid emailConditionId)
        {
            var result = string.Empty;
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_TEMPLATE_CRETERION_MATCH";

                objParam[1] = new SqlParameter("@EmailTemplateConditionId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = emailConditionId;


                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplateConditions", objParam).Tables[0].Rows[0]["EmailConditionCriteriaMatch"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public DataTable GetAllEmailConditions()
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ALL_EMAIL_CONDITIONS";


                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetEmailTemplateConditions", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void AddPendingMail(Email email)
        {
            string strLogText = "Making database call for details of pending email";
            try
            {
                SqlParameter[] objParam = new SqlParameter[7];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "CREATE_PENDING_MAIL";

                objParam[1] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = email.OrderId;

                objParam[2] = new SqlParameter("@MailToAddress", SqlDbType.VarChar, 50);
                objParam[2].Value = email.MailToAddress;

                objParam[3] = new SqlParameter("@SentMailBody", SqlDbType.VarChar, 3000);
                objParam[3].Value = email.SentMailBody;

                objParam[4] = new SqlParameter("@SentSubject", SqlDbType.VarChar, 1000);
                objParam[4].Value = email.SentSubject;

                objParam[5] = new SqlParameter("@IsMailSent", SqlDbType.Bit);
                objParam[5].Value = email.IsMailSent;

                objParam[6] = new SqlParameter("@EmailTemplateId", SqlDbType.UniqueIdentifier);
                objParam[6].Value = email.EmailTemplateId;


                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetMails", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            strLogText = strLogText + "&& Email Details save in database for NumOrderId:- " + email.NumOrderId;
            ODM.Data.Util.GlobalVariablesAndMethods.OrderIdToLog = email.OrderId;
            ODM.Data.Util.GlobalVariablesAndMethods.LogHistory(strLogText);


        }

        public DataTable GetAllPendingEmails()
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ALL_PENDING_MAILS";


                result = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetMails", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }

        public void DeleteMails(string orderIdsSelected)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "DELETE_PENDING_MAILS";

                objParam[1] = new SqlParameter("@OrderIdsToDelete", SqlDbType.VarChar, 3000);
                objParam[1].Value = orderIdsSelected;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetMails", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public void MarkMailAsSent(Guid orderId)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "UPDATE_MAIL_AS_SENT";

                objParam[1] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objParam[1].Value = orderId;

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetMails", objParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
        }

        public DataTable GetAllSentMails()
        {
            var result = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 100);
                objParam[0].Value = "GET_ALL_SENT_MAILS";


                result=SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetSetMails", objParam).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { PostOperationEvents(); }
            return result;
        }
    }
}
