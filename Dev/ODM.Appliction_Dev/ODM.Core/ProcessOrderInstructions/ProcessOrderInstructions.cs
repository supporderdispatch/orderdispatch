﻿using ODM.Core.Helpers;
using ODM.Entities;
using ODM.Entities.InvoiceTemplate;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Core.ProcessOrderInstructions
{
    public class ProcessOrderInstructions
    {
        public ODM.Entities.ProcessOrderInstrunctions.CriteriaResult GetInstructionsForOrder(Guid orderId)
        {
            var orderDetails = GetOrderDetails(orderId);
            var allActiveCriterias = new ProcessOrderCondition().GetActiveCriterias();
            if (allActiveCriterias.Count > 0)
            {
                foreach (var activeCriteria in allActiveCriterias.ToList())
                {
                    if (!CheckIfPrintConditionValid(activeCriteria, orderDetails.Where(x => x.TableName.Equals(activeCriteria.TableName)).FirstOrDefault()))
                    {
                        allActiveCriterias.Remove(activeCriteria);
                        allActiveCriterias = allActiveCriterias.ToList();
                    }
                }
                if (allActiveCriterias.Count > 0)
                {
                    var successCriteria=string.Empty;
                    allActiveCriterias.ForEach(x =>
                    {
                        successCriteria = successCriteria+x.ConditionCriteriaId.ToString() + ",";
                    });
                    successCriteria += allActiveCriterias.FirstOrDefault().ConditionCriteriaId.ToString();
                    var criteriaResults = Activator.CreateInstance<ODM.Core.ProcessOrderInstructions.ProcessOrderCondition>().GetCriteriaResultsOf(successCriteria);
                    var textResult=string.Empty;
                    criteriaResults.ForEach(x=>
                    {
                        textResult=textResult+x.TextResult+Environment.NewLine;
                    });

                    var imageResult=criteriaResults.Where(x=>!string.IsNullOrEmpty(x.ImageResult));
                    return new Entities.ProcessOrderInstrunctions.CriteriaResult()
                    {
                        TextResult=textResult,
                        ImageResult=imageResult.Count()>0?imageResult.FirstOrDefault().ImageResult:string.Empty
                    };
                }
            }
            return new ODM.Entities.ProcessOrderInstrunctions.CriteriaResult();
        }

        public List<DataTable> GetOrderDetails(Guid orderId)
        {
            var dataset = ((DataSet)new ODM.Data.DbContext.InvoiceTemplateContext().GetDataForInvoice(orderId));
            var orderDetails = new List<DataTable>();
            var orderOfTables = new Dictionary<int, string>();
            orderOfTables.Add(0, "Order");
            orderOfTables.Add(1, "OrderItem");
            orderOfTables.Add(2, "CustomerAddress");
            orderOfTables.Add(3, "SenderAddress");

            for (int i = 0; i < dataset.Tables.Count; i++)
            {
                var table = dataset.Tables[i];
                table.TableName = orderOfTables[i];
                orderDetails.Add(table);
            }
            return orderDetails;
        }
        private bool CheckIfPrintConditionValid(ConditionCriteria conditionCriteria, DataTable detailTable)
        {
            var tableName = conditionCriteria.TableName;
            if (tableName.Contains("Customer"))
                conditionCriteria.ColumnName = "Customer-" + conditionCriteria.ColumnName;

            if (tableName.Contains("Sender"))
                conditionCriteria.ColumnName = "Sender-" + conditionCriteria.ColumnName;

            if (!tableName.Equals("OrderItem"))
            {
                return CompareDataValueWithCreterion(conditionCriteria, detailTable.Rows[0][conditionCriteria.ColumnName].ToString());
            }
            else
            {
                var result = false;
                for (int rowIndex = 0; rowIndex < detailTable.Rows.Count; rowIndex++)
                {
                    result = result || CompareDataValueWithCreterion(conditionCriteria, detailTable.Rows[rowIndex][conditionCriteria.ColumnName].ToString());
                }
                return result;
            }
        }

        private bool CompareDataValueWithCreterion(ConditionCriteria conditionCriteria, string dbValue)
        {
            var result = false;
            var criteria = (PrintConditionCreterion)Enum.Parse(typeof(PrintConditionCreterion), conditionCriteria.Creterion);
            Type dataType = Conversions.GetPrintConditionCriteriaDataType(conditionCriteria.CriteriaDataType);
            switch (criteria)
            {
                case PrintConditionCreterion.equalTo:
                    {
                        try
                        {
                            result = Conversions.ParseToCustomDataType(dbValue.ToString(), dataType).Equals(Conversions.ParseToCustomDataType(conditionCriteria.CompareWith.ToString(), dataType));
                        }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.greaterThan:
                    {
                        try { result = Convert.ToDouble(dbValue) > Convert.ToDouble(conditionCriteria.CompareWith); }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.lessThan:
                    {
                        try { result = Convert.ToDouble(dbValue) < Convert.ToDouble(conditionCriteria.CompareWith); }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.like:
                    {
                        try { result = dbValue.ToString().ToLower().Trim().Contains(conditionCriteria.CompareWith.ToString().ToLower()); }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.In:
                    {
                        try
                        {

                            var valuesToCompareWith = conditionCriteria.CompareWith.ToString().Split(',').ToList();
                            result = valuesToCompareWith.Any(x => Conversions.ParseToCustomDataType(x, dataType).Equals(Conversions.ParseToCustomDataType(dbValue, dataType)));
                        }
                        catch
                        {

                        }
                        break;
                    }

                case PrintConditionCreterion.NotIn:
                    {
                        try
                        {
                            var valuesToCompareWith = conditionCriteria.CompareWith.ToString().Split(',').ToList();
                            result = !valuesToCompareWith.Any(x => Conversions.ParseToCustomDataType(x, dataType).Equals(Conversions.ParseToCustomDataType(dbValue, dataType)));
                        }
                        catch { }
                        break;
                    }
            }

            return result;
        }
    }
}
