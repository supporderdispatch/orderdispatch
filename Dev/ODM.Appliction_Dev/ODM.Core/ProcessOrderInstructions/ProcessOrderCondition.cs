﻿using ODM.Data.DbContext;
using ODM.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Core.ProcessOrderInstructions
{
    public class ProcessOrderCondition
    {
        private ProcessOrderInstructionContext Context;

        public ProcessOrderCondition()
        {
            Context = new ProcessOrderInstructionContext();
        }

        public DataTable GetConditinsFor(string ConditionId)
        {
            return Context.GetProcessOrderConditionCriteriaOf(Guid.Parse(ConditionId));
        }
        public void AddEmptyCondition(string ConditionId, ConditionCriteria conditionCriteria)
        {
            Context.SaveProcessOrderConditionCriteriaOf(Guid.Parse(ConditionId), conditionCriteria);
        }

        private bool DeleteConditionsCriteriaOf(string conditionId)
        {
            return Context.DeleteConditionsCriteriaOf(Guid.Parse(conditionId));
        }

        public void SaveConditionsCriteriaFor(string conditionId, List<ConditionCriteria> conditionCriterias)
        {
            if (DeleteConditionsCriteriaOf(conditionId))
            {
                foreach (var conditionCriteria in conditionCriterias)
                {
                    Context.SaveConditionsCriteriaOf(Guid.Parse(conditionId), conditionCriteria);
                }
            }
        }

        public void CopyCondition(string conditionId)
        {
            Context.CopyCondition(Guid.Parse(conditionId));
        }

        public void DeleteCondition(string conditionId)
        {
            Context.DeleteCondition(Guid.Parse(conditionId));
        }

        public void UpdateConditions(List<Condition> updatedConditions)
        {
            foreach (var updatedCondition in updatedConditions)
            {
                Context.UpdateCondition(updatedCondition);
            }
        }

        public void SaveConditionCriteriaResult(Entities.ProcessOrderInstrunctions.CriteriaResult criteriaResult)
        {
            Context.SaveConditionCriteriaResult(criteriaResult);
        }

        public void DeleteCriteria(Guid criteriaId)
        {
            Context.DeleteCriteria(criteriaId);
        }

        public Entities.ProcessOrderInstrunctions.CriteriaResult GetConditionCriteriaResult(Guid creteriaId)
        {
            var data=(DataTable)Context.GetConditionCriteriaResult(creteriaId);
            if (data.Rows.Count > 0)
            {
                return data.AsEnumerable().Select(x=>
                    new Entities.ProcessOrderInstrunctions.CriteriaResult()
                    {
                        ConditionCriteriaId = x.Field<Guid>("ConditionCriteriaId"),
                        ResultType = (ODM.Entities.ProcessOrderInstrunctions.CriteriaResultType)Enum.Parse(typeof(ODM.Entities.ProcessOrderInstrunctions.CriteriaResultType), x.Field<string>("ResultType")),
                        ImageResult = x.Field<string>("ResultImage"),
                        TextResult = x.Field<string>("ResultText")
                    }).FirstOrDefault();
            }

            return new Entities.ProcessOrderInstrunctions.CriteriaResult();
        }

        public List<ODM.Entities.ConditionCriteria> GetActiveCriterias()
        {
            var data = (DataTable)Context.GetAllActiveCriteria();
            if (data.Rows.Count > 0)
            {
                for (int rowIndex = 0; rowIndex < data.Rows.Count; rowIndex++)
                {
                    for (int columnIndex = 0; columnIndex < data.Columns.Count; columnIndex++)
                    {
                        if (data.Rows[rowIndex][columnIndex].GetType().Equals(typeof(DBNull)))
                        {
                            data.Rows.RemoveAt(rowIndex);
                            break;
                        }
                    }
                }
                data.AcceptChanges();

                return data.AsEnumerable().Select(x =>
                    new ODM.Entities.ConditionCriteria()
                    {
                        ConditionId = x.Field<Guid>("ConditionId"),
                        ColumnName = x.Field<string>("ColumnName"),
                        CompareWith = x.Field<string>("CompareWith"),
                        ConditionCriteriaId = (Guid)(x.Field<Guid>("ConditionCriteriaId").GetType().Equals(typeof(DBNull)) ? Guid.Empty : x.Field<Guid>("ConditionCriteriaId")),
                        ResultType = (ODM.Entities.ProcessOrderInstrunctions.CriteriaResultType)Enum.Parse(typeof(ODM.Entities.ProcessOrderInstrunctions.CriteriaResultType), x.Field<string>("ResultType")),
                        Creterion = x.Field<string>("Creterion"),
                        CriteriaDataType = x.Field<string>("CriteriaDataType"),
                        TableName = x.Field<string>("TableName")
                    }).ToList();
            }
            return new List<ConditionCriteria>() { };
        }

        public  List<ODM.Entities.ProcessOrderInstrunctions.CriteriaResult> GetCriteriaResultsOf(string successCriteria)
        {
            var data = Context.GetCriteriaResultOf(successCriteria);
            if (data.Rows.Count > 0)
            {
                return data.AsEnumerable().Select(x=>
                    new ODM.Entities.ProcessOrderInstrunctions.CriteriaResult() 
                    {
                        ConditionCriteriaId = x.Field<Guid>("ConditionCriteriaId"),
                        ImageResult = x.Field<string>("ResultImage"),
                        TextResult = x.Field<string>("ResultText"),
                        ResultType = (ODM.Entities.ProcessOrderInstrunctions.CriteriaResultType)Enum.Parse(typeof(ODM.Entities.ProcessOrderInstrunctions.CriteriaResultType), x.Field<string>("ResultType"))
                    }).ToList();
            }
            return new List<Entities.ProcessOrderInstrunctions.CriteriaResult>() { };
        }
    }
}
