﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.Util;
using ODM.Entities;

namespace ODM.Core.UserTracking
{
    public static class OrderTracker
    {
        public static Tracker Tracker = new Tracker();
        public static string CurrentOrder;
        public static string OrderId;
        public static List<OrderExecutionDetails> TrackingDigest = new List<OrderExecutionDetails>();
        public static List<string> OrderPool = new List<string>();

        public static void MarkOrderProcessed(string orderId)
        {
            try
            {
                var trackingDigestRecord = TrackingDigest.Where(x => x.OrderId.Equals(OrderId)).FirstOrDefault();
                if (TrackingDigest.Any(x => x.OrderId.Equals(orderId)))
                {
                    TrackingDigest.Remove(trackingDigestRecord);
                    trackingDigestRecord.Processed = true;
                    TrackingDigest.Add(trackingDigestRecord);
                }
                else
                {
                    TrackingDigest.Add(new OrderExecutionDetails()
                    {
                        OrderId = orderId,
                        Processed = true
                    });
                }
            }
            catch { }
        }

        public static void BeginTrackerForOrder(string orderId)
        {
            try
            {
                if(UserTracker.UserTrackingPaused)
                {
                    UserTracker.LoginTime = DateTime.UtcNow;
                    UserTracker.UserTrackingPaused=false;
                    UserTracker.NumberOfSessionWithoutAnyOrderProcessed = 0;
                    UserTracker.BeginTrackingForUser(UserTracker.UserId);
                }
                OrderId = orderId;
                Tracker.BeginTracking();
            }
            catch (Exception x)
            {
                GlobalVariablesAndMethods.OrderIdToLog = Guid.Parse(OrderId);
                GlobalVariablesAndMethods.LogHistory(String.Format("Exception while order tracking for order {0} : {1}", OrderId, x.Message));
            }
        }
        public static void EndTracking()
        {
            try
            {
                var timeCaptured = Tracker.StopTracking();
                if (!string.IsNullOrEmpty(OrderId))
                {
                    if (TrackingDigest.Any(x => x.OrderId.Equals(OrderId)))
                    {
                        var trackingDigestRecord = TrackingDigest.Where(x => x.OrderId.Equals(OrderId)).FirstOrDefault();
                        TrackingDigest.Remove(trackingDigestRecord);
                        trackingDigestRecord.TimeTakenForExecution = trackingDigestRecord.TimeTakenForExecution + timeCaptured;
                        TrackingDigest.Add(trackingDigestRecord);
                    }
                    else
                    {
                        TrackingDigest.Add(new OrderExecutionDetails()
                        {
                            OrderId=OrderId,
                            Processed=false,
                            TimeTakenForExecution=timeCaptured
                        });
                    }
                }

            }
            catch (Exception x)
            {
                GlobalVariablesAndMethods.OrderIdToLog = Guid.Parse(OrderId);
                GlobalVariablesAndMethods.LogHistory(String.Format("Exception while order tracking for order {0} : {1}", OrderId, x.Message));
            }
        }
    }
}
