﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Entities.Emails;

namespace ODM.Core.Emails
{
    public class EmailConditions
    {
        private ODM.Data.DbContext.EmailContext Context;

        public EmailConditions() 
        {
            Context = new Data.DbContext.EmailContext();
        }

        public static EmailConditions GetInstance()
        {
            return new EmailConditions();
        }

        public void SaveEmailCondition(EmailCondition condition)
        {
            Context.SaveEmailCondition(condition);
        }

        public object GetEmailConditionsFor(Guid emailConditionId)
        {
            return Context.GetEmailCondtionsFor(emailConditionId);
        }

        public void SaveEmailConditionsFor(Guid EmailConditionId, List<EmailCondition> conditions)
        {
            DeleteEmailConditionsof(EmailConditionId);
            conditions.ForEach(x => 
            {
                SaveEmailCondition(x);
            });
        }

        private void DeleteEmailConditionsof(Guid emailConditionId)
        {
            Context.DeleteEmailConditionsOf(emailConditionId);
        }

        public string GetEmailConditionCreteriaMatch(Guid emailConditionId)
        {
            return Context.GetEmailConditionCriteriaMatch(emailConditionId);
        }



        public void UpdateEmailCondtionMatchForTemplate(Guid emailConditionId, EmailConditionCriteriaMatch selectedEmailCreterionMatch)
        {
            Context.UpdateEmailConditionMatchForTemplate(emailConditionId, selectedEmailCreterionMatch);
        }
    }
}
