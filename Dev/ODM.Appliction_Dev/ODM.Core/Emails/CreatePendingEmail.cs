﻿using ODM.Core.Helpers;
using ODM.Entities.Emails;
using ODM.Entities.InvoiceTemplate;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace ODM.Core.Emails
{
    public class CreatePendingEmail
    {
        public static void CreatePendingEmailForOrder(Guid orderId)
        {
            var createPendingMail = new CreatePendingEmail();
            var orderDetails = createPendingMail.GetOrderDetailsOfOrder(orderId);
            var customerEmail = (string)orderDetails.Where(x => x.TableName.Equals("CustomerAddress")).FirstOrDefault().Rows[0]["Customer-EmailAddress"] ?? string.Empty;
            if (String.IsNullOrEmpty(customerEmail))
            {
                return;
            }
            var emailTemplates = createPendingMail.GetAllEmailTemplates();
            var successConditionId = createPendingMail.GetCriteriaFullfillingEmailTemplate(orderDetails, createPendingMail.GetAllEmailConditions());
            var successTemplateId = emailTemplates.Where(x => x.EmailTemplateConditionId.Equals(successConditionId)).Select(c => c.EmailTemplateId).FirstOrDefault();

            if (!successTemplateId.Equals(Guid.Empty))
            {
                var parsedEmail = GetParsedEmail(orderDetails, ODM.Core.Emails.EmailTemplates.GetInstance().GetEmailTemplate(successTemplateId));
                var email = new ODM.Entities.Emails.Email();
                email.OrderId = orderId;
                email.MailToAddress = customerEmail;
                email.SentMailBody = parsedEmail.Body;
                email.SentSubject = parsedEmail.Subject;
                email.IsMailSent = false;
                email.EmailTemplateId = successTemplateId;
                ODM.Core.Emails.Mails.GetInstance().AddPendingMail(email);
            }
        }

        private static EmailTemplate GetParsedEmail(List<DataTable> orderDetails, EmailTemplate emailTemplate)
        {
            //var layoutDesign = emailTemplate.Subject;

            Func<string, bool, string> parseNonItemDbField = (layoutDesign, removeHtmlMarkup) =>
            {
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(layoutDesign);
                var htmlNodes = doc.DocumentNode.SelectNodes("//span[@type='dbField']");
                orderDetails.Where(x => x.TableName != "OrderItem").ToList().ForEach(x =>
                {
                    var columnPrefix = string.Empty;

                    foreach (DataColumn column in x.Columns)
                    {
                        var columnName = column.ColumnName;
                        if (x.TableName.Equals("CustomerAddress"))
                        {
                            columnName = columnName.Replace("Customer-", string.Empty);
                        }
                        if (layoutDesign.Contains("@@" + columnName))
                        {
                            doc.LoadHtml(doc.DocumentNode.OuterHtml.Replace("@@" + columnName, x.Rows[0][column.ColumnName].ToString().Trim()));
                        }
                        if (layoutDesign.Contains(columnName) && htmlNodes != null && htmlNodes.Count > 0)
                        {
                            var nodes = htmlNodes.Where(h => h.OuterHtml.Contains(columnName)).ToList();
                            if (nodes.Count > 0)
                            {
                                nodes.ForEach(node =>
                                {
                                    if (node != null && !string.IsNullOrEmpty(node.InnerText) && node.InnerText.Trim().Equals(columnName.Trim()))
                                    {
                                        var dbValue = x.Rows[0][column.ColumnName].ToString().Trim();
                                        if (string.IsNullOrEmpty(dbValue))
                                        {
                                            var emptyNode = HtmlNode.CreateNode("");
                                            node.ParentNode.ReplaceChild(emptyNode, node);
                                        }
                                        else
                                        {
                                            node.SetAttributeValue("style", node.GetAttributeValue("style", string.Empty) + ";background-color: inherit;");
                                            var newOuterHtml = node.OuterHtml.Replace(columnName, string.IsNullOrEmpty(dbValue) ? string.Empty : dbValue);

                                            if (removeHtmlMarkup)
                                            {
                                                newOuterHtml = node.InnerText.Replace(columnName, string.IsNullOrEmpty(dbValue) ? string.Empty : dbValue);
                                            }
                                            var newNode = HtmlNode.CreateNode(newOuterHtml);

                                            node.ParentNode.ReplaceChild(newNode, node);
                                        }
                                    }
                                });
                            }

                        }

                    }
                });

                return doc.DocumentNode.OuterHtml;
            };

            Func<string, string> parseItemDbFields = (layoutDesign) =>
            {
                var doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(layoutDesign);

                var tableRow = doc.DocumentNode.SelectSingleNode("//tr");
                if (tableRow == null)
                {
                    return doc.DocumentNode.OuterHtml;
                }
                foreach (DataRow row in orderDetails.Where(x => x.TableName.Equals("OrderItem")).FirstOrDefault().Rows)
                {
                    var rowStyle = tableRow.GetAttributeValue("style", true);
                    HtmlNode newTableRow = HtmlNode.CreateNode("<tr style='" + rowStyle + "'></tr>");

                    foreach (HtmlNode node in tableRow.SelectNodes("//th"))
                    {
                        var shallContinue = true;
                        var mappedWithField = node.GetAttributeValue("mappedWithField", string.Empty);
                        if (string.IsNullOrEmpty(mappedWithField))
                        {
                            shallContinue = false;
                        }

                        HtmlNode newTd;
                        if (row[mappedWithField] != null && shallContinue)
                        {
                            // var style = mappedWithField.ToLower().Contains("title") || node.InnerText.Trim().ToLower().Contains("sku") ? "text-align:left;background-color: inherit;" : "text-align: center;background-color: inherit;";
                            //style += "font-size:" + nodeStyle.Split(';').Where(x => x.ToLower().Trim().Contains("font-size")).FirstOrDefault().Split(':')[1].ToString() + ";";
                            var dbValue = row[mappedWithField].ToString().Trim();
                            if (mappedWithField.ToLower().Contains("title") || node.InnerText.Trim().ToLower().Contains("sku"))
                            {
                                newTd = HtmlNode.CreateNode("<td style='" + node.GetAttributeValue("style", "") + ";font-weight:normal;" + "'>" + dbValue + "</td>");
                            }
                            else
                            {
                                var tdContent = node.InnerHtml.Replace(node.InnerText.Replace(Environment.NewLine, string.Empty), dbValue);

                                newTd = HtmlNode.CreateNode("<td style='" + node.GetAttributeValue("style", "") + ";font-weight:normal;" + "'>" + tdContent + "</td>");
                            }

                        }
                        else
                        {
                            newTd = HtmlNode.CreateNode("<td> </td>");
                        }
                        newTableRow.AppendChild(newTd);
                    }
                    doc.DocumentNode.SelectSingleNode("//table//tbody").AppendChild(newTableRow);
                }

                return doc.DocumentNode.OuterHtml;
            };

            emailTemplate.Subject = Regex.Replace(parseNonItemDbField.Invoke(emailTemplate.Subject, true), @"<(.|\n)*?>", string.Empty).Replace("&nbsp;", string.Empty);
            emailTemplate.Body = parseNonItemDbField.Invoke(emailTemplate.Body, false);
            emailTemplate.Body = parseItemDbFields.Invoke(emailTemplate.Body);
            return emailTemplate;
        }

        private Guid GetCriteriaFullfillingEmailTemplate(List<DataTable> orderDetails, List<EmailCondition> emailConditions)
        {
            var result = Guid.Empty;
            var conditionGroup = emailConditions.GroupBy(x => x.EmailConditionId).Select(c => new { EmailConditionId = c.Key, EmailConditionList = c.ToList() }).ToList();
            var satisfyingEmailConditionsId = new List<Guid>();

            foreach (var condition in conditionGroup)
            {
                var emailConditionId = condition.EmailConditionId;
                var conditionCriteriaMatch = condition.EmailConditionList.FirstOrDefault().EmailConditionCriteriaMatch;

                if (((EmailConditionCriteriaMatch)conditionCriteriaMatch) == EmailConditionCriteriaMatch.All)
                {
                    bool skipThisEmailCondition = false;
                    foreach (var emailCondition in condition.EmailConditionList)
                    {
                        bool isEmailConditionValid = CheckIfEmailConditionValid(emailCondition, orderDetails.Where(x => x.TableName.Equals(emailCondition.TableName)).FirstOrDefault());
                        if (!isEmailConditionValid)
                        {
                            skipThisEmailCondition = true;
                            break;
                        }
                        if (skipThisEmailCondition)
                        {
                            break;
                        }
                    }
                    if (!skipThisEmailCondition)
                    {
                        satisfyingEmailConditionsId.Add(emailConditionId);
                    }
                }
                else
                {
                    foreach (var emailCondition in condition.EmailConditionList)
                    {
                        bool isEmailConditionValid = CheckIfEmailConditionValid(emailCondition, orderDetails.Where(x => x.TableName.Equals(emailCondition.TableName)).FirstOrDefault());
                        if (isEmailConditionValid)
                        {
                            satisfyingEmailConditionsId.Add(emailConditionId);
                            break;
                        }
                    }
                }
            }
            if (satisfyingEmailConditionsId.Count > 0)
            {
                result = satisfyingEmailConditionsId.FirstOrDefault();
            }
            return result;
        }

        private bool CheckIfEmailConditionValid(EmailCondition emailCondition, DataTable detailTable)
        {
            var tableName = emailCondition.TableName;
            if (tableName.Contains("Customer"))
                emailCondition.ColumnName = "Customer-" + emailCondition.ColumnName;

            if (tableName.Contains("Sender"))
                emailCondition.ColumnName = "Sender-" + emailCondition.ColumnName;

            if (!tableName.Equals("OrderItem"))
            {
                return CompareDataValueWithCreterion(emailCondition, detailTable.Rows[0][emailCondition.ColumnName].ToString());
            }
            else
            {
                var result = false;
                for (int rowIndex = 0; rowIndex < detailTable.Rows.Count; rowIndex++)
                {
                    result = result || CompareDataValueWithCreterion(emailCondition, detailTable.Rows[rowIndex][emailCondition.ColumnName].ToString());
                }
                return result;
            }
        }

        private bool CompareDataValueWithCreterion(EmailCondition emailCondition, string dbValue)
        {
            var result = false;
            var criteria = (PrintConditionCreterion)Enum.Parse(typeof(PrintConditionCreterion), emailCondition.Creterion);
            Type dataType = Conversions.GetPrintConditionCriteriaDataType(emailCondition.CriteriaDataType);
            switch (criteria)
            {
                case PrintConditionCreterion.equalTo:
                    {
                        try
                        {
                            result = Conversions.ParseToCustomDataType(dbValue.ToString(), dataType).Equals(Conversions.ParseToCustomDataType(emailCondition.CompareWith.ToString(), dataType));
                            //result=dbValue.ToString().ToLower().Trim().Equals(printCondition.CompareWith.ToString().ToLower());
                        }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.greaterThan:
                    {
                        try { result = Convert.ToDouble(dbValue) > Convert.ToDouble(emailCondition.CompareWith); }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.lessThan:
                    {
                        try { result = Convert.ToDouble(dbValue) < Convert.ToDouble(emailCondition.CompareWith); }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.like:
                    {
                        try { result = dbValue.ToString().ToLower().Trim().Contains(emailCondition.CompareWith.ToString().ToLower()); }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.In:
                    {
                        try
                        {

                            var valuesToCompareWith = emailCondition.CompareWith.ToString().Split(',').ToList();
                            result = valuesToCompareWith.Any(x => Conversions.ParseToCustomDataType(x, dataType).Equals(Conversions.ParseToCustomDataType(dbValue, dataType)));
                        }
                        catch
                        {

                        }
                        break;
                    }

                case PrintConditionCreterion.NotIn:
                    {
                        try
                        {
                            var valuesToCompareWith = emailCondition.CompareWith.ToString().Split(',').ToList();
                            result = !valuesToCompareWith.Any(x => Conversions.ParseToCustomDataType(x, dataType).Equals(Conversions.ParseToCustomDataType(dbValue, dataType)));
                        }
                        catch { }
                        break;
                    }
            }

            return result;
        }


        private List<EmailCondition> GetAllEmailConditions()
        {
            var conditions = new ODM.Data.DbContext.EmailContext().GetAllEmailConditions();
            return conditions.AsEnumerable().Select(row => new EmailCondition()
            {
                EmailConditionId = row.Field<Guid>("EmailTemplateConditionId"),
                TableName = row.Field<string>("TableName"),
                ColumnName = row.Field<string>("ColumnName"),
                CompareWith = row.Field<string>("CompareWith"),
                Creterion = row.Field<string>("Creterion"),
                CriteriaDataType = row.Field<string>("CriteriaDataType"),
                EmailConditionCriteriaMatch = row.Field<int>("EmailConditionCriteriaMatch")
            }).ToList();
        }

        private List<EmailTemplate> GetAllEmailTemplates()
        {
            var emailTemplates = ODM.Core.Emails.EmailTemplates.GetInstance().GetAllEmailTemplates();
            return emailTemplates.AsEnumerable().Select(x => new EmailTemplate()
            {
                EmailTemplateId = x.Field<Guid>("EmailTemplateId"),
                EmailTemplateName = x.Field<string>("EmailTemplateName"),
                IsHtml = x.Field<bool>("IsEnabled"),
                IsEnabled = x.Field<bool>("IsHtml"),
                WithPdf = x.Field<bool>("WithPdf"),
                WithImage = x.Field<bool>("WithImage"),
                EmailTemplateConditionId = x.Field<Guid>("EmailTemplateConditionId"),
            }).ToList();
        }

        private List<DataTable> GetOrderDetailsOfOrder(Guid orderId)
        {
            var dataset = ((DataSet)new ODM.Data.DbContext.InvoiceTemplateContext().GetDataForInvoice(orderId));
            var orderDetails = new List<DataTable>();
            var orderOfTables = new Dictionary<int, string>();
            orderOfTables.Add(0, "Order");
            orderOfTables.Add(1, "OrderItem");
            orderOfTables.Add(2, "CustomerAddress");
            orderOfTables.Add(3, "SenderAddress");

            for (int i = 0; i < dataset.Tables.Count; i++)
            {
                var table = dataset.Tables[i];
                table.TableName = orderOfTables[i];
                orderDetails.Add(table);
            }
            return orderDetails;
        }
    }
}
