﻿using ODM.Data.DbContext;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Entities.InvoiceTemplate;
using System.Windows.Forms;
using ODM.Core.Helpers;

namespace ODM.Core.InvoiceTemplate
{
    public class TemplateMappedCondition
    {
        public string TemplateName { get; set; }
        public Guid PrintConditionId { get; set; }
        public bool IsAutoPrint { get; set; }
        public bool IsPdfTemplate { get; set; }
        public bool IsConditional { get; set; }
        public bool IsEnabled { get; set; }
        public int PrintPriority { get;set; }
    }
    public class PrintConditions
    {
        private InvoiceTemplateContext Context;

        public PrintConditions()
        {
            Context = new InvoiceTemplateContext();
        }
        public DataTable GetPrintConditinsFor(string printConditionId)
        {
            return Context.GetPrintConditions(Guid.Parse(printConditionId));
        }
        private bool DeletePrintConditionsOf(string printConditionId)
        {
            return Context.DeletePrintConditionsOf(Guid.Parse(printConditionId));
        }
        public void AddEmptyPrintCondition(string printConditionId, PrintCondition printCondition)
        {
            Context.SavePrintConditionsOf(Guid.Parse(printConditionId), printCondition);
        }
        public void SavePrintConditionsFor(string printConditionId, List<PrintCondition> printConditions)
        {
            if (DeletePrintConditionsOf(printConditionId))
            {
                foreach (var printcondition in printConditions)
                {
                    Context.SavePrintConditionsOf(Guid.Parse(printConditionId), printcondition);
                }
            }
        }
        public void UpdatePrintCondtionMatchForTemplate(Guid printConditionId, PrintConditionCretiaMatch selectedPrintCreterionMatchStrat)
        {
            Context.UpdatePrintConditionMatchForTemplate(printConditionId, selectedPrintCreterionMatchStrat);
        }
        public ODM.Entities.InvoiceTemplate.InvoiceTemplate GetCriteriaFulfillingTemplateFor(Guid orderId,bool autoPrint)
        {
            var allInvoiceTemplates = Activator.CreateInstance<ODM.Data.DbContext.InvoiceTemplateContext>()
                .GetAllTemplateNamesWithPrintCondition();

            var templateMappedConditions = allInvoiceTemplates.AsEnumerable()
                .Select(x => new TemplateMappedCondition
                {
                    TemplateName = x.Field<string>("TemplateName"),
                    PrintConditionId = x.Field<Guid>("PrintConditionId"),
                    IsConditional = Convert.ToBoolean(x.Field<int>("IsConditional")),
                    IsEnabled = Convert.ToBoolean(x.Field<int>("IsEnabled")),
                    IsAutoPrint = Convert.ToBoolean(x.Field<int>("IsAutoPrint")),
                    PrintPriority = x.Field<int>("PrintPriority")
                }).ToList();

            var allTemplates= new List<TemplateMappedCondition>();
            allTemplates.AddRange(templateMappedConditions);

            //returning if AutoPrint=1 & IsConditional=0
            if (templateMappedConditions.Any(x => x.IsAutoPrint && x.IsEnabled && !x.IsConditional))
            {
                var priority = templateMappedConditions.Where(x => x.IsAutoPrint && x.IsEnabled && !x.IsConditional).ToList().Min(c => c.PrintPriority);
                return Activator.CreateInstance<ODM.Data.DbContext.InvoiceTemplateContext>().GetInvoiceTemplate(templateMappedConditions.Where(x => x.IsAutoPrint && x.IsEnabled && !x.IsConditional & x.PrintPriority == priority).FirstOrDefault().PrintConditionId)
                    .AsEnumerable().Select(x => new ODM.Entities.InvoiceTemplate.InvoiceTemplate()
                    {
                        LayoutDesign = x.Field<string>("LayoutDesign"),
                        TemplateId = x.Field<Guid>("ID"),
                        PageSize = x.Field<string>("PageSize"),
                        PageOrientation = x.Field<int>("PageOrientation")
                    }).FirstOrDefault();
            }

            var printConditionsList = new List<ODM.Entities.InvoiceTemplate.PrintCondition>();
            var satisfyingConditionId = new List<Guid>();
            Context.GetAllPrintConditions().AsEnumerable().ToList().ForEach(x =>
            {
                printConditionsList.Add(
                    new PrintCondition()
                    {
                        PrintConditionId = x.Field<Guid>("PrintConditionId"),
                        TableName = x.Field<string>("TableName"),
                        ColumnName = x.Field<string>("ColumnName"),
                        Creterion = x.Field<string>("Creterion"),
                        CompareWith = x.Field<string>("CompareWith"),
                        PrintConditionMatch = x.Field<int>("PrintConditionCreteriaMatch").ToString(),
                        CriteriaDataType = x.Field<string>("CriteriaDataType").ToString()
                    });
            });
            var groupedPrintConditions = printConditionsList.Where(c => !string.IsNullOrEmpty(c.TableName)).ToList().GroupBy(x => x.PrintConditionId)
                .Select(s => new { PrintConditionId = s.Key, Conditions = s.ToList() }).ToList();

            var dataset = ((DataSet)new ODM.Data.DbContext.InvoiceTemplateContext().GetDataForInvoice(orderId));
            var orderDetails = new List<DataTable>();
            var orderOfTables = new Dictionary<int, string>();
            orderOfTables.Add(0, "Order");
            orderOfTables.Add(1, "OrderItem");
            orderOfTables.Add(2, "CustomerAddress");
            orderOfTables.Add(3, "SenderAddress");

            for (int i = 0; i < dataset.Tables.Count; i++)
            {
                var table = dataset.Tables[i];
                table.TableName = orderOfTables[i];
                orderDetails.Add(table);
            }

            foreach (var printConditions in groupedPrintConditions)
            {
                var printConditionId = printConditions.PrintConditionId;
                if ((PrintConditionCretiaMatch)Enum.Parse(typeof(PrintConditionCretiaMatch), printConditions.Conditions.FirstOrDefault().PrintConditionMatch) == PrintConditionCretiaMatch.All)
                {
                    bool skipThisPrintCondition = false;
                    foreach (var printCondition in printConditions.Conditions)
                    {
                        bool isPrintConditionValid = CheckIfPrintConditionValid(printCondition, orderDetails.Where(x => x.TableName.Equals(printCondition.TableName)).FirstOrDefault());
                        if (!isPrintConditionValid)
                        {
                            skipThisPrintCondition = true;
                            break;
                        }
                        if (skipThisPrintCondition)
                        {
                            break;
                        }
                    }
                    if (!skipThisPrintCondition)
                    {
                        satisfyingConditionId.Add(printConditionId);
                    }
                }
                else
                {
                    foreach (var printCondition in printConditions.Conditions)
                    {
                        bool isPrintConditionValid = CheckIfPrintConditionValid(printCondition, orderDetails.Where(x => x.TableName.Equals(printCondition.TableName)).FirstOrDefault());
                        if (isPrintConditionValid)
                        {
                            satisfyingConditionId.Add(printConditionId);
                            break;
                        }
                    }
                }
            }
            templateMappedConditions = templateMappedConditions.Where(
                c => c.IsConditional == true & c.IsEnabled == true & satisfyingConditionId.Contains(c.PrintConditionId) 
                ).ToList();

            //returning print invoice if  IsAutoPrint=1 & IsConditional=1
            if (templateMappedConditions.Any(x => x.IsAutoPrint && x.IsEnabled && x.IsConditional))
            {
                var priority = templateMappedConditions.Where(x => x.IsAutoPrint && x.IsEnabled && x.IsConditional).ToList().Min(c => c.PrintPriority);
                return Activator.CreateInstance<ODM.Data.DbContext.InvoiceTemplateContext>().GetInvoiceTemplate(templateMappedConditions.Where(x => x.IsAutoPrint && x.IsEnabled && x.IsConditional && x.PrintPriority == priority).FirstOrDefault().PrintConditionId)
                    .AsEnumerable().Select(x => new ODM.Entities.InvoiceTemplate.InvoiceTemplate()
                    {
                        LayoutDesign = x.Field<string>("LayoutDesign"),
                        TemplateId = x.Field<Guid>("ID"),
                        PageSize = x.Field<string>("PageSize"),
                        PageOrientation = x.Field<int>("PageOrientation")
                    }).FirstOrDefault();
            }

            if (templateMappedConditions.Count == 0)
            {
                if(allTemplates.Any(x=>!x.IsAutoPrint & !x.IsConditional & x.IsEnabled & autoPrint==false))
                {
                    var priority = allTemplates.Where(x => !x.IsAutoPrint & !x.IsConditional & x.IsEnabled & autoPrint == false).ToList().Min(c => c.PrintPriority);
                    return Activator.CreateInstance<ODM.Data.DbContext.InvoiceTemplateContext>().GetInvoiceTemplate(allTemplates.Where(x => !x.IsAutoPrint & !x.IsConditional & x.IsEnabled & autoPrint==false & x.PrintPriority==priority).FirstOrDefault().PrintConditionId)
                        .AsEnumerable().Select(x => new ODM.Entities.InvoiceTemplate.InvoiceTemplate()
                        {
                            LayoutDesign = x.Field<string>("LayoutDesign"),
                            TemplateId = x.Field<Guid>("ID"),
                            PageSize = x.Field<string>("PageSize"),
                            PageOrientation = x.Field<int>("PageOrientation")
                        }).FirstOrDefault();
                }
                return new ODM.Entities.InvoiceTemplate.InvoiceTemplate();
            }

            var selectedTemplateConditionId = templateMappedConditions.FirstOrDefault().PrintConditionId;

            if (templateMappedConditions.Count > 1)
                selectedTemplateConditionId = PromptUserToChooseFromTemplates(templateMappedConditions).PrintConditionId;

            return Activator.CreateInstance<ODM.Data.DbContext.InvoiceTemplateContext>().GetInvoiceTemplate(selectedTemplateConditionId)
                .AsEnumerable().Select(x => new ODM.Entities.InvoiceTemplate.InvoiceTemplate()
                {

                    LayoutDesign = x.Field<string>("LayoutDesign"),
                    TemplateId = x.Field<Guid>("ID"),
                    PageSize = x.Field<string>("PageSize"),
                    PageOrientation = x.Field<int>("PageOrientation")
                }).FirstOrDefault();
        }

        public ODM.Entities.InvoiceTemplate.InvoiceTemplate GetCriteriaFulfillingTemplateFor(Guid orderId)
        {
            var allInvoiceTemplates = Activator.CreateInstance<InvoiceTemplateContext>()
                .GetAllTemplateNamesWithPrintConditionForPdfTemplate();

            var templateMappedConditions = allInvoiceTemplates.AsEnumerable()
                .Select(x => new TemplateMappedCondition
                {
                    TemplateName = x.Field<string>("TemplateName"),
                    PrintConditionId = x.Field<Guid>("PrintConditionId"),
                    IsConditional = Convert.ToBoolean(x.Field<int>("IsConditional")),
                    IsEnabled = Convert.ToBoolean(x.Field<int>("IsEnabled")),
                    IsPdfTemplate = Convert.ToBoolean(x.Field<int>("IsPdfTemplate")),
                    PrintPriority = x.Field<int>("PrintPriority")
                }).ToList();

            var allTemplates = new List<TemplateMappedCondition>();
            allTemplates.AddRange(templateMappedConditions);


            var printConditionsList = new List<PrintCondition>();
            var satisfyingConditionId = new List<Guid>();
            Context.GetAllPrintConditions().AsEnumerable().ToList().ForEach(x =>
            {
                printConditionsList.Add(
                    new PrintCondition()
                    {
                        PrintConditionId = x.Field<Guid>("PrintConditionId"),
                        TableName = x.Field<string>("TableName"),
                        ColumnName = x.Field<string>("ColumnName"),
                        Creterion = x.Field<string>("Creterion"),
                        CompareWith = x.Field<string>("CompareWith"),
                        PrintConditionMatch = x.Field<int>("PrintConditionCreteriaMatch").ToString(),
                        CriteriaDataType = x.Field<string>("CriteriaDataType").ToString()
                    });
            });
            var groupedPrintConditions = printConditionsList.Where(c => !string.IsNullOrEmpty(c.TableName)).ToList().GroupBy(x => x.PrintConditionId)
                .Select(s => new { PrintConditionId = s.Key, Conditions = s.ToList() }).ToList();

            var dataset = ((DataSet)new InvoiceTemplateContext().GetDataForInvoice(orderId));
            var orderDetails = new List<DataTable>();
            var orderOfTables = new Dictionary<int, string>();
            orderOfTables.Add(0, "Order");
            orderOfTables.Add(1, "OrderItem");
            orderOfTables.Add(2, "CustomerAddress");
            orderOfTables.Add(3, "SenderAddress");

            for (int i = 0; i < dataset.Tables.Count; i++)
            {
                var table = dataset.Tables[i];
                table.TableName = orderOfTables[i];
                orderDetails.Add(table);
            }

            foreach (var printConditions in groupedPrintConditions)
            {
                var printConditionId = printConditions.PrintConditionId;
                if ((PrintConditionCretiaMatch)Enum.Parse(typeof(PrintConditionCretiaMatch), printConditions.Conditions.FirstOrDefault().PrintConditionMatch) == PrintConditionCretiaMatch.All)
                {
                    bool skipThisPrintCondition = false;
                    foreach (var printCondition in printConditions.Conditions)
                    {
                        bool isPrintConditionValid = CheckIfPrintConditionValid(printCondition, orderDetails.Where(x => x.TableName.Equals(printCondition.TableName)).FirstOrDefault());
                        if (!isPrintConditionValid)
                        {
                            skipThisPrintCondition = true;
                            break;
                        }
                        if (skipThisPrintCondition)
                        {
                            break;
                        }
                    }
                    if (!skipThisPrintCondition)
                    {
                        satisfyingConditionId.Add(printConditionId);
                    }
                }
                else
                {
                    foreach (var printCondition in printConditions.Conditions)
                    {
                        bool isPrintConditionValid = CheckIfPrintConditionValid(printCondition, orderDetails.Where(x => x.TableName.Equals(printCondition.TableName)).FirstOrDefault());
                        if (isPrintConditionValid)
                        {
                            satisfyingConditionId.Add(printConditionId);
                            break;
                        }
                    }
                }
            }
            templateMappedConditions = templateMappedConditions.Where(
                c => c.IsConditional == true & c.IsEnabled == true & satisfyingConditionId.Contains(c.PrintConditionId)
                ).ToList();

            //returning print invoice if  IsAutoPrint=1 & IsConditional=1
            if (templateMappedConditions.Any(x => x.IsPdfTemplate && x.IsEnabled && x.IsConditional))
            {
                var priority = templateMappedConditions.Where(x => x.IsPdfTemplate && x.IsEnabled && x.IsConditional).ToList().Min(c => c.PrintPriority);
                return Activator.CreateInstance<InvoiceTemplateContext>().GetInvoiceTemplate(templateMappedConditions.Where(x => x.IsPdfTemplate && x.IsEnabled && x.IsConditional && x.PrintPriority == priority).FirstOrDefault().PrintConditionId)
                    .AsEnumerable().Select(x => new ODM.Entities.InvoiceTemplate.InvoiceTemplate()
                    {
                        LayoutDesign = x.Field<string>("LayoutDesign"),
                        TemplateId = x.Field<Guid>("ID"),
                        PageSize = x.Field<string>("PageSize"),
                        PageOrientation = x.Field<int>("PageOrientation")
                    }).FirstOrDefault();
            }

            if (templateMappedConditions.Count == 0)
            {
                if (allTemplates.Any(x => x.IsPdfTemplate & !x.IsConditional & x.IsEnabled))
                {
                    var priority = allTemplates.Where(x => x.IsPdfTemplate & !x.IsConditional & x.IsEnabled).ToList().Min(c => c.PrintPriority);
                    return Activator.CreateInstance<InvoiceTemplateContext>().GetInvoiceTemplate(allTemplates.Where(x => x.IsPdfTemplate & !x.IsConditional & x.IsEnabled & x.PrintPriority == priority).FirstOrDefault().PrintConditionId)
                        .AsEnumerable().Select(x => new ODM.Entities.InvoiceTemplate.InvoiceTemplate()
                        {
                            LayoutDesign = x.Field<string>("LayoutDesign"),
                            TemplateId = x.Field<Guid>("ID"),
                            PageSize = x.Field<string>("PageSize"),
                            PageOrientation = x.Field<int>("PageOrientation")
                        }).FirstOrDefault();
                }
                return new ODM.Entities.InvoiceTemplate.InvoiceTemplate();
            }

            var selectedTemplateConditionId = templateMappedConditions.FirstOrDefault().PrintConditionId;

            return Activator.CreateInstance<InvoiceTemplateContext>().GetInvoiceTemplate(selectedTemplateConditionId)
                .AsEnumerable().Select(x => new ODM.Entities.InvoiceTemplate.InvoiceTemplate()
                {

                    LayoutDesign = x.Field<string>("LayoutDesign"),
                    TemplateId = x.Field<Guid>("ID"),
                    PageSize = x.Field<string>("PageSize"),
                    PageOrientation = x.Field<int>("PageOrientation")
                }).FirstOrDefault();
        }
        private TemplateMappedCondition PromptUserToChooseFromTemplates(List<TemplateMappedCondition> templateMappedConditions)
        {
            var prompt = new Form()
            {
                Text = "Select template",
                Width = 380,
                Height = 170,
                StartPosition = FormStartPosition.CenterScreen,
                ShowIcon = false,
                MaximizeBox = false
            };
            var lblChooseFromTemplate = new Label()
            {
                Text = "Order matches " + templateMappedConditions.Count + " templates, which one would you like to print",
                Width = 300,
                Top = 20,
                Left = 20
            };
            var cmbAvailableTemplates = new ComboBox()
            {
                DataSource = templateMappedConditions.Select(x => x.TemplateName).ToList(),
                Width = 200,
                Height = 20,
                Top = 40,
                Left = 20
            };
            var btnOk = new Button()
            {
                Width = 100,
                Top = 100,
                Left = 250,
                Text = "Ok",
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                FlatStyle = FlatStyle.Flat

            };
            btnOk.Click += (sender, e) =>
            {
                prompt.Close();
            };
            prompt.Controls.Add(cmbAvailableTemplates);
            prompt.Controls.Add(lblChooseFromTemplate);
            prompt.Controls.Add(btnOk);
            prompt.ShowDialog();
            return templateMappedConditions.Where(x => x.TemplateName.Equals(cmbAvailableTemplates.SelectedItem.ToString())).FirstOrDefault();
        }

        private bool CheckIfPrintConditionValid(PrintCondition printCondition, DataTable detailTable)
        {
            var tableName = printCondition.TableName;
            if (tableName.Contains("Customer"))
                printCondition.ColumnName = "Customer-" + printCondition.ColumnName;

            if (tableName.Contains("Sender"))
                printCondition.ColumnName = "Sender-" + printCondition.ColumnName;

            if (!tableName.Equals("OrderItem"))
            {
                return CompareDataValueWithCreterion(printCondition, detailTable.Rows[0][printCondition.ColumnName].ToString());
            }
            else
            {
                var result = false;
                for (int rowIndex = 0; rowIndex < detailTable.Rows.Count; rowIndex++)
                {
                    result = result || CompareDataValueWithCreterion(printCondition, detailTable.Rows[rowIndex][printCondition.ColumnName].ToString());
                }
                return result;
            }
        }

        private bool CompareDataValueWithCreterion(PrintCondition printCondition, string dbValue)
        {
            var result = false;
            var criteria = (PrintConditionCreterion)Enum.Parse(typeof(PrintConditionCreterion), printCondition.Creterion);
            Type dataType = Conversions.GetPrintConditionCriteriaDataType(printCondition.CriteriaDataType);
            switch (criteria)
            {
                case PrintConditionCreterion.equalTo:
                    {
                        try
                        {
                            result = Conversions.ParseToCustomDataType(dbValue.ToString(), dataType).Equals(Conversions.ParseToCustomDataType(printCondition.CompareWith.ToString(), dataType));
                            //result=dbValue.ToString().ToLower().Trim().Equals(printCondition.CompareWith.ToString().ToLower());
                        }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.greaterThan:
                    {
                        try { result = Convert.ToDouble(dbValue) > Convert.ToDouble(printCondition.CompareWith); }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.lessThan:
                    {
                        try { result = Convert.ToDouble(dbValue) < Convert.ToDouble(printCondition.CompareWith); }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.like:
                    {
                        try { result = dbValue.ToString().ToLower().Trim().Contains(printCondition.CompareWith.ToString().ToLower()); }
                        catch { }
                        break;
                    }

                case PrintConditionCreterion.In:
                    {
                        try
                        {

                            var valuesToCompareWith = printCondition.CompareWith.ToString().Split(',').ToList();
                            result = valuesToCompareWith.Any(x => Conversions.ParseToCustomDataType(x, dataType).Equals(Conversions.ParseToCustomDataType(dbValue, dataType)));
                        }
                        catch
                        {

                        }
                        break;
                    }

                case PrintConditionCreterion.NotIn:
                    {
                        try
                        {
                            var valuesToCompareWith = printCondition.CompareWith.ToString().Split(',').ToList();
                            result = !valuesToCompareWith.Any(x => Conversions.ParseToCustomDataType(x, dataType).Equals(Conversions.ParseToCustomDataType(dbValue, dataType)));
                        }
                        catch { }
                        break;
                    }
            }

            return result;
        }




        public string GetPrintConditionCreteriaMatch(Guid printConditionId)
        {
            return Context.GetPrintConditionCreteriaMatch(printConditionId);
        }
    }
}
