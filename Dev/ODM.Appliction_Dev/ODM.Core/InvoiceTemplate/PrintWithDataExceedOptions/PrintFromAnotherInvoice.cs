﻿using HtmlAgilityPack;
using ODM.Core.Helpers;
using ODM.Entities.InvoiceTemplate;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Core.InvoiceTemplate.PrintWithDataExceedOptions
{
    public class PrintFromAnotherInvoice
    {
        public static string GetParsedInvoice(Guid templateId, List<System.Data.DataTable> orderDetails,PageDimension pageDimension)
        {
            var resultSet=Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().GetInvoiceTemplate(templateId);

            var layoutDesign = resultSet.LayoutDesign;
            //replacing non order item values with db one
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(layoutDesign);
            var htmlNodes = doc.DocumentNode.SelectNodes("//span");


            //replacing order item values and add rows corresponding to number of items
            var tableRow = doc.DocumentNode.SelectSingleNode("//table//tr");
            if (tableRow != null)
            {
                var coveredHeight = 0.0;
                var orderDetailsTable = orderDetails.Where(x => x.TableName.Equals("OrderItem")).FirstOrDefault();
                foreach (DataRow row in orderDetailsTable.Rows)
                {
                    var thNode = tableRow.SelectNodes("//th").FirstOrDefault();
                    string thHeightPercentage = (string)GetCssValue(thNode.GetAttributeValue("style", ""), "height").ToString().Replace("px", "");
                    var thHeight = string.IsNullOrEmpty(thHeightPercentage) ? 0 : (int)(Math.Ceiling(Convert.ToDouble(thHeightPercentage)));
                    coveredHeight = coveredHeight + thHeight + 4;
                    var rowStyle = tableRow.GetAttributeValue("style", true);
                    HtmlNode newTableRow = HtmlNode.CreateNode("<tr style='" + rowStyle + "'></tr>");

                    foreach (HtmlNode node in tableRow.SelectNodes("//th"))
                    {
                        var nodeStyle = ReplacePercentWithPx(node.GetAttributeValue("style", ""), pageDimension);
                        node.SetAttributeValue("style", nodeStyle);
                        var mappedWithField = node.GetAttributeValue("field", node.InnerText.Trim());
                        HtmlNode newTd;
                        if (row[mappedWithField] != null)
                        {
                            // var style = mappedWithField.ToLower().Contains("title") || node.InnerText.Trim().ToLower().Contains("sku") ? "text-align:left;background-color: inherit;" : "text-align: center;background-color: inherit;";
                            //style += "font-size:" + nodeStyle.Split(';').Where(x => x.ToLower().Trim().Contains("font-size")).FirstOrDefault().Split(':')[1].ToString() + ";";
                            var dbValue = row[mappedWithField].ToString().Trim();
                            var dataType = node.GetAttributeValue("dataType", "Text");
                            var decimalValue = node.GetAttributeValue("decimalPlaces", "none");
                            dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                            if (dataType.ToLower().Equals("decimal") && !string.IsNullOrEmpty(decimalValue) && !decimalValue.Equals("none"))
                            {
                                dbValue = Math.Round(Convert.ToDouble(dbValue), Convert.ToInt32(decimalValue)).ToString("n" + decimalValue);
                                dbValue = dbValue.Replace(",", "");
                            }
                            if (mappedWithField.ToLower().Contains("title") || node.InnerText.Trim().ToLower().Contains("sku"))
                            {
                                newTd = HtmlNode.CreateNode("<td style='" + node.GetAttributeValue("style", "") + ";font-weight:normal;" + "'>" + dbValue + "</td>");
                            }
                            else
                            {
                                var tdContent = node.InnerHtml.Replace(node.InnerText.Replace(Environment.NewLine, string.Empty), dbValue);

                                newTd = HtmlNode.CreateNode("<td style='" + node.GetAttributeValue("style", "") + ";font-weight:normal;" + "'>" + tdContent + "</td>");
                            }

                        }
                        else
                        {
                            newTd = HtmlNode.CreateNode("<td> </td>");
                        }
                        newTableRow.AppendChild(newTd);
                    }
                    doc.DocumentNode.SelectSingleNode("//table//tbody").AppendChild(newTableRow);
                }
            }

            //  pageDimension.Height = pageDimentionChangeAfterTable.Height;

            orderDetails.Where(x => x.TableName != "OrderItem").ToList().ForEach(x =>
            {
                foreach (DataColumn column in x.Columns)
                {
                    if (layoutDesign.Contains(column.ColumnName) && htmlNodes != null && htmlNodes.Count > 0)
                    {
                        var nodes = htmlNodes.Where(h => h.OuterHtml.Contains("_" + column.ColumnName)).ToList();
                        if (nodes.Count > 0)
                        {
                            nodes.ForEach(node =>
                            {
                                if (node != null)
                                {
                                    var dataType = node.GetAttributeValue("dataType", "Text");
                                    var dbValue = x.Rows[0][column].ToString().Trim();
                                    dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                                    var decimalValue = node.GetAttributeValue("decimalPlaces", "none");
                                    dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                                    if (dataType.ToLower().Equals("decimal") && !string.IsNullOrEmpty(decimalValue) && !decimalValue.Equals("none"))
                                    {
                                        dbValue = Math.Round(Convert.ToDouble(dbValue), Convert.ToInt32(decimalValue)).ToString("n" + decimalValue);
                                        dbValue = dbValue.Replace(",", "");
                                    }
                                    if (string.IsNullOrEmpty(dbValue))
                                    {
                                        var emptyNode = HtmlNode.CreateNode("");
                                        node.ParentNode.ReplaceChild(emptyNode, node);
                                    }
                                    else
                                    {
                                        var newOuterHtml = node.OuterHtml.Replace("_" + column, string.IsNullOrEmpty(dbValue) ? "&nbsp;&nbsp;" : dbValue);
                                        var newNode = HtmlNode.CreateNode(newOuterHtml);
                                        node.ParentNode.ReplaceChild(newNode, node);
                                    }
                                }
                            });
                        }

                    }

                }
            });


            //todo : replacing percentage to pixel if required

            var bodyElements = doc.DocumentNode.SelectNodes("//body").FirstOrDefault().ChildNodes;

            for (int elementCount = 0; elementCount < bodyElements.Count; elementCount++)
            {
                var element = bodyElements[elementCount];
                if (element.OuterHtml.Contains("%"))
                {
                    var style = element.Attributes.Where(x => x.Name.ToLower().Equals("style")).FirstOrDefault();
                    if (style != null)
                    {
                        var newStyle = ReplacePercentWithPx(style.Value.ToString(), pageDimension);
                        var newNode = HtmlNode.CreateNode(element.OuterHtml.Replace(style.Value.ToString(), newStyle));
                        element.ParentNode.ReplaceChild(newNode, element);
                    }
                }
            }

            HtmlNode bodyElement = doc.DocumentNode.SelectSingleNode("//body");
            bodyElement.SetAttributeValue("style", "font-size:9px;");
            return doc.DocumentNode.OuterHtml.ToString();
        }
        private static object GetCssValue(string style, string attribute)
        {
            if (!string.IsNullOrEmpty(style) && style.ToLower().Contains(attribute))
            {
                return style.Split(';').Where(x => x.ToLower().Contains(attribute)).FirstOrDefault().Split(':')[1].Split('%')[0];
            }
            return string.Empty;
        }

        private static string ReplacePercentWithPx(string style, PageDimension pageDimension)
        {
            var styleElements = style.Split(';');
            var stylesToModify = styleElements.Where(x => x.Contains("%")).ToList();
            var attributesToIgnore = new List<string> { "line-height", "font-size" };
            foreach (var modifyStyle in stylesToModify)
            {
                var styleProperties = modifyStyle.Split(':');
                var key = Convert.ToDouble(styleProperties[1].Split('%')[0]);
                double pxConversion = 0.0;
                var attr = styleProperties[0].ToString();

                if (!attributesToIgnore.Contains(attr.ToLower().Trim()))
                {
                    if (attr.ToLower().Contains("height") || attr.ToLower().Contains("top"))
                    {
                        pxConversion = pageDimension.Height * (key / 100);
                    }
                    else
                    {
                        pxConversion = pageDimension.width * (key / 100);
                    }
                    var value = (int)Math.Round(pxConversion) + "px";
                    style = style.Replace(key + "%", value);
                }
            }
            return style;
        }

    }
}
