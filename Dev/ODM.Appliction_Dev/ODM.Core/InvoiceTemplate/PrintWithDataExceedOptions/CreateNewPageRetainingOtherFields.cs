﻿using HtmlAgilityPack;
using ODM.Core.Helpers;
using ODM.Entities.InvoiceTemplate;
using ODM.Entities.InvoiceTemplate.InvoiceTemplateDataContent;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Core.InvoiceTemplate.PrintWithDataExceedOptions
{
    public class CreateNewPageRetainingOtherFields
    {
        public static string GetParsedInvoice(string layoutDesign, List<System.Data.DataTable> orderDetails, Entities.InvoiceTemplate.PageDimension pageDimension)
        {

            //replacing non order item values with db one
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(layoutDesign);
            var htmlNodes = doc.DocumentNode.SelectNodes("//span");

            orderDetails.Where(x => x.TableName != "OrderItem").ToList().ForEach(x =>
            {
                foreach (DataColumn column in x.Columns)
                {
                    if (layoutDesign.Contains(column.ColumnName) && htmlNodes != null && htmlNodes.Count > 0)
                    {
                        var nodes = htmlNodes.Where(h => h.OuterHtml.Contains("_" + column.ColumnName)).ToList();
                        if (nodes.Count > 0)
                        {
                            nodes.ForEach(node =>
                            {
                                if (node != null)
                                {
                                    var dataType = node.GetAttributeValue("dataType", "Text");
                                    var dbValue = x.Rows[0][column].ToString().Trim();
                                    dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                                    var decimalValue = node.GetAttributeValue("decimalPlaces", "none");
                                    dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                                    if (dataType.ToLower().Equals("decimal") && !string.IsNullOrEmpty(decimalValue) && !decimalValue.Equals("none"))
                                    {
                                        dbValue = Math.Round(Convert.ToDouble(dbValue), Convert.ToInt32(decimalValue)).ToString("n" + decimalValue);
                                        dbValue = dbValue.Replace(",", "");
                                    }
                                    if (string.IsNullOrEmpty(dbValue))
                                    {
                                        var emptyNode = HtmlNode.CreateNode("");
                                        node.ParentNode.ReplaceChild(emptyNode, node);
                                    }
                                    else
                                    {
                                        var newOuterHtml = node.OuterHtml.Replace("_" + column, string.IsNullOrEmpty(dbValue) ? "&nbsp;&nbsp;" : dbValue);
                                        var newNode = HtmlNode.CreateNode(newOuterHtml);
                                        node.ParentNode.ReplaceChild(newNode, node);
                                    }
                                }
                            });
                        }

                    }

                }
            });
            //replacing order item values and add rows corresponding to number of items

            PageDimension pageDimentionChangeAfterTable = new PageDimension()
            {
                Height = pageDimension.Height,
                width = pageDimension.width
            };
            var tableTop = 0.0f;
           

            var tableRow = doc.DocumentNode.SelectSingleNode("//table//tr");
            HtmlNode tableRowParentDiv = tableRow.ParentNode.ParentNode.ParentNode;
            var tableTopStr = (string)GetCssValue(tableRowParentDiv.GetAttributeValue("style", ""), "top").ToString().Replace("%", "");
            tableTop = Convert.ToSingle(string.IsNullOrEmpty(tableTopStr) ? "0.0" : tableTopStr);

            string tableMaxHeight = (string)GetCssValue(tableRowParentDiv.GetAttributeValue("style", ""), "height");
            var tableMaxHeightPx = string.IsNullOrEmpty(tableMaxHeight) ? 0 : (int)(Math.Ceiling(Convert.ToDouble(tableMaxHeight) * pageDimension.Height / 100));

            var beforeTableHtml = doc.DocumentNode.OuterHtml.ToString();
            List<HtmlDocument> invoicePages = new List<HtmlDocument>();
            HtmlDocument docToAddTableElement = doc;

            if (tableRow != null)
            {
                var coveredHeight = 20;

                var orderDetailsTable = orderDetails.Where(x => x.TableName.Equals("OrderItem")).FirstOrDefault();

                for (int i = 0; i < pageDimension.Height ; i += 15)
                {
                    doc.DocumentNode.AppendChild(HtmlNode.CreateNode("<div style='display:block;'><div>"));
                }


                foreach (DataRow row in orderDetailsTable.Rows)
                {                
                    var thNode = tableRow.SelectNodes("//th").FirstOrDefault();
                    string thHeightPercentage = (string)GetCssValue(thNode.GetAttributeValue("style", ""), "height").ToString().Replace("px", "");
                    var thHeight = string.IsNullOrEmpty(thHeightPercentage) ? 18 : (int)(Math.Ceiling(Convert.ToDouble(thHeightPercentage)));
                    coveredHeight = coveredHeight + thHeight + 4;
                    var rowStyle = tableRow.GetAttributeValue("style", true);

                    HtmlNode newTableRow = HtmlNode.CreateNode("<tr style='" + rowStyle + "'></tr>");

                    var tableBeginAt = pageDimension.Height * tableTop / 100;
                    if (coveredHeight >= tableMaxHeightPx-(thHeight+8))
                    {
                        for (int i = 0; i < pageDimension.Height; i += 15)
                        {
                            docToAddTableElement.DocumentNode.AppendChild(HtmlNode.CreateNode("<div style='display:block;'><div>"));
                        }
                        invoicePages.Add(docToAddTableElement);
                        HtmlDocument newDocument = new HtmlDocument();
                        newDocument.LoadHtml(beforeTableHtml);
                        docToAddTableElement = newDocument;
                        coveredHeight = thHeight;
                    }

                    foreach (HtmlNode node in tableRow.SelectNodes("//th"))
                    {

                        var nodeStyle = ReplacePercentWithPx(node.GetAttributeValue("style", ""), pageDimension);
                        node.SetAttributeValue("style", nodeStyle);
                        var mappedWithField = node.GetAttributeValue("field", node.InnerText.Trim());
                        HtmlNode newTd;
                        if (row[mappedWithField] != null)
                        {
                            // var style = mappedWithField.ToLower().Contains("title") || node.InnerText.Trim().ToLower().Contains("sku") ? "text-align:left;background-color: inherit;" : "text-align: center;background-color: inherit;";
                            //style += "font-size:" + nodeStyle.Split(';').Where(x => x.ToLower().Trim().Contains("font-size")).FirstOrDefault().Split(':')[1].ToString() + ";";
                            var dbValue = row[mappedWithField].ToString().Trim();
                            var dataType = node.GetAttributeValue("dataType", "Text");
                            var decimalValue = node.GetAttributeValue("decimalPlaces", "none");
                            dbValue = Conversions.ParseToCustomDataType(dbValue, Conversions.GetPrintConditionCriteriaDataType(dataType)).ToString();
                            if (dataType.ToLower().Equals("decimal") && !string.IsNullOrEmpty(decimalValue) && !decimalValue.Equals("none"))
                            {
                                dbValue = Math.Round(Convert.ToDouble(dbValue), Convert.ToInt32(decimalValue)).ToString("n" + decimalValue);
                                dbValue = dbValue.Replace(",", "");
                            }
                            if (mappedWithField.ToLower().Contains("title") || node.InnerText.Trim().ToLower().Contains("sku"))
                            {
                                newTd = HtmlNode.CreateNode("<td style='" + node.GetAttributeValue("style", "") + ";font-weight:normal;" + "'>" + dbValue + "</td>");
                            }
                            else
                            {
                                var tdContent = node.InnerHtml.Replace(node.InnerText.Replace(Environment.NewLine, string.Empty), dbValue);

                                newTd = HtmlNode.CreateNode("<td style='" + node.GetAttributeValue("style", "") + ";font-weight:normal;" + "'>" + tdContent + "</td>");
                            }

                        }
                        else
                        {
                            newTd = HtmlNode.CreateNode("<td> </td>");
                        }
                        newTableRow.AppendChild(newTd);
                    }
                    docToAddTableElement.DocumentNode.SelectSingleNode("//table//tbody").AppendChild(newTableRow);

                }
            }
            HtmlDocument finalDocument = new HtmlDocument();
            finalDocument.LoadHtml(docToAddTableElement.DocumentNode.OuterHtml.ToString());
            invoicePages.Add(finalDocument);
            //  pageDimension.Height = pageDimentionChangeAfterTable.Height;



            //todo : replacing percentage to pixel if required

            //HtmlDocument newDocument = new HtmlDocument();
            //newDocument.LoadHtml("<html><body></body></html>");

            List<HtmlDocument> finalInvoicePages = new List<HtmlDocument>();
            foreach (var page in invoicePages)
            {
                var bodyElements = page.DocumentNode.SelectNodes("//body").FirstOrDefault().ChildNodes;

                for (int elementCount = 0; elementCount < bodyElements.Count; elementCount++)
                {
                    var element = bodyElements[elementCount];
                    if (element.OuterHtml.Contains("%"))
                    {
                        var style = element.Attributes.Where(x => x.Name.ToLower().Equals("style")).FirstOrDefault();
                        if (style != null)
                        {
                            var newStyle = ReplacePercentWithPx(style.Value.ToString(), pageDimension);
                            var newNode = HtmlNode.CreateNode(element.OuterHtml.Replace(style.Value.ToString(), newStyle));
                            element.ParentNode.ReplaceChild(newNode, element);
                        }
                    }
                }

                HtmlNode bodyElement = page.DocumentNode.SelectSingleNode("//body");
                bodyElement.SetAttributeValue("style", "font-size:9px;page-break-after:always");

                finalInvoicePages.Add(page);
            }

            var resultPage = string.Empty;
            finalInvoicePages.ForEach(x => 
            {
                resultPage += x.DocumentNode.OuterHtml.ToString();
            });

            return resultPage;
        }

        private static object GetCssValue(string style, string attribute)
        {
            try
            {


                if (!string.IsNullOrEmpty(style) && style.ToLower().Contains(attribute))
                {
                    var topList = style.Split(';').Where(x => x.ToLower().Contains(attribute)).ToList();
                    if (topList.Count > 0)
                    {
                        topList = topList.Where(c => c.ToLower().Split(':')[0].Trim().Equals(attribute)).ToList();
                        if (topList.Count > 0)
                        {
                            return topList.FirstOrDefault().Split(':')[1].Split('%')[0];
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        private static string ReplacePercentWithPx(string style, PageDimension pageDimension)
        {
            var pageDimenstionToCompareWith = pageDimension;

            var styleElements = style.Split(';');
            var stylesToModify = styleElements.Where(x => x.Contains("%")).ToList();
            var attributesToIgnore = new List<string> { "line-height", "font-size" };


            foreach (var modifyStyle in stylesToModify)
            {
                var styleProperties = modifyStyle.Split(':');
                var key = Convert.ToDouble(styleProperties[1].Split('%')[0]);
                double pxConversion = 0.0;
                var attr = styleProperties[0].ToString();

                if (!attributesToIgnore.Contains(attr.ToLower().Trim()))
                {
                    if (attr.ToLower().Contains("height") || attr.ToLower().Contains("top"))
                    {
                        pxConversion = pageDimenstionToCompareWith.Height * (key / 100);
                    }

                    else
                    {
                        pxConversion = pageDimension.width * (key / 100);
                    }
                    var value = (int)Math.Round(pxConversion) + "px";
                    style = style.Replace(key + "%", value);
                }
            }
            return style;
        }
    }
}
