﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.DbContext;
using System.Data;

namespace ODM.Core.Orders
{
    public class OrderDetails
    {
        private readonly OrderDetailsContext Context;

        public OrderDetails() 
        {
            Context = new OrderDetailsContext();
        }

        public DataTable GetAllOpenOrders(string searchParam)
        {
            return Context.GetAllOpenOrders(searchParam);
        }
        public DataTable GetOrderItems(Guid orderId)
        {
            return Context.GetOrderItems(orderId);
        }

        public string GetOrderImage(Guid orderId)
        {
            return Context.GetOrderImage(orderId);
        }
    }
}
