﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Core.Helpers
{
    public class TimerUtils
    {
        public static double GetMinutes(double seconds)
        {
            var result = 0.0;
            var time = TimeSpan.FromSeconds(seconds);
            var minutes = time.Minutes;
            var sec = (seconds - Convert.ToDouble(minutes * 59));
            result = minutes + sec / 100;
            return result;
        }
    }
}
