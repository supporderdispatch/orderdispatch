﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Data.Entity;
using System.Data.SqlClient;
using System.Data;
using ODM.Entities;
using ODM.Data.DbContext;

namespace ODM.Core
{
    public class Bins
    {
        BinContext context;
        private static int? intLastCountOfBins = 0;

        public Bins()
        {
            context = new BinContext();
        }

        //
        // Summary:
        //     Get number of maximum bins allowed by administrator
        //
        // Return:
        //     int : number of maximum bins
        //
        public int GetNumberOfBins()
        {
            int intCountOfBin=Convert.ToInt32(context.GetMaxNumOfAllowedBins().Rows[0]["MaxBinAllowed"]);
            intLastCountOfBins = intCountOfBin;
            return intCountOfBin;
        }
        //
        // Summary:
        //    Update the number of bins in database , only administrator user can update 
        //
        // Parameters:
        //     numberOfBins:integer value fill in textbox at general setting screen
        //
        // Return:
        //     true if successfully update without any exception or error
        //
        public bool UpdateMaxNumberOfBins(int numberOfBins)
        {
            if (numberOfBins != intLastCountOfBins)
            {
                return context.UpdateMaxNumberOfBins(numberOfBins);
            }
            return false;            
        }
        //
        // Summary:
        //     Add item having barcode into its order Bin
        //
        // Parameter:
        //     ordernumber and Barcode
        //       
        // Return:
        //     anonymous object contain status showing whether item added or not with updated data
        //
        public object AddItemToBin(string orderNumber, string barcode,int itemQuantity)
        {
            return context.AddItemToBin(orderNumber,barcode,itemQuantity);
        }
        //
        // Summary:
        //     get all bins with their details.
        // 
        // Return:
        //     list of bins.
        public List<Bin> GetBins()
        {
            var bins = new List<Bin>();
            try
            {
                DataTable _bins = context.GetBins();
                foreach (DataRow row in _bins.Rows)
                {
                    Bin bin;
                    bin.BinNumber=Convert.ToInt32(row["BinNumber"].ToString()??"0");
                    bin.NumberOfItemsAlreadyAddedInBin = Convert.ToInt32(row["ConsolidatedNumberOfItems"].ToString() ?? "0");
                    bin.OrderNumber = Convert.ToInt32(row["NumOrderId"].ToString() ?? "0");
                    bin.TotalNumberofItems = Convert.ToInt32(row["TotalItems"].ToString() ?? "0");
                    bins.Add(bin);
                }
                return bins;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //
        // Summary:
        //     Return individual bin details
        //
        // Paramters:
        //     ordernumber of perticual order
        //
        // Return:
        //     anonymous object containing current bin details and items
        //
        public object GetBinDetails(int orderNumber)
        {
            try
            {
                var items = context.GetBinDetails(orderNumber);
                var row = items.Rows[0];
                Bin bin;
                bin.BinNumber = Convert.ToInt32(row["BinNumber"].ToString() ?? "0");
                bin.NumberOfItemsAlreadyAddedInBin = Convert.ToInt32(row["ConsolidatedNumberOfItems"].ToString() ?? "0");
                bin.OrderNumber = Convert.ToInt32(row["NumOrderId"].ToString() ?? "0");
                bin.TotalNumberofItems = Convert.ToInt32(row["TotalItems"].ToString() ?? "0");
                // As we need detail of bin, we are removing column we don't required anymore because the same as the belong to same order
                items.Columns.Remove("BinID");
                items.Columns.Remove("BinNumber");
                items.Columns.Remove("ConsolidatedNumberOfItems");
                items.Columns.Remove("NumOrderId");
                items.Columns.Remove("TotalItems");
                items.Columns.Remove("ConsolidatedItems");

                for (int currentRow = 0; currentRow < items.Rows.Count; currentRow++)
                {
                    //remove empty rows
                    if (String.IsNullOrEmpty(items.Rows[currentRow].ItemArray[1].ToString()))
                    {
                        items.Rows.RemoveAt(currentRow);
                    }
                }
                return new {Bin=bin,DataTable=items };
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        //
        // Summary:
        //     add item into bin having no barcode
        // Parameter:
        //     ordernumber :to which bin assigned
        //     numOfItemsHavingNoBarcode: quantity of items to add
        //
        // Return:
        //     true if suceed else false
        //
        public bool AddItemToBinHavingNoBarcode(int orderNumber, int numOfItemsHavingNoBarcode)
        {
            return context.AddItemToBinHavingNoBarcode(orderNumber, numOfItemsHavingNoBarcode);
        }
        public bool RemoveOrderFromBin(int orderNumber)
        {
            return context.RemoveOrderFromBin(orderNumber);
        }

        public bool AddIndividualItemToBin(string sku, int orderNumber, string barcode)
        {
            return context.AddIndividualItemToBin(sku,orderNumber,barcode);
        }
        public Bin GetIndividualBin(int orderNumber)
        {
            try
            {
                var items = context.GetIndividualBin(orderNumber);
                var row = items.Rows[0];
                Bin bin;
                bin.BinNumber = Convert.ToInt32(row["BinNumber"].ToString() ?? "0");
                bin.NumberOfItemsAlreadyAddedInBin = Convert.ToInt32(row["ConsolidatedNumberOfItems"].ToString() ?? "0");
                bin.OrderNumber = Convert.ToInt32(row["NumOrderId"].ToString() ?? "0");
                bin.TotalNumberofItems = Convert.ToInt32(row["TotalItems"].ToString() ?? "0");
                items = null;
                return bin;
            }
            catch (Exception ex)
            {
                throw ex;
            }        
        }
        public void RemoveBinsOfProcessedOrders()
        {
            new Task(() =>
            {
                context.RemoveBinsOfProcessedOrders();
            }).Start();
        }

        public bool CheckBarcodeIsPresentInAnyBin(string barcode)
        {
            var data = context.GetBinsHavingThisBarcode(barcode);
            if (data.Rows.Count > 0)
                return true;
            else
                return false;
        }
    }
}
