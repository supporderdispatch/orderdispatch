﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ODM.Core
{
    public class AppUpdator
    {
        private readonly ODM.Data.DbContext.AppUpdatorContext Context;

        public AppUpdator()
        {
            Context = new Data.DbContext.AppUpdatorContext();
        }
        public static AppUpdator GetInstance()
        {
            return new AppUpdator();
        }

        public ODM.Entities.AppVersionDetails GetLatestAppVersionDetails()
        {
            return Context.GetLatestAppVersion().AsEnumerable()
                .Select(x => new ODM.Entities.AppVersionDetails()
                {
                    AppVersion=x.Field<string>("AppVersion"),
                    Approved=x.Field<bool>("Approved"),
                    CreatedOn=x.Field<DateTime>("CreatedOn"),
                    DownloadUrl=x.Field<string>("DownloadUrl"),
                    Remark=x.Field<string>("Remark")

                }).FirstOrDefault();
        }

        public void ApproveAppVersion(Entities.AppVersionDetails appVersion)
        {
            Context.ApproveAppVersion(appVersion);
        }

        public void RejectAppVersion(Entities.AppVersionDetails appVersion)
        {
            Context.RejectAppVersion(appVersion);
        }

        public Entities.AppVersionDetails GetLatestAppVersionToBeApprove()
        {
            var latestAppversionToBeUpdate = Context.GetLatestAppVersionToBeApprove();
            if (latestAppversionToBeUpdate.Rows.Count > 0)
            {
                return latestAppversionToBeUpdate.AsEnumerable()
                .Select(x => new Entities.AppVersionDetails()
                {
                    AppVersion = x.Field<string>("AppVersion"),
                    DownloadUrl = x.Field<string>("DownloadUrl"),
                    Remark = x.Field<string>("Remark")

                }).FirstOrDefault();
            }
            return new Entities.AppVersionDetails();                
        }

        public void AddAppVersion(Entities.AppVersionDetails appVersion)
        {
            Context.AddAppVersion(appVersion);
        }
    }
}
