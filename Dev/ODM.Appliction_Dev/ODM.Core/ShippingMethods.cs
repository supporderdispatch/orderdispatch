﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Entities;
using System.Data;
using ODM.Data.DbContext;
using ODM.Core.UserTracking;

namespace ODM.Core
{
    public class ShippingMethods
    {
        ShippingMethodsContext context;

        public ShippingMethods()
        {
            context = new ShippingMethodsContext();
        }

        public List<ShippingMethodsGrpByVendor> GetShippingMethodsGroupByVendor()
        {
           var data=context.GetShippingMethodAndData();
           var result = from d in data.AsEnumerable()
                        group d by d.Field<string>("Vendor") into g
                        select new ShippingMethodsGrpByVendor
                        {
                            Vendor = g.Key,
                            ShippingMethods = g.ToList().Select(x => x.Field<string>("PostalServiceName")).ToList()
                        };
                       
           return result.ToList();
        }

        public void ChangeShippingMethodOfOrderTo(List<string> orderIds,string shippingService)
        {
            foreach (var orderId in orderIds)
            {
                OrderTracker.BeginTrackerForOrder(orderId);
                context.ChangeShippingMethodOfOrderTo(orderId, shippingService);
                OrderTracker.EndTracking();
            }
        }
    }
}
