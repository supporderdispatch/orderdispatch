﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using ODM.Data.DbContext;
using System.Collections.Generic;
using System.Linq;
using ODM.Data.Util;
using ODM.Entities;

namespace ODM.Core
{
    public class PickList
    {
        private PickListContext Context;

        public PickList()
        {
            Context = new PickListContext();
        }

        public void CreateAndPrintPickListForBatch(string batchName, string commaSepratedselectedOrderIds)
        {
            var pickListItemsData = Context
                .CreateAndGetPickList(batchName, commaSepratedselectedOrderIds);

            if (pickListItemsData.Rows.Count > 0)
            {
                Activator.CreateInstance<ODM.Data.Printing.PrintPickList>()
                    .printPickList(batchName, CreatePickListPage(pickListItemsData, batchName));

            }
        }

        public string CreatePickListPage(DataTable pickListItems,string batchName)
        {
            var distinctSkuCount = pickListItems.AsEnumerable()
                .Select(x => x.Field<string>("SKU").Trim())
                .ToList().Distinct().ToList().Count;

            string HeadComponent = @"
         <html>
         <head><title>Pick List Items</title>
         <style>
         table{
            font-size: 12px;
            border-collapse: collapse;
         max-width: 2480px;
         width:100%;
         text-align: left;
            border: 1px solid black;
         }
         th{
            background-color: whitesmoke;
         }

         th,td{
         padding:5px;
         border:1px solid black;
         width: auto;
         overflow: hidden;
         word-wrap: break-word;
         }
		.picklist-info{
		margin: 2px; 
        font-size: 12px;
		}
         </style>
         </head>
        <body>
";
            HeadComponent += "<p class='picklist-info'> Batch Name   :   " + batchName + "</p>";
            HeadComponent += "<p class='picklist-info'> Printed On   :   " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss") +" by "+GlobalVariablesAndMethods.UserDetails.Firstname+" "+GlobalVariablesAndMethods.UserDetails.Lastname +" - " + distinctSkuCount + " SKU's to pick"+ "</p><br/>";
            string TableComponent = @"<table><tr>";

            foreach(DataColumn column in pickListItems.Columns)
            {
                TableComponent += @"<th>" + column.ColumnName.ToString() + @"</th>";
            }
            TableComponent += @"</tr>";
            foreach (DataRow row in pickListItems.Rows)
            {
                TableComponent += "@<tr>";
                int columnCount = 1;
                foreach (var item in row.ItemArray)
                {
                    var field=object.ReferenceEquals(item,null)?string.Empty:item.ToString().Trim();
                    if (columnCount == 2)
                    {
                        try
                        {
                            if (Convert.ToInt32(field.ToString()) > 1)
                            {
                                TableComponent += "@<td style='background-color:gray'>" + field + @"</td>";
                            }
                            else
                            {
                                TableComponent += "@<td>" + field + @"</td>";
                            }
                        }
                        catch
                        {
                            TableComponent += "@<td>" + field + @"</td>";
                        }
                        }
                    else
                    {
                        TableComponent += "@<td>" + field + @"</td>";
                    }
                    columnCount++;
                }
                TableComponent += "@</tr>";
            }
            TableComponent += @"</table>";

            string PageEndComponent = @"</body></html>";

            return HeadComponent + TableComponent + PageEndComponent;
        }

        public List<PickListBatch> GetPickListBatches()
        {
            var result=Context
                .GetPickingBatches();

            return result.AsEnumerable()
                .Select(x => new PickListBatch()
                {
                    BatchName=x.Field<string>("PickingBatches"),
                    OrdersRemaining=x.Field<int>("OrdersRemaining")
                }).ToList();
        }
    }
}
