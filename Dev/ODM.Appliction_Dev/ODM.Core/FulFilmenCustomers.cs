﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODM.Entities;
using System.Data;

namespace ODM.Core
{
    public class FulFilmenCustomers
    {
        private readonly ODM.Data.DbContext.FulfilmentCustomerContext _Context;

        public FulFilmenCustomers()
        {
            _Context = new Data.DbContext.FulfilmentCustomerContext();
        }

        public static FulFilmenCustomers GetInstance()
        {
            return new FulFilmenCustomers();
        }

        public object GetCustomers()
        {
            return _Context.GetCustomers();
        }

        public FulFilmentCustomer GetCustomer(Guid customerId)
        {
            return _Context.GetCustomer(customerId).AsEnumerable()
                .Select(x => new FulFilmentCustomer() 
                {
                    CustomerId=customerId,
                    ConditionId=x.Field<Guid>("ConditionId"),
                    Name=x.Field<string>("CustomerName"),
                    Company=x.Field<string>("Company"),
                    UserName=x.Field<string>("Username"),
                    Password=x.Field<string>("Password")
                }).FirstOrDefault();
        }

        public void SaveCustomer(FulFilmentCustomer customer)
        {
            _Context.SaveCustomer(customer);
        }

        public void SaveCondition(List<ConditionCriteria> conditions,Guid conditionId)
        {
            _Context.DeleteCustomerCondition(conditionId);
            conditions.ForEach(condition =>
            {
                _Context.SaveCondition(condition);
            });
        }

        public void DeleteCustomer(string customerId)
        {
            _Context.DeleteCustomer(Guid.Parse(customerId));
        }

        public DataTable GetCustomerConditions(Guid conditionId)
        {
            return _Context.GetCustomerConditions(conditionId);
        }
    }
}
