﻿namespace ODM.Appliction.Helpers
{
    partial class PrintPickList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintPickList));
            this.label1 = new System.Windows.Forms.Label();
            this.txtBatchName = new System.Windows.Forms.TextBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.PicLoading = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTotalNumberOfItems = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTotalSkuCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Batch Name";
            // 
            // txtBatchName
            // 
            this.txtBatchName.Location = new System.Drawing.Point(119, 33);
            this.txtBatchName.Name = "txtBatchName";
            this.txtBatchName.Size = new System.Drawing.Size(202, 20);
            this.txtBatchName.TabIndex = 1;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(246, 115);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // PicLoading
            // 
            this.PicLoading.Image = ((System.Drawing.Image)(resources.GetObject("PicLoading.Image")));
            this.PicLoading.Location = new System.Drawing.Point(226, 117);
            this.PicLoading.Name = "PicLoading";
            this.PicLoading.Size = new System.Drawing.Size(20, 20);
            this.PicLoading.TabIndex = 30;
            this.PicLoading.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Total Items";
            // 
            // lblTotalNumberOfItems
            // 
            this.lblTotalNumberOfItems.AutoSize = true;
            this.lblTotalNumberOfItems.Location = new System.Drawing.Point(116, 67);
            this.lblTotalNumberOfItems.Name = "lblTotalNumberOfItems";
            this.lblTotalNumberOfItems.Size = new System.Drawing.Size(0, 13);
            this.lblTotalNumberOfItems.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Total SKU\'s";
            // 
            // lblTotalSkuCount
            // 
            this.lblTotalSkuCount.AutoSize = true;
            this.lblTotalSkuCount.Location = new System.Drawing.Point(116, 92);
            this.lblTotalSkuCount.Name = "lblTotalSkuCount";
            this.lblTotalSkuCount.Size = new System.Drawing.Size(0, 13);
            this.lblTotalSkuCount.TabIndex = 34;
            // 
            // PrintPickList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 149);
            this.Controls.Add(this.lblTotalSkuCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblTotalNumberOfItems);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PicLoading);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.txtBatchName);
            this.Controls.Add(this.label1);
            this.Name = "PrintPickList";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Print Pick List";
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBatchName;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.PictureBox PicLoading;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTotalNumberOfItems;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTotalSkuCount;
    }
}