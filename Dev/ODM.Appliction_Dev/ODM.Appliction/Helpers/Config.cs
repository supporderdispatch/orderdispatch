﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace ODM.Appliction.Helpers
{
    public class Config
    {
        public static void SaveConfig(string userName, string password, bool saveLogin)
        {
            var path = Path.GetTempPath();
            var configName = "od-dev-config.xml";

            var writer = new XmlTextWriter(Path.Combine(path, configName), System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("UserCofigurations");
            writeNodeWithValue(writer, "Username", userName);
            writeNodeWithValue(writer, "Password", password);
            writeNodeWithValue(writer, "SaveLogin", saveLogin.ToString());
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
        }

        public static string GetNodeValue(string nodeName)
        {
            var result = string.Empty;
            var configPath =  Path.Combine(Path.GetTempPath(), "od-dev-config.xml");
            if (File.Exists(configPath))
            {
                var document = XDocument.Load(configPath);
                var storedValues = document.Descendants("UserCofigurations").FirstOrDefault();
                if (Convert.ToBoolean(storedValues.Element("SaveLogin").Value))
                {
                    result = storedValues.Element(nodeName).Value;
                }
            }
            return result;
        }

        private static void writeNodeWithValue(XmlTextWriter writer, string node, string value)
        {
            writer.WriteStartElement(node);
            writer.WriteString(value);
            writer.WriteEndElement();
        }
    }
}
