﻿namespace ODM.Appliction
{
    partial class InvoiceTemplateEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InvoiceTemplateEditor));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkAutoPrint = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chkPdfTemplate = new System.Windows.Forms.CheckBox();
            this.chkConditional = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.chkTemplateEnabled = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTemplateName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.GridLinesSqureSize = new System.Windows.Forms.TrackBar();
            this.chkGridLines = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkOrderItem = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chkCustomerDetail = new System.Windows.Forms.CheckBox();
            this.chkSenderDetail = new System.Windows.Forms.CheckBox();
            this.chkOrderDetails = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnExitWithoutSave = new System.Windows.Forms.Button();
            this.btnSaveAndExit = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.linkDataPrintSettings = new System.Windows.Forms.LinkLabel();
            this.btnPrintCondition = new System.Windows.Forms.Button();
            this.cmbInvoiceTemplatePrinter = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.linkEditCustomPageSize = new System.Windows.Forms.LinkLabel();
            this.cmbPageOrientations = new System.Windows.Forms.ComboBox();
            this.cmbPageSizes = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlInvoiceTemplate = new System.Windows.Forms.Panel();
            this.wbTemplateEditor = new System.Windows.Forms.WebBrowser();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbViewportView = new System.Windows.Forms.ComboBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.lblTemplateSavedToolbar = new System.Windows.Forms.ToolStripLabel();
            this.btnSaveTemplate = new System.Windows.Forms.ToolStripButton();
            this.btnCut = new System.Windows.Forms.ToolStripButton();
            this.btnCopy = new System.Windows.Forms.ToolStripButton();
            this.btnPaste = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.fontComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.fontSizeComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.btnBold = new System.Windows.Forms.ToolStripButton();
            this.btnItalic = new System.Windows.Forms.ToolStripButton();
            this.btnUnderLine = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnJustifyLeft = new System.Windows.Forms.ToolStripButton();
            this.btnJustifyCenter = new System.Windows.Forms.ToolStripButton();
            this.btnJustifyFull = new System.Windows.Forms.ToolStripButton();
            this.btnJustifyRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAddImage = new System.Windows.Forms.ToolStripButton();
            this.btnIndent = new System.Windows.Forms.ToolStripButton();
            this.btnOutdent = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnLineHeight = new System.Windows.Forms.ToolStripButton();
            this.cmbLineHeight = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.btnTextColor = new System.Windows.Forms.ToolStripButton();
            this.btnFillColor = new System.Windows.Forms.ToolStripButton();
            this.cmbBlockType = new System.Windows.Forms.ComboBox();
            this.lblTemplateSaved = new System.Windows.Forms.Label();
            this.wbDataBlockEditor = new System.Windows.Forms.WebBrowser();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.pnlDataBlockEditorControl = new System.Windows.Forms.Panel();
            this.linkToggleDataBlockEdit = new System.Windows.Forms.LinkLabel();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grdInvoiceFields = new System.Windows.Forms.DataGridView();
            this.btnCancelUpdateBlock = new System.Windows.Forms.Button();
            this.btnUpdateBlock = new System.Windows.Forms.Button();
            this.cmbKeyValueSeprator = new System.Windows.Forms.ComboBox();
            this.btnAddBlockToTemplate = new System.Windows.Forms.Button();
            this.cmbDataBlockBorder = new System.Windows.Forms.ComboBox();
            this.btnKeyValueParing = new System.Windows.Forms.Button();
            this.btnAddBlock = new System.Windows.Forms.Button();
            this.btnAutoInsertLineBreak = new System.Windows.Forms.Button();
            this.btnResetBlock = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridLinesSqureSize)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pnlInvoiceTemplate.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.pnlDataBlockEditorControl.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdInvoiceFields)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.groupBox1.Controls.Add(this.chkAutoPrint);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.chkPdfTemplate);
            this.groupBox1.Controls.Add(this.chkConditional);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.chkTemplateEnabled);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtTemplateName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(7, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(223, 124);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Template";
            // 
            // chkAutoPrint
            // 
            this.chkAutoPrint.AutoSize = true;
            this.chkAutoPrint.Checked = true;
            this.chkAutoPrint.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoPrint.Location = new System.Drawing.Point(62, 91);
            this.chkAutoPrint.Name = "chkAutoPrint";
            this.chkAutoPrint.Size = new System.Drawing.Size(15, 14);
            this.chkAutoPrint.TabIndex = 9;
            this.chkAutoPrint.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(2, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 15);
            this.label13.TabIndex = 8;
            this.label13.Text = "Auto Print";
            // 
            // chkPdfTemplate
            // 
            this.chkPdfTemplate.AutoSize = true;
            this.chkPdfTemplate.Checked = true;
            this.chkPdfTemplate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPdfTemplate.Location = new System.Drawing.Point(190, 91);
            this.chkPdfTemplate.Name = "chkPdfTemplate";
            this.chkPdfTemplate.Size = new System.Drawing.Size(15, 14);
            this.chkPdfTemplate.TabIndex = 7;
            this.chkPdfTemplate.UseVisualStyleBackColor = true;
            // 
            // chkConditional
            // 
            this.chkConditional.AutoSize = true;
            this.chkConditional.Checked = true;
            this.chkConditional.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConditional.Location = new System.Drawing.Point(190, 56);
            this.chkConditional.Name = "chkConditional";
            this.chkConditional.Size = new System.Drawing.Size(15, 14);
            this.chkConditional.TabIndex = 6;
            this.chkConditional.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(95, 90);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 15);
            this.label10.TabIndex = 5;
            this.label10.Text = "PDF Template";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(112, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 15);
            this.label9.TabIndex = 4;
            this.label9.Text = "Conditional";
            // 
            // chkTemplateEnabled
            // 
            this.chkTemplateEnabled.AutoSize = true;
            this.chkTemplateEnabled.Checked = true;
            this.chkTemplateEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTemplateEnabled.Location = new System.Drawing.Point(62, 55);
            this.chkTemplateEnabled.Name = "chkTemplateEnabled";
            this.chkTemplateEnabled.Size = new System.Drawing.Size(15, 14);
            this.chkTemplateEnabled.TabIndex = 3;
            this.chkTemplateEnabled.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Enabled";
            // 
            // txtTemplateName
            // 
            this.txtTemplateName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTemplateName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTemplateName.Location = new System.Drawing.Point(59, 27);
            this.txtTemplateName.Name = "txtTemplateName";
            this.txtTemplateName.Size = new System.Drawing.Size(143, 20);
            this.txtTemplateName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.btnExitWithoutSave);
            this.panel1.Controls.Add(this.btnSaveAndExit);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1108, 131);
            this.panel1.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.GridLinesSqureSize);
            this.groupBox5.Controls.Add(this.chkGridLines);
            this.groupBox5.Location = new System.Drawing.Point(634, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(176, 124);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "View";
            // 
            // GridLinesSqureSize
            // 
            this.GridLinesSqureSize.Location = new System.Drawing.Point(82, 21);
            this.GridLinesSqureSize.Maximum = 6;
            this.GridLinesSqureSize.Minimum = 1;
            this.GridLinesSqureSize.Name = "GridLinesSqureSize";
            this.GridLinesSqureSize.Size = new System.Drawing.Size(88, 45);
            this.GridLinesSqureSize.TabIndex = 8;
            this.GridLinesSqureSize.Value = 3;
            // 
            // chkGridLines
            // 
            this.chkGridLines.AutoSize = true;
            this.chkGridLines.Location = new System.Drawing.Point(4, 26);
            this.chkGridLines.Name = "chkGridLines";
            this.chkGridLines.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkGridLines.Size = new System.Drawing.Size(79, 19);
            this.chkGridLines.TabIndex = 7;
            this.chkGridLines.Text = "GridLines";
            this.chkGridLines.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkOrderItem);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.chkCustomerDetail);
            this.groupBox4.Controls.Add(this.chkSenderDetail);
            this.groupBox4.Controls.Add(this.chkOrderDetails);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(813, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(154, 124);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Data Settings";
            // 
            // chkOrderItem
            // 
            this.chkOrderItem.AutoSize = true;
            this.chkOrderItem.Location = new System.Drawing.Point(121, 52);
            this.chkOrderItem.Name = "chkOrderItem";
            this.chkOrderItem.Size = new System.Drawing.Size(15, 14);
            this.chkOrderItem.TabIndex = 8;
            this.chkOrderItem.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 7;
            this.label11.Text = "Order Item";
            // 
            // chkCustomerDetail
            // 
            this.chkCustomerDetail.AutoSize = true;
            this.chkCustomerDetail.Checked = true;
            this.chkCustomerDetail.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCustomerDetail.Location = new System.Drawing.Point(121, 101);
            this.chkCustomerDetail.Name = "chkCustomerDetail";
            this.chkCustomerDetail.Size = new System.Drawing.Size(15, 14);
            this.chkCustomerDetail.TabIndex = 6;
            this.chkCustomerDetail.UseVisualStyleBackColor = true;
            // 
            // chkSenderDetail
            // 
            this.chkSenderDetail.AutoSize = true;
            this.chkSenderDetail.Location = new System.Drawing.Point(121, 77);
            this.chkSenderDetail.Name = "chkSenderDetail";
            this.chkSenderDetail.Size = new System.Drawing.Size(15, 14);
            this.chkSenderDetail.TabIndex = 5;
            this.chkSenderDetail.UseVisualStyleBackColor = true;
            // 
            // chkOrderDetails
            // 
            this.chkOrderDetails.AutoSize = true;
            this.chkOrderDetails.Location = new System.Drawing.Point(121, 28);
            this.chkOrderDetails.Name = "chkOrderDetails";
            this.chkOrderDetails.Size = new System.Drawing.Size(15, 14);
            this.chkOrderDetails.TabIndex = 4;
            this.chkOrderDetails.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 15);
            this.label8.TabIndex = 2;
            this.label8.Text = "Customer Details";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Sender Details";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Order details";
            // 
            // btnExitWithoutSave
            // 
            this.btnExitWithoutSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExitWithoutSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExitWithoutSave.Location = new System.Drawing.Point(972, 45);
            this.btnExitWithoutSave.Name = "btnExitWithoutSave";
            this.btnExitWithoutSave.Size = new System.Drawing.Size(128, 27);
            this.btnExitWithoutSave.TabIndex = 7;
            this.btnExitWithoutSave.Text = "Exit Without Saving";
            this.btnExitWithoutSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExitWithoutSave.UseVisualStyleBackColor = true;
            this.btnExitWithoutSave.Click += new System.EventHandler(this.btnExitWithoutSave_Click);
            // 
            // btnSaveAndExit
            // 
            this.btnSaveAndExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAndExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveAndExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveAndExit.Location = new System.Drawing.Point(972, 16);
            this.btnSaveAndExit.Name = "btnSaveAndExit";
            this.btnSaveAndExit.Size = new System.Drawing.Size(128, 25);
            this.btnSaveAndExit.TabIndex = 6;
            this.btnSaveAndExit.Text = "Save and Exit";
            this.btnSaveAndExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveAndExit.UseVisualStyleBackColor = true;
            this.btnSaveAndExit.Click += new System.EventHandler(this.btnSaveAndExit_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.linkDataPrintSettings);
            this.groupBox3.Controls.Add(this.btnPrintCondition);
            this.groupBox3.Controls.Add(this.cmbInvoiceTemplatePrinter);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(430, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(201, 124);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Print Settings";
            // 
            // linkDataPrintSettings
            // 
            this.linkDataPrintSettings.AutoSize = true;
            this.linkDataPrintSettings.Location = new System.Drawing.Point(49, 103);
            this.linkDataPrintSettings.Name = "linkDataPrintSettings";
            this.linkDataPrintSettings.Size = new System.Drawing.Size(108, 15);
            this.linkDataPrintSettings.TabIndex = 7;
            this.linkDataPrintSettings.TabStop = true;
            this.linkDataPrintSettings.Text = "Data Print Settings";
            this.linkDataPrintSettings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkDataPrintSettings_LinkClicked);
            // 
            // btnPrintCondition
            // 
            this.btnPrintCondition.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintCondition.Location = new System.Drawing.Point(52, 54);
            this.btnPrintCondition.Name = "btnPrintCondition";
            this.btnPrintCondition.Size = new System.Drawing.Size(122, 31);
            this.btnPrintCondition.TabIndex = 5;
            this.btnPrintCondition.Text = "Printing Condition";
            this.btnPrintCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrintCondition.UseVisualStyleBackColor = true;
            this.btnPrintCondition.Click += new System.EventHandler(this.btnPrintCondition_Click);
            // 
            // cmbInvoiceTemplatePrinter
            // 
            this.cmbInvoiceTemplatePrinter.FormattingEnabled = true;
            this.cmbInvoiceTemplatePrinter.Location = new System.Drawing.Point(52, 21);
            this.cmbInvoiceTemplatePrinter.Name = "cmbInvoiceTemplatePrinter";
            this.cmbInvoiceTemplatePrinter.Size = new System.Drawing.Size(141, 23);
            this.cmbInvoiceTemplatePrinter.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Printer";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.linkEditCustomPageSize);
            this.groupBox2.Controls.Add(this.cmbPageOrientations);
            this.groupBox2.Controls.Add(this.cmbPageSizes);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(233, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(194, 124);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Page Setting";
            // 
            // linkEditCustomPageSize
            // 
            this.linkEditCustomPageSize.AutoSize = true;
            this.linkEditCustomPageSize.Location = new System.Drawing.Point(44, 100);
            this.linkEditCustomPageSize.Name = "linkEditCustomPageSize";
            this.linkEditCustomPageSize.Size = new System.Drawing.Size(136, 15);
            this.linkEditCustomPageSize.TabIndex = 4;
            this.linkEditCustomPageSize.TabStop = true;
            this.linkEditCustomPageSize.Text = "Edit custom Page Sizes";
            this.linkEditCustomPageSize.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkEditCustomPageSize_LinkClicked);
            // 
            // cmbPageOrientations
            // 
            this.cmbPageOrientations.FormattingEnabled = true;
            this.cmbPageOrientations.Location = new System.Drawing.Point(75, 51);
            this.cmbPageOrientations.Name = "cmbPageOrientations";
            this.cmbPageOrientations.Size = new System.Drawing.Size(105, 23);
            this.cmbPageOrientations.TabIndex = 3;
            // 
            // cmbPageSizes
            // 
            this.cmbPageSizes.FormattingEnabled = true;
            this.cmbPageSizes.Location = new System.Drawing.Point(75, 22);
            this.cmbPageSizes.Name = "cmbPageSizes";
            this.cmbPageSizes.Size = new System.Drawing.Size(105, 23);
            this.cmbPageSizes.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Orientation";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Page Size";
            // 
            // pnlInvoiceTemplate
            // 
            this.pnlInvoiceTemplate.AutoScroll = true;
            this.pnlInvoiceTemplate.BackColor = System.Drawing.SystemColors.Control;
            this.pnlInvoiceTemplate.Controls.Add(this.wbTemplateEditor);
            this.pnlInvoiceTemplate.Location = new System.Drawing.Point(194, 157);
            this.pnlInvoiceTemplate.Name = "pnlInvoiceTemplate";
            this.pnlInvoiceTemplate.Size = new System.Drawing.Size(571, 457);
            this.pnlInvoiceTemplate.TabIndex = 2;
            // 
            // wbTemplateEditor
            // 
            this.wbTemplateEditor.Location = new System.Drawing.Point(3, 3);
            this.wbTemplateEditor.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbTemplateEditor.Name = "wbTemplateEditor";
            this.wbTemplateEditor.ScriptErrorsSuppressed = true;
            this.wbTemplateEditor.ScrollBarsEnabled = false;
            this.wbTemplateEditor.Size = new System.Drawing.Size(592, 432);
            this.wbTemplateEditor.TabIndex = 0;
            this.wbTemplateEditor.Url = new System.Uri("", System.UriKind.Relative);
            this.wbTemplateEditor.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.wbTemplateEditor_DocumentCompleted);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 654);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Viewport";
            // 
            // cmbViewportView
            // 
            this.cmbViewportView.FormattingEnabled = true;
            this.cmbViewportView.Location = new System.Drawing.Point(66, 651);
            this.cmbViewportView.Name = "cmbViewportView";
            this.cmbViewportView.Size = new System.Drawing.Size(122, 21);
            this.cmbViewportView.TabIndex = 5;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblTemplateSavedToolbar,
            this.btnSaveTemplate,
            this.btnCut,
            this.btnCopy,
            this.btnPaste,
            this.toolStripBtnRefresh,
            this.toolStripSeparator1,
            this.fontComboBox,
            this.fontSizeComboBox,
            this.btnBold,
            this.btnItalic,
            this.btnUnderLine,
            this.toolStripSeparator2,
            this.btnJustifyLeft,
            this.btnJustifyCenter,
            this.btnJustifyFull,
            this.btnJustifyRight,
            this.toolStripSeparator3,
            this.btnAddImage,
            this.btnIndent,
            this.btnOutdent,
            this.toolStripSeparator4,
            this.btnLineHeight,
            this.cmbLineHeight,
            this.toolStripLabel1,
            this.btnTextColor,
            this.btnFillColor});
            this.toolStrip1.Location = new System.Drawing.Point(0, 132);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1108, 29);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // lblTemplateSavedToolbar
            // 
            this.lblTemplateSavedToolbar.AutoSize = false;
            this.lblTemplateSavedToolbar.Name = "lblTemplateSavedToolbar";
            this.lblTemplateSavedToolbar.Size = new System.Drawing.Size(82, 26);
            this.lblTemplateSavedToolbar.Text = "                         ";
            // 
            // btnSaveTemplate
            // 
            this.btnSaveTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveTemplate.Image")));
            this.btnSaveTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveTemplate.Name = "btnSaveTemplate";
            this.btnSaveTemplate.Size = new System.Drawing.Size(23, 26);
            this.btnSaveTemplate.Text = "Save";
            this.btnSaveTemplate.Click += new System.EventHandler(this.btnSaveTemplate_Click);
            // 
            // btnCut
            // 
            this.btnCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCut.Image = ((System.Drawing.Image)(resources.GetObject("btnCut.Image")));
            this.btnCut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCut.Name = "btnCut";
            this.btnCut.Size = new System.Drawing.Size(23, 26);
            this.btnCut.Text = "C&ut";
            this.btnCut.Click += new System.EventHandler(this.btnCut_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnCopy.Image")));
            this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(23, 26);
            this.btnCopy.Text = "&Copy";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnPaste
            // 
            this.btnPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPaste.Image = ((System.Drawing.Image)(resources.GetObject("btnPaste.Image")));
            this.btnPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPaste.Name = "btnPaste";
            this.btnPaste.Size = new System.Drawing.Size(23, 26);
            this.btnPaste.Text = "&Paste";
            this.btnPaste.Click += new System.EventHandler(this.btnPaste_Click);
            // 
            // toolStripBtnRefresh
            // 
            this.toolStripBtnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBtnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnRefresh.Image")));
            this.toolStripBtnRefresh.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripBtnRefresh.Name = "toolStripBtnRefresh";
            this.toolStripBtnRefresh.Size = new System.Drawing.Size(23, 26);
            this.toolStripBtnRefresh.Text = "toolStripButton1";
            this.toolStripBtnRefresh.Click += new System.EventHandler(this.toolStripBtnRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 29);
            // 
            // fontComboBox
            // 
            this.fontComboBox.AutoSize = false;
            this.fontComboBox.Name = "fontComboBox";
            this.fontComboBox.Size = new System.Drawing.Size(200, 23);
            // 
            // fontSizeComboBox
            // 
            this.fontSizeComboBox.Name = "fontSizeComboBox";
            this.fontSizeComboBox.Size = new System.Drawing.Size(75, 29);
            // 
            // btnBold
            // 
            this.btnBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBold.Image = ((System.Drawing.Image)(resources.GetObject("btnBold.Image")));
            this.btnBold.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBold.Name = "btnBold";
            this.btnBold.Size = new System.Drawing.Size(23, 26);
            this.btnBold.Text = "toolStripButton1";
            this.btnBold.Click += new System.EventHandler(this.btnBold_Click);
            // 
            // btnItalic
            // 
            this.btnItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnItalic.Image = ((System.Drawing.Image)(resources.GetObject("btnItalic.Image")));
            this.btnItalic.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnItalic.Name = "btnItalic";
            this.btnItalic.Size = new System.Drawing.Size(23, 26);
            this.btnItalic.Text = "toolStripButton2";
            this.btnItalic.Click += new System.EventHandler(this.btnItalic_Click);
            // 
            // btnUnderLine
            // 
            this.btnUnderLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUnderLine.Image = ((System.Drawing.Image)(resources.GetObject("btnUnderLine.Image")));
            this.btnUnderLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUnderLine.Name = "btnUnderLine";
            this.btnUnderLine.Size = new System.Drawing.Size(23, 26);
            this.btnUnderLine.Text = "toolStripButton3";
            this.btnUnderLine.Click += new System.EventHandler(this.btnUnderLine_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 29);
            // 
            // btnJustifyLeft
            // 
            this.btnJustifyLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnJustifyLeft.Image = ((System.Drawing.Image)(resources.GetObject("btnJustifyLeft.Image")));
            this.btnJustifyLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJustifyLeft.Name = "btnJustifyLeft";
            this.btnJustifyLeft.Size = new System.Drawing.Size(23, 26);
            this.btnJustifyLeft.Text = "Justify Left";
            this.btnJustifyLeft.Click += new System.EventHandler(this.btnJustifyLeft_Click);
            // 
            // btnJustifyCenter
            // 
            this.btnJustifyCenter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnJustifyCenter.Image = ((System.Drawing.Image)(resources.GetObject("btnJustifyCenter.Image")));
            this.btnJustifyCenter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJustifyCenter.Name = "btnJustifyCenter";
            this.btnJustifyCenter.Size = new System.Drawing.Size(23, 26);
            this.btnJustifyCenter.Text = "toolStripButton5";
            this.btnJustifyCenter.Click += new System.EventHandler(this.btnJustifyCenter_Click);
            // 
            // btnJustifyFull
            // 
            this.btnJustifyFull.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnJustifyFull.Image = ((System.Drawing.Image)(resources.GetObject("btnJustifyFull.Image")));
            this.btnJustifyFull.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJustifyFull.Name = "btnJustifyFull";
            this.btnJustifyFull.Size = new System.Drawing.Size(23, 26);
            this.btnJustifyFull.Text = "toolStripButton6";
            this.btnJustifyFull.Click += new System.EventHandler(this.btnJustifyFull_Click);
            // 
            // btnJustifyRight
            // 
            this.btnJustifyRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnJustifyRight.Image = ((System.Drawing.Image)(resources.GetObject("btnJustifyRight.Image")));
            this.btnJustifyRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJustifyRight.Name = "btnJustifyRight";
            this.btnJustifyRight.Size = new System.Drawing.Size(23, 26);
            this.btnJustifyRight.Text = "toolStripButton7";
            this.btnJustifyRight.Click += new System.EventHandler(this.btnJustifyRight_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 29);
            // 
            // btnAddImage
            // 
            this.btnAddImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddImage.Image = ((System.Drawing.Image)(resources.GetObject("btnAddImage.Image")));
            this.btnAddImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddImage.Name = "btnAddImage";
            this.btnAddImage.Size = new System.Drawing.Size(23, 26);
            this.btnAddImage.Text = "insert image";
            this.btnAddImage.Click += new System.EventHandler(this.btnAddImage_Click);
            // 
            // btnIndent
            // 
            this.btnIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnIndent.Image = ((System.Drawing.Image)(resources.GetObject("btnIndent.Image")));
            this.btnIndent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnIndent.Name = "btnIndent";
            this.btnIndent.Size = new System.Drawing.Size(23, 26);
            this.btnIndent.Text = "toolStripButton8";
            this.btnIndent.Click += new System.EventHandler(this.btnIndent_Click);
            // 
            // btnOutdent
            // 
            this.btnOutdent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnOutdent.Image = ((System.Drawing.Image)(resources.GetObject("btnOutdent.Image")));
            this.btnOutdent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOutdent.Name = "btnOutdent";
            this.btnOutdent.Size = new System.Drawing.Size(23, 26);
            this.btnOutdent.Text = "toolStripButton9";
            this.btnOutdent.Click += new System.EventHandler(this.btnOutdent_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 29);
            // 
            // btnLineHeight
            // 
            this.btnLineHeight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnLineHeight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLineHeight.Image = ((System.Drawing.Image)(resources.GetObject("btnLineHeight.Image")));
            this.btnLineHeight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLineHeight.Name = "btnLineHeight";
            this.btnLineHeight.Size = new System.Drawing.Size(23, 26);
            this.btnLineHeight.Text = "Line Height";
            this.btnLineHeight.Click += new System.EventHandler(this.btnLineHeight_Click);
            // 
            // cmbLineHeight
            // 
            this.cmbLineHeight.AutoSize = false;
            this.cmbLineHeight.DropDownHeight = 50;
            this.cmbLineHeight.DropDownWidth = 20;
            this.cmbLineHeight.IntegralHeight = false;
            this.cmbLineHeight.Name = "cmbLineHeight";
            this.cmbLineHeight.Size = new System.Drawing.Size(40, 23);
            this.cmbLineHeight.Visible = false;
            this.cmbLineHeight.Click += new System.EventHandler(this.cmbLineHeight_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(76, 26);
            this.toolStripLabel1.Text = "                       ";
            // 
            // btnTextColor
            // 
            this.btnTextColor.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnTextColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTextColor.Image = ((System.Drawing.Image)(resources.GetObject("btnTextColor.Image")));
            this.btnTextColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTextColor.Name = "btnTextColor";
            this.btnTextColor.Size = new System.Drawing.Size(23, 26);
            this.btnTextColor.Text = "Text Color";
            this.btnTextColor.Click += new System.EventHandler(this.btnTextColor_Click);
            // 
            // btnFillColor
            // 
            this.btnFillColor.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnFillColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnFillColor.Enabled = false;
            this.btnFillColor.Image = ((System.Drawing.Image)(resources.GetObject("btnFillColor.Image")));
            this.btnFillColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFillColor.Name = "btnFillColor";
            this.btnFillColor.Size = new System.Drawing.Size(23, 26);
            this.btnFillColor.Text = "Fill Color";
            this.btnFillColor.Click += new System.EventHandler(this.btnFillColor_Click);
            // 
            // cmbBlockType
            // 
            this.cmbBlockType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBlockType.FormattingEnabled = true;
            this.cmbBlockType.Location = new System.Drawing.Point(162, 3);
            this.cmbBlockType.Name = "cmbBlockType";
            this.cmbBlockType.Size = new System.Drawing.Size(155, 21);
            this.cmbBlockType.TabIndex = 17;
            this.cmbBlockType.Visible = false;
            this.cmbBlockType.SelectedIndexChanged += new System.EventHandler(this.cmbBlockType_SelectedIndexChanged);
            // 
            // lblTemplateSaved
            // 
            this.lblTemplateSaved.AutoSize = true;
            this.lblTemplateSaved.Location = new System.Drawing.Point(51, 627);
            this.lblTemplateSaved.Name = "lblTemplateSaved";
            this.lblTemplateSaved.Size = new System.Drawing.Size(0, 13);
            this.lblTemplateSaved.TabIndex = 20;
            // 
            // wbDataBlockEditor
            // 
            this.wbDataBlockEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.wbDataBlockEditor.Location = new System.Drawing.Point(12, 35);
            this.wbDataBlockEditor.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbDataBlockEditor.Name = "wbDataBlockEditor";
            this.wbDataBlockEditor.ScriptErrorsSuppressed = true;
            this.wbDataBlockEditor.Size = new System.Drawing.Size(308, 192);
            this.wbDataBlockEditor.TabIndex = 10;
            // 
            // pnlDataBlockEditorControl
            // 
            this.pnlDataBlockEditorControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlDataBlockEditorControl.Controls.Add(this.linkToggleDataBlockEdit);
            this.pnlDataBlockEditorControl.Controls.Add(this.cmbBlockType);
            this.pnlDataBlockEditorControl.Controls.Add(this.wbDataBlockEditor);
            this.pnlDataBlockEditorControl.Location = new System.Drawing.Point(784, 173);
            this.pnlDataBlockEditorControl.Name = "pnlDataBlockEditorControl";
            this.pnlDataBlockEditorControl.Size = new System.Drawing.Size(324, 235);
            this.pnlDataBlockEditorControl.TabIndex = 21;
            // 
            // linkToggleDataBlockEdit
            // 
            this.linkToggleDataBlockEdit.AutoSize = true;
            this.linkToggleDataBlockEdit.Location = new System.Drawing.Point(19, 11);
            this.linkToggleDataBlockEdit.Name = "linkToggleDataBlockEdit";
            this.linkToggleDataBlockEdit.Size = new System.Drawing.Size(115, 13);
            this.linkToggleDataBlockEdit.TabIndex = 18;
            this.linkToggleDataBlockEdit.TabStop = true;
            this.linkToggleDataBlockEdit.Text = "Enable Data Bock Edit";
            this.linkToggleDataBlockEdit.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkToggleDataBlockEdit_LinkClicked);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.grdInvoiceFields);
            this.panel2.Controls.Add(this.btnCancelUpdateBlock);
            this.panel2.Controls.Add(this.btnUpdateBlock);
            this.panel2.Controls.Add(this.cmbKeyValueSeprator);
            this.panel2.Controls.Add(this.btnAddBlockToTemplate);
            this.panel2.Controls.Add(this.cmbDataBlockBorder);
            this.panel2.Controls.Add(this.btnKeyValueParing);
            this.panel2.Controls.Add(this.btnAddBlock);
            this.panel2.Controls.Add(this.btnAutoInsertLineBreak);
            this.panel2.Controls.Add(this.btnResetBlock);
            this.panel2.Location = new System.Drawing.Point(784, 408);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(324, 258);
            this.panel2.TabIndex = 22;
            // 
            // grdInvoiceFields
            // 
            this.grdInvoiceFields.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grdInvoiceFields.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.grdInvoiceFields.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdInvoiceFields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdInvoiceFields.Enabled = false;
            this.grdInvoiceFields.Location = new System.Drawing.Point(6, 123);
            this.grdInvoiceFields.Name = "grdInvoiceFields";
            this.grdInvoiceFields.Size = new System.Drawing.Size(308, 127);
            this.grdInvoiceFields.TabIndex = 3;
            // 
            // btnCancelUpdateBlock
            // 
            this.btnCancelUpdateBlock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelUpdateBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelUpdateBlock.Location = new System.Drawing.Point(168, 86);
            this.btnCancelUpdateBlock.Name = "btnCancelUpdateBlock";
            this.btnCancelUpdateBlock.Size = new System.Drawing.Size(141, 31);
            this.btnCancelUpdateBlock.TabIndex = 19;
            this.btnCancelUpdateBlock.Text = "Cancel";
            this.btnCancelUpdateBlock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelUpdateBlock.UseVisualStyleBackColor = true;
            this.btnCancelUpdateBlock.Visible = false;
            this.btnCancelUpdateBlock.Click += new System.EventHandler(this.btnCancelUpdateBlock_Click);
            // 
            // btnUpdateBlock
            // 
            this.btnUpdateBlock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateBlock.Location = new System.Drawing.Point(8, 86);
            this.btnUpdateBlock.Name = "btnUpdateBlock";
            this.btnUpdateBlock.Size = new System.Drawing.Size(155, 31);
            this.btnUpdateBlock.TabIndex = 18;
            this.btnUpdateBlock.Text = "+ Update Block";
            this.btnUpdateBlock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdateBlock.UseVisualStyleBackColor = true;
            this.btnUpdateBlock.Visible = false;
            this.btnUpdateBlock.Click += new System.EventHandler(this.btnUpdateBlock_Click);
            // 
            // cmbKeyValueSeprator
            // 
            this.cmbKeyValueSeprator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbKeyValueSeprator.Enabled = false;
            this.cmbKeyValueSeprator.FormattingEnabled = true;
            this.cmbKeyValueSeprator.Location = new System.Drawing.Point(168, 4);
            this.cmbKeyValueSeprator.Name = "cmbKeyValueSeprator";
            this.cmbKeyValueSeprator.Size = new System.Drawing.Size(141, 21);
            this.cmbKeyValueSeprator.TabIndex = 16;
            this.cmbKeyValueSeprator.SelectedIndexChanged += new System.EventHandler(this.cmbKeyValueSeprator_SelectedIndexChanged);
            // 
            // btnAddBlockToTemplate
            // 
            this.btnAddBlockToTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddBlockToTemplate.Enabled = false;
            this.btnAddBlockToTemplate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddBlockToTemplate.Location = new System.Drawing.Point(169, 86);
            this.btnAddBlockToTemplate.Name = "btnAddBlockToTemplate";
            this.btnAddBlockToTemplate.Size = new System.Drawing.Size(141, 31);
            this.btnAddBlockToTemplate.TabIndex = 12;
            this.btnAddBlockToTemplate.Text = "Add Block To Template";
            this.btnAddBlockToTemplate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddBlockToTemplate.UseVisualStyleBackColor = true;
            this.btnAddBlockToTemplate.Click += new System.EventHandler(this.btnAddBlockToTemplate_Click);
            // 
            // cmbDataBlockBorder
            // 
            this.cmbDataBlockBorder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDataBlockBorder.Enabled = false;
            this.cmbDataBlockBorder.FormattingEnabled = true;
            this.cmbDataBlockBorder.Location = new System.Drawing.Point(8, 6);
            this.cmbDataBlockBorder.Name = "cmbDataBlockBorder";
            this.cmbDataBlockBorder.Size = new System.Drawing.Size(155, 21);
            this.cmbDataBlockBorder.TabIndex = 11;
            this.cmbDataBlockBorder.SelectedIndexChanged += new System.EventHandler(this.cmbDataBlockBorder_SelectedIndexChanged);
            // 
            // btnKeyValueParing
            // 
            this.btnKeyValueParing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKeyValueParing.Enabled = false;
            this.btnKeyValueParing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKeyValueParing.Location = new System.Drawing.Point(168, 31);
            this.btnKeyValueParing.Name = "btnKeyValueParing";
            this.btnKeyValueParing.Size = new System.Drawing.Size(141, 24);
            this.btnKeyValueParing.TabIndex = 15;
            this.btnKeyValueParing.Text = "KeyValue Paring";
            this.btnKeyValueParing.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKeyValueParing.UseVisualStyleBackColor = true;
            this.btnKeyValueParing.Click += new System.EventHandler(this.btnKeyValueParing_Click);
            // 
            // btnAddBlock
            // 
            this.btnAddBlock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddBlock.Location = new System.Drawing.Point(8, 86);
            this.btnAddBlock.Name = "btnAddBlock";
            this.btnAddBlock.Size = new System.Drawing.Size(155, 31);
            this.btnAddBlock.TabIndex = 9;
            this.btnAddBlock.Text = "+ Create Block";
            this.btnAddBlock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddBlock.UseVisualStyleBackColor = true;
            this.btnAddBlock.Click += new System.EventHandler(this.btnAddBlock_Click);
            // 
            // btnAutoInsertLineBreak
            // 
            this.btnAutoInsertLineBreak.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAutoInsertLineBreak.Enabled = false;
            this.btnAutoInsertLineBreak.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAutoInsertLineBreak.Location = new System.Drawing.Point(7, 32);
            this.btnAutoInsertLineBreak.Name = "btnAutoInsertLineBreak";
            this.btnAutoInsertLineBreak.Size = new System.Drawing.Size(155, 24);
            this.btnAutoInsertLineBreak.TabIndex = 13;
            this.btnAutoInsertLineBreak.Text = "Auto Insert Line Break";
            this.btnAutoInsertLineBreak.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAutoInsertLineBreak.UseVisualStyleBackColor = true;
            this.btnAutoInsertLineBreak.Click += new System.EventHandler(this.btnAutoInsertLineBreak_Click);
            // 
            // btnResetBlock
            // 
            this.btnResetBlock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetBlock.Enabled = false;
            this.btnResetBlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResetBlock.Location = new System.Drawing.Point(7, 60);
            this.btnResetBlock.Name = "btnResetBlock";
            this.btnResetBlock.Size = new System.Drawing.Size(155, 24);
            this.btnResetBlock.TabIndex = 14;
            this.btnResetBlock.Text = "Reset Block";
            this.btnResetBlock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnResetBlock.UseVisualStyleBackColor = true;
            this.btnResetBlock.Click += new System.EventHandler(this.btnResetBlock_Click);
            // 
            // InvoiceTemplateEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1110, 679);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlDataBlockEditorControl);
            this.Controls.Add(this.lblTemplateSaved);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.cmbViewportView);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pnlInvoiceTemplate);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InvoiceTemplateEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InvoiceTemplateEditor";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridLinesSqureSize)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnlInvoiceTemplate.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.pnlDataBlockEditorControl.ResumeLayout(false);
            this.pnlDataBlockEditorControl.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdInvoiceFields)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkTemplateEnabled;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTemplateName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbPageOrientations;
        private System.Windows.Forms.ComboBox cmbPageSizes;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnPrintCondition;
        private System.Windows.Forms.Button btnExitWithoutSave;
        private System.Windows.Forms.Button btnSaveAndExit;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel pnlInvoiceTemplate;
        private System.Windows.Forms.WebBrowser wbTemplateEditor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkCustomerDetail;
        private System.Windows.Forms.CheckBox chkSenderDetail;
        private System.Windows.Forms.CheckBox chkOrderDetails;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkPdfTemplate;
        private System.Windows.Forms.CheckBox chkConditional;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkOrderItem;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbViewportView;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnCut;
        private System.Windows.Forms.ToolStripButton btnCopy;
        private System.Windows.Forms.ToolStripButton btnPaste;
        private System.Windows.Forms.ToolStripButton btnBold;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripComboBox fontComboBox;
        private System.Windows.Forms.ToolStripComboBox fontSizeComboBox;
        private System.Windows.Forms.ToolStripButton btnItalic;
        private System.Windows.Forms.ToolStripButton btnUnderLine;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnJustifyLeft;
        private System.Windows.Forms.ToolStripButton btnJustifyCenter;
        private System.Windows.Forms.ToolStripButton btnJustifyFull;
        private System.Windows.Forms.ToolStripButton btnJustifyRight;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnIndent;
        private System.Windows.Forms.ToolStripButton btnOutdent;
        private System.Windows.Forms.ComboBox cmbBlockType;
        private System.Windows.Forms.ToolStripButton btnSaveTemplate;
        private System.Windows.Forms.ComboBox cmbInvoiceTemplatePrinter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripLabel lblTemplateSavedToolbar;
        private System.Windows.Forms.Label lblTemplateSaved;
        private System.Windows.Forms.WebBrowser wbDataBlockEditor;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Panel pnlDataBlockEditorControl;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TrackBar GridLinesSqureSize;
        private System.Windows.Forms.CheckBox chkGridLines;
        private System.Windows.Forms.LinkLabel linkEditCustomPageSize;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton btnTextColor;
        private System.Windows.Forms.ToolStripButton btnFillColor;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView grdInvoiceFields;
        private System.Windows.Forms.Button btnCancelUpdateBlock;
        private System.Windows.Forms.Button btnUpdateBlock;
        private System.Windows.Forms.ComboBox cmbKeyValueSeprator;
        private System.Windows.Forms.Button btnAddBlockToTemplate;
        private System.Windows.Forms.ComboBox cmbDataBlockBorder;
        private System.Windows.Forms.Button btnKeyValueParing;
        private System.Windows.Forms.Button btnAddBlock;
        private System.Windows.Forms.Button btnAutoInsertLineBreak;
        private System.Windows.Forms.Button btnResetBlock;
        private System.Windows.Forms.ToolStripButton btnLineHeight;
        private System.Windows.Forms.ToolStripComboBox cmbLineHeight;
        private System.Windows.Forms.CheckBox chkAutoPrint;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ToolStripButton btnAddImage;
        private System.Windows.Forms.LinkLabel linkToggleDataBlockEdit;
        private System.Windows.Forms.ToolStripButton toolStripBtnRefresh;
        private System.Windows.Forms.LinkLabel linkDataPrintSettings;
    }
}