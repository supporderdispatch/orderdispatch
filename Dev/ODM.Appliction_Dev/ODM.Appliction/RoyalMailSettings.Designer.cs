﻿namespace ODM.Appliction
{
    partial class RoyalMailSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoyalMailSettings));
            this.btnManifest = new System.Windows.Forms.Button();
            this.lblPasswordRequired = new System.Windows.Forms.Label();
            this.lblUserNameRequired = new System.Windows.Forms.Label();
            this.lblApplicationIDRequired = new System.Windows.Forms.Label();
            this.lblSecretRequired = new System.Windows.Forms.Label();
            this.lblClientIDRequired = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grdMappedServices = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSaveClose = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpBoxShippingSettings = new System.Windows.Forms.GroupBox();
            this.ddlPrinter = new System.Windows.Forms.ComboBox();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.txtApplicationID = new System.Windows.Forms.TextBox();
            this.lblApplicationID = new System.Windows.Forms.Label();
            this.txtSecret = new System.Windows.Forms.TextBox();
            this.lblSecret = new System.Windows.Forms.Label();
            this.txtClientID = new System.Windows.Forms.TextBox();
            this.lblClientID = new System.Windows.Forms.Label();
            this.btnSenderDetails = new System.Windows.Forms.Button();
            this.ddlManifestPrinter = new System.Windows.Forms.ComboBox();
            this.lblRoyalMailManifest = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).BeginInit();
            this.grpBoxShippingSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnManifest
            // 
            this.btnManifest.Location = new System.Drawing.Point(422, 137);
            this.btnManifest.Name = "btnManifest";
            this.btnManifest.Size = new System.Drawing.Size(180, 29);
            this.btnManifest.TabIndex = 65;
            this.btnManifest.Text = "Manifest";
            this.btnManifest.UseVisualStyleBackColor = true;
            this.btnManifest.Click += new System.EventHandler(this.btnManifest_Click);
            // 
            // lblPasswordRequired
            // 
            this.lblPasswordRequired.AutoSize = true;
            this.lblPasswordRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPasswordRequired.Location = new System.Drawing.Point(714, 41);
            this.lblPasswordRequired.Name = "lblPasswordRequired";
            this.lblPasswordRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPasswordRequired.TabIndex = 58;
            this.lblPasswordRequired.Text = "Required";
            this.lblPasswordRequired.Visible = false;
            // 
            // lblUserNameRequired
            // 
            this.lblUserNameRequired.AutoSize = true;
            this.lblUserNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblUserNameRequired.Location = new System.Drawing.Point(714, 9);
            this.lblUserNameRequired.Name = "lblUserNameRequired";
            this.lblUserNameRequired.Size = new System.Drawing.Size(50, 13);
            this.lblUserNameRequired.TabIndex = 57;
            this.lblUserNameRequired.Text = "Required";
            this.lblUserNameRequired.Visible = false;
            // 
            // lblApplicationIDRequired
            // 
            this.lblApplicationIDRequired.AutoSize = true;
            this.lblApplicationIDRequired.ForeColor = System.Drawing.Color.Red;
            this.lblApplicationIDRequired.Location = new System.Drawing.Point(332, 69);
            this.lblApplicationIDRequired.Name = "lblApplicationIDRequired";
            this.lblApplicationIDRequired.Size = new System.Drawing.Size(50, 13);
            this.lblApplicationIDRequired.TabIndex = 56;
            this.lblApplicationIDRequired.Text = "Required";
            this.lblApplicationIDRequired.Visible = false;
            // 
            // lblSecretRequired
            // 
            this.lblSecretRequired.AutoSize = true;
            this.lblSecretRequired.ForeColor = System.Drawing.Color.Red;
            this.lblSecretRequired.Location = new System.Drawing.Point(332, 37);
            this.lblSecretRequired.Name = "lblSecretRequired";
            this.lblSecretRequired.Size = new System.Drawing.Size(50, 13);
            this.lblSecretRequired.TabIndex = 55;
            this.lblSecretRequired.Text = "Required";
            this.lblSecretRequired.Visible = false;
            // 
            // lblClientIDRequired
            // 
            this.lblClientIDRequired.AutoSize = true;
            this.lblClientIDRequired.ForeColor = System.Drawing.Color.Red;
            this.lblClientIDRequired.Location = new System.Drawing.Point(332, 10);
            this.lblClientIDRequired.Name = "lblClientIDRequired";
            this.lblClientIDRequired.Size = new System.Drawing.Size(50, 13);
            this.lblClientIDRequired.TabIndex = 54;
            this.lblClientIDRequired.Text = "Required";
            this.lblClientIDRequired.Visible = false;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(339, 75);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(92, 13);
            this.lblMessage.TabIndex = 15;
            this.lblMessage.Text = "No Record Found";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(601, 174);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(180, 29);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // grdMappedServices
            // 
            this.grdMappedServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMappedServices.Location = new System.Drawing.Point(0, 19);
            this.grdMappedServices.Name = "grdMappedServices";
            this.grdMappedServices.Size = new System.Drawing.Size(781, 150);
            this.grdMappedServices.TabIndex = 12;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(252, 137);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(164, 29);
            this.btnDelete.TabIndex = 52;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSaveClose
            // 
            this.btnSaveClose.Location = new System.Drawing.Point(415, 175);
            this.btnSaveClose.Name = "btnSaveClose";
            this.btnSaveClose.Size = new System.Drawing.Size(180, 29);
            this.btnSaveClose.TabIndex = 13;
            this.btnSaveClose.Text = "Save && Close";
            this.btnSaveClose.UseVisualStyleBackColor = true;
            this.btnSaveClose.Click += new System.EventHandler(this.btnSaveClose_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(66, 137);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(180, 29);
            this.btnAdd.TabIndex = 51;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grpBoxShippingSettings
            // 
            this.grpBoxShippingSettings.Controls.Add(this.lblMessage);
            this.grpBoxShippingSettings.Controls.Add(this.btnCancel);
            this.grpBoxShippingSettings.Controls.Add(this.btnSaveClose);
            this.grpBoxShippingSettings.Controls.Add(this.grdMappedServices);
            this.grpBoxShippingSettings.Location = new System.Drawing.Point(7, 172);
            this.grpBoxShippingSettings.Name = "grpBoxShippingSettings";
            this.grpBoxShippingSettings.Size = new System.Drawing.Size(787, 229);
            this.grpBoxShippingSettings.TabIndex = 53;
            this.grpBoxShippingSettings.TabStop = false;
            this.grpBoxShippingSettings.Text = "Shipping Mapping";
            // 
            // ddlPrinter
            // 
            this.ddlPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPrinter.FormattingEnabled = true;
            this.ddlPrinter.Location = new System.Drawing.Point(517, 69);
            this.ddlPrinter.Name = "ddlPrinter";
            this.ddlPrinter.Size = new System.Drawing.Size(191, 21);
            this.ddlPrinter.TabIndex = 50;
            // 
            // lblPrinter
            // 
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Location = new System.Drawing.Point(437, 75);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(70, 13);
            this.lblPrinter.TabIndex = 49;
            this.lblPrinter.Text = "Select Printer";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(517, 40);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(191, 20);
            this.txtPassword.TabIndex = 45;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(453, 44);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 44;
            this.lblPassword.Text = "Password";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(517, 6);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(191, 20);
            this.txtUserName.TabIndex = 43;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(447, 10);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(60, 13);
            this.lblUserName.TabIndex = 42;
            this.lblUserName.Text = "User Name";
            // 
            // txtApplicationID
            // 
            this.txtApplicationID.Location = new System.Drawing.Point(97, 65);
            this.txtApplicationID.Name = "txtApplicationID";
            this.txtApplicationID.Size = new System.Drawing.Size(229, 20);
            this.txtApplicationID.TabIndex = 41;
            // 
            // lblApplicationID
            // 
            this.lblApplicationID.AutoSize = true;
            this.lblApplicationID.Location = new System.Drawing.Point(15, 71);
            this.lblApplicationID.Name = "lblApplicationID";
            this.lblApplicationID.Size = new System.Drawing.Size(73, 13);
            this.lblApplicationID.TabIndex = 40;
            this.lblApplicationID.Text = "Application ID";
            // 
            // txtSecret
            // 
            this.txtSecret.Location = new System.Drawing.Point(97, 37);
            this.txtSecret.Name = "txtSecret";
            this.txtSecret.Size = new System.Drawing.Size(229, 20);
            this.txtSecret.TabIndex = 39;
            // 
            // lblSecret
            // 
            this.lblSecret.AutoSize = true;
            this.lblSecret.Location = new System.Drawing.Point(15, 40);
            this.lblSecret.Name = "lblSecret";
            this.lblSecret.Size = new System.Drawing.Size(38, 13);
            this.lblSecret.TabIndex = 38;
            this.lblSecret.Text = "Secret";
            // 
            // txtClientID
            // 
            this.txtClientID.Location = new System.Drawing.Point(97, 9);
            this.txtClientID.Name = "txtClientID";
            this.txtClientID.Size = new System.Drawing.Size(229, 20);
            this.txtClientID.TabIndex = 37;
            // 
            // lblClientID
            // 
            this.lblClientID.AutoSize = true;
            this.lblClientID.Location = new System.Drawing.Point(16, 9);
            this.lblClientID.Name = "lblClientID";
            this.lblClientID.Size = new System.Drawing.Size(47, 13);
            this.lblClientID.TabIndex = 36;
            this.lblClientID.Text = "Client ID";
            // 
            // btnSenderDetails
            // 
            this.btnSenderDetails.Location = new System.Drawing.Point(608, 137);
            this.btnSenderDetails.Name = "btnSenderDetails";
            this.btnSenderDetails.Size = new System.Drawing.Size(180, 29);
            this.btnSenderDetails.TabIndex = 66;
            this.btnSenderDetails.Text = "Sender Details";
            this.btnSenderDetails.UseVisualStyleBackColor = true;
            this.btnSenderDetails.Click += new System.EventHandler(this.btnSenderDetails_Click);
            // 
            // ddlManifestPrinter
            // 
            this.ddlManifestPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlManifestPrinter.FormattingEnabled = true;
            this.ddlManifestPrinter.Location = new System.Drawing.Point(517, 100);
            this.ddlManifestPrinter.Name = "ddlManifestPrinter";
            this.ddlManifestPrinter.Size = new System.Drawing.Size(191, 21);
            this.ddlManifestPrinter.TabIndex = 68;
            // 
            // lblRoyalMailManifest
            // 
            this.lblRoyalMailManifest.AutoSize = true;
            this.lblRoyalMailManifest.Location = new System.Drawing.Point(346, 103);
            this.lblRoyalMailManifest.Name = "lblRoyalMailManifest";
            this.lblRoyalMailManifest.Size = new System.Drawing.Size(160, 13);
            this.lblRoyalMailManifest.TabIndex = 67;
            this.lblRoyalMailManifest.Text = "Select Printer For Manifest Label";
            // 
            // RoyalMailSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 411);
            this.Controls.Add(this.ddlManifestPrinter);
            this.Controls.Add(this.lblRoyalMailManifest);
            this.Controls.Add(this.btnSenderDetails);
            this.Controls.Add(this.btnManifest);
            this.Controls.Add(this.lblPasswordRequired);
            this.Controls.Add(this.lblUserNameRequired);
            this.Controls.Add(this.lblApplicationIDRequired);
            this.Controls.Add(this.lblSecretRequired);
            this.Controls.Add(this.lblClientIDRequired);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpBoxShippingSettings);
            this.Controls.Add(this.ddlPrinter);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.txtApplicationID);
            this.Controls.Add(this.lblApplicationID);
            this.Controls.Add(this.txtSecret);
            this.Controls.Add(this.lblSecret);
            this.Controls.Add(this.txtClientID);
            this.Controls.Add(this.lblClientID);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RoyalMailSettings";
            this.Text = "Royal Mail Settings";
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).EndInit();
            this.grpBoxShippingSettings.ResumeLayout(false);
            this.grpBoxShippingSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnManifest;
        private System.Windows.Forms.Label lblPasswordRequired;
        private System.Windows.Forms.Label lblUserNameRequired;
        private System.Windows.Forms.Label lblApplicationIDRequired;
        private System.Windows.Forms.Label lblSecretRequired;
        private System.Windows.Forms.Label lblClientIDRequired;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView grdMappedServices;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSaveClose;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grpBoxShippingSettings;
        private System.Windows.Forms.ComboBox ddlPrinter;
        private System.Windows.Forms.Label lblPrinter;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.TextBox txtApplicationID;
        private System.Windows.Forms.Label lblApplicationID;
        private System.Windows.Forms.TextBox txtSecret;
        private System.Windows.Forms.Label lblSecret;
        private System.Windows.Forms.TextBox txtClientID;
        private System.Windows.Forms.Label lblClientID;
        private System.Windows.Forms.Button btnSenderDetails;
        private System.Windows.Forms.ComboBox ddlManifestPrinter;
        private System.Windows.Forms.Label lblRoyalMailManifest;
    }
}