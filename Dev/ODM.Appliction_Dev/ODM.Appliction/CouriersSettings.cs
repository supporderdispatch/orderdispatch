﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class CouriersSettings : Form
    {
        #region "Global Variables"
        string _CourierName = "";
        #endregion

        #region "Constructor"
        public CouriersSettings()
        {
            InitializeComponent();
            BindGrid();
        }
        #endregion

        #region "Events"
        private void grdCouriers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                string strSelectedRow = grdCouriers.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                if (strSelectedRow == "Settings")
                {
                    string strCourierID = grdCouriers.Rows[e.RowIndex].Cells[0].Value.ToString();
                    string strCourierName = grdCouriers.Rows[e.RowIndex].Cells[1].Value.ToString();
                    _CourierName = strCourierName;
                    ToggleWindows(strCourierID, strCourierName);
                }
            }
        }
        private void btnClose_Click(object sender, System.EventArgs e)
        {
            switch (_CourierName.ToUpper())
            {
                case "MYHERMES":
                    {
                        Form frmSettings = Application.OpenForms["myHermesSettings"];
                        if (frmSettings != null)
                        {
                            frmSettings.Close();
                        }
                    }
                    break;

                case "HERMES":
                    {
                        Form frmSettings = Application.OpenForms["HermesSetting"];
                        if (frmSettings != null)
                        {
                            frmSettings.Close();
                        }
                    }
                    break;

                case "SPRING":
                    {
                        Form frmSettings = Application.OpenForms["SpringCourierSettings_New"];
                        
                        
                       // Form frmSettings = Application.OpenForms["SpringSettings"];
                        if (frmSettings != null)
                        {
                            frmSettings.Close();
                        }
                    }
                    break;

                case "DPD":
                    {
                        Form frmSettings = Application.OpenForms["DPDCourierSettings"];
                        if (frmSettings != null)
                        {
                            frmSettings.Close();
                        }
                    }
                    break;

                case "ROYAL MAIL":
                    {
                        Form frmSettings = Application.OpenForms["RoyalMailSettings"];
                        if (frmSettings != null)
                        {
                            frmSettings.Close();
                        }
                    }
                    break;

                case "FEDEX":
                    {
                        Form frmSettings = Application.OpenForms["FedExSettings"];
                        if (frmSettings != null)
                        {
                            frmSettings.Close();
                        }
                    }
                    break;
                case "AMAZON":
                    {
                        Form frmSettings = Application.OpenForms["AmazonCourierSettings"];
                        if (frmSettings != null)
                        {
                            frmSettings.Close();
                        }
                    }
                    break;
            }
            Form frmAddService = Application.OpenForms["AddServices"];
            if (frmAddService != null)
            {
                frmAddService.Close();
            }
            this.Close();
        }
        #endregion

        #region "Methods"
        private void BindGrid()
        {
            DataTable ObjdtCourier = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            ObjdtCourier = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetCourierData").Tables[0];
            if (ObjdtCourier.Rows.Count > 0)
            {
                grdCouriers.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
                grdCouriers.AllowUserToAddRows = false;
                grdCouriers.DataSource = ObjdtCourier;
                grdCouriers.Columns[0].Visible = false;
                grdCouriers.Columns[1].Width = 250;
                grdCouriers.Columns[2].Width = 250;
            }
        }
        private static void ToggleWindows(string strCourierID, string strCourierName)
        {
            Form frmHermesSettings = Application.OpenForms["HermesSetting"];
            Form frmmyHermesSettings = Application.OpenForms["myHermesSettings"];
            //Form frmSpringSettings = Application.OpenForms["SpringSettings"];
            Form frmSpringSettings = Application.OpenForms["SpringCourierSettings_New"];
            Form frmDPDSettings = Application.OpenForms["DPDCourierSettings"];
            Form frmRoyalMailSettings = Application.OpenForms["RoyalMailSettings"];
            Form frmFedExSettings = Application.OpenForms["FedEXSettings"];
            Form frmAmazonSettings = Application.OpenForms["AmazonCourierSettings"];
            switch (strCourierName.ToUpper())
            {
                case "MYHERMES":
                    if (frmmyHermesSettings == null)
                    {
                        myHermesSettings objMyHermes = new myHermesSettings(strCourierName, strCourierID);
                        objMyHermes.Show();
                    }
                    else
                    {
                        frmHermesSettings.Close();
                    }
                    break;

                case "HERMES":
                    if (frmHermesSettings == null)
                    {
                        HermesSetting objHermesSetting = new HermesSetting(strCourierName, strCourierID);
                        objHermesSetting.Show();
                    }
                    else
                    {
                        frmmyHermesSettings.Close();
                    }
                    break;

                case "SPRING":
                    if (frmSpringSettings == null)
                    {
                        SpringCourierSettings_New objSpringSetting = new SpringCourierSettings_New(strCourierName, strCourierID);
                        //SpringSettings objSpringSetting = new SpringSettings(strCourierName, strCourierID);
                        objSpringSetting.Show();
                    }
                    else
                    {
                        frmSpringSettings.Close();
                    }
                    break;

                case "DPD":
                    if (frmDPDSettings == null)
                    {
                        DPDCourierSettings objDPDSetting = new DPDCourierSettings(strCourierName, strCourierID);
                        objDPDSetting.Show();
                    }
                    else
                    {
                        frmDPDSettings.Close();
                    }
                    break;

                case "ROYAL MAIL":
                    if (frmRoyalMailSettings == null)
                    {
                        RoyalMailSettings objRoyalMailSetting = new RoyalMailSettings(strCourierName, strCourierID);
                        objRoyalMailSetting.Show();
                    }
                    else
                    {
                        frmRoyalMailSettings.Close();
                    }
                    break;

                case "FEDEX":
                    if (frmFedExSettings == null)
                    {
                        FedExSettings objFedExSettings = new FedExSettings(strCourierName, strCourierID);
                        objFedExSettings.Show();
                    }
                    else
                    {
                        frmFedExSettings.Close();
                    }
                    break;

                case "AMAZON":
                    if (frmAmazonSettings == null)
                    {
                        AmazonCourierSettings objAmazonCourierSettings = new AmazonCourierSettings(strCourierName, strCourierID);
                        objAmazonCourierSettings.Show();
                    }
                    else
                    {
                        frmAmazonSettings.Close();
                    }
                    break;
            }
        }
        #endregion
    }
}
