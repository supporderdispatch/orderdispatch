﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class MapServiceUrl : Form
    {
        #region "Global Variables"
        bool blIsFirstCall = false;
        DataTable objDt = new DataTable();
        #endregion

        #region "Constructor"
        public MapServiceUrl()
        {
            InitializeComponent();
            blIsFirstCall = true;
            BindCountryDdlAndUpdateServiceURL("GET");
            blIsFirstCall = false;
        }
        #endregion

        #region "Events"
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            ValiDateAndUpdateServiceURL();
        }
        private void ddlCountryName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!blIsFirstCall)
            {
                BindCountryDdlAndUpdateServiceURL("GET");
                if (objDt.Rows.Count > 0)
                {
                    txtServiceURL.Text = Convert.ToString(objDt.Rows[0]["ServiceURL"]);
                    txtServiceURL.Focus();
                }
            }
        }
        private void txtServiceURL_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                ValiDateAndUpdateServiceURL();
            }
        }
        #endregion

        #region "Methods"
        private void ValiDateAndUpdateServiceURL()
        {
            if (!string.IsNullOrEmpty(txtServiceURL.Text.Trim()))
            {
                lblUrlRequired.Hide();
                BindCountryDdlAndUpdateServiceURL("UPDATE");
            }
            else
            {
                lblUrlRequired.Show();
            }
        }
        private void BindCountryDdlAndUpdateServiceURL(string strMode)
        {
            SqlConnection conn = new SqlConnection();
            string strID = ""; int intResult = 100;
            try
            {
                conn.ConnectionString = MakeConnection.GetConnection();
                conn.Open();
                if (ddlCountryName.SelectedValue != null)
                {
                    strID = Convert.ToString(ddlCountryName.SelectedValue.ToString());
                }
                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 20);
                objParam[0].Value = strMode;

                objParam[1] = new SqlParameter("@ID", SqlDbType.VarChar, 40);
                objParam[1].Value = strID;

                objParam[2] = new SqlParameter("@ServiceURL", SqlDbType.VarChar, 500);
                objParam[2].Value = txtServiceURL.Text;

                objParam[3] = new SqlParameter("@Result", SqlDbType.Int);
                objParam[3].Direction = ParameterDirection.Output;

                if (strMode.ToUpper() == "GET")
                {
                    objDt = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAndUpdateServiceURL", objParam).Tables[0];
                }
                else if (strMode.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAndUpdateServiceURL", objParam);
                }
                intResult = Convert.ToInt32(objParam[3].Value);
                if (blIsFirstCall)
                {
                    if (objDt.Rows.Count > 0)
                    {
                        ddlCountryName.DataSource = objDt;
                        ddlCountryName.ValueMember = "ID";
                        ddlCountryName.DisplayMember = "CountryName";
                        ddlCountryName.SelectedValue = Convert.ToString(objDt.Rows[0]["ID"]);
                        txtServiceURL.Text = Convert.ToString(objDt.Rows[0]["ServiceURL"]);
                    }
                }
                if (intResult == 1)
                {
                    MessageBox.Show("Record Updated Successfully", "Record Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occrred", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion
    }
}
