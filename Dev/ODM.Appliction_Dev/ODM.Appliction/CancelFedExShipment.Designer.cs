﻿namespace ODM.Appliction
{
    partial class CancelFedExShipment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CancelFedExShipment));
            this.grdOrderDetails = new System.Windows.Forms.DataGridView();
            this.grdTrackingNumber = new System.Windows.Forms.DataGridView();
            this.lblOrder = new System.Windows.Forms.Label();
            this.txtOrderID = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblOrderIdRequired = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTrackingNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // grdOrderDetails
            // 
            this.grdOrderDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOrderDetails.Location = new System.Drawing.Point(2, 63);
            this.grdOrderDetails.Name = "grdOrderDetails";
            this.grdOrderDetails.Size = new System.Drawing.Size(778, 150);
            this.grdOrderDetails.TabIndex = 0;
            this.grdOrderDetails.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOrderDetails_CellClick);
            // 
            // grdTrackingNumber
            // 
            this.grdTrackingNumber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdTrackingNumber.Location = new System.Drawing.Point(2, 219);
            this.grdTrackingNumber.Name = "grdTrackingNumber";
            this.grdTrackingNumber.Size = new System.Drawing.Size(778, 150);
            this.grdTrackingNumber.TabIndex = 1;
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Location = new System.Drawing.Point(13, 26);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(163, 13);
            this.lblOrder.TabIndex = 2;
            this.lblOrder.Text = "Find processed Order by Order Id";
            // 
            // txtOrderID
            // 
            this.txtOrderID.Location = new System.Drawing.Point(198, 23);
            this.txtOrderID.MaxLength = 6;
            this.txtOrderID.Name = "txtOrderID";
            this.txtOrderID.Size = new System.Drawing.Size(267, 20);
            this.txtOrderID.TabIndex = 3;
            this.txtOrderID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOrderID_KeyPress);
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(482, 21);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(112, 23);
            this.btnFind.TabIndex = 4;
            this.btnFind.Text = "Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(318, 138);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(92, 13);
            this.lblMessage.TabIndex = 5;
            this.lblMessage.Text = "No Record Found";
            this.lblMessage.Visible = false;
            // 
            // lblOrderIdRequired
            // 
            this.lblOrderIdRequired.AutoSize = true;
            this.lblOrderIdRequired.ForeColor = System.Drawing.Color.Red;
            this.lblOrderIdRequired.Location = new System.Drawing.Point(247, 7);
            this.lblOrderIdRequired.Name = "lblOrderIdRequired";
            this.lblOrderIdRequired.Size = new System.Drawing.Size(0, 13);
            this.lblOrderIdRequired.TabIndex = 6;
            this.lblOrderIdRequired.Visible = false;
            // 
            // CancelFedExShipment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 372);
            this.Controls.Add(this.lblOrderIdRequired);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.txtOrderID);
            this.Controls.Add(this.lblOrder);
            this.Controls.Add(this.grdTrackingNumber);
            this.Controls.Add(this.grdOrderDetails);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CancelFedExShipment";
            this.Text = "Cancel FedEx Shipment";
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTrackingNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdOrderDetails;
        private System.Windows.Forms.DataGridView grdTrackingNumber;
        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.TextBox txtOrderID;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblOrderIdRequired;
    }
}