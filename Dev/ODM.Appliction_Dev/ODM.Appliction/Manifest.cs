﻿using linnworks.finaware.CommonData.Objects;
using Microsoft.ApplicationBlocks.Data;
using ODM.Data.ShippingLabel;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using linnworks.finaware.CommonData.Objects;

namespace ODM.Appliction
{
    public partial class Manifest : Form
    {
        #region "Global Variables"
        SqlConnection connection;
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        DataTable objDtGridData = new DataTable();
        string _CourierName, _SearchText = "";
        #endregion

        #region "Constructor"
        public Manifest(string CourierName)
        {
            InitializeComponent();
            _CourierName = CourierName;
            if (CourierName.ToUpper() == "AMAZON")
            {
                btnManifest.Hide();
                btnVoidAll.Hide();
            }
            CheckManifestLabel();
            BindGrid();
        }
        #endregion

        #region "Methods"
        public void BindGrid()
        {
            try
            {
                objDtGridData = GlobalVar.GetManifestOrders(_CourierName, _SearchText);
                if (objDtGridData.Rows.Count > 0)
                {
                    btnVoidAll.Enabled = true;
                    btnManifest.Enabled = true;
                    grdManifest.Visible = true;
                    grdSummary.Visible = true;
                    lblMsg.Visible = false;

                    grdManifest.AllowUserToAddRows = false;
                    grdManifest.DataSource = objDtGridData;
                    grdManifest.AutoGenerateColumns = false;


                    grdManifest.Columns[0].Visible = false;
                    grdManifest.Columns[1].ReadOnly = true;
                    grdManifest.Columns[2].ReadOnly = true;
                    grdManifest.Columns[3].ReadOnly = true;
                    grdManifest.Columns[4].ReadOnly = true;
                    grdManifest.Columns[5].ReadOnly = true;
                    grdManifest.Columns[6].ReadOnly = true;
                    grdManifest.Columns[1].Width = 110;
                    grdManifest.Columns[4].Width = 200;
                    grdManifest.Columns[5].Width = 200;
                    BindSummaryGrid();
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "No Record Found";

                    btnVoidAll.Enabled = false;
                    btnManifest.Enabled = false;
                    grdManifest.Visible = false;
                    grdSummary.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An Error Occurred Contact to the Administrator");
            }
        }
        private void BindSummaryGrid()
        {
            try
            {
                DataTable objDtGridDataSummary = new DataTable();
                objDtGridDataSummary = GlobalVar.GetManifestOrderSummary(_CourierName, _SearchText);
                if (objDtGridDataSummary.Rows.Count > 0)
                {
                    grdSummary.AllowUserToAddRows = false;
                    grdSummary.DataSource = objDtGridDataSummary;
                    grdSummary.AutoGenerateColumns = false;
                    grdSummary.Columns[0].Visible = false;
                    grdSummary.Columns[1].ReadOnly = true;
                    grdSummary.Columns[2].ReadOnly = true;
                    grdSummary.Columns[3].ReadOnly = true;
                    grdSummary.Columns[1].Width = 280;
                    grdSummary.Columns[2].Width = 280;
                    grdSummary.Columns[3].Width = 270;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void RemoveTrackingNumberInDatabase(string strOrderIds, string IsManifestOrVoidCall)
        {
            try
            {
                connection = new SqlConnection();
                connection.ConnectionString = MakeConnection.GetConnection();
                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@OrderIDs", SqlDbType.VarChar, int.MaxValue);
                objParam[0].Value = strOrderIds;

                objParam[1] = new SqlParameter("@IsManifestOrVoidCall", SqlDbType.VarChar, 20);
                objParam[1].Value = IsManifestOrVoidCall;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "VoidManifestOrders", objParam);
            }
            catch
            {
                MessageBox.Show("Error occurred during void manifest orders", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region "Events"
        private void btnRefress_Click(object sender, EventArgs e)
        {
            _SearchText = "";
            txtSearch.Text = "";
            BindGrid();
        }

        private void btnVoidAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (objDtGridData.Rows.Count > 0)
                {
                    string strOrderIds = "";
                    string[] strArrTrackingNumber = new string[objDtGridData.Rows.Count];
                    for (int i = 0; i < objDtGridData.Rows.Count; i++)
                    {
                        string strOrderID = Convert.ToString(objDtGridData.Rows[i]["Orderid"]);
                        string strTrackingNumber = Convert.ToString(objDtGridData.Rows[i]["Tracking Number"]);
                        strOrderIds = strOrderIds == "" ? strOrderID : strOrderIds + "," + strOrderID;
                        strArrTrackingNumber[i] = strTrackingNumber;
                        if (_CourierName.ToUpper() == "SPRING")
                        {
                            VoidManifestOrdersForSpring(strTrackingNumber);
                        }
                    }
                    if (_CourierName.ToUpper() == "ROYAL MAIL" || _CourierName.ToUpper() == "OBA")
                    {
                        VoidManifestOrdersForRoyalMail(strArrTrackingNumber);
                    }
                    RemoveTrackingNumberInDatabase(strOrderIds, "Void");
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnManifest_Click(object sender, EventArgs e)
        {
            if (objDtGridData.Rows.Count > 0)
            {
                string[] strArrTrackingNumber = new string[objDtGridData.Rows.Count];
                string strOrderIds = "";
                for (int i = 0; i < objDtGridData.Rows.Count; i++)
                {
                    string strOrderID = Convert.ToString(objDtGridData.Rows[i]["Orderid"]);
                    strArrTrackingNumber[i] = Convert.ToString(objDtGridData.Rows[i]["Tracking Number"]);
                    strOrderIds = strOrderIds == "" ? strOrderID : strOrderIds + "," + strOrderID;
                }
                switch (_CourierName.ToUpper())
                {
                    case "SPRING":
                        {
                            FileManifestForSpring(strArrTrackingNumber, strOrderIds);
                        }
                        break;

                    case "ROYAL MAIL":
                    case "OBA":
                        {
                            FileManifestForRoyalMail(strArrTrackingNumber, strOrderIds);
                        }
                        break;
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdManifest_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    string strSelectedRow = grdManifest.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    if (strSelectedRow.ToUpper() == "CANCEL")
                    {
                        string OrderID = grdManifest.Rows[e.RowIndex].Cells[0].Value.ToString();
                        switch (_CourierName.ToUpper())
                        {
                            case "SPRING":
                                {
                                    string strTrackingNumber = grdManifest.Rows[e.RowIndex].Cells[5].Value.ToString();
                                    VoidManifestOrdersForSpring(strTrackingNumber);
                                }
                                break;
                            case "AMAZON":
                                {
                                    string strTrackingNumber = grdManifest.Rows[e.RowIndex].Cells[5].Value.ToString();
                                    VoidManifestOrdersForAmazon(OrderID, strTrackingNumber);
                                }
                                break;
                            case "ROYAL MAIL":
                            case "OBA":
                                {
                                    string[] strTrackingNumbers = new string[1];
                                    strTrackingNumbers[0] = grdManifest.Rows[e.RowIndex].Cells[5].Value.ToString();
                                    VoidManifestOrdersForRoyalMail(strTrackingNumbers);
                                }
                                break;
                        }
                        RemoveTrackingNumberInDatabase(OrderID, "Void");
                        BindGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region "Spring"
        #region "Methods"
        private void SetGlobalVariables()
        {
            connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            DataTable objDt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetSpringSettingDetails";
                cmd.CommandTimeout = 200;
                SqlDataAdapter objAdapt = new SqlDataAdapter(cmd);
                objAdapt.Fill(objDt);
                bool IsLiveMode = Convert.ToBoolean(objDt.Rows[0]["IsLive"]);
                if (IsLiveMode)
                {
                    GlobalVariablesAndMethods.UseLive = true;
                    GlobalVariablesAndMethods.strApiToken = Convert.ToString(objDt.Rows[0]["LiveActivationKey"]);
                    GlobalVariablesAndMethods.APIUser = Convert.ToString(objDt.Rows[0]["LiveLoginName"]);
                    GlobalVariablesAndMethods.APIPass = Convert.ToString(objDt.Rows[0]["LivePassword"]);
                }
                else
                {
                    GlobalVariablesAndMethods.UseLive = false;
                    GlobalVariablesAndMethods.strApiToken = Convert.ToString(objDt.Rows[0]["TestActivationKey"]);
                    GlobalVariablesAndMethods.APIUser = Convert.ToString(objDt.Rows[0]["TestLoginName"]);
                    GlobalVariablesAndMethods.APIPass = Convert.ToString(objDt.Rows[0]["TestPassword"]);
                }
                GlobalVariablesAndMethods.blVerboseLogging = Convert.ToBoolean(objDt.Rows[0]["IsBoseLogin"]);
                GlobalVariablesAndMethods.strDefaultWeightInKg = Convert.ToString(objDt.Rows[0]["DefaultWeight"]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }
        private void VoidManifestOrdersForSpring(string TrackingNumber)
        {
            try
            {
                SetGlobalVariables();
                ShippingLabelMacroSpring objSpring = new ShippingLabelMacroSpring();
                throw new NotImplementedException();
                //linnworks.scripting.core.Debugger objDebug = new linnworks.scripting.core.Debugger();
                //objSpring.VoidManifestOrders(TrackingNumber, objDebug);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FileManifestForSpring(string[] strArrTrackingNumber, string strOrderIds)
        {
            System.Drawing.Image ImageToPrint = FileManifestOrders(strArrTrackingNumber);
            RemoveTrackingNumberInDatabase(strOrderIds, "Manifest");
            BindGrid();
            SpringPrintLabel objSpringPrint = new SpringPrintLabel();
            SpringPrintLabel.ManifestLabel = ImageToPrint;
            objSpringPrint.PrintLabel();
            SpringPrintLabel.ManifestLabel = null;
        }
        private System.Drawing.Image FileManifestOrders(string[] strTrackingNumbers)
        {
            try
            {
                SetGlobalVariables();
                ShippingLabelMacroSpring objSpring = new ShippingLabelMacroSpring();
                throw new NotFiniteNumberException();
                //linnworks.scripting.core.Debugger objDebug = new linnworks.scripting.core.Debugger();
                //return objSpring.FileManifestOrders(strTrackingNumbers, objDebug);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion

        #region "Royal Mail"
        #region "Methods"
        private void VoidManifestOrdersForRoyalMail(string[] TrackingNumber)
        {
            try
            {
                RoyalMailPrintLabel objPrintLabel = new RoyalMailPrintLabel();
                objPrintLabel.SetVariableForApiCall();
                ShippingLabelRoyalMail objRoyalMail = new ShippingLabelRoyalMail();
                objRoyalMail.CancelShipmet(TrackingNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FileManifestForRoyalMail(string[] strArrTrackingNumber, string strOrderIds)
        {
            try
            {
                RoyalMailPrintLabel objPrintLabel = new RoyalMailPrintLabel();
                objPrintLabel.SetVariableForApiCall();
                ShippingLabelRoyalMail objRoyalMail = new ShippingLabelRoyalMail();
                objRoyalMail.CreateManifestRequest();
                RemoveTrackingNumberInDatabase(strOrderIds, "Manifest");
                BindGrid();
                objPrintLabel.PrintLabelRoyalMail();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("not ready for printing") || RoyalMailPrintLabel.blIsShipmentCreated)
                {
                    RemoveTrackingNumberInDatabase(strOrderIds, "Manifest");
                    BindGrid();
                    CheckManifestLabel();
                }
                else
                {
                    MessageBox.Show(ex.Message, "Roayl Mail API Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                RoyalMailPrintLabel.blIsShipmentCreated = false;
            }
        }
        private void CheckManifestLabel()
        {
            if (_CourierName.ToUpper() == "ROYAL MAIL" || _CourierName.ToUpper() == "OBA")
            {
                bool IsThereAnyPendingManifestLabel;
                DataTable dt = RoyalMailPrintLabel.WorkWithPendingLabels("", "GET", out IsThereAnyPendingManifestLabel);
                if (IsThereAnyPendingManifestLabel)
                {
                    btnPrintPendingLabel.Visible = true;
                    if (dt.Rows.Count > 1)
                    {
                        btnPrintPendingLabel.Text = string.Format("Print Pending({0}) Labels", dt.Rows.Count);
                    }
                    else if (dt.Rows.Count > 0)
                    {
                        btnPrintPendingLabel.Text = string.Format("Print Pending({0}) Label", dt.Rows.Count);
                    }
                }
                else
                {
                    btnPrintPendingLabel.Visible = false;
                }
            }
        }
        #endregion
        #region "Events"
        private void btnPrintPendingLabel_Click(object sender, EventArgs e)
        {
            string strBatchNumber = string.Empty;
            DataTable objPendingManifestLabel = new DataTable();
            RoyalMailPrintLabel.IsManifest = true;
            string strBatchNumbersToDelete = string.Empty, strBatchNumbersNotFound = string.Empty;
            int intRowCount = 0, intPrintedLabelCount = 0;
            bool IsHaveData;

            // This goto label is to execute the all batchnumbers if not found error occurred.
        RepeatIfBatchNumberNotFound:
            try
            {
                if (objPendingManifestLabel == null || objPendingManifestLabel.Rows.Count == 0)
                {
                    objPendingManifestLabel = RoyalMailPrintLabel.WorkWithPendingLabels("", "GET", out IsHaveData);
                }

                for (int i = intRowCount; i < objPendingManifestLabel.Rows.Count; i++)
                {
                    strBatchNumber = Convert.ToString(objPendingManifestLabel.Rows[i]["BatchNumber"]);
                    strBatchNumbersToDelete = string.IsNullOrEmpty(strBatchNumbersToDelete) ? strBatchNumber : strBatchNumbersToDelete + "," + strBatchNumber;

                    RoyalMailPrintLabel objPrintLabel = new RoyalMailPrintLabel();
                    objPrintLabel.SetVariableForApiCall();

                    ShippingLabelRoyalMail objRoyalMail = new ShippingLabelRoyalMail();
                    objRoyalMail.PrintManifestLabel(strBatchNumber);
                    objPrintLabel.PrintLabelRoyalMail();
                    intRowCount++;
                    intPrintedLabelCount++;
                }
                if (!string.IsNullOrEmpty(strBatchNumbersNotFound))
                {
                    ExecuteIfLabelNotFoundOccurred(ref strBatchNumbersToDelete, strBatchNumbersNotFound, intPrintedLabelCount);
                }
                else
                {
                    RoyalMailPrintLabel.WorkWithPendingLabels(strBatchNumbersToDelete, "DELETE", out IsHaveData);
                    CheckManifestLabel();
                }
            }
            catch (Exception ex)
            {
                //Check if batchnumber is not found
                if (ex.Message.ToUpper().Contains("MANIFESTBATCHNUMBER " + strBatchNumber.ToUpper() + " NOT FOUND"))
                {
                    strBatchNumbersNotFound = string.IsNullOrEmpty(strBatchNumbersNotFound) ? strBatchNumber : strBatchNumbersNotFound + "," + strBatchNumber;

                    //To check if that batch number is the last one then show message from here otherwise sending back to lable RepeatIfBatchNumberNotFound
                    if (intRowCount + 1 == objPendingManifestLabel.Rows.Count)
                    {
                        ExecuteIfLabelNotFoundOccurred(ref strBatchNumbersToDelete, strBatchNumbersNotFound, intPrintedLabelCount);
                    }
                    else
                    {
                        intRowCount++;
                        goto RepeatIfBatchNumberNotFound;
                    }
                }
                else
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //End of check batchnumber not found
            }
        }
        private void ExecuteIfLabelNotFoundOccurred(ref string strBatchNumbersToDelete, string strBatchNumbersNotFound, int intPrintedLabelCount)
        {
            bool IsHaveData;
            string strCustomErrorMessage = string.Format("Label(s) printed: {0} \nBatchNumber(s): {1} doesn't exists", intPrintedLabelCount, strBatchNumbersNotFound);
            RoyalMailPrintLabel.WorkWithPendingLabels(strBatchNumbersToDelete, "DELETE", out IsHaveData);
            CheckManifestLabel();
            MessageBox.Show(strCustomErrorMessage, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        #endregion

        #endregion

        private void VoidManifestOrdersForAmazon(string strOrderID, string strTrackingNumber)
        {
            try
            {
                Guid dummyGuid = new Guid();
                Guid OrderID = new Guid(strOrderID);
                AmazonPrintLabel objAmazon = new AmazonPrintLabel();
                objAmazon.CancelShipment(OrderID);
                GlobalVariablesAndMethods.GetAddDeleteShipmentDetails(dummyGuid, "DELETE", OrderID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchManifest();
        }
        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                SearchManifest();
            }
        }
        private void SearchManifest()
        {
            if (string.IsNullOrEmpty(txtSearch.Text))
            {
                lblRequired.Show();
            }
            else
            {
                lblRequired.Hide();
                _SearchText = txtSearch.Text;
                BindGrid();
            }
        }
    }
}
