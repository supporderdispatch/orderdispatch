﻿using ODM.Data.Entity;
using ODM.Data.Service;
using System;
using System.Configuration;
using System.Windows.Forms;
using ODM.Core.UserTracking;

namespace ODM.Appliction
{
    public partial class Login : BaseForm
    {
        private IUserService userService;

        public Login()
        {
            InitializeComponent();
            CheckIfLoginExists();
            //string t= ConfigurationSettings.AppSettings.Get("OrderDispatchEntities");
        }

        private void CheckIfLoginExists()
        {
            try
            {
                txtUserName.Text = Helpers.Config.GetNodeValue("Username");
                txtPass.Text = Helpers.Config.GetNodeValue("Password");
                chkSaveLogin.Checked = Convert.ToBoolean(Helpers.Config.GetNodeValue("SaveLogin"));
            }
            catch { }
        }

        void ValidateLogin()
        {
            if (RequiredFieldValidation())
            {
                var appSettings = ODM.Data.Util.MakeConnection.GetConnection();
                userService = new UserService(appSettings.ToString());
                ConfigString = appSettings.ToString();
                User userObject = userService.CheckUserLogin(txtUserName.Text, txtPass.Text);
                if (userObject != null)
                {
                    // txtUserName.Text = "Done";
                    lblError.Text = string.Empty;
                    SaveLogin();
                    this.Hide();
                    UserTracker.BeginTrackingForUser(userObject.Userid);             
                    // show other form
                    LoginedUser = userObject;
                    var mainForm = Application.OpenForms["Main"];
                    if (mainForm != null)
                    {
                        mainForm.Focus();
                    }
                    else
                    {

                        Main frmMain = new Main(userObject);
                        frmMain.ShowDialog();
                    }
                    

                    // close application
                    this.Close();
                }
                else
                {
                    lblError.Text = "Invalid User or password";
                    txtUserName.Text = string.Empty;
                    txtPass.Text = string.Empty;
                    txtUserName.Focus();
                }
            }
        }

        private void SaveLogin()
        {
            Helpers.Config.SaveConfig(txtUserName.Text, txtPass.Text, chkSaveLogin.Checked);
        }
        private void Login_Load(object sender, EventArgs e)
        {
            //txtUserName.Focus();
            txtPass.Focus();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            ValidateLogin();
        }

        bool RequiredFieldValidation()
        {
            bool status = true;
            if (txtUserName.Text == string.Empty)
            {
                lblError.Text = "User is required";
                status = false;
                txtUserName.Focus();
                txtUserName.Text = string.Empty;
                txtPass.Text = string.Empty;
            }
            return status;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtUserName.Text = string.Empty;
            txtPass.Text = string.Empty;
            chkSaveLogin.Checked = false;
        }

        private void txtUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                //BindGrid();
                ValidateLogin();
            }
        }

        private void txtPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                //BindGrid();
                ValidateLogin();
            }
        }
    }
}
