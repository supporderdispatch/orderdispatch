﻿namespace ODM.Appliction
{
    partial class FedExSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FedExSettings));
            this.lblWeightInvalid = new System.Windows.Forms.Label();
            this.lblAccountLiveRequired = new System.Windows.Forms.Label();
            this.lblMeterLiveRequired = new System.Windows.Forms.Label();
            this.lblPswdLiveRequired = new System.Windows.Forms.Label();
            this.lblAccessKeyLiveRequired = new System.Windows.Forms.Label();
            this.lblWeightRequired = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpBoxShippingSettings = new System.Windows.Forms.GroupBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSaveClose = new System.Windows.Forms.Button();
            this.grdMappedServices = new System.Windows.Forms.DataGridView();
            this.txtDefaultWeight = new System.Windows.Forms.TextBox();
            this.lblDefaultWeight = new System.Windows.Forms.Label();
            this.ddlPrinter = new System.Windows.Forms.ComboBox();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.txtAccountNumberLive = new System.Windows.Forms.TextBox();
            this.lblAccountNumberLive = new System.Windows.Forms.Label();
            this.txtMeterNumberLive = new System.Windows.Forms.TextBox();
            this.lblMeterNumberLive = new System.Windows.Forms.Label();
            this.txtApiPassLive = new System.Windows.Forms.TextBox();
            this.lblAPIPassLive = new System.Windows.Forms.Label();
            this.txtApiAccessKeyLive = new System.Windows.Forms.TextBox();
            this.lblApiAccessKeyLive = new System.Windows.Forms.Label();
            this.btnShipper = new System.Windows.Forms.Button();
            this.lblAccountTestRequired = new System.Windows.Forms.Label();
            this.lblPswdTestRequired = new System.Windows.Forms.Label();
            this.lblAccessKeyTestRequired = new System.Windows.Forms.Label();
            this.txtAccountNumberTest = new System.Windows.Forms.TextBox();
            this.lblAccountNumberTest = new System.Windows.Forms.Label();
            this.txtMeterNumberTest = new System.Windows.Forms.TextBox();
            this.lblMeterNumberTest = new System.Windows.Forms.Label();
            this.txtApiPassTest = new System.Windows.Forms.TextBox();
            this.lblAPIPassTest = new System.Windows.Forms.Label();
            this.txtApiAccessKeyTest = new System.Windows.Forms.TextBox();
            this.lblAccessKeyTest = new System.Windows.Forms.Label();
            this.lblMeterTestRequired = new System.Windows.Forms.Label();
            this.ddlStockLabel = new System.Windows.Forms.ComboBox();
            this.lblLabelStock = new System.Windows.Forms.Label();
            this.chkIsLive = new System.Windows.Forms.CheckBox();
            this.btnVoidShipment = new System.Windows.Forms.Button();
            this.grpBoxShippingSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).BeginInit();
            this.SuspendLayout();
            // 
            // lblWeightInvalid
            // 
            this.lblWeightInvalid.AutoSize = true;
            this.lblWeightInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblWeightInvalid.Location = new System.Drawing.Point(733, 148);
            this.lblWeightInvalid.Name = "lblWeightInvalid";
            this.lblWeightInvalid.Size = new System.Drawing.Size(65, 13);
            this.lblWeightInvalid.TabIndex = 68;
            this.lblWeightInvalid.Text = "Invalid Input";
            this.lblWeightInvalid.Visible = false;
            // 
            // lblAccountLiveRequired
            // 
            this.lblAccountLiveRequired.AutoSize = true;
            this.lblAccountLiveRequired.ForeColor = System.Drawing.Color.Red;
            this.lblAccountLiveRequired.Location = new System.Drawing.Point(353, 113);
            this.lblAccountLiveRequired.Name = "lblAccountLiveRequired";
            this.lblAccountLiveRequired.Size = new System.Drawing.Size(50, 13);
            this.lblAccountLiveRequired.TabIndex = 66;
            this.lblAccountLiveRequired.Text = "Required";
            this.lblAccountLiveRequired.Visible = false;
            // 
            // lblMeterLiveRequired
            // 
            this.lblMeterLiveRequired.AutoSize = true;
            this.lblMeterLiveRequired.ForeColor = System.Drawing.Color.Red;
            this.lblMeterLiveRequired.Location = new System.Drawing.Point(353, 81);
            this.lblMeterLiveRequired.Name = "lblMeterLiveRequired";
            this.lblMeterLiveRequired.Size = new System.Drawing.Size(50, 13);
            this.lblMeterLiveRequired.TabIndex = 65;
            this.lblMeterLiveRequired.Text = "Required";
            this.lblMeterLiveRequired.Visible = false;
            // 
            // lblPswdLiveRequired
            // 
            this.lblPswdLiveRequired.AutoSize = true;
            this.lblPswdLiveRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPswdLiveRequired.Location = new System.Drawing.Point(353, 47);
            this.lblPswdLiveRequired.Name = "lblPswdLiveRequired";
            this.lblPswdLiveRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPswdLiveRequired.TabIndex = 64;
            this.lblPswdLiveRequired.Text = "Required";
            this.lblPswdLiveRequired.Visible = false;
            // 
            // lblAccessKeyLiveRequired
            // 
            this.lblAccessKeyLiveRequired.AutoSize = true;
            this.lblAccessKeyLiveRequired.ForeColor = System.Drawing.Color.Red;
            this.lblAccessKeyLiveRequired.Location = new System.Drawing.Point(353, 10);
            this.lblAccessKeyLiveRequired.Name = "lblAccessKeyLiveRequired";
            this.lblAccessKeyLiveRequired.Size = new System.Drawing.Size(50, 13);
            this.lblAccessKeyLiveRequired.TabIndex = 63;
            this.lblAccessKeyLiveRequired.Text = "Required";
            this.lblAccessKeyLiveRequired.Visible = false;
            // 
            // lblWeightRequired
            // 
            this.lblWeightRequired.AutoSize = true;
            this.lblWeightRequired.ForeColor = System.Drawing.Color.Red;
            this.lblWeightRequired.Location = new System.Drawing.Point(735, 148);
            this.lblWeightRequired.Name = "lblWeightRequired";
            this.lblWeightRequired.Size = new System.Drawing.Size(50, 13);
            this.lblWeightRequired.TabIndex = 67;
            this.lblWeightRequired.Text = "Required";
            this.lblWeightRequired.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(612, 225);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(164, 29);
            this.btnDelete.TabIndex = 14;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(426, 225);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(180, 29);
            this.btnAdd.TabIndex = 13;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grpBoxShippingSettings
            // 
            this.grpBoxShippingSettings.Controls.Add(this.lblMessage);
            this.grpBoxShippingSettings.Controls.Add(this.btnCancel);
            this.grpBoxShippingSettings.Controls.Add(this.btnSaveClose);
            this.grpBoxShippingSettings.Controls.Add(this.grdMappedServices);
            this.grpBoxShippingSettings.Location = new System.Drawing.Point(10, 260);
            this.grpBoxShippingSettings.Name = "grpBoxShippingSettings";
            this.grpBoxShippingSettings.Size = new System.Drawing.Size(804, 229);
            this.grpBoxShippingSettings.TabIndex = 60;
            this.grpBoxShippingSettings.TabStop = false;
            this.grpBoxShippingSettings.Text = "Shipping Mapping";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(339, 78);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(92, 13);
            this.lblMessage.TabIndex = 16;
            this.lblMessage.Text = "No Record Found";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(590, 193);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(180, 29);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSaveClose
            // 
            this.btnSaveClose.Location = new System.Drawing.Point(404, 193);
            this.btnSaveClose.Name = "btnSaveClose";
            this.btnSaveClose.Size = new System.Drawing.Size(180, 29);
            this.btnSaveClose.TabIndex = 16;
            this.btnSaveClose.Text = "Save && Close";
            this.btnSaveClose.UseVisualStyleBackColor = true;
            this.btnSaveClose.Click += new System.EventHandler(this.btnSaveClose_Click);
            // 
            // grdMappedServices
            // 
            this.grdMappedServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMappedServices.Location = new System.Drawing.Point(1, 19);
            this.grdMappedServices.Name = "grdMappedServices";
            this.grdMappedServices.Size = new System.Drawing.Size(792, 150);
            this.grdMappedServices.TabIndex = 15;
            // 
            // txtDefaultWeight
            // 
            this.txtDefaultWeight.Location = new System.Drawing.Point(526, 145);
            this.txtDefaultWeight.Name = "txtDefaultWeight";
            this.txtDefaultWeight.Size = new System.Drawing.Size(205, 20);
            this.txtDefaultWeight.TabIndex = 10;
            // 
            // lblDefaultWeight
            // 
            this.lblDefaultWeight.AutoSize = true;
            this.lblDefaultWeight.Location = new System.Drawing.Point(410, 148);
            this.lblDefaultWeight.Name = "lblDefaultWeight";
            this.lblDefaultWeight.Size = new System.Drawing.Size(108, 13);
            this.lblDefaultWeight.TabIndex = 61;
            this.lblDefaultWeight.Text = "Default Weight In KG";
            // 
            // ddlPrinter
            // 
            this.ddlPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPrinter.FormattingEnabled = true;
            this.ddlPrinter.Items.AddRange(new object[] {
            "ZDesigner GC420d(EPL)",
            "Brother DCP-1510 series"});
            this.ddlPrinter.Location = new System.Drawing.Point(526, 180);
            this.ddlPrinter.Name = "ddlPrinter";
            this.ddlPrinter.Size = new System.Drawing.Size(205, 21);
            this.ddlPrinter.TabIndex = 11;
            // 
            // lblPrinter
            // 
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Location = new System.Drawing.Point(410, 183);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(70, 13);
            this.lblPrinter.TabIndex = 59;
            this.lblPrinter.Text = "Select Printer";
            // 
            // txtAccountNumberLive
            // 
            this.txtAccountNumberLive.Location = new System.Drawing.Point(146, 109);
            this.txtAccountNumberLive.Name = "txtAccountNumberLive";
            this.txtAccountNumberLive.Size = new System.Drawing.Size(205, 20);
            this.txtAccountNumberLive.TabIndex = 4;
            // 
            // lblAccountNumberLive
            // 
            this.lblAccountNumberLive.AutoSize = true;
            this.lblAccountNumberLive.Location = new System.Drawing.Point(10, 110);
            this.lblAccountNumberLive.Name = "lblAccountNumberLive";
            this.lblAccountNumberLive.Size = new System.Drawing.Size(110, 13);
            this.lblAccountNumberLive.TabIndex = 56;
            this.lblAccountNumberLive.Text = "Live Account Number";
            // 
            // txtMeterNumberLive
            // 
            this.txtMeterNumberLive.Location = new System.Drawing.Point(146, 79);
            this.txtMeterNumberLive.Name = "txtMeterNumberLive";
            this.txtMeterNumberLive.Size = new System.Drawing.Size(205, 20);
            this.txtMeterNumberLive.TabIndex = 3;
            // 
            // lblMeterNumberLive
            // 
            this.lblMeterNumberLive.AutoSize = true;
            this.lblMeterNumberLive.Location = new System.Drawing.Point(13, 80);
            this.lblMeterNumberLive.Name = "lblMeterNumberLive";
            this.lblMeterNumberLive.Size = new System.Drawing.Size(97, 13);
            this.lblMeterNumberLive.TabIndex = 52;
            this.lblMeterNumberLive.Text = "Live Meter Number";
            // 
            // txtApiPassLive
            // 
            this.txtApiPassLive.Location = new System.Drawing.Point(146, 44);
            this.txtApiPassLive.Name = "txtApiPassLive";
            this.txtApiPassLive.Size = new System.Drawing.Size(205, 20);
            this.txtApiPassLive.TabIndex = 2;
            // 
            // lblAPIPassLive
            // 
            this.lblAPIPassLive.AutoSize = true;
            this.lblAPIPassLive.Location = new System.Drawing.Point(10, 42);
            this.lblAPIPassLive.Name = "lblAPIPassLive";
            this.lblAPIPassLive.Size = new System.Drawing.Size(96, 13);
            this.lblAPIPassLive.TabIndex = 49;
            this.lblAPIPassLive.Text = "Live API Password";
            // 
            // txtApiAccessKeyLive
            // 
            this.txtApiAccessKeyLive.Location = new System.Drawing.Point(146, 8);
            this.txtApiAccessKeyLive.Name = "txtApiAccessKeyLive";
            this.txtApiAccessKeyLive.Size = new System.Drawing.Size(205, 20);
            this.txtApiAccessKeyLive.TabIndex = 1;
            // 
            // lblApiAccessKeyLive
            // 
            this.lblApiAccessKeyLive.AutoSize = true;
            this.lblApiAccessKeyLive.Location = new System.Drawing.Point(10, 11);
            this.lblApiAccessKeyLive.Name = "lblApiAccessKeyLive";
            this.lblApiAccessKeyLive.Size = new System.Drawing.Size(106, 13);
            this.lblApiAccessKeyLive.TabIndex = 46;
            this.lblApiAccessKeyLive.Text = "Live API Access Key";
            // 
            // btnShipper
            // 
            this.btnShipper.Location = new System.Drawing.Point(240, 225);
            this.btnShipper.Name = "btnShipper";
            this.btnShipper.Size = new System.Drawing.Size(180, 29);
            this.btnShipper.TabIndex = 12;
            this.btnShipper.Text = "Edit Shipper Address";
            this.btnShipper.UseVisualStyleBackColor = true;
            this.btnShipper.Click += new System.EventHandler(this.btnShipper_Click);
            // 
            // lblAccountTestRequired
            // 
            this.lblAccountTestRequired.AutoSize = true;
            this.lblAccountTestRequired.ForeColor = System.Drawing.Color.Red;
            this.lblAccountTestRequired.Location = new System.Drawing.Point(732, 113);
            this.lblAccountTestRequired.Name = "lblAccountTestRequired";
            this.lblAccountTestRequired.Size = new System.Drawing.Size(50, 13);
            this.lblAccountTestRequired.TabIndex = 81;
            this.lblAccountTestRequired.Text = "Required";
            this.lblAccountTestRequired.Visible = false;
            // 
            // lblPswdTestRequired
            // 
            this.lblPswdTestRequired.AutoSize = true;
            this.lblPswdTestRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPswdTestRequired.Location = new System.Drawing.Point(732, 47);
            this.lblPswdTestRequired.Name = "lblPswdTestRequired";
            this.lblPswdTestRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPswdTestRequired.TabIndex = 80;
            this.lblPswdTestRequired.Text = "Required";
            this.lblPswdTestRequired.Visible = false;
            // 
            // lblAccessKeyTestRequired
            // 
            this.lblAccessKeyTestRequired.AutoSize = true;
            this.lblAccessKeyTestRequired.ForeColor = System.Drawing.Color.Red;
            this.lblAccessKeyTestRequired.Location = new System.Drawing.Point(732, 10);
            this.lblAccessKeyTestRequired.Name = "lblAccessKeyTestRequired";
            this.lblAccessKeyTestRequired.Size = new System.Drawing.Size(50, 13);
            this.lblAccessKeyTestRequired.TabIndex = 79;
            this.lblAccessKeyTestRequired.Text = "Required";
            this.lblAccessKeyTestRequired.Visible = false;
            // 
            // txtAccountNumberTest
            // 
            this.txtAccountNumberTest.Location = new System.Drawing.Point(526, 109);
            this.txtAccountNumberTest.Name = "txtAccountNumberTest";
            this.txtAccountNumberTest.Size = new System.Drawing.Size(205, 20);
            this.txtAccountNumberTest.TabIndex = 9;
            // 
            // lblAccountNumberTest
            // 
            this.lblAccountNumberTest.AutoSize = true;
            this.lblAccountNumberTest.Location = new System.Drawing.Point(407, 113);
            this.lblAccountNumberTest.Name = "lblAccountNumberTest";
            this.lblAccountNumberTest.Size = new System.Drawing.Size(111, 13);
            this.lblAccountNumberTest.TabIndex = 78;
            this.lblAccountNumberTest.Text = "Test Account Number";
            // 
            // txtMeterNumberTest
            // 
            this.txtMeterNumberTest.Location = new System.Drawing.Point(526, 79);
            this.txtMeterNumberTest.Name = "txtMeterNumberTest";
            this.txtMeterNumberTest.Size = new System.Drawing.Size(205, 20);
            this.txtMeterNumberTest.TabIndex = 8;
            // 
            // lblMeterNumberTest
            // 
            this.lblMeterNumberTest.AutoSize = true;
            this.lblMeterNumberTest.Location = new System.Drawing.Point(410, 83);
            this.lblMeterNumberTest.Name = "lblMeterNumberTest";
            this.lblMeterNumberTest.Size = new System.Drawing.Size(98, 13);
            this.lblMeterNumberTest.TabIndex = 77;
            this.lblMeterNumberTest.Text = "Test Meter Number";
            // 
            // txtApiPassTest
            // 
            this.txtApiPassTest.Location = new System.Drawing.Point(526, 44);
            this.txtApiPassTest.Name = "txtApiPassTest";
            this.txtApiPassTest.Size = new System.Drawing.Size(205, 20);
            this.txtApiPassTest.TabIndex = 7;
            // 
            // lblAPIPassTest
            // 
            this.lblAPIPassTest.AutoSize = true;
            this.lblAPIPassTest.Location = new System.Drawing.Point(407, 45);
            this.lblAPIPassTest.Name = "lblAPIPassTest";
            this.lblAPIPassTest.Size = new System.Drawing.Size(97, 13);
            this.lblAPIPassTest.TabIndex = 75;
            this.lblAPIPassTest.Text = "Test API Password";
            // 
            // txtApiAccessKeyTest
            // 
            this.txtApiAccessKeyTest.Location = new System.Drawing.Point(526, 8);
            this.txtApiAccessKeyTest.Name = "txtApiAccessKeyTest";
            this.txtApiAccessKeyTest.Size = new System.Drawing.Size(205, 20);
            this.txtApiAccessKeyTest.TabIndex = 6;
            // 
            // lblAccessKeyTest
            // 
            this.lblAccessKeyTest.AutoSize = true;
            this.lblAccessKeyTest.Location = new System.Drawing.Point(407, 9);
            this.lblAccessKeyTest.Name = "lblAccessKeyTest";
            this.lblAccessKeyTest.Size = new System.Drawing.Size(107, 13);
            this.lblAccessKeyTest.TabIndex = 72;
            this.lblAccessKeyTest.Text = "Test API Access Key";
            // 
            // lblMeterTestRequired
            // 
            this.lblMeterTestRequired.AutoSize = true;
            this.lblMeterTestRequired.ForeColor = System.Drawing.Color.Red;
            this.lblMeterTestRequired.Location = new System.Drawing.Point(732, 82);
            this.lblMeterTestRequired.Name = "lblMeterTestRequired";
            this.lblMeterTestRequired.Size = new System.Drawing.Size(50, 13);
            this.lblMeterTestRequired.TabIndex = 82;
            this.lblMeterTestRequired.Text = "Required";
            this.lblMeterTestRequired.Visible = false;
            // 
            // ddlStockLabel
            // 
            this.ddlStockLabel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlStockLabel.FormattingEnabled = true;
            this.ddlStockLabel.Items.AddRange(new object[] {
            "ZDesigner GC420d(EPL)",
            "Brother DCP-1510 series"});
            this.ddlStockLabel.Location = new System.Drawing.Point(146, 140);
            this.ddlStockLabel.Name = "ddlStockLabel";
            this.ddlStockLabel.Size = new System.Drawing.Size(205, 21);
            this.ddlStockLabel.TabIndex = 83;
            // 
            // lblLabelStock
            // 
            this.lblLabelStock.AutoSize = true;
            this.lblLabelStock.Location = new System.Drawing.Point(10, 143);
            this.lblLabelStock.Name = "lblLabelStock";
            this.lblLabelStock.Size = new System.Drawing.Size(130, 13);
            this.lblLabelStock.TabIndex = 84;
            this.lblLabelStock.Text = "Select Stock Label Printer";
            // 
            // chkIsLive
            // 
            this.chkIsLive.AutoSize = true;
            this.chkIsLive.Location = new System.Drawing.Point(146, 179);
            this.chkIsLive.Name = "chkIsLive";
            this.chkIsLive.Size = new System.Drawing.Size(100, 17);
            this.chkIsLive.TabIndex = 85;
            this.chkIsLive.Text = "Is Live Account";
            this.chkIsLive.UseVisualStyleBackColor = true;
            // 
            // btnVoidShipment
            // 
            this.btnVoidShipment.Location = new System.Drawing.Point(73, 225);
            this.btnVoidShipment.Name = "btnVoidShipment";
            this.btnVoidShipment.Size = new System.Drawing.Size(161, 29);
            this.btnVoidShipment.TabIndex = 86;
            this.btnVoidShipment.Text = "Void Shipment";
            this.btnVoidShipment.UseVisualStyleBackColor = true;
            this.btnVoidShipment.Click += new System.EventHandler(this.btnVoidShipment_Click);
            // 
            // FedExSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 490);
            this.Controls.Add(this.btnVoidShipment);
            this.Controls.Add(this.chkIsLive);
            this.Controls.Add(this.ddlStockLabel);
            this.Controls.Add(this.lblLabelStock);
            this.Controls.Add(this.lblMeterTestRequired);
            this.Controls.Add(this.lblAccountTestRequired);
            this.Controls.Add(this.lblPswdTestRequired);
            this.Controls.Add(this.lblAccessKeyTestRequired);
            this.Controls.Add(this.txtAccountNumberTest);
            this.Controls.Add(this.lblAccountNumberTest);
            this.Controls.Add(this.txtMeterNumberTest);
            this.Controls.Add(this.lblMeterNumberTest);
            this.Controls.Add(this.txtApiPassTest);
            this.Controls.Add(this.lblAPIPassTest);
            this.Controls.Add(this.txtApiAccessKeyTest);
            this.Controls.Add(this.lblAccessKeyTest);
            this.Controls.Add(this.btnShipper);
            this.Controls.Add(this.lblWeightInvalid);
            this.Controls.Add(this.lblAccountLiveRequired);
            this.Controls.Add(this.lblMeterLiveRequired);
            this.Controls.Add(this.lblPswdLiveRequired);
            this.Controls.Add(this.lblAccessKeyLiveRequired);
            this.Controls.Add(this.lblWeightRequired);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpBoxShippingSettings);
            this.Controls.Add(this.txtDefaultWeight);
            this.Controls.Add(this.lblDefaultWeight);
            this.Controls.Add(this.ddlPrinter);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.txtAccountNumberLive);
            this.Controls.Add(this.lblAccountNumberLive);
            this.Controls.Add(this.txtMeterNumberLive);
            this.Controls.Add(this.lblMeterNumberLive);
            this.Controls.Add(this.txtApiPassLive);
            this.Controls.Add(this.lblAPIPassLive);
            this.Controls.Add(this.txtApiAccessKeyLive);
            this.Controls.Add(this.lblApiAccessKeyLive);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FedExSettings";
            this.Text = "FedEx Settings";
            this.grpBoxShippingSettings.ResumeLayout(false);
            this.grpBoxShippingSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWeightInvalid;
        private System.Windows.Forms.Label lblAccountLiveRequired;
        private System.Windows.Forms.Label lblMeterLiveRequired;
        private System.Windows.Forms.Label lblPswdLiveRequired;
        private System.Windows.Forms.Label lblAccessKeyLiveRequired;
        private System.Windows.Forms.Label lblWeightRequired;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grpBoxShippingSettings;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSaveClose;
        private System.Windows.Forms.DataGridView grdMappedServices;
        private System.Windows.Forms.TextBox txtDefaultWeight;
        private System.Windows.Forms.Label lblDefaultWeight;
        private System.Windows.Forms.ComboBox ddlPrinter;
        private System.Windows.Forms.Label lblPrinter;
        private System.Windows.Forms.TextBox txtAccountNumberLive;
        private System.Windows.Forms.Label lblAccountNumberLive;
        private System.Windows.Forms.TextBox txtMeterNumberLive;
        private System.Windows.Forms.Label lblMeterNumberLive;
        private System.Windows.Forms.TextBox txtApiPassLive;
        private System.Windows.Forms.Label lblAPIPassLive;
        private System.Windows.Forms.TextBox txtApiAccessKeyLive;
        private System.Windows.Forms.Label lblApiAccessKeyLive;
        private System.Windows.Forms.Button btnShipper;
        private System.Windows.Forms.Label lblAccountTestRequired;
        private System.Windows.Forms.Label lblPswdTestRequired;
        private System.Windows.Forms.Label lblAccessKeyTestRequired;
        private System.Windows.Forms.TextBox txtAccountNumberTest;
        private System.Windows.Forms.Label lblAccountNumberTest;
        private System.Windows.Forms.TextBox txtMeterNumberTest;
        private System.Windows.Forms.Label lblMeterNumberTest;
        private System.Windows.Forms.TextBox txtApiPassTest;
        private System.Windows.Forms.Label lblAPIPassTest;
        private System.Windows.Forms.TextBox txtApiAccessKeyTest;
        private System.Windows.Forms.Label lblAccessKeyTest;
        private System.Windows.Forms.Label lblMeterTestRequired;
        private System.Windows.Forms.ComboBox ddlStockLabel;
        private System.Windows.Forms.Label lblLabelStock;
        private System.Windows.Forms.CheckBox chkIsLive;
        private System.Windows.Forms.Button btnVoidShipment;

    }
}