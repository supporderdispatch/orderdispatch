﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class EmailConfiguration : Form
    {
        public EmailConfiguration()
        {
            InitializeComponent();
            BindTemplateGrid();
        }

        private void btnAdditonalEmail_Click(object sender, EventArgs e)
        {
            Form frmAdditionalEmailAccount = Application.OpenForms["AdditionalEmailAccount"];
            if (frmAdditionalEmailAccount == null)
            {
                AdditionalEmailAccount obj = new AdditionalEmailAccount();
                obj.Show();
            }
        }

        private void btnSendTestMail_Click(object sender, EventArgs e)
        {
            Form frmTemplateEditor = Application.OpenForms["TemplateEditor"];
            if (frmTemplateEditor == null)
            {
                TemplateEditor obj = new TemplateEditor();
                obj.Show();
            }
        }
        private void BindTemplateGrid()
        {
            try
            {
                grdTemplates.DataSource = GlobalVariablesAndMethods.GetEmailTemplates();
                grdTemplates.AllowUserToAddRows = false;
                grdTemplates.Columns["TemplateText"].Width = 705;
                grdTemplates.Columns["Is Active"].Width = 100;
                for (int i = 0; i < grdTemplates.Columns.Count; i++)
                {
                    grdTemplates.Columns[i].ReadOnly = true;
                    if (grdTemplates.Columns[i].Name.ToUpper() != "TEMPLATETEXT" && grdTemplates.Columns[i].Name.ToUpper() != "IS ACTIVE")
                    {
                        grdTemplates.Columns[i].Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occrred\n" + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void grdTemplates_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!(e.ColumnIndex > 0))
            {
                return;
            }
            string strColumnName = grdTemplates.Columns[e.ColumnIndex].Name.ToUpper();
            string strCellValue = grdTemplates.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            Guid ID = new Guid(grdTemplates.Rows[e.RowIndex].Cells[0].Value.ToString());
            string strValueOfStatusCell = Convert.ToString(grdTemplates.Rows[e.RowIndex].Cells[2].Value);
           // string str
            if (strColumnName == "IS ACTIVE")
            {
                bool IsActiveStatus = strCellValue == "Active" ? false : true;
                string strStatus = strCellValue == "Active" ? "Disable" : "Enable";
                DialogResult ConfirmTemplateStatus = MessageBox.Show("Are you sure, you want to " + strStatus + " selected template?", "Update Template Status", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (ConfirmTemplateStatus == DialogResult.Yes)
                {
                    UpdateTemplate(ID, IsActiveStatus, "ToggleActiveStatus");
                    BindTemplateGrid();
                }
            }
            else if (strColumnName == "TEMPLATETEXT" && strValueOfStatusCell.ToUpper() == "ACTIVE")
            {
                if (grdTemplates.Rows[e.RowIndex].Cells[1].Value.ToString().ToUpper()=="RESEND INVOICE TEMPLATE")
                {
                    Form frmEmailMessageTemplateSettings = Application.OpenForms["EmailMessageTemplateSettings"];
                    
                    if (frmEmailMessageTemplateSettings == null)
                    {
                        EmailMessageTemplateSettings objEmailInvoiceForMessage = new EmailMessageTemplateSettings(ID);
                        objEmailInvoiceForMessage.Show();
                    }
                         
                }
                else
                {
                    Form frmTemplateEditor = Application.OpenForms["TemplateEditor"];
                    if (frmTemplateEditor == null)
                    {
                        TemplateEditor obj = new TemplateEditor(ID);
                        obj.Show();
                    }
                }
                


               
            }
        }

        private void grdTemplates_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewRow row = grdTemplates.Rows[e.RowIndex];
            string strIsActiveValue = Convert.ToString(row.Cells["Is Active"].Value);
            e.CellStyle.BackColor = strIsActiveValue.ToUpper() == "ACTIVE" ? Color.Green : Color.Red;
        }

        public static void UpdateTemplate(Guid ID, bool IsActiveStatus, string strMode)
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            try
            {
                SqlParameter[] objParam = new SqlParameter[3];

                objParam[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = ID;

                objParam[1] = new SqlParameter("@Status", SqlDbType.Bit);
                objParam[1].Value = IsActiveStatus;

                objParam[2] = new SqlParameter("@Mode", SqlDbType.VarChar, 30);
                objParam[2].Value = strMode;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateTemplate", objParam);
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Error occurred while updating template&&" + ex.Message);
            }
        }
    }
}
