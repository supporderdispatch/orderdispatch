﻿namespace ODM.Appliction
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTotalOpenOrders = new System.Windows.Forms.Label();
            this.lblPicked = new System.Windows.Forms.Label();
            this.lblPickedSingle = new System.Windows.Forms.Label();
            this.lblPickedSingleUrgent = new System.Windows.Forms.Label();
            this.lblPickedsinglenotUrgent = new System.Windows.Forms.Label();
            this.lblPickedMultiple = new System.Windows.Forms.Label();
            this.lblPickedMultipleUrgent = new System.Windows.Forms.Label();
            this.lblPickedMultipleNotUrgent = new System.Windows.Forms.Label();
            this.lblUnPickedMultipleNotUrgent = new System.Windows.Forms.Label();
            this.lblUnPickedMultipleUrgent = new System.Windows.Forms.Label();
            this.lblUnPickedMultiple = new System.Windows.Forms.Label();
            this.lblUnPickedsinglenotUrgent = new System.Windows.Forms.Label();
            this.lblUnPickedSingleUrgent = new System.Windows.Forms.Label();
            this.lblUnPickedSingle = new System.Windows.Forms.Label();
            this.lblUnPicked = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.chkAutoRefresh = new System.Windows.Forms.CheckBox();
            this.cmbAutoRefreshInterval = new System.Windows.Forms.ComboBox();
            this.lblLastAutoUpdate = new System.Windows.Forms.Label();
            this.PicLoading = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lblSkuCount = new System.Windows.Forms.Label();
            this.lblItemCount = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(52, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Total open Orders";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(73, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Picked";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(102, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Single";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(95, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Multiple";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(155, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Urgent ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(135, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Not-Urgent ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(135, 249);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Not-Urgent ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(155, 228);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Urgent ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(207, 55);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 13);
            this.label17.TabIndex = 18;
            this.label17.Text = ":";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(207, 88);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = ":";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(207, 117);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 13);
            this.label19.TabIndex = 20;
            this.label19.Text = ":";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(207, 140);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = ":";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(207, 159);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = ":";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(207, 198);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(11, 13);
            this.label22.TabIndex = 23;
            this.label22.Text = ":";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(207, 228);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 13);
            this.label23.TabIndex = 24;
            this.label23.Text = ":";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(207, 249);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(11, 13);
            this.label24.TabIndex = 25;
            this.label24.Text = ":";
            // 
            // label10
            // 
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Location = new System.Drawing.Point(98, 218);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(180, 2);
            this.label10.TabIndex = 27;
            this.label10.Text = "label10";
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(97, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(180, 2);
            this.label3.TabIndex = 28;
            this.label3.Text = "label3";
            // 
            // lblTotalOpenOrders
            // 
            this.lblTotalOpenOrders.AutoSize = true;
            this.lblTotalOpenOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalOpenOrders.Location = new System.Drawing.Point(253, 51);
            this.lblTotalOpenOrders.Name = "lblTotalOpenOrders";
            this.lblTotalOpenOrders.Size = new System.Drawing.Size(32, 16);
            this.lblTotalOpenOrders.TabIndex = 29;
            this.lblTotalOpenOrders.Text = "000";
            // 
            // lblPicked
            // 
            this.lblPicked.AutoSize = true;
            this.lblPicked.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPicked.Location = new System.Drawing.Point(256, 84);
            this.lblPicked.Name = "lblPicked";
            this.lblPicked.Size = new System.Drawing.Size(0, 18);
            this.lblPicked.TabIndex = 30;
            // 
            // lblPickedSingle
            // 
            this.lblPickedSingle.AutoSize = true;
            this.lblPickedSingle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPickedSingle.Location = new System.Drawing.Point(259, 114);
            this.lblPickedSingle.Name = "lblPickedSingle";
            this.lblPickedSingle.Size = new System.Drawing.Size(0, 16);
            this.lblPickedSingle.TabIndex = 31;
            // 
            // lblPickedSingleUrgent
            // 
            this.lblPickedSingleUrgent.AutoSize = true;
            this.lblPickedSingleUrgent.Location = new System.Drawing.Point(259, 140);
            this.lblPickedSingleUrgent.Name = "lblPickedSingleUrgent";
            this.lblPickedSingleUrgent.Size = new System.Drawing.Size(0, 13);
            this.lblPickedSingleUrgent.TabIndex = 32;
            // 
            // lblPickedsinglenotUrgent
            // 
            this.lblPickedsinglenotUrgent.AutoSize = true;
            this.lblPickedsinglenotUrgent.Location = new System.Drawing.Point(259, 164);
            this.lblPickedsinglenotUrgent.Name = "lblPickedsinglenotUrgent";
            this.lblPickedsinglenotUrgent.Size = new System.Drawing.Size(0, 13);
            this.lblPickedsinglenotUrgent.TabIndex = 33;
            // 
            // lblPickedMultiple
            // 
            this.lblPickedMultiple.AutoSize = true;
            this.lblPickedMultiple.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPickedMultiple.Location = new System.Drawing.Point(259, 196);
            this.lblPickedMultiple.Name = "lblPickedMultiple";
            this.lblPickedMultiple.Size = new System.Drawing.Size(0, 16);
            this.lblPickedMultiple.TabIndex = 34;
            // 
            // lblPickedMultipleUrgent
            // 
            this.lblPickedMultipleUrgent.AutoSize = true;
            this.lblPickedMultipleUrgent.Location = new System.Drawing.Point(259, 228);
            this.lblPickedMultipleUrgent.Name = "lblPickedMultipleUrgent";
            this.lblPickedMultipleUrgent.Size = new System.Drawing.Size(0, 13);
            this.lblPickedMultipleUrgent.TabIndex = 35;
            // 
            // lblPickedMultipleNotUrgent
            // 
            this.lblPickedMultipleNotUrgent.AutoSize = true;
            this.lblPickedMultipleNotUrgent.Location = new System.Drawing.Point(259, 249);
            this.lblPickedMultipleNotUrgent.Name = "lblPickedMultipleNotUrgent";
            this.lblPickedMultipleNotUrgent.Size = new System.Drawing.Size(0, 13);
            this.lblPickedMultipleNotUrgent.TabIndex = 36;
            // 
            // lblUnPickedMultipleNotUrgent
            // 
            this.lblUnPickedMultipleNotUrgent.AutoSize = true;
            this.lblUnPickedMultipleNotUrgent.Location = new System.Drawing.Point(493, 249);
            this.lblUnPickedMultipleNotUrgent.Name = "lblUnPickedMultipleNotUrgent";
            this.lblUnPickedMultipleNotUrgent.Size = new System.Drawing.Size(0, 13);
            this.lblUnPickedMultipleNotUrgent.TabIndex = 59;
            // 
            // lblUnPickedMultipleUrgent
            // 
            this.lblUnPickedMultipleUrgent.AutoSize = true;
            this.lblUnPickedMultipleUrgent.Location = new System.Drawing.Point(493, 228);
            this.lblUnPickedMultipleUrgent.Name = "lblUnPickedMultipleUrgent";
            this.lblUnPickedMultipleUrgent.Size = new System.Drawing.Size(0, 13);
            this.lblUnPickedMultipleUrgent.TabIndex = 58;
            // 
            // lblUnPickedMultiple
            // 
            this.lblUnPickedMultiple.AutoSize = true;
            this.lblUnPickedMultiple.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnPickedMultiple.Location = new System.Drawing.Point(493, 196);
            this.lblUnPickedMultiple.Name = "lblUnPickedMultiple";
            this.lblUnPickedMultiple.Size = new System.Drawing.Size(0, 16);
            this.lblUnPickedMultiple.TabIndex = 57;
            // 
            // lblUnPickedsinglenotUrgent
            // 
            this.lblUnPickedsinglenotUrgent.AutoSize = true;
            this.lblUnPickedsinglenotUrgent.Location = new System.Drawing.Point(493, 164);
            this.lblUnPickedsinglenotUrgent.Name = "lblUnPickedsinglenotUrgent";
            this.lblUnPickedsinglenotUrgent.Size = new System.Drawing.Size(0, 13);
            this.lblUnPickedsinglenotUrgent.TabIndex = 56;
            // 
            // lblUnPickedSingleUrgent
            // 
            this.lblUnPickedSingleUrgent.AutoSize = true;
            this.lblUnPickedSingleUrgent.Location = new System.Drawing.Point(493, 140);
            this.lblUnPickedSingleUrgent.Name = "lblUnPickedSingleUrgent";
            this.lblUnPickedSingleUrgent.Size = new System.Drawing.Size(0, 13);
            this.lblUnPickedSingleUrgent.TabIndex = 55;
            // 
            // lblUnPickedSingle
            // 
            this.lblUnPickedSingle.AutoSize = true;
            this.lblUnPickedSingle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnPickedSingle.Location = new System.Drawing.Point(493, 114);
            this.lblUnPickedSingle.Name = "lblUnPickedSingle";
            this.lblUnPickedSingle.Size = new System.Drawing.Size(0, 16);
            this.lblUnPickedSingle.TabIndex = 54;
            // 
            // lblUnPicked
            // 
            this.lblUnPicked.AutoSize = true;
            this.lblUnPicked.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnPicked.Location = new System.Drawing.Point(490, 84);
            this.lblUnPicked.Name = "lblUnPicked";
            this.lblUnPicked.Size = new System.Drawing.Size(0, 18);
            this.lblUnPicked.TabIndex = 53;
            // 
            // label34
            // 
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label34.Location = new System.Drawing.Point(331, 133);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(180, 2);
            this.label34.TabIndex = 52;
            this.label34.Text = "label34";
            // 
            // label35
            // 
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label35.Location = new System.Drawing.Point(332, 218);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(180, 2);
            this.label35.TabIndex = 51;
            this.label35.Text = "label35";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(441, 249);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(11, 13);
            this.label36.TabIndex = 50;
            this.label36.Text = ":";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(441, 228);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(11, 13);
            this.label37.TabIndex = 49;
            this.label37.Text = ":";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(441, 198);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(11, 13);
            this.label38.TabIndex = 48;
            this.label38.Text = ":";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(441, 159);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(11, 13);
            this.label39.TabIndex = 47;
            this.label39.Text = ":";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(441, 140);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(11, 13);
            this.label40.TabIndex = 46;
            this.label40.Text = ":";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(441, 117);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(11, 13);
            this.label41.TabIndex = 45;
            this.label41.Text = ":";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(441, 88);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(11, 13);
            this.label42.TabIndex = 44;
            this.label42.Text = ":";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(369, 249);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(62, 13);
            this.label43.TabIndex = 43;
            this.label43.Text = "Not-Urgent ";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(389, 228);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(42, 13);
            this.label44.TabIndex = 42;
            this.label44.Text = "Urgent ";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(369, 164);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(62, 13);
            this.label45.TabIndex = 41;
            this.label45.Text = "Not-Urgent ";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(389, 140);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(42, 13);
            this.label46.TabIndex = 40;
            this.label46.Text = "Urgent ";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(328, 196);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(54, 16);
            this.label47.TabIndex = 39;
            this.label47.Text = "Multiple";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(336, 114);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(46, 16);
            this.label48.TabIndex = 38;
            this.label48.Text = "Single";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(307, 84);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(77, 18);
            this.label49.TabIndex = 37;
            this.label49.Text = "Un-Picked";
            // 
            // label16
            // 
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Location = new System.Drawing.Point(299, 84);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(1, 200);
            this.label16.TabIndex = 60;
            this.label16.Text = "label16";
            // 
            // chkAutoRefresh
            // 
            this.chkAutoRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkAutoRefresh.AutoSize = true;
            this.chkAutoRefresh.Checked = true;
            this.chkAutoRefresh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoRefresh.Location = new System.Drawing.Point(424, 12);
            this.chkAutoRefresh.Name = "chkAutoRefresh";
            this.chkAutoRefresh.Size = new System.Drawing.Size(163, 17);
            this.chkAutoRefresh.TabIndex = 61;
            this.chkAutoRefresh.Text = "Auto Refresh    Interval (min):";
            this.chkAutoRefresh.UseVisualStyleBackColor = true;
            // 
            // cmbAutoRefreshInterval
            // 
            this.cmbAutoRefreshInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAutoRefreshInterval.FormattingEnabled = true;
            this.cmbAutoRefreshInterval.Location = new System.Drawing.Point(580, 8);
            this.cmbAutoRefreshInterval.Name = "cmbAutoRefreshInterval";
            this.cmbAutoRefreshInterval.Size = new System.Drawing.Size(38, 21);
            this.cmbAutoRefreshInterval.TabIndex = 62;
            // 
            // lblLastAutoUpdate
            // 
            this.lblLastAutoUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastAutoUpdate.AutoSize = true;
            this.lblLastAutoUpdate.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblLastAutoUpdate.Location = new System.Drawing.Point(421, 32);
            this.lblLastAutoUpdate.Name = "lblLastAutoUpdate";
            this.lblLastAutoUpdate.Size = new System.Drawing.Size(105, 13);
            this.lblLastAutoUpdate.TabIndex = 63;
            this.lblLastAutoUpdate.Text = "last auto update time";
            // 
            // PicLoading
            // 
            this.PicLoading.Image = ((System.Drawing.Image)(resources.GetObject("PicLoading.Image")));
            this.PicLoading.Location = new System.Drawing.Point(398, 10);
            this.PicLoading.Name = "PicLoading";
            this.PicLoading.Size = new System.Drawing.Size(20, 20);
            this.PicLoading.TabIndex = 64;
            this.PicLoading.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(77, 330);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 18);
            this.label12.TabIndex = 65;
            this.label12.Text = "Sku :";
            // 
            // lblSkuCount
            // 
            this.lblSkuCount.AutoSize = true;
            this.lblSkuCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSkuCount.Location = new System.Drawing.Point(120, 330);
            this.lblSkuCount.Name = "lblSkuCount";
            this.lblSkuCount.Size = new System.Drawing.Size(0, 18);
            this.lblSkuCount.TabIndex = 67;
            // 
            // lblItemCount
            // 
            this.lblItemCount.AutoSize = true;
            this.lblItemCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemCount.Location = new System.Drawing.Point(217, 330);
            this.lblItemCount.Name = "lblItemCount";
            this.lblItemCount.Size = new System.Drawing.Size(0, 18);
            this.lblItemCount.TabIndex = 70;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(165, 330);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 18);
            this.label15.TabIndex = 68;
            this.label15.Text = "Items :";
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 374);
            this.Controls.Add(this.lblItemCount);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.lblSkuCount);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.PicLoading);
            this.Controls.Add(this.lblLastAutoUpdate);
            this.Controls.Add(this.cmbAutoRefreshInterval);
            this.Controls.Add(this.chkAutoRefresh);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblUnPickedMultipleNotUrgent);
            this.Controls.Add(this.lblUnPickedMultipleUrgent);
            this.Controls.Add(this.lblUnPickedMultiple);
            this.Controls.Add(this.lblUnPickedsinglenotUrgent);
            this.Controls.Add(this.lblUnPickedSingleUrgent);
            this.Controls.Add(this.lblUnPickedSingle);
            this.Controls.Add(this.lblUnPicked);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.lblPickedMultipleNotUrgent);
            this.Controls.Add(this.lblPickedMultipleUrgent);
            this.Controls.Add(this.lblPickedMultiple);
            this.Controls.Add(this.lblPickedsinglenotUrgent);
            this.Controls.Add(this.lblPickedSingleUrgent);
            this.Controls.Add(this.lblPickedSingle);
            this.Controls.Add(this.lblPicked);
            this.Controls.Add(this.lblTotalOpenOrders);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard";
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTotalOpenOrders;
        private System.Windows.Forms.Label lblPicked;
        private System.Windows.Forms.Label lblPickedSingle;
        private System.Windows.Forms.Label lblPickedSingleUrgent;
        private System.Windows.Forms.Label lblPickedsinglenotUrgent;
        private System.Windows.Forms.Label lblPickedMultiple;
        private System.Windows.Forms.Label lblPickedMultipleUrgent;
        private System.Windows.Forms.Label lblPickedMultipleNotUrgent;
        private System.Windows.Forms.Label lblUnPickedMultipleNotUrgent;
        private System.Windows.Forms.Label lblUnPickedMultipleUrgent;
        private System.Windows.Forms.Label lblUnPickedMultiple;
        private System.Windows.Forms.Label lblUnPickedsinglenotUrgent;
        private System.Windows.Forms.Label lblUnPickedSingleUrgent;
        private System.Windows.Forms.Label lblUnPickedSingle;
        private System.Windows.Forms.Label lblUnPicked;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chkAutoRefresh;
        private System.Windows.Forms.ComboBox cmbAutoRefreshInterval;
        private System.Windows.Forms.Label lblLastAutoUpdate;
        private System.Windows.Forms.PictureBox PicLoading;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblSkuCount;
        private System.Windows.Forms.Label lblItemCount;
        private System.Windows.Forms.Label label15;


    }
}