﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class SenderDetails : Form
    {
        #region "Global Variables"
        string _CourierName = "";
        string _CourierID = "";
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        #endregion

        #region "Constructors"
        public SenderDetails()
        {
            InitializeComponent();
        }
        public SenderDetails(string strCourierName, string strCourierID)
        {
            InitializeComponent();
            _CourierName = strCourierName;
            _CourierID = strCourierID;
            PopulateControls();
        }
        #endregion

        #region "Events"
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveSenderDetails();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region "Methods"
        private void PopulateControls()
        {
            try
            {
                DataTable ObjdtCourier = new DataTable();
                ObjdtCourier = GlobalVar.GetCourierDetailsByNameOrId(_CourierName, _CourierID);
                if (ObjdtCourier.Rows.Count > 0)
                {
                    BindCountryDDL();
                    txtName.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderName"]);
                    txtPhone.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderPhoneNumber"]);
                    txtAddress1.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderAddress1"]);
                    txtAddress2.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderAddress2"]);
                    txtTown.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderAddressTown"]);
                    txtPostCode.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderPostCode"]);
                    txtCompany.Text = Convert.ToString(ObjdtCourier.Rows[0]["SenderCompany"]);
                    ddlCountry.SelectedValue = Convert.ToString(ObjdtCourier.Rows[0]["SenderAddressCountryID"]);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void BindCountryDDL()
        {
            try
            {
                ddlCountry.ValueMember = "CountryId";
                ddlCountry.DisplayMember = "CountryName";
                ddlCountry.DataSource = GlobalVar.GetAllCountries();
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void SaveSenderDetails()
        {
            try
            {
                SqlConnection connection = new SqlConnection();
                if (ValidateControls())
                {
                    Guid CountryID = new Guid(ddlCountry.SelectedValue.ToString());
                    connection.ConnectionString = MakeConnection.GetConnection();
                    connection.Open();

                    SqlParameter[] objParam = new SqlParameter[8];

                    objParam[0] = new SqlParameter("@SenderName", SqlDbType.VarChar, 100);
                    objParam[0].Value = txtName.Text;

                    objParam[1] = new SqlParameter("@SenderPhoneNumber", SqlDbType.VarChar, 100);
                    objParam[1].Value = txtPhone.Text;

                    objParam[2] = new SqlParameter("@SenderAddress1", SqlDbType.VarChar, 100);
                    objParam[2].Value = txtAddress1.Text;

                    objParam[3] = new SqlParameter("@SenderAddress2", SqlDbType.VarChar, 100);
                    objParam[3].Value = txtAddress2.Text;

                    objParam[4] = new SqlParameter("@SenderAddressTown", SqlDbType.VarChar, 100);
                    objParam[4].Value = txtTown.Text;

                    objParam[5] = new SqlParameter("@PostCode", SqlDbType.VarChar, 50);
                    objParam[5].Value = txtPostCode.Text;

                    objParam[6] = new SqlParameter("@SenderCompany", SqlDbType.VarChar, 100);
                    objParam[6].Value = txtCompany.Text;

                    objParam[7] = new SqlParameter("@SenderAddressCounty", SqlDbType.UniqueIdentifier);
                    objParam[7].Value = CountryID;

                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateSenderDetails", objParam);
                    this.Close();
                    MessageBox.Show("Sender Details Saved Sucessfully");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool ValidateControls()
        {
            bool IsValid = true;
            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                lblNameRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblNameRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPhone.Text))
            {
                lblPhoneRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPhoneRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAddress1.Text))
            {
                lblAddress1Required.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAddress1Required.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAddress2.Text))
            {
                lblAddress2Required.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAddress2Required.Visible = false;
            }
            
            if (string.IsNullOrWhiteSpace(txtTown.Text))
            {
                lblTownRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblTownRequired.Visible = false;
            }
            
            if (string.IsNullOrWhiteSpace(txtPostCode.Text))
            {
                lblPostCodeRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPostCodeRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtCompany.Text))
            {
                lblCompanyRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblCompanyRequired.Visible = false;
            }
            return IsValid;
        }
        #endregion
    }
}
