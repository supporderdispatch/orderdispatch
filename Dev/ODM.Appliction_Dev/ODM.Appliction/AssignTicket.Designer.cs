﻿namespace ODM.Appliction
{
    partial class AssignTicket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAssignMessage = new System.Windows.Forms.Label();
            this.ddlFolders = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblAssignMessage
            // 
            this.lblAssignMessage.AutoSize = true;
            this.lblAssignMessage.Location = new System.Drawing.Point(47, 49);
            this.lblAssignMessage.Name = "lblAssignMessage";
            this.lblAssignMessage.Size = new System.Drawing.Size(106, 13);
            this.lblAssignMessage.TabIndex = 0;
            this.lblAssignMessage.Text = "Assign Message To :";
            // 
            // ddlFolders
            // 
            this.ddlFolders.FormattingEnabled = true;
            this.ddlFolders.Location = new System.Drawing.Point(179, 45);
            this.ddlFolders.Name = "ddlFolders";
            this.ddlFolders.Size = new System.Drawing.Size(193, 21);
            this.ddlFolders.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(120, 93);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(273, 93);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // AssignTicket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 128);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ddlFolders);
            this.Controls.Add(this.lblAssignMessage);
            this.Name = "AssignTicket";
            this.Text = "AssignTicket";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAssignMessage;
        private System.Windows.Forms.ComboBox ddlFolders;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}