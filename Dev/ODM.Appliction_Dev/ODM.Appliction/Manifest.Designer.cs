﻿namespace ODM.Appliction
{
    partial class Manifest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Manifest));
            this.grdManifest = new System.Windows.Forms.DataGridView();
            this.grdSummary = new System.Windows.Forms.DataGridView();
            this.btnRefress = new System.Windows.Forms.Button();
            this.btnVoidAll = new System.Windows.Forms.Button();
            this.btnManifest = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblMsg = new System.Windows.Forms.Label();
            this.btnPrintPendingLabel = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblRequired = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdManifest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // grdManifest
            // 
            this.grdManifest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdManifest.Location = new System.Drawing.Point(3, 53);
            this.grdManifest.Name = "grdManifest";
            this.grdManifest.Size = new System.Drawing.Size(860, 245);
            this.grdManifest.TabIndex = 0;
            this.grdManifest.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdManifest_CellClick);
            // 
            // grdSummary
            // 
            this.grdSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSummary.Location = new System.Drawing.Point(3, 313);
            this.grdSummary.Name = "grdSummary";
            this.grdSummary.Size = new System.Drawing.Size(860, 149);
            this.grdSummary.TabIndex = 1;
            // 
            // btnRefress
            // 
            this.btnRefress.Location = new System.Drawing.Point(447, 20);
            this.btnRefress.Name = "btnRefress";
            this.btnRefress.Size = new System.Drawing.Size(99, 25);
            this.btnRefress.TabIndex = 2;
            this.btnRefress.Text = "Refresh";
            this.btnRefress.UseVisualStyleBackColor = true;
            this.btnRefress.Click += new System.EventHandler(this.btnRefress_Click);
            // 
            // btnVoidAll
            // 
            this.btnVoidAll.Location = new System.Drawing.Point(552, 20);
            this.btnVoidAll.Name = "btnVoidAll";
            this.btnVoidAll.Size = new System.Drawing.Size(99, 25);
            this.btnVoidAll.TabIndex = 3;
            this.btnVoidAll.Text = "Void All";
            this.btnVoidAll.UseVisualStyleBackColor = true;
            this.btnVoidAll.Click += new System.EventHandler(this.btnVoidAll_Click);
            // 
            // btnManifest
            // 
            this.btnManifest.Location = new System.Drawing.Point(657, 20);
            this.btnManifest.Name = "btnManifest";
            this.btnManifest.Size = new System.Drawing.Size(99, 25);
            this.btnManifest.TabIndex = 4;
            this.btnManifest.Text = "File Manifest";
            this.btnManifest.UseVisualStyleBackColor = true;
            this.btnManifest.Click += new System.EventHandler(this.btnManifest_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(763, 20);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(99, 25);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Location = new System.Drawing.Point(419, 138);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(35, 13);
            this.lblMsg.TabIndex = 6;
            this.lblMsg.Text = "label1";
            this.lblMsg.Visible = false;
            // 
            // btnPrintPendingLabel
            // 
            this.btnPrintPendingLabel.Location = new System.Drawing.Point(316, 20);
            this.btnPrintPendingLabel.Name = "btnPrintPendingLabel";
            this.btnPrintPendingLabel.Size = new System.Drawing.Size(125, 25);
            this.btnPrintPendingLabel.TabIndex = 7;
            this.btnPrintPendingLabel.Text = "Print Pending Labels";
            this.btnPrintPendingLabel.UseVisualStyleBackColor = true;
            this.btnPrintPendingLabel.Visible = false;
            this.btnPrintPendingLabel.Click += new System.EventHandler(this.btnPrintPendingLabel_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(103, 23);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(207, 20);
            this.txtSearch.TabIndex = 8;
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // lblRequired
            // 
            this.lblRequired.AutoSize = true;
            this.lblRequired.ForeColor = System.Drawing.Color.Red;
            this.lblRequired.Location = new System.Drawing.Point(184, 4);
            this.lblRequired.Name = "lblRequired";
            this.lblRequired.Size = new System.Drawing.Size(50, 13);
            this.lblRequired.TabIndex = 9;
            this.lblRequired.Text = "Required";
            this.lblRequired.Visible = false;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(3, 20);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(94, 25);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // Manifest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 461);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblRequired);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.btnPrintPendingLabel);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnManifest);
            this.Controls.Add(this.btnVoidAll);
            this.Controls.Add(this.btnRefress);
            this.Controls.Add(this.grdSummary);
            this.Controls.Add(this.grdManifest);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Manifest";
            this.Text = "Manifest";
            ((System.ComponentModel.ISupportInitialize)(this.grdManifest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSummary)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdManifest;
        private System.Windows.Forms.DataGridView grdSummary;
        private System.Windows.Forms.Button btnRefress;
        private System.Windows.Forms.Button btnVoidAll;
        private System.Windows.Forms.Button btnManifest;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button btnPrintPendingLabel;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lblRequired;
        private System.Windows.Forms.Button btnSearch;
    }
}