﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class AmazonSenderDetails : Form
    {
        Guid guID = new Guid();
        DataTable objDtSenderDetails = new DataTable();
        DataTable objdtCountry = new DataTable();
        public AmazonSenderDetails()
        {
            InitializeComponent();
            GetAddUpdateAmazonSenderDetails("GET");
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                GetAddUpdateAmazonSenderDetails("UPDATE");
            }
        }
        private void GetAddUpdateAmazonSenderDetails(string strMode)
        {
            DataSet objDs = new DataSet();

            SqlConnection conn = new SqlConnection();
            Guid guCountryID = new Guid();
            conn.ConnectionString = MakeConnection.GetConnection();
            string strResult = "";
            if (ddlCountryName.SelectedValue != null)
            {
                guCountryID = new Guid(ddlCountryName.SelectedValue.ToString());
            }
            try
            {
                SqlParameter[] objParam = new SqlParameter[10];

                objParam[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = guID;

                objParam[1] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                objParam[1].Value = txtName.Text;

                objParam[2] = new SqlParameter("@AddressLine1", SqlDbType.VarChar, 50);
                objParam[2].Value = txtAddressLine1.Text;

                objParam[3] = new SqlParameter("@Email", SqlDbType.VarChar, 50);
                objParam[3].Value = txtEmail.Text;

                objParam[4] = new SqlParameter("@City", SqlDbType.VarChar, 50);
                objParam[4].Value = txtCity.Text;

                objParam[5] = new SqlParameter("@PostalCode", SqlDbType.VarChar, 50);
                objParam[5].Value = txtPostalCode.Text;

                objParam[6] = new SqlParameter("@CountryID", SqlDbType.UniqueIdentifier);
                objParam[6].Value = guCountryID;

                objParam[7] = new SqlParameter("@Phone", SqlDbType.VarChar, 50);
                objParam[7].Value = txtPhone.Text;

                objParam[8] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[8].Value = strMode;

                objParam[9] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[9].Direction = ParameterDirection.Output;

                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateAmazonSenderDetails", objParam);
                    strResult = objParam[9].Value.ToString();
                    if (objDs.Tables.Count > 0)
                    {
                        objdtCountry = objDs.Tables[0];
                        objDtSenderDetails = objDs.Tables[1];
                        PopulateControls();
                    }
                }
                else if (strMode.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateAmazonSenderDetails", objParam);
                    strResult = objParam[9].Value.ToString();
                    //If strResult==1 then it updated record in db
                    if (strResult == "1")
                    {
                        MessageBox.Show("Record updated successfully", "Updated Sender Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    //If strResult contains error then sql is throwing exception
                    else if (strResult.ToUpper().Contains("ERROR"))
                    {
                        GlobalVariablesAndMethods.LogHistory("Exception occurred while populating Amazon Sender Details&&" + strResult);
                        MessageBox.Show(strResult, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    //Otherwise its and insertion and sql will return the ID of inserted row which we are assigning in variable for next call
                    else
                    {
                        guID = new Guid(strResult);
                        MessageBox.Show("Record Added Successfully", "Added Sender Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Exception occurred while populating Amazon Sender Details&&" + ex.Message);
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }
        private void PopulateControls()
        {
            if (objdtCountry.Rows.Count > 0)
            {
                ddlCountryName.DataSource = objdtCountry;
                ddlCountryName.ValueMember = "CountryId";
                ddlCountryName.DisplayMember = "CountryNameCode";
            }
            if (objDtSenderDetails.Rows.Count > 0)
            {
                guID = new Guid(objDtSenderDetails.Rows[0]["ID"].ToString());
                txtName.Text = objDtSenderDetails.Rows[0]["Name"].ToString();
                txtAddressLine1.Text = objDtSenderDetails.Rows[0]["AddressLine1"].ToString();
                txtEmail.Text = objDtSenderDetails.Rows[0]["Email"].ToString();
                txtCity.Text = objDtSenderDetails.Rows[0]["City"].ToString();
                txtPhone.Text = objDtSenderDetails.Rows[0]["Phone"].ToString();
                txtPostalCode.Text = objDtSenderDetails.Rows[0]["PostalCode"].ToString();
                ddlCountryName.SelectedValue = objDtSenderDetails.Rows[0]["CountryID"].ToString();
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool ValidateControls()
        {
            bool IsValid = true;
            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                lblNameRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblNameRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAddressLine1.Text))
            {
                lblAddressLine1Required.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAddressLine1Required.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtEmail.Text))
            {
                lblEmailRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblEmailRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtCity.Text))
            {
                lblCityRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblCityRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPostalCode.Text))
            {
                lblPostalCodeRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPostalCodeRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPhone.Text))
            {
                lblPhoneRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPhoneRequired.Visible = false;
            }
            return IsValid;
        }
    }
}
