﻿namespace ODM.Appliction
{
    partial class AmazonCourierSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AmazonCourierSettings));
            this.lblAppVersionRequired = new System.Windows.Forms.Label();
            this.lblAppNameRequired = new System.Windows.Forms.Label();
            this.lblSellerIDRequired = new System.Windows.Forms.Label();
            this.lblSecretKeyRequired = new System.Windows.Forms.Label();
            this.lblAccessKeyRequired = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpBoxShippingSettings = new System.Windows.Forms.GroupBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSaveClose = new System.Windows.Forms.Button();
            this.grdMappedServices = new System.Windows.Forms.DataGridView();
            this.ddlPrinter = new System.Windows.Forms.ComboBox();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.txtAppVersion = new System.Windows.Forms.TextBox();
            this.lblAppVersion = new System.Windows.Forms.Label();
            this.txtAppName = new System.Windows.Forms.TextBox();
            this.lblAppName = new System.Windows.Forms.Label();
            this.txtSellerID = new System.Windows.Forms.TextBox();
            this.lblSellerID = new System.Windows.Forms.Label();
            this.txtSecretKey = new System.Windows.Forms.TextBox();
            this.lblSecretKey = new System.Windows.Forms.Label();
            this.txtAccessKey = new System.Windows.Forms.TextBox();
            this.lblAccessKey = new System.Windows.Forms.Label();
            this.lblUrlRequired = new System.Windows.Forms.Label();
            this.txtServiceURL = new System.Windows.Forms.TextBox();
            this.lblServiceUrlText = new System.Windows.Forms.Label();
            this.ddlCountryName = new System.Windows.Forms.ComboBox();
            this.lblCountryName = new System.Windows.Forms.Label();
            this.btnManifest = new System.Windows.Forms.Button();
            this.btnSenderDetails = new System.Windows.Forms.Button();
            this.grpBoxShippingSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAppVersionRequired
            // 
            this.lblAppVersionRequired.AutoSize = true;
            this.lblAppVersionRequired.ForeColor = System.Drawing.Color.Red;
            this.lblAppVersionRequired.Location = new System.Drawing.Point(719, 80);
            this.lblAppVersionRequired.Name = "lblAppVersionRequired";
            this.lblAppVersionRequired.Size = new System.Drawing.Size(50, 13);
            this.lblAppVersionRequired.TabIndex = 80;
            this.lblAppVersionRequired.Text = "Required";
            this.lblAppVersionRequired.Visible = false;
            // 
            // lblAppNameRequired
            // 
            this.lblAppNameRequired.AutoSize = true;
            this.lblAppNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblAppNameRequired.Location = new System.Drawing.Point(719, 46);
            this.lblAppNameRequired.Name = "lblAppNameRequired";
            this.lblAppNameRequired.Size = new System.Drawing.Size(50, 13);
            this.lblAppNameRequired.TabIndex = 79;
            this.lblAppNameRequired.Text = "Required";
            this.lblAppNameRequired.Visible = false;
            // 
            // lblSellerIDRequired
            // 
            this.lblSellerIDRequired.AutoSize = true;
            this.lblSellerIDRequired.ForeColor = System.Drawing.Color.Red;
            this.lblSellerIDRequired.Location = new System.Drawing.Point(719, 14);
            this.lblSellerIDRequired.Name = "lblSellerIDRequired";
            this.lblSellerIDRequired.Size = new System.Drawing.Size(50, 13);
            this.lblSellerIDRequired.TabIndex = 78;
            this.lblSellerIDRequired.Text = "Required";
            this.lblSellerIDRequired.Visible = false;
            // 
            // lblSecretKeyRequired
            // 
            this.lblSecretKeyRequired.AutoSize = true;
            this.lblSecretKeyRequired.ForeColor = System.Drawing.Color.Red;
            this.lblSecretKeyRequired.Location = new System.Drawing.Point(321, 41);
            this.lblSecretKeyRequired.Name = "lblSecretKeyRequired";
            this.lblSecretKeyRequired.Size = new System.Drawing.Size(50, 13);
            this.lblSecretKeyRequired.TabIndex = 77;
            this.lblSecretKeyRequired.Text = "Required";
            this.lblSecretKeyRequired.Visible = false;
            // 
            // lblAccessKeyRequired
            // 
            this.lblAccessKeyRequired.AutoSize = true;
            this.lblAccessKeyRequired.ForeColor = System.Drawing.Color.Red;
            this.lblAccessKeyRequired.Location = new System.Drawing.Point(321, 14);
            this.lblAccessKeyRequired.Name = "lblAccessKeyRequired";
            this.lblAccessKeyRequired.Size = new System.Drawing.Size(50, 13);
            this.lblAccessKeyRequired.TabIndex = 76;
            this.lblAccessKeyRequired.Text = "Required";
            this.lblAccessKeyRequired.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(286, 180);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(164, 26);
            this.btnDelete.TabIndex = 74;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(102, 180);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(180, 26);
            this.btnAdd.TabIndex = 73;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grpBoxShippingSettings
            // 
            this.grpBoxShippingSettings.Controls.Add(this.lblMessage);
            this.grpBoxShippingSettings.Controls.Add(this.btnCancel);
            this.grpBoxShippingSettings.Controls.Add(this.btnSaveClose);
            this.grpBoxShippingSettings.Controls.Add(this.grdMappedServices);
            this.grpBoxShippingSettings.Location = new System.Drawing.Point(15, 209);
            this.grpBoxShippingSettings.Name = "grpBoxShippingSettings";
            this.grpBoxShippingSettings.Size = new System.Drawing.Size(787, 241);
            this.grpBoxShippingSettings.TabIndex = 75;
            this.grpBoxShippingSettings.TabStop = false;
            this.grpBoxShippingSettings.Text = "Shipping Mapping";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(339, 75);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(92, 13);
            this.lblMessage.TabIndex = 15;
            this.lblMessage.Text = "No Record Found";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(590, 193);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(180, 29);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSaveClose
            // 
            this.btnSaveClose.Location = new System.Drawing.Point(404, 193);
            this.btnSaveClose.Name = "btnSaveClose";
            this.btnSaveClose.Size = new System.Drawing.Size(180, 29);
            this.btnSaveClose.TabIndex = 13;
            this.btnSaveClose.Text = "Save && Close";
            this.btnSaveClose.UseVisualStyleBackColor = true;
            this.btnSaveClose.Click += new System.EventHandler(this.btnSaveClose_Click);
            // 
            // grdMappedServices
            // 
            this.grdMappedServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMappedServices.Location = new System.Drawing.Point(3, 29);
            this.grdMappedServices.Name = "grdMappedServices";
            this.grdMappedServices.Size = new System.Drawing.Size(781, 150);
            this.grdMappedServices.TabIndex = 12;
            // 
            // ddlPrinter
            // 
            this.ddlPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPrinter.FormattingEnabled = true;
            this.ddlPrinter.Location = new System.Drawing.Point(123, 74);
            this.ddlPrinter.Name = "ddlPrinter";
            this.ddlPrinter.Size = new System.Drawing.Size(191, 21);
            this.ddlPrinter.TabIndex = 72;
            // 
            // lblPrinter
            // 
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Location = new System.Drawing.Point(17, 77);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(70, 13);
            this.lblPrinter.TabIndex = 71;
            this.lblPrinter.Text = "Select Printer";
            // 
            // txtAppVersion
            // 
            this.txtAppVersion.Location = new System.Drawing.Point(522, 77);
            this.txtAppVersion.Name = "txtAppVersion";
            this.txtAppVersion.Size = new System.Drawing.Size(191, 20);
            this.txtAppVersion.TabIndex = 70;
            // 
            // lblAppVersion
            // 
            this.lblAppVersion.AutoSize = true;
            this.lblAppVersion.Location = new System.Drawing.Point(406, 77);
            this.lblAppVersion.Name = "lblAppVersion";
            this.lblAppVersion.Size = new System.Drawing.Size(64, 13);
            this.lblAppVersion.TabIndex = 69;
            this.lblAppVersion.Text = "App Version";
            // 
            // txtAppName
            // 
            this.txtAppName.Location = new System.Drawing.Point(522, 45);
            this.txtAppName.Name = "txtAppName";
            this.txtAppName.Size = new System.Drawing.Size(191, 20);
            this.txtAppName.TabIndex = 68;
            // 
            // lblAppName
            // 
            this.lblAppName.AutoSize = true;
            this.lblAppName.Location = new System.Drawing.Point(405, 45);
            this.lblAppName.Name = "lblAppName";
            this.lblAppName.Size = new System.Drawing.Size(57, 13);
            this.lblAppName.TabIndex = 67;
            this.lblAppName.Text = "App Name";
            // 
            // txtSellerID
            // 
            this.txtSellerID.Location = new System.Drawing.Point(522, 11);
            this.txtSellerID.Name = "txtSellerID";
            this.txtSellerID.Size = new System.Drawing.Size(191, 20);
            this.txtSellerID.TabIndex = 66;
            // 
            // lblSellerID
            // 
            this.lblSellerID.AutoSize = true;
            this.lblSellerID.Location = new System.Drawing.Point(406, 13);
            this.lblSellerID.Name = "lblSellerID";
            this.lblSellerID.Size = new System.Drawing.Size(47, 13);
            this.lblSellerID.TabIndex = 65;
            this.lblSellerID.Text = "Seller ID";
            // 
            // txtSecretKey
            // 
            this.txtSecretKey.Location = new System.Drawing.Point(124, 42);
            this.txtSecretKey.Name = "txtSecretKey";
            this.txtSecretKey.Size = new System.Drawing.Size(191, 20);
            this.txtSecretKey.TabIndex = 64;
            // 
            // lblSecretKey
            // 
            this.lblSecretKey.AutoSize = true;
            this.lblSecretKey.Location = new System.Drawing.Point(20, 45);
            this.lblSecretKey.Name = "lblSecretKey";
            this.lblSecretKey.Size = new System.Drawing.Size(59, 13);
            this.lblSecretKey.TabIndex = 63;
            this.lblSecretKey.Text = "Secret Key";
            // 
            // txtAccessKey
            // 
            this.txtAccessKey.Location = new System.Drawing.Point(124, 14);
            this.txtAccessKey.Name = "txtAccessKey";
            this.txtAccessKey.Size = new System.Drawing.Size(191, 20);
            this.txtAccessKey.TabIndex = 62;
            // 
            // lblAccessKey
            // 
            this.lblAccessKey.AutoSize = true;
            this.lblAccessKey.Location = new System.Drawing.Point(21, 14);
            this.lblAccessKey.Name = "lblAccessKey";
            this.lblAccessKey.Size = new System.Drawing.Size(63, 13);
            this.lblAccessKey.TabIndex = 61;
            this.lblAccessKey.Text = "Access Key";
            // 
            // lblUrlRequired
            // 
            this.lblUrlRequired.AutoSize = true;
            this.lblUrlRequired.ForeColor = System.Drawing.Color.Red;
            this.lblUrlRequired.Location = new System.Drawing.Point(719, 139);
            this.lblUrlRequired.Name = "lblUrlRequired";
            this.lblUrlRequired.Size = new System.Drawing.Size(50, 13);
            this.lblUrlRequired.TabIndex = 86;
            this.lblUrlRequired.Text = "Required";
            this.lblUrlRequired.Visible = false;
            // 
            // txtServiceURL
            // 
            this.txtServiceURL.Location = new System.Drawing.Point(522, 137);
            this.txtServiceURL.Name = "txtServiceURL";
            this.txtServiceURL.Size = new System.Drawing.Size(191, 20);
            this.txtServiceURL.TabIndex = 85;
            // 
            // lblServiceUrlText
            // 
            this.lblServiceUrlText.AutoSize = true;
            this.lblServiceUrlText.Location = new System.Drawing.Point(405, 139);
            this.lblServiceUrlText.Name = "lblServiceUrlText";
            this.lblServiceUrlText.Size = new System.Drawing.Size(68, 13);
            this.lblServiceUrlText.TabIndex = 84;
            this.lblServiceUrlText.Text = "Service URL";
            // 
            // ddlCountryName
            // 
            this.ddlCountryName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCountryName.FormattingEnabled = true;
            this.ddlCountryName.Location = new System.Drawing.Point(522, 103);
            this.ddlCountryName.Name = "ddlCountryName";
            this.ddlCountryName.Size = new System.Drawing.Size(191, 21);
            this.ddlCountryName.TabIndex = 83;
            this.ddlCountryName.SelectedIndexChanged += new System.EventHandler(this.ddlCountryName_SelectedIndexChanged);
            // 
            // lblCountryName
            // 
            this.lblCountryName.AutoSize = true;
            this.lblCountryName.Location = new System.Drawing.Point(403, 106);
            this.lblCountryName.Name = "lblCountryName";
            this.lblCountryName.Size = new System.Drawing.Size(113, 13);
            this.lblCountryName.TabIndex = 82;
            this.lblCountryName.Text = "Choose Country Name";
            // 
            // btnManifest
            // 
            this.btnManifest.Location = new System.Drawing.Point(454, 180);
            this.btnManifest.Name = "btnManifest";
            this.btnManifest.Size = new System.Drawing.Size(164, 26);
            this.btnManifest.TabIndex = 87;
            this.btnManifest.Text = "Manifest";
            this.btnManifest.UseVisualStyleBackColor = true;
            this.btnManifest.Click += new System.EventHandler(this.btnManifest_Click);
            // 
            // btnSenderDetails
            // 
            this.btnSenderDetails.Location = new System.Drawing.Point(621, 180);
            this.btnSenderDetails.Name = "btnSenderDetails";
            this.btnSenderDetails.Size = new System.Drawing.Size(180, 26);
            this.btnSenderDetails.TabIndex = 88;
            this.btnSenderDetails.Text = "Sender Details";
            this.btnSenderDetails.UseVisualStyleBackColor = true;
            this.btnSenderDetails.Click += new System.EventHandler(this.btnSenderDetails_Click);
            // 
            // AmazonCourierSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 456);
            this.Controls.Add(this.btnSenderDetails);
            this.Controls.Add(this.btnManifest);
            this.Controls.Add(this.lblUrlRequired);
            this.Controls.Add(this.txtServiceURL);
            this.Controls.Add(this.lblServiceUrlText);
            this.Controls.Add(this.ddlCountryName);
            this.Controls.Add(this.lblCountryName);
            this.Controls.Add(this.lblAppVersionRequired);
            this.Controls.Add(this.lblAppNameRequired);
            this.Controls.Add(this.lblSellerIDRequired);
            this.Controls.Add(this.lblSecretKeyRequired);
            this.Controls.Add(this.lblAccessKeyRequired);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpBoxShippingSettings);
            this.Controls.Add(this.ddlPrinter);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.txtAppVersion);
            this.Controls.Add(this.lblAppVersion);
            this.Controls.Add(this.txtAppName);
            this.Controls.Add(this.lblAppName);
            this.Controls.Add(this.txtSellerID);
            this.Controls.Add(this.lblSellerID);
            this.Controls.Add(this.txtSecretKey);
            this.Controls.Add(this.lblSecretKey);
            this.Controls.Add(this.txtAccessKey);
            this.Controls.Add(this.lblAccessKey);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AmazonCourierSettings";
            this.Text = "Amazon Courier Settings";
            this.grpBoxShippingSettings.ResumeLayout(false);
            this.grpBoxShippingSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAppVersionRequired;
        private System.Windows.Forms.Label lblAppNameRequired;
        private System.Windows.Forms.Label lblSellerIDRequired;
        private System.Windows.Forms.Label lblSecretKeyRequired;
        private System.Windows.Forms.Label lblAccessKeyRequired;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grpBoxShippingSettings;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSaveClose;
        private System.Windows.Forms.DataGridView grdMappedServices;
        private System.Windows.Forms.ComboBox ddlPrinter;
        private System.Windows.Forms.Label lblPrinter;
        private System.Windows.Forms.TextBox txtAppVersion;
        private System.Windows.Forms.Label lblAppVersion;
        private System.Windows.Forms.TextBox txtAppName;
        private System.Windows.Forms.Label lblAppName;
        private System.Windows.Forms.TextBox txtSellerID;
        private System.Windows.Forms.Label lblSellerID;
        private System.Windows.Forms.TextBox txtSecretKey;
        private System.Windows.Forms.Label lblSecretKey;
        private System.Windows.Forms.TextBox txtAccessKey;
        private System.Windows.Forms.Label lblAccessKey;
        private System.Windows.Forms.Label lblUrlRequired;
        private System.Windows.Forms.TextBox txtServiceURL;
        private System.Windows.Forms.Label lblServiceUrlText;
        private System.Windows.Forms.ComboBox ddlCountryName;
        private System.Windows.Forms.Label lblCountryName;
        private System.Windows.Forms.Button btnManifest;
        private System.Windows.Forms.Button btnSenderDetails;
    }
}