﻿namespace ODM.Appliction
{
    partial class CaptureImage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CaptureImage));
            this.btnStop = new System.Windows.Forms.Button();
            this.lblSnapshotResolution = new System.Windows.Forms.Label();
            this.ddlSnapshotResolution = new System.Windows.Forms.ComboBox();
            this.ddlVideoResolution = new System.Windows.Forms.ComboBox();
            this.lblVideoResolution = new System.Windows.Forms.Label();
            this.btnCapture = new System.Windows.Forms.Button();
            this.ddlVideoDevice = new System.Windows.Forms.ComboBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblVideoDevice = new System.Windows.Forms.Label();
            this.tlPanelCaptureImage = new System.Windows.Forms.TableLayoutPanel();
            this.picBoxCaptureImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCaptureImage)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(303, 12);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 2;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Visible = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // lblSnapshotResolution
            // 
            this.lblSnapshotResolution.AutoSize = true;
            this.lblSnapshotResolution.Location = new System.Drawing.Point(601, 17);
            this.lblSnapshotResolution.Name = "lblSnapshotResolution";
            this.lblSnapshotResolution.Size = new System.Drawing.Size(106, 13);
            this.lblSnapshotResolution.TabIndex = 17;
            this.lblSnapshotResolution.Text = "Snapshot Resoluton:";
            this.lblSnapshotResolution.Visible = false;
            // 
            // ddlSnapshotResolution
            // 
            this.ddlSnapshotResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSnapshotResolution.Enabled = false;
            this.ddlSnapshotResolution.FormattingEnabled = true;
            this.ddlSnapshotResolution.Location = new System.Drawing.Point(710, 13);
            this.ddlSnapshotResolution.Name = "ddlSnapshotResolution";
            this.ddlSnapshotResolution.Size = new System.Drawing.Size(148, 21);
            this.ddlSnapshotResolution.TabIndex = 4;
            this.ddlSnapshotResolution.Visible = false;
            // 
            // ddlVideoResolution
            // 
            this.ddlVideoResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlVideoResolution.Enabled = false;
            this.ddlVideoResolution.FormattingEnabled = true;
            this.ddlVideoResolution.Location = new System.Drawing.Point(478, 12);
            this.ddlVideoResolution.Name = "ddlVideoResolution";
            this.ddlVideoResolution.Size = new System.Drawing.Size(122, 21);
            this.ddlVideoResolution.TabIndex = 3;
            this.ddlVideoResolution.Visible = false;
            // 
            // lblVideoResolution
            // 
            this.lblVideoResolution.AutoSize = true;
            this.lblVideoResolution.Location = new System.Drawing.Point(391, 16);
            this.lblVideoResolution.Name = "lblVideoResolution";
            this.lblVideoResolution.Size = new System.Drawing.Size(88, 13);
            this.lblVideoResolution.TabIndex = 14;
            this.lblVideoResolution.Text = "Video Resoluton:";
            this.lblVideoResolution.Visible = false;
            // 
            // btnCapture
            // 
            this.btnCapture.Location = new System.Drawing.Point(369, 57);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(129, 27);
            this.btnCapture.TabIndex = 5;
            this.btnCapture.Text = "Capture";
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // ddlVideoDevice
            // 
            this.ddlVideoDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlVideoDevice.Enabled = false;
            this.ddlVideoDevice.FormattingEnabled = true;
            this.ddlVideoDevice.Location = new System.Drawing.Point(86, 12);
            this.ddlVideoDevice.Name = "ddlVideoDevice";
            this.ddlVideoDevice.Size = new System.Drawing.Size(121, 21);
            this.ddlVideoDevice.TabIndex = 0;
            this.ddlVideoDevice.Visible = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(222, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Visible = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblVideoDevice
            // 
            this.lblVideoDevice.AutoSize = true;
            this.lblVideoDevice.Location = new System.Drawing.Point(5, 16);
            this.lblVideoDevice.Name = "lblVideoDevice";
            this.lblVideoDevice.Size = new System.Drawing.Size(74, 13);
            this.lblVideoDevice.TabIndex = 19;
            this.lblVideoDevice.Text = "Video Device:";
            this.lblVideoDevice.Visible = false;
            // 
            // tlPanelCaptureImage
            // 
            this.tlPanelCaptureImage.ColumnCount = 1;
            this.tlPanelCaptureImage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlPanelCaptureImage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlPanelCaptureImage.Location = new System.Drawing.Point(38, 112);
            this.tlPanelCaptureImage.Name = "tlPanelCaptureImage";
            this.tlPanelCaptureImage.RowCount = 1;
            this.tlPanelCaptureImage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlPanelCaptureImage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlPanelCaptureImage.Size = new System.Drawing.Size(796, 505);
            this.tlPanelCaptureImage.TabIndex = 21;
            // 
            // picBoxCaptureImage
            // 
            this.picBoxCaptureImage.BackColor = System.Drawing.Color.Black;
            this.picBoxCaptureImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBoxCaptureImage.Location = new System.Drawing.Point(38, 112);
            this.picBoxCaptureImage.Name = "picBoxCaptureImage";
            this.picBoxCaptureImage.Size = new System.Drawing.Size(796, 505);
            this.picBoxCaptureImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxCaptureImage.TabIndex = 20;
            this.picBoxCaptureImage.TabStop = false;
            // 
            // CaptureImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 642);
            this.Controls.Add(this.picBoxCaptureImage);
            this.Controls.Add(this.tlPanelCaptureImage);
            this.Controls.Add(this.lblVideoDevice);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.lblSnapshotResolution);
            this.Controls.Add(this.ddlSnapshotResolution);
            this.Controls.Add(this.ddlVideoResolution);
            this.Controls.Add(this.lblVideoResolution);
            this.Controls.Add(this.btnCapture);
            this.Controls.Add(this.ddlVideoDevice);
            this.Controls.Add(this.btnStart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CaptureImage";
            this.Text = "CaptureImage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CaptureImage_FormClosing);
            this.Load += new System.EventHandler(this.CaptureImage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCaptureImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label lblSnapshotResolution;
        private System.Windows.Forms.ComboBox ddlSnapshotResolution;
        private System.Windows.Forms.ComboBox ddlVideoResolution;
        private System.Windows.Forms.Label lblVideoResolution;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.ComboBox ddlVideoDevice;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblVideoDevice;
        private System.Windows.Forms.TableLayoutPanel tlPanelCaptureImage;
        private System.Windows.Forms.PictureBox picBoxCaptureImage;
    }
}