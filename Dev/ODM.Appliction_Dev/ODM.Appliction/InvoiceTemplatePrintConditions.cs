﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Data.DbContext;
using ODM.Entities.InvoiceTemplate;

namespace ODM.Appliction
{
    public partial class InvoiceTemplatePrintConditions : Form
    {
        private Guid PrintConditionId;
        public InvoiceTemplatePrintConditions(string forInvoice, Guid printConditionForTemplate)
        {
            InitializeComponent();
            this.Text = "Print Condition For " + forInvoice;
            PrintConditionId = printConditionForTemplate;

            RegisterEvents();
            InitializeConditionGrid();
        }

        private void RegisterEvents()
        {
            grdPrintConditions.EditingControlShowing += grdPrintConditions_EditingControlShowing;
            grdPrintConditions.DataError += grdPrintConditions_DataError;
            grdPrintConditions.CellContentClick += grdPrintConditions_CellContentClick;
            chkMatchAny.Click += chkMatchAny_Click;
            chkMatchAll.Click += chkMatchAll_Click;
        }

        void grdPrintConditions_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (grdPrintConditions.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("Delete"))
                {
                    grdPrintConditions.Rows.Remove(grdPrintConditions.Rows[e.RowIndex]);
                }
            }
        }

        void grdPrintConditions_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            // throw new NotImplementedException();
        }

        void grdPrintConditions_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.CellStyle.BackColor = SystemColors.ButtonFace;
            ComboBox combo = e.Control as ComboBox;
            if (combo != null)
            {
                combo.SelectedIndexChanged -=
                    new EventHandler(ComboBox_SelectedIndexChanged);

                combo.SelectedIndexChanged +=
                    new EventHandler(ComboBox_SelectedIndexChanged);
            }
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cell = sender as ComboBox;
            if (GetTables().AsEnumerable().Select(x => x.Field<string>("Table")).ToList().Contains(cell.Text))
            {
                var gridCel = grdPrintConditions.CurrentRow.Cells["Column"];
                var gridCell = gridCel as DataGridViewComboBoxCell;
                gridCell.DataSource = GetColumns(cell.Text);
            }
            grdPrintConditions.Refresh();
        }

        private void InitializeConditionGrid()
        {
            grdPrintConditions.DoubleBuffered(true);
            grdPrintConditions.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdPrintConditions.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdPrintConditions.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdPrintConditions.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdPrintConditions.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdPrintConditions.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdPrintConditions.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdPrintConditions.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdPrintConditions.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdPrintConditions.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdPrintConditions.ScrollBars = ScrollBars.Both;
            grdPrintConditions.RowHeadersVisible = false;
            grdPrintConditions.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdPrintConditions.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdPrintConditions.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdPrintConditions.EnableHeadersVisualStyles = false;
            grdPrintConditions.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdPrintConditions.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdPrintConditions.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdPrintConditions.DefaultCellStyle.Padding = new Padding(5, 5, 5, 5);
            grdPrintConditions.CellBorderStyle = DataGridViewCellBorderStyle.RaisedHorizontal;
            grdPrintConditions.AllowUserToAddRows = false;
            RenderGridWithControls();
        }

        private void RenderGridWithControls()
        {
            grdPrintConditions.Visible = false;
            grdPrintConditions.DataSource = null;
            grdPrintConditions.Refresh();
            grdPrintConditions.Rows.Clear();
            grdPrintConditions.Columns.Clear();
            grdPrintConditions.DataSource = Activator.CreateInstance<ODM.Core.InvoiceTemplate.PrintConditions>()
                .GetPrintConditinsFor(PrintConditionId.ToString());

            grdPrintConditions.Columns["PrintConditionId"].Visible = false;
            grdPrintConditions.Columns["TableName"].Visible = false;
            grdPrintConditions.Columns["ColumnName"].Visible = false;
            grdPrintConditions.Columns["Creterion"].Visible = false;
            grdPrintConditions.Columns["CriteriaDataType"].Visible = false;

            DataGridViewComboBoxColumn table = new DataGridViewComboBoxColumn();
            table.HeaderText = "Table";
            table.Name = "Table";
            table.DataSource = GetTables();
            table.DataPropertyName = "TableName";
            table.ValueMember = "Table";
            table.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            table.FlatStyle = FlatStyle.Flat;
            grdPrintConditions.Columns.Insert(0, table);

            DataGridViewComboBoxColumn column = new DataGridViewComboBoxColumn();
            column.HeaderText = "Column";
            column.Name = "Column";
            column.DataSource = GetAllColumns();
            column.FlatStyle = FlatStyle.Flat;
            column.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            column.DataPropertyName = "ColumnName";
            grdPrintConditions.Columns.Insert(1, column);

            DataGridViewComboBoxColumn criteria = new DataGridViewComboBoxColumn();
            criteria.HeaderText = "criteria";
            criteria.Name = "criteria";
            criteria.FlatStyle = FlatStyle.Flat;
            criteria.DataSource = Enum.GetNames(typeof(PrintConditionCreterion));
            criteria.DataPropertyName = "Creterion";
            grdPrintConditions.Columns.Insert(2, criteria);

            grdPrintConditions.Columns["CompareWith"].DisplayIndex = 3;

            DataGridViewComboBoxColumn criteriaDataType = new DataGridViewComboBoxColumn();
            criteriaDataType.HeaderText = "criteriaDataType";
            criteriaDataType.Name = "criteriaDataType";
            criteriaDataType.FlatStyle = FlatStyle.Flat;
            criteriaDataType.DataSource = Enum.GetNames(typeof(CustomDataTypes));
            criteriaDataType.DataPropertyName = "CriteriaDataType";
            grdPrintConditions.Columns.Insert(4, criteriaDataType);

            DataGridViewButtonColumn delete = new DataGridViewButtonColumn();
            delete.HeaderText = "";
            delete.Text = "Delete";
            delete.UseColumnTextForButtonValue = true;
            delete.Name = "Delete";
            delete.FlatStyle = FlatStyle.Flat;
            delete.DefaultCellStyle.ForeColor = Color.Black;
            grdPrintConditions.Columns.Insert(5, delete);

            //grdPrintConditions.Columns["CompareWith"].ReadOnly = false;
            //grdPrintConditions.DefaultCellStyle.Padding = new Padding(5);
            //grdPrintConditions.Columns["Table"].DisplayIndex = 0;
            //grdPrintConditions.Columns["Column"].DisplayIndex = 1;
            //grdPrintConditions.Columns["criteria"].DisplayIndex = 2;
            //grdPrintConditions.Columns["CompareWith"].DisplayIndex = 3;
            //grdPrintConditions.Columns["Delete"].DisplayIndex = 6;
            //grdPrintConditions.Columns["criteriaDataType"].DisplayIndex = 4;
            //grdPrintConditions.Columns["CriteriaDataType"].DisplayIndex = 5;

            //grdPrintConditions.Columns["PrintConditionId"].DisplayIndex = 6;
            //grdPrintConditions.Columns["TableName"].DisplayIndex = 7;
            //grdPrintConditions.Columns["ColumnName"].DisplayIndex = 8;
            //grdPrintConditions.Columns["Creterion"].DisplayIndex = 9;
            grdPrintConditions.AllowUserToAddRows = false;
            grdPrintConditions.ClearSelection();
            grdPrintConditions.Visible = true;

            LoadConditionValidation();
        }

        private void LoadConditionValidation()
        {
            var selectedPrintConditionCreteriaMatch = Activator.CreateInstance<ODM.Core.InvoiceTemplate.PrintConditions>().GetPrintConditionCreteriaMatch(PrintConditionId);
            if ((PrintConditionCretiaMatch)Enum.Parse(typeof(PrintConditionCretiaMatch), selectedPrintConditionCreteriaMatch) == PrintConditionCretiaMatch.All)
            {
                chkMatchAll.Checked = true;
                chkMatchAny.Checked = false;
            }
            else
            {
                chkMatchAll.Checked = false;
                chkMatchAny.Checked = true;
            }
        }

        private object GetAllColumns()
        {
            var result = new List<string>();
            foreach (var tableName in typeof(VirtualTables).GetProperties())
            {
                var list = typeof(VirtualTables).GetProperty(tableName.Name).GetValue(VirtualTables.Order);
                result.AddRange((List<string>)(list));
            }
            return result;
        }

        private object GetColumns(string tableName)
        {
            return typeof(VirtualTables).GetProperty(tableName).GetValue(VirtualTables.Order);
        }

        private DataTable GetTables()
        {
            var tables = new DataTable();
            tables.Columns.Add("Table");
            foreach (var tableName in typeof(VirtualTables).GetProperties())
            {
                tables.Rows.Add(tableName.Name);
            }
            return tables;
        }

        private void btnAddCondition_Click(object sender, EventArgs e)
        {
            SaveAllCriteria();
            var printCondition = new PrintCondition()
            {
                PrintConditionId = PrintConditionId
            };
            Activator.CreateInstance<ODM.Core.InvoiceTemplate.PrintConditions>()
                .AddEmptyPrintCondition(PrintConditionId.ToString(), printCondition);
            RenderGridWithControls();
            btnSavePrintConditions.BackColor = SystemColors.ControlLight;
        }

        private void btnSavePrintConditions_Click(object sender, EventArgs e)
        {
            SaveAllCriteria();
            RenderGridWithControls();
            btnSavePrintConditions.BackColor = SystemColors.Control;
        }

        private void SaveAllCriteria()
        {
            var conditions = new List<PrintCondition>();
            foreach (DataGridViewRow row in grdPrintConditions.Rows)
            {
                var validRow = true;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.Value == System.DBNull.Value)
                    {
                        validRow = false;
                        break;
                    }
                }

                if (validRow)
                {
                    var printCondition = new PrintCondition();
                    printCondition.PrintConditionId = PrintConditionId;
                    printCondition.TableName = (string)row.Cells["Table"].Value ?? string.Empty;
                    printCondition.ColumnName = (string)row.Cells["Column"].Value ?? string.Empty;
                    printCondition.Creterion = (string)row.Cells["criteria"].Value ?? string.Empty;
                    printCondition.CompareWith = (string)row.Cells["CompareWith"].Value ?? string.Empty;
                    printCondition.CriteriaDataType = (string)row.Cells["criteriaDataType"].Value ?? string.Empty;
                    conditions.Add(printCondition);
                }
            }
            Activator.CreateInstance<ODM.Core.InvoiceTemplate.PrintConditions>()
                .SavePrintConditionsFor(PrintConditionId.ToString(), conditions);
        }


        private void UpdatePrintConditionMatchForTemplate()
        {
            var selectedPrintCreterionMatchStrat = chkMatchAll.Checked ? PrintConditionCretiaMatch.All : PrintConditionCretiaMatch.Any;
            Activator.CreateInstance<ODM.Core.InvoiceTemplate.PrintConditions>().UpdatePrintCondtionMatchForTemplate(PrintConditionId, selectedPrintCreterionMatchStrat);
        }

        void chkMatchAll_Click(object sender, EventArgs e)
        {
            chkMatchAny.Checked = false;
            chkMatchAll.Checked = true;
            UpdatePrintConditionMatchForTemplate();
        }

        void chkMatchAny_Click(object sender, EventArgs e)
        {
            chkMatchAll.Checked = false;
            chkMatchAny.Checked = true;
            UpdatePrintConditionMatchForTemplate();
        }
    }
}
