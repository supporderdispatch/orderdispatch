﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class LaunchSignInEbay : Form
    {
        string _AppKey, _DevKey, _CertKey, _SessionId, _RuName, _Token;
        RestClient _client = new RestClient("https://www.google.com");
        int _intStatus = -1;
        bool IsEbaySandbox ;
        DataTable _objDt = new DataTable();
        public LaunchSignInEbay()
        {
            InitializeComponent();
            _objDt = GlobalVariablesAndMethods.GetEbayAccountAppKeys("GET");
            
            
            BindDropDownList();
        }
        private void BindDropDownList()
        {
            //ddlEbayAcounts.DataSource = _objDt;
            //ddlEbayAcounts.ValueMember = "AccountID";
            //ddlEbayAcounts.DisplayMember = "AccountName";
            //ddlEbayAcounts.SelectedIndex = 0;

            ddlAppKeys.DataSource = _objDt;
            ddlAppKeys.ValueMember = "ID";
            ddlAppKeys.DisplayMember = "AppKeysName";
            ddlAppKeys.SelectedIndex = 0;

        }

        private void btnEbayUserSignIn_Click(object sender, EventArgs e)
        {
            if (txtEbayUserName.Text.ToString() != "")
            {


                DataView dv = new DataView(_objDt);
                dv.RowFilter = "ID='" + ddlAppKeys.SelectedValue + "'";
                DataTable objSelectedEbayAccount = dv.ToTable();
                _AppKey = objSelectedEbayAccount.Rows[0]["AppKey"].ToString();
                _CertKey = objSelectedEbayAccount.Rows[0]["CertKey"].ToString();
                _DevKey = objSelectedEbayAccount.Rows[0]["DevKey"].ToString();
                _RuName = objSelectedEbayAccount.Rows[0]["RuName"].ToString();
                IsEbaySandbox = Convert.ToBoolean(objSelectedEbayAccount.Rows[0]["IsEbaySandbox"].ToString());
                
                if (IsEbaySandbox)
                {
                    _client = new RestClient("https://api.sandbox.ebay.com/ws/api.dll");
                }
                else
                {
                    // use live url for ebay api's
                    _client = new RestClient("https://api.ebay.com/ws/api.dll");
                }
                var request = new RestRequest(Method.POST);

                request.AddHeader("X-EBAY-API-SITEID", "3");
                request.AddHeader("X-EBAY-API-COMPATIBILITY-LEVEL", "967");
                request.AddHeader("X-EBAY-API-CALL-NAME", "GetSessionID");
                request.AddHeader("X-EBAY-API-APP-NAME", _AppKey);
                request.AddHeader("X-EBAY-API-DEV-NAME", _DevKey);
                request.AddHeader("X-EBAY-API-CERT-NAME", _CertKey);
                string strxml = @"<?xml version=""1.0"" encoding=""utf - 8""?>
  <GetSessionIDRequest xmlns = ""urn:ebay:apis:eBLBaseComponents"">
     <RuName>" + _RuName + @"</RuName>
   </GetSessionIDRequest>";
                request.AddParameter("text/xml", strxml, ParameterType.RequestBody);
                IRestResponse response = _client.Execute(request);
                string strResponseData = response.Content;
                string strResponseStatus = response.StatusCode.ToString();
                if (GlobalVariablesAndMethods.GetSubString(strResponseData, "<Ack>", "</Ack>").ToUpper() == "SUCCESS")
                {
                    _SessionId = GlobalVariablesAndMethods.GetSubString(strResponseData, "<SessionID>", "</SessionID>");
                    string SignInURL = string.Empty;
                    if (IsEbaySandbox)
                    {
                         SignInURL = "https://signin.sandbox.ebay.com/ws/eBayISAPI.dll?SignIn&RuName=" + _RuName + "&SessID=" + _SessionId;
                    }
                    else
                    {
                        // use live url for singing in user's ebay account.
                        SignInURL = "https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&RuName=" + _RuName + "&SessID=" + _SessionId;
                    }
                       
                    AuthorizeEbayUser objAuthorizeEbayUser = new AuthorizeEbayUser(SignInURL);
                    objAuthorizeEbayUser.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Please Enter UserName!", "Ebay SignIn", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFetchandSaveToken_Click(object sender, EventArgs e)
        {
            string strResult = string.Empty;
            if (_intStatus == -1)
            {
                if (chkLogIn.Checked && !string.IsNullOrEmpty(_AppKey))
                {                    
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("X-EBAY-API-SITEID", "3");
                    request.AddHeader("X-EBAY-API-COMPATIBILITY-LEVEL", "1031");
                    request.AddHeader("X-EBAY-API-CALL-NAME", "FetchToken");
                    request.AddHeader("X-EBAY-API-APP-NAME", _AppKey);
                    request.AddHeader("X-EBAY-API-DEV-NAME", _DevKey);
                    request.AddHeader("X-EBAY-API-CERT-NAME", _CertKey);
                    string strxml = @"<?xml version=""1.0"" encoding=""utf-8""?>
<FetchTokenRequest xmlns=""urn:ebay:apis:eBLBaseComponents"">
  <SessionID>" + _SessionId + @"</SessionID>
</FetchTokenRequest>";
                    request.AddParameter("text/xml", strxml, ParameterType.RequestBody);
                    IRestResponse response = _client.Execute(request);
                    string strResponseData = response.Content;
                    
                    if (GlobalVariablesAndMethods.GetSubString(strResponseData, "<Ack>", "</Ack>").ToUpper() == "SUCCESS")
                    {
                        _Token = GlobalVariablesAndMethods.GetSubString(strResponseData, "<eBayAuthToken>", "</eBayAuthToken>");

                        strResult = GlobalVariablesAndMethods.AddUpdateDeleteEbayUsers("UPDATE", Guid.NewGuid(), new Guid(ddlEbayAcounts.SelectedValue.ToString()), txtEbayUserName.Text.ToString(), _Token);

                        if (strResult == "1")
                        {
                            _intStatus = 0;
                            MessageBox.Show("Your Token has been saved in the database successfully.", "Fetch Token Request", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                        else if (strResult == "2")
                        {
                            _intStatus = 0;
                            MessageBox.Show("Your Token has been updated in the database successfully.", "Fetch Token Request", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                       
                    }
                    else
                    {
                        MessageBox.Show("Error In Fetch Token Request Call", "Fetch Token Request", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Your Token Is Already Saved in the database.", "Fetch Token Request", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
               
            }
           
         if(!chkLogIn.Checked)
            {
                MessageBox.Show("Please check the checkbox above.", "Fetch Token Request", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
