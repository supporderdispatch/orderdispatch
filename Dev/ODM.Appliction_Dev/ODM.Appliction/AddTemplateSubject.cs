﻿using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class AddTemplateSubject : Form
    {
        string strMode, strSubject;
        string strId;
        public bool IsCreated = false;
        public AddTemplateSubject()
        {
            InitializeComponent();
        }
        public AddTemplateSubject(string strHeader, string strMode, string strSubject, string strId)
        {

            InitializeComponent();
            PopulateControls(strHeader, strMode, strSubject, strId);

        }
        private void PopulateControls(string strHeader, string strMode, string strSubject, string strId)
        {
            this.strId = strId;
            this.strMode = strMode;
            this.strSubject = strSubject;
            if (strMode.ToUpper() == "UPDATE")
            {
                txtNewSubject.Text = strSubject.ToUpper();
            }

            this.Text = strHeader;
            this.btnCreateTemplate.Text = strMode;
        }
        private void btnCreateTemplate_Click(object sender, EventArgs e)
        {
            string strResult = "";
            if (string.IsNullOrEmpty(txtNewSubject.Text.ToString()))
            {
                lblError.Visible = true;
            }
            else
            {
                if(strMode.ToUpper() == "CREATE")
                {
                    IsCreated = true;
                    strResult = GlobalVariablesAndMethods.AddUpdateDeleteTemplateSubject("INSERT", "", txtNewSubject.Text.ToUpper().ToString());
                }
                else if (strMode.ToUpper() == "UPDATE")
                {
                   
                    strResult = GlobalVariablesAndMethods.AddUpdateDeleteTemplateSubject("UPDATE", strId, txtNewSubject.Text.ToUpper().ToString());
                }
                
                if (strResult == "0")
                {
                    MessageBox.Show("Subject Added Successfully!", "Add New Subject", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (strResult == "1")
                {
                    MessageBox.Show("Subject Already Exists!", "Add New Subject", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (strResult == "2")
                {
                    MessageBox.Show("Subject Updated Successfully!", "Update Subject", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
