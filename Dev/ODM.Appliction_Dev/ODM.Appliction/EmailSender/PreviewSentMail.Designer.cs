﻿namespace ODM.Appliction.EmailSender
{
    partial class PreviewSentMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreviewSentMail));
            this.wbMailSubject = new System.Windows.Forms.WebBrowser();
            this.lblBody = new System.Windows.Forms.Label();
            this.lblBCC = new System.Windows.Forms.Label();
            this.txtBCC = new System.Windows.Forms.TextBox();
            this.lblSubject = new System.Windows.Forms.Label();
            this.lblCC = new System.Windows.Forms.Label();
            this.txtCC = new System.Windows.Forms.TextBox();
            this.wbMailBody = new System.Windows.Forms.WebBrowser();
            this.chkOrderImage = new System.Windows.Forms.CheckBox();
            this.lblOrderImage = new System.Windows.Forms.Label();
            this.chkInvoice = new System.Windows.Forms.CheckBox();
            this.lblInvoice = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // wbMailSubject
            // 
            this.wbMailSubject.Location = new System.Drawing.Point(61, 42);
            this.wbMailSubject.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbMailSubject.Name = "wbMailSubject";
            this.wbMailSubject.ScrollBarsEnabled = false;
            this.wbMailSubject.Size = new System.Drawing.Size(427, 47);
            this.wbMailSubject.TabIndex = 56;
            // 
            // lblBody
            // 
            this.lblBody.AutoSize = true;
            this.lblBody.Location = new System.Drawing.Point(14, 95);
            this.lblBody.Name = "lblBody";
            this.lblBody.Size = new System.Drawing.Size(37, 13);
            this.lblBody.TabIndex = 55;
            this.lblBody.Text = "Body :";
            // 
            // lblBCC
            // 
            this.lblBCC.AutoSize = true;
            this.lblBCC.Location = new System.Drawing.Point(271, 15);
            this.lblBCC.Name = "lblBCC";
            this.lblBCC.Size = new System.Drawing.Size(32, 13);
            this.lblBCC.TabIndex = 54;
            this.lblBCC.Text = "Bcc :";
            // 
            // txtBCC
            // 
            this.txtBCC.Location = new System.Drawing.Point(303, 12);
            this.txtBCC.Name = "txtBCC";
            this.txtBCC.Size = new System.Drawing.Size(185, 20);
            this.txtBCC.TabIndex = 53;
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(6, 42);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(49, 13);
            this.lblSubject.TabIndex = 52;
            this.lblSubject.Text = "Subject :";
            // 
            // lblCC
            // 
            this.lblCC.AutoSize = true;
            this.lblCC.Location = new System.Drawing.Point(29, 15);
            this.lblCC.Name = "lblCC";
            this.lblCC.Size = new System.Drawing.Size(26, 13);
            this.lblCC.TabIndex = 51;
            this.lblCC.Text = "Cc :";
            // 
            // txtCC
            // 
            this.txtCC.Location = new System.Drawing.Point(61, 12);
            this.txtCC.Name = "txtCC";
            this.txtCC.Size = new System.Drawing.Size(185, 20);
            this.txtCC.TabIndex = 50;
            // 
            // wbMailBody
            // 
            this.wbMailBody.Location = new System.Drawing.Point(61, 97);
            this.wbMailBody.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbMailBody.Name = "wbMailBody";
            this.wbMailBody.Size = new System.Drawing.Size(783, 305);
            this.wbMailBody.TabIndex = 57;
            // 
            // chkOrderImage
            // 
            this.chkOrderImage.AutoSize = true;
            this.chkOrderImage.Checked = true;
            this.chkOrderImage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOrderImage.Location = new System.Drawing.Point(647, 19);
            this.chkOrderImage.Name = "chkOrderImage";
            this.chkOrderImage.Size = new System.Drawing.Size(15, 14);
            this.chkOrderImage.TabIndex = 61;
            this.chkOrderImage.UseVisualStyleBackColor = true;
            // 
            // lblOrderImage
            // 
            this.lblOrderImage.AutoSize = true;
            this.lblOrderImage.Location = new System.Drawing.Point(565, 18);
            this.lblOrderImage.Name = "lblOrderImage";
            this.lblOrderImage.Size = new System.Drawing.Size(65, 13);
            this.lblOrderImage.TabIndex = 60;
            this.lblOrderImage.Text = "Order Image";
            // 
            // chkInvoice
            // 
            this.chkInvoice.AutoSize = true;
            this.chkInvoice.Checked = true;
            this.chkInvoice.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInvoice.Location = new System.Drawing.Point(647, 53);
            this.chkInvoice.Name = "chkInvoice";
            this.chkInvoice.Size = new System.Drawing.Size(15, 14);
            this.chkInvoice.TabIndex = 59;
            this.chkInvoice.UseVisualStyleBackColor = true;
            // 
            // lblInvoice
            // 
            this.lblInvoice.AutoSize = true;
            this.lblInvoice.Location = new System.Drawing.Point(566, 52);
            this.lblInvoice.Name = "lblInvoice";
            this.lblInvoice.Size = new System.Drawing.Size(42, 13);
            this.lblInvoice.TabIndex = 58;
            this.lblInvoice.Text = "Invoice";
            // 
            // PreviewSentMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 412);
            this.Controls.Add(this.chkOrderImage);
            this.Controls.Add(this.lblOrderImage);
            this.Controls.Add(this.chkInvoice);
            this.Controls.Add(this.lblInvoice);
            this.Controls.Add(this.wbMailBody);
            this.Controls.Add(this.wbMailSubject);
            this.Controls.Add(this.lblBody);
            this.Controls.Add(this.lblBCC);
            this.Controls.Add(this.txtBCC);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.lblCC);
            this.Controls.Add(this.txtCC);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PreviewSentMail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PreviewSentMail";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser wbMailSubject;
        private System.Windows.Forms.Label lblBody;
        private System.Windows.Forms.Label lblBCC;
        private System.Windows.Forms.TextBox txtBCC;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label lblCC;
        private System.Windows.Forms.TextBox txtCC;
        private System.Windows.Forms.WebBrowser wbMailBody;
        private System.Windows.Forms.CheckBox chkOrderImage;
        private System.Windows.Forms.Label lblOrderImage;
        private System.Windows.Forms.CheckBox chkInvoice;
        private System.Windows.Forms.Label lblInvoice;
    }
}