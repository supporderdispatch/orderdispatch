﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mshtml;

namespace ODM.Appliction.EmailSender
{
    public partial class AddItemsIntoItemTable : Form
    {
        private string TableText = "<table><tr></tr></table>";
        public AddItemsIntoItemTable()
        {
            InitializeComponent();
            PrepareGrid();
            RegisterEvents();
        }

        private void RegisterEvents()
        {
            txtTableOutput.KeyUp+=txtTableOutput_KeyUp;
        }

        private void txtTableOutput_KeyUp(object sender, KeyEventArgs e)
        {
            wbTableOutput.DocumentText = txtTableOutput.Text;           
        }

        private void PrepareGrid()
        {
            var orderItems=GetDataForGrid();
            var customDataTable = new DataTable();
            customDataTable.Columns.Add("Column_Name");
            customDataTable.Columns.Add("Inserted");
            customDataTable.Columns["Inserted"].DataType = typeof(bool);
            foreach (DataRow column in orderItems.Rows)
            {
                var row = customDataTable.NewRow();
                row["Column_Name"] = column["Column_Name"];
                row["Inserted"] = false;
                customDataTable.Rows.Add(row);
            }
            grdItems.DataSource = customDataTable;
            RenderGridWithPhysicalStyle();
        }

        private void RenderGridWithPhysicalStyle()
        {
            grdItems.DoubleBuffered(true);
            grdItems.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdItems.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdItems.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdItems.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdItems.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdItems.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdItems.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdItems.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdItems.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdItems.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdItems.ScrollBars = ScrollBars.Both;
            grdItems.RowHeadersVisible = false;
            grdItems.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdItems.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdItems.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdItems.EnableHeadersVisualStyles = false;
            grdItems.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdItems.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdItems.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdItems.DefaultCellStyle.Padding = new Padding(3, 3, 3, 3);
            grdItems.CellBorderStyle = DataGridViewCellBorderStyle.RaisedHorizontal;
            grdItems.AllowUserToAddRows = false;

            grdItems.CellContentClick += grdItems_CellContentClick;
        }

        void grdItems_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex ==grdItems.Columns["Inserted"].Index)
            {
                if (!Convert.ToBoolean(grdItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()))
                {
                    InsertValueIntoTable(e);
                }
                else
                {
                    RemoveValueFromTable(e);
                }
            }
        }

        private void RemoveValueFromTable(DataGridViewCellEventArgs e)
        {
            var webBrowser = new WebBrowser();
            TableText = string.IsNullOrEmpty(txtTableOutput.Text) ? TableText : txtTableOutput.Text;
            webBrowser.DocumentText = "<HTML><BODY>" + TableText + "</BODY></HTML>";
            webBrowser.DocumentCompleted += (s, ev) =>
            {
                var tableHeaders = webBrowser.Document.GetElementsByTagName("TH");
                var selectedValue=grdItems.Rows[e.RowIndex].Cells["Column_Name"].Value.ToString();
                var tbl="<table><tr>";
                foreach (HtmlElement tableHeader in tableHeaders)
                {

                    if (!(tableHeader.InnerText != null && tableHeader.InnerText.Equals(selectedValue)))
                    {
                        tbl += tableHeader.OuterHtml;
                    }
                }
                tbl += "</tr></table>";
                TableText = tbl;
                txtTableOutput.Text = TableText;
                wbTableOutput.DocumentText = TableText;
            };

        }

        private void InsertValueIntoTable(DataGridViewCellEventArgs e)
        {
            var webBrowser = new WebBrowser();
            TableText = string.IsNullOrEmpty(txtTableOutput.Text) ? TableText : txtTableOutput.Text;
            webBrowser.DocumentText = "<HTML><BODY>"+TableText+"</BODY></HTML>";
            webBrowser.DocumentCompleted += (s, ev) => 
            {
                var tableRow=webBrowser.Document.GetElementsByTagName("TR")[0];
                var newTh = webBrowser.Document.CreateElement("TH");
                newTh.InnerText = grdItems.Rows[e.RowIndex].Cells["Column_Name"].Value.ToString();
                newTh.SetAttribute("mappedWithField", newTh.InnerText);
                tableRow.AppendChild(newTh);
                TableText = webBrowser.Document.GetElementsByTagName("TABLE")[0].OuterHtml;
                txtTableOutput.Text = TableText;
                wbTableOutput.DocumentText = TableText;
            };
        }
        private DataTable GetDataForGrid()
        {
            var result = new DataTable();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            var tables = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDataAndColumnNamesForMenu");
            foreach (DataTable table in tables.Tables)
            {
                if (table.Rows[0]["Table_Name"].ToString().Trim().Equals("OrderItem"))
                {
                    result = table;
                    break;
                }
            }
            connection.Close();
            return result;
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            ODM.Appliction.EmailSender.EmailTemplateEditor.ItemsTable = txtTableOutput.Text;
            this.Close();
        }
    }
}
