﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Appliction.Extensions;

namespace ODM.Appliction.EmailSender
{
    public partial class SentMails : Form
    {
        private DataTable _SentMails;

        public SentMails()
        {
            InitializeComponent();
            LoadSentMails();
            RegisterEvents();
        }

        private void RegisterEvents()
        {
            grdSentMails.MouseClick += grdSentMails_MouseClick;
        }

        void grdSentMails_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenuStrip cm = new ContextMenuStrip();
                cm.Items.Add(new ToolStripMenuItem("Preview Sent Mail", new Bitmap(1, 1),PreviewSentMail_Click ));
                cm.Show(grdSentMails, new Point(e.X, e.Y));
            }
        }

        private void PreviewSentMail_Click(object sender, EventArgs e)
        {
            var row = grdSentMails.SelectedRows[0];
            var email = new ODM.Entities.Emails.Email();
            email.OrderId = (Guid)row.Cells["OrderId"].Value;
            email.NumOrderId = row.Cells["NumOrderId"].Value.ToString();
            email.MailToAddress = row.Cells["MailToAddress"].Value.ToString();
            email.SentMailBody = row.Cells["SentMailBody"].Value.ToString();
            email.EmailTemplateId = (Guid)row.Cells["EmailTemplateId"].Value;
            email.CreatedOn = Convert.ToDateTime(row.Cells["CreatedOn"].Value.ToString());
            email.SentSubject = row.Cells["SentSubject"].Value.ToString();

            var previewForm = Application.OpenForms["PreviewSentMail"];
            if (previewForm != null)
                previewForm.Close();

            new PreviewSentMail(email).Show();
        }


        private void LoadSentMails()
        {
            LoadDataForGrid();
            RenderGridPhysicalStyle();
        }

        private void LoadDataForGrid()
        {
            _SentMails = ODM.Core.Emails.Mails.GetInstance().GetAllSentMails();
            foreach (DataRow row in _SentMails.Rows)
            {
                row["CreatedOn"] = ODM.Core.Helpers.Conversions.GetLocalDateFromUtc(row["CreatedOn"].ToString());
                row["MailSentOn"] = ODM.Core.Helpers.Conversions.GetLocalDateFromUtc(row["MailSentOn"].ToString());
            }
            grdSentMails.DataSource = _SentMails;
            Action act = () => { };
            grdSentMails.ApplyFilter(_SentMails, act);
        }


        private void RenderGridPhysicalStyle()
        {
            grdSentMails.Columns["SentMailBody"].Visible = false;
            grdSentMails.Columns["IsMailSent"].Visible = false;
            grdSentMails.Columns["IsProcessed"].Visible = false;
            grdSentMails.Columns["OrderId"].Visible = false;
            grdSentMails.Columns["EmailTemplateId"].Visible = false;

            CheckForIllegalCrossThreadCalls = false;
            grdSentMails.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdSentMails.AllowUserToAddRows = false;
            grdSentMails.RowHeadersVisible = false;
            grdSentMails.DoubleBuffered(true);
            grdSentMails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
    }
}
