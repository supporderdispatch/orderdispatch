﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Data.DbContext;
using ODM.Entities.Emails;
using ODM.Entities.InvoiceTemplate;

namespace ODM.Appliction.EmailSender
{
    public partial class EmailConditions : Form
    {
        private Guid EmailConditionId;
        public EmailConditions(Guid emailConditionId)
        {
            InitializeComponent();
            EmailConditionId = emailConditionId;

            RegisterEvents();
            InitializeConditionGrid();
        }

        private void RegisterEvents()
        {
            grdCriteria.EditingControlShowing += grdPrintConditions_EditingControlShowing;
            grdCriteria.DataError += grdPrintConditions_DataError;
            grdCriteria.CellContentClick += grdPrintConditions_CellContentClick;
            chkMatchAny.Click += chkMatchAny_Click;
            chkMatchAll.Click += chkMatchAll_Click;
        }

        void grdPrintConditions_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (grdCriteria.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("Delete"))
                {
                    grdCriteria.Rows.Remove(grdCriteria.Rows[e.RowIndex]);
                }
            }
        }

        void grdPrintConditions_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            // throw new NotImplementedException();
        }

        void grdPrintConditions_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.CellStyle.BackColor = SystemColors.ButtonFace;
            ComboBox combo = e.Control as ComboBox;
            if (combo != null)
            {
                combo.SelectedIndexChanged -=
                    new EventHandler(ComboBox_SelectedIndexChanged);

                combo.SelectedIndexChanged +=
                    new EventHandler(ComboBox_SelectedIndexChanged);
            }
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cell = sender as ComboBox;
            if (GetTables().AsEnumerable().Select(x => x.Field<string>("Table")).ToList().Contains(cell.Text))
            {
                var gridCel = grdCriteria.CurrentRow.Cells["Column"];
                var gridCell = gridCel as DataGridViewComboBoxCell;
                gridCell.DataSource = GetColumns(cell.Text);
            }
            grdCriteria.Refresh();
        }

        private void InitializeConditionGrid()
        {
            grdCriteria.DoubleBuffered(true);
            grdCriteria.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdCriteria.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdCriteria.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdCriteria.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdCriteria.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdCriteria.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdCriteria.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdCriteria.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdCriteria.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdCriteria.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdCriteria.ScrollBars = ScrollBars.Both;
            grdCriteria.RowHeadersVisible = false;
            grdCriteria.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdCriteria.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdCriteria.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdCriteria.EnableHeadersVisualStyles = false;
            grdCriteria.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdCriteria.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdCriteria.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdCriteria.DefaultCellStyle.Padding = new Padding(5, 5, 5, 5);
            grdCriteria.CellBorderStyle = DataGridViewCellBorderStyle.RaisedHorizontal;
            grdCriteria.AllowUserToAddRows = false;
            RenderGridWithControls();
        }

        private void RenderGridWithControls()
        {
            grdCriteria.Visible = false;
            grdCriteria.DataSource = null;
            grdCriteria.Refresh();
            grdCriteria.Rows.Clear();
            grdCriteria.Columns.Clear();
            grdCriteria.DataSource = ODM.Core.Emails.EmailConditions.GetInstance().GetEmailConditionsFor(EmailConditionId);

            grdCriteria.Columns["EmailTemplateConditionId"].Visible = false;
            grdCriteria.Columns["TableName"].Visible = false;
            grdCriteria.Columns["ColumnName"].Visible = false;
            grdCriteria.Columns["Creterion"].Visible = false;
            grdCriteria.Columns["CriteriaDataType"].Visible = false;

            DataGridViewComboBoxColumn table = new DataGridViewComboBoxColumn();
            table.HeaderText = "Table";
            table.Name = "Table";
            table.DataSource = GetTables();
            table.DataPropertyName = "TableName";
            table.ValueMember = "Table";
            table.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            table.FlatStyle = FlatStyle.Flat;
            grdCriteria.Columns.Insert(0, table);

            DataGridViewComboBoxColumn column = new DataGridViewComboBoxColumn();
            column.HeaderText = "Column";
            column.Name = "Column";
            column.DataSource = GetAllColumns();
            column.FlatStyle = FlatStyle.Flat;
            column.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            column.DataPropertyName = "ColumnName";
            grdCriteria.Columns.Insert(1, column);

            DataGridViewComboBoxColumn criteria = new DataGridViewComboBoxColumn();
            criteria.HeaderText = "criteria";
            criteria.Name = "criteria";
            criteria.FlatStyle = FlatStyle.Flat;
            criteria.DataSource = Enum.GetNames(typeof(PrintConditionCreterion));
            criteria.DataPropertyName = "Creterion";
            grdCriteria.Columns.Insert(2, criteria);

            grdCriteria.Columns["CompareWith"].DisplayIndex = 3;

            DataGridViewComboBoxColumn criteriaDataType = new DataGridViewComboBoxColumn();
            criteriaDataType.HeaderText = "criteriaDataType";
            criteriaDataType.Name = "criteriaDataType";
            criteriaDataType.FlatStyle = FlatStyle.Flat;
            criteriaDataType.DataSource = Enum.GetNames(typeof(CustomDataTypes));
            criteriaDataType.DataPropertyName = "CriteriaDataType";
            grdCriteria.Columns.Insert(4, criteriaDataType);

            DataGridViewButtonColumn delete = new DataGridViewButtonColumn();
            delete.HeaderText = "";
            delete.Text = "Delete";
            delete.UseColumnTextForButtonValue = true;
            delete.Name = "Delete";
            delete.FlatStyle = FlatStyle.Flat;
            delete.DefaultCellStyle.ForeColor = Color.Black;
            grdCriteria.Columns.Insert(5, delete);

            grdCriteria.AllowUserToAddRows = false;
            grdCriteria.ClearSelection();
            grdCriteria.Visible = true;

            LoadConditionValidation();
        }

        private void LoadConditionValidation()
        {
            var selectedEmailConditionCreteriaMatch = ODM.Core.Emails.EmailConditions.GetInstance().GetEmailConditionCreteriaMatch(EmailConditionId);
            if ((EmailConditionCriteriaMatch)Enum.Parse(typeof(EmailConditionCriteriaMatch), selectedEmailConditionCreteriaMatch) == EmailConditionCriteriaMatch.All)
            {
                chkMatchAll.Checked = true;
                chkMatchAny.Checked = false;
            }
            else
            {
                chkMatchAll.Checked = false;
                chkMatchAny.Checked = true;
            }
        }

        private object GetAllColumns()
        {
            var result = new List<string>();
            foreach (var tableName in typeof(VirtualTables).GetProperties())
            {
                var list = typeof(VirtualTables).GetProperty(tableName.Name).GetValue(VirtualTables.Order);
                result.AddRange((List<string>)(list));
            }
            return result;
        }

        private object GetColumns(string tableName)
        {
            return typeof(VirtualTables).GetProperty(tableName).GetValue(VirtualTables.Order);
        }

        private DataTable GetTables()
        {
            var tables = new DataTable();
            tables.Columns.Add("Table");
            foreach (var tableName in typeof(VirtualTables).GetProperties())
            {
                tables.Rows.Add(tableName.Name);
            }
            return tables;
        }

        private void btnAddCondition_Click(object sender, EventArgs e)
        {
            SaveAllCriteria();
            var emailCondition = new EmailCondition()
            {
                EmailConditionId = EmailConditionId
            };
            ODM.Core.Emails.EmailConditions.GetInstance().SaveEmailCondition(emailCondition);
            RenderGridWithControls();
            btnSave.BackColor = SystemColors.ControlLight;

        }

        private void btnSavePrintConditions_Click(object sender, EventArgs e)
        {
            SaveAllCriteria();
            RenderGridWithControls();
            btnSave.BackColor = SystemColors.Control;
        }

        private void SaveAllCriteria()
        {
            var conditions = new List<EmailCondition>();
            foreach (DataGridViewRow row in grdCriteria.Rows)
            {
                var validRow = true;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.Value == System.DBNull.Value)
                    {
                        validRow = false;
                        break;
                    }
                }

                if (validRow)
                {
                    var emailCondition = new EmailCondition();
                    emailCondition.EmailConditionId = EmailConditionId;
                    emailCondition.TableName = (string)row.Cells["Table"].Value ?? string.Empty;
                    emailCondition.ColumnName = (string)row.Cells["Column"].Value ?? string.Empty;
                    emailCondition.Creterion = (string)row.Cells["criteria"].Value ?? string.Empty;
                    emailCondition.CompareWith = (string)row.Cells["CompareWith"].Value ?? string.Empty;
                    emailCondition.CriteriaDataType = (string)row.Cells["criteriaDataType"].Value ?? string.Empty;
                    conditions.Add(emailCondition);
                }
            }

            ODM.Core.Emails.EmailConditions.GetInstance().SaveEmailConditionsFor(EmailConditionId, conditions);
        }


        private void UpdateEmailConditionMatchForTemplate()
        {
            var selectedEmailCreterionMatch = chkMatchAll.Checked ? EmailConditionCriteriaMatch.All : EmailConditionCriteriaMatch.Any;
            ODM.Core.Emails.EmailConditions.GetInstance().UpdateEmailCondtionMatchForTemplate(EmailConditionId, selectedEmailCreterionMatch);
        }

        void chkMatchAll_Click(object sender, EventArgs e)
        {
            chkMatchAny.Checked = false;
            chkMatchAll.Checked = true;
            UpdateEmailConditionMatchForTemplate();
        }

        void chkMatchAny_Click(object sender, EventArgs e)
        {
            chkMatchAll.Checked = false;
            chkMatchAny.Checked = true;
            UpdateEmailConditionMatchForTemplate();
        }
    }
}
