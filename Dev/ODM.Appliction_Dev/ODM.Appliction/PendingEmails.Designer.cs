﻿namespace ODM.Appliction
{
    partial class PendingEmails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PendingEmails));
            this.grdPendingEmails = new System.Windows.Forms.DataGridView();
            this.btnAutoSend = new System.Windows.Forms.Button();
            this.btnDeleteSelected = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSendSelected = new System.Windows.Forms.Button();
            this.menuStriptEmail = new System.Windows.Forms.MenuStrip();
            this.emailNotificationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailNotificationsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sentEmailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblMsg = new System.Windows.Forms.Label();
            this.lblTotalRecord = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdPendingEmails)).BeginInit();
            this.menuStriptEmail.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdPendingEmails
            // 
            this.grdPendingEmails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPendingEmails.Location = new System.Drawing.Point(-1, 43);
            this.grdPendingEmails.Name = "grdPendingEmails";
            this.grdPendingEmails.Size = new System.Drawing.Size(966, 476);
            this.grdPendingEmails.TabIndex = 0;
            // 
            // btnAutoSend
            // 
            this.btnAutoSend.Location = new System.Drawing.Point(351, 3);
            this.btnAutoSend.Name = "btnAutoSend";
            this.btnAutoSend.Size = new System.Drawing.Size(75, 23);
            this.btnAutoSend.TabIndex = 1;
            this.btnAutoSend.Text = "Auto Send";
            this.btnAutoSend.UseVisualStyleBackColor = true;
            this.btnAutoSend.Click += new System.EventHandler(this.btnAutoSend_Click);
            // 
            // btnDeleteSelected
            // 
            this.btnDeleteSelected.Location = new System.Drawing.Point(548, 3);
            this.btnDeleteSelected.Name = "btnDeleteSelected";
            this.btnDeleteSelected.Size = new System.Drawing.Size(111, 23);
            this.btnDeleteSelected.TabIndex = 2;
            this.btnDeleteSelected.Text = "Delete Selected";
            this.btnDeleteSelected.UseVisualStyleBackColor = true;
            this.btnDeleteSelected.Click += new System.EventHandler(this.btnDeleteSelected_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(665, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(746, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSendSelected
            // 
            this.btnSendSelected.Location = new System.Drawing.Point(432, 3);
            this.btnSendSelected.Name = "btnSendSelected";
            this.btnSendSelected.Size = new System.Drawing.Size(110, 23);
            this.btnSendSelected.TabIndex = 5;
            this.btnSendSelected.Text = "Send Selected";
            this.btnSendSelected.UseVisualStyleBackColor = true;
            this.btnSendSelected.Click += new System.EventHandler(this.btnSendSelected_Click);
            // 
            // menuStriptEmail
            // 
            this.menuStriptEmail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.emailNotificationsToolStripMenuItem});
            this.menuStriptEmail.Location = new System.Drawing.Point(0, 0);
            this.menuStriptEmail.Name = "menuStriptEmail";
            this.menuStriptEmail.Size = new System.Drawing.Size(963, 24);
            this.menuStriptEmail.TabIndex = 6;
            this.menuStriptEmail.Text = "menuStrip1";
            // 
            // emailNotificationsToolStripMenuItem
            // 
            this.emailNotificationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.emailNotificationsToolStripMenuItem1,
            this.sentEmailsToolStripMenuItem});
            this.emailNotificationsToolStripMenuItem.Name = "emailNotificationsToolStripMenuItem";
            this.emailNotificationsToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.emailNotificationsToolStripMenuItem.Text = "Email Processs";
            // 
            // emailNotificationsToolStripMenuItem1
            // 
            this.emailNotificationsToolStripMenuItem1.Name = "emailNotificationsToolStripMenuItem1";
            this.emailNotificationsToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.emailNotificationsToolStripMenuItem1.Text = "Email Configuration";
            this.emailNotificationsToolStripMenuItem1.Click += new System.EventHandler(this.emailConfigurationToolStripMenuItem_Click);
            // 
            // sentEmailsToolStripMenuItem
            // 
            this.sentEmailsToolStripMenuItem.Name = "sentEmailsToolStripMenuItem";
            this.sentEmailsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sentEmailsToolStripMenuItem.Text = "Sent Emails";
            this.sentEmailsToolStripMenuItem.Click += new System.EventHandler(this.sentEmailsToolStripMenuItem_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Location = new System.Drawing.Point(406, 208);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(132, 13);
            this.lblMsg.TabIndex = 7;
            this.lblMsg.Text = "No pending emails to send";
            // 
            // lblTotalRecord
            // 
            this.lblTotalRecord.AutoSize = true;
            this.lblTotalRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalRecord.Location = new System.Drawing.Point(837, 8);
            this.lblTotalRecord.Name = "lblTotalRecord";
            this.lblTotalRecord.Size = new System.Drawing.Size(0, 13);
            this.lblTotalRecord.TabIndex = 8;
            // 
            // PendingEmails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 514);
            this.Controls.Add(this.lblTotalRecord);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnSendSelected);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnDeleteSelected);
            this.Controls.Add(this.btnAutoSend);
            this.Controls.Add(this.grdPendingEmails);
            this.Controls.Add(this.menuStriptEmail);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStriptEmail;
            this.Name = "PendingEmails";
            this.Text = "Pending Emails";
            ((System.ComponentModel.ISupportInitialize)(this.grdPendingEmails)).EndInit();
            this.menuStriptEmail.ResumeLayout(false);
            this.menuStriptEmail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdPendingEmails;
        private System.Windows.Forms.Button btnAutoSend;
        private System.Windows.Forms.Button btnDeleteSelected;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSendSelected;
        private System.Windows.Forms.MenuStrip menuStriptEmail;
        private System.Windows.Forms.ToolStripMenuItem emailNotificationsToolStripMenuItem;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Label lblTotalRecord;
        private System.Windows.Forms.ToolStripMenuItem emailNotificationsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sentEmailsToolStripMenuItem;
    }
}