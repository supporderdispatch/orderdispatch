﻿namespace ODM.Appliction
{
    partial class EmailConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailConfiguration));
            this.grpbxTemplate = new System.Windows.Forms.GroupBox();
            this.grdTemplates = new System.Windows.Forms.DataGridView();
            this.grpbxSMTP = new System.Windows.Forms.GroupBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.chkAuthSMTP = new System.Windows.Forms.CheckBox();
            this.chkSSL = new System.Windows.Forms.CheckBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.lblPort = new System.Windows.Forms.Label();
            this.txtSMTPServer = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.lblService = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.lblReplyToMail = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.lblFromName = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.lblSendCopyTo = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.lblDelay = new System.Windows.Forms.Label();
            this.btnSendTestMail = new System.Windows.Forms.Button();
            this.btnAdditonalEmail = new System.Windows.Forms.Button();
            this.grpbxTemplate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTemplates)).BeginInit();
            this.grpbxSMTP.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpbxTemplate
            // 
            this.grpbxTemplate.Controls.Add(this.grdTemplates);
            this.grpbxTemplate.Location = new System.Drawing.Point(0, 226);
            this.grpbxTemplate.Name = "grpbxTemplate";
            this.grpbxTemplate.Size = new System.Drawing.Size(850, 371);
            this.grpbxTemplate.TabIndex = 0;
            this.grpbxTemplate.TabStop = false;
            this.grpbxTemplate.Text = "Templates";
            // 
            // grdTemplates
            // 
            this.grdTemplates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdTemplates.Location = new System.Drawing.Point(0, 16);
            this.grdTemplates.Name = "grdTemplates";
            this.grdTemplates.Size = new System.Drawing.Size(850, 355);
            this.grdTemplates.TabIndex = 0;
            this.grdTemplates.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdTemplates_CellDoubleClick);
            this.grdTemplates.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdTemplates_CellFormatting);
            // 
            // grpbxSMTP
            // 
            this.grpbxSMTP.Controls.Add(this.txtPassword);
            this.grpbxSMTP.Controls.Add(this.txtUserName);
            this.grpbxSMTP.Controls.Add(this.lblPassword);
            this.grpbxSMTP.Controls.Add(this.lblUserName);
            this.grpbxSMTP.Controls.Add(this.chkAuthSMTP);
            this.grpbxSMTP.Controls.Add(this.chkSSL);
            this.grpbxSMTP.Controls.Add(this.txtPort);
            this.grpbxSMTP.Controls.Add(this.lblPort);
            this.grpbxSMTP.Controls.Add(this.txtSMTPServer);
            this.grpbxSMTP.Controls.Add(this.lblServer);
            this.grpbxSMTP.Location = new System.Drawing.Point(58, 3);
            this.grpbxSMTP.Name = "grpbxSMTP";
            this.grpbxSMTP.Size = new System.Drawing.Size(530, 100);
            this.grpbxSMTP.TabIndex = 1;
            this.grpbxSMTP.TabStop = false;
            this.grpbxSMTP.Text = "SMTP Settings";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(394, 74);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(100, 20);
            this.txtPassword.TabIndex = 10;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(231, 74);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(100, 20);
            this.txtUserName.TabIndex = 9;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(340, 78);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 8;
            this.lblPassword.Text = "Password";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(170, 78);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(60, 13);
            this.lblUserName.TabIndex = 7;
            this.lblUserName.Text = "User Name";
            // 
            // chkAuthSMTP
            // 
            this.chkAuthSMTP.AutoSize = true;
            this.chkAuthSMTP.Location = new System.Drawing.Point(10, 77);
            this.chkAuthSMTP.Name = "chkAuthSMTP";
            this.chkAuthSMTP.Size = new System.Drawing.Size(157, 17);
            this.chkAuthSMTP.TabIndex = 6;
            this.chkAuthSMTP.Text = "Using SMTP Authentication";
            this.chkAuthSMTP.UseVisualStyleBackColor = true;
            // 
            // chkSSL
            // 
            this.chkSSL.AutoSize = true;
            this.chkSSL.Location = new System.Drawing.Point(426, 46);
            this.chkSSL.Name = "chkSSL";
            this.chkSSL.Size = new System.Drawing.Size(68, 17);
            this.chkSSL.TabIndex = 5;
            this.chkSSL.Text = "Use SSL";
            this.chkSSL.UseVisualStyleBackColor = true;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(84, 47);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(99, 20);
            this.txtPort.TabIndex = 3;
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(9, 50);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(26, 13);
            this.lblPort.TabIndex = 2;
            this.lblPort.Text = "Port";
            // 
            // txtSMTPServer
            // 
            this.txtSMTPServer.Location = new System.Drawing.Point(84, 17);
            this.txtSMTPServer.Name = "txtSMTPServer";
            this.txtSMTPServer.Size = new System.Drawing.Size(410, 20);
            this.txtSMTPServer.TabIndex = 1;
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(7, 20);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(71, 13);
            this.lblServer.TabIndex = 0;
            this.lblServer.Text = "SMTP Server";
            // 
            // lblService
            // 
            this.lblService.AutoSize = true;
            this.lblService.Location = new System.Drawing.Point(0, 10);
            this.lblService.Name = "lblService";
            this.lblService.Size = new System.Drawing.Size(43, 13);
            this.lblService.TabIndex = 2;
            this.lblService.Text = "Service";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(142, 114);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(172, 20);
            this.textBox3.TabIndex = 12;
            // 
            // lblReplyToMail
            // 
            this.lblReplyToMail.AutoSize = true;
            this.lblReplyToMail.Location = new System.Drawing.Point(55, 118);
            this.lblReplyToMail.Name = "lblReplyToMail";
            this.lblReplyToMail.Size = new System.Drawing.Size(76, 13);
            this.lblReplyToMail.TabIndex = 11;
            this.lblReplyToMail.Text = " Reply to email";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(142, 140);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(172, 20);
            this.textBox4.TabIndex = 14;
            // 
            // lblFromName
            // 
            this.lblFromName.AutoSize = true;
            this.lblFromName.Location = new System.Drawing.Point(55, 144);
            this.lblFromName.Name = "lblFromName";
            this.lblFromName.Size = new System.Drawing.Size(61, 13);
            this.lblFromName.TabIndex = 13;
            this.lblFromName.Text = "From Name";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(142, 166);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(172, 20);
            this.textBox5.TabIndex = 16;
            // 
            // lblSendCopyTo
            // 
            this.lblSendCopyTo.AutoSize = true;
            this.lblSendCopyTo.Location = new System.Drawing.Point(55, 170);
            this.lblSendCopyTo.Name = "lblSendCopyTo";
            this.lblSendCopyTo.Size = new System.Drawing.Size(75, 13);
            this.lblSendCopyTo.TabIndex = 15;
            this.lblSendCopyTo.Text = "Send Copy To";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(264, 194);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(50, 20);
            this.textBox6.TabIndex = 18;
            // 
            // lblDelay
            // 
            this.lblDelay.AutoSize = true;
            this.lblDelay.Location = new System.Drawing.Point(55, 198);
            this.lblDelay.Name = "lblDelay";
            this.lblDelay.Size = new System.Drawing.Size(173, 13);
            this.lblDelay.TabIndex = 17;
            this.lblDelay.Text = "Delay between send in outbox(sec)";
            // 
            // btnSendTestMail
            // 
            this.btnSendTestMail.Location = new System.Drawing.Point(401, 160);
            this.btnSendTestMail.Name = "btnSendTestMail";
            this.btnSendTestMail.Size = new System.Drawing.Size(151, 23);
            this.btnSendTestMail.TabIndex = 19;
            this.btnSendTestMail.Text = "Send Test Mail";
            this.btnSendTestMail.UseVisualStyleBackColor = true;
            this.btnSendTestMail.Click += new System.EventHandler(this.btnSendTestMail_Click);
            // 
            // btnAdditonalEmail
            // 
            this.btnAdditonalEmail.Location = new System.Drawing.Point(401, 193);
            this.btnAdditonalEmail.Name = "btnAdditonalEmail";
            this.btnAdditonalEmail.Size = new System.Drawing.Size(151, 23);
            this.btnAdditonalEmail.TabIndex = 20;
            this.btnAdditonalEmail.Text = "Additional Email Accounts";
            this.btnAdditonalEmail.UseVisualStyleBackColor = true;
            this.btnAdditonalEmail.Click += new System.EventHandler(this.btnAdditonalEmail_Click);
            // 
            // EmailConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 599);
            this.Controls.Add(this.btnAdditonalEmail);
            this.Controls.Add(this.btnSendTestMail);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.lblDelay);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.lblSendCopyTo);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.lblFromName);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.lblService);
            this.Controls.Add(this.lblReplyToMail);
            this.Controls.Add(this.grpbxSMTP);
            this.Controls.Add(this.grpbxTemplate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmailConfiguration";
            this.Text = "Email Sender Configuration";
            this.grpbxTemplate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdTemplates)).EndInit();
            this.grpbxSMTP.ResumeLayout(false);
            this.grpbxSMTP.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpbxTemplate;
        private System.Windows.Forms.DataGridView grdTemplates;
        private System.Windows.Forms.GroupBox grpbxSMTP;
        private System.Windows.Forms.Label lblService;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.TextBox txtSMTPServer;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.CheckBox chkSSL;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.CheckBox chkAuthSMTP;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label lblReplyToMail;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label lblFromName;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label lblSendCopyTo;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label lblDelay;
        private System.Windows.Forms.Button btnSendTestMail;
        private System.Windows.Forms.Button btnAdditonalEmail;

    }
}