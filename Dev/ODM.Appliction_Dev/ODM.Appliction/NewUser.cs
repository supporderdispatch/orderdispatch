﻿using ODM.Data.Service;
using System;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class NewUser : BaseForm
    {
        public NewUser()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            IUserService userService = new UserService(ConfigString);
            string result = userService.AddUser(txtLogin.Text, "test", txtFirstName.Text, txtLastName.Text, txtEmail.Text, txtPhone.Text, chkIsAdmin.Checked, LoginedUser.Userid);
            if (!string.IsNullOrEmpty(result))
            {
                lblError.Text = result;
                txtLogin.Focus();
            }
            else
            {
                MessageBox.Show("User Sucessfully created!");
                txtLogin.Text = string.Empty;
                txtEmail.Text = string.Empty;
                txtFirstName.Text = string.Empty;
                txtLastName.Text = string.Empty;
                txtPhone.Text = string.Empty;
                lblError.Text = string.Empty;
                txtLogin.Focus();
            }
        }

        private void NewUser_Load(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
