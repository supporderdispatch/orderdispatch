﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class DeleteUsers : Form
    {
        public User userDetails { get; set; }
        string strUserIds = "";
        public DeleteUsers()
        {
            InitializeComponent();
        }
        public DeleteUsers(User objUser)
        {
            InitializeComponent();
            userDetails = objUser;
            BindUsersGrid(false);
        }
        private void BindUsersGrid(bool IsRebind)
        {
            try
            {
                DataTable objDt = new DataTable();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                SqlParameter[] objUsers = new SqlParameter[2];

                objUsers[0] = new SqlParameter("@UserID", SqlDbType.UniqueIdentifier);
                objUsers[0].Value = userDetails.Userid;

                objUsers[1] = new SqlParameter("@Action", SqlDbType.VarChar, 2);
                objUsers[1].Value = "D";

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetUsersList", objUsers).Tables[0];
                if (objDt != null)
                {
                    grdDeleteUser.DataSource = objDt;
                    grdDeleteUser.AllowUserToAddRows = false;

                    for (int i = 0; i < objDt.Columns.Count; i++)
                    {
                        grdDeleteUser.Columns[i].ReadOnly = true;
                        if (objDt.Columns[i].ColumnName == "Userid")
                        {
                            grdDeleteUser.Columns[i].Width = 250;
                        }
                    }
                    if (!IsRebind)
                    {
                        DataGridViewCheckBoxColumn chkColumn = new DataGridViewCheckBoxColumn();
                        chkColumn.HeaderText = "";
                        chkColumn.Width = 30;
                        chkColumn.Name = "chkSelectUser";
                        grdDeleteUser.Columns.Insert(0, chkColumn);
                    }
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }
        private void btnDeleteUsers_Click(object sender, EventArgs e)
        {
            if (GetSelectedUserId())
            {
                DialogResult result = MessageBox.Show("Are you sure, you want to delete User(s)?", "Delete User", MessageBoxButtons.YesNo);
                if (result.Equals(DialogResult.Yes))
                {
                    SqlConnection connection = new SqlConnection();
                    connection.ConnectionString = MakeConnection.GetConnection();
                    connection.Open();
                    SqlParameter[] objUsers = new SqlParameter[1];
                    objUsers[0] = new SqlParameter("@UserIds", SqlDbType.VarChar, 5000);
                    objUsers[0].Value = strUserIds;
                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "DeleteUsers", objUsers);
                    BindUsersGrid(true);
                    MessageBox.Show("User(s) Deleted Successfully !");
                }
            }
            else
            {
                MessageBox.Show("Please select at least one user to delete");
            }
        }
        private bool GetSelectedUserId()
        {
            int i = 0;
            string strUserID = "";
            bool blIsAnyRowOrderItemSelected = false;
            foreach (DataGridViewRow row in grdDeleteUser.Rows)
            {
                bool IsSelected = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells[0]).Value);
                if (IsSelected)
                {
                    blIsAnyRowOrderItemSelected = true;
                    if (strUserID == "")
                    {
                        strUserID = grdDeleteUser.Rows[i].Cells[1].Value.ToString();
                    }
                    else
                    {
                        strUserID = strUserID + "," + grdDeleteUser.Rows[i].Cells[1].Value.ToString();
                    }
                }
                i++;
            }
            strUserIds = strUserID;
            return blIsAnyRowOrderItemSelected;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
