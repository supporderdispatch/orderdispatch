﻿namespace ODM.Appliction
{
    partial class EbayAccountAppKeys
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdEbayAccountAppKeys = new System.Windows.Forms.DataGridView();
            this.btnAddNewKeys = new System.Windows.Forms.Button();
            this.btnAuthorizeEbay = new System.Windows.Forms.Button();
            this.btnEbayUsersList = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdEbayAccountAppKeys)).BeginInit();
            this.SuspendLayout();
            // 
            // grdEbayAccountAppKeys
            // 
            this.grdEbayAccountAppKeys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEbayAccountAppKeys.Location = new System.Drawing.Point(0, 140);
            this.grdEbayAccountAppKeys.Name = "grdEbayAccountAppKeys";
            this.grdEbayAccountAppKeys.Size = new System.Drawing.Size(689, 309);
            this.grdEbayAccountAppKeys.TabIndex = 0;
            this.grdEbayAccountAppKeys.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdEbayAccountAppKeys_CellMouseDoubleClick);
            // 
            // btnAddNewKeys
            // 
            this.btnAddNewKeys.Location = new System.Drawing.Point(30, 31);
            this.btnAddNewKeys.Name = "btnAddNewKeys";
            this.btnAddNewKeys.Size = new System.Drawing.Size(75, 23);
            this.btnAddNewKeys.TabIndex = 1;
            this.btnAddNewKeys.Text = "Add New Keys";
            this.btnAddNewKeys.UseVisualStyleBackColor = true;
            this.btnAddNewKeys.Click += new System.EventHandler(this.btnAddNewKeys_Click);
            // 
            // btnAuthorizeEbay
            // 
            this.btnAuthorizeEbay.Location = new System.Drawing.Point(286, 31);
            this.btnAuthorizeEbay.Name = "btnAuthorizeEbay";
            this.btnAuthorizeEbay.Size = new System.Drawing.Size(88, 23);
            this.btnAuthorizeEbay.TabIndex = 2;
            this.btnAuthorizeEbay.Text = "Authorize Ebay";
            this.btnAuthorizeEbay.UseVisualStyleBackColor = true;
            this.btnAuthorizeEbay.Click += new System.EventHandler(this.btnAuthorizeEbay_Click);
            // 
            // btnEbayUsersList
            // 
            this.btnEbayUsersList.Location = new System.Drawing.Point(498, 30);
            this.btnEbayUsersList.Name = "btnEbayUsersList";
            this.btnEbayUsersList.Size = new System.Drawing.Size(91, 23);
            this.btnEbayUsersList.TabIndex = 3;
            this.btnEbayUsersList.Text = "Ebay Users List";
            this.btnEbayUsersList.UseVisualStyleBackColor = true;
            this.btnEbayUsersList.Click += new System.EventHandler(this.btnEbayUsersList_Click);
            // 
            // EbayAccountAppKeys
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 451);
            this.Controls.Add(this.btnEbayUsersList);
            this.Controls.Add(this.btnAuthorizeEbay);
            this.Controls.Add(this.btnAddNewKeys);
            this.Controls.Add(this.grdEbayAccountAppKeys);
            this.Name = "EbayAccountAppKeys";
            this.Text = "EbayAccountAppKeys";
            ((System.ComponentModel.ISupportInitialize)(this.grdEbayAccountAppKeys)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grdEbayAccountAppKeys;
        private System.Windows.Forms.Button btnAddNewKeys;
        private System.Windows.Forms.Button btnAuthorizeEbay;
        private System.Windows.Forms.Button btnEbayUsersList;
    }
}