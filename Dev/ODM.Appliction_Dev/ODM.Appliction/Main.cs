﻿using MarketplaceWebServiceOrders;
using MarketplaceWebServiceOrders.Model;
using MWSMerchantFulfillmentService;
using MWSMerchantFulfillmentService.Model;
using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Collections;
using System.Threading;
using System.Windows.Forms;
using ODM.Core.UserTracking;
using System.Threading.Tasks;
using System.Linq;

namespace ODM.Appliction
{
    public partial class Main : BaseForm
    {
        public static Form globalForm;
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        PDFLib objPdf = new PDFLib();
        public Main()
        {
            InitializeComponent();
            GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
        }
        public Main(User loginUser)
        {
            InitializeComponent();
            globalForm = new Form();
            globalForm.Show();
            globalForm.Hide();
            GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
            GlobalVariablesAndMethods.UserDetails = loginUser;
            this.lblUserName.Text = String.Format("{0} {1}", loginUser.Firstname,loginUser.Lastname);
            this.FormClosed+=Main_FormClosed;
            LoadUserStatistics(loginUser.Userid);
            LoadAppVersion();
        }

        private void LoadAppVersion()
        {
            lblAppVersion.Text = "v" + Application.ProductVersion.ToString();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            UserTracker.EndTracking();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            var loginName=GlobalVariablesAndMethods.UserDetails.Loginname.ToLower();
            if (loginName.Equals("oliver") || loginName.Equals("admin"))
            {
                rootToolStripMenuItem.Visible = true;
            }
            if (GlobalVariablesAndMethods.UserDetails.IsAdmin)
            {
                usersToolStripMenuItem.Visible = true;
                settingsToolStripMenuItem.Visible = true;
                courierIntegrationToolStripMenuItem.Visible = true;
                GlobalVar.GetPrinterNameByIpAddress(); //Get and set the printer name to all courier services.
            }
            else
            {
                usersToolStripMenuItem.Visible = false;
                settingsToolStripMenuItem.Visible = false;
                courierIntegrationToolStripMenuItem.Visible = false;
            }
            ODM.Appliction.Updates.Updator.CheckForUpdates();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewUser newUser = new NewUser();
            newUser.ShowDialog();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResetPassword uPass = new ResetPassword();
            uPass.ShowDialog();
        }

        private void dispatchOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmGetOrderData = Application.OpenForms["GetOrderData"];
            if (frmGetOrderData == null)
            {
                GetOrderData objOrderData = new GetOrderData();
                objOrderData.Show();
            }
            else
            {
                frmGetOrderData.Focus();
            }
        }

        private void deleteUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteUsers frmDelete = new DeleteUsers(GlobalVariablesAndMethods.UserDetails);
            frmDelete.Show();
        }

        private void editUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UsersList frmUsersList = new UsersList();
            frmUsersList.Show();
        }

        private void urgentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmUrgntOrder = Application.OpenForms["UrgentOrderSetting"];
            if (frmUrgntOrder == null)
            {
                UrgentOrderSetting objUrgentOrders = new UrgentOrderSetting();
                objUrgentOrders.Show();
            }
            else
            {
                frmUrgntOrder.Focus();
            }
        }

        private void courierIntegrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmCouriersSettings = Application.OpenForms["CouriersSettings"];
            if (frmCouriersSettings == null)
            {
                CouriersSettings objCourier = new CouriersSettings();
                objCourier.Show();
            }
            else
            {
                frmCouriersSettings.Focus();
            }
        }

        private void generalSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmGeneralSettings = Application.OpenForms["GeneralSettings"];
            if (frmGeneralSettings == null)
            {
                GeneralSettings objGeneralSettings = new GeneralSettings();
                objGeneralSettings.Show();
            }
            else
            {
                frmGeneralSettings.Focus();
            }
        }

        private void allOpenOrdersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmAllOpenOrders = Application.OpenForms["AllOpenOrders"];
            if (frmAllOpenOrders == null)
            {
                AllOpenOrders objAllOpenOrders = new AllOpenOrders();
                objAllOpenOrders.Show();
            }
            else
            {
                frmAllOpenOrders.Focus();
            }
        }

        private void orderBookToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form frmAllOrders = Application.OpenForms["AllOrders"];
            if (frmAllOrders == null)
            {
                AllOrders objAllOrders = new AllOrders();
                objAllOrders.Show();
            }
            else
            {
              
                frmAllOrders.WindowState = FormWindowState.Normal;
                frmAllOrders.Focus();
            }
        }

        private void emailSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread objPendingMail = new Thread(OpenPendingEmailScreen);
            objPendingMail.Start();
            //OpenPendingEmailScreen();
        }

        private void orderProcessStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmCheckOrderStatus = Application.OpenForms["CheckOrderStatus"];
            if (frmCheckOrderStatus == null)
            {
                CheckOrderStatus objCheckOrderStatus = new CheckOrderStatus();
                objCheckOrderStatus.Show();
            }
            else
            {
                frmCheckOrderStatus.Focus();
            }
        }

        private void viewOrderLogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmViewLogHistory = Application.OpenForms["ViewLogHistory"];
            if (frmViewLogHistory == null)
            {
                ViewLogHistory objViewLogHistory = new ViewLogHistory();
                objViewLogHistory.Show();
            }
            else
            {
                frmViewLogHistory.Focus();
            }
        }
        private void OpenPendingEmailScreen()
        {
            Form frmPendingEmails = Application.OpenForms["PendingEmails"];
            if (frmPendingEmails == null)
            {
                //CheckForIllegalCrossThreadCalls = false;
                PendingEmails objPendingEmails = new PendingEmails();
                globalForm.Invoke((MethodInvoker)delegate()
                {
                    objPendingEmails.Show();
                });
                //objPendingEmails.Show();

            }
            else
            {
                frmPendingEmails.Focus();
            }
        }

        private void channelIntegrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmMessageFolders = Application.OpenForms["MessageFolders"];
            if (frmMessageFolders == null)
            {
                MessageFolders objMessageFolders = new MessageFolders();
                objMessageFolders.Show();
            }
            else
            {
                frmMessageFolders.Focus();
            }
        }
        private void LoadUserStatistics(Guid userid)
        {
            this.lblDateOfLog.Visible = false;
            this.lblTotalLoggedInTime.Visible = false;
            this.lblAvgPerOrderTimeTaken.Visible = false;
            this.lblTotalOrdersProcessed.Visible = false;
            this.lblOrderPerHourRate.Visible = false;

            var index = 0;
            var incUnit = 25;
            var listOfFinalDigestView = new ODM.Core.Logger().GetUserStatisticsOfPrevDays(userid);
            foreach (var finalDigest in listOfFinalDigestView)
            {
                var lbldateOfLog = new System.Windows.Forms.Label() { Left = lblDateOfLog.Left, Top = lblDateOfLog.Top + (index * incUnit),Width=80 };
                var lbltotalLoggedInTime = new System.Windows.Forms.Label() { Left = lblTotalLoggedInTime.Left, Top = lblTotalLoggedInTime.Top + (index * incUnit) };
                var lblavgperorderTimeTaken = new System.Windows.Forms.Label() { Left = lblAvgPerOrderTimeTaken.Left, Top = lblAvgPerOrderTimeTaken.Top + (index * incUnit) };
                var lblorderPerHouravgRate = new System.Windows.Forms.Label() { Left = lblOrderPerHourRate.Left, Top = lblOrderPerHourRate.Top + (index * incUnit) };
                var lblordersProcessed = new System.Windows.Forms.Label() { Left = lblTotalOrdersProcessed.Left, Top = lblTotalOrdersProcessed.Top + (index * incUnit) };

                var liTime = finalDigest.TotalLoggedInTime;//logged in time

                lbldateOfLog.Text = finalDigest.OfDate.ToString("MMM dd");               
                lbltotalLoggedInTime.Text = String.Format("{0:00}:{1:00}:{2:00}", liTime.Hours, liTime.Minutes, liTime.Seconds);              
                lblavgperorderTimeTaken.Text = String.Format("{0:0.00}", ODM.Core.Helpers.TimerUtils.GetMinutes(Convert.ToDouble(finalDigest.PerOrderAvgProcessTime)));             
                lblorderPerHouravgRate.Text = String.Format("{0:0.00}", finalDigest.HourlyPerOrderProcessRate);               
                lblordersProcessed.Text = String.Format("{0}", finalDigest.TotalOrderProcessed);


                index++;

                this.panel1.Controls.Add(lbldateOfLog);
                this.panel1.Controls.Add(lbltotalLoggedInTime);
                this.panel1.Controls.Add(lblavgperorderTimeTaken);
                this.panel1.Controls.Add(lblorderPerHouravgRate);
                this.panel1.Controls.Add(lblordersProcessed);
            }

        }

        private void userStatisticsReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form userStatisticsReport = Application.OpenForms["UserStatisticsReport"];
            if (userStatisticsReport == null)
            {
                UserStatisticsReport statisticsReport = new UserStatisticsReport();
                statisticsReport.Show();
            }
            else
            {
                userStatisticsReport.Focus();
            }
        }

        private void invoiceSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form invoiceSettings = Application.OpenForms["InvoiceSettings"];
            if (invoiceSettings == null)
            {
                var invoiceSettingsForm = new InvoiceSettings();
                invoiceSettingsForm.Show();
            }
            else
            {
                invoiceSettings.Focus();
            }
        }

        private void emailSettingsNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form email = Application.OpenForms["EmailTemplatesSetting"];
            if (email == null)
            {
                var emails = new ODM.Appliction.EmailSender.Emails();
                emails.Show();
            }
            else
            {
                email.Focus();
            }
        }

        private void updateManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pushUpdateForm = Application.OpenForms["PushUpdates"];
            if (pushUpdateForm != null)
            {
                pushUpdateForm.Focus();
            }
            else
            {
                var pushUpdates = new ODM.Appliction.Updates.PushUpdates();
                pushUpdates.Show();
            }
        }

        private void customerFeaturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var customers = Application.OpenForms["Customers"];
            if (customers == null)
            {
                var frmCustomers = new Customers.Customers();
                frmCustomers.Show();
            }
            else
            {
                customers.Focus();
            }
        }

        private void dashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dashboard = Application.OpenForms["Dashboard"];
            if (dashboard == null)
            {
                var dashboardForm = new Dashboard();
                dashboardForm.Show();
            }
            else
            {
                dashboard.Focus();
            }
        }

    }
}
