﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Data.Util;
namespace ODM.Appliction
{
    public partial class MessageAccountSettings : Form
    {
        bool isEbay = false;
        string strMode = "";
        public MessageAccountSettings()
        {
            InitializeComponent();
            rdbtnEmail.Checked = true;
            if (rdbtnEbay.Checked)
            {
                isEbay = true;
                strMode = "EBAY";
            }
        }
        private void PopulateControls()
        {
            grdMessageAccounts.EditMode = DataGridViewEditMode.EditProgrammatically;
            grdMessageAccounts.Columns["IsEbay"].Visible = false;
            grdMessageAccounts.Columns["SSL"].Visible = false;
            grdMessageAccounts.Columns["Port"].Visible = false;
            grdMessageAccounts.Columns["ID"].Visible = false;
            if (strMode.ToUpper() == "EBAY")
            {
                grdMessageAccounts.Columns["HostName"].Visible = false;
                grdMessageAccounts.Columns["SmtpHostName"].Visible = false;
            }
            else
            {
                grdMessageAccounts.Columns["HostName"].Visible = true;
                grdMessageAccounts.Columns["SmtpHostName"].Visible = true;
                grdMessageAccounts.Columns["IsEbaySandbox"].Visible = false;
            }
        }
        private void BindGridForAccounts(string strMode)
        {
            DataTable objDt = new DataTable();
            objDt = GlobalVariablesAndMethods.GetMessageAccount(strMode);
            grdMessageAccounts.DataSource = objDt;
            if (objDt.Rows.Count > 0)
            {
                lblNoRecordError.Visible = false;
                PopulateControls();
            }
            else
            {
                lblNoRecordError.Visible = true;
            }
        }

        private void rdbtnEmail_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbtnEmail.Checked)
            {
                isEbay = false;
                strMode = "EMAIL";
            }
            else
            {
                isEbay = true;
                strMode = "EBAY";
            }
        }

        private void btnAddNewAccount_Click(object sender, EventArgs e)
        {

            AddMessageAccount objAddMessageAccount = new AddMessageAccount("INSERT", Guid.NewGuid());
            objAddMessageAccount.ShowDialog();
            BindGridForAccounts(strMode);


        }

        private void rdbtnEbay_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbtnEbay.Checked)
            {
                isEbay = true;
                strMode = "EBAY";
            }
            else
            {
                isEbay = false;
                strMode = "EMAIL";
            }
        }

        private void grdMessageAccounts_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AddMessageAccount objMessageAccount = new AddMessageAccount("UPDATE", new Guid(grdMessageAccounts.CurrentRow.Cells["ID"].Value.ToString()), grdMessageAccounts.CurrentRow.Cells["AccountName"].Value.ToString(), grdMessageAccounts.CurrentRow.Cells["UserName"].Value.ToString(), grdMessageAccounts.CurrentRow.Cells["Password"].Value.ToString(), grdMessageAccounts.CurrentRow.Cells["HostName"].Value.ToString(), Convert.ToBoolean(grdMessageAccounts.CurrentRow.Cells["IsEbay"].Value.ToString()), grdMessageAccounts.CurrentRow.Cells["SmtpHostName"].Value.ToString(), Convert.ToBoolean(grdMessageAccounts.CurrentRow.Cells["IsEbaySandbox"].Value.ToString()));
            objMessageAccount.ShowDialog();
            BindGridForAccounts(strMode);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindGridForAccounts(strMode);
            btnDeleteMessageAccount.Visible = true;
        }

        private void btnDeleteMessageAccount_Click(object sender, EventArgs e)
        {

            DialogResult objDResult = MessageBox.Show("Are you sure you want to Delete this Account ?", "Delete Account", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (objDResult == DialogResult.Yes)
            {
                AddMessageAccount objMessageAccount = new AddMessageAccount("DELETE", new Guid(grdMessageAccounts.CurrentRow.Cells["ID"].Value.ToString()), grdMessageAccounts.CurrentRow.Cells["AccountName"].Value.ToString(), grdMessageAccounts.CurrentRow.Cells["UserName"].Value.ToString(), grdMessageAccounts.CurrentRow.Cells["Password"].Value.ToString(), grdMessageAccounts.CurrentRow.Cells["HostName"].Value.ToString(), Convert.ToBoolean(grdMessageAccounts.CurrentRow.Cells["IsEbay"].Value.ToString()), grdMessageAccounts.CurrentRow.Cells["SmtpHostName"].Value.ToString(), Convert.ToBoolean(grdMessageAccounts.CurrentRow.Cells["IsEbaySandbox"].Value.ToString()));
                objMessageAccount.AddUpdateDeleteMessageAccounts(new Guid(grdMessageAccounts.CurrentRow.Cells["ID"].Value.ToString()), "DELETE");
                BindGridForAccounts(strMode);

            }
        }

        private void btnEbayAccountSettings_Click(object sender, EventArgs e)
        {
            EbayAccountAppKeys objEbayAccountKeys = new EbayAccountAppKeys();
            objEbayAccountKeys.ShowDialog();

        }
    }
}
