﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using S22.Imap;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace ODM.Appliction
{
    public partial class MessageFolders : Form
    {
        private int _intTotalMessages = 0, _intReadMessages = 0, _intUnreadMessages = 0;
        private int _intPageSize = 0;
        private int _intPageCount = 0;
        private int _intCurrentPage = 1;
        int _PageSize = 50;
        public string _strMode, _strFolderID, _strFolderName;
        string[] _strArrGrdSelectedValues;
        //static MessageFolders frmMessageFolders;
        DataTable _objDtMessagesList = new DataTable();
        DataTable _objDtRules = new DataTable();
        DataTable _objMessageAccountsForEmails = new DataTable();
        public MessageFolders(string strMode)
        {
            InitializeComponent();
        }
        public MessageFolders()
        {
            InitializeComponent();
            BindGridForFolders();
            //GetAllRules();
        }
        private void MakeDefaultFolderToCatchUnmatachedEmails()
        {
            string strDefaultFolderName = GlobalVariablesAndMethods.UserDetails.Firstname + "'s Assigned Messages";
            string strDefaultFolderForEachUserResult = GetUpdateMessageFolders("INSERT", Guid.NewGuid(), GlobalVariablesAndMethods.UserDetails.Userid.ToString(), strDefaultFolderName);
            string strResult = GetUpdateMessageFolders("INSERT", new Guid("00000000-0000-0000-0000-000000000000"), "00000000-0000-0000-0000-000000000000", "All Unmatched Emails");
            //strResult means value is inserted (new folder is added in DB)
            //if (strResult == "1" && strDefaultFolderForEachUserResult == "1")
            //{
            //    BindGridForFolders();
            //}
            //else if (strResult == "2")
            //{
            //    // MessageBox.Show("Folder Already Exist.", "Add New Folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //BindGridForFolders();
        }
        public DataTable GetNumberOfMessagesInFolder()
        {
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@CreatedBy", SqlDbType.UniqueIdentifier);
                objParam[0].Value = GlobalVariablesAndMethods.UserDetails.Userid;
                objDt = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetNumberOfMessagesInFolders", objParam).Tables[0];
                return objDt;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally { conn.Close(); }

        }
        public void BindGridForFolders()
        {
            MakeDefaultFolderToCatchUnmatachedEmails();
            DataTable objDt = new DataTable();
            //string strMode;
            //strMode = "GET";
            //objDt = GetUpdateMessageFolders(strMode.ToUpper(), new Guid(), GlobalVariablesAndMethods.UserDetails.Userid.ToString());
            objDt = GetNumberOfMessagesInFolder();
            if (objDt.Rows.Count > 0)
            {
                grdMessageFolders.EditMode = DataGridViewEditMode.EditProgrammatically;
                grdMessageFolders.AllowUserToAddRows = false;
                grdMessageFolders.DataSource = objDt;
                grdMessageFolders.Columns["ID"].Visible = false;
                grdMessageFolders.Columns["CreatedOn"].Visible = false;
                grdMessageFolders.Columns["CreatedBy"].Visible = false;
                grdMessageFolders.Columns["ModifiedOn"].Visible = false;
            }
        }

        public dynamic GetUpdateMessageFolders(string strMode, Guid FolderID, string CreatedBy, string strFolderName = "")
        {

            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            string strResult;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[5];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objParam[1] = new SqlParameter("@FolderName", SqlDbType.VarChar, 50);
                objParam[1].Value = strFolderName;

                objParam[2] = new SqlParameter("@FolderID", SqlDbType.UniqueIdentifier);
                objParam[2].Value = FolderID;

                objParam[3] = new SqlParameter("@CreatedBy", SqlDbType.UniqueIdentifier);
                objParam[3].Value = new Guid(CreatedBy);

                objParam[4] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[4].Direction = ParameterDirection.Output;
                if (strMode.ToUpper() == "GET" || strMode.ToUpper() == "GETALL")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateDeleteMessageFolder", objParam);
                }
                else if (strMode.ToUpper() == "UPDATE" || strMode.ToUpper() == "INSERT"|| strMode.ToUpper()=="DELETE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateDeleteMessageFolder", objParam);
                }
                strResult = objParam[4].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                // For select query result equals to 0
                else if (strResult == "0" || strResult == "3")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];

                    }
                    return objDt;
                }
                return strResult;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        private void btnAddFolder_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFolderName.Text.ToString()))
            {
                lblError.Visible = false;
                DataTable objDt = new DataTable();
                _strMode = "INSERT";
                string strResult = GetUpdateMessageFolders(_strMode, Guid.NewGuid(), GlobalVariablesAndMethods.UserDetails.Userid.ToString(), txtFolderName.Text.ToString());
                //strResult means value is inserted (new folder is added in DB)
                if (strResult == "1")
                {
                    BindGridForFolders();
                    txtFolderName.Text = "";
                }
                else if (strResult == "2")
                {
                    MessageBox.Show("Folder Already Exist.", "Add New Folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                lblError.Visible = true;
            }
        }

        //private void GetAllRules()
        //{
        //    string strTestID = "00000000-0000-0000-0000-000000000000";

        //    _objDtRules = GlobalVariablesAndMethods.GetAddUpdateRules("GET", new Guid(), new Guid(), "", "", 1, true, strTestID);
        //}

        private void GetMessageAccounts()
        {

            DataTable objAllMessageAccount = new DataTable();
            //DataTable objMessageAccounts = new DataTable();

            objAllMessageAccount = GlobalVariablesAndMethods.GetMessageAccount("ALL");

        }

        //public void GetUnseenMessagesForGmail()   
        //{
        //   AddUpdateEmailMessagesToDB();
        //}

        public void BindGridForMessageList(string strFolderName, string strFolderID, int PageIndex = 1, int PageSize = 50, bool IsReadMessages = false, int intSkip = 0)
        {
            lblFolderNameValue.Text = strFolderName;
            MailMessage Mail = new MailMessage();
            int intMessageUID = 0;
            _objDtMessagesList = GlobalVariablesAndMethods.GetAddUpdateMessages(Mail, "GET", intMessageUID, "GMAIL", "", "", "", strFolderID, new Guid(), PageIndex, PageSize, IsReadMessages, intSkip);
            //ShowReadMessages(chkShowReadMessages.Checked);
            PopulatedGrid();
        }
        public void PopulatedGrid()
        {

            if (!(_objDtMessagesList.Rows.Count > 0))
            {
                lblNoRecordError.Visible = true;
                grdMessageList.Hide();
            }
            else
            {
                //  this.PopulatePager(Convert.ToInt32(grdMessageFolders.CurrentRow.Cells["Total"].Value.ToString()), PageIndex);
                grdMessageList.Show();

                lblNoRecordError.Visible = false;
                grdMessageList.EditMode = DataGridViewEditMode.EditProgrammatically;
                grdMessageList.AllowUserToAddRows = false;
                grdMessageList.DataSource = _objDtMessagesList;
                grdMessageList = PopulateColumns(ref grdMessageList);
            }
        }
        public void MarkReadUnreadSentMessages(ref DataGridView grdMessageList)
        {
            foreach (DataGridViewRow row in grdMessageList.Rows)
            {
                if (row.Cells["IsReplySent"].Value.ToString().ToUpper() != "TRUE")
                {
                    if (row.Cells["IsRead"].Value.ToString().ToUpper() == "TRUE")
                    {
                        row.DefaultCellStyle.BackColor = Color.Green;
                    }
                    else
                    {
                        row.DefaultCellStyle.BackColor = Color.Red;
                    }
                }
                else
                {
                    row.DefaultCellStyle.BackColor = Color.Gray;

                }
            }

        }
        public DataGridView PopulateColumns(ref DataGridView grdMessageList)
        {

            MarkReadUnreadSentMessages(ref grdMessageList);
            //'IsAttachment' column hidden
            grdMessageList.Columns["IsAttachment"].Visible = false;
            //'IsOpen' column hidden
            grdMessageList.Columns["IsOpen"].Visible = false;
            //'IsReplySent' column hidden
            grdMessageList.Columns["IsReplySent"].Visible = false;
            //'ModifiedOn' column hidden
            grdMessageList.Columns["ModifiedOn"].Visible = false;
            
            //'SentMessageBody' column hidden
            grdMessageList.Columns["SentMessageBody"].Visible = false;
            //MessageUID column hidden
            grdMessageList.Columns["MessageUID"].Visible = false;
            //FolderId column hidden
            grdMessageList.Columns["FolderID"].Visible = false;
            //AccountId column hidden
            grdMessageList.Columns["AccountID"].Visible = false;
            //IsRead Column Hidden
            grdMessageList.Columns["IsRead"].Visible = false;

            grdMessageList.Columns["CustomerEmail"].Width = 200;

            grdMessageList.Columns["MessageBody"].Width = 200;

            grdMessageList.Columns["ItemID"].Visible = false;
            grdMessageList.Columns["ExtMessageID"].Visible = false;
            grdMessageList.Columns["EbayUserID"].Visible = false;
            //grdMessageFolders.Columns[""]

            //=grdMessageList.Rows.Count.ToString();

            //   DataView dv;
            //     dv = new DataView(grdMessageList,"")
            //  grdMessageList.Rows.GetRowCount()
            return grdMessageList;
        }
        private void PopulateControlsForMessages(bool blIsVisible)
        {
            lblStatus.Visible = blIsVisible;
            btnFirst.Visible = blIsVisible;
            btnLast.Visible = blIsVisible;
            btnNext.Visible = blIsVisible;
            btnPrevious.Visible = blIsVisible;
            chkShowReadMessages.Visible = blIsVisible;
            pnlPaging.Visible = blIsVisible;

        }
        private void grdMessageFolders_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            PopulateControlsForMessages(true);
            //FolderIndex = e.RowIndex;
            _strFolderName = grdMessageFolders.CurrentRow.Cells["FolderName"].Value.ToString();
            _strFolderID = grdMessageFolders.CurrentRow.Cells["ID"].Value.ToString();
            _intTotalMessages = Convert.ToInt32(grdMessageFolders.CurrentRow.Cells["Total"].Value.ToString());
            _intUnreadMessages = Convert.ToInt32(grdMessageFolders.CurrentRow.Cells["Unread"].Value.ToString());
            _intReadMessages = _intTotalMessages - _intUnreadMessages;
            //strFolderName = "GMAIL";
            fillGrid();
            //BindGridForMessageList(_strFolderName, _strFolderID,1);

        }
        private void MessageFolders_Load(object sender, EventArgs e)
        {
            GetMessageAccounts();
            //  GetUnseenMessagesForGmail();
        }

        private void grdMessageList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Form frmMessageBody = Application.OpenForms["MessageBody"];
            if (frmMessageBody == null)
            {
                string[] strArrGrdSelectedValues = new string[]
                {
                    grdMessageList.Rows[e.RowIndex].Cells["MessageUID"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["CustomerEmail"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["MessageType"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["Subject"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["MessageBody"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["IsAttachment"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["IsOpen"].Value.ToString(),
                   // grdMessageList.Rows[e.RowIndex].Cells["IsProcessed"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["CreatedOn"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["AccountID"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["FolderID"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["ExtMessageID"].Value.ToString(),
                    grdMessageList.Rows[e.RowIndex].Cells["ItemID"].Value.ToString(),
                     grdMessageList.Rows[e.RowIndex].Cells["EbayUserID"].Value.ToString(),
                     grdMessageList.Rows[e.RowIndex].Cells["IsReplySent"].Value.ToString(),
                      grdMessageList.Rows[e.RowIndex].Cells["SentMessageBody"].Value.ToString()
                };

                if (grdMessageList.Rows[e.RowIndex].Cells["IsOpen"].Value.ToString().ToUpper() == "FALSE")
                {
                    UpdateIsOpenandReadStatus("N", strArrGrdSelectedValues[0], new Guid(strArrGrdSelectedValues[9]), new Guid(strArrGrdSelectedValues[8]));                   
                    MessageBody objMessageBody = new MessageBody(strArrGrdSelectedValues);
                    objMessageBody.ShowDialog();
                    UpdateIsOpenandReadStatus("Y", strArrGrdSelectedValues[0], new Guid(strArrGrdSelectedValues[9]), new Guid(strArrGrdSelectedValues[8]));

                    RefreshFoldersAndMessages();
                }
                else
                {
                    MessageBox.Show("Already Opened by other user", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //BindGridForMessageList(grdMessageFolders.SelectedCells[0].Value.ToString(), grdMessageFolders.CurrentRow.Cells["ID"].Value.ToString());
                    fillGrid();
                }
            }
        }
        public void UpdateIsOpenandReadStatus(string IsResetCall, string MessageUID, Guid FolderID, Guid AccountID)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[4];
                objParam[0] = new SqlParameter("@IsResetCall", SqlDbType.VarChar, 50);
                objParam[0].Value = IsResetCall;

                objParam[1] = new SqlParameter("@MessageUID", SqlDbType.VarChar, 50);
                objParam[1].Value = MessageUID;

                objParam[2] = new SqlParameter("@FolderID", SqlDbType.UniqueIdentifier);
                objParam[2].Value = FolderID;

                objParam[3] = new SqlParameter("@AccountID", SqlDbType.UniqueIdentifier);
                objParam[3].Value = AccountID;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "UpdateIsOpenAndReadStatus", objParam);

            }
            catch (Exception)
            {
                throw;
            }

        }
        private void btnTemplateSettings_Click(object sender, EventArgs e)
        {
            MessageTemplateSettings objMessageTemplateSettings = new MessageTemplateSettings();
            objMessageTemplateSettings.Show();
        }

        private void btnRuleList_Click(object sender, EventArgs e)
        {
            Form frmRuleList = Application.OpenForms["RuleList"];
            if (frmRuleList == null)
            {
                RuleList objRuleList = new RuleList();
                objRuleList.ShowDialog();
            }
            //  GetAllRules();
        }

        private void btnAccountSettings_Click(object sender, EventArgs e)
        {
            MessageAccountSettings objMessageAccountSettings = new MessageAccountSettings();
            objMessageAccountSettings.Show();
        }

        private void btnRefreshMessages_Click(object sender, EventArgs e)
        {

            RefreshFoldersAndMessages();
        }
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void RefreshFoldersAndMessages()
        {
            btnRefreshMessages.Enabled = false;
            BindGridForFolders();
            //  grdMessageFolders.
            //   GetUnseenMessagesForGmail();
            //BindGridForMessageList(_strFolderName, _strFolderID);
            fillGrid();
            btnRefreshMessages.Enabled = true;
        }




        private void AssignTicket_Click(object sender, EventArgs e)
        {
            AssignTicket objAssignTicket = new AssignTicket(_strArrGrdSelectedValues);
            objAssignTicket.ShowDialog();

            RefreshFoldersAndMessages();
        }
        public void ChangeReadUnreadStatus_Click(object sender, EventArgs e)
        {
            UpdateIsOpenandReadStatus("C", _strArrGrdSelectedValues[0], new Guid(_strArrGrdSelectedValues[9]), new Guid(_strArrGrdSelectedValues[8]));
            RefreshFoldersAndMessages();
        }
        private void grdMessageList_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int intSelectedRowIndex = grdMessageList.HitTest(e.X, e.Y).RowIndex;
                string[] strArrGrdSelectedValues = new string[]
                {
                    grdMessageList.Rows[intSelectedRowIndex].Cells["MessageUID"].Value.ToString(),
                    grdMessageList.Rows[intSelectedRowIndex].Cells["CustomerEmail"].Value.ToString(),
                    grdMessageList.Rows[intSelectedRowIndex].Cells["MessageType"].Value.ToString(),
                    grdMessageList.Rows[intSelectedRowIndex].Cells["Subject"].Value.ToString(),
                    grdMessageList.Rows[intSelectedRowIndex].Cells["MessageBody"].Value.ToString(),
                    grdMessageList.Rows[intSelectedRowIndex].Cells["IsAttachment"].Value.ToString(),
                    grdMessageList.Rows[intSelectedRowIndex].Cells["IsOpen"].Value.ToString(),
                   // grdMessageList.Rows[intSelectedRowIndex].Cells["IsProcessed"].Value.ToString(),
                    grdMessageList.Rows[intSelectedRowIndex].Cells["CreatedOn"].Value.ToString(),
                    grdMessageList.Rows[intSelectedRowIndex].Cells["AccountID"].Value.ToString(),
                    grdMessageList.Rows[intSelectedRowIndex].Cells["FolderID"].Value.ToString()
                };
                _strArrGrdSelectedValues = strArrGrdSelectedValues;

                ContextMenu RightClickMenu = new ContextMenu();
                RightClickMenu.MenuItems.Add(new MenuItem("Assign Ticket", AssignTicket_Click));
                RightClickMenu.MenuItems.Add(new MenuItem("Change Read/UnRead Status", ChangeReadUnreadStatus_Click));
                RightClickMenu.Show(grdMessageList, new Point(e.X, e.Y));

            }

        }
        //public void ShowReadMessages(bool blIsRead)
        //{
        //DataView dvShowReadMessages = new DataView(_objDtMessagesList);
        //dvShowReadMessages.RowFilter = "IsRead='" + blIsRead+"'";
        //DataTable objDtShowReadMessages = dvShowReadMessages.ToTable();
        //_objDtMessagesList = objDtShowReadMessages;




        //}
        //private void PopulatePager(int recordCount, int currentPage)
        //{
        //    List<Page> pages = new List<Page>();
        //    int startIndex, endIndex;
        //    int pagerSpan = 5;

        //    //Calculate the Start and End Index of pages to be displayed.
        //    double dblPageCount = (double)((decimal)recordCount / Convert.ToDecimal(PageSize));
        //    int pageCount = (int)Math.Ceiling(dblPageCount);
        //    startIndex = currentPage > 1 && currentPage + pagerSpan - 1 < pagerSpan ? currentPage : 1;
        //    endIndex = pageCount > pagerSpan ? pagerSpan : pageCount;
        //    if (currentPage > pagerSpan % 2)
        //    {
        //        if (currentPage == 2)
        //        {
        //            endIndex = 5;
        //        }
        //        else
        //        {
        //            endIndex = currentPage + 2;
        //        }
        //    }
        //    else
        //    {
        //        endIndex = (pagerSpan - currentPage) + 1;
        //    }

        //    if (endIndex - (pagerSpan - 1) > startIndex)
        //    {
        //        startIndex = endIndex - (pagerSpan - 1);
        //    }

        //    if (endIndex > pageCount)
        //    {
        //        endIndex = pageCount;
        //        startIndex = ((endIndex - pagerSpan) + 1) > 0 ? (endIndex - pagerSpan) + 1 : 1;
        //    }

        //    //Add the First Page Button.
        //    if (currentPage > 1)
        //    {
        //        pages.Add(new Page { Text = "First", Value = "1" });
        //    }

        //    //Add the Previous Button.
        //    if (currentPage > 1)
        //    {
        //        pages.Add(new Page { Text = "<<", Value = (currentPage - 1).ToString() });
        //    }

        //    for (int i = startIndex; i <= endIndex; i++)
        //    {
        //        pages.Add(new Page { Text = i.ToString(), Value = i.ToString(), Selected = i == currentPage });
        //    }

        //    //Add the Next Button.
        //    if (currentPage < pageCount)
        //    {
        //        pages.Add(new Page { Text = ">>", Value = (currentPage + 1).ToString() });
        //    }

        //    //Add the Last Button.
        //    if (currentPage != pageCount)
        //    {
        //        pages.Add(new Page { Text = "Last", Value = pageCount.ToString() });
        //    }

        //    //Clear existing Pager Buttons.
        //    pnlPager.Controls.Clear();

        //    //Loop and add Buttons for Pager.
        //    int count = 0;
        //    foreach (Page page in pages)
        //    {
        //        Button btnPage = new Button();
        //        btnPage.Location = new System.Drawing.Point(38 * count, 5);
        //        btnPage.Size = new System.Drawing.Size(35, 20);
        //        btnPage.Name = page.Value;
        //        btnPage.Text = page.Text;
        //        btnPage.Enabled = !page.Selected;
        //        btnPage.Click += new System.EventHandler(this.Page_Click);
        //        pnlPager.Controls.Add(btnPage);
        //        count++;
        //    }
        //}

        //private void Page_Click(object sender, EventArgs e)
        //{
        //    Button btnPager = (sender as Button);
        //    this.BindGridForMessageList(_strFolderName,_strFolderID ,int.Parse(btnPager.Name));
        //}

        //public partial class Page
        //{
        //    public string Text { get; set; }
        //    public string Value { get; set; }
        //    public bool Selected { get; set; }
        //}


        private void grdMessageList_Sorted(object sender, EventArgs e)
        {
            MarkReadUnreadSentMessages(ref grdMessageList);
        }
        public void fillGrid()
        {
            // For Page view.
            this._intPageSize = this._PageSize;
            //this.mintTotalRecords = getCount();
            if (chkShowReadMessages.Checked == true)
            {
                this._intPageCount = this._intReadMessages / this._intPageSize;
                if (this._intReadMessages % this._intPageSize > 0)
                    this._intPageCount++;
            }
            else
            {
                this._intPageCount = this._intUnreadMessages / this._intPageSize;
                if (this._intUnreadMessages % this._intPageSize > 0)
                    this._intPageCount++;
            }


            // Adjust page count if the last page contains partial page.


            this._intCurrentPage = 0;

            LoadPage();

        }
        public void LoadPage()
        {
            int intSkip = 0;
            intSkip = (this._intCurrentPage * this._intPageSize);
            if (_strFolderID != null)
            {
                BindGridForMessageList(_strFolderName, _strFolderID, _intCurrentPage, _intPageSize, chkShowReadMessages.Checked, intSkip);
            }
            // Show Status
            this.lblStatus.Text = (this._intCurrentPage + 1).ToString() +
              " / " + this._intPageCount.ToString();

        }
        public void goFirst()
        {
            this._intCurrentPage = 0;

            LoadPage();
        }

        private void goPrevious()
        {
            if (this._intCurrentPage == this._intPageCount)
                this._intCurrentPage = this._intPageCount - 1;

            this._intCurrentPage--;

            if (this._intCurrentPage < 1)
                this._intCurrentPage = 0;

            LoadPage();
        }

        private void goNext()
        {
            this._intCurrentPage++;

            if (this._intCurrentPage > (this._intPageCount - 1))
                this._intCurrentPage = this._intPageCount - 1;

            LoadPage();
        }

        private void goLast()
        {
            this._intCurrentPage = this._intPageCount - 1;

            LoadPage();
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            goFirst();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            goPrevious();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            goNext();
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            goLast();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkShowReadMessages_CheckedChanged(object sender, EventArgs e)
        {

            fillGrid();
            //ShowReadMessages(chkShowReadMessages.Checked);
        }

        private void btnSearchMessages_Click(object sender, EventArgs e)
        {
            Form frmSearchMessages = Application.OpenForms["SearchMessages"];
            if (frmSearchMessages == null)
            {
                SearchMessages objSearchMessages = new SearchMessages();
                objSearchMessages.Show();
            }
        }

        private void btnDeleteFolder_Click(object sender, EventArgs e)
            {
                DialogResult objDResult = MessageBox.Show("Are you sure you want to Delete this Folder ?", "Delete Folder", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (objDResult == DialogResult.Yes)
                {
                    string strFolderID = grdMessageFolders.CurrentRow.Cells["ID"].Value.ToString();
                    string strFolderName = grdMessageFolders.CurrentRow.Cells["FolderName"].Value.ToString();
                    string strDeleteResult = GetUpdateMessageFolders("DELETE", new Guid(strFolderID), GlobalVariablesAndMethods.UserDetails.Userid.ToString(), strFolderName);
                    if (strDeleteResult == "4")
                    {
                        MessageBox.Show("Folder Deleted Successfully and Message sent to Unmatched Emails Folder", "Delete Folder", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        RefreshFoldersAndMessages();
                    }

                }
        }






    }
}