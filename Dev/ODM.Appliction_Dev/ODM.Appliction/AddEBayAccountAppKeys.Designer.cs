﻿namespace ODM.Appliction
{
    partial class AddEBayAccountAppKeys
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEBayAccountAppKeys));
            this.grpEbayAuthAPI = new System.Windows.Forms.GroupBox();
            this.LblDeveloper = new System.Windows.Forms.Label();
            this.LblApplication = new System.Windows.Forms.Label();
            this.TxtApplication = new System.Windows.Forms.TextBox();
            this.TxtDeveloper = new System.Windows.Forms.TextBox();
            this.TxtRuName = new System.Windows.Forms.TextBox();
            this.LblRuName = new System.Windows.Forms.Label();
            this.TxtCertificate = new System.Windows.Forms.TextBox();
            this.LblCertificate = new System.Windows.Forms.Label();
            this.TxtAppKeysName = new System.Windows.Forms.TextBox();
            this.LblAppKeysName = new System.Windows.Forms.Label();
            this.btnSaveAppKeys = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.ddlEbayAccounts = new System.Windows.Forms.ComboBox();
            this.LblEbayAccount = new System.Windows.Forms.Label();
            this.grpEbayAuthAPI.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpEbayAuthAPI
            // 
            this.grpEbayAuthAPI.Controls.Add(this.LblDeveloper);
            this.grpEbayAuthAPI.Controls.Add(this.LblApplication);
            this.grpEbayAuthAPI.Controls.Add(this.TxtApplication);
            this.grpEbayAuthAPI.Controls.Add(this.TxtDeveloper);
            this.grpEbayAuthAPI.Controls.Add(this.TxtRuName);
            this.grpEbayAuthAPI.Controls.Add(this.LblRuName);
            this.grpEbayAuthAPI.Controls.Add(this.TxtCertificate);
            this.grpEbayAuthAPI.Controls.Add(this.LblCertificate);
            this.grpEbayAuthAPI.Location = new System.Drawing.Point(66, 42);
            this.grpEbayAuthAPI.Name = "grpEbayAuthAPI";
            this.grpEbayAuthAPI.Size = new System.Drawing.Size(401, 242);
            this.grpEbayAuthAPI.TabIndex = 42;
            this.grpEbayAuthAPI.TabStop = false;
            this.grpEbayAuthAPI.Text = "API Keys";
            // 
            // LblDeveloper
            // 
            this.LblDeveloper.Location = new System.Drawing.Point(16, 25);
            this.LblDeveloper.Name = "LblDeveloper";
            this.LblDeveloper.Size = new System.Drawing.Size(112, 19);
            this.LblDeveloper.TabIndex = 18;
            this.LblDeveloper.Text = "Developer:";
            // 
            // LblApplication
            // 
            this.LblApplication.Location = new System.Drawing.Point(16, 64);
            this.LblApplication.Name = "LblApplication";
            this.LblApplication.Size = new System.Drawing.Size(112, 19);
            this.LblApplication.TabIndex = 20;
            this.LblApplication.Text = "Application:";
            // 
            // TxtApplication
            // 
            this.TxtApplication.Location = new System.Drawing.Point(128, 63);
            this.TxtApplication.Name = "TxtApplication";
            this.TxtApplication.Size = new System.Drawing.Size(256, 20);
            this.TxtApplication.TabIndex = 1;
            // 
            // TxtDeveloper
            // 
            this.TxtDeveloper.Location = new System.Drawing.Point(128, 24);
            this.TxtDeveloper.Name = "TxtDeveloper";
            this.TxtDeveloper.Size = new System.Drawing.Size(256, 20);
            this.TxtDeveloper.TabIndex = 0;
            // 
            // TxtRuName
            // 
            this.TxtRuName.Location = new System.Drawing.Point(128, 132);
            this.TxtRuName.Name = "TxtRuName";
            this.TxtRuName.PasswordChar = '*';
            this.TxtRuName.Size = new System.Drawing.Size(256, 20);
            this.TxtRuName.TabIndex = 2;
            // 
            // LblRuName
            // 
            this.LblRuName.Location = new System.Drawing.Point(16, 133);
            this.LblRuName.Name = "LblRuName";
            this.LblRuName.Size = new System.Drawing.Size(112, 19);
            this.LblRuName.TabIndex = 22;
            this.LblRuName.Text = "RuName :";
            // 
            // TxtCertificate
            // 
            this.TxtCertificate.Location = new System.Drawing.Point(128, 97);
            this.TxtCertificate.Name = "TxtCertificate";
            this.TxtCertificate.PasswordChar = '*';
            this.TxtCertificate.Size = new System.Drawing.Size(256, 20);
            this.TxtCertificate.TabIndex = 2;
            // 
            // LblCertificate
            // 
            this.LblCertificate.Location = new System.Drawing.Point(16, 98);
            this.LblCertificate.Name = "LblCertificate";
            this.LblCertificate.Size = new System.Drawing.Size(112, 19);
            this.LblCertificate.TabIndex = 22;
            this.LblCertificate.Text = "Certificate:";
            // 
            // TxtAppKeysName
            // 
            this.TxtAppKeysName.Location = new System.Drawing.Point(194, 249);
            this.TxtAppKeysName.Name = "TxtAppKeysName";
            this.TxtAppKeysName.Size = new System.Drawing.Size(256, 20);
            this.TxtAppKeysName.TabIndex = 2;
            // 
            // LblAppKeysName
            // 
            this.LblAppKeysName.Location = new System.Drawing.Point(82, 250);
            this.LblAppKeysName.Name = "LblAppKeysName";
            this.LblAppKeysName.Size = new System.Drawing.Size(112, 19);
            this.LblAppKeysName.TabIndex = 22;
            this.LblAppKeysName.Text = "App Keys Name :";
            // 
            // btnSaveAppKeys
            // 
            this.btnSaveAppKeys.Location = new System.Drawing.Point(153, 329);
            this.btnSaveAppKeys.Name = "btnSaveAppKeys";
            this.btnSaveAppKeys.Size = new System.Drawing.Size(75, 23);
            this.btnSaveAppKeys.TabIndex = 43;
            this.btnSaveAppKeys.Text = "Save";
            this.btnSaveAppKeys.UseVisualStyleBackColor = true;
            this.btnSaveAppKeys.Click += new System.EventHandler(this.btnSaveAppKeys_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(331, 329);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 43;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ddlEbayAccounts
            // 
            this.ddlEbayAccounts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEbayAccounts.FormattingEnabled = true;
            this.ddlEbayAccounts.Location = new System.Drawing.Point(194, 212);
            this.ddlEbayAccounts.Name = "ddlEbayAccounts";
            this.ddlEbayAccounts.Size = new System.Drawing.Size(256, 21);
            this.ddlEbayAccounts.TabIndex = 44;
            // 
            // LblEbayAccount
            // 
            this.LblEbayAccount.Location = new System.Drawing.Point(82, 215);
            this.LblEbayAccount.Name = "LblEbayAccount";
            this.LblEbayAccount.Size = new System.Drawing.Size(112, 19);
            this.LblEbayAccount.TabIndex = 22;
            this.LblEbayAccount.Text = "Ebay Account : ";
            // 
            // AddEBayAccountAppKeys
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 390);
            this.Controls.Add(this.TxtAppKeysName);
            this.Controls.Add(this.LblAppKeysName);
            this.Controls.Add(this.LblEbayAccount);
            this.Controls.Add(this.ddlEbayAccounts);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSaveAppKeys);
            this.Controls.Add(this.grpEbayAuthAPI);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddEBayAccountAppKeys";
            this.Text = "EBayAppKeys";
            this.grpEbayAuthAPI.ResumeLayout(false);
            this.grpEbayAuthAPI.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpEbayAuthAPI;
        private System.Windows.Forms.Label LblDeveloper;
        private System.Windows.Forms.Label LblApplication;
        private System.Windows.Forms.TextBox TxtApplication;
        private System.Windows.Forms.TextBox TxtDeveloper;
        private System.Windows.Forms.TextBox TxtCertificate;
        private System.Windows.Forms.Label LblCertificate;
        private System.Windows.Forms.TextBox TxtRuName;
        private System.Windows.Forms.TextBox TxtAppKeysName;
        private System.Windows.Forms.Label LblRuName;
        private System.Windows.Forms.Label LblAppKeysName;
        private System.Windows.Forms.Button btnSaveAppKeys;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox ddlEbayAccounts;
        private System.Windows.Forms.Label LblEbayAccount;
    }
}