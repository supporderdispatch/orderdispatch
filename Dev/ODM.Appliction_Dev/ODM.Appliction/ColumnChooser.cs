﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Linq;
using System.Drawing;
namespace ODM.Appliction
{
    public partial class ColumnChooser : Form
    {
        DataTable objDtChooseColumns = new DataTable();
        SqlConnection conn;
        string SearchedInput = string.Empty;
        bool blIsCellClick = false, blIsAllSelect = false;
        public ColumnChooser()
        {
            InitializeComponent();
            GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
        }

        public ColumnChooser(DataTable objDt, string strSearchedInput)
        {
            InitializeComponent();
            GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
            objDtChooseColumns = objDt;
            SearchedInput = strSearchedInput;
            BindGrid();
        }

        private void BindGrid()
        {
            try
            {
                if (objDtChooseColumns.Rows.Count > 0)
                {
                    lblMsg.Visible = false;

                    grdColumnChooser.DataSource = MakeDataSource();
                    grdColumnChooser.Columns[0].Resizable = DataGridViewTriState.False;
                    grdColumnChooser.AllowUserToAddRows = false;

                    grdColumnChooser.Columns["Column Name"].ReadOnly = true;
                    grdColumnChooser.Columns["Column Name"].Width = 330;
                    grdColumnChooser.Columns[0].Width = 25;


                    //Code to add checkbox on header of datagridview
                    CheckBox checkBox = new CheckBox();
                    checkBox.Size = new Size(15, 15);
                    checkBox.BackColor = Color.Transparent;
                    checkBox.Name = "chkHeader";
                    checkBox.CheckedChanged += new EventHandler(chkHeader_Changed);

                    // Reset properties
                    checkBox.Padding = new Padding(0);
                    checkBox.Margin = new Padding(0);
                    checkBox.Text = "";

                    // Add checkbox to datagrid cell
                    grdColumnChooser.Controls.Add(checkBox);
                    checkBox.Location = new Point(47, 5);
                    //End of code to add checkbox in datagrid

                    if (blIsAllSelect)
                    {
                        CheckBox ChkHearder = this.Controls.Find("chkHeader", true).FirstOrDefault() as CheckBox;
                        ChkHearder.Checked = true;
                    }
                    grdColumnChooser.EndEdit();
                }
                else
                {
                    lblMsg.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private DataTable MakeDataSource()
        {
            DataTable objCustomDt = new DataTable();
            try
            {
                objCustomDt.Columns.Add(" ", typeof(Boolean));
                objCustomDt.Columns.Add("Column Name", typeof(string));
                DataTable objDtSelectedColumns = new DataTable();
                int bResult = 100;
                objDtSelectedColumns = GetAddOrUpdateColumnChooser("GET", GlobalVariablesAndMethods.UserDetails.Userid, "", ref bResult);
                if (bResult == 0)
                {
                    string[] arrColumns = Convert.ToString(objDtSelectedColumns.Rows[0]["SelectedColumns"]).Split(',');
                    int intCount = 0, intCountOfSelectedColumns = 0;
                    for (int i = 0; i < objDtChooseColumns.Columns.Count; i++)
                    {
                        string strColumnName = objDtChooseColumns.Columns[i].ColumnName;
                        if (strColumnName.ToUpper() != "ORDERID")
                        {
                            DataRow RowToInsert = objCustomDt.NewRow();
                            objCustomDt.Rows.InsertAt(RowToInsert, intCount);
                            objCustomDt.Rows[intCount]["Column Name"] = strColumnName;
                            int intCountOfItem = arrColumns.Where(x => x == strColumnName).Count();
                            if (intCountOfItem > 0)
                            {
                                objCustomDt.Rows[intCount][" "] = true;
                                intCountOfSelectedColumns++;
                            }
                            else
                            {
                                //objCustomDt.Rows[intCount]["Select Column(s)"] = false;
                                objCustomDt.Rows[intCount][" "] = false;
                            }
                            intCount++;
                        }
                    }

                    //Checking are all columns selected or not,objDtChooseColumns.Columns.Count - 1 because we are not counting OrderID
                    if (intCountOfSelectedColumns == objDtChooseColumns.Columns.Count - 1)
                    {
                        blIsAllSelect = true;
                    }
                    //End of column checks
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred while making datasource for grid\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return objCustomDt;

        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            string strSelectedColumns = string.Empty;
            try
            {
                foreach (DataGridViewRow row in grdColumnChooser.Rows)
                {
                    bool IsSelected = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells[0]).Value);
                    if (IsSelected)
                    {
                        string strCurrentColumnName = Convert.ToString(row.Cells[1].Value);
                        strSelectedColumns = strSelectedColumns == string.Empty ? strCurrentColumnName : strSelectedColumns + "," + strCurrentColumnName;
                    }
                }

                if (strSelectedColumns != string.Empty)
                {
                    int bResult = 100;
                    GetAddOrUpdateColumnChooser("UPDATE", GlobalVariablesAndMethods.UserDetails.Userid, strSelectedColumns, ref bResult);
                    if (bResult == 1)
                    {
                        this.Close();
                        Form frmAllOpenOrders = Application.OpenForms["AllOpenOrders"];
                        if (frmAllOpenOrders != null)
                        {
                            frmAllOpenOrders.Close();
                        }
                        AllOpenOrders objOpenOrders = new AllOpenOrders(SearchedInput, new DataTable());
                        objOpenOrders.Show();
                        MessageBox.Show("Settings Applied");
                        
                    }
                }
                else
                {
                    MessageBox.Show("Please select at least one column");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// This method is responsible to get the selected columns(by logged in user), or to insert or update the 
        /// columns list in our database
        /// </summary>
        /// <param name="strCommand">To perform specific operation either GET or UPDATE</param>
        /// <param name="UserID">Currently logged in user id</param>
        /// <param name="strColumns">Name of columns to save in database(to specific user)</param>
        /// <returns></returns>
        public DataTable GetAddOrUpdateColumnChooser(string strCommand, Guid UserID, string strColumns, ref int intResult)
        {
            try
            {
                DataTable objDt = new DataTable();
                conn = new SqlConnection();
                conn.ConnectionString = MakeConnection.GetConnection();
                conn.Open();

                SqlParameter[] param = new SqlParameter[4];

                param[0] = new SqlParameter("@Command", SqlDbType.VarChar, 10);
                param[0].Value = strCommand;

                param[1] = new SqlParameter("@UserID", SqlDbType.UniqueIdentifier);
                param[1].Value = UserID;

                param[2] = new SqlParameter("@ColumnNames", SqlDbType.VarChar, 8000);
                param[2].Value = strColumns;

                param[3] = new SqlParameter("@Result", SqlDbType.Int);
                param[3].Direction = ParameterDirection.Output;

                if (strCommand.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateColumnsByUser", param);
                }
                else if (strCommand.ToUpper() == "GET")
                {
                    DataSet dsData = new DataSet();
                    dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateColumnsByUser", param);
                    if (dsData.Tables.Count > 0 && dsData.Tables[0].Rows.Count > 0)
                    {
                        objDt = dsData.Tables[0];
                    }
                }
                intResult = Convert.ToInt32(param[3].Value);
                return objDt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        private void grdColumnChooser_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int intTotalRows = grdColumnChooser.Rows.Count;
            int intSelectedRows = 0;
            int intCount = 0;
            blIsCellClick = true;
            grdColumnChooser.EndEdit();
            CheckBox ChkHearder = this.Controls.Find("chkHeader", true).FirstOrDefault() as CheckBox;
            foreach (DataGridViewRow row in grdColumnChooser.Rows)
            {
                DataGridViewCheckBoxCell chk = row.Cells[0] as DataGridViewCheckBoxCell;
                chk.Selected = false;
                bool IsSelected = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells[0]).Value);
                if (IsSelected)
                {
                    intSelectedRows++;
                }
                intCount++;
            }
            if (intTotalRows == intSelectedRows && intSelectedRows > 0)
            {
                ChkHearder.Checked = true;
            }
            else
            {
                ChkHearder.Checked = false;
            }
        }

        private void chkHeader_Changed(object sender, EventArgs e)
        {

            CheckBox ChkHearder = (CheckBox)sender;
            foreach (DataGridViewRow row in grdColumnChooser.Rows)
            {
                DataGridViewCheckBoxCell chk = row.Cells[0] as DataGridViewCheckBoxCell;
                chk.Selected = false;
                if (ChkHearder.Checked)
                {
                    chk.Value = true;
                }
                else if (!blIsCellClick && !ChkHearder.Checked)
                {
                    chk.Value = false;
                }
            }
            blIsCellClick = false;
            grdColumnChooser.EndEdit();
        }
    }
}
