﻿namespace ODM.Appliction
{
    partial class AddMessageAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddMessageAccount));
            this.lblAccountName = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblHostName = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.lblSSL = new System.Windows.Forms.Label();
            this.lblIsEbay = new System.Windows.Forms.Label();
            this.txtAccountName = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtImapHostName = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.chkIsEbay = new System.Windows.Forms.CheckBox();
            this.chkIsSSL = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblSmtpHostName = new System.Windows.Forms.Label();
            this.txtSmtpHostName = new System.Windows.Forms.TextBox();
            this.lblIsEbaySandbox = new System.Windows.Forms.Label();
            this.chkIsEbaySandbox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblAccountName
            // 
            this.lblAccountName.AutoSize = true;
            this.lblAccountName.Location = new System.Drawing.Point(15, 52);
            this.lblAccountName.Name = "lblAccountName";
            this.lblAccountName.Size = new System.Drawing.Size(84, 13);
            this.lblAccountName.TabIndex = 0;
            this.lblAccountName.Text = "Account Name :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(36, 93);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 1;
            this.lblUserName.Text = "UserName : ";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(40, 132);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(59, 13);
            this.lblPassword.TabIndex = 2;
            this.lblPassword.Text = "Password :";
            // 
            // lblHostName
            // 
            this.lblHostName.AutoSize = true;
            this.lblHostName.Location = new System.Drawing.Point(3, 167);
            this.lblHostName.Name = "lblHostName";
            this.lblHostName.Size = new System.Drawing.Size(99, 13);
            this.lblHostName.TabIndex = 3;
            this.lblHostName.Text = "Hostname (IMAP) : ";
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(70, 240);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(32, 13);
            this.lblPort.TabIndex = 4;
            this.lblPort.Text = "Port :";
            this.lblPort.Visible = false;
            // 
            // lblSSL
            // 
            this.lblSSL.AutoSize = true;
            this.lblSSL.Location = new System.Drawing.Point(66, 350);
            this.lblSSL.Name = "lblSSL";
            this.lblSSL.Size = new System.Drawing.Size(33, 13);
            this.lblSSL.TabIndex = 5;
            this.lblSSL.Text = "SSL :";
            this.lblSSL.Visible = false;
            // 
            // lblIsEbay
            // 
            this.lblIsEbay.AutoSize = true;
            this.lblIsEbay.Location = new System.Drawing.Point(55, 273);
            this.lblIsEbay.Name = "lblIsEbay";
            this.lblIsEbay.Size = new System.Drawing.Size(45, 13);
            this.lblIsEbay.TabIndex = 6;
            this.lblIsEbay.Text = "IsEbay :";
            // 
            // txtAccountName
            // 
            this.txtAccountName.Location = new System.Drawing.Point(105, 49);
            this.txtAccountName.Name = "txtAccountName";
            this.txtAccountName.Size = new System.Drawing.Size(290, 20);
            this.txtAccountName.TabIndex = 7;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(105, 90);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(290, 20);
            this.txtUserName.TabIndex = 7;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(105, 129);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(290, 20);
            this.txtPassword.TabIndex = 7;
            // 
            // txtImapHostName
            // 
            this.txtImapHostName.Location = new System.Drawing.Point(105, 164);
            this.txtImapHostName.Name = "txtImapHostName";
            this.txtImapHostName.Size = new System.Drawing.Size(290, 20);
            this.txtImapHostName.TabIndex = 7;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(108, 236);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(290, 20);
            this.txtPort.TabIndex = 7;
            this.txtPort.Visible = false;
            // 
            // chkIsEbay
            // 
            this.chkIsEbay.AutoSize = true;
            this.chkIsEbay.Location = new System.Drawing.Point(108, 273);
            this.chkIsEbay.Name = "chkIsEbay";
            this.chkIsEbay.Size = new System.Drawing.Size(15, 14);
            this.chkIsEbay.TabIndex = 8;
            this.chkIsEbay.UseVisualStyleBackColor = true;
            this.chkIsEbay.CheckedChanged += new System.EventHandler(this.chkIsEbay_CheckedChanged);
            // 
            // chkIsSSL
            // 
            this.chkIsSSL.AutoSize = true;
            this.chkIsSSL.Location = new System.Drawing.Point(106, 350);
            this.chkIsSSL.Name = "chkIsSSL";
            this.chkIsSSL.Size = new System.Drawing.Size(15, 14);
            this.chkIsSSL.TabIndex = 8;
            this.chkIsSSL.UseVisualStyleBackColor = true;
            this.chkIsSSL.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(129, 313);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(293, 313);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblSmtpHostName
            // 
            this.lblSmtpHostName.AutoSize = true;
            this.lblSmtpHostName.Location = new System.Drawing.Point(-1, 200);
            this.lblSmtpHostName.Name = "lblSmtpHostName";
            this.lblSmtpHostName.Size = new System.Drawing.Size(103, 13);
            this.lblSmtpHostName.TabIndex = 3;
            this.lblSmtpHostName.Text = "Hostname (SMTP) : ";
            // 
            // txtSmtpHostName
            // 
            this.txtSmtpHostName.Location = new System.Drawing.Point(105, 197);
            this.txtSmtpHostName.Name = "txtSmtpHostName";
            this.txtSmtpHostName.Size = new System.Drawing.Size(290, 20);
            this.txtSmtpHostName.TabIndex = 7;
            // 
            // lblIsEbaySandbox
            // 
            this.lblIsEbaySandbox.AutoSize = true;
            this.lblIsEbaySandbox.Location = new System.Drawing.Point(178, 273);
            this.lblIsEbaySandbox.Name = "lblIsEbaySandbox";
            this.lblIsEbaySandbox.Size = new System.Drawing.Size(87, 13);
            this.lblIsEbaySandbox.TabIndex = 6;
            this.lblIsEbaySandbox.Text = "IsEbaySandbox :";
            this.lblIsEbaySandbox.Visible = false;
            // 
            // chkIsEbaySandbox
            // 
            this.chkIsEbaySandbox.AutoSize = true;
            this.chkIsEbaySandbox.Location = new System.Drawing.Point(274, 273);
            this.chkIsEbaySandbox.Name = "chkIsEbaySandbox";
            this.chkIsEbaySandbox.Size = new System.Drawing.Size(15, 14);
            this.chkIsEbaySandbox.TabIndex = 8;
            this.chkIsEbaySandbox.UseVisualStyleBackColor = true;
            this.chkIsEbaySandbox.Visible = false;
            // 
            // AddMessageAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 392);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkIsEbaySandbox);
            this.Controls.Add(this.chkIsEbay);
            this.Controls.Add(this.chkIsSSL);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.txtSmtpHostName);
            this.Controls.Add(this.txtImapHostName);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.txtAccountName);
            this.Controls.Add(this.lblIsEbaySandbox);
            this.Controls.Add(this.lblIsEbay);
            this.Controls.Add(this.lblSSL);
            this.Controls.Add(this.lblSmtpHostName);
            this.Controls.Add(this.lblPort);
            this.Controls.Add(this.lblHostName);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.lblAccountName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddMessageAccount";
            this.Text = "AddMessageAccount";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAccountName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblHostName;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label lblSSL;
        private System.Windows.Forms.Label lblIsEbay;
        private System.Windows.Forms.TextBox txtAccountName;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtImapHostName;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.CheckBox chkIsEbay;
        private System.Windows.Forms.CheckBox chkIsSSL;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblSmtpHostName;
        private System.Windows.Forms.TextBox txtSmtpHostName;
        private System.Windows.Forms.Label lblIsEbaySandbox;
        private System.Windows.Forms.CheckBox chkIsEbaySandbox;
    }
}