﻿namespace ODM.Appliction
{
    partial class AddRuleCondition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddRuleCondition));
            this.lblNewCondition = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtNewCondition = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblNewCondition
            // 
            this.lblNewCondition.AutoSize = true;
            this.lblNewCondition.Location = new System.Drawing.Point(85, 60);
            this.lblNewCondition.Name = "lblNewCondition";
            this.lblNewCondition.Size = new System.Drawing.Size(82, 13);
            this.lblNewCondition.TabIndex = 0;
            this.lblNewCondition.Text = "New Condition :";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(236, 105);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(381, 105);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtNewCondition
            // 
            this.txtNewCondition.Location = new System.Drawing.Point(187, 55);
            this.txtNewCondition.Name = "txtNewCondition";
            this.txtNewCondition.Size = new System.Drawing.Size(369, 20);
            this.txtNewCondition.TabIndex = 1;
            // 
            // AddRuleCondition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 157);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtNewCondition);
            this.Controls.Add(this.lblNewCondition);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddRuleCondition";
            this.Text = "AddRuleCondition";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNewCondition;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtNewCondition;
    }
}