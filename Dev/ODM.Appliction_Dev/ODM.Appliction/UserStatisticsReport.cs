﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Appliction.Extensions;
using ODM.Core.UserTracking;

namespace ODM.Appliction
{
    public struct UserStatisticsSummary
    {
        public TimeSpan TotalHourWorked;
        public int TotalOrdersProcessed;
    }

    public partial class UserStatisticsReport : Form
    {
        UsersStatisticsReportMethods statisticsReportMethods;
        private List<Entities.FinalViewDigest> CurrentDateFinalDigestList = new List<Entities.FinalViewDigest>();
        private List<List<Entities.FinalViewDigest>> RangeFinalDigestList = new List<List<Entities.FinalViewDigest>>();
        private bool TodayViewLabelsAlreadyAdded = false;
        private System.Timers.Timer AutoRefreshTimer;

        public UserStatisticsReport()
        {
            InitializeComponent();
            picLoading.Visible = false;
            btnShowTodayStat.Checked = true;
            statisticsReportMethods = new UsersStatisticsReportMethods();
            PerformShowStatChanged();
            this.Load += UserStatisticsReport_Load;
            this.FormClosed += UserStatisticsReport_FormClosed;
        }

        void UserStatisticsReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (AutoRefreshTimer != null)
                AutoRefreshTimer.Dispose();
        }

        void UserStatisticsReport_Load(object sender, EventArgs e)
        {
            LoadAutoRefreshOptions();
        }

        private void LoadAutoRefreshOptions()
        {
            cmbAutoRefreshInterval.Items.Clear();
            cmbAutoRefreshInterval.Items.AddRange(new string[] { "1","2", "5", "10", "15", "20", "30", "60" });
            cmbAutoRefreshInterval.SelectedIndex = 0;
        }

        #region Methods to control views when optns toggled among views of today/weekly/custom Range
        private async void PerformShowStatChanged()
        {
            CheckForIllegalCrossThreadCalls = false;
            if (btnShowTodayStat.Checked)
            {
                ShowCustomRangeControl(false);
                grdRangeReportView.Visible = false;
                btnShowWeeklyStats.Enabled = false;
                btnShowCustomRangeStat.Enabled = false;
                btnRefresh.Visible = false;
                picLoading.Visible = true;
                await Task.Run(() =>
                {
                    CurrentDateFinalDigestList = new List<Entities.FinalViewDigest>();
                    PopulateCurrentDateFinalDigestList();
                });

                LoadUserStatisticsForTodayView();
                btnShowWeeklyStats.Enabled = true;
                btnShowCustomRangeStat.Enabled = true;
                picLoading.Visible = false;
                btnRefresh.Visible = true;
            }
            if (btnShowWeeklyStats.Checked)
            {
                ShowCustomRangeControl(false);
                picLoading.Visible = true;
                btnRefresh.Visible = false;
                btnShowTodayStat.Enabled = false;
                btnShowCustomRangeStat.Enabled = false;
                await Task.Run(() =>
                {
                    RangeFinalDigestList = new List<List<Entities.FinalViewDigest>>();
                    PopulateWeeklyFinalDigestList();
                });

                LoadUserStatisticsForRangeView();
                btnShowTodayStat.Enabled = true;
                btnShowCustomRangeStat.Enabled = true;
                picLoading.Visible = false;
                btnRefresh.Visible = true;
            }
            if (btnShowCustomRangeStat.Checked)
            {
                ShowCustomRangeControl(true);
                picLoading.Visible = true;
                btnRefresh.Visible = false;
                btnShowTodayStat.Enabled = false;
                btnShowWeeklyStats.Enabled = false;
                await Task.Run(() =>
                {
                    RangeFinalDigestList = new List<List<Entities.FinalViewDigest>>();
                    PopulateCustomRangeFinalDigestList();
                });

                LoadUserStatisticsForRangeView();
                btnShowTodayStat.Enabled = true;
                btnShowWeeklyStats.Enabled = true;
                picLoading.Visible = false;
                btnRefresh.Visible = true;
            }

        }
        private void ShowCustomRangeControl(bool value)
        {
            lblToDate.Visible = value;
            lblFromDate.Visible = value;
            dateTimePickFrom.Visible = value;
            dateTimePickTo.Visible = value;
        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            PerformShowStatChanged();
        }
        private void btnShowTodayStat_CheckedChanged(object sender, EventArgs e)
        {
            PerformShowStatChanged();
        }

        private void btnShowWeeklyStats_CheckedChanged(object sender, EventArgs e)
        {
            PerformShowStatChanged();
        }
        private void dateTimePickTo_ValueChanged(object sender, EventArgs e)
        {
            PerformShowStatChanged();
        }

        private void dateTimePickFrom_ValueChanged(object sender, EventArgs e)
        {
            PerformShowStatChanged();
        }
        #endregion

        #region Load views of selected today/week/custom Range
        private void LoadUserStatisticsForTodayView()
        {
            this.lblUserName.Visible = false;
            this.lblTotalLoggedInTime.Visible = false;
            this.lblAvgPerOrderTimeTaken.Visible = false;
            this.lblOrderPerHourRate.Visible = false;
            this.lblTotalOrdersProcessed.Visible = false;

            var incUnit = 25;
            var listOfFinalDigestView = CurrentDateFinalDigestList.ToList();
            var index = 0;

            if (TodayViewLabelsAlreadyAdded)
            {
                foreach (var finalDigest in listOfFinalDigestView)
                {
                    this.pnlTodayUserStatistics.Controls.Remove(this.Controls.Find("lbluserName", true).FirstOrDefault() as Label);
                    this.pnlTodayUserStatistics.Controls.Remove(this.Controls.Find("lbltotalLoggedInTime", true).FirstOrDefault() as Label);
                    this.pnlTodayUserStatistics.Controls.Remove(this.Controls.Find("lblavgperorderTimeTaken", true).FirstOrDefault() as Label);
                    this.pnlTodayUserStatistics.Controls.Remove(this.Controls.Find("lblorderPerHouravgRate", true).FirstOrDefault() as Label);
                    this.pnlTodayUserStatistics.Controls.Remove(this.Controls.Find("lblordersProcessed", true).FirstOrDefault() as Label);
                }
            }
            foreach (var finalDigest in listOfFinalDigestView)
            {
                var lbluserName = new Label() { Name = "lbluserName", Left = lblUserName.Left, Top = lblUserName.Top + (index * incUnit) };
                var lbltotalLoggedInTime = new Label() { Name = "lbltotalLoggedInTime", Left = lblTotalLoggedInTime.Left, Top = lblTotalLoggedInTime.Top + (index * incUnit) };
                var lblavgperorderTimeTaken = new Label() { Name = "lblavgperorderTimeTaken", Left = lblAvgPerOrderTimeTaken.Left, Top = lblAvgPerOrderTimeTaken.Top + (index * incUnit) };
                var lblorderPerHouravgRate = new Label() { Name = "lblorderPerHouravgRate", Left = lblOrderPerHourRate.Left, Top = lblOrderPerHourRate.Top + (index * incUnit) };
                var lblordersProcessed = new Label() { Name = "lblordersProcessed", Left = lblTotalOrdersProcessed.Left, Top = lblTotalOrdersProcessed.Top + (index * incUnit) };

                var liTime = finalDigest.TotalLoggedInTime;//logged in time

                lbluserName.Text = String.Format("{0}", finalDigest.UserName);
                lbltotalLoggedInTime.Text = String.Format("{0:00}:{1:00}:{2:00}", liTime.Hours, liTime.Minutes, liTime.Seconds);
                lblavgperorderTimeTaken.Text = String.Format("{0:0.00}", ODM.Core.Helpers.TimerUtils.GetMinutes(Convert.ToDouble(finalDigest.PerOrderAvgProcessTime)));
                lblorderPerHouravgRate.Text = String.Format("{0:0.00}", finalDigest.HourlyPerOrderProcessRate);
                lblordersProcessed.Text = String.Format("{0}", finalDigest.TotalOrderProcessed);

                index++;

                CheckForIllegalCrossThreadCalls = false;
                this.pnlTodayUserStatistics.Controls.Add(lbluserName);
                this.pnlTodayUserStatistics.Controls.Add(lbltotalLoggedInTime);
                this.pnlTodayUserStatistics.Controls.Add(lblavgperorderTimeTaken);
                this.pnlTodayUserStatistics.Controls.Add(lblorderPerHouravgRate);
                this.pnlTodayUserStatistics.Controls.Add(lblordersProcessed);

                TodayViewLabelsAlreadyAdded = true;
            }    
        }
        private void LoadUserStatisticsForRangeView()
        {

            grdRangeReportView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            grdRangeReportView.DoubleBuffered(true);
            grdRangeReportView.BackgroundColor = Color.WhiteSmoke;
            grdRangeReportView.ScrollBars = ScrollBars.Both;
            grdRangeReportView.DataSource = GetParsedRangeReport();
            grdRangeReportView.Columns[0].Frozen = true;
            grdRangeReportView.RowTemplate.MinimumHeight = 35;
            grdRangeReportView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            if (grdRangeReportView.Rows.Count > 0)
            {
                for (int columnIndex = 0; columnIndex < grdRangeReportView.Columns.Count; columnIndex++)
                {
                    if (columnIndex == 0)
                    {
                        grdRangeReportView.Columns[columnIndex].MinimumWidth = 100;
                    }
                    else
                    {
                        grdRangeReportView.Columns[columnIndex].MinimumWidth = 280;
                    }
                }

            }
            grdRangeReportView.Columns[1].MinimumWidth = 230;
            grdRangeReportView.Visible = true;
        }

        private DataTable GetParsedRangeReport()
        {
            var result = new DataTable();
            var totalSummaryList =new List<UserStatisticsSummary>();

            result.Columns.Add("UserName");
            result.Columns.Add("Summary\n\n" + "Total_Hours".AppendSpaces(4) + "Total_Orders".AppendSpaces(4) + "Order/Hour");
            var colIndex = 2;
            //enter usernames

            var index = 0;
            if (RangeFinalDigestList.Count == 0)
                return result;

            foreach (var weeklyDigest in RangeFinalDigestList.FirstOrDefault().ToList())
            {
                var userStatSummary = new UserStatisticsSummary();
                var userName = weeklyDigest.UserName;
                var summary = string.Empty.AppendSpaces(3);
                var userNameRecords = RangeFinalDigestList.SelectMany(s => s.Where(d => d.UserName.Equals(userName))).ToList();

                var totalHourWorked = TimeSpan.FromMinutes(userNameRecords.Sum(x => x.TotalLoggedInTime.TotalMinutes));
                var totalOrdersProcessed = userNameRecords.Sum(x => x.TotalOrderProcessed);
                var totalPerHourProcessRate = Convert.ToDecimal(totalOrdersProcessed) / Convert.ToDecimal(totalHourWorked.TotalHours > 0 ? totalHourWorked.TotalHours : 1);
                var hoursWorked = totalHourWorked.TotalHours > 24 ? totalHourWorked.TotalHours : totalHourWorked.Hours;

                userStatSummary.TotalHourWorked = totalHourWorked;
                userStatSummary.TotalOrdersProcessed = totalOrdersProcessed;
                totalSummaryList.Add(userStatSummary);


                summary += String.Format("{0:00}:{1:00}:{2:00}", hoursWorked, totalHourWorked.Minutes, totalHourWorked.Seconds).AppendSpaces(14);
                summary += String.Format("{0:000}", totalOrdersProcessed).AppendSpaces(16);
                summary += String.Format("{0:00.00}", totalPerHourProcessRate);

                if (totalHourWorked.TotalSeconds==0)
                {
                    RangeFinalDigestList.ForEach(r => r.RemoveAll(c => c.UserName.Equals(userName)));
                }
                else
                {
                    result.Rows.Add(userName);
                    result.Rows[index].SetField(1, summary);
                    index++;
                }
            }
            foreach (var digestList in RangeFinalDigestList.ToList())
            {
                if (digestList.Count > 0)
                {
                    result.Columns.Add(digestList.FirstOrDefault().OfDate.ToString("MMM dd ") + "\n\nHours_Worked  \t\t\t Orders  \t\t\t Order/Hour  \t\t\t PerOrderAvg");
                }

                var rowIndex = 0;
                foreach (var digest in digestList)
                {
                    result.Rows[rowIndex].SetField(0, digest.UserName);

                    var liTime = digest.TotalLoggedInTime;
                    var loggedInTime = String.Format("{0:00}:{1:00}:{2:00}", liTime.Hours, liTime.Minutes, liTime.Seconds);
                    var avgperorderTimeTaken = String.Format("{0:00.00}", ODM.Core.Helpers.TimerUtils.GetMinutes(Convert.ToDouble(digest.PerOrderAvgProcessTime)));
                    var orderPerHouravgRate = String.Format("{0:00.00}", digest.HourlyPerOrderProcessRate);
                    var ordersProcessed = String.Format("{0:000}", digest.TotalOrderProcessed);
                    result.Rows[rowIndex].SetField(colIndex, string.Empty.AppendSpaces(5) + loggedInTime.AppendSpaces(12) + ordersProcessed.AppendSpaces(12) + orderPerHouravgRate.AppendSpaces(15) + avgperorderTimeTaken);
                    rowIndex++;
                }
                colIndex++;
            }

            string totalOfSummary = string.Empty.AppendSpaces(3);
            var totalSummaryHours = TimeSpan.FromMinutes(totalSummaryList.Sum(x => x.TotalHourWorked.TotalMinutes));
            var totalSummaryOrder = totalSummaryList.Sum(x => x.TotalOrdersProcessed);
            var totalSummaryPerHourProcessRate = Convert.ToDecimal(totalSummaryOrder) / Convert.ToDecimal(totalSummaryHours.TotalHours > 0 ? totalSummaryHours.TotalHours : 1);

            var totalSummaryHoursWorked = totalSummaryHours.TotalHours > 24 ? totalSummaryHours.TotalHours : totalSummaryHours.Hours;
            totalOfSummary += String.Format("{0:00}:{1:00}:{2:00}", totalSummaryHoursWorked, totalSummaryHours.Minutes, totalSummaryHours.Seconds).AppendSpaces(14);
            totalOfSummary += String.Format("{0:000}", totalSummaryOrder).AppendSpaces(16);
            totalOfSummary += String.Format("{0:00.00}", totalSummaryPerHourProcessRate);

            result.Rows.Add();
            result.Rows[result.Rows.Count - 1].SetField(0,"Totals");
            result.Rows[result.Rows.Count - 1].SetField(1, totalOfSummary);

            return result;
        }
        #endregion

        #region Async called  methods to Get user Statistics Based on Range

        public void PopulateCurrentDateFinalDigestList()
        {
            CurrentDateFinalDigestList = statisticsReportMethods.GetAllUsersReportForDate(DateTime.UtcNow);
            CurrentDateFinalDigestList.RemoveAll(r => r.TotalLoggedInTime.TotalSeconds == 0);
        }
        public void PopulateWeeklyFinalDigestList()
        {
            RangeFinalDigestList = statisticsReportMethods.GetAllUsersRangeReport(DateTime.UtcNow.AddDays(-7), DateTime.UtcNow);
        }
        private void PopulateCustomRangeFinalDigestList()
        {
            RangeFinalDigestList = statisticsReportMethods.GetAllUsersRangeReport(dateTimePickFrom.Value, dateTimePickTo.Value);
        }
        #endregion

        private void chkAutoRefresh_CheckedChanged(object sender, EventArgs e)
        {
            SetUnsetAutoRefresh();
        }

        private void SetUnsetAutoRefresh()
        {
            CheckForIllegalCrossThreadCalls = false;
            if (AutoRefreshTimer != null)
            {
                AutoRefreshTimer.Dispose();
            }

            AutoRefreshTimer = new System.Timers.Timer();
            AutoRefreshTimer.Interval = TimeSpan.FromMinutes(Convert.ToInt32(cmbAutoRefreshInterval.SelectedItem.ToString())).TotalMilliseconds;
            AutoRefreshTimer.Elapsed += (s, e) => { SetUnsetAutoRefresh(); };

            if (chkAutoRefresh.Checked)
            {
                AutoRefreshTimer.Start();
                Action act = () => { btnRefresh.PerformClick(); };
                this.BeginInvoke(act);
                
                lblLastAutoUpdate.Text = "Last updated at : " + DateTime.Now.ToLocalTime();
            }
            else
            {
                AutoRefreshTimer.Stop();
                lblLastAutoUpdate.Text = string.Empty;
            }
        }

        private void cmbAutoRefreshInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetUnsetAutoRefresh();
        }

    }
}
