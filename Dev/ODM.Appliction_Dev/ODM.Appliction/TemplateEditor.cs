﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class TemplateEditor : Form
    {
        #region "Global variables and objects"
        Guid TemplateID = new Guid();
        DataTable objDt = new DataTable();
        SqlConnection connection = new SqlConnection();
        bool blIsLoadCall = false;
        string strIsMailOrSubjectCall = "";
        string strSelectedTableColumnName = "";
        #endregion

        #region "Constructors"
        public TemplateEditor()
        {
            InitializeComponent();
            connection.ConnectionString = MakeConnection.GetConnection();
            blIsLoadCall = true;
            BindDropdownLists();
            GetTemplateEditorSettings();
            CreateAndAttachMenuStrip();
            blIsLoadCall = false;
        }
        public TemplateEditor(Guid TemplateId)
        {
            InitializeComponent();
            TemplateID = TemplateId;
            connection.ConnectionString = MakeConnection.GetConnection();
            blIsLoadCall = true;
            BindDropdownLists();
            GetTemplateEditorSettings();
            CreateAndAttachMenuStrip();
            blIsLoadCall = false;
        }
        
        #endregion

        #region "Events"
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            Form frmAddCondition = Application.OpenForms["AddCondition"];
            if (frmAddCondition == null)
            {
                AddCondition obj = new AddCondition(TemplateID);
                obj.Show();
            }
        }
        private void btnChangeCondition_Click(object sender, EventArgs e)
        {
            Form frmAddCondition = Application.OpenForms["AddCondition"];
            if (frmAddCondition == null)
            {
                AddCondition obj = new AddCondition(TemplateID, true, ddlCondition.SelectedValue.ToString(), ddlCondition.Text);
                obj.Show();
            }
        }
        private void btnDeleteCondition_Click(object sender, EventArgs e)
        {
            DialogResult ConfirmDelete = MessageBox.Show("Are you sure, you want to delete selected Condition\n \"" + ddlCondition.Text + "\"", "Delete Condition", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ConfirmDelete == DialogResult.Yes)
            {
                string strConditionToDelete = ddlCondition.SelectedValue.ToString();
                string strResult = GlobalVariablesAndMethods.AddUpdateDeleteCondition(strConditionToDelete, "", "Delete");
                if (strResult.ToLower().Contains("error"))
                { MessageBox.Show(strResult, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                else
                {
                    MessageBox.Show(strResult, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    BindDropdownLists();
                    GetTemplateEditorSettings();
                }
            }
        }
        private void ddlCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!blIsLoadCall)
                GetTemplateEditorSettings();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            AddUpdateTemplateEditorSettings();
        }
        private void btnPreview_Click(object sender, EventArgs e)
        {

        }
        private void btnPreviewDummy_Click(object sender, EventArgs e)
        {

        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //Event which will fire to handle the selection of current selected option
        private void MenuOfColumnsClick(object sender, EventArgs e)
        {
            //Getting the text of selected menu(column) and its parent(table) and making a condition by concating [{ and }]
            ToolStripMenuItem SelectedMenu = (ToolStripMenuItem)sender;
            string strSelectedMenu = SelectedMenu.Text;
            string strSelectedMenuParent = SelectedMenu.OwnerItem.Text;
            strSelectedTableColumnName = "[{" + strSelectedMenuParent + "." + strSelectedMenu + "}]";//Condition text
            //End of making condition for subject and mail body

            //strIsMailOrSubjectCall == "MB" means we have focus on Mail Body i.e email template textbox
            if (strIsMailOrSubjectCall == "MB")
            {
                int intSelection = txtTemplate.SelectionStart;//Current cursor point on template textbox
                //putting condition text at the current cursor location in txtTemplate textbox
                txtTemplate.Text = txtTemplate.Text.Insert(intSelection, strSelectedTableColumnName);
                //to keep cursor point where it is now in txtTemplate textbox   
                txtTemplate.SelectionStart = intSelection + strSelectedTableColumnName.Length;
            }
            //strIsMailOrSubjectCall == "SUB" means we have focus on Subject part of mail i.e subject textbox
            else if (strIsMailOrSubjectCall == "SUB")
            {
                int intSelection = txtSubject.SelectionStart;//Current cursor point on subject textbox
                txtSubject.Text = txtSubject.Text.Insert(txtSubject.SelectionStart, strSelectedTableColumnName);
                txtSubject.SelectionStart = intSelection + strSelectedTableColumnName.Length;
            }
        }
        private void txtSubject_Enter(object sender, EventArgs e)
        {
            strIsMailOrSubjectCall = "SUB";
        }
        private void txtSubject_Leave(object sender, EventArgs e)
        {
            strIsMailOrSubjectCall = "";
        }
        private void txtTemplate_Enter(object sender, EventArgs e)
        {
            strIsMailOrSubjectCall = "MB";
        }
        private void txtTemplate_Leave(object sender, EventArgs e)
        {
            strIsMailOrSubjectCall = "";
        }

        #endregion

        #region "Methods"
        private void BindDropdownLists()
        {
            ddlEmailAccount.DataSource = GlobalVariablesAndMethods.GetEmailAccounts();
            ddlEmailAccount.ValueMember = "ID";
            ddlEmailAccount.DisplayMember = "EmailAccount";

            ddlCondition.DataSource = GlobalVariablesAndMethods.GetEmailConditions();
            ddlCondition.ValueMember = "ID";
            ddlCondition.DisplayMember = "Text";

            ddlCondition.SelectedValue = "00000000-0000-0000-0000-000000000000";
            ddlEmailAccount.SelectedValue = "00000000-0000-0000-0000-000000000000";

            btnChangeCondition.Enabled = false;
            //chkIsEnabled.Checked = true;
            //chkIsEnabled.Enabled = false;
        }
        private void GetTemplateEditorSettings()
        {
            try
            {
                Guid ConditionID = new Guid();
                if (ddlCondition.SelectedValue != null)
                {
                    ConditionID = new Guid(ddlCondition.SelectedValue.ToString());
                }
                objDt = GlobalVariablesAndMethods.GetTemplateDetailsByTemplateId(TemplateID, ConditionID);
                if (objDt.Rows.Count > 0)
                {
                    txtSubject.Text = Convert.ToString(objDt.Rows[0]["Subject"]);
                    txtTemplate.Text = Convert.ToString(objDt.Rows[0]["Text"]);
                    chkIsEnabled.Checked = Convert.ToBoolean(objDt.Rows[0]["IsEnabled"]);
                    chkInvoice.Checked = Convert.ToBoolean(objDt.Rows[0]["IsPDF"]);
                    chkOrderImage.Checked = Convert.ToBoolean(objDt.Rows[0]["IsImage"]);
                    chkHTML.Checked = Convert.ToBoolean(objDt.Rows[0]["IsHtml"]);
                    chkPrivew.Checked = Convert.ToBoolean(objDt.Rows[0]["IsPreview"]);
                    ddlEmailAccount.SelectedValue = Convert.ToString(objDt.Rows[0]["EmailAccId"]);
                }
                else
                {
                    txtSubject.Text = "";
                    txtTemplate.Text = "";
                    chkIsEnabled.Checked = false;
                    chkInvoice.Checked = false;
                    chkOrderImage.Checked = false;
                    chkHTML.Checked = false;
                    chkPrivew.Checked = false;
                    ddlEmailAccount.SelectedValue = "00000000-0000-0000-0000-000000000000";
                }
                if (ddlCondition.SelectedValue != null && ddlCondition.SelectedValue.ToString() == "00000000-0000-0000-0000-000000000000")
                {
                    btnChangeCondition.Enabled = false;
                    //chkIsEnabled.Checked = true;
                    //chkIsEnabled.Enabled = false;
                }
                else
                {
                    btnChangeCondition.Enabled = true;
                    chkIsEnabled.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occrred\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }
        private void AddUpdateTemplateEditorSettings()
        {
            try
            {
                Guid dummyID = new Guid();
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                SqlParameter[] objParam = new SqlParameter[12];

                objParam[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = dummyID;

                objParam[1] = new SqlParameter("@Text", SqlDbType.VarChar, int.MaxValue);
                objParam[1].Value = txtTemplate.Text;

                objParam[2] = new SqlParameter("@ConditionId", SqlDbType.UniqueIdentifier);
                objParam[2].Value = ddlCondition.SelectedValue;

                objParam[3] = new SqlParameter("@EmailAccId", SqlDbType.UniqueIdentifier);
                objParam[3].Value = ddlEmailAccount.SelectedValue;

                objParam[4] = new SqlParameter("@TemplateId", SqlDbType.UniqueIdentifier);
                objParam[4].Value = TemplateID;

                objParam[5] = new SqlParameter("@Subject", SqlDbType.VarChar, 1000);
                objParam[5].Value = txtSubject.Text;

                objParam[6] = new SqlParameter("@IsEnabled", SqlDbType.Bit);
                objParam[6].Value = chkIsEnabled.Checked;

                objParam[7] = new SqlParameter("@IsPreview", SqlDbType.Bit);
                objParam[7].Value = chkPrivew.Checked;

                objParam[8] = new SqlParameter("@IsHtml", SqlDbType.Bit);
                objParam[8].Value = chkHTML.Checked;

                objParam[9] = new SqlParameter("@IsPDF", SqlDbType.Bit);
                objParam[9].Value = chkInvoice.Checked;

                objParam[10] = new SqlParameter("@IsImage", SqlDbType.Bit);
                objParam[10].Value = chkOrderImage.Checked;

                objParam[11] = new SqlParameter("@Result", SqlDbType.VarChar, 3000);
                objParam[11].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateTemplateConfig", objParam);
                string strResult = Convert.ToString(objParam[11].Value);
                if (strResult.ToUpper().Contains("SUCCESSFULLY"))
                {
                    MessageBox.Show(strResult, "Template Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Error occurred while saving template info\n" + strResult, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }
        /// <summary>
        /// Creating menu so that user could choose those to show in mail body or subject text
        /// </summary>
        private void CreateAndAttachMenuStrip()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            DataSet ds = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDataAndColumnNamesForMenu");

            //Creating object of menustrip and setting its location on this screen
            MenuStrip objMenu = new MenuStrip();
            objMenu.Name = "MenuOD";
            objMenu.Text = "Make Selection";
            objMenu.Dock = DockStyle.None;
            objMenu.Location = new Point(45, 152);
            //End of creation of menustrip and location

            //Main and first menu that will display for user
            ToolStripMenuItem MnuStripItemMain = new ToolStripMenuItem("Choose Column");
            objMenu.Items.Add(MnuStripItemMain);
            //End of main menu

            if (ds != null && ds.Tables.Count > 0)
            {
                //Looping for the tables which we got to add as menu
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    DataTable objDt = new DataTable();
                    objDt = ds.Tables[i];
                    string strTableName = Convert.ToString(objDt.Rows[i]["Table_Name"]);//Table name as inner main menu

                    //Creating inner main menu to show list of tables
                    ToolStripMenuItem MenuTable = new ToolStripMenuItem(strTableName);
                    MnuStripItemMain.DropDownItems.Add(MenuTable);

                    //Looping to get columns existence under a table
                    foreach (DataRow dr in objDt.Rows)
                    {
                        //Adding column as menu and that will get data from db to show in mail, for subject and mail body
                        ToolStripMenuItem MenuColumnName = new ToolStripMenuItem(dr["Column_Name"].ToString(), null, MenuOfColumnsClick);
                        MenuTable.DropDownItems.Add(MenuColumnName);
                    }
                }
            }
            this.Controls.Add(objMenu);
        }
        #endregion
    }
}
