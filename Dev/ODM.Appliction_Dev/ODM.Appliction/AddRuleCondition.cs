﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Data.Util;
namespace ODM.Appliction
{
    public partial class AddRuleCondition : Form
    {
        Guid ID = new Guid();
        string strMode = "";
        public AddRuleCondition()
        {
            InitializeComponent();
        }
        public AddRuleCondition(string strMode, string strConditionText, string strConditionID)
        {
            InitializeComponent();
            txtNewCondition.Text = strConditionText;
            this.ID = new Guid(strConditionID);
            this.strMode = strMode;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            string strResult = "";
            if (strMode.ToUpper() == "UPDATE")
            {
                strResult = GlobalVariablesAndMethods.GetAddUpdateMessageRuleCondition("UPDATE", Convert.ToString(this.ID), txtNewCondition.Text.ToString());
            }
            else
            {
                strResult = GlobalVariablesAndMethods.GetAddUpdateMessageRuleCondition("UPDATE", Convert.ToString(Guid.NewGuid()), txtNewCondition.Text.ToString());
            }
            if (strResult == "1")
            {
                MessageBox.Show("Condition Added Successfully", "Add New Rule Condition", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (strResult == "2")
            {
                MessageBox.Show("Condition Updated Successfully", "Update Rule Condition", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
