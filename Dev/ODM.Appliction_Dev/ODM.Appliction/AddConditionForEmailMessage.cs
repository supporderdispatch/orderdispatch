﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Data.Util;
namespace ODM.Appliction
{
    public partial class AddConditionForEmailMessage : Form
    {
        
        Guid TemplateID = new Guid();
        string strConditionID = "";
        string Mode = "Insert";
         public AddConditionForEmailMessage()
        {
            InitializeComponent();
        }
      
        public AddConditionForEmailMessage(Guid TemplateId, bool blIsUpdateCall = false, string ConditionID = "", string strConditionText = "")
        {
            InitializeComponent();
            TemplateID = TemplateId;
            if (blIsUpdateCall)
            {
                Mode = "Update";
                strConditionID = ConditionID;
                btnCreate.Text = "Update";
                this.Text = "Update Condition";
                txtCondition.Text = strConditionText;
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            AddUpdateCondition(Mode, strConditionID);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// To add or update email condition
        /// </summary>
        /// <param name="strMode">Either Insert or Update</param>
        /// <param name="strConditionIDs">Id for upate otherwise pass blank string(to insert)</param>
        private void AddUpdateCondition(string strMode, string strConditionIDs)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCondition.Text))
                {
                    lblRequired.Show();
                }
                else
                {
                    lblRequired.Hide();
                    if (txtCondition.Text.ToUpper() == "DEFAULT" && strMode == "Insert")
                    {
                        strConditionIDs = "00000000-0000-0000-0000-000000000000";
                    }
                    else if (txtCondition.Text.ToUpper() != "DEFAULT" && strMode == "Insert")
                    {
                        Guid ID = Guid.NewGuid();
                        strConditionIDs = ID.ToString();
                    }

                    string strResult = GlobalVariablesAndMethods.AddUpdateDeleteConditionForMessage(strConditionIDs, txtCondition.Text, strMode);
                    if (strResult.ToLower().Contains("error"))
                    { MessageBox.Show(strResult, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    else
                    {
                        MessageBox.Show(strResult, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                        Form frmEmailMessageTemplateSettings = Application.OpenForms["EmailMessageTemplateSettings"];
                        EmailMessageTemplateSettings obj = new EmailMessageTemplateSettings(TemplateID);
                        if (frmEmailMessageTemplateSettings == null)
                        {
                            obj.Show();
                        }
                        else
                        {
                            frmEmailMessageTemplateSettings.Close();
                            obj.Show();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCondition_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                AddUpdateCondition(Mode, strConditionID);
            }
        }

       
    }
}
