﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Data.Util;
using System.Data.SqlClient;
using System.Net.Mail;
namespace ODM.Appliction
{
    public partial class AssignTicket : Form
    {
        string[] _strArrGrdSelectedValues;
        MessageFolders objMessageFolders = new MessageFolders("AssignMessage");
        public AssignTicket(string[] strArrGrdSelectedValues)
        {
            InitializeComponent();
            _strArrGrdSelectedValues = strArrGrdSelectedValues;
            BindFolderListToDropDown();
        }
        private void BindFolderListToDropDown()
        {
            DataTable objDt = new DataTable();           
            objDt = objMessageFolders.GetUpdateMessageFolders("GETALL", new Guid(), GlobalVariablesAndMethods.UserDetails.Userid.ToString());
            DataView dv = new DataView(objDt);
            dv.RowFilter = "FolderName Like '%Assigned Messages%'";
          //  dv.RowStateFilter = DataViewRowState.ModifiedCurrent;
            objDt = dv.ToTable();
            if (objDt.Rows.Count > 0)
            {
                ddlFolders.DataSource = objDt;
                ddlFolders.ValueMember = "ID";
                ddlFolders.DisplayMember = "FolderName";
            }
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string strResult;
         //   DataTable objDt = new DataTable();
            if (ddlFolders.SelectedValue != null) {
                strResult = GlobalVariablesAndMethods.GetAddUpdateMessages(new MailMessage(), "UPDATE", Convert.ToInt32(_strArrGrdSelectedValues.ElementAt(0).ToString()), _strArrGrdSelectedValues.ElementAt(2).ToString(), _strArrGrdSelectedValues.ElementAt(1).ToString(), _strArrGrdSelectedValues.ElementAt(3).ToString(), _strArrGrdSelectedValues.ElementAt(4).ToString(), ddlFolders.SelectedValue.ToString(), new Guid(_strArrGrdSelectedValues.ElementAt(8).ToString()));
            }
            this.Close();
            
            
             
        }
    }
}
