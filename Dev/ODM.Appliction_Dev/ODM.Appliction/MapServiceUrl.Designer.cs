﻿namespace ODM.Appliction
{
    partial class MapServiceUrl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapServiceUrl));
            this.lblCountryName = new System.Windows.Forms.Label();
            this.ddlCountryName = new System.Windows.Forms.ComboBox();
            this.lblServiceUrlText = new System.Windows.Forms.Label();
            this.txtServiceURL = new System.Windows.Forms.TextBox();
            this.lblUrlRequired = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblCountryName
            // 
            this.lblCountryName.AutoSize = true;
            this.lblCountryName.Location = new System.Drawing.Point(9, 11);
            this.lblCountryName.Name = "lblCountryName";
            this.lblCountryName.Size = new System.Drawing.Size(113, 13);
            this.lblCountryName.TabIndex = 0;
            this.lblCountryName.Text = "Choose Country Name";
            // 
            // ddlCountryName
            // 
            this.ddlCountryName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCountryName.FormattingEnabled = true;
            this.ddlCountryName.Location = new System.Drawing.Point(126, 8);
            this.ddlCountryName.Name = "ddlCountryName";
            this.ddlCountryName.Size = new System.Drawing.Size(284, 21);
            this.ddlCountryName.TabIndex = 1;
            this.ddlCountryName.SelectedIndexChanged += new System.EventHandler(this.ddlCountryName_SelectedIndexChanged);
            // 
            // lblServiceUrlText
            // 
            this.lblServiceUrlText.AutoSize = true;
            this.lblServiceUrlText.Location = new System.Drawing.Point(54, 50);
            this.lblServiceUrlText.Name = "lblServiceUrlText";
            this.lblServiceUrlText.Size = new System.Drawing.Size(68, 13);
            this.lblServiceUrlText.TabIndex = 2;
            this.lblServiceUrlText.Text = "Service URL";
            // 
            // txtServiceURL
            // 
            this.txtServiceURL.Location = new System.Drawing.Point(126, 46);
            this.txtServiceURL.Name = "txtServiceURL";
            this.txtServiceURL.Size = new System.Drawing.Size(284, 20);
            this.txtServiceURL.TabIndex = 3;
            this.txtServiceURL.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtServiceURL_KeyPress);
            // 
            // lblUrlRequired
            // 
            this.lblUrlRequired.AutoSize = true;
            this.lblUrlRequired.ForeColor = System.Drawing.Color.Red;
            this.lblUrlRequired.Location = new System.Drawing.Point(416, 49);
            this.lblUrlRequired.Name = "lblUrlRequired";
            this.lblUrlRequired.Size = new System.Drawing.Size(50, 13);
            this.lblUrlRequired.TabIndex = 4;
            this.lblUrlRequired.Text = "Required";
            this.lblUrlRequired.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(178, 78);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(262, 78);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // MapServiceUrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 134);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblUrlRequired);
            this.Controls.Add(this.txtServiceURL);
            this.Controls.Add(this.lblServiceUrlText);
            this.Controls.Add(this.ddlCountryName);
            this.Controls.Add(this.lblCountryName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MapServiceUrl";
            this.Text = "Map ServiceUrl With Country";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCountryName;
        private System.Windows.Forms.ComboBox ddlCountryName;
        private System.Windows.Forms.Label lblServiceUrlText;
        private System.Windows.Forms.TextBox txtServiceURL;
        private System.Windows.Forms.Label lblUrlRequired;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}