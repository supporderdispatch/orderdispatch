﻿namespace ODM.Appliction
{
    partial class SaveImage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaveImage));
            this.picBoxSaveImage = new System.Windows.Forms.PictureBox();
            this.btnTryAgain = new System.Windows.Forms.Button();
            this.btnSaveImage = new System.Windows.Forms.Button();
            this.panelSaveImage = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSaveImage)).BeginInit();
            this.SuspendLayout();
            // 
            // picBoxSaveImage
            // 
            this.picBoxSaveImage.BackColor = System.Drawing.Color.Black;
            this.picBoxSaveImage.Location = new System.Drawing.Point(58, 95);
            this.picBoxSaveImage.Name = "picBoxSaveImage";
            this.picBoxSaveImage.Size = new System.Drawing.Size(796, 521);
            this.picBoxSaveImage.TabIndex = 21;
            this.picBoxSaveImage.TabStop = false;
            // 
            // btnTryAgain
            // 
            this.btnTryAgain.Location = new System.Drawing.Point(258, 12);
            this.btnTryAgain.Name = "btnTryAgain";
            this.btnTryAgain.Size = new System.Drawing.Size(109, 32);
            this.btnTryAgain.TabIndex = 22;
            this.btnTryAgain.Text = "Try Again";
            this.btnTryAgain.UseVisualStyleBackColor = true;
            this.btnTryAgain.Click += new System.EventHandler(this.btnTryAgain_Click);
            // 
            // btnSaveImage
            // 
            this.btnSaveImage.Location = new System.Drawing.Point(542, 12);
            this.btnSaveImage.Name = "btnSaveImage";
            this.btnSaveImage.Size = new System.Drawing.Size(109, 32);
            this.btnSaveImage.TabIndex = 23;
            this.btnSaveImage.Text = "Save Image";
            this.btnSaveImage.UseVisualStyleBackColor = true;
            this.btnSaveImage.Click += new System.EventHandler(this.btnSaveImage_Click);
            // 
            // panelSaveImage
            // 
            this.panelSaveImage.Location = new System.Drawing.Point(58, 95);
            this.panelSaveImage.Name = "panelSaveImage";
            this.panelSaveImage.Size = new System.Drawing.Size(796, 521);
            this.panelSaveImage.TabIndex = 24;
            // 
            // SaveImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 655);
            this.Controls.Add(this.btnSaveImage);
            this.Controls.Add(this.picBoxSaveImage);
            this.Controls.Add(this.btnTryAgain);
            this.Controls.Add(this.panelSaveImage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SaveImage";
            this.Text = "SaveImage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SaveImage_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSaveImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxSaveImage;
        private System.Windows.Forms.Button btnTryAgain;
        private System.Windows.Forms.Button btnSaveImage;
        private System.Windows.Forms.Panel panelSaveImage;
    }
}