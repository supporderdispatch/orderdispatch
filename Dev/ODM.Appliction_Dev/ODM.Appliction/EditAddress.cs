﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class EditAddress : Form
    {
        DataTable _objOrderDetails = new DataTable();
        DataTable _objDtCountry = new DataTable();
        Guid OrderId = new Guid();
        Guid AddressId = new Guid();
        Guid CustomerId = new Guid();

        public string OrderID;
        public string OrderItemID;
        public string BarCode;
        bool blIsScannedChecked = false;

        public EditAddress()
        {
            InitializeComponent();
        }
        public EditAddress(DataTable objOrderDetails, DataTable objDtCountry, string strOrderId, string strOrderItemID, string strBarCode, bool IsScannedChecked)
        {
            InitializeComponent();
            _objOrderDetails = objOrderDetails;
            _objDtCountry = objDtCountry;
            OrderID = strOrderId;
            OrderItemID = strOrderItemID;
            BarCode = strBarCode;
            blIsScannedChecked = IsScannedChecked;
            PopulateControls();
        }
        private void UpdateAddress()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            Guid CountryID = new Guid(Convert.ToString(ddlEditCounrty.SelectedValue));
            
            try
            {
                SqlParameter[] objAddressParams = new SqlParameter[15];

                objAddressParams[0] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                objAddressParams[0].Value = OrderId;
                objAddressParams[1] = new SqlParameter("@CustomerId", SqlDbType.UniqueIdentifier);
                objAddressParams[1].Value = CustomerId;

                objAddressParams[2] = new SqlParameter("@Company", SqlDbType.VarChar,500);
                objAddressParams[2].Value = txtCompany.Text;

                objAddressParams[3] = new SqlParameter("@EmailAddress", SqlDbType.VarChar, 500);
                objAddressParams[3].Value = txtEmail.Text;

                objAddressParams[4] = new SqlParameter("@ChannelBuyerName", SqlDbType.VarChar, 500);
                objAddressParams[4].Value = txtChannelBuyer.Text;

                objAddressParams[5] = new SqlParameter("@FullName", SqlDbType.VarChar, 500);
                objAddressParams[5].Value = txtName.Text;

                objAddressParams[6] = new SqlParameter("@Town", SqlDbType.VarChar, 500);
                objAddressParams[6].Value = txtTown.Text;

                objAddressParams[7] = new SqlParameter("@Address1", SqlDbType.VarChar, 500);
                objAddressParams[7].Value = txtAddress1.Text;
                objAddressParams[8] = new SqlParameter("@Address2", SqlDbType.VarChar, 500);
                objAddressParams[8].Value = txtAddress2.Text;
                objAddressParams[9] = new SqlParameter("@Address3", SqlDbType.VarChar, 500);
                objAddressParams[9].Value = txtAddress3.Text;
                //objAddressParams[7] = new SqlParameter("@Town", SqlDbType.VarChar, 500);
                //objAddressParams[7].Value = txtTown.Text;

                objAddressParams[10] = new SqlParameter("@Region", SqlDbType.VarChar, 500);
                objAddressParams[10].Value = txtRegion.Text;

                objAddressParams[11] = new SqlParameter("@PostCode", SqlDbType.VarChar, 500);
                objAddressParams[11].Value = txtPostalCode.Text;

                objAddressParams[12] = new SqlParameter("@CountryID", SqlDbType.UniqueIdentifier);
                objAddressParams[12].Value = CountryID;

                objAddressParams[13] = new SqlParameter("@PhoneNumber", SqlDbType.VarChar, 50);
                objAddressParams[13].Value = txtPhoneNumber.Text;

                objAddressParams[14] = new SqlParameter("@Result", SqlDbType.Int);
                objAddressParams[14].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "EditAddress", objAddressParams);

                Int16 intResult = Convert.ToInt16(objAddressParams[14].Value);
                if (intResult == 1)
                {
                    this.Close();
                    Form frmProcessOrder = Application.OpenForms["ProcessOrder"];
                    if (frmProcessOrder != null)
                    {
                        frmProcessOrder.Close();
                        ProcessOrder objOrder = new ProcessOrder(OrderID, OrderItemID, BarCode, blIsScannedChecked);
                        objOrder.Show();
                    }
                    MessageBox.Show("Address updated successfully!");
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
            finally
            {
                connection.Close();
            }
        }
        private void PopulateControls()
        {
            if (_objDtCountry != null && _objDtCountry.Rows.Count > 0)
            {
                ddlEditCounrty.ValueMember = "CountryId";
                ddlEditCounrty.DisplayMember = "CountryName";
                ddlEditCounrty.DataSource = _objDtCountry;
            }
            if (_objOrderDetails != null && _objDtCountry.Rows.Count > 0)
            {
                txtChannelBuyer.Text = Convert.ToString(_objOrderDetails.Rows[0]["ChannelBuyerName"]);
                txtEmail.Text = Convert.ToString(_objOrderDetails.Rows[0]["EmailAddress"]);
                txtName.Text = Convert.ToString(_objOrderDetails.Rows[0]["CustomerFullName"]);
                txtCompany.Text = Convert.ToString(_objOrderDetails.Rows[0]["CustomerCompany"]);
                txtTown.Text = Convert.ToString(_objOrderDetails.Rows[0]["CustomerAddress"]);
                CustomerId = new Guid(Convert.ToString(_objOrderDetails.Rows[0]["CustomerId"]));
                txtAddress1.Text = Convert.ToString(_objOrderDetails.Rows[0]["CompanyAddress"]);
                txtAddress2.Text = Convert.ToString(_objOrderDetails.Rows[0]["Address2"]);
                txtAddress3.Text = Convert.ToString(_objOrderDetails.Rows[0]["Address3"]);
                //     txtTown.Text = Convert.ToString(_objOrderDetails.Rows[0]["Town"]);
                txtRegion.Text = Convert.ToString(_objOrderDetails.Rows[0]["Region"]);
                txtPostalCode.Text = Convert.ToString(_objOrderDetails.Rows[0]["PostCode"]);
                ddlEditCounrty.SelectedValue = Convert.ToString(_objOrderDetails.Rows[0]["CountryId"]);
                txtPhoneNumber.Text = Convert.ToString(_objOrderDetails.Rows[0]["PhoneNumber"]);
                OrderId = new Guid(Convert.ToString(_objOrderDetails.Rows[0]["Orderid"]));
                AddressId = new Guid(Convert.ToString(_objOrderDetails.Rows[0]["AddressId"]));
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            UpdateAddress();
        }

    }
}
