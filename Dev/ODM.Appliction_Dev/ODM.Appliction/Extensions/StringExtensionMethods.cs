﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Appliction.Extensions
{
    public static class StringExtensionMethods
    {
        public static string AppendSpaces(this string value, int numOfRepeatChars)
        {
            for (int i = 0; i < numOfRepeatChars; i++)
            {
                value += " ";
            }
            return value;
        }
    }
}
