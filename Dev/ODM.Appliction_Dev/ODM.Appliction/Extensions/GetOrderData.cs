﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using ODM.Entities;
using ODM.Core;
using System.Threading.Tasks;

namespace ODM.Appliction
{
    public partial class GetOrderData
    {
        //
        //summary:
        //  custom dialogue window showing bin details.
        //
        //parameters:
        //  caption: title of bin prompt 
        //  gridItemsData: details of order assigned to bin
        //  bin : details of bin , including bin number and items and their details
        //
        private String[] BinDialogue(string caption, DataTable gridItemsData, bool readyToScan, Bin bin)
        {
            DataTable _grdItems = gridItemsData.Copy();
            var selectedOption = BinPromptResult.Cancel;
            int numOfItemsWithoutBarcode = 0;
            var anyDataChange = false;
            Form prompt = new Form();
            string anyBarcode = string.Empty;
            prompt.ShowIcon = false;
            prompt.Width = 480;
            prompt.Height = 350;
            prompt.Text = caption;
            prompt.BackColor = Color.WhiteSmoke;
            prompt.Cursor = Cursors.Hand;
            prompt.StartPosition = FormStartPosition.CenterScreen;
            //
            //label
            //
            Label statusLabel = new Label() { Left = 20, Top = 20, Width = 300, Height = 80 };
            Label count = new Label() { Left = 20, Width = 80, Top = 85 };
            //
            //grid
            //
            DataGridView grdItems = new DataGridView() { Left = 20, Width = 420, Height = 100, Top = 110 };
            grdItems.DoubleBuffered(true);
            //
            //button
            Button addItemsHavingNoBarcode = new Button() { Text = "Add items having no barcode to Bin ", Left = 20, Width = 250, Top = 225 };
            Button deleteBin = new Button() { Text = "Delete Bin", Left = 20, Width = 100, Top = 260 };
            Button moveToScan = new Button() { Text = "Proceed to Scan", Left = 140, Width = 100, Top = 260 };
            //
            //radiobuttons
            //
            RadioButton itemsAlreadyAdded = new RadioButton() { Left = 20, Width = 140, Top = 55 };
            itemsAlreadyAdded.Text = "Items Already Added";
            itemsAlreadyAdded.Checked = true;
            RadioButton itemsPendingToAdd = new RadioButton() { Left = 170, Width = 140, Top = 55 };
            itemsPendingToAdd.Text = "Items Remaining to Add";
            RadioButton itemsWithoutBarcode = new RadioButton() { Left = 320, Width = 150, Top = 55 };
            itemsWithoutBarcode.Text = "Items having no Barcode";
            //
            addItemsHavingNoBarcode.Enabled = false;
            moveToScan.Enabled = readyToScan ? true : false;
            //
            //events
            addItemsHavingNoBarcode.Click += (sender, e) =>
            {
                selectedOption = BinPromptResult.AddItemsHavingNoBacode;
                prompt.Close();
            };
            //

            deleteBin.Click += (sender, e) =>
            {
                selectedOption = BinPromptResult.DeleteBin;
                prompt.Close();
            };
            moveToScan.Click += (Sender, e) =>
            {
                selectedOption = BinPromptResult.ProceedToScan;
                prompt.Close();
            };
            //
            //Action : will add the items according to item status selected radio button
            //
            #region delegate
            Action<bool, bool> refreshDataGridWithCondition = (isAddedToBin, hasBarcodeEmpty) =>
            {
                _grdItems.Clear();
                _grdItems = null;
                _grdItems = gridItemsData.Copy();
                _grdItems.Columns.Add("isDeleted");
                _grdItems.Columns.Add("Add to Bin");
                var rowCount = _grdItems.Rows.Count;
                try
                {
                    for (int currentRow = 0; currentRow < rowCount; currentRow++)
                    {
                        var row = _grdItems.Rows[currentRow];
                        var addedToBin = row.ItemArray[3];
                        addedToBin = object.ReferenceEquals(addedToBin, null) ? "false" : addedToBin.ToString();
                        addedToBin = String.IsNullOrEmpty(addedToBin.ToString()) ? "false" : addedToBin.ToString();
                        var barCodeNum = row.ItemArray[4];
                        var barCodeNumber = object.ReferenceEquals(barCodeNum, null) ? String.Empty : barCodeNum.ToString().Trim();

                        var barcodeCondition = !(String.IsNullOrEmpty(barCodeNumber) && Convert.ToBoolean(addedToBin) == false);
                        bool condition = isAddedToBin ? !Convert.ToBoolean(addedToBin) :
                            hasBarcodeEmpty ? barcodeCondition : Convert.ToBoolean(addedToBin);

                        _grdItems.Rows[currentRow][4] = barCodeNumber.ToString().Trim();
                        anyBarcode = String.IsNullOrEmpty(barCodeNumber) ? anyBarcode : barCodeNumber;
                        if (condition)
                        {
                            //setting isDeleted value 
                            _grdItems.Rows[currentRow][5] = "true";
                        }
                        else
                        {
                            _grdItems.Rows[currentRow][5] = "false";
                            //setting Add to Bin value
                            _grdItems.Rows[currentRow][6] = "Add to Bin";
                        }
                    }
                    DataTable filteredItems = _grdItems.Clone();
                    foreach (DataRow row in _grdItems.Rows)
                    {
                        //checking whether row has to delete
                        var isDeleted = row[5].ToString();
                        if (!Convert.ToBoolean(isDeleted))
                        {
                            filteredItems.Rows.Add(row.ItemArray);
                        }
                    }

                    grdItems.DataSource = filteredItems;
                    var text = String.Format("This bin assigned to Order : {0} \n Items Added into bin : {1}/{2}",
                    bin.OrderNumber, gridItemsData.Select("IsAddedToBin=True").Count(), bin.TotalNumberofItems);
                    statusLabel.Text = text;
                    moveToScan.Enabled = gridItemsData.Select("IsAddedToBin=True").Count() == bin.TotalNumberofItems ? true : false;
                    count.Text = String.Format("Count : {0}", filteredItems.Rows.Count);
                }
                catch { }
                grdItems.Columns["IsAddedToBin"].Visible = false;
                grdItems.Columns["isDeleted"].Visible = false;
                //changing view of add item to bin columns
                var addToBinColumn = grdItems.Columns["Add to Bin"];
                addToBinColumn.DisplayIndex = 2;
                addToBinColumn.HeaderText = String.Empty;
                addToBinColumn.DefaultCellStyle.ForeColor = Color.Red;
                grdItems.Columns["Add to Bin"].Visible = false;

            };
            #endregion
            #region Bin prompt events
            //
            itemsAlreadyAdded.Click += (Sender, e) =>
            {
                var isAddedToBin = true;
                refreshDataGridWithCondition.Invoke(isAddedToBin, false);
                grdItems.Columns["BarcodeNumber"].Visible = true;
                addItemsHavingNoBarcode.Enabled = false;
            };
            //
            itemsPendingToAdd.Click += (Sender, e) =>
            {
                var isAddedToBin = false;
                refreshDataGridWithCondition.Invoke(isAddedToBin, false);
                grdItems.Columns["BarcodeNumber"].Visible = true;
                addItemsHavingNoBarcode.Enabled = false;
                grdItems.Columns["Add to Bin"].Visible = true;
            };
            //
            itemsWithoutBarcode.Click += (Sender, e) =>
            {
                var hasBarcodeEmpty = true;
                var isAddedToBin = false;
                refreshDataGridWithCondition.Invoke(isAddedToBin, hasBarcodeEmpty);
                grdItems.Columns["BarcodeNumber"].Visible = false;
                if (grdItems.Rows.Count > 1)
                {
                    addItemsHavingNoBarcode.Enabled = true;
                    numOfItemsWithoutBarcode = grdItems.Rows.Count - 1;
                }
            };
            prompt.Shown += (Sender, e) => { refreshDataGridWithCondition.Invoke(true, false); };
            grdItems.CellMouseEnter += (Sender, evnt) =>
            {
                if (evnt.RowIndex >= 0)
                {
                    var row = grdItems.Rows[evnt.RowIndex];
                    if (!object.ReferenceEquals(grdItems.Rows[evnt.RowIndex].Cells[0].Value, null) && !String.IsNullOrEmpty(grdItems.Rows[evnt.RowIndex].Cells["Add to Bin"].Value.ToString().Trim()))
                        grdItems.Rows[evnt.RowIndex].Cells["Add to Bin"].Style.ForeColor = Color.Blue;
                }
            };
            grdItems.CellMouseLeave += (Sender, evnt) =>
            {
                if (evnt.RowIndex >= 0)
                {
                    var row = grdItems.Rows[evnt.RowIndex];
                    if (!object.ReferenceEquals(grdItems.Rows[evnt.RowIndex].Cells[0].Value, null) && !String.IsNullOrEmpty(grdItems.Rows[evnt.RowIndex].Cells["Add to Bin"].Value.ToString().Trim()))
                        grdItems.Rows[evnt.RowIndex].Cells["Add to Bin"].Style.ForeColor = Color.Red;
                }
            };
            grdItems.CellClick += (Sender, e) =>
            {
                if (e.RowIndex >= 0)
                {
                    var selectedCellValue = grdItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    if (selectedCellValue.Equals("Add to Bin"))
                    {
                        var row = grdItems.Rows[e.RowIndex];
                        var barcode = row.Cells["BarcodeNumber"].Value.ToString();
                        var sku = row.Cells["SKU"].Value.ToString();
                        if (bin.NumberOfItemsAlreadyAddedInBin <= bin.TotalNumberofItems)
                            if (new Bins().AddIndividualItemToBin(sku, bin.OrderNumber, barcode))
                            {
                                anyDataChange = true;
                                var currentBinDetails = bin; //new Bins().GetBinDetails(bin.OrderNumber);
                                //gridItemsData = null;
                                for (int currentRow = 0; currentRow < gridItemsData.Rows.Count; currentRow++)
                                {
                                    var currentRowSku = gridItemsData.Rows[currentRow].ItemArray[0].ToString().Trim();
                                    //getting sku of grid data and match it with updated item sku
                                    if (currentRowSku.Equals(sku.Trim()) && !Convert.ToBoolean(gridItemsData.Rows[currentRow].ItemArray[3].ToString()))
                                    {
                                        gridItemsData.Rows[currentRow].SetField("IsAddedToBin", "True");
                                        break;
                                    }
                                }
                                itemsPendingToAdd.PerformClick();
                            }
                    }
                }
            };
            #endregion
            //
            //Adding items to prompt
            //
            //
            prompt.Controls.Add(itemsAlreadyAdded);
            prompt.Controls.Add(itemsPendingToAdd);
            prompt.Controls.Add(itemsWithoutBarcode);
            prompt.Controls.Add(count);
            prompt.Controls.Add(grdItems);
            prompt.Controls.Add(statusLabel);
            prompt.Controls.Add(addItemsHavingNoBarcode);
            prompt.Controls.Add(deleteBin);
            prompt.Controls.Add(moveToScan);
            prompt.FormClosed += (Sender, e) =>
            {
                if (anyDataChange)
                {
                    try
                    {
                        if (!String.IsNullOrEmpty(txtSearch.Text))
                        {
                            GetDataToBindGird();
                            lblerror.Text = String.Empty;
                            BindGrid();
                        }
                        else
                        {
                            LoadBins();
                        }
                    }
                    catch { LoadBins(); }
                }
            };
            prompt.ShowDialog();
            string[] data = { selectedOption.ToString(), numOfItemsWithoutBarcode.ToString(), anyBarcode };
            return data;
        }

        private void ViewUpdateUserStatistics()
        {
            try
            {
                ViewUserStatistics();
                var timer = new System.Timers.Timer();
                timer.Interval = TimeSpan.FromMinutes(2).TotalMilliseconds;
                timer.Elapsed += (sender, e) =>
                {
                    ViewUserStatistics();
                };
                timer.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private async Task ViewUserStatistics()
        {
            var logger =new ODM.Core.Logger();
            await Task.Run(() =>
            {
                CheckForIllegalCrossThreadCalls = false;            
                var stats=logger.GetUserLogs(GlobalVariablesAndMethods.UserDetails.Userid,DateTime.UtcNow);
                var loggedInTime = stats.TotalLoggedInTime;
                lblTotalNumOfProcessedOrders.Text = String.Format("You have Processed {0} Orders",stats.TotalOrderProcessed);
                lblLoggedInTime.Text = String.Format("You have logged In for {0:00}:{1:00}:{2:00}", loggedInTime.Hours,loggedInTime.Minutes,loggedInTime.Seconds);
                lblOrdersPerHour.Text = String.Format("Average {0:0.00} order per Hour", stats.HourlyPerOrderProcessRate);
                lblPerOrderProcessTime.Text = String.Format("Average {0:0.00} min process time per Order", ODM.Core.Helpers.TimerUtils.GetMinutes(Convert.ToDouble(stats.PerOrderAvgProcessTime)));
            });
        }
    }
}
