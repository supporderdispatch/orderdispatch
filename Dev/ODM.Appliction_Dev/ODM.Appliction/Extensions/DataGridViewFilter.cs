﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using ODM.Entities;

namespace ODM.Appliction.Extensions
{
    public static class DataGridViewFilter
    {
        private static List<SavedFilterSetting> SavedFilterSettings { get; set; }
        public static DataTable dataSource { get; set; }
        public static DataTable MainDataSource { get; set; }
        public static bool activateFilters = false;
        public static DataGridView grd { get; set; }
        private static string FilterPic = @"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEcSURBVDhPtZA9ioNAGEBNZytE7KJp1CqHsPMIgjfQFLHwDjZaWAieQrGwFfxFDBoXBdHGm+yXOEtMMLBb7CummHmP+Waw+I+goCzL2+329YG6rtM0Xcx7ALZpmoIgMAzD8/zhcKBpGlaWZY/HI8dxkiSFYbg096Drut1uR1GUYRiO4wRB4Hme7/uwWpZ1Pp8xDJNl+Xq9ogCY51kURTjYJIoiGHgxn48ex/F0OiFlBdxTVRWS1kGSJH3f7/d7JD6wbbtpGmQ8eAZAlmVt2yIXwzRNg+ehsx9eAqAoCpgYbHjSNE1od8V7AAzDQJKkruvLt7yxEcAYBEFcLpf/DHAcV1X1twH8uqIoruvmeY62VmwEADSbNrAdfCSOvwEOXOibM8dhdwAAAABJRU5ErkJggg==";
        private static string FilterPicForSearchCell = @"iVBORw0KGgoAAAANSUhEUgAAAA4AAAAQCAYAAAAmlE46AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAExSURBVDhPnZMxqoNAEIZHC7VKFQkeQos0Cd4gdwg21jZexYtYeAALkRAjqJA0CYJHSKOgwjxn3/J4+xKFlw+m2Jn5d3Z2diWcgA9gwizLQFVVkGWZu98zjiO0bQu2bYN0Pp8xSRKIogjqugZN01hQkiSgw9B6GAZQFAW22y04jgOr1Qrger3ilISbzYaKz+J5HrWEx+MR8zxH2hWbpsHD4cACSxbHMZZlyTZiQuJ+v6NlWW8FZGEY4uVy4dm/hFOfeLvdcL1ev4iCIMCiKHjmNz9CIk1TrKpKEPm+j3QPfxGExOl0Yr2QiPp+PB48IvIyuN1uB4ZhgK7rYJomPJ9PHhF5O3EaNBnNb47lp7LArLDruv9XJJHrujDNFfq+516R2d8xPStWcb/fc4/Ih98K4AsKjjmBempFiQAAAABJRU5ErkJggg==";
        private static Label MatchFound;
        private static Button picBox;

        public static bool CheckIfFilterActivatedFor(this DataGridView grid)
        {
            var result = false;
            if (grd != null && grd.Parent != null && grd.Parent == grid.Parent)
            {
                if (activateFilters)
                {
                    result = true;
                }
            }
            return result;
        }
        public static void DisableFilter(this DataGridView grid)
        {
            if (grd != null && grd.Parent != null && grd.Parent == grid.Parent)
            {
                if (activateFilters)
                {
                    var picB = grid.Parent.Controls.Find(picBox.Name, true).FirstOrDefault() as Button;
                    if (picB != null)
                    {
                        if (activateFilters)
                        {
                            picB.PerformClick();
                            grd.DataSource = dataSource.Copy();
                            // (grd.Parent as Form).Close();
                        }
                    }
                }
            }


        }
        public static void ApplyFilter(this DataGridView grid, DataTable dataSourceOfGrid, Delegate invoker)
        {
            if (grd != null && grd.Parent != null && grd.Parent != grid.Parent)
            {
                var picB = grd.Parent.Controls.Find(picBox.Name, true).FirstOrDefault() as Button;
                var matchCount = grd.Parent.Controls.Find(MatchFound.Name, true).FirstOrDefault() as Label;
                if (picB != null)
                {
                    if (activateFilters)
                    {
                        picB.PerformClick();
                        grd.Parent.Controls.Remove(picB);
                        Action act = () => { };
                        PostOperationEvent = act;
                        grd.Parent.Controls.Remove(matchCount);
                        // (grd.Parent as Form).Close();
                    }
                    else
                    {
                        grd.Parent.Controls.Remove(picB);
                        grd.Parent.Controls.Remove(matchCount);
                    }


                }
            }

            dataSource = new DataTable();
            MatchFound = new Label();
            picBox = new Button();
            picBox.FlatStyle = FlatStyle.Flat;
            SavedFilterSettings = new List<SavedFilterSetting>();
            MainDataSource = new DataTable();
            activateFilters = false;
            grd = new DataGridView();

            picBox.Image = ODM.Core.Helpers.Conversions.Base64StringToImage(FilterPic);
            picBox.Bounds = new Rectangle(new Point(grid.Left + 4, grid.Top - (20)), new Size(19, 18));
            picBox.Name = Guid.NewGuid().ToString();
            picBox.BackgroundImageLayout = ImageLayout.None;
            picBox.Cursor = Cursors.Hand;
            picBox.Click -= picBox_Click;
            picBox.Click += picBox_Click;
            grd = grid;
            grid.Parent.Controls.Add(picBox);
            MatchFound.Bounds = new Rectangle(new Point(grid.Left + 4 + picBox.Width, grid.Top - (20)), new Size(100, 20));
            MatchFound.Text = "";
            MatchFound.Name = Guid.NewGuid().ToString();
            grd.Parent.Controls.Add(MatchFound);
            MainDataSource = dataSourceOfGrid.Copy();
            PostOperationEvent = invoker;
            if (activateFilters)
            {
                picBox.Click -= picBox_Click;
                ActivateFiltersforGrid();
            }

        }
        public static void ApplyFilter(this DataGridView grid, DataTable data)
        {
            DeactivateFiltersforGrid();
            grd.DataSource = data;
            dataSource = data;
            ActivateFiltersforGrid();
        }

        static void picBox_Click(object sender, EventArgs e)
        {
            activateFilters = activateFilters ? false : true;
            if (activateFilters)
            {
                ActivateFiltersforGrid();
                picBox.FlatStyle = FlatStyle.Standard;

            }
            else
            {
                DeactivateFiltersforGrid();
                picBox.FlatStyle = FlatStyle.Flat;
                //grd.PerformLayout();
                grd.DataSource = MainDataSource;
                PostOperationEvent.Method.Invoke(grd.Parent, null);
                MatchFound.Text = string.Empty;
            }
        }

        private static void DeactivateFiltersforGrid()
        {
            if (dataSource.Rows.Count > 0)
            {
                dataSource.Rows.RemoveAt(0);
                grd.DataSource = dataSource;
            }
            grd.Click -= grd_Click;
            grd.CellContentClick -= grd_CellContentClick;
            grd.CellPainting -= grd_CellPainting;
            grd.KeyUp -= grd_KeyUp;
            grd.EditingControlShowing -= grd_EditingControlShowing;
            grd.ClearSelection();
            grd.ReadOnly = true;
            if (Application.OpenForms["AllOpenOrders"] != null && grd.Parent.Name == Application.OpenForms["AllOpenOrders"].Name)
            {
                PostOperationEvent.Method.Invoke(grd.Parent, null);
                MatchFound.Visible = false;
            }

        }

        private static void ActivateFiltersforGrid()
        {
            grd.Click -= grd_Click;
            grd.CellContentClick -= grd_CellContentClick;
            grd.CellPainting -= grd_CellPainting;
            grd.DataError -= grd_DataError;
            grd.KeyUp -= grd_KeyUp;
            grd.EditingControlShowing -= grd_EditingControlShowing;

            dataSource = (DataTable)grd.DataSource;
            grd.ClearSelection();
            grd.Click += grd_Click;
            grd.DataError += grd_DataError;
            grd.CellContentClick += grd_CellContentClick;
            grd.KeyUp += grd_KeyUp;

            if (activateFilters)
            {
                dataSource.Rows.InsertAt(dataSource.NewRow(), 0);
            }
            grd.CellPainting += grd_CellPainting;
            foreach (DataGridViewColumn column in grd.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            if (grd.Rows.Count > 0 && grd.Rows[0].Cells.Count > 0)
            {
                foreach (DataGridViewCell cell in grd.Rows[0].Cells)
                {
                    cell.ReadOnly = false;
                    cell.Style.ForeColor = Color.Black;
                    cell.Style.BackColor = Color.White;

                    if (cell is DataGridViewButtonCell)
                    {

                        cell.Style.ForeColor = Color.White;
                        cell.Style.BackColor = Color.White;
                        ((DataGridViewButtonCell)cell).ReadOnly = true;
                        ((DataGridViewButtonCell)cell).FlatStyle = FlatStyle.System;
                        var borderInputStyle = new DataGridViewAdvancedBorderStyle()
                        {
                            All = DataGridViewAdvancedCellBorderStyle.None
                        };
                        ((DataGridViewButtonCell)cell).AdjustCellBorderStyle(borderInputStyle, borderInputStyle, false, false, false, false);
                    }
                }
            }

            grd.EditingControlShowing += grd_EditingControlShowing;
            SavedFilterSettings = new List<SavedFilterSetting>();
            foreach (DataColumn column in ((DataTable)grd.DataSource).Columns)
            {
                SavedFilterSettings.Add(new SavedFilterSetting()
                {
                    AppliedRecordFilter = RecordFilter.Contains,
                    ColumnName = column.ColumnName
                });
            }

            grd.ReadOnly = false;
            grd.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        static void grd_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            var textCell = (TextBox)(e.Control);
            textCell.KeyUp -= textCell_KeyUp;
            textCell.KeyUp += textCell_KeyUp;

        }

        static void textCell_KeyUp(object sender, KeyEventArgs e)
        {
            grd.EditMode = DataGridViewEditMode.EditOnKeystroke;

        }

        static void grd_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                FilterDataSourceAndBindItWithGrid();
                grd.EditMode = DataGridViewEditMode.EditProgrammatically;
                grd.ReadOnly = true;
            }
        }

        static void grd_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private static void FilterDataSourceAndBindItWithGrid()
        {
            var listOfGridColumnsWithTheirDisplayIndexes = new List<ColumnWithDisplayIndex>();
            foreach (DataGridViewColumn column in grd.Columns)
            {
                listOfGridColumnsWithTheirDisplayIndexes.Add(new ColumnWithDisplayIndex()
                {
                    ColumnName = column.Name,
                    DisplayIndex = column.DisplayIndex
                });
            }
            var columns = new List<string>();
            foreach (DataColumn column in MainDataSource.Columns)
            {
                columns.Add(column.ColumnName);
            }

            var filtersText = new List<CellAndValue>();
            foreach (DataGridViewCell cell in grd.Rows[0].Cells)
            {
                if (cell is DataGridViewTextBoxCell)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(cell.Value ?? string.Empty)))
                    {
                        filtersText.Add(new CellAndValue()
                        {
                            CellColumnName = grd.Columns[cell.ColumnIndex].Name,
                            CellValue = cell.Value.ToString()
                        });
                    }
                }
            }

            var successDataTable = new DataTable();
            successDataTable = MainDataSource.Clone();
            foreach (DataRow row in MainDataSource.Rows)
            {
                if (CheckIfRowSatisfyAppliedFilters(row, columns))
                {
                    successDataTable.ImportRow(row);
                }
            }
            var emptyRow = successDataTable.NewRow();
            successDataTable.Rows.InsertAt(emptyRow, 0);
            dataSource = successDataTable;
            MatchFound.Text = "Matches : " + (dataSource.Rows.Count - 1).ToString();
            grd.DataSource = dataSource;
            DeactivateFiltersforGrid();
            ActivateFiltersforGrid();
            listOfGridColumnsWithTheirDisplayIndexes.ForEach(x =>
            {
                grd.Columns[x.ColumnName].DisplayIndex = x.DisplayIndex;
            });

            foreach (var cellValue in filtersText)
            {
                grd.Rows[0].Cells[cellValue.CellColumnName].Value = cellValue.CellValue;
            }
            grd.ClearSelection();
            grd.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private static bool CheckIfRowSatisfyAppliedFilters(DataRow row, List<string> columns)
        {
            var result = true;
            var perColumnCriteria = SavedFilterSettings.Where(x => columns.Contains(x.ColumnName)).ToList();
            if (perColumnCriteria.Count > 0)
            {
                foreach (var column in perColumnCriteria.Select(x => x.ColumnName).ToList())
                {
                    var filterValue = Convert.ToString(grd.Rows[0].Cells[column].Value ?? string.Empty);
                    if (CellFullFillCriteria(row[column], perColumnCriteria.Where(x => x.ColumnName.Equals(column)).FirstOrDefault().AppliedRecordFilter, filterValue))
                    {
                        result = result && true;
                    }
                    else
                    {
                        result = result && false;
                    }
                }
            }

            return result;
        }

        private static bool CellFullFillCriteria(object cellValue, RecordFilter recordFilter, string compareWith)
        {
            var currentCellValue = Convert.ToString(cellValue.GetType().Equals(typeof(DBNull)) ? string.Empty : cellValue ?? string.Empty).Trim().ToLower();
            compareWith = compareWith.Trim().ToLower();


            if (recordFilter == RecordFilter.NoFilter)
            {
                return true;
            }
            switch (recordFilter)
            {
                case RecordFilter.Contains:
                    {
                        return currentCellValue.Contains(compareWith);
                    }

                case RecordFilter.Equals:
                    {
                        Type possibleTypeofData = GetTypeOfData(currentCellValue);
                        if (possibleTypeofData == typeof(double))
                        {
                            double currentVal;
                            double compWith;
                            if (double.TryParse(currentCellValue, out currentVal) && double.TryParse(compareWith, out compWith))
                            {
                                return currentVal == compWith;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        if (possibleTypeofData == typeof(DateTime))
                        {
                            DateTime currentVal;
                            DateTime compWith;
                            if (DateTime.TryParse(currentCellValue, out currentVal))
                            {
                                if (string.IsNullOrEmpty(compareWith))
                                {
                                    return false;
                                }
                                return DateTime.Parse(currentCellValue).Date == DateTime.Parse(compareWith).Date;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return currentCellValue.Equals(compareWith);
                        }
                    }
                case RecordFilter.DoesNotContain:
                    {
                        return !currentCellValue.Contains(compareWith);
                    }

                case RecordFilter.GreaterThan:
                    {
                        Type possibleTypeofData = GetTypeOfData(currentCellValue);
                        if (possibleTypeofData == typeof(double))
                        {
                            double currentVal;
                            double compWith;
                            if (double.TryParse(currentCellValue, out currentVal) && double.TryParse(compareWith, out compWith))
                            {
                                return currentVal > compWith;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        if (possibleTypeofData == typeof(DateTime))
                        {
                            DateTime currentVal;
                            DateTime compWith;
                            if (DateTime.TryParse(currentCellValue, out currentVal))
                            {
                                return DateTime.Parse(currentCellValue).Date > DateTime.Parse(compareWith).Date;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return currentCellValue.ToString().Length > compareWith.ToString().Length;
                        }
                    }
                case RecordFilter.GreaterThanOrEqualTo:
                    {
                        Type possibleTypeofData = GetTypeOfData(currentCellValue);
                        if (possibleTypeofData == typeof(double))
                        {
                            double currentVal;
                            double compWith;
                            if (double.TryParse(currentCellValue, out currentVal) && double.TryParse(compareWith, out compWith))
                            {
                                return currentVal >= compWith;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        if (possibleTypeofData == typeof(DateTime))
                        {
                            DateTime currentVal;
                            DateTime compWith;
                            if (DateTime.TryParse(currentCellValue, out currentVal))
                            {
                                return DateTime.Parse(currentCellValue).Date >= DateTime.Parse(compareWith).Date;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return currentCellValue.ToString().Length >= compareWith.ToString().Length;
                        }
                    }
                case RecordFilter.LessThan:
                    {
                        Type possibleTypeofData = GetTypeOfData(currentCellValue);
                        if (possibleTypeofData == typeof(double))
                        {
                            double currentVal;
                            double compWith;
                            if (double.TryParse(currentCellValue, out currentVal) && double.TryParse(compareWith, out compWith))
                            {
                                return currentVal < compWith;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        if (possibleTypeofData == typeof(DateTime))
                        {
                            DateTime currentVal;
                            DateTime compWith;
                            if (DateTime.TryParse(currentCellValue, out currentVal))
                            {
                                return DateTime.Parse(currentCellValue).Date < DateTime.Parse(compareWith).Date;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return currentCellValue.ToString().Length < compareWith.ToString().Length;
                        }
                    }
                case RecordFilter.LessThanOrEqualTo:
                    {
                        Type possibleTypeofData = GetTypeOfData(currentCellValue);
                        if (possibleTypeofData == typeof(double))
                        {
                            double currentVal;
                            double compWith;
                            if (double.TryParse(currentCellValue, out currentVal) && double.TryParse(compareWith, out compWith))
                            {
                                return currentVal <= compWith;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        if (possibleTypeofData == typeof(DateTime))
                        {
                            DateTime currentVal;
                            DateTime compWith;
                            if (DateTime.TryParse(currentCellValue, out currentVal))
                            {
                                return DateTime.Parse(currentCellValue).Date <= DateTime.Parse(compareWith).Date;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return currentCellValue.ToString().Length <= compareWith.ToString().Length;
                        }
                    }

                case RecordFilter.NotEqualTo:
                    {
                        Type possibleTypeofData = GetTypeOfData(currentCellValue);
                        if (possibleTypeofData == typeof(double))
                        {
                            double currentVal;
                            double compWith;
                            if (double.TryParse(currentCellValue, out currentVal) && double.TryParse(compareWith, out compWith))
                            {
                                return currentVal != compWith;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        if (possibleTypeofData == typeof(DateTime))
                        {
                            DateTime currentVal;
                            DateTime compWith;
                            if (DateTime.TryParse(currentCellValue, out currentVal))
                            {
                                return DateTime.Parse(currentCellValue).Date != DateTime.Parse(compareWith).Date;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return !currentCellValue.Equals(compareWith);
                        }
                    }
                case RecordFilter.StartsWith:
                    {
                        return currentCellValue.Substring(0, compareWith.Length).Equals(compareWith);
                    }

                case RecordFilter.IsNull:
                    {
                        return cellValue == null;
                    }
                case RecordFilter.IsNotNull:
                    {
                        return cellValue != null;
                    }
                case RecordFilter.IsEmpty:
                    {
                        return string.IsNullOrEmpty(currentCellValue);
                    }
                case RecordFilter.IsNotEmpty:
                    {
                        return !string.IsNullOrEmpty(currentCellValue);
                    }
            }

            return true;
        }

        private static Type GetTypeOfData(string currentCellValue)
        {
            Type t;
            double tempNum;
            DateTime tempDatetime;
            if (double.TryParse(currentCellValue, out tempNum))
            {
                t = typeof(double);
            }
            else if (DateTime.TryParse(currentCellValue, out tempDatetime))
            {
                t = typeof(DateTime);
            }
            else
            {
                t = typeof(string);
            }
            return t;
        }

        static void grd_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == 0)
            {
            }
        }

        static void grd_Click(object sender, EventArgs e)
        {
            grd.ReadOnly = false;
            var grid = sender as DataGridView;
            var position = e as MouseEventArgs;
            var el = grid.HitTest(position.X, position.Y);
            if (el.RowIndex == 0 && el.ColumnIndex >= 0)
            {
                var cell = grid.Rows[0].Cells[el.ColumnIndex];
                var clickXCoordinate = position.X;
                var buttonXCoordinate = el.ColumnX;
                cell.ReadOnly = false;
                ((DataGridView)sender).EditMode = DataGridViewEditMode.EditProgrammatically;
                var coveredpx = clickXCoordinate - buttonXCoordinate;
                if (coveredpx > (cell.Size.Width - cell.Size.Height))
                {
                    ShowAndApplyFilterOptions(position, sender, cell);
                }
                else
                {
                    ((DataGridView)sender).EditMode = DataGridViewEditMode.EditOnEnter;
                }
            }
            if (el.RowIndex != 0)
            {
                ((DataGridView)sender).EditMode = DataGridViewEditMode.EditProgrammatically;
                grd.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            else
            {
                ((DataGridView)sender).EditMode = DataGridViewEditMode.EditOnKeystroke;
                grd.SelectionMode = DataGridViewSelectionMode.CellSelect;
            }

        }

        private static void ShowAndApplyFilterOptions(MouseEventArgs position, object sender, DataGridViewCell cell)
        {

            var grid = ((DataGridView)sender);
            var columnName = grid.Columns[cell.ColumnIndex].Name;

            ContextMenuStrip cm = new ContextMenuStrip();
            GetLateActionFilters().ForEach(x =>
            {
                Bitmap statusImage = new Bitmap(5, 5);
                var text = string.Empty;
                if (SavedFilterSettings.Any(a => a.ColumnName.Equals(columnName) && a.AppliedRecordFilter == GetSelectedFilter(x.ToString()) && a.AppliedRecordFilter != RecordFilter.NoFilter))
                {
                    statusImage = GetStatusImage(Color.DarkGreen);
                }
                else if (!SavedFilterSettings.Any(a => a.ColumnName.Equals(columnName)) && GetSelectedFilter(x.ToString()) == RecordFilter.NoFilter ||
                    SavedFilterSettings.Any(a => a.ColumnName.Equals(columnName) && a.AppliedRecordFilter == GetSelectedFilter(x.ToString()) && a.AppliedRecordFilter == RecordFilter.NoFilter))
                {
                    statusImage = GetStatusImage(Color.OrangeRed);
                }

                cm.Items.Add(new ToolStripMenuItem(x.ToString(), statusImage, (s, e) =>
                {
                    var selectedFilter = GetSelectedFilter(s.ToString());
                    ApplySelectedFilterOnGrid(sender, selectedFilter, position);
                }));
            });
            cm.Items.Add(new ToolStripSeparator());
            GetEarlyActionFilters().ForEach(x =>
            {
                cm.Items.Add(new ToolStripMenuItem(x.ToString(), new Bitmap(1, 1), (s, e) =>
                {
                    var selectedFilter = GetSelectedFilter(s.ToString());
                    ApplyEarlyActionFilterOnGrid(sender, selectedFilter, position);
                }));
            });
            cm.Show(grid, position.X, position.Y);
        }

        private static void ApplyEarlyActionFilterOnGrid(object sender, RecordFilter selectedFilter, MouseEventArgs position)
        {
            var listOfGridColumnsWithTheirDisplayIndexes = new List<ColumnWithDisplayIndex>();
            foreach (DataGridViewColumn column in grd.Columns)
            {
                listOfGridColumnsWithTheirDisplayIndexes.Add(new ColumnWithDisplayIndex()
                {
                    ColumnName = column.Name,
                    DisplayIndex = column.DisplayIndex
                });
            }
            var columns = new List<string>();
            foreach (DataColumn column in MainDataSource.Columns)
            {
                columns.Add(column.ColumnName);
            }



            var successDataTable = new DataTable();
            successDataTable = MainDataSource.Clone();
            foreach (DataRow row in MainDataSource.Rows)
            {
                if (CheckIfRowSatisfyAppliedFilters(row, selectedFilter, grd.Columns[grd.HitTest(position.X, position.Y).ColumnIndex].Name))
                {
                    successDataTable.ImportRow(row);
                }
            }
            var emptyRow = successDataTable.NewRow();
            successDataTable.Rows.InsertAt(emptyRow, 0);
            dataSource = successDataTable;
            grd.DataSource = dataSource;
            DeactivateFiltersforGrid();
            ActivateFiltersforGrid();
            listOfGridColumnsWithTheirDisplayIndexes.ForEach(x =>
            {
                grd.Columns[x.ColumnName].DisplayIndex = x.DisplayIndex;
            });
            MatchFound.Text = "Matches : " + (dataSource.Rows.Count - 1).ToString();
            grd.EditMode = DataGridViewEditMode.EditProgrammatically;

        }

        private static bool CheckIfRowSatisfyAppliedFilters(DataRow row, RecordFilter selectedFilter, string column)
        {
            var result = true;
            var filterValue = Convert.ToString(grd.Rows[0].Cells[column].Value ?? string.Empty);
            if (CellFullFillCriteria(row[column], selectedFilter, filterValue))
            {
                result = result && true;
            }
            else
            {
                result = result && false;
            }
            return result;
        }

        private static Bitmap GetStatusImage(Color color)
        {
            var bmp = new Bitmap(5, 5);
            Image img = (Image)bmp;
            Graphics g = Graphics.FromImage(img);
            Pen pen = new Pen(color, 1);
            g.DrawEllipse(pen, 2, 2, 0.75f, 0.75f);
            return (Bitmap)img;

        }

        private static void ApplySelectedFilterOnGrid(object sender, RecordFilter selectedFilter, MouseEventArgs position)
        {
            var columnName = ((DataGridView)sender).Columns[((DataGridView)sender).HitTest(position.X, position.Y).ColumnIndex].Name;
            if (SavedFilterSettings.Any(x => x.ColumnName.Equals(columnName)))
            {
                var savedFilterSetting = SavedFilterSettings.Where(x => x.ColumnName.Equals(columnName)).FirstOrDefault();
                SavedFilterSettings.Remove(savedFilterSetting);
                savedFilterSetting.AppliedRecordFilter = selectedFilter;
                SavedFilterSettings.Add(savedFilterSetting);
            }
            else
            {
                SavedFilterSettings.Add(new SavedFilterSetting()
                {
                    AppliedRecordFilter = selectedFilter,
                    ColumnName = columnName
                });
            }
        }

        private static RecordFilter GetSelectedFilter(string selectedFilter)
        {
            RecordFilter filter = new RecordFilter();

            switch (selectedFilter)
            {
                case "No Filter":
                    {
                        filter = RecordFilter.NoFilter;
                        break;
                    }

                case "Contains":
                    {
                        filter = RecordFilter.Contains;
                        break;
                    }

                case "Does not contain":
                    {
                        filter = RecordFilter.DoesNotContain;
                        break;
                    }
                case "Starts with":
                    {
                        filter = RecordFilter.StartsWith;
                        break;
                    }
                case "Equals":
                    {
                        filter = RecordFilter.Equals;
                        break;
                    }
                case "Not equal to":
                    {
                        filter = RecordFilter.NotEqualTo;
                        break;
                    }
                case "Greater than":
                    {
                        filter = RecordFilter.GreaterThan;
                        break;
                    }
                case "Less than":
                    {
                        filter = RecordFilter.LessThan;
                        break;
                    }
                case "Greater than or equal to":
                    {
                        filter = RecordFilter.GreaterThanOrEqualTo;
                        break;
                    }
                case "Less than or equal to":
                    {
                        filter = RecordFilter.LessThanOrEqualTo;
                        break;
                    }
                case "Is empty":
                    {
                        filter = RecordFilter.IsEmpty;
                        break;
                    }
                case "Is not empty":
                    {
                        filter = RecordFilter.IsNotEmpty;
                        break;
                    }
                case "Is null":
                    {
                        filter = RecordFilter.IsNull;
                        break;
                    }
                case "Is not null":
                    {
                        filter = RecordFilter.IsNotNull;
                        break;
                    }
            }
            return filter;
        }

        private static List<string> GetLateActionFilters()
        {
            return new List<string>() { "No Filter", "Contains", "Does not contain", "Starts with", "Equals", "Not equal to", "Greater than", "Less than", "Greater than or equal to", "Less than or equal to", };
        }
        private static List<string> GetEarlyActionFilters()
        {
            return new List<string>() { "Is empty", "Is not empty", "Is null", "Is not null" };
        }

        static void grd_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (sender is DataGridView)
            {
                if (e.RowIndex == 0)  // use your own checks here!!
                {
                    e.PaintBackground(e.CellBounds, true);
                    e.PaintContent(e.CellBounds);

                    Graphics g = e.Graphics;
                    Color c = Color.Empty;
                    Brush br = SystemBrushes.WindowText;
                    Brush brBack;
                    Rectangle rDraw;

                    rDraw = e.CellBounds;
                    rDraw.Inflate(-1, -1);

                    {
                        brBack = Brushes.White;
                        g.FillRectangle(brBack, rDraw);  // **
                    }


                    int butSize = e.CellBounds.Height;
                    Rectangle rbut = new Rectangle(e.CellBounds.Right + 5 - butSize,
                                                   e.CellBounds.Top + 3, butSize - 10, butSize - 7);
                    //ComboBoxRenderer.DrawDropDownButton(e.Graphics, rbut,
                    //           System.Windows.Forms.VisualStyles.ComboBoxState.Normal);

                    g.DrawImage(ODM.Core.Helpers.Conversions.Base64StringToImage(FilterPicForSearchCell), rbut);

                    Rectangle textRect = new Rectangle(e.CellBounds.Left,
                        e.CellBounds.Top, e.CellBounds.Width - butSize, butSize);

                    var text = string.Empty;
                    if (e.FormattedValue != null && e.FormattedValue.GetType().Equals(typeof(string)))
                    {
                        text = (string)e.FormattedValue ?? string.Empty;
                    }

                    g.DrawString(text, ((Control)sender).Font, Brushes.Black, textRect);

                    e.Handled = true;
                }
                if (e.ColumnIndex >= 0 && ((DataGridView)sender).Columns[e.ColumnIndex].GetType().Equals(typeof(DataGridViewButtonColumn)) && e.RowIndex == 0)
                {
                    e.PaintBackground(e.CellBounds, true);
                    e.PaintContent(e.CellBounds);

                    Graphics g = e.Graphics;
                    Color c = Color.Empty;
                    string s = "";
                    Brush br = SystemBrushes.WindowText;
                    Brush brBack;
                    Rectangle rDraw;

                    rDraw = e.CellBounds;
                    rDraw.Inflate(-1, -1);

                    {
                        brBack = Brushes.White;
                        g.FillRectangle(brBack, rDraw);  // **
                    }
                    e.Handled = true;
                }
            }


        }


        public static Delegate PostOperationEvent { get; set; }
    }
}
