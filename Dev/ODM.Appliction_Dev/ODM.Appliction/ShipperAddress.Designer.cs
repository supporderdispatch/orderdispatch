﻿namespace ODM.Appliction
{
    partial class ShipperAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShipperAddress));
            this.lblPostCodeRequired = new System.Windows.Forms.Label();
            this.lblCityTownRequired = new System.Windows.Forms.Label();
            this.lblAddressLine2Required = new System.Windows.Forms.Label();
            this.lblAddressLine1Required = new System.Windows.Forms.Label();
            this.lblNameRequired = new System.Windows.Forms.Label();
            this.lblCompanyRequired = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtPostCode = new System.Windows.Forms.TextBox();
            this.lblPostZip = new System.Windows.Forms.Label();
            this.txtCityTown = new System.Windows.Forms.TextBox();
            this.lblCityTown = new System.Windows.Forms.Label();
            this.txtAddressLine2 = new System.Windows.Forms.TextBox();
            this.lblAddressLine2 = new System.Windows.Forms.Label();
            this.txtAddressLine1 = new System.Windows.Forms.TextBox();
            this.lblAddressLine1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblStateRegionRequired = new System.Windows.Forms.Label();
            this.txtStateRegion = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.lblPhoneRequired = new System.Windows.Forms.Label();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblCountryRequired = new System.Windows.Forms.Label();
            this.ddlCountry = new System.Windows.Forms.ComboBox();
            this.lblCountryID = new System.Windows.Forms.Label();
            this.lblEmailRequired = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPostCodeRequired
            // 
            this.lblPostCodeRequired.AutoSize = true;
            this.lblPostCodeRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPostCodeRequired.Location = new System.Drawing.Point(691, 77);
            this.lblPostCodeRequired.Name = "lblPostCodeRequired";
            this.lblPostCodeRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPostCodeRequired.TabIndex = 98;
            this.lblPostCodeRequired.Text = "Required";
            this.lblPostCodeRequired.Visible = false;
            // 
            // lblCityTownRequired
            // 
            this.lblCityTownRequired.AutoSize = true;
            this.lblCityTownRequired.ForeColor = System.Drawing.Color.Red;
            this.lblCityTownRequired.Location = new System.Drawing.Point(691, 16);
            this.lblCityTownRequired.Name = "lblCityTownRequired";
            this.lblCityTownRequired.Size = new System.Drawing.Size(50, 13);
            this.lblCityTownRequired.TabIndex = 97;
            this.lblCityTownRequired.Text = "Required";
            this.lblCityTownRequired.Visible = false;
            // 
            // lblAddressLine2Required
            // 
            this.lblAddressLine2Required.AutoSize = true;
            this.lblAddressLine2Required.ForeColor = System.Drawing.Color.Red;
            this.lblAddressLine2Required.Location = new System.Drawing.Point(292, 116);
            this.lblAddressLine2Required.Name = "lblAddressLine2Required";
            this.lblAddressLine2Required.Size = new System.Drawing.Size(50, 13);
            this.lblAddressLine2Required.TabIndex = 95;
            this.lblAddressLine2Required.Text = "Required";
            this.lblAddressLine2Required.Visible = false;
            // 
            // lblAddressLine1Required
            // 
            this.lblAddressLine1Required.AutoSize = true;
            this.lblAddressLine1Required.ForeColor = System.Drawing.Color.Red;
            this.lblAddressLine1Required.Location = new System.Drawing.Point(292, 80);
            this.lblAddressLine1Required.Name = "lblAddressLine1Required";
            this.lblAddressLine1Required.Size = new System.Drawing.Size(50, 13);
            this.lblAddressLine1Required.TabIndex = 94;
            this.lblAddressLine1Required.Text = "Required";
            this.lblAddressLine1Required.Visible = false;
            // 
            // lblNameRequired
            // 
            this.lblNameRequired.AutoSize = true;
            this.lblNameRequired.ForeColor = System.Drawing.Color.Red;
            this.lblNameRequired.Location = new System.Drawing.Point(292, 48);
            this.lblNameRequired.Name = "lblNameRequired";
            this.lblNameRequired.Size = new System.Drawing.Size(50, 13);
            this.lblNameRequired.TabIndex = 93;
            this.lblNameRequired.Text = "Required";
            this.lblNameRequired.Visible = false;
            // 
            // lblCompanyRequired
            // 
            this.lblCompanyRequired.AutoSize = true;
            this.lblCompanyRequired.ForeColor = System.Drawing.Color.Red;
            this.lblCompanyRequired.Location = new System.Drawing.Point(292, 15);
            this.lblCompanyRequired.Name = "lblCompanyRequired";
            this.lblCompanyRequired.Size = new System.Drawing.Size(50, 13);
            this.lblCompanyRequired.TabIndex = 92;
            this.lblCompanyRequired.Text = "Required";
            this.lblCompanyRequired.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(404, 196);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(107, 29);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(276, 197);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 29);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save && Close";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtPostCode
            // 
            this.txtPostCode.Location = new System.Drawing.Point(508, 74);
            this.txtPostCode.Name = "txtPostCode";
            this.txtPostCode.Size = new System.Drawing.Size(168, 20);
            this.txtPostCode.TabIndex = 7;
            // 
            // lblPostZip
            // 
            this.lblPostZip.AutoSize = true;
            this.lblPostZip.Location = new System.Drawing.Point(416, 77);
            this.lblPostZip.Name = "lblPostZip";
            this.lblPostZip.Size = new System.Drawing.Size(76, 13);
            this.lblPostZip.TabIndex = 86;
            this.lblPostZip.Text = "Post/Zip Code";
            // 
            // txtCityTown
            // 
            this.txtCityTown.Location = new System.Drawing.Point(508, 13);
            this.txtCityTown.Name = "txtCityTown";
            this.txtCityTown.Size = new System.Drawing.Size(168, 20);
            this.txtCityTown.TabIndex = 5;
            // 
            // lblCityTown
            // 
            this.lblCityTown.AutoSize = true;
            this.lblCityTown.Location = new System.Drawing.Point(416, 16);
            this.lblCityTown.Name = "lblCityTown";
            this.lblCityTown.Size = new System.Drawing.Size(54, 13);
            this.lblCityTown.TabIndex = 84;
            this.lblCityTown.Text = "City Town";
            // 
            // txtAddressLine2
            // 
            this.txtAddressLine2.Location = new System.Drawing.Point(118, 116);
            this.txtAddressLine2.Name = "txtAddressLine2";
            this.txtAddressLine2.Size = new System.Drawing.Size(168, 20);
            this.txtAddressLine2.TabIndex = 4;
            // 
            // lblAddressLine2
            // 
            this.lblAddressLine2.AutoSize = true;
            this.lblAddressLine2.Location = new System.Drawing.Point(26, 116);
            this.lblAddressLine2.Name = "lblAddressLine2";
            this.lblAddressLine2.Size = new System.Drawing.Size(71, 13);
            this.lblAddressLine2.TabIndex = 80;
            this.lblAddressLine2.Text = "AddressLine2";
            // 
            // txtAddressLine1
            // 
            this.txtAddressLine1.Location = new System.Drawing.Point(118, 80);
            this.txtAddressLine1.Name = "txtAddressLine1";
            this.txtAddressLine1.Size = new System.Drawing.Size(168, 20);
            this.txtAddressLine1.TabIndex = 3;
            // 
            // lblAddressLine1
            // 
            this.lblAddressLine1.AutoSize = true;
            this.lblAddressLine1.Location = new System.Drawing.Point(26, 80);
            this.lblAddressLine1.Name = "lblAddressLine1";
            this.lblAddressLine1.Size = new System.Drawing.Size(71, 13);
            this.lblAddressLine1.TabIndex = 78;
            this.lblAddressLine1.Text = "AddressLine1";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(118, 44);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(168, 20);
            this.txtName.TabIndex = 2;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(26, 44);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 76;
            this.lblName.Text = "Name";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(118, 11);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(168, 20);
            this.txtCompany.TabIndex = 1;
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(26, 11);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 74;
            this.lblCompany.Text = "Company";
            // 
            // lblStateRegionRequired
            // 
            this.lblStateRegionRequired.AutoSize = true;
            this.lblStateRegionRequired.ForeColor = System.Drawing.Color.Red;
            this.lblStateRegionRequired.Location = new System.Drawing.Point(691, 44);
            this.lblStateRegionRequired.Name = "lblStateRegionRequired";
            this.lblStateRegionRequired.Size = new System.Drawing.Size(50, 13);
            this.lblStateRegionRequired.TabIndex = 102;
            this.lblStateRegionRequired.Text = "Required";
            this.lblStateRegionRequired.Visible = false;
            // 
            // txtStateRegion
            // 
            this.txtStateRegion.Location = new System.Drawing.Point(508, 41);
            this.txtStateRegion.Name = "txtStateRegion";
            this.txtStateRegion.Size = new System.Drawing.Size(168, 20);
            this.txtStateRegion.TabIndex = 6;
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(416, 44);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(71, 13);
            this.lblState.TabIndex = 100;
            this.lblState.Text = "State/Region";
            // 
            // lblPhoneRequired
            // 
            this.lblPhoneRequired.AutoSize = true;
            this.lblPhoneRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPhoneRequired.Location = new System.Drawing.Point(691, 112);
            this.lblPhoneRequired.Name = "lblPhoneRequired";
            this.lblPhoneRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPhoneRequired.TabIndex = 105;
            this.lblPhoneRequired.Text = "Required";
            this.lblPhoneRequired.Visible = false;
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(508, 109);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(168, 20);
            this.txtTelephone.TabIndex = 8;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(416, 112);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(58, 13);
            this.lblPhone.TabIndex = 103;
            this.lblPhone.Text = "Telephone";
            // 
            // lblCountryRequired
            // 
            this.lblCountryRequired.AutoSize = true;
            this.lblCountryRequired.ForeColor = System.Drawing.Color.Red;
            this.lblCountryRequired.Location = new System.Drawing.Point(691, 149);
            this.lblCountryRequired.Name = "lblCountryRequired";
            this.lblCountryRequired.Size = new System.Drawing.Size(50, 13);
            this.lblCountryRequired.TabIndex = 108;
            this.lblCountryRequired.Text = "Required";
            this.lblCountryRequired.Visible = false;
            // 
            // ddlCountry
            // 
            this.ddlCountry.FormattingEnabled = true;
            this.ddlCountry.Location = new System.Drawing.Point(508, 146);
            this.ddlCountry.Name = "ddlCountry";
            this.ddlCountry.Size = new System.Drawing.Size(168, 21);
            this.ddlCountry.TabIndex = 9;
            // 
            // lblCountryID
            // 
            this.lblCountryID.AutoSize = true;
            this.lblCountryID.Location = new System.Drawing.Point(416, 149);
            this.lblCountryID.Name = "lblCountryID";
            this.lblCountryID.Size = new System.Drawing.Size(74, 13);
            this.lblCountryID.TabIndex = 106;
            this.lblCountryID.Text = "Country Name";
            // 
            // lblEmailRequired
            // 
            this.lblEmailRequired.AutoSize = true;
            this.lblEmailRequired.ForeColor = System.Drawing.Color.Red;
            this.lblEmailRequired.Location = new System.Drawing.Point(292, 146);
            this.lblEmailRequired.Name = "lblEmailRequired";
            this.lblEmailRequired.Size = new System.Drawing.Size(50, 13);
            this.lblEmailRequired.TabIndex = 111;
            this.lblEmailRequired.Text = "Required";
            this.lblEmailRequired.Visible = false;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(118, 146);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(168, 20);
            this.txtEmail.TabIndex = 109;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(26, 146);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(36, 13);
            this.lblEmail.TabIndex = 110;
            this.lblEmail.Text = "E-Mail";
            // 
            // ShipperAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 256);
            this.Controls.Add(this.lblEmailRequired);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblCountryRequired);
            this.Controls.Add(this.ddlCountry);
            this.Controls.Add(this.lblCountryID);
            this.Controls.Add(this.lblPhoneRequired);
            this.Controls.Add(this.txtTelephone);
            this.Controls.Add(this.lblPhone);
            this.Controls.Add(this.lblStateRegionRequired);
            this.Controls.Add(this.txtStateRegion);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.lblPostCodeRequired);
            this.Controls.Add(this.lblCityTownRequired);
            this.Controls.Add(this.lblAddressLine2Required);
            this.Controls.Add(this.lblAddressLine1Required);
            this.Controls.Add(this.lblNameRequired);
            this.Controls.Add(this.lblCompanyRequired);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtPostCode);
            this.Controls.Add(this.lblPostZip);
            this.Controls.Add(this.txtCityTown);
            this.Controls.Add(this.lblCityTown);
            this.Controls.Add(this.txtAddressLine2);
            this.Controls.Add(this.lblAddressLine2);
            this.Controls.Add(this.txtAddressLine1);
            this.Controls.Add(this.lblAddressLine1);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.lblCompany);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ShipperAddress";
            this.Text = "ShipperAddress";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPostCodeRequired;
        private System.Windows.Forms.Label lblCityTownRequired;
        private System.Windows.Forms.Label lblAddressLine2Required;
        private System.Windows.Forms.Label lblAddressLine1Required;
        private System.Windows.Forms.Label lblNameRequired;
        private System.Windows.Forms.Label lblCompanyRequired;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtPostCode;
        private System.Windows.Forms.Label lblPostZip;
        private System.Windows.Forms.TextBox txtCityTown;
        private System.Windows.Forms.Label lblCityTown;
        private System.Windows.Forms.TextBox txtAddressLine2;
        private System.Windows.Forms.Label lblAddressLine2;
        private System.Windows.Forms.TextBox txtAddressLine1;
        private System.Windows.Forms.Label lblAddressLine1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.Label lblStateRegionRequired;
        private System.Windows.Forms.TextBox txtStateRegion;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label lblPhoneRequired;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblCountryRequired;
        private System.Windows.Forms.ComboBox ddlCountry;
        private System.Windows.Forms.Label lblCountryID;
        private System.Windows.Forms.Label lblEmailRequired;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
    }
}