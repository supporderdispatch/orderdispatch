﻿namespace ODM.Appliction
{
    partial class EmailMessageTemplateSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailMessageTemplateSettings));
            this.chkOrderImage = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnPreviewDummy = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtTemplate = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkInvoice = new System.Windows.Forms.CheckBox();
            this.chkHTML = new System.Windows.Forms.CheckBox();
            this.chkPrivew = new System.Windows.Forms.CheckBox();
            this.chkIsEnabled = new System.Windows.Forms.CheckBox();
            this.ddlEmailAccount = new System.Windows.Forms.ComboBox();
            this.lblAccount = new System.Windows.Forms.Label();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.lblSubject = new System.Windows.Forms.Label();
            this.btnDeleteCondition = new System.Windows.Forms.Button();
            this.btnChangeCondition = new System.Windows.Forms.Button();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.lblCondition = new System.Windows.Forms.Label();
            this.ddlCondition = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkOrderImage
            // 
            this.chkOrderImage.AutoSize = true;
            this.chkOrderImage.Location = new System.Drawing.Point(346, 132);
            this.chkOrderImage.Name = "chkOrderImage";
            this.chkOrderImage.Size = new System.Drawing.Size(135, 17);
            this.chkOrderImage.TabIndex = 31;
            this.chkOrderImage.Text = "Attach Package Image";
            this.chkOrderImage.UseVisualStyleBackColor = true;
            this.chkOrderImage.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(715, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPreviewDummy
            // 
            this.btnPreviewDummy.Location = new System.Drawing.Point(608, 3);
            this.btnPreviewDummy.Name = "btnPreviewDummy";
            this.btnPreviewDummy.Size = new System.Drawing.Size(102, 23);
            this.btnPreviewDummy.TabIndex = 3;
            this.btnPreviewDummy.Text = "Preview Dummy";
            this.btnPreviewDummy.UseVisualStyleBackColor = true;
            this.btnPreviewDummy.Click += new System.EventHandler(this.btnPreviewDummy_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(527, 3);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 2;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(446, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(7, 4);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(112, 13);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Email Template Config";
            // 
            // txtTemplate
            // 
            this.txtTemplate.Location = new System.Drawing.Point(1, 174);
            this.txtTemplate.Name = "txtTemplate";
            this.txtTemplate.Size = new System.Drawing.Size(829, 381);
            this.txtTemplate.TabIndex = 30;
            this.txtTemplate.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnPreviewDummy);
            this.panel1.Controls.Add(this.btnPreview);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.lblTitle);
            this.panel1.Location = new System.Drawing.Point(10, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(806, 29);
            this.panel1.TabIndex = 29;
            // 
            // chkInvoice
            // 
            this.chkInvoice.AutoSize = true;
            this.chkInvoice.Location = new System.Drawing.Point(189, 132);
            this.chkInvoice.Name = "chkInvoice";
            this.chkInvoice.Size = new System.Drawing.Size(119, 17);
            this.chkInvoice.TabIndex = 28;
            this.chkInvoice.Text = "Attach PDF Invoice";
            this.chkInvoice.UseVisualStyleBackColor = true;
            // 
            // chkHTML
            // 
            this.chkHTML.AutoSize = true;
            this.chkHTML.Location = new System.Drawing.Point(62, 132);
            this.chkHTML.Name = "chkHTML";
            this.chkHTML.Size = new System.Drawing.Size(98, 17);
            this.chkHTML.TabIndex = 27;
            this.chkHTML.Text = "Send as HTML";
            this.chkHTML.UseVisualStyleBackColor = true;
            // 
            // chkPrivew
            // 
            this.chkPrivew.AutoSize = true;
            this.chkPrivew.Location = new System.Drawing.Point(189, 100);
            this.chkPrivew.Name = "chkPrivew";
            this.chkPrivew.Size = new System.Drawing.Size(206, 17);
            this.chkPrivew.TabIndex = 26;
            this.chkPrivew.Text = "Preview Before Generating Email Shot";
            this.chkPrivew.UseVisualStyleBackColor = true;
            // 
            // chkIsEnabled
            // 
            this.chkIsEnabled.AutoSize = true;
            this.chkIsEnabled.Location = new System.Drawing.Point(62, 100);
            this.chkIsEnabled.Name = "chkIsEnabled";
            this.chkIsEnabled.Size = new System.Drawing.Size(76, 17);
            this.chkIsEnabled.TabIndex = 25;
            this.chkIsEnabled.Text = "Is Enabled";
            this.chkIsEnabled.UseVisualStyleBackColor = true;
            // 
            // ddlEmailAccount
            // 
            this.ddlEmailAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEmailAccount.FormattingEnabled = true;
            this.ddlEmailAccount.Location = new System.Drawing.Point(506, 77);
            this.ddlEmailAccount.Name = "ddlEmailAccount";
            this.ddlEmailAccount.Size = new System.Drawing.Size(310, 21);
            this.ddlEmailAccount.TabIndex = 24;
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Location = new System.Drawing.Point(425, 79);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(75, 13);
            this.lblAccount.TabIndex = 23;
            this.lblAccount.Text = "Email Account";
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(62, 74);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(357, 20);
            this.txtSubject.TabIndex = 22;
            this.txtSubject.Enter += new System.EventHandler(this.txtSubject_Enter);
            this.txtSubject.Leave += new System.EventHandler(this.txtSubject_Leave);
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(9, 76);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(43, 13);
            this.lblSubject.TabIndex = 21;
            this.lblSubject.Text = "Subject";
            // 
            // btnDeleteCondition
            // 
            this.btnDeleteCondition.Location = new System.Drawing.Point(624, 42);
            this.btnDeleteCondition.Name = "btnDeleteCondition";
            this.btnDeleteCondition.Size = new System.Drawing.Size(116, 23);
            this.btnDeleteCondition.TabIndex = 20;
            this.btnDeleteCondition.Text = "Delete Condition";
            this.btnDeleteCondition.UseVisualStyleBackColor = true;
            this.btnDeleteCondition.Click += new System.EventHandler(this.btnDeleteCondition_Click);
            // 
            // btnChangeCondition
            // 
            this.btnChangeCondition.Location = new System.Drawing.Point(506, 42);
            this.btnChangeCondition.Name = "btnChangeCondition";
            this.btnChangeCondition.Size = new System.Drawing.Size(113, 23);
            this.btnChangeCondition.TabIndex = 19;
            this.btnChangeCondition.Text = "Change Condition";
            this.btnChangeCondition.UseVisualStyleBackColor = true;
            this.btnChangeCondition.Click += new System.EventHandler(this.btnChangeCondition_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Location = new System.Drawing.Point(425, 42);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnAddNew.TabIndex = 18;
            this.btnAddNew.Text = "Add New";
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // lblCondition
            // 
            this.lblCondition.AutoSize = true;
            this.lblCondition.Location = new System.Drawing.Point(7, 46);
            this.lblCondition.Name = "lblCondition";
            this.lblCondition.Size = new System.Drawing.Size(51, 13);
            this.lblCondition.TabIndex = 17;
            this.lblCondition.Text = "Condition";
            // 
            // ddlCondition
            // 
            this.ddlCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCondition.FormattingEnabled = true;
            this.ddlCondition.Location = new System.Drawing.Point(62, 43);
            this.ddlCondition.Name = "ddlCondition";
            this.ddlCondition.Size = new System.Drawing.Size(357, 21);
            this.ddlCondition.TabIndex = 16;
            this.ddlCondition.SelectedIndexChanged += new System.EventHandler(this.ddlCondition_SelectedIndexChanged);
            // 
            // EmailMessageTemplateSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 554);
            this.Controls.Add(this.chkOrderImage);
            this.Controls.Add(this.txtTemplate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chkInvoice);
            this.Controls.Add(this.chkHTML);
            this.Controls.Add(this.chkPrivew);
            this.Controls.Add(this.chkIsEnabled);
            this.Controls.Add(this.ddlEmailAccount);
            this.Controls.Add(this.lblAccount);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.btnDeleteCondition);
            this.Controls.Add(this.btnChangeCondition);
            this.Controls.Add(this.btnAddNew);
            this.Controls.Add(this.lblCondition);
            this.Controls.Add(this.ddlCondition);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmailMessageTemplateSettings";
            this.Text = "EmailMessageTemplateSettings";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkOrderImage;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnPreviewDummy;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.RichTextBox txtTemplate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkInvoice;
        private System.Windows.Forms.CheckBox chkHTML;
        private System.Windows.Forms.CheckBox chkPrivew;
        private System.Windows.Forms.CheckBox chkIsEnabled;
        private System.Windows.Forms.ComboBox ddlEmailAccount;
        private System.Windows.Forms.Label lblAccount;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Button btnDeleteCondition;
        private System.Windows.Forms.Button btnChangeCondition;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.Label lblCondition;
        private System.Windows.Forms.ComboBox ddlCondition;
    }
}