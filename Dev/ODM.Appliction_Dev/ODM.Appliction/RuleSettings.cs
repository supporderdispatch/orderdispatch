﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class RuleSettings : Form
    {
        Guid RuleID = new Guid();
        string strMode = "";
        //int index;
        public RuleSettings()
        {

            InitializeComponent();
            this.strMode = "INSERT";
            GetFolderDetails();
            GetRuleConditions();
        }
        public RuleSettings(string strMode, string strRuleName, string strRuleConditionText, string FolderID, Guid ID, int Position, bool IsActive, string strRuleConditionID)
        {
            InitializeComponent();
            this.strMode = strMode.ToUpper();
            PopulateControlsWithSelectedRule(strRuleName, strRuleConditionText, FolderID, ID, Position, IsActive, strRuleConditionID);
        }
        private void PopulateControlsWithSelectedRule(string strRuleName, string strRuleConditionText, string FolderID, Guid ID, int Position, bool IsActive, string strRuleConditionID)
        {
            GetFolderDetails();
            GetRuleConditions();
            txtRuleName.Text = strRuleName;
            txtRuleCondition.Text = strRuleConditionText;
            txtPosition.Text = Position.ToString();
            ddlFolders.SelectedValue = FolderID;
            ddlRuleConditions.SelectedValue = strRuleConditionID;
            chkIsActive.Checked = IsActive;
            RuleID = ID;
        }

        private void GetFolderDetails()
        {
            MessageFolders objMessageFolders = new MessageFolders("GET");
            DataTable objDt = new DataTable();
            objDt = objMessageFolders.GetUpdateMessageFolders("GET", new Guid(), "00000000-0000-0000-0000-000000000000");
            ddlFolders.DataSource = objDt;
            ddlFolders.DisplayMember = "FolderName";
            ddlFolders.ValueMember = "ID";
            ddlFolders.SelectedIndex = 0;
        }

        private void GetRuleConditions()
        {
            ddlRuleConditions.DataSource = GlobalVariablesAndMethods.GetAddUpdateMessageRuleCondition("GET", "00000000-0000-0000-0000-000000000000", txtRuleCondition.Text.ToString());
            ddlRuleConditions.DisplayMember = "ConditionText";
            ddlRuleConditions.ValueMember = "ID";

        }
        private void AddNewRule()
        {
            string strResult = GlobalVariablesAndMethods.GetAddUpdateDeleteRules("UPDATE", Guid.NewGuid(), new Guid(ddlFolders.SelectedValue.ToString()), txtRuleName.Text.ToString(), txtRuleCondition.Text.ToString(), Convert.ToInt16(txtPosition.Text.ToString()), chkIsActive.Checked, ddlRuleConditions.SelectedValue.ToString());
            if (strResult == "1")
            {
                MessageBox.Show("Rule Added Successfully!", "Add New Rule", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void UpdateRule(Guid ID)
        {
            string strResult = GlobalVariablesAndMethods.GetAddUpdateDeleteRules("UPDATE", ID, new Guid(ddlFolders.SelectedValue.ToString()), txtRuleName.Text, txtRuleCondition.Text, Convert.ToInt16(txtPosition.Text), chkIsActive.Checked, ddlRuleConditions.SelectedValue.ToString());
            if (strResult == "2")
            {
                MessageBox.Show("Rule Updated Successfully!", "Update Rule", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (strMode.ToUpper() == "UPDATE")
            {
                UpdateRule(RuleID);
            }
            else if (strMode.ToUpper() == "INSERT")
            {
                AddNewRule();
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
