﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class EbayUsersList : Form
    {
        public string strEbayUserID = string.Empty;
        public EbayUsersList()
        {
            InitializeComponent();
            BindGridForEbayUsers();
        }


        //public void PopulateGrid(DataTable objDt)
        //{



        //}
        public void BindGridForEbayUsers()
        {
            grdEbayUsersList.EditMode = DataGridViewEditMode.EditProgrammatically;

            DataTable objDt = new DataTable();
            objDt = GetEbayUsers("GET", new Guid(), new Guid(), "", "");
            if (objDt.Rows.Count > 0)
            {
                grdEbayUsersList.DataSource = objDt;
            }
            else
            {
                lblNoRecords.Visible = true;
            }

        }

        #region "Get Ebay User Details"
        public DataTable GetEbayUsers(string strMode, Guid ID, Guid AccountID, string UserName, string EbayAuthToken)
        {
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objParam[1] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = ID;

                objParam[2] = new SqlParameter("@AccountID", SqlDbType.UniqueIdentifier);
                objParam[2].Value = AccountID;

                objParam[3] = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
                objParam[3].Value = UserName;

                objParam[4] = new SqlParameter("@EbayAuthToken", SqlDbType.VarChar, EbayAuthToken.Length);
                objParam[4].Value = EbayAuthToken;

                objParam[5] = new SqlParameter("@Result", SqlDbType.VarChar, 50);
                objParam[5].Direction = ParameterDirection.Output;

                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetEbayTokenWithUserName", objParam);
                }

                string strResult = objParam[5].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                if (strResult == "0")
                {
                    if (objDs.Tables.Count > 0)
                    {
                        if (objDs.Tables[0].Rows.Count > 0)
                        {
                            objDt = objDs.Tables[0];

                        }
                    }

                }
                return objDt;

            }
            catch (Exception ex)
            {
                throw;
            }
            finally { conn.Close(); }
        }
        #endregion


        private void grdEbayUsersList_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int intSelectedRowIndex = grdEbayUsersList.HitTest(e.X, e.Y).RowIndex;
                strEbayUserID = grdEbayUsersList.Rows[intSelectedRowIndex].Cells["ID"].Value.ToString();
                ContextMenu RightClickMenu = new ContextMenu();
                RightClickMenu.MenuItems.Add(new MenuItem("Delete", DeleteEbayUsers_Click));
                RightClickMenu.Show(grdEbayUsersList, new Point(e.X, e.Y));

            }
        }
        public void DeleteEbayUsers_Click(object sender, EventArgs e)
        {
            DialogResult objResult = MessageBox.Show("Are you sure you want to delete this user ?", "Delete Ebay User", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (objResult == DialogResult.Yes)
            {
                string strResult = GlobalVariablesAndMethods.AddUpdateDeleteEbayUsers("Delete", new Guid(strEbayUserID), new Guid(), string.Empty, string.Empty);
                if (strResult == "3")
                {
                    MessageBox.Show("Ebay User Deleted!");
                }

            }

        }

    }
}
