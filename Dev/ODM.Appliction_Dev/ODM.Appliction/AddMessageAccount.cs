﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class AddMessageAccount : Form
    {
        string strMode="";
        Guid ID = new Guid();
        public AddMessageAccount(string strMode,Guid ID)
        {
            InitializeComponent();
            this.strMode = strMode;
            this.ID = ID;
        }
        public AddMessageAccount(string strMode, Guid ID,string strAccountName,string strUsername,string strPassword,string strImapHostName,bool IsEbay,string strSmtpHostName,bool IsEbaySandbox)
        {
            InitializeComponent();
            this.strMode = strMode;
            this.ID = ID;
            txtAccountName.Text = strAccountName;
            txtUserName.Text = strUsername;
            txtPassword.Text = strPassword;
            txtImapHostName.Text = strImapHostName;
           // txtPort.Text = Port.ToString();
            //chkIsSSL.Checked = IsSSL;
            chkIsEbay.Checked = IsEbay;
            chkIsEbaySandbox.Checked = IsEbaySandbox;
            txtSmtpHostName.Text = strSmtpHostName;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(strMode.ToUpper()=="INSERT"){
                ID=Guid.NewGuid();
            }
            AddUpdateDeleteMessageAccounts(ID, strMode.ToUpper());
            
                this.Close();
        }
        public void AddUpdateDeleteMessageAccounts(Guid ID, string strMode)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                  SqlParameter[] objParam = new SqlParameter[10];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode;

                objParam[1] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = ID;

                objParam[2] = new SqlParameter("@AccountName", SqlDbType.VarChar, 50);
                objParam[2].Value = txtAccountName.Text.ToString();

                objParam[3] = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
                objParam[3].Value = txtUserName.Text.ToString();

                objParam[4] = new SqlParameter("@Password", SqlDbType.VarChar, 50);
                objParam[4].Value = txtPassword.Text.ToString();

                objParam[5] = new SqlParameter("@HostName", SqlDbType.VarChar, 50);
                objParam[5].Value = txtImapHostName.Text.ToString();

                //objParam[6] = new SqlParameter("@Port", SqlDbType.Int);
                //objParam[6].Value = Convert.ToInt16(txtPort.Text.ToString());

                //objParam[7] = new SqlParameter("@SSL", SqlDbType.Bit);
                //objParam[7].Value = chkIsSSL.Checked;

                objParam[6] = new SqlParameter("@SmtpHostName", SqlDbType.VarChar, 50);
                objParam[6].Value = txtSmtpHostName.Text.ToString();

                objParam[7] = new SqlParameter("@IsEbay", SqlDbType.Bit);
                objParam[7].Value = chkIsEbay.Checked;

                objParam[8] = new SqlParameter("@IsEbaySandbox", SqlDbType.Bit);
                objParam[8].Value = chkIsEbaySandbox.Checked;


                objParam[9] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[9].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "AddUpdateDeleteMessageAccounts", objParam);

                string strResult = objParam[9].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                else if (strResult == "0") {
                    MessageBox.Show("Account Added Successfully!", "Add New Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if(strResult=="1")
                {
                    MessageBox.Show("Account Updated Successfully!", "Update Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if(strResult=="2")
                {
                    MessageBox.Show("Account Deleted Successfully!", "Delete Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
            }
            catch (Exception)
            {
                throw;
            }
            finally { conn.Close(); }
        }

        private void chkIsEbay_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsEbay.Checked) {
                lblIsEbaySandbox.Visible = true;
                chkIsEbaySandbox.Visible = true;
            }
            else
            {
                lblIsEbaySandbox.Visible = false;
                chkIsEbaySandbox.Visible = false;
            }
        }
    }
}
