﻿namespace ODM.Appliction
{
    partial class RuleList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RuleList));
            this.btnAddRuleConditon = new System.Windows.Forms.Button();
            this.pnlRuleList = new System.Windows.Forms.Panel();
            this.lblNoRuleFound = new System.Windows.Forms.Label();
            this.grdRuleList = new System.Windows.Forms.DataGridView();
            this.btnRuleConditions = new System.Windows.Forms.Button();
            this.btnDeleteRule = new System.Windows.Forms.Button();
            this.pnlRuleList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRuleList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddRuleConditon
            // 
            this.btnAddRuleConditon.Location = new System.Drawing.Point(267, 12);
            this.btnAddRuleConditon.Name = "btnAddRuleConditon";
            this.btnAddRuleConditon.Size = new System.Drawing.Size(114, 23);
            this.btnAddRuleConditon.TabIndex = 3;
            this.btnAddRuleConditon.Text = "Add New Rule";
            this.btnAddRuleConditon.UseVisualStyleBackColor = true;
            this.btnAddRuleConditon.Click += new System.EventHandler(this.btnAddRule_Click);
            // 
            // pnlRuleList
            // 
            this.pnlRuleList.Controls.Add(this.lblNoRuleFound);
            this.pnlRuleList.Controls.Add(this.grdRuleList);
            this.pnlRuleList.Location = new System.Drawing.Point(0, 41);
            this.pnlRuleList.Name = "pnlRuleList";
            this.pnlRuleList.Size = new System.Drawing.Size(644, 457);
            this.pnlRuleList.TabIndex = 4;
            // 
            // lblNoRuleFound
            // 
            this.lblNoRuleFound.AutoSize = true;
            this.lblNoRuleFound.Location = new System.Drawing.Point(264, 179);
            this.lblNoRuleFound.Name = "lblNoRuleFound";
            this.lblNoRuleFound.Size = new System.Drawing.Size(79, 13);
            this.lblNoRuleFound.TabIndex = 1;
            this.lblNoRuleFound.Text = "No Rule Found";
            this.lblNoRuleFound.Visible = false;
            // 
            // grdRuleList
            // 
            this.grdRuleList.AllowUserToOrderColumns = true;
            this.grdRuleList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdRuleList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdRuleList.Location = new System.Drawing.Point(0, 0);
            this.grdRuleList.Name = "grdRuleList";
            this.grdRuleList.Size = new System.Drawing.Size(644, 457);
            this.grdRuleList.TabIndex = 0;
            this.grdRuleList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdRuleList_CellDoubleClick);
            // 
            // btnRuleConditions
            // 
            this.btnRuleConditions.Location = new System.Drawing.Point(141, 13);
            this.btnRuleConditions.Name = "btnRuleConditions";
            this.btnRuleConditions.Size = new System.Drawing.Size(107, 23);
            this.btnRuleConditions.TabIndex = 5;
            this.btnRuleConditions.Text = "Rule Conditions";
            this.btnRuleConditions.UseVisualStyleBackColor = true;
            this.btnRuleConditions.Click += new System.EventHandler(this.btnRuleCondition_Click);
            // 
            // btnDeleteRule
            // 
            this.btnDeleteRule.Location = new System.Drawing.Point(404, 12);
            this.btnDeleteRule.Name = "btnDeleteRule";
            this.btnDeleteRule.Size = new System.Drawing.Size(94, 23);
            this.btnDeleteRule.TabIndex = 6;
            this.btnDeleteRule.Text = "Delete Rule";
            this.btnDeleteRule.UseVisualStyleBackColor = true;
            this.btnDeleteRule.Click += new System.EventHandler(this.btnDeleteRule_Click);
            // 
            // RuleList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(646, 498);
            this.Controls.Add(this.btnDeleteRule);
            this.Controls.Add(this.btnRuleConditions);
            this.Controls.Add(this.pnlRuleList);
            this.Controls.Add(this.btnAddRuleConditon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RuleList";
            this.Text = "RuleSettings";
            this.pnlRuleList.ResumeLayout(false);
            this.pnlRuleList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRuleList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddRuleConditon;
        private System.Windows.Forms.Panel pnlRuleList;
        private System.Windows.Forms.DataGridView grdRuleList;
        private System.Windows.Forms.Label lblNoRuleFound;
        private System.Windows.Forms.Button btnRuleConditions;
        private System.Windows.Forms.Button btnDeleteRule;
    }
}