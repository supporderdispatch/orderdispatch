﻿namespace ODM.Appliction
{
    partial class HermesSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HermesSetting));
            this.lblAPIuser = new System.Windows.Forms.Label();
            this.txtApiUser = new System.Windows.Forms.TextBox();
            this.lblAPIPass = new System.Windows.Forms.Label();
            this.txtApiPass = new System.Windows.Forms.TextBox();
            this.lblAPIuserTest = new System.Windows.Forms.Label();
            this.txtAPIuserTest = new System.Windows.Forms.TextBox();
            this.lblAPIPassTest = new System.Windows.Forms.Label();
            this.txtApiPassTest = new System.Windows.Forms.TextBox();
            this.chkIsTest = new System.Windows.Forms.CheckBox();
            this.lblPrinter = new System.Windows.Forms.Label();
            this.ddlPrinter = new System.Windows.Forms.ComboBox();
            this.lblDefaultWeight = new System.Windows.Forms.Label();
            this.txtDefaultWeight = new System.Windows.Forms.TextBox();
            this.chkIsBoseLogIn = new System.Windows.Forms.CheckBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpBoxShippingSettings = new System.Windows.Forms.GroupBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSaveClose = new System.Windows.Forms.Button();
            this.grdMappedServices = new System.Windows.Forms.DataGridView();
            this.chkIsLandscape = new System.Windows.Forms.CheckBox();
            this.lblUserRequired = new System.Windows.Forms.Label();
            this.lblPswdRequired = new System.Windows.Forms.Label();
            this.lblTestUserRequired = new System.Windows.Forms.Label();
            this.lblTestPswdRequired = new System.Windows.Forms.Label();
            this.lblWeightRequired = new System.Windows.Forms.Label();
            this.lblWeightInvalid = new System.Windows.Forms.Label();
            this.grpBoxShippingSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAPIuser
            // 
            this.lblAPIuser.AutoSize = true;
            this.lblAPIuser.Location = new System.Drawing.Point(6, 8);
            this.lblAPIuser.Name = "lblAPIuser";
            this.lblAPIuser.Size = new System.Drawing.Size(49, 13);
            this.lblAPIuser.TabIndex = 2;
            this.lblAPIuser.Text = "API User";
            // 
            // txtApiUser
            // 
            this.txtApiUser.Location = new System.Drawing.Point(129, 8);
            this.txtApiUser.Name = "txtApiUser";
            this.txtApiUser.Size = new System.Drawing.Size(205, 20);
            this.txtApiUser.TabIndex = 1;
            // 
            // lblAPIPass
            // 
            this.lblAPIPass.AutoSize = true;
            this.lblAPIPass.Location = new System.Drawing.Point(6, 44);
            this.lblAPIPass.Name = "lblAPIPass";
            this.lblAPIPass.Size = new System.Drawing.Size(73, 13);
            this.lblAPIPass.TabIndex = 4;
            this.lblAPIPass.Text = "API Password";
            // 
            // txtApiPass
            // 
            this.txtApiPass.Location = new System.Drawing.Point(129, 44);
            this.txtApiPass.Name = "txtApiPass";
            this.txtApiPass.Size = new System.Drawing.Size(205, 20);
            this.txtApiPass.TabIndex = 2;
            // 
            // lblAPIuserTest
            // 
            this.lblAPIuserTest.AutoSize = true;
            this.lblAPIuserTest.Location = new System.Drawing.Point(9, 82);
            this.lblAPIuserTest.Name = "lblAPIuserTest";
            this.lblAPIuserTest.Size = new System.Drawing.Size(73, 13);
            this.lblAPIuserTest.TabIndex = 6;
            this.lblAPIuserTest.Text = "API User Test";
            // 
            // txtAPIuserTest
            // 
            this.txtAPIuserTest.Location = new System.Drawing.Point(129, 79);
            this.txtAPIuserTest.Name = "txtAPIuserTest";
            this.txtAPIuserTest.Size = new System.Drawing.Size(205, 20);
            this.txtAPIuserTest.TabIndex = 3;
            // 
            // lblAPIPassTest
            // 
            this.lblAPIPassTest.AutoSize = true;
            this.lblAPIPassTest.Location = new System.Drawing.Point(6, 112);
            this.lblAPIPassTest.Name = "lblAPIPassTest";
            this.lblAPIPassTest.Size = new System.Drawing.Size(97, 13);
            this.lblAPIPassTest.TabIndex = 8;
            this.lblAPIPassTest.Text = "API Password Test";
            // 
            // txtApiPassTest
            // 
            this.txtApiPassTest.Location = new System.Drawing.Point(129, 109);
            this.txtApiPassTest.Name = "txtApiPassTest";
            this.txtApiPassTest.Size = new System.Drawing.Size(205, 20);
            this.txtApiPassTest.TabIndex = 4;
            // 
            // chkIsTest
            // 
            this.chkIsTest.AutoSize = true;
            this.chkIsTest.Location = new System.Drawing.Point(509, 80);
            this.chkIsTest.Name = "chkIsTest";
            this.chkIsTest.Size = new System.Drawing.Size(88, 17);
            this.chkIsTest.TabIndex = 7;
            this.chkIsTest.Text = "Is Test Mode";
            this.chkIsTest.UseVisualStyleBackColor = true;
            // 
            // lblPrinter
            // 
            this.lblPrinter.AutoSize = true;
            this.lblPrinter.Location = new System.Drawing.Point(394, 41);
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.Size = new System.Drawing.Size(70, 13);
            this.lblPrinter.TabIndex = 11;
            this.lblPrinter.Text = "Select Printer";
            // 
            // ddlPrinter
            // 
            this.ddlPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPrinter.FormattingEnabled = true;
            this.ddlPrinter.Items.AddRange(new object[] {
            "ZDesigner GC420d(EPL)",
            "Brother DCP-1510 series"});
            this.ddlPrinter.Location = new System.Drawing.Point(509, 44);
            this.ddlPrinter.Name = "ddlPrinter";
            this.ddlPrinter.Size = new System.Drawing.Size(246, 21);
            this.ddlPrinter.TabIndex = 6;
            // 
            // lblDefaultWeight
            // 
            this.lblDefaultWeight.AutoSize = true;
            this.lblDefaultWeight.Location = new System.Drawing.Point(394, 9);
            this.lblDefaultWeight.Name = "lblDefaultWeight";
            this.lblDefaultWeight.Size = new System.Drawing.Size(108, 13);
            this.lblDefaultWeight.TabIndex = 13;
            this.lblDefaultWeight.Text = "Default Weight In KG";
            // 
            // txtDefaultWeight
            // 
            this.txtDefaultWeight.Location = new System.Drawing.Point(509, 9);
            this.txtDefaultWeight.Name = "txtDefaultWeight";
            this.txtDefaultWeight.Size = new System.Drawing.Size(246, 20);
            this.txtDefaultWeight.TabIndex = 5;
            // 
            // chkIsBoseLogIn
            // 
            this.chkIsBoseLogIn.AutoSize = true;
            this.chkIsBoseLogIn.Location = new System.Drawing.Point(509, 111);
            this.chkIsBoseLogIn.Name = "chkIsBoseLogIn";
            this.chkIsBoseLogIn.Size = new System.Drawing.Size(91, 17);
            this.chkIsBoseLogIn.TabIndex = 8;
            this.chkIsBoseLogIn.Text = "Is Bose LogIn";
            this.chkIsBoseLogIn.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(602, 228);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(164, 29);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(416, 228);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(180, 29);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grpBoxShippingSettings
            // 
            this.grpBoxShippingSettings.Controls.Add(this.lblMessage);
            this.grpBoxShippingSettings.Controls.Add(this.btnCancel);
            this.grpBoxShippingSettings.Controls.Add(this.btnSaveClose);
            this.grpBoxShippingSettings.Controls.Add(this.grdMappedServices);
            this.grpBoxShippingSettings.Location = new System.Drawing.Point(12, 267);
            this.grpBoxShippingSettings.Name = "grpBoxShippingSettings";
            this.grpBoxShippingSettings.Size = new System.Drawing.Size(799, 229);
            this.grpBoxShippingSettings.TabIndex = 11;
            this.grpBoxShippingSettings.TabStop = false;
            this.grpBoxShippingSettings.Text = "Shipping Mapping";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(339, 78);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(92, 13);
            this.lblMessage.TabIndex = 16;
            this.lblMessage.Text = "No Record Found";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(590, 193);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(180, 29);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSaveClose
            // 
            this.btnSaveClose.Location = new System.Drawing.Point(404, 193);
            this.btnSaveClose.Name = "btnSaveClose";
            this.btnSaveClose.Size = new System.Drawing.Size(180, 29);
            this.btnSaveClose.TabIndex = 13;
            this.btnSaveClose.Text = "Save && Close";
            this.btnSaveClose.UseVisualStyleBackColor = true;
            this.btnSaveClose.Click += new System.EventHandler(this.btnSaveClose_Click);
            // 
            // grdMappedServices
            // 
            this.grdMappedServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMappedServices.Location = new System.Drawing.Point(1, 19);
            this.grdMappedServices.Name = "grdMappedServices";
            this.grdMappedServices.Size = new System.Drawing.Size(792, 150);
            this.grdMappedServices.TabIndex = 12;
            // 
            // chkIsLandscape
            // 
            this.chkIsLandscape.AutoSize = true;
            this.chkIsLandscape.Location = new System.Drawing.Point(509, 143);
            this.chkIsLandscape.Name = "chkIsLandscape";
            this.chkIsLandscape.Size = new System.Drawing.Size(90, 17);
            this.chkIsLandscape.TabIndex = 14;
            this.chkIsLandscape.Text = "Is Landscape";
            this.chkIsLandscape.UseVisualStyleBackColor = true;
            // 
            // lblUserRequired
            // 
            this.lblUserRequired.AutoSize = true;
            this.lblUserRequired.ForeColor = System.Drawing.Color.Red;
            this.lblUserRequired.Location = new System.Drawing.Point(341, 8);
            this.lblUserRequired.Name = "lblUserRequired";
            this.lblUserRequired.Size = new System.Drawing.Size(50, 13);
            this.lblUserRequired.TabIndex = 15;
            this.lblUserRequired.Text = "Required";
            this.lblUserRequired.Visible = false;
            // 
            // lblPswdRequired
            // 
            this.lblPswdRequired.AutoSize = true;
            this.lblPswdRequired.ForeColor = System.Drawing.Color.Red;
            this.lblPswdRequired.Location = new System.Drawing.Point(341, 44);
            this.lblPswdRequired.Name = "lblPswdRequired";
            this.lblPswdRequired.Size = new System.Drawing.Size(50, 13);
            this.lblPswdRequired.TabIndex = 16;
            this.lblPswdRequired.Text = "Required";
            this.lblPswdRequired.Visible = false;
            // 
            // lblTestUserRequired
            // 
            this.lblTestUserRequired.AutoSize = true;
            this.lblTestUserRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTestUserRequired.Location = new System.Drawing.Point(341, 81);
            this.lblTestUserRequired.Name = "lblTestUserRequired";
            this.lblTestUserRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTestUserRequired.TabIndex = 17;
            this.lblTestUserRequired.Text = "Required";
            this.lblTestUserRequired.Visible = false;
            // 
            // lblTestPswdRequired
            // 
            this.lblTestPswdRequired.AutoSize = true;
            this.lblTestPswdRequired.ForeColor = System.Drawing.Color.Red;
            this.lblTestPswdRequired.Location = new System.Drawing.Point(341, 110);
            this.lblTestPswdRequired.Name = "lblTestPswdRequired";
            this.lblTestPswdRequired.Size = new System.Drawing.Size(50, 13);
            this.lblTestPswdRequired.TabIndex = 18;
            this.lblTestPswdRequired.Text = "Required";
            this.lblTestPswdRequired.Visible = false;
            // 
            // lblWeightRequired
            // 
            this.lblWeightRequired.AutoSize = true;
            this.lblWeightRequired.ForeColor = System.Drawing.Color.Red;
            this.lblWeightRequired.Location = new System.Drawing.Point(761, 12);
            this.lblWeightRequired.Name = "lblWeightRequired";
            this.lblWeightRequired.Size = new System.Drawing.Size(50, 13);
            this.lblWeightRequired.TabIndex = 19;
            this.lblWeightRequired.Text = "Required";
            this.lblWeightRequired.Visible = false;
            // 
            // lblWeightInvalid
            // 
            this.lblWeightInvalid.AutoSize = true;
            this.lblWeightInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblWeightInvalid.Location = new System.Drawing.Point(761, 12);
            this.lblWeightInvalid.Name = "lblWeightInvalid";
            this.lblWeightInvalid.Size = new System.Drawing.Size(65, 13);
            this.lblWeightInvalid.TabIndex = 20;
            this.lblWeightInvalid.Text = "Invalid Input";
            this.lblWeightInvalid.Visible = false;
            // 
            // HermesSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 521);
            this.Controls.Add(this.lblWeightInvalid);
            this.Controls.Add(this.lblWeightRequired);
            this.Controls.Add(this.lblTestPswdRequired);
            this.Controls.Add(this.lblTestUserRequired);
            this.Controls.Add(this.lblPswdRequired);
            this.Controls.Add(this.lblUserRequired);
            this.Controls.Add(this.chkIsLandscape);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpBoxShippingSettings);
            this.Controls.Add(this.chkIsBoseLogIn);
            this.Controls.Add(this.txtDefaultWeight);
            this.Controls.Add(this.lblDefaultWeight);
            this.Controls.Add(this.ddlPrinter);
            this.Controls.Add(this.lblPrinter);
            this.Controls.Add(this.chkIsTest);
            this.Controls.Add(this.txtApiPassTest);
            this.Controls.Add(this.lblAPIPassTest);
            this.Controls.Add(this.txtAPIuserTest);
            this.Controls.Add(this.lblAPIuserTest);
            this.Controls.Add(this.txtApiPass);
            this.Controls.Add(this.lblAPIPass);
            this.Controls.Add(this.txtApiUser);
            this.Controls.Add(this.lblAPIuser);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HermesSetting";
            this.Text = "Hermes Setting";
            this.grpBoxShippingSettings.ResumeLayout(false);
            this.grpBoxShippingSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappedServices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAPIuser;
        private System.Windows.Forms.TextBox txtApiUser;
        private System.Windows.Forms.Label lblAPIPass;
        private System.Windows.Forms.TextBox txtApiPass;
        private System.Windows.Forms.Label lblAPIuserTest;
        private System.Windows.Forms.TextBox txtAPIuserTest;
        private System.Windows.Forms.Label lblAPIPassTest;
        private System.Windows.Forms.TextBox txtApiPassTest;
        private System.Windows.Forms.CheckBox chkIsTest;
        private System.Windows.Forms.Label lblPrinter;
        private System.Windows.Forms.ComboBox ddlPrinter;
        private System.Windows.Forms.Label lblDefaultWeight;
        private System.Windows.Forms.TextBox txtDefaultWeight;
        private System.Windows.Forms.CheckBox chkIsBoseLogIn;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grpBoxShippingSettings;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSaveClose;
        private System.Windows.Forms.DataGridView grdMappedServices;
        private System.Windows.Forms.CheckBox chkIsLandscape;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblUserRequired;
        private System.Windows.Forms.Label lblPswdRequired;
        private System.Windows.Forms.Label lblTestUserRequired;
        private System.Windows.Forms.Label lblTestPswdRequired;
        private System.Windows.Forms.Label lblWeightRequired;
        private System.Windows.Forms.Label lblWeightInvalid;
    }
}