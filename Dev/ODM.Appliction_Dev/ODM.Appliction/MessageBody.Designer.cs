﻿namespace ODM.Appliction
{
    partial class MessageBody
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBody));
            this.tlPanelMessageBody = new System.Windows.Forms.TableLayoutPanel();
            this.splContMessageBody = new System.Windows.Forms.SplitContainer();
            this.wbMessageBody = new System.Windows.Forms.WebBrowser();
            this.txtMessageBody = new System.Windows.Forms.RichTextBox();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.lblSubject = new System.Windows.Forms.Label();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.lblTotalValue = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblOrderShipped = new System.Windows.Forms.Label();
            this.lblOrderShippedDate = new System.Windows.Forms.Label();
            this.lblOrderReceivedDate = new System.Windows.Forms.Label();
            this.lblOrderReceived = new System.Windows.Forms.Label();
            this.lblTrackingNumber = new System.Windows.Forms.Label();
            this.lblCourierName = new System.Windows.Forms.Label();
            this.lblOrderItems = new System.Windows.Forms.Label();
            this.lblCustomerAddress = new System.Windows.Forms.Label();
            this.pnlOrderItems = new System.Windows.Forms.Panel();
            this.grdOrderItemDetails = new System.Windows.Forms.DataGridView();
            this.pnlAddress = new System.Windows.Forms.Panel();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.btnManageResends = new System.Windows.Forms.Button();
            this.btnManageReturns = new System.Windows.Forms.Button();
            this.btnEmailInvoice = new System.Windows.Forms.Button();
            this.lblNumOrderId = new System.Windows.Forms.Label();
            this.ddlNumOrderId = new System.Windows.Forms.ComboBox();
            this.tbConMessageReply = new System.Windows.Forms.TabControl();
            this.tbpCustomerReply = new System.Windows.Forms.TabPage();
            this.chkDisplayReplyToPublic = new System.Windows.Forms.CheckBox();
            this.lblReplyMessageBodyShow = new System.Windows.Forms.Label();
            this.txtReplyMessageBody = new System.Windows.Forms.RichTextBox();
            this.ddlTemplates = new System.Windows.Forms.ComboBox();
            this.lblMessageTemplates = new System.Windows.Forms.Label();
            this.tlPanelMessageBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splContMessageBody)).BeginInit();
            this.splContMessageBody.Panel1.SuspendLayout();
            this.splContMessageBody.Panel2.SuspendLayout();
            this.splContMessageBody.SuspendLayout();
            this.pnlOrderItems.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderItemDetails)).BeginInit();
            this.pnlAddress.SuspendLayout();
            this.tbConMessageReply.SuspendLayout();
            this.tbpCustomerReply.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlPanelMessageBody
            // 
            this.tlPanelMessageBody.ColumnCount = 1;
            this.tlPanelMessageBody.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlPanelMessageBody.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 634F));
            this.tlPanelMessageBody.Controls.Add(this.splContMessageBody, 0, 0);
            this.tlPanelMessageBody.Dock = System.Windows.Forms.DockStyle.Left;
            this.tlPanelMessageBody.Location = new System.Drawing.Point(0, 0);
            this.tlPanelMessageBody.Name = "tlPanelMessageBody";
            this.tlPanelMessageBody.RowCount = 1;
            this.tlPanelMessageBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlPanelMessageBody.Size = new System.Drawing.Size(1365, 562);
            this.tlPanelMessageBody.TabIndex = 0;
            // 
            // splContMessageBody
            // 
            this.splContMessageBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splContMessageBody.Location = new System.Drawing.Point(3, 3);
            this.splContMessageBody.Name = "splContMessageBody";
            // 
            // splContMessageBody.Panel1
            // 
            this.splContMessageBody.Panel1.Controls.Add(this.wbMessageBody);
            this.splContMessageBody.Panel1.Controls.Add(this.txtMessageBody);
            this.splContMessageBody.Panel1.Controls.Add(this.txtSubject);
            this.splContMessageBody.Panel1.Controls.Add(this.txtCustomerName);
            this.splContMessageBody.Panel1.Controls.Add(this.lblSubject);
            this.splContMessageBody.Panel1.Controls.Add(this.lblCustomerName);
            // 
            // splContMessageBody.Panel2
            // 
            this.splContMessageBody.Panel2.Controls.Add(this.btnSendMessage);
            this.splContMessageBody.Panel2.Controls.Add(this.lblTotalValue);
            this.splContMessageBody.Panel2.Controls.Add(this.lblTotal);
            this.splContMessageBody.Panel2.Controls.Add(this.lblOrderShipped);
            this.splContMessageBody.Panel2.Controls.Add(this.lblOrderShippedDate);
            this.splContMessageBody.Panel2.Controls.Add(this.lblOrderReceivedDate);
            this.splContMessageBody.Panel2.Controls.Add(this.lblOrderReceived);
            this.splContMessageBody.Panel2.Controls.Add(this.lblTrackingNumber);
            this.splContMessageBody.Panel2.Controls.Add(this.lblCourierName);
            this.splContMessageBody.Panel2.Controls.Add(this.lblOrderItems);
            this.splContMessageBody.Panel2.Controls.Add(this.lblCustomerAddress);
            this.splContMessageBody.Panel2.Controls.Add(this.pnlOrderItems);
            this.splContMessageBody.Panel2.Controls.Add(this.pnlAddress);
            this.splContMessageBody.Panel2.Controls.Add(this.btnManageResends);
            this.splContMessageBody.Panel2.Controls.Add(this.btnManageReturns);
            this.splContMessageBody.Panel2.Controls.Add(this.btnEmailInvoice);
            this.splContMessageBody.Panel2.Controls.Add(this.lblNumOrderId);
            this.splContMessageBody.Panel2.Controls.Add(this.ddlNumOrderId);
            this.splContMessageBody.Panel2.Controls.Add(this.tbConMessageReply);
            this.splContMessageBody.Size = new System.Drawing.Size(1359, 556);
            this.splContMessageBody.SplitterDistance = 480;
            this.splContMessageBody.TabIndex = 0;
            // 
            // wbMessageBody
            // 
            this.wbMessageBody.Location = new System.Drawing.Point(5, 80);
            this.wbMessageBody.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbMessageBody.Name = "wbMessageBody";
            this.wbMessageBody.Size = new System.Drawing.Size(462, 415);
            this.wbMessageBody.TabIndex = 11;
            // 
            // txtMessageBody
            // 
            this.txtMessageBody.Location = new System.Drawing.Point(7, 80);
            this.txtMessageBody.Name = "txtMessageBody";
            this.txtMessageBody.Size = new System.Drawing.Size(460, 415);
            this.txtMessageBody.TabIndex = 10;
            this.txtMessageBody.Text = "";
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(95, 42);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(365, 20);
            this.txtSubject.TabIndex = 9;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new System.Drawing.Point(96, 13);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(365, 20);
            this.txtCustomerName.TabIndex = 8;
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(10, 41);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(49, 13);
            this.lblSubject.TabIndex = 6;
            this.lblSubject.Text = "Subject :";
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.AutoSize = true;
            this.lblCustomerName.Location = new System.Drawing.Point(6, 17);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(85, 13);
            this.lblCustomerName.TabIndex = 5;
            this.lblCustomerName.Text = "Customer Email :";
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.Location = new System.Drawing.Point(379, 513);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(124, 39);
            this.btnSendMessage.TabIndex = 1;
            this.btnSendMessage.Text = "Send Message";
            this.btnSendMessage.UseVisualStyleBackColor = true;
            this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // lblTotalValue
            // 
            this.lblTotalValue.AutoSize = true;
            this.lblTotalValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalValue.Location = new System.Drawing.Point(742, 396);
            this.lblTotalValue.Name = "lblTotalValue";
            this.lblTotalValue.Size = new System.Drawing.Size(0, 13);
            this.lblTotalValue.TabIndex = 17;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(691, 395);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(44, 13);
            this.lblTotal.TabIndex = 16;
            this.lblTotal.Text = "Total :";
            // 
            // lblOrderShipped
            // 
            this.lblOrderShipped.AutoSize = true;
            this.lblOrderShipped.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderShipped.Location = new System.Drawing.Point(518, 475);
            this.lblOrderShipped.Name = "lblOrderShipped";
            this.lblOrderShipped.Size = new System.Drawing.Size(96, 13);
            this.lblOrderShipped.TabIndex = 15;
            this.lblOrderShipped.Text = "Order Shipped :";
            // 
            // lblOrderShippedDate
            // 
            this.lblOrderShippedDate.AutoSize = true;
            this.lblOrderShippedDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderShippedDate.Location = new System.Drawing.Point(713, 475);
            this.lblOrderShippedDate.Name = "lblOrderShippedDate";
            this.lblOrderShippedDate.Size = new System.Drawing.Size(0, 13);
            this.lblOrderShippedDate.TabIndex = 14;
            // 
            // lblOrderReceivedDate
            // 
            this.lblOrderReceivedDate.AutoSize = true;
            this.lblOrderReceivedDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderReceivedDate.Location = new System.Drawing.Point(713, 453);
            this.lblOrderReceivedDate.Name = "lblOrderReceivedDate";
            this.lblOrderReceivedDate.Size = new System.Drawing.Size(0, 13);
            this.lblOrderReceivedDate.TabIndex = 14;
            // 
            // lblOrderReceived
            // 
            this.lblOrderReceived.AutoSize = true;
            this.lblOrderReceived.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderReceived.Location = new System.Drawing.Point(518, 450);
            this.lblOrderReceived.Name = "lblOrderReceived";
            this.lblOrderReceived.Size = new System.Drawing.Size(104, 13);
            this.lblOrderReceived.TabIndex = 13;
            this.lblOrderReceived.Text = "Order Received :";
            // 
            // lblTrackingNumber
            // 
            this.lblTrackingNumber.AutoSize = true;
            this.lblTrackingNumber.Location = new System.Drawing.Point(668, 430);
            this.lblTrackingNumber.Name = "lblTrackingNumber";
            this.lblTrackingNumber.Size = new System.Drawing.Size(0, 13);
            this.lblTrackingNumber.TabIndex = 12;
            // 
            // lblCourierName
            // 
            this.lblCourierName.AutoSize = true;
            this.lblCourierName.Location = new System.Drawing.Point(515, 430);
            this.lblCourierName.Name = "lblCourierName";
            this.lblCourierName.Size = new System.Drawing.Size(0, 13);
            this.lblCourierName.TabIndex = 11;
            // 
            // lblOrderItems
            // 
            this.lblOrderItems.AutoSize = true;
            this.lblOrderItems.Location = new System.Drawing.Point(525, 261);
            this.lblOrderItems.Name = "lblOrderItems";
            this.lblOrderItems.Size = new System.Drawing.Size(67, 13);
            this.lblOrderItems.TabIndex = 9;
            this.lblOrderItems.Text = "Order Items :";
            // 
            // lblCustomerAddress
            // 
            this.lblCustomerAddress.AutoSize = true;
            this.lblCustomerAddress.Location = new System.Drawing.Point(525, 143);
            this.lblCustomerAddress.Name = "lblCustomerAddress";
            this.lblCustomerAddress.Size = new System.Drawing.Size(51, 13);
            this.lblCustomerAddress.TabIndex = 8;
            this.lblCustomerAddress.Text = "Address :";
            // 
            // pnlOrderItems
            // 
            this.pnlOrderItems.AutoScroll = true;
            this.pnlOrderItems.Controls.Add(this.grdOrderItemDetails);
            this.pnlOrderItems.Location = new System.Drawing.Point(473, 288);
            this.pnlOrderItems.Name = "pnlOrderItems";
            this.pnlOrderItems.Size = new System.Drawing.Size(398, 100);
            this.pnlOrderItems.TabIndex = 7;
            // 
            // grdOrderItemDetails
            // 
            this.grdOrderItemDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOrderItemDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdOrderItemDetails.Location = new System.Drawing.Point(0, 0);
            this.grdOrderItemDetails.Name = "grdOrderItemDetails";
            this.grdOrderItemDetails.Size = new System.Drawing.Size(398, 100);
            this.grdOrderItemDetails.TabIndex = 0;
            // 
            // pnlAddress
            // 
            this.pnlAddress.Controls.Add(this.txtAddress);
            this.pnlAddress.Location = new System.Drawing.Point(612, 137);
            this.pnlAddress.Name = "pnlAddress";
            this.pnlAddress.Size = new System.Drawing.Size(222, 100);
            this.pnlAddress.TabIndex = 6;
            // 
            // txtAddress
            // 
            this.txtAddress.Enabled = false;
            this.txtAddress.Location = new System.Drawing.Point(1, 3);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(219, 94);
            this.txtAddress.TabIndex = 0;
            // 
            // btnManageResends
            // 
            this.btnManageResends.Location = new System.Drawing.Point(727, 12);
            this.btnManageResends.Name = "btnManageResends";
            this.btnManageResends.Size = new System.Drawing.Size(107, 33);
            this.btnManageResends.TabIndex = 5;
            this.btnManageResends.Text = "Manage Resends";
            this.btnManageResends.UseVisualStyleBackColor = true;
            // 
            // btnManageReturns
            // 
            this.btnManageReturns.Location = new System.Drawing.Point(616, 12);
            this.btnManageReturns.Name = "btnManageReturns";
            this.btnManageReturns.Size = new System.Drawing.Size(97, 33);
            this.btnManageReturns.TabIndex = 5;
            this.btnManageReturns.Text = "Manage Returns";
            this.btnManageReturns.UseVisualStyleBackColor = true;
            // 
            // btnEmailInvoice
            // 
            this.btnEmailInvoice.Location = new System.Drawing.Point(520, 12);
            this.btnEmailInvoice.Name = "btnEmailInvoice";
            this.btnEmailInvoice.Size = new System.Drawing.Size(78, 33);
            this.btnEmailInvoice.TabIndex = 5;
            this.btnEmailInvoice.Text = "Email Invoice";
            this.btnEmailInvoice.UseVisualStyleBackColor = true;
            this.btnEmailInvoice.Click += new System.EventHandler(this.btnEmailInvoice_Click);
            // 
            // lblNumOrderId
            // 
            this.lblNumOrderId.AutoSize = true;
            this.lblNumOrderId.Location = new System.Drawing.Point(525, 87);
            this.lblNumOrderId.Name = "lblNumOrderId";
            this.lblNumOrderId.Size = new System.Drawing.Size(51, 13);
            this.lblNumOrderId.TabIndex = 4;
            this.lblNumOrderId.Text = "Order Id :";
            // 
            // ddlNumOrderId
            // 
            this.ddlNumOrderId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlNumOrderId.FormattingEnabled = true;
            this.ddlNumOrderId.Location = new System.Drawing.Point(613, 82);
            this.ddlNumOrderId.Name = "ddlNumOrderId";
            this.ddlNumOrderId.Size = new System.Drawing.Size(121, 21);
            this.ddlNumOrderId.TabIndex = 3;
            // 
            // tbConMessageReply
            // 
            this.tbConMessageReply.Controls.Add(this.tbpCustomerReply);
            this.tbConMessageReply.Location = new System.Drawing.Point(3, 3);
            this.tbConMessageReply.Name = "tbConMessageReply";
            this.tbConMessageReply.SelectedIndex = 0;
            this.tbConMessageReply.Size = new System.Drawing.Size(464, 496);
            this.tbConMessageReply.TabIndex = 2;
            // 
            // tbpCustomerReply
            // 
            this.tbpCustomerReply.Controls.Add(this.chkDisplayReplyToPublic);
            this.tbpCustomerReply.Controls.Add(this.lblReplyMessageBodyShow);
            this.tbpCustomerReply.Controls.Add(this.txtReplyMessageBody);
            this.tbpCustomerReply.Controls.Add(this.ddlTemplates);
            this.tbpCustomerReply.Controls.Add(this.lblMessageTemplates);
            this.tbpCustomerReply.Location = new System.Drawing.Point(4, 22);
            this.tbpCustomerReply.Name = "tbpCustomerReply";
            this.tbpCustomerReply.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCustomerReply.Size = new System.Drawing.Size(456, 470);
            this.tbpCustomerReply.TabIndex = 0;
            this.tbpCustomerReply.Text = "Customer Reply";
            this.tbpCustomerReply.UseVisualStyleBackColor = true;
            // 
            // chkDisplayReplyToPublic
            // 
            this.chkDisplayReplyToPublic.AutoSize = true;
            this.chkDisplayReplyToPublic.Location = new System.Drawing.Point(336, 17);
            this.chkDisplayReplyToPublic.Name = "chkDisplayReplyToPublic";
            this.chkDisplayReplyToPublic.Size = new System.Drawing.Size(102, 17);
            this.chkDisplayReplyToPublic.TabIndex = 14;
            this.chkDisplayReplyToPublic.Text = "DisplayToPublic";
            this.chkDisplayReplyToPublic.UseVisualStyleBackColor = true;
            this.chkDisplayReplyToPublic.Visible = false;
            // 
            // lblReplyMessageBodyShow
            // 
            this.lblReplyMessageBodyShow.AutoSize = true;
            this.lblReplyMessageBodyShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReplyMessageBodyShow.ForeColor = System.Drawing.Color.Red;
            this.lblReplyMessageBodyShow.Location = new System.Drawing.Point(165, 42);
            this.lblReplyMessageBodyShow.Name = "lblReplyMessageBodyShow";
            this.lblReplyMessageBodyShow.Size = new System.Drawing.Size(249, 13);
            this.lblReplyMessageBodyShow.TabIndex = 13;
            this.lblReplyMessageBodyShow.Text = "This is the reply message sent to this user.";
            this.lblReplyMessageBodyShow.Visible = false;
            // 
            // txtReplyMessageBody
            // 
            this.txtReplyMessageBody.Location = new System.Drawing.Point(4, 59);
            this.txtReplyMessageBody.Name = "txtReplyMessageBody";
            this.txtReplyMessageBody.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtReplyMessageBody.Size = new System.Drawing.Size(442, 405);
            this.txtReplyMessageBody.TabIndex = 12;
            this.txtReplyMessageBody.Text = "";
            // 
            // ddlTemplates
            // 
            this.ddlTemplates.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlTemplates.FormattingEnabled = true;
            this.ddlTemplates.Location = new System.Drawing.Point(66, 15);
            this.ddlTemplates.Name = "ddlTemplates";
            this.ddlTemplates.Size = new System.Drawing.Size(249, 21);
            this.ddlTemplates.TabIndex = 11;
            this.ddlTemplates.SelectedIndexChanged += new System.EventHandler(this.ddlTemplates_SelectedIndexChanged);
            // 
            // lblMessageTemplates
            // 
            this.lblMessageTemplates.AutoSize = true;
            this.lblMessageTemplates.Location = new System.Drawing.Point(3, 18);
            this.lblMessageTemplates.Name = "lblMessageTemplates";
            this.lblMessageTemplates.Size = new System.Drawing.Size(59, 13);
            this.lblMessageTemplates.TabIndex = 10;
            this.lblMessageTemplates.Text = "Templates:";
            // 
            // MessageBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 562);
            this.Controls.Add(this.tlPanelMessageBody);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MessageBody";
            this.Text = "ReplyMessageBody";
            this.tlPanelMessageBody.ResumeLayout(false);
            this.splContMessageBody.Panel1.ResumeLayout(false);
            this.splContMessageBody.Panel1.PerformLayout();
            this.splContMessageBody.Panel2.ResumeLayout(false);
            this.splContMessageBody.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splContMessageBody)).EndInit();
            this.splContMessageBody.ResumeLayout(false);
            this.pnlOrderItems.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderItemDetails)).EndInit();
            this.pnlAddress.ResumeLayout(false);
            this.pnlAddress.PerformLayout();
            this.tbConMessageReply.ResumeLayout(false);
            this.tbpCustomerReply.ResumeLayout(false);
            this.tbpCustomerReply.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlPanelMessageBody;
        private System.Windows.Forms.SplitContainer splContMessageBody;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label lblCustomerName;
        private System.Windows.Forms.Button btnSendMessage;
        private System.Windows.Forms.RichTextBox txtMessageBody;
        private System.Windows.Forms.WebBrowser wbMessageBody;
        private System.Windows.Forms.TabControl tbConMessageReply;
        private System.Windows.Forms.TabPage tbpCustomerReply;
        private System.Windows.Forms.CheckBox chkDisplayReplyToPublic;
        private System.Windows.Forms.Label lblReplyMessageBodyShow;
        private System.Windows.Forms.RichTextBox txtReplyMessageBody;
        private System.Windows.Forms.ComboBox ddlTemplates;
        private System.Windows.Forms.Label lblMessageTemplates;
        private System.Windows.Forms.ComboBox ddlNumOrderId;
        private System.Windows.Forms.Label lblNumOrderId;
        private System.Windows.Forms.Button btnManageResends;
        private System.Windows.Forms.Button btnManageReturns;
        private System.Windows.Forms.Button btnEmailInvoice;
        private System.Windows.Forms.Panel pnlAddress;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Panel pnlOrderItems;
        private System.Windows.Forms.Label lblOrderItems;
        private System.Windows.Forms.Label lblCustomerAddress;
        private System.Windows.Forms.Label lblTrackingNumber;
        private System.Windows.Forms.Label lblCourierName;
        private System.Windows.Forms.DataGridView grdOrderItemDetails;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblOrderShipped;
        private System.Windows.Forms.Label lblOrderShippedDate;
        private System.Windows.Forms.Label lblOrderReceivedDate;
        private System.Windows.Forms.Label lblOrderReceived;
        private System.Windows.Forms.Label lblTotalValue;
    }
}