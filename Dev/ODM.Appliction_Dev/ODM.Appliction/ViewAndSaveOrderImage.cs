﻿using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class ViewAndSaveOrderImage : Form
    {
        Image _OrderImage=null;
        Image _OriginalOrderImage;
        string _strNumOrderId = string.Empty;
        private Image Image;
        public ViewAndSaveOrderImage()
        {
            InitializeComponent();
        }
        public ViewAndSaveOrderImage(string strBase64ImageString,string strNumOrderId)
        {
            InitializeComponent();
            _OrderImage = GlobalVariablesAndMethods.Base64StringToImage(strBase64ImageString);
            _OriginalOrderImage = GlobalVariablesAndMethods.Base64StringToImage(strBase64ImageString);
            _strNumOrderId = strNumOrderId;
          //  picBoxSaveImage.Image = OrderImage;
            pbOrderImage.Image = _OrderImage;
           // pbOrderImage.SizeMode = PictureBoxSizeMode.AutoSize;
        }
        public ViewAndSaveOrderImage(string strBase64ImageString)
        {
            InitializeComponent();
            _OrderImage = GlobalVariablesAndMethods.Base64StringToImage(strBase64ImageString);
            _OriginalOrderImage = GlobalVariablesAndMethods.Base64StringToImage(strBase64ImageString);            
            //  picBoxSaveImage.Image = OrderImage;
            pbOrderImage.Image = _OrderImage;
            // pbOrderImage.SizeMode = PictureBoxSizeMode.AutoSize;
        }

       
      

        private void picture_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void pbOrderImage_Click(object sender, EventArgs e)
        {
           
            ContextMenuStrip cm = new ContextMenuStrip();
            cm.Items.Add(new ToolStripMenuItem("Rotate Image", new Bitmap(1, 1), rotateImage_Click));
        }

        private void rotateImage_Click(object sender, EventArgs e)
        {
           

        }

        private void rotateRight90ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Image image = pbOrderImage.Image;
            image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            pbOrderImage.Image = image;
        }

        private void rotateLeft90ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Image image = pbOrderImage.Image;
            image.RotateFlip(RotateFlipType.Rotate90FlipXY);
            pbOrderImage.Image = image;

        }

        private void rotate180ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Image image = pbOrderImage.Image;
            image.RotateFlip(RotateFlipType.Rotate180FlipNone);
            pbOrderImage.Image = image;
        }

        private void flipVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Image image = pbOrderImage.Image;
            image.RotateFlip(RotateFlipType.RotateNoneFlipY);
            pbOrderImage.Image = image;
        }

        private void flipHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Image image = pbOrderImage.Image;
            image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            pbOrderImage.Image = image;
        }

        private void btnSaveRotatedImage_Click(object sender, EventArgs e)
        {
            string strPath = System.IO.Path.GetTempPath() + _strNumOrderId + ".jpeg";
            _OrderImage.Save(strPath, ImageFormat.Jpeg);

            SaveFileDialog objSaveFileDialog = new SaveFileDialog();
            objSaveFileDialog.FileName = _strNumOrderId;
            objSaveFileDialog.DefaultExt = ".jpeg";
            objSaveFileDialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.JPEG)|*.BMP;*.JPG;*.GIF;*.JPEG|All files(*.*)|*.*";
            if (objSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                _OrderImage.Save(objSaveFileDialog.FileName, ImageFormat.Jpeg);
                //_OrderImage.Save(strPath,ImageFormat.Jpeg);
            }
        }

        private void btnSaveOriginalImage_Click(object sender, EventArgs e)
        {
            string strPath = System.IO.Path.GetTempPath() + _strNumOrderId + ".jpeg";
            _OriginalOrderImage.Save(strPath, ImageFormat.Jpeg);

            SaveFileDialog objSaveFileDialog = new SaveFileDialog();
            objSaveFileDialog.FileName = _strNumOrderId;
            objSaveFileDialog.DefaultExt = ".jpeg";
            objSaveFileDialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.JPEG)|*.BMP;*.JPG;*.GIF;*.JPEG|All files(*.*)|*.*";
            if (objSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                _OriginalOrderImage.Save(objSaveFileDialog.FileName, ImageFormat.Jpeg);
                //_OrderImage.Save(strPath,ImageFormat.Jpeg);
            }
        }
    }
}
