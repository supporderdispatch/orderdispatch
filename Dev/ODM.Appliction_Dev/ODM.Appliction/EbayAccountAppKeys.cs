﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class EbayAccountAppKeys : Form
    {
        DataTable objDt = new DataTable();

        public EbayAccountAppKeys()
        {
            InitializeComponent();
            BindGridForAccount();
        }

        private void BindGridForAccount()
        {

            grdEbayAccountAppKeys.EditMode = DataGridViewEditMode.EditProgrammatically;
            DataTable objDt = new DataTable();
            objDt = GlobalVariablesAndMethods.GetEbayAccountAppKeys("GET");
            if (objDt.Rows.Count > 0)
            {
                grdEbayAccountAppKeys.DataSource = objDt;
               // grdEbayAccountAppKeys.Columns["AccountID"].Visible = false;
            }


        }
        

        private void btnAddNewKeys_Click(object sender, EventArgs e)
        {
            AddEBayAccountAppKeys objAddEbayKeys = new AddEBayAccountAppKeys(Guid.NewGuid(), "INSERT");
            objAddEbayKeys.ShowDialog();
            BindGridForAccount();
        }

        private void grdEbayAccountAppKeys_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            AddEBayAccountAppKeys objAddEbayKeys = new AddEBayAccountAppKeys(new Guid(grdEbayAccountAppKeys.CurrentRow.Cells["AccountID"].Value.ToString()), "UPDATE", new Guid(grdEbayAccountAppKeys.CurrentRow.Cells["ID"].Value.ToString()), grdEbayAccountAppKeys.CurrentRow.Cells["RuName"].Value.ToString(), grdEbayAccountAppKeys.CurrentRow.Cells["CertKey"].Value.ToString(), grdEbayAccountAppKeys.CurrentRow.Cells["DevKey"].Value.ToString(), grdEbayAccountAppKeys.CurrentRow.Cells["AppKey"].Value.ToString(), grdEbayAccountAppKeys.CurrentRow.Cells["AppKeysName"].Value.ToString());
            objAddEbayKeys.ShowDialog();
        
        }

        private void btnAuthorizeEbay_Click(object sender, EventArgs e)
        {
            LaunchSignInEbay objLaunchSignInEbay = new LaunchSignInEbay();
            objLaunchSignInEbay.ShowDialog();
                
                //string SessionID
           
        }

        private void btnEbayUsersList_Click(object sender, EventArgs e)
        {
            EbayUsersList objEbayUsersList = new EbayUsersList();
            objEbayUsersList.ShowDialog();
        }
    }
}
