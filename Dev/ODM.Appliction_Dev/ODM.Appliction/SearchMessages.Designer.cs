﻿namespace ODM.Appliction
{
    partial class SearchMessages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchMessages));
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.chkMessageSubject = new System.Windows.Forms.CheckBox();
            this.chkEmailAddress = new System.Windows.Forms.CheckBox();
            this.chkMessageBody = new System.Windows.Forms.CheckBox();
            this.btnSearchMessages = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.grdMessageList = new System.Windows.Forms.DataGridView();
            this.chkShowReadMessages = new System.Windows.Forms.CheckBox();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.pnlPaging = new System.Windows.Forms.Panel();
            this.chkShowAllMessages = new System.Windows.Forms.CheckBox();
            this.btnRepliedMessagesColor = new System.Windows.Forms.Button();
            this.btnUnReadMessagesColor = new System.Windows.Forms.Button();
            this.btnReadMessagesColor = new System.Windows.Forms.Button();
            this.lblRepliedMessagesColorText = new System.Windows.Forms.Label();
            this.lblUnreadMessagesColorText = new System.Windows.Forms.Label();
            this.lblReadMessagesColorText = new System.Windows.Forms.Label();
            this.lblNoRecordError = new System.Windows.Forms.Label();
            this.lblRequiredError = new System.Windows.Forms.Label();
            this.lblPageRecords = new System.Windows.Forms.Label();
            this.lblPageRecordsValue = new System.Windows.Forms.Label();
            this.lblTotalRecords = new System.Windows.Forms.Label();
            this.lblTotalRecordsValue = new System.Windows.Forms.Label();
            this.pnlSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMessageList)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlSearch.Controls.Add(this.chkMessageSubject);
            this.pnlSearch.Controls.Add(this.chkEmailAddress);
            this.pnlSearch.Controls.Add(this.chkMessageBody);
            this.pnlSearch.Controls.Add(this.btnSearchMessages);
            this.pnlSearch.Controls.Add(this.txtSearch);
            this.pnlSearch.Location = new System.Drawing.Point(562, 12);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(372, 63);
            this.pnlSearch.TabIndex = 25;
            // 
            // chkMessageSubject
            // 
            this.chkMessageSubject.AutoSize = true;
            this.chkMessageSubject.Location = new System.Drawing.Point(111, 34);
            this.chkMessageSubject.Name = "chkMessageSubject";
            this.chkMessageSubject.Size = new System.Drawing.Size(111, 17);
            this.chkMessageSubject.TabIndex = 22;
            this.chkMessageSubject.Text = "Search In Subject";
            this.chkMessageSubject.UseVisualStyleBackColor = true;
            // 
            // chkEmailAddress
            // 
            this.chkEmailAddress.AutoSize = true;
            this.chkEmailAddress.Location = new System.Drawing.Point(230, 34);
            this.chkEmailAddress.Name = "chkEmailAddress";
            this.chkEmailAddress.Size = new System.Drawing.Size(100, 17);
            this.chkEmailAddress.TabIndex = 24;
            this.chkEmailAddress.Text = "Search In Email";
            this.chkEmailAddress.UseVisualStyleBackColor = true;
            // 
            // chkMessageBody
            // 
            this.chkMessageBody.AutoSize = true;
            this.chkMessageBody.Location = new System.Drawing.Point(6, 34);
            this.chkMessageBody.Name = "chkMessageBody";
            this.chkMessageBody.Size = new System.Drawing.Size(99, 17);
            this.chkMessageBody.TabIndex = 22;
            this.chkMessageBody.Text = "Search In Body";
            this.chkMessageBody.UseVisualStyleBackColor = true;
            // 
            // btnSearchMessages
            // 
            this.btnSearchMessages.Location = new System.Drawing.Point(266, 2);
            this.btnSearchMessages.Name = "btnSearchMessages";
            this.btnSearchMessages.Size = new System.Drawing.Size(75, 23);
            this.btnSearchMessages.TabIndex = 23;
            this.btnSearchMessages.Text = "Search";
            this.btnSearchMessages.UseVisualStyleBackColor = true;
            this.btnSearchMessages.Click += new System.EventHandler(this.btnSearchMessages_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(4, 4);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(256, 20);
            this.txtSearch.TabIndex = 21;
            // 
            // grdMessageList
            // 
            this.grdMessageList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMessageList.Location = new System.Drawing.Point(1, 134);
            this.grdMessageList.Name = "grdMessageList";
            this.grdMessageList.Size = new System.Drawing.Size(971, 522);
            this.grdMessageList.TabIndex = 26;
            this.grdMessageList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMessageList_CellDoubleClick);
            // 
            // chkShowReadMessages
            // 
            this.chkShowReadMessages.AutoSize = true;
            this.chkShowReadMessages.Location = new System.Drawing.Point(377, 96);
            this.chkShowReadMessages.Name = "chkShowReadMessages";
            this.chkShowReadMessages.Size = new System.Drawing.Size(157, 17);
            this.chkShowReadMessages.TabIndex = 32;
            this.chkShowReadMessages.Text = "Show Read Messages Only";
            this.chkShowReadMessages.UseVisualStyleBackColor = true;
            this.chkShowReadMessages.Visible = false;
            this.chkShowReadMessages.CheckedChanged += new System.EventHandler(this.chkShowReadMessages_CheckedChanged);
            // 
            // btnLast
            // 
            this.btnLast.Location = new System.Drawing.Point(196, 96);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(30, 23);
            this.btnLast.TabIndex = 28;
            this.btnLast.Text = "| >";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Visible = false;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(160, 96);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(30, 23);
            this.btnNext.TabIndex = 29;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(63, 96);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(30, 23);
            this.btnPrevious.TabIndex = 30;
            this.btnPrevious.Text = "<";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Visible = false;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Location = new System.Drawing.Point(28, 96);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(30, 23);
            this.btnFirst.TabIndex = 31;
            this.btnFirst.Text = "< |";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Visible = false;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(108, 101);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(13, 13);
            this.lblStatus.TabIndex = 27;
            this.lblStatus.Text = "0";
            this.lblStatus.Visible = false;
            // 
            // pnlPaging
            // 
            this.pnlPaging.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPaging.Location = new System.Drawing.Point(12, 74);
            this.pnlPaging.Name = "pnlPaging";
            this.pnlPaging.Size = new System.Drawing.Size(231, 54);
            this.pnlPaging.TabIndex = 33;
            this.pnlPaging.Visible = false;
            // 
            // chkShowAllMessages
            // 
            this.chkShowAllMessages.AutoSize = true;
            this.chkShowAllMessages.Checked = true;
            this.chkShowAllMessages.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowAllMessages.Location = new System.Drawing.Point(557, 96);
            this.chkShowAllMessages.Name = "chkShowAllMessages";
            this.chkShowAllMessages.Size = new System.Drawing.Size(198, 17);
            this.chkShowAllMessages.TabIndex = 32;
            this.chkShowAllMessages.Text = "Show Both Read/Unread Messages";
            this.chkShowAllMessages.UseVisualStyleBackColor = true;
            this.chkShowAllMessages.Visible = false;
            this.chkShowAllMessages.CheckedChanged += new System.EventHandler(this.chkShowAllMessages_CheckedChanged);
            // 
            // btnRepliedMessagesColor
            // 
            this.btnRepliedMessagesColor.BackColor = System.Drawing.Color.Gray;
            this.btnRepliedMessagesColor.Location = new System.Drawing.Point(259, 45);
            this.btnRepliedMessagesColor.Name = "btnRepliedMessagesColor";
            this.btnRepliedMessagesColor.Size = new System.Drawing.Size(30, 19);
            this.btnRepliedMessagesColor.TabIndex = 37;
            this.btnRepliedMessagesColor.UseVisualStyleBackColor = false;
            // 
            // btnUnReadMessagesColor
            // 
            this.btnUnReadMessagesColor.BackColor = System.Drawing.Color.Red;
            this.btnUnReadMessagesColor.Location = new System.Drawing.Point(347, 8);
            this.btnUnReadMessagesColor.Name = "btnUnReadMessagesColor";
            this.btnUnReadMessagesColor.Size = new System.Drawing.Size(30, 19);
            this.btnUnReadMessagesColor.TabIndex = 38;
            this.btnUnReadMessagesColor.UseVisualStyleBackColor = false;
            // 
            // btnReadMessagesColor
            // 
            this.btnReadMessagesColor.BackColor = System.Drawing.Color.Green;
            this.btnReadMessagesColor.Location = new System.Drawing.Point(117, 7);
            this.btnReadMessagesColor.Name = "btnReadMessagesColor";
            this.btnReadMessagesColor.Size = new System.Drawing.Size(30, 19);
            this.btnReadMessagesColor.TabIndex = 39;
            this.btnReadMessagesColor.UseVisualStyleBackColor = false;
            // 
            // lblRepliedMessagesColorText
            // 
            this.lblRepliedMessagesColorText.AutoSize = true;
            this.lblRepliedMessagesColorText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRepliedMessagesColorText.Location = new System.Drawing.Point(60, 48);
            this.lblRepliedMessagesColorText.Name = "lblRepliedMessagesColorText";
            this.lblRepliedMessagesColorText.Size = new System.Drawing.Size(195, 13);
            this.lblRepliedMessagesColorText.TabIndex = 34;
            this.lblRepliedMessagesColorText.Text = "Messages Whose Resply is Sent:";
            // 
            // lblUnreadMessagesColorText
            // 
            this.lblUnreadMessagesColorText.AutoSize = true;
            this.lblUnreadMessagesColorText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnreadMessagesColorText.Location = new System.Drawing.Point(227, 10);
            this.lblUnreadMessagesColorText.Name = "lblUnreadMessagesColorText";
            this.lblUnreadMessagesColorText.Size = new System.Drawing.Size(121, 13);
            this.lblUnreadMessagesColorText.TabIndex = 35;
            this.lblUnreadMessagesColorText.Text = "UnRead Messages :";
            // 
            // lblReadMessagesColorText
            // 
            this.lblReadMessagesColorText.AutoSize = true;
            this.lblReadMessagesColorText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReadMessagesColorText.Location = new System.Drawing.Point(12, 9);
            this.lblReadMessagesColorText.Name = "lblReadMessagesColorText";
            this.lblReadMessagesColorText.Size = new System.Drawing.Size(105, 13);
            this.lblReadMessagesColorText.TabIndex = 36;
            this.lblReadMessagesColorText.Text = "Read Messages :";
            // 
            // lblNoRecordError
            // 
            this.lblNoRecordError.AutoSize = true;
            this.lblNoRecordError.Location = new System.Drawing.Point(447, 331);
            this.lblNoRecordError.Name = "lblNoRecordError";
            this.lblNoRecordError.Size = new System.Drawing.Size(92, 13);
            this.lblNoRecordError.TabIndex = 40;
            this.lblNoRecordError.Text = "No Record Found";
            this.lblNoRecordError.Visible = false;
            // 
            // lblRequiredError
            // 
            this.lblRequiredError.AutoSize = true;
            this.lblRequiredError.ForeColor = System.Drawing.Color.Red;
            this.lblRequiredError.Location = new System.Drawing.Point(501, 18);
            this.lblRequiredError.Name = "lblRequiredError";
            this.lblRequiredError.Size = new System.Drawing.Size(50, 13);
            this.lblRequiredError.TabIndex = 41;
            this.lblRequiredError.Text = "Required";
            this.lblRequiredError.Visible = false;
            // 
            // lblPageRecords
            // 
            this.lblPageRecords.AutoSize = true;
            this.lblPageRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageRecords.Location = new System.Drawing.Point(770, 97);
            this.lblPageRecords.Name = "lblPageRecords";
            this.lblPageRecords.Size = new System.Drawing.Size(95, 13);
            this.lblPageRecords.TabIndex = 42;
            this.lblPageRecords.Text = "Page Records :";
            this.lblPageRecords.Visible = false;
            // 
            // lblPageRecordsValue
            // 
            this.lblPageRecordsValue.AutoSize = true;
            this.lblPageRecordsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageRecordsValue.Location = new System.Drawing.Point(866, 97);
            this.lblPageRecordsValue.Name = "lblPageRecordsValue";
            this.lblPageRecordsValue.Size = new System.Drawing.Size(14, 13);
            this.lblPageRecordsValue.TabIndex = 42;
            this.lblPageRecordsValue.Text = "0";
            this.lblPageRecordsValue.Visible = false;
            // 
            // lblTotalRecords
            // 
            this.lblTotalRecords.AutoSize = true;
            this.lblTotalRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalRecords.Location = new System.Drawing.Point(400, 45);
            this.lblTotalRecords.Name = "lblTotalRecords";
            this.lblTotalRecords.Size = new System.Drawing.Size(95, 13);
            this.lblTotalRecords.TabIndex = 42;
            this.lblTotalRecords.Text = "Total Records :";
            // 
            // lblTotalRecordsValue
            // 
            this.lblTotalRecordsValue.AutoSize = true;
            this.lblTotalRecordsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalRecordsValue.Location = new System.Drawing.Point(496, 45);
            this.lblTotalRecordsValue.Name = "lblTotalRecordsValue";
            this.lblTotalRecordsValue.Size = new System.Drawing.Size(14, 13);
            this.lblTotalRecordsValue.TabIndex = 42;
            this.lblTotalRecordsValue.Text = "0";
            // 
            // SearchMessages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 657);
            this.Controls.Add(this.lblTotalRecordsValue);
            this.Controls.Add(this.lblPageRecordsValue);
            this.Controls.Add(this.lblTotalRecords);
            this.Controls.Add(this.lblPageRecords);
            this.Controls.Add(this.lblRequiredError);
            this.Controls.Add(this.lblNoRecordError);
            this.Controls.Add(this.btnRepliedMessagesColor);
            this.Controls.Add(this.btnUnReadMessagesColor);
            this.Controls.Add(this.btnReadMessagesColor);
            this.Controls.Add(this.lblRepliedMessagesColorText);
            this.Controls.Add(this.lblUnreadMessagesColorText);
            this.Controls.Add(this.lblReadMessagesColorText);
            this.Controls.Add(this.chkShowAllMessages);
            this.Controls.Add(this.chkShowReadMessages);
            this.Controls.Add(this.btnLast);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnFirst);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.pnlPaging);
            this.Controls.Add(this.grdMessageList);
            this.Controls.Add(this.pnlSearch);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SearchMessages";
            this.Text = "SearchMessages";
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMessageList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.CheckBox chkMessageSubject;
        private System.Windows.Forms.CheckBox chkEmailAddress;
        private System.Windows.Forms.CheckBox chkMessageBody;
        private System.Windows.Forms.Button btnSearchMessages;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView grdMessageList;
        private System.Windows.Forms.CheckBox chkShowReadMessages;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Panel pnlPaging;
        private System.Windows.Forms.CheckBox chkShowAllMessages;
        private System.Windows.Forms.Button btnRepliedMessagesColor;
        private System.Windows.Forms.Button btnUnReadMessagesColor;
        private System.Windows.Forms.Button btnReadMessagesColor;
        private System.Windows.Forms.Label lblRepliedMessagesColorText;
        private System.Windows.Forms.Label lblUnreadMessagesColorText;
        private System.Windows.Forms.Label lblReadMessagesColorText;
        private System.Windows.Forms.Label lblNoRecordError;
        private System.Windows.Forms.Label lblRequiredError;
        private System.Windows.Forms.Label lblPageRecords;
        private System.Windows.Forms.Label lblPageRecordsValue;
        private System.Windows.Forms.Label lblTotalRecords;
        private System.Windows.Forms.Label lblTotalRecordsValue;
    }
}