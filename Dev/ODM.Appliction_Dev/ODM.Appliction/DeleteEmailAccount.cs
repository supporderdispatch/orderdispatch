﻿using System;
using System.Data;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class DeleteEmailAccount : Form
    {
        DataTable dtDetails = new DataTable();
        string strAccountIds = "";
        public DeleteEmailAccount()
        {
            InitializeComponent();
        }
        public DeleteEmailAccount(DataTable objDtDetails)
        {
            InitializeComponent();
            dtDetails = objDtDetails;
            BindGrid(false);
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (GetSelectedUserId())
            {
                DialogResult result = MessageBox.Show("Are you sure, you want to delete Email Account(s)", "Delete Email Account", MessageBoxButtons.YesNo);
                if (result.Equals(DialogResult.Yes))
                {
                    AdditionalEmailAccount objAdditionalEmailAccount = new AdditionalEmailAccount();
                    objAdditionalEmailAccount.AddUpdateDeleteEmailAccount(strMode: "DELETE", strAccountIdsToDelete: strAccountIds);
                    BindGrid(true);
                    MessageBox.Show("Email Account(s) Deleted Successfully !");
                }
            }
            else
            {
                MessageBox.Show("Please select at least one email account to delete");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindGrid(bool IsRebind)
        {
            try
            {
                DataTable objDt = new DataTable();
                DataView DvDetails = new DataView(dtDetails);
                if (IsRebind)
                {
                    string[] arrAccountIDs = strAccountIds.Split(',');
                    for (int i = 0; i < arrAccountIDs.Length; i++)
                    {
                        DvDetails.RowFilter = "ID <> '" + arrAccountIDs[i] + "'";
                        objDt = DvDetails.ToTable();
                        DvDetails = new DataView(objDt);
                    }
                }

                //Excluding default email account so that user could not remove that
                DvDetails.RowFilter = "ID <> '00000000-0000-0000-0000-000000000000'";

                objDt = DvDetails.ToTable(false, "ID", "EmailAccount");
                grdEmailAccount.DataSource = objDt;
                grdEmailAccount.AllowUserToAddRows = false;
                grdEmailAccount.Columns["ID"].Width = 250;
                grdEmailAccount.Columns["EmailAccount"].Width = 150;
                if (!IsRebind)
                {
                    DataGridViewCheckBoxColumn chkColumn = new DataGridViewCheckBoxColumn();
                    chkColumn.HeaderText = "";
                    chkColumn.Width = 30;
                    chkColumn.Name = "chkSelectAccount";
                    grdEmailAccount.Columns.Insert(0, chkColumn);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error occurred while binding email accounts");
            }
        }
        private bool GetSelectedUserId()
        {
            int i = 0;
            string strAccountID = "";
            bool blIsAnyRowItemSelected = false;
            foreach (DataGridViewRow row in grdEmailAccount.Rows)
            {
                bool IsSelected = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells[0]).Value);
                if (IsSelected)
                {
                    blIsAnyRowItemSelected = true;
                    if (strAccountID == "")
                    {
                        strAccountID = grdEmailAccount.Rows[i].Cells[1].Value.ToString();
                    }
                    else
                    {
                        strAccountID = strAccountID + "," + grdEmailAccount.Rows[i].Cells[1].Value.ToString();
                    }
                }
                i++;
            }
            strAccountIds = strAccountID;
            return blIsAnyRowItemSelected;
        }
        private void DeleteEmailAccount_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form frmAdditionalAccount = Application.OpenForms["AdditionalEmailAccount"];
            if (frmAdditionalAccount != null)
            {
                frmAdditionalAccount.Close();
                AdditionalEmailAccount AdditionalAccount = new AdditionalEmailAccount();
                AdditionalAccount.Show();
            }
        }
    }
}
