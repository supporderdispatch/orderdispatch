﻿namespace ODM.Appliction
{
    partial class GetOrderData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GetOrderData));
            this.grdOrder = new System.Windows.Forms.DataGridView();
            this.chkscanning = new System.Windows.Forms.CheckBox();
            this.chkfirst = new System.Windows.Forms.CheckBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.btnScan = new System.Windows.Forms.Button();
            this.lblerror = new System.Windows.Forms.Label();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbtnUrgent = new System.Windows.Forms.RadioButton();
            this.rbtnAllOrders = new System.Windows.Forms.RadioButton();
            this.chkConsolidateOrderItems = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrder)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdOrder
            // 
            this.grdOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOrder.Location = new System.Drawing.Point(1, 170);
            this.grdOrder.Name = "grdOrder";
            this.grdOrder.Size = new System.Drawing.Size(815, 377);
            this.grdOrder.TabIndex = 5;
            // 
            // chkscanning
            // 
            this.chkscanning.AutoSize = true;
            this.chkscanning.Location = new System.Drawing.Point(26, 97);
            this.chkscanning.Name = "chkscanning";
            this.chkscanning.Size = new System.Drawing.Size(188, 17);
            this.chkscanning.TabIndex = 2;
            this.chkscanning.Text = "Proceed to process after scanning";
            this.chkscanning.UseVisualStyleBackColor = true;
            // 
            // chkfirst
            // 
            this.chkfirst.AutoSize = true;
            this.chkfirst.Location = new System.Drawing.Point(26, 120);
            this.chkfirst.Name = "chkfirst";
            this.chkfirst.Size = new System.Drawing.Size(126, 17);
            this.chkfirst.TabIndex = 3;
            this.chkfirst.Text = "Process first one only";
            this.chkfirst.UseVisualStyleBackColor = true;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(157, 24);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(166, 20);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.TextAlignChanged += new System.EventHandler(this.txtSearch_TextAlignChanged);
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(347, 24);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(79, 21);
            this.btnFind.TabIndex = 1;
            this.btnFind.Text = "Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(326, 116);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(100, 22);
            this.btnScan.TabIndex = 4;
            this.btnScan.Text = "Proceed To Scan";
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // lblerror
            // 
            this.lblerror.AutoSize = true;
            this.lblerror.Location = new System.Drawing.Point(26, 154);
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(0, 13);
            this.lblerror.TabIndex = 10;
            this.lblerror.Visible = false;
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.Location = new System.Drawing.Point(26, 24);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(115, 13);
            this.lblBarcode.TabIndex = 11;
            this.lblBarcode.Text = "Enter Barcode Number";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbtnUrgent);
            this.panel1.Controls.Add(this.rbtnAllOrders);
            this.panel1.Location = new System.Drawing.Point(12, 62);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(339, 29);
            this.panel1.TabIndex = 12;
            // 
            // rbtnUrgent
            // 
            this.rbtnUrgent.AutoSize = true;
            this.rbtnUrgent.Location = new System.Drawing.Point(145, 3);
            this.rbtnUrgent.Name = "rbtnUrgent";
            this.rbtnUrgent.Size = new System.Drawing.Size(160, 17);
            this.rbtnUrgent.TabIndex = 15;
            this.rbtnUrgent.TabStop = true;
            this.rbtnUrgent.Text = "Dispatch Only Urgent Orders";
            this.rbtnUrgent.UseVisualStyleBackColor = true;
            this.rbtnUrgent.CheckedChanged += new System.EventHandler(this.rbtnUrgent_CheckedChanged);
            // 
            // rbtnAllOrders
            // 
            this.rbtnAllOrders.AutoSize = true;
            this.rbtnAllOrders.Location = new System.Drawing.Point(14, 3);
            this.rbtnAllOrders.Name = "rbtnAllOrders";
            this.rbtnAllOrders.Size = new System.Drawing.Size(115, 17);
            this.rbtnAllOrders.TabIndex = 14;
            this.rbtnAllOrders.TabStop = true;
            this.rbtnAllOrders.Text = "Dispatch All Orders";
            this.rbtnAllOrders.UseVisualStyleBackColor = true;
            this.rbtnAllOrders.CheckedChanged += new System.EventHandler(this.rbtnAllOrders_CheckedChanged);
            // 
            // chkConsolidateOrderItems
            // 
            this.chkConsolidateOrderItems.AutoSize = true;
            this.chkConsolidateOrderItems.Location = new System.Drawing.Point(592, 120);
            this.chkConsolidateOrderItems.Name = "chkConsolidateOrderItems";
            this.chkConsolidateOrderItems.Size = new System.Drawing.Size(138, 17);
            this.chkConsolidateOrderItems.TabIndex = 13;
            this.chkConsolidateOrderItems.Text = "Consolidate Order Items";
            this.chkConsolidateOrderItems.UseVisualStyleBackColor = true;
            // 
            // GetOrderData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 547);
            this.Controls.Add(this.chkConsolidateOrderItems);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblBarcode);
            this.Controls.Add(this.lblerror);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.chkfirst);
            this.Controls.Add(this.chkscanning);
            this.Controls.Add(this.grdOrder);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "GetOrderData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Get Order Data";
            ((System.ComponentModel.ISupportInitialize)(this.grdOrder)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdOrder;
        private System.Windows.Forms.CheckBox chkscanning;
        private System.Windows.Forms.CheckBox chkfirst;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Label lblerror;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbtnUrgent;
        private System.Windows.Forms.RadioButton rbtnAllOrders;
        private System.Windows.Forms.CheckBox chkConsolidateOrderItems;
    }
}