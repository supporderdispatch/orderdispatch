﻿namespace ODM.Appliction
{
    partial class Scan_Items
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Scan_Items));
            this.txtBarCodeSearch = new System.Windows.Forms.TextBox();
            this.btnScan = new System.Windows.Forms.Button();
            this.grdScan = new System.Windows.Forms.DataGridView();
            this.btndispatch = new System.Windows.Forms.Button();
            this.lblmessage = new System.Windows.Forms.Label();
            this.btnback = new System.Windows.Forms.Button();
            this.grdOrderItemDetails = new System.Windows.Forms.DataGridView();
            this.lblScannedItemStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderItemDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBarCodeSearch
            // 
            this.txtBarCodeSearch.Location = new System.Drawing.Point(104, 13);
            this.txtBarCodeSearch.Name = "txtBarCodeSearch";
            this.txtBarCodeSearch.Size = new System.Drawing.Size(218, 20);
            this.txtBarCodeSearch.TabIndex = 0;
            this.txtBarCodeSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarCodeSearch_KeyPress);
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(12, 10);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(75, 23);
            this.btnScan.TabIndex = 1;
            this.btnScan.Text = "Scan";
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // grdScan
            // 
            this.grdScan.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grdScan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdScan.Location = new System.Drawing.Point(0, 204);
            this.grdScan.Name = "grdScan";
            this.grdScan.Size = new System.Drawing.Size(556, 363);
            this.grdScan.TabIndex = 4;
            // 
            // btndispatch
            // 
            this.btndispatch.Location = new System.Drawing.Point(351, 11);
            this.btndispatch.Name = "btndispatch";
            this.btndispatch.Size = new System.Drawing.Size(75, 23);
            this.btndispatch.TabIndex = 2;
            this.btndispatch.Text = "Dispatch";
            this.btndispatch.UseVisualStyleBackColor = true;
            this.btndispatch.Click += new System.EventHandler(this.btndispatch_Click);
            // 
            // lblmessage
            // 
            this.lblmessage.AutoSize = true;
            this.lblmessage.Location = new System.Drawing.Point(29, 74);
            this.lblmessage.Name = "lblmessage";
            this.lblmessage.Size = new System.Drawing.Size(0, 13);
            this.lblmessage.TabIndex = 5;
            // 
            // btnback
            // 
            this.btnback.Location = new System.Drawing.Point(440, 10);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(79, 23);
            this.btnback.TabIndex = 3;
            this.btnback.Text = "Back";
            this.btnback.UseVisualStyleBackColor = true;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // grdOrderItemDetails
            // 
            this.grdOrderItemDetails.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grdOrderItemDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdOrderItemDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOrderItemDetails.Location = new System.Drawing.Point(103, 46);
            this.grdOrderItemDetails.Name = "grdOrderItemDetails";
            this.grdOrderItemDetails.Size = new System.Drawing.Size(297, 127);
            this.grdOrderItemDetails.TabIndex = 6;
            // 
            // lblScannedItemStatus
            // 
            this.lblScannedItemStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblScannedItemStatus.Location = new System.Drawing.Point(297, 176);
            this.lblScannedItemStatus.Name = "lblScannedItemStatus";
            this.lblScannedItemStatus.Size = new System.Drawing.Size(99, 23);
            this.lblScannedItemStatus.TabIndex = 7;
            this.lblScannedItemStatus.Text = "Total : ";
            // 
            // Scan_Items
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 579);
            this.ControlBox = false;
            this.Controls.Add(this.lblScannedItemStatus);
            this.Controls.Add(this.grdOrderItemDetails);
            this.Controls.Add(this.btnback);
            this.Controls.Add(this.lblmessage);
            this.Controls.Add(this.btndispatch);
            this.Controls.Add(this.grdScan);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.txtBarCodeSearch);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Scan_Items";
            this.Text = "Scan Items";
            this.Load += new System.EventHandler(this.Scan_Items_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrderItemDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtBarCodeSearch;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.DataGridView grdScan;
        private System.Windows.Forms.Button btndispatch;
        private System.Windows.Forms.Label lblmessage;
        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.DataGridView grdOrderItemDetails;
        private System.Windows.Forms.Label lblScannedItemStatus;
      
    }
}