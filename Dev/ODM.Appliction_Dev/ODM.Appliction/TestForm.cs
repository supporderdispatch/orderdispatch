﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class TestForm : Form
    {
        string strIsMailOrSubjectCall = "";
        public TestForm()
        {
            InitializeComponent();
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            CreateAndAttachMenuStrip();
        }
        private void CreateAndAttachMenuStrip()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetDataAndColumnNamesForMenu");
            //this.IsMdiContainer = true;

            //MenuStrip objMenu2 = new MenuStrip();
            //objMenu2.Name = "MenuOD2";
            //objMenu2.Text = "Make Selections";
            //objMenu2.Dock = DockStyle.None;
            //objMenu2.Location = new Point(1, 50);
            //string[] row = new string[] { "File", "Edit", "View", "Refactor", "Project" };
            //string[] rows = new string[] { "New", "Open", "Add", "Close", "Close Solution" };
            //foreach (string rw in row)
            //{
            //    ToolStripMenuItem MnuStripItem = new ToolStripMenuItem(rw);
            //    objMenu2.Items.Add(MnuStripItem);
            //    //foreach (string var in row)
            //    //{
            //    if (rw == "File")
            //    {
            //        foreach (string rws in rows)
            //        {
            //            ToolStripMenuItem SSMenu = new ToolStripMenuItem(rws, null, ChildClick);
            //            // SubMenu(SSMenu,rw);  I have included this piece of code to add a Sub-Menu to the New Menu
            //            MnuStripItem.DropDownItems.Add(SSMenu);
            //            if (rws == "New")
            //            {
            //                string[] rowss = new string[] { "Project", "Web Site", "File..", "Project From Existing Code" };
            //                foreach (string rw2 in rowss)
            //                {
            //                    ToolStripMenuItem SSSMenu = new ToolStripMenuItem(rw2, null, ChildClick);
            //                    SSMenu.DropDownItems.Add(SSSMenu);
            //                }
            //            }
            //        }
            //        //}
            //    }
            //}

            ////ToolStripMenuItem MenuStripItem = new ToolStripMenuItem("Order Dispatch");
            ////objMenu.drop.Add(MenuStripItem);

            ////this.MainMenuStrip = objMenu;
            //this.Controls.Add(objMenu2);

            MenuStrip objMenu = new MenuStrip();
            objMenu.Name = "MenuOD";
            objMenu.Text = "Make Selection";
            objMenu.Dock = DockStyle.None;
            objMenu.Location = new Point(10, 130);

            ToolStripMenuItem MnuStripItemMain = new ToolStripMenuItem("Choose Column");
            objMenu.Items.Add(MnuStripItemMain);
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    DataTable objDt = new DataTable();
                    objDt = ds.Tables[i];
                    string strTableName = Convert.ToString(objDt.Rows[i]["Table_Name"]);

                    ToolStripMenuItem MenuTable = new ToolStripMenuItem(strTableName);
                    MnuStripItemMain.DropDownItems.Add(MenuTable);

                    foreach (DataRow dr in objDt.Rows)
                    {
                        ToolStripMenuItem MenuColumnName = new ToolStripMenuItem(dr["Column_Name"].ToString(), null, ChildClick);
                        MenuTable.DropDownItems.Add(MenuColumnName);
                    }
                }
            }
            this.Controls.Add(objMenu);
        }
        private void ChildClick(object sender, EventArgs e)
        {
            if (strIsMailOrSubjectCall == "MB")
            {
                int intSelection = txtTemplate.SelectionStart;
                string stringToInsert = "Helloz";
                txtTemplate.Text = txtTemplate.Text.Insert(intSelection, stringToInsert);
                txtTemplate.SelectionStart = intSelection + stringToInsert.Length;
            }
            else if (strIsMailOrSubjectCall == "SUB")
            {
                int intSelection = textBox1.SelectionStart;
                string stringToInsert = "Helloz";
                textBox1.Text = textBox1.Text.Insert(textBox1.SelectionStart, stringToInsert);
                textBox1.SelectionStart = intSelection + stringToInsert.Length;
            }
        }

        private void txtTemplate_Enter(object sender, EventArgs e)
        {
            strIsMailOrSubjectCall = "MB";
        }

        private void txtTemplate_Leave(object sender, EventArgs e)
        {
            strIsMailOrSubjectCall = "";
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            strIsMailOrSubjectCall = "SUB";
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            strIsMailOrSubjectCall = "";
        }
    }
}
