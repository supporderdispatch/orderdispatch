﻿using ODM.Data.DbContext;
using ODM.Entities;
using ODM.Entities.InvoiceTemplate;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction.Customers
{
    public partial class AddCustomer : Form
    {
        private ODM.Entities.FulFilmentCustomer Customer;

        public AddCustomer()
        {
            InitializeComponent();
            InitilizeAddNewCustomer();
            PrepareConditionGrid();
        }

        public AddCustomer(string customerId)
        {
            InitializeComponent();
            InitializeForCustomer(Guid.Parse(customerId));
            PrepareConditionGrid();
        }

        private void InitializeForCustomer(Guid customerId)
        {
            Customer = ODM.Core.FulFilmenCustomers.GetInstance().GetCustomer(customerId);
            txtCustomerName.Text = Customer.Name;
            txtCompany.Text = Customer.Company;
            txtUserName.Text = Customer.UserName;
            txtPassword.Text = Customer.Password;
        }


        private void InitilizeAddNewCustomer()
        {
            Customer = new Entities.FulFilmentCustomer();
            Customer.CustomerId = Guid.NewGuid();
            Customer.ConditionId = Guid.NewGuid();
        }

        private void PrepareConditionGrid()
        {
            RegisterEvents();
            InitializeGridDisplay();

            grdConditions.Visible = false;
            grdConditions.DataSource = ODM.Core.FulFilmenCustomers.GetInstance().GetCustomerConditions(Customer.ConditionId);
            grdConditions.Refresh();

            grdConditions.Columns["ConditionId"].Visible = false;
            grdConditions.Columns["TableName"].Visible = false;
            grdConditions.Columns["ColumnName"].Visible = false;
            grdConditions.Columns["Creterion"].Visible = false;
            grdConditions.Columns["CriteriaDataType"].Visible = false;

            DataGridViewComboBoxColumn table = new DataGridViewComboBoxColumn();
            table.HeaderText = "Table";
            table.Name = "Table";
            table.DataSource = GetTables();
            table.DataPropertyName = "TableName";
            table.ValueMember = "Table";
            table.DefaultCellStyle.NullValue = "OrderItem";
            table.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            table.FlatStyle = FlatStyle.Flat;
            grdConditions.Columns.Insert(0,table);

            DataGridViewComboBoxColumn column = new DataGridViewComboBoxColumn();
            column.HeaderText = "Column";
            column.Name = "Column";
            column.DataSource = GetAllColumns();
            column.FlatStyle = FlatStyle.Flat;
            column.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            column.DataPropertyName = "ColumnName";
            column.DefaultCellStyle.NullValue = "SKU";
            grdConditions.Columns.Insert(1,column);

            DataGridViewComboBoxColumn criteria = new DataGridViewComboBoxColumn();
            criteria.HeaderText = "criteria";
            criteria.Name = "criteria";
            criteria.FlatStyle = FlatStyle.Flat;
            criteria.DataSource = new string[] {"like","equalTo","In" }; 
            criteria.DataPropertyName = "Creterion";
            grdConditions.Columns.Insert(2,criteria);

            grdConditions.Columns["CompareWith"].DisplayIndex = 3;

            DataGridViewComboBoxColumn criteriaDataType = new DataGridViewComboBoxColumn();
            criteriaDataType.HeaderText = "criteriaDataType";
            criteriaDataType.Name = "criteriaDataType";
            criteriaDataType.FlatStyle = FlatStyle.Flat;
            criteriaDataType.DataSource = Enum.GetNames(typeof(CustomDataTypes));
            criteriaDataType.DataPropertyName = "CriteriaDataType";
            criteriaDataType.DefaultCellStyle.NullValue = "Text";
            grdConditions.Columns.Insert(4,criteriaDataType);


            DataGridViewButtonColumn delete = new DataGridViewButtonColumn();
            delete.HeaderText = "";
            delete.Text = "Delete";
            delete.UseColumnTextForButtonValue = true;
            delete.Name = "Delete";
            delete.FlatStyle = FlatStyle.Flat;
            delete.DefaultCellStyle.ForeColor = Color.Black;
            grdConditions.Columns.Insert(5,delete);


            grdConditions.AllowUserToAddRows = false;
            grdConditions.ClearSelection();
            grdConditions.Visible = true;

            grdConditions.Refresh();

        }

        private void InitializeGridDisplay()
        {
            grdConditions.DoubleBuffered(true);
            grdConditions.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            grdConditions.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            grdConditions.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            grdConditions.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            grdConditions.CellBorderStyle = DataGridViewCellBorderStyle.None;
            grdConditions.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdConditions.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdConditions.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdConditions.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdConditions.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdConditions.ScrollBars = ScrollBars.Both;
            grdConditions.RowHeadersVisible = false;
            grdConditions.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdConditions.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdConditions.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdConditions.EnableHeadersVisualStyles = false;
            grdConditions.DefaultCellStyle.SelectionBackColor = SystemColors.ButtonFace;
            grdConditions.DefaultCellStyle.SelectionForeColor = Color.Black;
            grdConditions.DefaultCellStyle.BackColor = SystemColors.ButtonFace;
            grdConditions.DefaultCellStyle.Padding = new Padding(5, 5, 5, 5);
            grdConditions.CellBorderStyle = DataGridViewCellBorderStyle.RaisedHorizontal;
            grdConditions.AllowUserToAddRows = false;
            grdConditions.EditMode = DataGridViewEditMode.EditOnEnter;
        }

        private void RegisterEvents()
        {
            grdConditions.EditingControlShowing+=grdConditions_EditingControlShowing;
            grdConditions.DataError += grdConditions_DataError;
            grdConditions.CellContentClick+=grdConditions_CellContentClick;
        }

        private void grdConditions_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (grdConditions.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("Delete"))
                {
                    grdConditions.Rows.Remove(grdConditions.Rows[e.RowIndex]);
                }
            }
        }

        void grdConditions_DataError(object sender, DataGridViewDataErrorEventArgs e){}

        private void grdConditions_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.CellStyle.BackColor = SystemColors.ButtonFace;
            ComboBox combo = e.Control as ComboBox;
            if (combo != null)
            {
                combo.SelectedIndexChanged -=
                    new EventHandler(ComboBox_SelectedIndexChanged);

                combo.SelectedIndexChanged +=
                    new EventHandler(ComboBox_SelectedIndexChanged);
            }
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cell = sender as ComboBox;
            if (GetTables().AsEnumerable().Select(x => x.Field<string>("Table")).ToList().Contains(cell.Text))
            {

                var gridCel = grdConditions.CurrentRow.Cells["Column"];
                var gridCell = gridCel as DataGridViewComboBoxCell;
                gridCell.DataSource = GetColumns(cell.Text);
            }
            grdConditions.Refresh();
        }

        private object GetColumns(string tableName)
        {
            return new string[] { "SKU" };
        }

        private object GetAllColumns()
        {
            return new string[] {"SKU"};
        }

        private DataTable GetTables()
        {
            var tables = new DataTable();
            tables.Columns.Add("Table");
            foreach (var tableName in typeof(VirtualTables).GetProperties())
            {
                if(tableName.Name.ToLower().Contains("item"))
                    tables.Rows.Add(tableName.Name);
            }
            return tables;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveCustomer();
            SaveCustomerConditions();
            Close();
        }

        private void SaveCustomerConditions()
        {
            grdConditions.EndEdit();
            var ConditionCriterias = new List<ConditionCriteria>();
            foreach (DataGridViewRow row in grdConditions.Rows)
            {
                try
                {
                    var CriteriaDatatype=row.Cells["criteriaDataType"].Value;
                    CriteriaDatatype = CriteriaDatatype.GetType().Equals(typeof(DBNull)) ? string.Empty : CriteriaDatatype;

                    var conditionCriteria = new ConditionCriteria();
                    conditionCriteria.ConditionId = Customer.ConditionId;
                    conditionCriteria.TableName = "OrderItem";
                    conditionCriteria.ColumnName = "SKU";
                    conditionCriteria.Creterion = (string)row.Cells["criteria"].Value ?? string.Empty;
                    conditionCriteria.CompareWith = (string)row.Cells["CompareWith"].Value ?? string.Empty;
                    conditionCriteria.CriteriaDataType = (string) CriteriaDatatype?? string.Empty;
                    ConditionCriterias.Add(conditionCriteria);
                }
                catch 
                {
                    MessageBox.Show("Please fill all fields");
                    return;
                }
            }
            ODM.Core.FulFilmenCustomers.GetInstance().SaveCondition(ConditionCriterias, Customer.ConditionId);
        }

        private void SaveCustomer()
        {
            Customer.Name = txtCustomerName.Text;
            Customer.Company = txtCompany.Text;
            Customer.UserName = txtUserName.Text;
            Customer.Password = txtPassword.Text;
            ODM.Core.FulFilmenCustomers.GetInstance().SaveCustomer(Customer);
        }

        private void btnAddCriteria_Click(object sender, EventArgs e)
        {
            var dataSource = (DataTable)grdConditions.DataSource;
            dataSource.Rows.Add(dataSource.NewRow());
            grdConditions.DataSource = dataSource;
        }
    }
}
