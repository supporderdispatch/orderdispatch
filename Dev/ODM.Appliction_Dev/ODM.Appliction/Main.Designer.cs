﻿namespace ODM.Appliction
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.dispatchOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteUsersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rootToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userStatisticsReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerFeaturesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderBookToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.allOpenOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderProcessStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewOrderLogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.urgentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.courierIntegrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailSettingsNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.channelIntegrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblAppVersion = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblAvgPerOrderTimeTaken = new System.Windows.Forms.Label();
            this.lblOrderPerHourRate = new System.Windows.Forms.Label();
            this.lblTotalOrdersProcessed = new System.Windows.Forms.Label();
            this.lblTotalLoggedInTime = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDateOfLog = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dashboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dispatchOrderToolStripMenuItem,
            this.usersToolStripMenuItem,
            this.orderBookToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(454, 24);
            this.mnuMain.TabIndex = 1;
            this.mnuMain.Text = "menuStrip1";
            // 
            // dispatchOrderToolStripMenuItem
            // 
            this.dispatchOrderToolStripMenuItem.Name = "dispatchOrderToolStripMenuItem";
            this.dispatchOrderToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.dispatchOrderToolStripMenuItem.Text = "Dispatch Order";
            this.dispatchOrderToolStripMenuItem.Click += new System.EventHandler(this.dispatchOrderToolStripMenuItem_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.changePasswordToolStripMenuItem,
            this.deleteUsersToolStripMenuItem,
            this.editUserToolStripMenuItem,
            this.rootToolStripMenuItem});
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.usersToolStripMenuItem.Text = "Users";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.changePasswordToolStripMenuItem.Text = "Change Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // deleteUsersToolStripMenuItem
            // 
            this.deleteUsersToolStripMenuItem.Name = "deleteUsersToolStripMenuItem";
            this.deleteUsersToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.deleteUsersToolStripMenuItem.Text = "Delete User";
            this.deleteUsersToolStripMenuItem.Click += new System.EventHandler(this.deleteUsersToolStripMenuItem_Click);
            // 
            // editUserToolStripMenuItem
            // 
            this.editUserToolStripMenuItem.Name = "editUserToolStripMenuItem";
            this.editUserToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.editUserToolStripMenuItem.Text = "Edit User";
            this.editUserToolStripMenuItem.Click += new System.EventHandler(this.editUserToolStripMenuItem_Click);
            // 
            // rootToolStripMenuItem
            // 
            this.rootToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userStatisticsReportToolStripMenuItem,
            this.updateManagerToolStripMenuItem,
            this.customerFeaturesToolStripMenuItem});
            this.rootToolStripMenuItem.Name = "rootToolStripMenuItem";
            this.rootToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.rootToolStripMenuItem.Text = "Root";
            this.rootToolStripMenuItem.Visible = false;
            // 
            // userStatisticsReportToolStripMenuItem
            // 
            this.userStatisticsReportToolStripMenuItem.Name = "userStatisticsReportToolStripMenuItem";
            this.userStatisticsReportToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.userStatisticsReportToolStripMenuItem.Text = "User Statistics Report";
            this.userStatisticsReportToolStripMenuItem.Click += new System.EventHandler(this.userStatisticsReportToolStripMenuItem_Click);
            // 
            // updateManagerToolStripMenuItem
            // 
            this.updateManagerToolStripMenuItem.Name = "updateManagerToolStripMenuItem";
            this.updateManagerToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.updateManagerToolStripMenuItem.Text = "Update Manager";
            this.updateManagerToolStripMenuItem.Click += new System.EventHandler(this.updateManagerToolStripMenuItem_Click);
            // 
            // customerFeaturesToolStripMenuItem
            // 
            this.customerFeaturesToolStripMenuItem.Name = "customerFeaturesToolStripMenuItem";
            this.customerFeaturesToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.customerFeaturesToolStripMenuItem.Text = "Customer Details";
            this.customerFeaturesToolStripMenuItem.Click += new System.EventHandler(this.customerFeaturesToolStripMenuItem_Click);
            // 
            // orderBookToolStripMenuItem
            // 
            this.orderBookToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dashboardToolStripMenuItem,
            this.orderBookToolStripMenuItem1,
            this.allOpenOrdersToolStripMenuItem,
            this.orderProcessStatusToolStripMenuItem,
            this.viewOrderLogsToolStripMenuItem});
            this.orderBookToolStripMenuItem.Name = "orderBookToolStripMenuItem";
            this.orderBookToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.orderBookToolStripMenuItem.Text = "Orders";
            // 
            // orderBookToolStripMenuItem1
            // 
            this.orderBookToolStripMenuItem1.Name = "orderBookToolStripMenuItem1";
            this.orderBookToolStripMenuItem1.Size = new System.Drawing.Size(182, 22);
            this.orderBookToolStripMenuItem1.Text = "Order Book";
            this.orderBookToolStripMenuItem1.Click += new System.EventHandler(this.orderBookToolStripMenuItem1_Click);
            // 
            // allOpenOrdersToolStripMenuItem
            // 
            this.allOpenOrdersToolStripMenuItem.Name = "allOpenOrdersToolStripMenuItem";
            this.allOpenOrdersToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.allOpenOrdersToolStripMenuItem.Text = "All Open Orders";
            this.allOpenOrdersToolStripMenuItem.Click += new System.EventHandler(this.allOpenOrdersToolStripMenuItem_Click);
            // 
            // orderProcessStatusToolStripMenuItem
            // 
            this.orderProcessStatusToolStripMenuItem.Name = "orderProcessStatusToolStripMenuItem";
            this.orderProcessStatusToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.orderProcessStatusToolStripMenuItem.Text = "Order Process Status";
            this.orderProcessStatusToolStripMenuItem.Click += new System.EventHandler(this.orderProcessStatusToolStripMenuItem_Click);
            // 
            // viewOrderLogsToolStripMenuItem
            // 
            this.viewOrderLogsToolStripMenuItem.Name = "viewOrderLogsToolStripMenuItem";
            this.viewOrderLogsToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.viewOrderLogsToolStripMenuItem.Text = "View Order Logs";
            this.viewOrderLogsToolStripMenuItem.Click += new System.EventHandler(this.viewOrderLogsToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.urgentToolStripMenuItem,
            this.courierIntegrationToolStripMenuItem,
            this.generalSettingsToolStripMenuItem,
            this.emailSettingsNewToolStripMenuItem,
            this.channelIntegrationToolStripMenuItem,
            this.invoiceSettingsToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // urgentToolStripMenuItem
            // 
            this.urgentToolStripMenuItem.Name = "urgentToolStripMenuItem";
            this.urgentToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.urgentToolStripMenuItem.Text = "Urgent Order Services";
            this.urgentToolStripMenuItem.Click += new System.EventHandler(this.urgentToolStripMenuItem_Click);
            // 
            // courierIntegrationToolStripMenuItem
            // 
            this.courierIntegrationToolStripMenuItem.Name = "courierIntegrationToolStripMenuItem";
            this.courierIntegrationToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.courierIntegrationToolStripMenuItem.Text = "Courier Integration";
            this.courierIntegrationToolStripMenuItem.Click += new System.EventHandler(this.courierIntegrationToolStripMenuItem_Click);
            // 
            // generalSettingsToolStripMenuItem
            // 
            this.generalSettingsToolStripMenuItem.Name = "generalSettingsToolStripMenuItem";
            this.generalSettingsToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.generalSettingsToolStripMenuItem.Text = "General Settings";
            this.generalSettingsToolStripMenuItem.Click += new System.EventHandler(this.generalSettingsToolStripMenuItem_Click);
            // 
            // emailSettingsNewToolStripMenuItem
            // 
            this.emailSettingsNewToolStripMenuItem.Name = "emailSettingsNewToolStripMenuItem";
            this.emailSettingsNewToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.emailSettingsNewToolStripMenuItem.Text = "Email Settings";
            this.emailSettingsNewToolStripMenuItem.Click += new System.EventHandler(this.emailSettingsNewToolStripMenuItem_Click);
            // 
            // channelIntegrationToolStripMenuItem
            // 
            this.channelIntegrationToolStripMenuItem.Name = "channelIntegrationToolStripMenuItem";
            this.channelIntegrationToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.channelIntegrationToolStripMenuItem.Text = "Channel Integration";
            this.channelIntegrationToolStripMenuItem.Click += new System.EventHandler(this.channelIntegrationToolStripMenuItem_Click);
            // 
            // invoiceSettingsToolStripMenuItem
            // 
            this.invoiceSettingsToolStripMenuItem.Name = "invoiceSettingsToolStripMenuItem";
            this.invoiceSettingsToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.invoiceSettingsToolStripMenuItem.Text = "Invoice Settings";
            this.invoiceSettingsToolStripMenuItem.Click += new System.EventHandler(this.invoiceSettingsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.lblAppVersion);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lblAvgPerOrderTimeTaken);
            this.panel1.Controls.Add(this.lblOrderPerHourRate);
            this.panel1.Controls.Add(this.lblTotalOrdersProcessed);
            this.panel1.Controls.Add(this.lblTotalLoggedInTime);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblDateOfLog);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(454, 398);
            this.panel1.TabIndex = 3;
            // 
            // lblAppVersion
            // 
            this.lblAppVersion.AutoSize = true;
            this.lblAppVersion.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblAppVersion.Location = new System.Drawing.Point(397, 377);
            this.lblAppVersion.Name = "lblAppVersion";
            this.lblAppVersion.Size = new System.Drawing.Size(52, 13);
            this.lblAppVersion.TabIndex = 16;
            this.lblAppVersion.Text = "v1.00.0.0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(82, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "(hh:mm:ss)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(372, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "(min)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(289, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "(min)";
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(68, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(386, 1);
            this.label5.TabIndex = 12;
            this.label5.Text = "Statistics";
            // 
            // lblAvgPerOrderTimeTaken
            // 
            this.lblAvgPerOrderTimeTaken.AutoSize = true;
            this.lblAvgPerOrderTimeTaken.Location = new System.Drawing.Point(372, 150);
            this.lblAvgPerOrderTimeTaken.Name = "lblAvgPerOrderTimeTaken";
            this.lblAvgPerOrderTimeTaken.Size = new System.Drawing.Size(46, 13);
            this.lblAvgPerOrderTimeTaken.TabIndex = 11;
            this.lblAvgPerOrderTimeTaken.Text = "lblAPOT";
            // 
            // lblOrderPerHourRate
            // 
            this.lblOrderPerHourRate.AutoSize = true;
            this.lblOrderPerHourRate.Location = new System.Drawing.Point(290, 150);
            this.lblOrderPerHourRate.Name = "lblOrderPerHourRate";
            this.lblOrderPerHourRate.Size = new System.Drawing.Size(48, 13);
            this.lblOrderPerHourRate.TabIndex = 10;
            this.lblOrderPerHourRate.Text = "lblOPHR";
            // 
            // lblTotalOrdersProcessed
            // 
            this.lblTotalOrdersProcessed.AutoSize = true;
            this.lblTotalOrdersProcessed.Location = new System.Drawing.Point(192, 150);
            this.lblTotalOrdersProcessed.Name = "lblTotalOrdersProcessed";
            this.lblTotalOrdersProcessed.Size = new System.Drawing.Size(32, 13);
            this.lblTotalOrdersProcessed.TabIndex = 9;
            this.lblTotalOrdersProcessed.Text = "lblOP";
            // 
            // lblTotalLoggedInTime
            // 
            this.lblTotalLoggedInTime.AutoSize = true;
            this.lblTotalLoggedInTime.Location = new System.Drawing.Point(84, 150);
            this.lblTotalLoggedInTime.Name = "lblTotalLoggedInTime";
            this.lblTotalLoggedInTime.Size = new System.Drawing.Size(40, 13);
            this.lblTotalLoggedInTime.TabIndex = 8;
            this.lblTotalLoggedInTime.Text = "lblTLIT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(351, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Avg Per Order Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(259, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Order/Hour Rate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Orders Processed";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Logged In Time";
            // 
            // lblDateOfLog
            // 
            this.lblDateOfLog.AutoSize = true;
            this.lblDateOfLog.Location = new System.Drawing.Point(3, 150);
            this.lblDateOfLog.Name = "lblDateOfLog";
            this.lblDateOfLog.Size = new System.Drawing.Size(39, 13);
            this.lblDateOfLog.TabIndex = 2;
            this.lblDateOfLog.Text = "lblDOL";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(355, 41);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(70, 15);
            this.lblUserName.TabIndex = 1;
            this.lblUserName.Text = "User Name";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(358, 14);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(33, 36);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // dashboardToolStripMenuItem
            // 
            this.dashboardToolStripMenuItem.Name = "dashboardToolStripMenuItem";
            this.dashboardToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.dashboardToolStripMenuItem.Text = "Dashboard";
            this.dashboardToolStripMenuItem.Click += new System.EventHandler(this.dashboardToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(454, 424);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.mnuMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mnuMain;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order Dispatch";
            this.Load += new System.EventHandler(this.Main_Load);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem dispatchOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteUsersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem urgentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem courierIntegrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generalSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderBookToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem allOpenOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderProcessStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewOrderLogsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem channelIntegrationToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblDateOfLog;
        private System.Windows.Forms.Label lblAvgPerOrderTimeTaken;
        private System.Windows.Forms.Label lblOrderPerHourRate;
        private System.Windows.Forms.Label lblTotalOrdersProcessed;
        private System.Windows.Forms.Label lblTotalLoggedInTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblLine;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripMenuItem rootToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userStatisticsReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invoiceSettingsToolStripMenuItem;
        private System.Windows.Forms.Label lblAppVersion;
        private System.Windows.Forms.ToolStripMenuItem emailSettingsNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerFeaturesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dashboardToolStripMenuItem;
    }
}