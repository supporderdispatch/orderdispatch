﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class ViewLogHistory : Form
    {
        #region "Global Variables"
        string strSearchText = string.Empty;
        string strSearchMode = string.Empty;
        string strLogIds = string.Empty;
        string strLogID = string.Empty;
        string strOrderNumberID = string.Empty;
        string strLoggedUser = string.Empty;
        string strFromDate = string.Empty;
        string strToDate = string.Empty;
        bool blHasCheckboxAdded = false;
        DataView dvLogDetails = new DataView();
        #endregion

        #region "Constructors"
        public ViewLogHistory()
        {
            InitializeComponent();
            dtFrom.Format = DateTimePickerFormat.Custom;
            dtFrom.CustomFormat = "dd-MM-yyyy";

            dtTo.Format = DateTimePickerFormat.Custom;
            dtTo.CustomFormat = "dd-MM-yyyy";
        }
        #endregion

        #region "Events"
        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchLogs();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteLogs();
        }
        private void btnErrorLogs_Click(object sender, EventArgs e)
        {
            strSearchMode = "ERROR"; txtOrderId.Text = "";
            if (chkFromDate.Checked || chkToDate.Checked)
            {
                DialogResult ConfirmDateFilter = MessageBox.Show("Filter error logs by date?", "Filter Error Logs", MessageBoxButtons.YesNo);
                if (ConfirmDateFilter.Equals(DialogResult.Yes))
                {
                    if (ValidateFromAndToDate(true))
                        BindGrid();
                    else
                    {
                        grdViewLogHistory.Hide(); lblMsg.Show();
                        lblRecordCount.Text = "Total Records : 0";
                    }
                }
                else
                {
                    BindGrid();
                }
            }
            else
            {
                BindGrid();
            }
        }
        private void txtOrderId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                SearchLogs();
        }
        private void btnSearchByDate_Click(object sender, EventArgs e)
        {
            if (ValidateFromAndToDate(false)) { BindGrid(); }
            else
            {
                grdViewLogHistory.Hide(); lblMsg.Show();
                lblRecordCount.Text = "Total Records : 0";
            }
        }
        private void chkFromDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFromDate.Checked)
                dtFrom.Enabled = true;
            else
                dtFrom.Enabled = false;
        }
        private void chkToDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkToDate.Checked)
                dtTo.Enabled = true;
            else
                dtTo.Enabled = false;
        }
        #region "Context Menu"
        private void grdViewLogHistory_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int intSelectedRowIndex = grdViewLogHistory.HitTest(e.X, e.Y).RowIndex;
                int intCount = 0;
                if (intSelectedRowIndex >= 0)
                {
                    strLogID = grdViewLogHistory.Rows[intSelectedRowIndex].Cells["ID"].Value.ToString();
                    strOrderNumberID = grdViewLogHistory.Rows[intSelectedRowIndex].Cells["NumOrderId"].Value.ToString();
                    strLoggedUser = grdViewLogHistory.Rows[intSelectedRowIndex].Cells["LoggedByName"].Value.ToString();
                    foreach (DataGridViewRow row in grdViewLogHistory.Rows)
                    {
                        if (intCount == intSelectedRowIndex)
                        { row.Selected = true; }
                        else { row.Selected = false; }
                        intCount++;
                    }
                    ContextMenu cm = new ContextMenu();
                    cm.MenuItems.Add(new MenuItem("Filter All Logs Of Order " + strOrderNumberID, FindLogsByOrderId_click));
                    cm.MenuItems.Add(new MenuItem("All Logs Logged By " + strLoggedUser, FindLogsByUserName_click));
                    cm.MenuItems.Add(new MenuItem("View Full Detail", GetLogDetails_click));
                    cm.Show(grdViewLogHistory, new Point(e.X, e.Y));
                }
            }
        }
        private void FindLogsByOrderId_click(object sender, EventArgs e)
        {
            strSearchText = strOrderNumberID;
            strSearchMode = "ID";
            BindGrid();
        }
        private void FindLogsByUserName_click(object sender, EventArgs e)
        {
            strSearchText = strLoggedUser;
            strSearchMode = "USER";
            BindGrid();
        }
        private void GetLogDetails_click(object sender, EventArgs e)
        {
            dvLogDetails.RowFilter = "ID=" + "'" + strLogID + "'";
            Form frmLogDetails = Application.OpenForms["ViewLogDetail"];
            if (frmLogDetails != null)
            {
                frmLogDetails.Close();
                ViewLogDetail objViewLog = new ViewLogDetail(dvLogDetails.ToTable());
                objViewLog.Show();
            }
            else
            {
                ViewLogDetail objViewLog = new ViewLogDetail(dvLogDetails.ToTable());
                objViewLog.Show();
            }
        }
        private void grdViewLogHistory_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewRow row = grdViewLogHistory.Rows[e.RowIndex];
            string strRowData = Convert.ToString(row.Cells["LogText"].Value).ToUpper();
            if (strRowData.Contains("ERROR") || strRowData.Contains("EXCEPTION") || strRowData.Contains("BADREQUEST") || strRowData.Contains("WRONG"))
                row.DefaultCellStyle.BackColor = btnErrorLogs.BackColor ;
        }
        #endregion

        #endregion

        #region "Methods"
        //Displaying all the logs in grid
        private void BindGrid()
        {
            ResetGridStyleForFastLoad();
            try
            {
                DataTable objDt = GetDataToBind();
                if (objDt.Rows.Count > 0)
                {
                    grdViewLogHistory.Show();
                    lblMsg.Hide();
                    lblRecordCount.Text = "Total Records : " + objDt.Rows.Count.ToString();
                    grdViewLogHistory.AllowUserToAddRows = false;
                    grdViewLogHistory.DataSource = objDt;
                    grdViewLogHistory.Columns["ID"].Visible = false;
                    grdViewLogHistory.Columns["LogText"].MinimumWidth = 460;
                    grdViewLogHistory.Columns["OrderId"].MinimumWidth = 220;
                    grdViewLogHistory.Columns["LoggedOn"].MinimumWidth = 123;
                  //  grdViewLogHistory.Columns["LoggedOn"].DefaultCellStyle.Format = "dd-MM-yyyy hh:mm:ss tt";
                    for (int i = 1; i < grdViewLogHistory.Columns.Count; i++)
                        grdViewLogHistory.Columns[i].ReadOnly = true;
                }
                else
                {
                    grdViewLogHistory.Hide();
                    lblMsg.Show();
                    lblRecordCount.Text = "Total Records : 0";
                }
                //Add checkbox column only at first time
                if (!blHasCheckboxAdded)
                {
                    DataGridViewCheckBoxColumn chkColumn = new DataGridViewCheckBoxColumn();
                    chkColumn.HeaderText = "";
                    chkColumn.Width = 30;
                    chkColumn.Name = "chkSelectLogToDelete";
                    grdViewLogHistory.Columns.Insert(0, chkColumn);
                    blHasCheckboxAdded = true;
                }
                lblRequired.Hide();
                SetGridStyle();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ResetGridStyleForFastLoad()
        {
            grdViewLogHistory.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            grdViewLogHistory.RowTemplate.MinimumHeight = 30;
            grdViewLogHistory.DoubleBuffered(false);
        }

        private void SetGridStyle()
        {
            grdViewLogHistory.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdViewLogHistory.RowTemplate.MinimumHeight = 30;
            grdViewLogHistory.Columns["chkSelectLogToDelete"].Width = 70;
            grdViewLogHistory.RowHeadersVisible = false;
            grdViewLogHistory.DoubleBuffered(true);
        }

        //Get custom datatable to bind grid,we are replacing && with \n so that we could display each logs in new line
        private DataTable GetDataToBind()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            DataTable objDt = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@SearchValue", SqlDbType.VarChar, 40);
                objParam[0].Value = strSearchText;

                objParam[1] = new SqlParameter("@FromDate", SqlDbType.VarChar, 10);
                objParam[1].Value = strFromDate;

                objParam[2] = new SqlParameter("@ToDate", SqlDbType.VarChar, 10);
                objParam[2].Value = strToDate;

                objParam[3] = new SqlParameter("@SearchMode", SqlDbType.VarChar, 40);
                objParam[3].Value = strSearchMode;

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetLogDetails", objParam).Tables[0];
                for (int i = 0; i < objDt.Rows.Count; i++)
                {
                    string strData = Convert.ToString(objDt.Rows[i]["LogText"]);
                    if (strData.Contains("&&"))
                        objDt.Rows[i]["LogText"] = System.Text.RegularExpressions.Regex.Replace(strData, @"\&&+", "\n");
                }

                //dvLogDetails is for the show complete details on on the viewlogdetails screen
                dvLogDetails = new DataView(objDt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally { connection.Close(); }
            return objDt;
        }

        //Deleting selected logs
        private void DeleteLogs()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            try
            {
                if (GetSelectedLog())
                {
                    DialogResult ConfirmDelete = MessageBox.Show("Are you sure, you want to delete selected log(s)", "Delete Log", MessageBoxButtons.YesNo);
                    if (ConfirmDelete.Equals(DialogResult.Yes))
                    {
                        SqlParameter[] objLog = new SqlParameter[2];
                        objLog[0] = new SqlParameter("@LogIds", SqlDbType.VarChar, int.MaxValue);
                        objLog[0].Value = strLogIds;

                        objLog[1] = new SqlParameter("@Result", SqlDbType.VarChar, 1000);
                        objLog[1].Direction = ParameterDirection.Output;

                        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "DeleteLogs", objLog);
                        string strResult = Convert.ToString(objLog[1].Value);
                        if (strResult == "1")
                        {
                            BindGrid();
                            MessageBox.Show("Log(s) Deleted Successfully !");
                        }
                        else
                            MessageBox.Show(strResult);
                    }
                }
                else
                {
                    MessageBox.Show("Please select at least one log to delete", "Log Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occurred while deleting logs\nMessage:-" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Get the selected logs to delete
        private bool GetSelectedLog()
        {
            bool blIsAnyRowSelected = false;
            try
            {
                int i = 0;
                string strLogID = "";
                foreach (DataGridViewRow row in grdViewLogHistory.Rows)
                {
                    bool IsSelected = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells[0]).Value);
                    if (IsSelected)
                    {
                        blIsAnyRowSelected = true;
                        if (strLogID == "")
                            strLogID = grdViewLogHistory.Rows[i].Cells[1].Value.ToString();
                        else
                            strLogID = strLogID + "," + grdViewLogHistory.Rows[i].Cells[1].Value.ToString();
                    }
                    i++;
                }
                strLogIds = strLogID;
            }
            catch { throw; }
            return blIsAnyRowSelected;
        }

        //To set the orderid or number order id to search
        private void SearchLogs()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtOrderId.Text))
                {
                    strSearchText = txtOrderId.Text;
                    strSearchMode = "ID";
                    Guid guOrderID = new Guid();
                    int intNumOrderID;
                    bool IsValidGuid = Guid.TryParse(strSearchText, out guOrderID);
                    bool IsValidNumOrderId = int.TryParse(strSearchText, out intNumOrderID);
                    if (IsValidGuid || IsValidNumOrderId)
                    {
                        BindGrid();
                    }
                    else
                    {
                        grdViewLogHistory.Hide();
                        lblMsg.Show();
                        lblRecordCount.Text = "Total Records : 0";
                    }
                }
                else { lblRequired.Show(); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //To validate from and to date
        private bool ValidateFromAndToDate(bool IsErrorLogCall)
        {
            bool IsValidDate = false;
            strFromDate = Convert.ToString(dtFrom.Value);
            strToDate = Convert.ToString(dtTo.Value);
            DateTime dtFromDummy = new DateTime();
            DateTime dtToDummy = new DateTime();
            bool isValidFromDate = DateTime.TryParse(Convert.ToString(dtFrom.Value), out dtFromDummy);
            bool isValidToDate = DateTime.TryParse(Convert.ToString(dtTo.Value), out dtToDummy);
            strFromDate = dtFromDummy.ToString("yyyy-MM-dd");
            strToDate = dtToDummy.ToString("yyyy-MM-dd");
            if ((chkFromDate.Checked && chkToDate.Checked))
            {
                if (dtFromDummy <= dtToDummy)
                { IsValidDate = true; strSearchMode = IsErrorLogCall ? "DATE_WITH_ERROR" : "DATE"; }
                else
                    MessageBox.Show("From date must be less than or equal to To date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (chkFromDate.Checked && !chkToDate.Checked)
            { IsValidDate = true; strSearchMode = IsErrorLogCall ? "DATEFROM_WITH_ERROR" : "DATEFROM"; }
            else if (!chkFromDate.Checked && chkToDate.Checked)
            { IsValidDate = true; strSearchMode = IsErrorLogCall ? "DATETO_WITH_ERROR" : "DATETO"; }
            return IsValidDate;
        }
        #endregion
    }
}
