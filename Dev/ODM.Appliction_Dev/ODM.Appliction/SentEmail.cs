﻿using ODM.Data.Entity;
using ODM.Data.Util;
using System;
using System.Data;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class SentEmail : Form
    {
        SendEmailEntity objSendEmail = new SendEmailEntity();
        string strFromDate = "", strToDate = "", strSearchValue = "";
        string strIdsToDeleteOrUpdate = "", strNumOrderIds = "", strOrderIds = "";
        int intLastSelectedRow = -1;
        bool blHasCheckboxAdded = false;
        DataTable objDt = new DataTable();
        public SentEmail()
        {
            InitializeComponent();

            dtFrom.Format = DateTimePickerFormat.Custom;
            dtFrom.CustomFormat = "dd-MMM-yyyy";

            dtTo.Format = DateTimePickerFormat.Custom;
            dtTo.CustomFormat = "dd-MMM-yyyy";

            //BindGrid();
            // SetTxtValuesOnSelectionOfEmailFromGrid(0);
        }

        private void chkFromDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFromDate.Checked)
                dtFrom.Enabled = true;
            else
                dtFrom.Enabled = false;
        }

        private void chkToDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkToDate.Checked)
                dtTo.Enabled = true;
            else
                dtTo.Enabled = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            intLastSelectedRow = -1;
            SearchLogs();
            SetTxtValuesOnSelectionOfEmailFromGrid(0);
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                intLastSelectedRow = -1;               
                SearchLogs();
                SetTxtValuesOnSelectionOfEmailFromGrid(0);
            }
        }
        private void BindGrid()
        {
            try
            {
                objDt = GlobalVariablesAndMethods.GetPendingOrSentEmail("Sent", strFromDate, strToDate, strSearchValue);
                if (objDt.Rows.Count > 0)
                {
                    lblMsg.Hide();
                    grdSentMails.Show();
                    lblRecordCount.Text = "Total Records: " + objDt.Rows.Count.ToString();
                    grdSentMails.AllowUserToAddRows = false;
                    grdSentMails.DataSource = objDt;
                    if (!blHasCheckboxAdded)
                    {
                        DataGridViewCheckBoxColumn chkColumn = new DataGridViewCheckBoxColumn();
                        chkColumn.HeaderText = "";
                        chkColumn.Width = 30;
                        chkColumn.Name = "chkSentEmail";
                        grdSentMails.Columns.Insert(0, chkColumn);
                        blHasCheckboxAdded = true;
                    }
                    for (int i = 1; i < grdSentMails.Columns.Count; i++)
                    {
                        if (grdSentMails.Columns[i].Name == "OrderId" || grdSentMails.Columns[i].Name == "ID")
                        {
                            grdSentMails.Columns[i].Visible = false;
                        }
                        grdSentMails.Columns[i].ReadOnly = true;
                    }
                    grdSentMails.Columns["To Mail"].Width = 230;
                    grdSentMails.Columns["To Name"].Width = 230;
                    grdSentMails.Columns["Subject"].Width = 230;
                    grdSentMails.Rows[0].Selected = true;
                }
                else
                {
                    grdSentMails.Hide();
                    lblMsg.Show();
                    lblRecordCount.Text = "Total Records: 0";
                }
            }
            catch (Exception ex)
            {
                GlobalVariablesAndMethods.LogHistory("Exception occurred while binding sent email grid&&" + ex.Message);
                MessageBox.Show("Error occrred while binding sent email list\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //To set the orderid or number order id to search
        private void SearchLogs()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtSearch.Text) && ValidateFromAndToDate())
                {
                    strSearchValue = txtSearch.Text;
                    Guid guOrderID = new Guid();
                    int intNumOrderID;
                    bool IsValidGuid = Guid.TryParse(strSearchValue, out guOrderID);
                    bool IsValidNumOrderId = int.TryParse(strSearchValue, out intNumOrderID);
                    if (!IsValidGuid && !IsValidNumOrderId)
                    {
                        objDt = new DataTable();
                        grdSentMails.Hide();
                        lblMsg.Show();
                        lblRecordCount.Text = "Total Records : 0";
                    }
                    else
                    {
                        BindGrid();
                    }
                }
                else if (string.IsNullOrEmpty(txtSearch.Text) && ValidateFromAndToDate())
                {
                    strSearchValue = txtSearch.Text;
                    BindGrid();
                }
                else if (string.IsNullOrEmpty(txtSearch.Text) && !ValidateFromAndToDate())
                {
                    grdSentMails.Hide();
                    lblMsg.Show();
                    lblRecordCount.Text = "Total Records : 0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //To validate from and to date
        private bool ValidateFromAndToDate()
        {
            bool IsValidDate = false;
            strFromDate = Convert.ToString(dtFrom.Value);
            strToDate = Convert.ToString(dtTo.Value);
            DateTime dtFromDummy = new DateTime();
            DateTime dtToDummy = new DateTime();
            bool isValidFromDate = DateTime.TryParse(Convert.ToString(dtFrom.Value), out dtFromDummy);
            bool isValidToDate = DateTime.TryParse(Convert.ToString(dtTo.Value), out dtToDummy);
            strFromDate = dtFromDummy.ToString("yyyy-MM-dd");
            strToDate = dtToDummy.ToString("yyyy-MM-dd");
            if ((chkFromDate.Checked && chkToDate.Checked))
            {
                if (dtFromDummy <= dtToDummy)
                { IsValidDate = true; }
                else
                    MessageBox.Show("From date must be less than or equal to To date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (chkFromDate.Checked && !chkToDate.Checked)
            { IsValidDate = true; strToDate = ""; }
            else if (!chkFromDate.Checked && chkToDate.Checked)
            { IsValidDate = true; strFromDate = ""; }
            else if (!chkFromDate.Checked && !chkToDate.Checked)
            { IsValidDate = true; strFromDate = ""; strToDate = ""; }
            return IsValidDate;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            strSearchValue = ""; strFromDate = ""; strToDate = "";
            BindGrid();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// To get the details of selected email either to send email or to delete those from db
        /// </summary>
        /// <param name="blIsSendEmailCall">True means details for send email, False means detials to delete selected rows from db</param>
        /// <returns>True if any row selected otherwise False</returns>
        private bool GetSelectedEmailIds(bool blIsSendEmailCall)
        {
            bool blIsAnyRowSelected = false;
            try
            {
                string strNumOrderID = "", strOrderID = "", strID = "";
                strIdsToDeleteOrUpdate = ""; strNumOrderIds = "";
                foreach (DataGridViewRow row in grdSentMails.Rows)
                {
                    bool IsSelected = Convert.ToBoolean(((DataGridViewCheckBoxCell)row.Cells[0]).Value);
                    if (IsSelected)
                    {
                        objSendEmail = new SendEmailEntity();
                        blIsAnyRowSelected = true;
                        strNumOrderID = Convert.ToString(row.Cells["NumOrderId"].Value);
                        strOrderID = Convert.ToString(row.Cells["OrderId"].Value);
                        strID = Convert.ToString(row.Cells["ID"].Value);

                        strIdsToDeleteOrUpdate = strIdsToDeleteOrUpdate == "" ? strID : strIdsToDeleteOrUpdate + "," + strID;
                        strNumOrderIds = strNumOrderIds == "" ? strNumOrderID : strNumOrderIds + "," + strNumOrderID;
                        strOrderIds = strOrderIds == "" ? strOrderID : strOrderIds + "," + strOrderID;
                    }
                }
            }
            catch { throw; }
            return blIsAnyRowSelected;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (GetSelectedEmailIds(false))
            {
                DialogResult ConfirmDelete = MessageBox.Show("Are you sure, you want to delete selected email(s)", "Delete Mails", MessageBoxButtons.YesNo);
                if (ConfirmDelete == DialogResult.Yes)
                {
                    objSendEmail.IDs = strIdsToDeleteOrUpdate;
                    objSendEmail.NumOrderIDs = strNumOrderIds;
                    objSendEmail.OrderIDs = strOrderIds;
                    objSendEmail.Date = DateTime.Now;
                    objSendEmail.Mode = "DELETE";
                    GlobalVariablesAndMethods.InsertUpdateDeleteDataForPendingEmail(objSendEmail);
                    BindGrid();
                    intLastSelectedRow = -1;
                    SetTxtValuesOnSelectionOfEmailFromGrid(0);
                    MessageBox.Show("Mail(s) Deleted Successfully !", "Email Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
                MessageBox.Show("Select at least one record to delete", "Make Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void grdSentMails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int intSelectedRow = e.RowIndex;
            SetTxtValuesOnSelectionOfEmailFromGrid(intSelectedRow);
        }

        private void SetTxtValuesOnSelectionOfEmailFromGrid(int intSelectedRow)
        {
            if (intSelectedRow != -1)
            {
                try
                {
                    if (objDt.Rows.Count > 0)
                    {
                        if (intLastSelectedRow != intSelectedRow)
                        {
                            txtName.Text = grdSentMails.Rows[intSelectedRow].Cells["To Name"].Value.ToString();
                            txtEmail.Text = grdSentMails.Rows[intSelectedRow].Cells["To Mail"].Value.ToString();
                            txtSubject.Text = grdSentMails.Rows[intSelectedRow].Cells["Subject"].Value.ToString();
                            txtSentMailBody.Text = grdSentMails.Rows[intSelectedRow].Cells["SentMailBody"].Value.ToString();
                            intLastSelectedRow = intSelectedRow;
                         //   grdSentMails.Rows[intSelectedRow].Cells[0].Selected = true;
                        }
                    }
                    else
                    {
                        ClearMailControls();
                    }
                }
                catch (Exception ex)
                {
                    GlobalVariablesAndMethods.LogHistory("Error occurred while setting email details on grid selection&&" + ex.Message);
                    MessageBox.Show("Error occurred while setting email details on grid selection\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ClearMailControls()
        {
            txtName.Text = "";
            txtEmail.Text = "";
            txtSubject.Text = "";
            txtSentMailBody.Text = "";
        }
    }
}
