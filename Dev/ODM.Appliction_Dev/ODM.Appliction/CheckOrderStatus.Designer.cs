﻿namespace ODM.Appliction
{
    partial class CheckOrderStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckOrderStatus));
            this.lblSearch = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblRequired = new System.Windows.Forms.Label();
            this.grdOrders = new System.Windows.Forms.DataGridView();
            this.lblMsg = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnProcessed = new System.Windows.Forms.Button();
            this.btnPending = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.picLoading = new System.Windows.Forms.PictureBox();
            this.cmbSearch = new System.Windows.Forms.ComboBox();
            this.lblStatusMsg = new System.Windows.Forms.Label();
            this.rbtnByReceivedDate = new System.Windows.Forms.RadioButton();
            this.rbtnByProcessedDate = new System.Windows.Forms.RadioButton();
            this.rbtnByAllDates = new System.Windows.Forms.RadioButton();
            this.btnNext = new System.Windows.Forms.Button();
            this.txtSearchNoOfRecords = new System.Windows.Forms.TextBox();
            this.lblPageIndex = new System.Windows.Forms.Label();
            this.btnPrevious = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(3, 28);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(44, 13);
            this.lblSearch.TabIndex = 0;
            this.lblSearch.Text = "Search:";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(192, 26);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(202, 20);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // btnSearch
            // 
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Location = new System.Drawing.Point(240, 102);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblRequired
            // 
            this.lblRequired.AutoSize = true;
            this.lblRequired.ForeColor = System.Drawing.Color.Red;
            this.lblRequired.Location = new System.Drawing.Point(237, 5);
            this.lblRequired.Name = "lblRequired";
            this.lblRequired.Size = new System.Drawing.Size(50, 13);
            this.lblRequired.TabIndex = 3;
            this.lblRequired.Text = "Required";
            this.lblRequired.Visible = false;
            // 
            // grdOrders
            // 
            this.grdOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdOrders.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grdOrders.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOrders.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdOrders.Location = new System.Drawing.Point(6, 153);
            this.grdOrders.Name = "grdOrders";
            this.grdOrders.Size = new System.Drawing.Size(929, 277);
            this.grdOrders.TabIndex = 4;
            this.grdOrders.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOrders_CellDoubleClick);
            this.grdOrders.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdOrders_CellFormatting);
            this.grdOrders.MouseClick += new System.Windows.Forms.MouseEventHandler(this.grdOrders_MouseClick);
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Location = new System.Drawing.Point(262, 228);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(92, 13);
            this.lblMsg.TabIndex = 5;
            this.lblMsg.Text = "No Record Found";
            this.lblMsg.Visible = false;
            // 
            // btnRefresh
            // 
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Location = new System.Drawing.Point(319, 102);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 6;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnProcessed
            // 
            this.btnProcessed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcessed.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnProcessed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcessed.Location = new System.Drawing.Point(726, 102);
            this.btnProcessed.Name = "btnProcessed";
            this.btnProcessed.Size = new System.Drawing.Size(97, 23);
            this.btnProcessed.TabIndex = 8;
            this.btnProcessed.Text = "Processed";
            this.btnProcessed.UseVisualStyleBackColor = false;
            this.btnProcessed.Click += new System.EventHandler(this.btnProcessed_Click);
            // 
            // btnPending
            // 
            this.btnPending.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPending.BackColor = System.Drawing.Color.BurlyWood;
            this.btnPending.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPending.Location = new System.Drawing.Point(829, 102);
            this.btnPending.Name = "btnPending";
            this.btnPending.Size = new System.Drawing.Size(106, 23);
            this.btnPending.TabIndex = 9;
            this.btnPending.Text = "Pending";
            this.btnPending.UseVisualStyleBackColor = false;
            this.btnPending.Click += new System.EventHandler(this.btnPending_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(574, 452);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(0, 13);
            this.lblTotal.TabIndex = 11;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpFromDate.Location = new System.Drawing.Point(558, 31);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(160, 20);
            this.dtpFromDate.TabIndex = 12;
            // 
            // dtpToDate
            // 
            this.dtpToDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpToDate.Location = new System.Drawing.Point(774, 31);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(160, 20);
            this.dtpToDate.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(522, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "From";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(744, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "To";
            // 
            // picLoading
            // 
            this.picLoading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picLoading.Image = ((System.Drawing.Image)(resources.GetObject("picLoading.Image")));
            this.picLoading.Location = new System.Drawing.Point(405, 18);
            this.picLoading.Name = "picLoading";
            this.picLoading.Size = new System.Drawing.Size(28, 28);
            this.picLoading.TabIndex = 31;
            this.picLoading.TabStop = false;
            this.picLoading.Visible = false;
            // 
            // cmbSearch
            // 
            this.cmbSearch.FormattingEnabled = true;
            this.cmbSearch.Location = new System.Drawing.Point(55, 25);
            this.cmbSearch.Name = "cmbSearch";
            this.cmbSearch.Size = new System.Drawing.Size(121, 21);
            this.cmbSearch.TabIndex = 32;
            this.cmbSearch.Text = "OrderNumber";
            // 
            // lblStatusMsg
            // 
            this.lblStatusMsg.AutoSize = true;
            this.lblStatusMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.4F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusMsg.Location = new System.Drawing.Point(12, 107);
            this.lblStatusMsg.Name = "lblStatusMsg";
            this.lblStatusMsg.Size = new System.Drawing.Size(70, 15);
            this.lblStatusMsg.TabIndex = 10;
            this.lblStatusMsg.Text = "All Orders";
            // 
            // rbtnByReceivedDate
            // 
            this.rbtnByReceivedDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbtnByReceivedDate.AutoSize = true;
            this.rbtnByReceivedDate.Location = new System.Drawing.Point(558, 66);
            this.rbtnByReceivedDate.Name = "rbtnByReceivedDate";
            this.rbtnByReceivedDate.Size = new System.Drawing.Size(141, 17);
            this.rbtnByReceivedDate.TabIndex = 33;
            this.rbtnByReceivedDate.Text = "By Order Received Date";
            this.rbtnByReceivedDate.UseVisualStyleBackColor = true;
            this.rbtnByReceivedDate.CheckedChanged += new System.EventHandler(this.rbtnByReceivedDate_CheckedChanged);
            // 
            // rbtnByProcessedDate
            // 
            this.rbtnByProcessedDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbtnByProcessedDate.AutoSize = true;
            this.rbtnByProcessedDate.Location = new System.Drawing.Point(705, 66);
            this.rbtnByProcessedDate.Name = "rbtnByProcessedDate";
            this.rbtnByProcessedDate.Size = new System.Drawing.Size(116, 17);
            this.rbtnByProcessedDate.TabIndex = 34;
            this.rbtnByProcessedDate.Text = "By Processed Date";
            this.rbtnByProcessedDate.UseVisualStyleBackColor = true;
            this.rbtnByProcessedDate.CheckedChanged += new System.EventHandler(this.rbtnByProcessedDate_CheckedChanged);
            // 
            // rbtnByAllDates
            // 
            this.rbtnByAllDates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbtnByAllDates.AutoSize = true;
            this.rbtnByAllDates.Checked = true;
            this.rbtnByAllDates.Location = new System.Drawing.Point(827, 66);
            this.rbtnByAllDates.Name = "rbtnByAllDates";
            this.rbtnByAllDates.Size = new System.Drawing.Size(67, 17);
            this.rbtnByAllDates.TabIndex = 35;
            this.rbtnByAllDates.TabStop = true;
            this.rbtnByAllDates.Text = "All Dates";
            this.rbtnByAllDates.UseVisualStyleBackColor = true;
            this.rbtnByAllDates.CheckedChanged += new System.EventHandler(this.rbtnByAllDates_CheckedChanged);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNext.Location = new System.Drawing.Point(539, 442);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(29, 23);
            this.btnNext.TabIndex = 37;
            this.btnNext.Text = ">>";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // txtSearchNoOfRecords
            // 
            this.txtSearchNoOfRecords.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtSearchNoOfRecords.Location = new System.Drawing.Point(448, 442);
            this.txtSearchNoOfRecords.Name = "txtSearchNoOfRecords";
            this.txtSearchNoOfRecords.Size = new System.Drawing.Size(45, 20);
            this.txtSearchNoOfRecords.TabIndex = 38;
            this.txtSearchNoOfRecords.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtbox_KeyUp);
            // 
            // lblPageIndex
            // 
            this.lblPageIndex.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblPageIndex.AutoSize = true;
            this.lblPageIndex.Location = new System.Drawing.Point(499, 447);
            this.lblPageIndex.Name = "lblPageIndex";
            this.lblPageIndex.Size = new System.Drawing.Size(24, 13);
            this.lblPageIndex.TabIndex = 39;
            this.lblPageIndex.Text = "0/0";
            // 
            // btnPrevious
            // 
            this.btnPrevious.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrevious.Location = new System.Drawing.Point(405, 442);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(29, 23);
            this.btnPrevious.TabIndex = 40;
            this.btnPrevious.Text = "<<";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // CheckOrderStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(946, 473);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.lblPageIndex);
            this.Controls.Add(this.txtSearchNoOfRecords);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.rbtnByAllDates);
            this.Controls.Add(this.rbtnByProcessedDate);
            this.Controls.Add(this.rbtnByReceivedDate);
            this.Controls.Add(this.cmbSearch);
            this.Controls.Add(this.picLoading);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblStatusMsg);
            this.Controls.Add(this.btnPending);
            this.Controls.Add(this.btnProcessed);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.grdOrders);
            this.Controls.Add(this.lblRequired);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.lblSearch);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CheckOrderStatus";
            this.Text = "Check Order Status";
            ((System.ComponentModel.ISupportInitialize)(this.grdOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblRequired;
        private System.Windows.Forms.DataGridView grdOrders;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnProcessed;
        private System.Windows.Forms.Button btnPending;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox picLoading;
        private System.Windows.Forms.ComboBox cmbSearch;
        private System.Windows.Forms.Label lblStatusMsg;
        private System.Windows.Forms.RadioButton rbtnByReceivedDate;
        private System.Windows.Forms.RadioButton rbtnByProcessedDate;
        private System.Windows.Forms.RadioButton rbtnByAllDates;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtSearchNoOfRecords;
        private System.Windows.Forms.Label lblPageIndex;
        private System.Windows.Forms.Button btnPrevious;
    }
}