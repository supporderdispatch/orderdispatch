﻿namespace ODM.Appliction
{
    partial class RuleConditions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RuleConditions));
            this.pnlRuleConditions = new System.Windows.Forms.Panel();
            this.grdRuleConditions = new System.Windows.Forms.DataGridView();
            this.btnAddNewCondition = new System.Windows.Forms.Button();
            this.pnlRuleConditions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRuleConditions)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlRuleConditions
            // 
            this.pnlRuleConditions.Controls.Add(this.grdRuleConditions);
            this.pnlRuleConditions.Location = new System.Drawing.Point(3, 72);
            this.pnlRuleConditions.Name = "pnlRuleConditions";
            this.pnlRuleConditions.Size = new System.Drawing.Size(366, 338);
            this.pnlRuleConditions.TabIndex = 0;
            // 
            // grdRuleConditions
            // 
            this.grdRuleConditions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdRuleConditions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdRuleConditions.Location = new System.Drawing.Point(0, 0);
            this.grdRuleConditions.Name = "grdRuleConditions";
            this.grdRuleConditions.Size = new System.Drawing.Size(366, 338);
            this.grdRuleConditions.TabIndex = 0;
            this.grdRuleConditions.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdRuleConditions_CellDoubleClick);
            // 
            // btnAddNewCondition
            // 
            this.btnAddNewCondition.Location = new System.Drawing.Point(244, 27);
            this.btnAddNewCondition.Name = "btnAddNewCondition";
            this.btnAddNewCondition.Size = new System.Drawing.Size(82, 23);
            this.btnAddNewCondition.TabIndex = 1;
            this.btnAddNewCondition.Text = "Add New";
            this.btnAddNewCondition.UseVisualStyleBackColor = true;
            this.btnAddNewCondition.Click += new System.EventHandler(this.btnAddNewCondition_Click);
            // 
            // RuleConditions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 411);
            this.Controls.Add(this.btnAddNewCondition);
            this.Controls.Add(this.pnlRuleConditions);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RuleConditions";
            this.Text = "RuleConditions";
            this.pnlRuleConditions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdRuleConditions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlRuleConditions;
        private System.Windows.Forms.DataGridView grdRuleConditions;
        private System.Windows.Forms.Button btnAddNewCondition;
    }
}