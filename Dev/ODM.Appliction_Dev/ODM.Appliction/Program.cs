﻿using System;
using System.Windows.Forms;

namespace ODM.Appliction
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());
           // new ODM.Core.InvoiceTemplate.PrintConditions().GetCriteriaFulfillingTemplateFor(Guid.Parse("751F8111-6938-44D5-85AB-455B2DC2111B"));
           // Application.Run(new InvoiceTemplateEditor());
           // Application.Run(new GetOrderData());
        }
    }
}
