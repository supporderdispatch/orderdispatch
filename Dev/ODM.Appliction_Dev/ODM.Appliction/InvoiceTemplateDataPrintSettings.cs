﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Entities.InvoiceTemplate.InvoiceTemplateDataContent;

namespace ODM.Appliction
{
    public partial class InvoiceTemplateDataPrintSettings : Form
    {
        private DataTable InvoiceTemplates { get; set; }
        private Guid BelongsToTemplate{get;set;}

        public InvoiceTemplateDataPrintSettings()
        {
            InitializeComponent();
            InitializeTableContentSettings();
        }
        public InvoiceTemplateDataPrintSettings(Guid templateId)
        {
            InitializeComponent();

            this.BelongsToTemplate = templateId;
            InitializeTableContentSettings();
            LoadTemplateSettingsIfSaved();
        }

        private void LoadTemplateSettingsIfSaved()
        {
            var settings = Activator.CreateInstance<ODM.Core.InvoiceTemplate.PrintWithDataExceedOptions.DataExceed>().GetDataSettings(BelongsToTemplate);
            if (!settings.TemplateId.Equals(Guid.Empty))
            {
                chkTryAdjustTemplateBlock.Checked = settings.TryAdjustBlock;
                cmbTableContentExceedOptions.SelectedItem = settings.DataExceedActionSelected.ToString();
                if (settings.DataExceedActionSelected == TableContentExceedOptions.ChooseAnotherTemplate)
                {
                    cmbTemplates.Enabled = true;
                    cmbTemplates.SelectedItem = InvoiceTemplates.AsEnumerable().Select(x => new { TemplateId = x.Field<Guid>("ID"), TemplateName = x.Field<string>("TemplateName") })
                    .Where(c => c.TemplateId.Equals(settings.TemplateSelectedWhenDataExceed)).FirstOrDefault().TemplateName;
                }
            }
        }

        private void InitializeTableContentSettings()
        {
            IntitializeTableContentExceedOptions();
        }

        private void IntitializeTableContentExceedOptions()
        {
            cmbTableContentExceedOptions.Items.Clear();
            cmbTableContentExceedOptions.Items.AddRange(Enum.GetNames(typeof(TableContentExceedOptions)));
            cmbTableContentExceedOptions.SelectedItem=TableContentExceedOptions.OverFlowHidden.ToString();

            LoadTableContentExceedHelpOptions();
        }

        private void LoadTableContentExceedHelpOptions()
        {
            DataTable description = new DataTable();
            description.Columns.Add("Heading");
            description.Columns.Add("Description");


            DataRow row = description.NewRow();
            row["Heading"] = "OverFlow Hidden (Default)";
            row["Description"] = @"forces table to remain in fixed width by hiding exceeding items";
            description.Rows.Add(row);

            DataRow row2 = description.NewRow();
            row2["Heading"] = "Create New Page Retaining Other Fields";
            row2["Description"] = @"cause applicaion to break and print template content to new page if table content exceed, retaining other fields with applied table configurations";
            description.Rows.Add(row2);

            DataRow row3 = description.NewRow();
            row3["Heading"] = "Create New PageAdjusting Other Fields";
            row3["Description"] = @"cause application to break and print template content to new page if table content exceed, ignoring other fields and table configurations";
            description.Rows.Add(row3);

            DataRow row4 = description.NewRow();
            row4["Heading"] = "Choose Another Template";
            row4["Description"] = @"forces application to use another Template if content exceed. Selected Tempalate Conditions default settings will be ignored when printing from this.";
            description.Rows.Add(row4);

            grdDescription.DataSource = description;
            grdDescription.ReadOnly = true;
            grdDescription.Columns["Heading"].MinimumWidth = 135;
            grdDescription.Columns["Description"].MinimumWidth = 170;  
        }

        private void cmbTableContentExceedOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch((TableContentExceedOptions)Enum.Parse(typeof(TableContentExceedOptions),cmbTableContentExceedOptions.SelectedItem.ToString()))
            {
                case TableContentExceedOptions.OverFlowHidden:
                    {
                        cmbTemplates.Enabled = false;
                        break;
                    }

                case TableContentExceedOptions.CreateNewPageAdjustingOtherFields:
                    {
                        cmbTemplates.Enabled = false;
                        break;
                    }
                    
                case TableContentExceedOptions.CreateNewPageRetainingOtherFields:
                    {
                        cmbTemplates.Enabled = false;
                        break;
                    }

                case TableContentExceedOptions.ChooseAnotherTemplate:
                    {
                        cmbTemplates.Enabled = true;
                        InvoiceTemplates=Activator.CreateInstance<ODM.Core.InvoiceTemplate.InvoiceTemplates>().GetAllInvoiceTemplates();
                        cmbTemplates.DataSource = InvoiceTemplates.AsEnumerable().Select(x => x.Field <string > ("TemplateName")).ToArray();
                        break;
                    }
            }
        }

        private void picHelp_Click(object sender, EventArgs e)
        {
            grdDescription.Visible = grdDescription.Visible ? false : true;
            grdDescription.ClearSelection();   
        }

        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            var dataPrintSettings = new DataPrintSettings();
            dataPrintSettings.TemplateId = BelongsToTemplate;
            dataPrintSettings.DataExceedActionSelected = (TableContentExceedOptions)Enum.Parse(typeof(TableContentExceedOptions),cmbTableContentExceedOptions.SelectedItem.ToString());
            if (dataPrintSettings.DataExceedActionSelected == TableContentExceedOptions.ChooseAnotherTemplate)
            {
                dataPrintSettings.TemplateSelectedWhenDataExceed = InvoiceTemplates.AsEnumerable().Select(x => new { TemplateId = x.Field<Guid>("ID"), TemplateName = x.Field<string>("TemplateName") })
                    .Where(c => c.TemplateName.Equals(cmbTemplates.SelectedItem)).FirstOrDefault().TemplateId;
            }
            dataPrintSettings.TryAdjustBlock = chkTryAdjustTemplateBlock.Checked;
            Activator.CreateInstance<ODM.Core.InvoiceTemplate.PrintWithDataExceedOptions.DataExceed>().SaveDataPrintSettings(dataPrintSettings);
            this.Close();
        }
    }
}