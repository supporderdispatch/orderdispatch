﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Entity;
using ODM.Data.ShippingLabel;
using ODM.Data.Util;
using ODM.Core;
using ODM.Core.UserTracking;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using ODM.Entities;
using ODM.Appliction.Extensions;

namespace ODM.Appliction
{
    public partial class AllOpenOrders : Form
    {
        #region "Global Variables"
        string strOrderID = "";
        string[] strOrderId, strNumOrderID;
        SqlConnection conn;
        PDFLib objPdf = new PDFLib();
        public Guid LocationID;
        public Guid ShippingID;
        DataTable objDt = new DataTable();
        DataTable objDtToBindGrid = new DataTable();
        Order objOrder = new Order();
        Guid Orderid = new Guid();
        string strPrintLabelError = "";
        string strTrackingNumber = "";
        string strServiceName = "";
        string strCourierType = "";
        string strSearchInput = "";
        string strNumOrderId = "";
        string strTextToLog = "";
        bool blIsNewSearchText = true;
        bool blIsOrderDeleted = false;
        Guid pkPostalServiceID = new Guid();
        SendEmailEntity objSendEmail = new SendEmailEntity();
        public DataTable MainDataSource = new DataTable();
        private System.Timers.Timer AutoRefreshTimer;
        #endregion

        #region "Constructor"
        public AllOpenOrders(string strInputToSearch, DataTable objLastDataTable)
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
            PicRight.Visible = false;
            PicError.Visible = false;
            PicLoading.Visible = false;
            txtSearch.Text = strInputToSearch;
            RegisterEvents();
            if (strInputToSearch != "" && objLastDataTable.Rows.Count > 0)
            {
                objDtToBindGrid = objLastDataTable;
                blIsNewSearchText = false;
                BindGrid();
            }
            InitializePickListPanel();

        }
        public AllOpenOrders()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            GlobalVariablesAndMethods.GuidForLog = Guid.NewGuid();
            PicRight.Visible = false;
            PicError.Visible = false;
            PicLoading.Visible = false;
            RegisterEvents();
            InitializePickListPanel();
        }

        private void InitializePickListPanel()
        {
            cmbMultipleItems.Items.Clear();
            cmbPickListPrinted.Items.Clear();
            cmbPickingBatchDrp.Items.Clear();
            cmbUrgentOrders.Items.Clear();

            cmbMultipleItems.Items.AddRange(Enum.GetNames(typeof(FilterOption)));
            cmbPickListPrinted.Items.AddRange(Enum.GetNames(typeof(FilterOption)));
            cmbUrgentOrders.Items.AddRange(Enum.GetNames(typeof(FilterOption)));
            cmbPickingBatchDrp.Items.Add("All");

            if (GlobalVariablesAndMethods.UserAppliedFilters == null)
            {
                InitializeUserAppliedFiltersIfEmpty();
            }
            else
            {
                if (cmbPickingBatchDrp.Items.Count == 1 && GlobalVariablesAndMethods.UserAppliedFilters.PickingBatch != null && !GlobalVariablesAndMethods.UserAppliedFilters.PickingBatch.ToString().Equals("All"))
                {
                    cmbPickingBatchDrp.Items.Insert(1, GlobalVariablesAndMethods.UserAppliedFilters.PickingBatch.ToString());
                }

                var pickingBatch = (string)GlobalVariablesAndMethods.UserAppliedFilters.PickingBatch ?? "All";
                var pickListPrinted = GlobalVariablesAndMethods.UserAppliedFilters.PickListPrinted.ToString();
                var urgentOrders = GlobalVariablesAndMethods.UserAppliedFilters.UrgentOrders.ToString();
                var multipleItems = GlobalVariablesAndMethods.UserAppliedFilters.MultipleItems.ToString();

                cmbPickingBatchDrp.SelectedItem = pickingBatch;
                cmbPickListPrinted.SelectedItem = pickListPrinted;
                cmbUrgentOrders.SelectedItem = urgentOrders;
                cmbMultipleItems.SelectedItem = multipleItems;
            }
        }

        private void InitializeUserAppliedFiltersIfEmpty()
        {
            cmbPickListPrinted.SelectedIndex = cmbPickListPrinted.SelectedIndex = cmbUrgentOrders.SelectedIndex = cmbPickingBatchDrp.SelectedIndex = 0;
        }
        #endregion

        #region "Events"
        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (ValidateControl())
                {
                    BindGrid();
                }
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (ValidateControl())
            {
                BindGrid();
            }
        }
        private void grdOrders_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (grdOrders.CheckIfFilterActivatedFor())
                {
                    var selectedOrderIds = new List<string>();
                    if (grdOrders.SelectedRows.Count > 0)
                    {
                        foreach (DataGridViewRow row in grdOrders.SelectedRows)
                        {
                            if (!string.IsNullOrEmpty(row.Cells["OrderId"].Value.ToString()))
                            {
                                selectedOrderIds.Add(row.Cells["OrderId"].Value.ToString());
                            }
                        }
                    }
                    grdOrders.DisableFilter();
                    foreach (DataGridViewRow row in grdOrders.Rows)
                    {
                        if (!string.IsNullOrEmpty(row.Cells["OrderId"].Value.ToString()))
                        {
                            if (selectedOrderIds.Contains(row.Cells["OrderId"].Value.ToString()))
                            {
                                row.Selected = true;
                            }
                            else
                            {
                                row.Selected = false;
                            }
                        }
                    }
                }
                int intSelectedRowIndex = grdOrders.HitTest(e.X, e.Y).RowIndex;
                DataGridViewSelectedRowCollection objDGSelectedRow = grdOrders.SelectedRows;
                strOrderId = new string[objDGSelectedRow.Count];
                strNumOrderID = new string[objDGSelectedRow.Count];
                for (int i = 0; i < objDGSelectedRow.Count; i++)
                {

                    strOrderId[i] = objDGSelectedRow[i].Cells["Orderid"].Value.ToString();
                    strNumOrderID[i] = objDGSelectedRow[i].Cells["NumOrderId"].Value.ToString();
                }
                ToolStripMenuItem changeShippingMethod = new ToolStripMenuItem();
                changeShippingMethod.Text = "Change Shipping Method To";
                changeShippingMethod.Image = new Bitmap(1, 1);
                var shippingMethodsGrpByVendor = Activator.CreateInstance<ShippingMethods>().GetShippingMethodsGroupByVendor();
                foreach (var vendorAndItsShippingMethods in shippingMethodsGrpByVendor)
                {
                    ToolStripMenuItem vendorItem = new ToolStripMenuItem();
                    vendorItem.Text = vendorAndItsShippingMethods.Vendor;
                    changeShippingMethod.DropDownItems.Add(vendorItem);

                    vendorAndItsShippingMethods.ShippingMethods.Sort();
                    foreach (var shippingMethod in vendorAndItsShippingMethods.ShippingMethods)
                    {
                        ToolStripMenuItem shippingMethodMenuItem = new ToolStripMenuItem();
                        shippingMethodMenuItem.Text = shippingMethod.ToString();
                        shippingMethodMenuItem.Click += (Sender, ev) =>
                        {
                            ChangeShippingMethodTo(shippingMethod.ToString());
                        };
                        vendorItem.DropDownItems.Add(shippingMethodMenuItem);
                    }
                }
                changeShippingMethod.DropDown.MaximumSize = new Size(300, 380);

                ContextMenuStrip cm = new ContextMenuStrip();
                cm.Items.Add(new ToolStripMenuItem("Print Label", new Bitmap(1, 1), PrintLabel_click));
                cm.Items.Add(new ToolStripMenuItem("Print Invoice", new Bitmap(1, 1), PrintInvoice_click));
                cm.Items.Add(new ToolStripMenuItem("Process Orders", new Bitmap(1, 1), ProcessOrder_click));
                cm.Items.Add(new ToolStripMenuItem("Skip to process order", new Bitmap(1, 1), SkipToProcessOrder_Click));
                cm.Items.Add(new ToolStripMenuItem("Remove Orders", new Bitmap(1, 1), RemoveOrder_click));
                cm.Items.Add(changeShippingMethod);
                cm.Items.Add(new ToolStripMenuItem("View Logs", new Bitmap(1, 1), ViewLog_click));
                cm.Items.Add(new ToolStripMenuItem("Unlock orders", new Bitmap(1, 1), Unlock_Orders));
                cm.Items.Add(new ToolStripMenuItem("Column Chooser", new Bitmap(1, 1), ColumnChooser_click));
                cm.Show(grdOrders, new Point(e.X, e.Y));

                //}
            }
        }

        private void SkipToProcessOrder_Click(object sender, EventArgs e)
        {
            string orderid = grdOrders.SelectedRows[0].Cells["OrderId"].Value.ToString();
            GlobalVariablesAndMethods.OrderIdToLog = Guid.Parse(orderid);
            GlobalVariablesAndMethods.LogHistory("Scanning skipped and opening Process order from Context menu");
            ProcessOrder ProcessOrderScreen = new ProcessOrder(orderid);
            ProcessOrderScreen.Show();
        }

        private void Unlock_Orders(object sender, EventArgs e)
        {
            var orders = new List<string>();
            foreach (DataGridViewRow row in grdOrders.SelectedRows)
            {
                string orderid = row.Cells["NumOrderId"].Value.ToString();
                orders.Add(orderid);
            }
            Activator.CreateInstance<ProcessStateOfOrder>().UnblockOrders(orders);
            grdOrders.Invalidate();
            objDtToBindGrid = null;
            BindGrid();
        }

        private void ChangeShippingMethodTo(string shippingService)
        {
            var listOfOrderIds = new List<string>();
            foreach (DataGridViewRow row in grdOrders.SelectedRows)
            {
                string orderid = row.Cells["OrderId"].Value.ToString();
                GlobalVariablesAndMethods.OrderIdToLog = new Guid(orderid);
                string textToLog = String.Empty;
                string oldShippingService = row.Cells["PostalServiceName"].Value.ToString();
                listOfOrderIds.Add(orderid);
                textToLog = String.Format("Changing shipping method from {0} to {1}", oldShippingService, shippingService);
                UpdateProcessStateOfOrder(new Guid(orderid), ProcessCompleted.Shipping_Method_Changed);
                GlobalVariablesAndMethods.LogHistory(textToLog);
            }
            Activator.CreateInstance<ShippingMethods>().ChangeShippingMethodOfOrderTo(listOfOrderIds, shippingService);
            objDtToBindGrid = null;
            BindGrid();
        }

        private void ViewLog_click(object sender, EventArgs e)
        {
            var selectedOrders = new List<string>();
            foreach (DataGridViewRow row in grdOrders.SelectedRows)
            {
                selectedOrders.Add(row.Cells["NumOrderId"].Value.ToString());
            }
            ShowLogsOfSelectedOrders(selectedOrders);
        }

        private void RemoveOrder_click(object sender, EventArgs e)
        {
            string ordersToRemove = string.Empty;
            DialogResult objDialog = MessageBox.Show("Are you sure, you want to remove selected Order?", "Remove Order", MessageBoxButtons.YesNo);
            if (objDialog.Equals(DialogResult.Yes))
            {
                for (int i = 0; i < strOrderId.Length; i++)
                {
                    ordersToRemove += strOrderId[i] + ",";
                }
                RemoveSelectedOrder(ordersToRemove);
                btnRefresh.PerformClick();
            }
        }
        private void PicErrorCustomEvent_MouseHover(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(strPrintLabelError))
            {
                ToolTip ErrorToolTip = new ToolTip();
                ErrorToolTip.SetToolTip(PicError, strPrintLabelError);
            }
        }

        bool orderProcessed = false;

        private List<RowData> checkAndIterate(string field)
        {
            CheckForIllegalCrossThreadCalls = false;
            grdOrders.Columns["process status"].Visible = true;
            grdOrders.DefaultCellStyle.SelectionBackColor = Color.WhiteSmoke;
            grdOrders.DefaultCellStyle.SelectionForeColor = Color.Black;
            var indexAndFieldData = new List<RowData>();
            foreach (DataGridViewRow row in grdOrders.SelectedRows)
            {
                indexAndFieldData.Add(new RowData() { RowIndex = row.Index, FieldData = row.Cells[field].Value.ToString() });
                currentProcessState = Tuple.Create(ProcessState.None, row.Index, true);
            }
            return indexAndFieldData;
        }
        private void PrintLabel_click(object sender, EventArgs e)
        {
            if (grdOrders.SelectedRows.Count > 0)
            {
                var rowData = checkAndIterate("OrderId");
                foreach (var data in rowData.OrderBy(x => x.RowIndex).ToList())
                {
                    var orderid = data.FieldData;
                    OrderTracker.BeginTrackerForOrder(orderid);
                    currentlyAnimating = false;
                    orderProcessed = false;
                    currentProcessState = Tuple.Create(ProcessState.None, data.RowIndex, true);
                    GlobalVariablesAndMethods.OrderIdToLog = new Guid(orderid);
                    var ableToSetValuesForPrintLabelOrProcess = populateVariablesToPrintLabelOrProcess(orderid);
                    for (int i = 0; i < 3; i++)
                    {
                        GlobalVariablesAndMethods.OrderIdToLog = new Guid(orderid);
                        if (!ableToSetValuesForPrintLabelOrProcess)
                        {
                            ableToSetValuesForPrintLabelOrProcess = populateVariablesToPrintLabelOrProcess(orderid);
                        }
                        if (ableToSetValuesForPrintLabelOrProcess)
                        {
                            i = 3;
                        }
                        if (i == 2 & ableToSetValuesForPrintLabelOrProcess == false)
                        {
                            MessageBox.Show("Something went wrong" + Environment.NewLine + "Printing label for order :" + GlobalVariablesAndMethods.OrderIdToLog.ToString() + Environment.NewLine + "While order in process: " + Orderid);
                        }
                    }

                    if (ableToSetValuesForPrintLabelOrProcess)
                    {
                        CheckCourierAndPrintForMultipleOrders(data.RowIndex);
                    }
                    else
                    {
                        orderProcessed = true;
                    }
                    while (!orderProcessed)
                    {
                        Task t = new Task(() =>
                        {
                        });
                        try { t.Wait(500); }
                        catch { }
                        t.Start();
                    }
                    OrderTracker.EndTracking();
                }
                selectionOperationFinished();
            }
        }

        private void selectionOperationFinished()
        {
            grdOrders.DefaultCellStyle.SelectionBackColor = Color.DodgerBlue;
            grdOrders.DefaultCellStyle.SelectionForeColor = Color.Black;
            objDtToBindGrid = null;
            BindGrid();
        }


        private void PrintInvoice_click(object sender, EventArgs e)
        {

            PicLoading.Visible = true;
            try
            {
                //PrintInvoiceProcess();
                PrintInvoiceForMultiple();
                selectionOperationFinished();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            PicLoading.Visible = false;
        }
        private void ProcessOrder_click(object sender, EventArgs e)
        {
            currentProcessState = Tuple.Create(ProcessState.Loading, grdOrders.SelectedRows[0].Index, true);
            var rowDataWithOrderId = checkAndIterate("OrderId");
            foreach (var data in rowDataWithOrderId.OrderBy(x => x.RowIndex).ToList())
            {
                var orderid = data.FieldData;
                GlobalVariablesAndMethods.OrderIdToLog = Guid.Parse(orderid);
                OrderTracker.BeginTrackerForOrder(orderid);
                var currentRowIndex = data.RowIndex;
                try
                {
                    currentProcessState = Tuple.Create(ProcessState.Loading, grdOrders.SelectedRows[0].Index, true);
                    GlobalVariablesAndMethods.OrderIdToLog = Guid.Parse(orderid);
                    if (populateVariablesToPrintLabelOrProcess(orderid))
                    {
                        if (string.IsNullOrEmpty(strTrackingNumber))
                        {
                            currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                            MessageBox.Show("Please print label before process order", "Error", MessageBoxButtons.OK);
                        }
                        else
                        {
                            ProcessOrderAndUpdateAPI.UpdateOrderAPI(Orderid, true, LocationID, strTrackingNumber);
                            UpdateProcessStatusInDB();
                            UpdateProcessStateOfOrder(Orderid, ProcessCompleted.Processed);
                            GlobalVariablesAndMethods.LogHistory("Process status 'processed' updated in Database");
                            //string strResult = GlobalVariablesAndMethods.InsertUpdateDeleteDataForPendingEmail(objSendEmail);
                            ODM.Core.Emails.CreatePendingEmail.CreatePendingEmailForOrder(Orderid);
                            ODM.Core.UserTracking.OrderTracker.MarkOrderProcessed(Orderid.ToString());
                            //GlobalVariablesAndMethods.LogHistory("Moving to Log database");
                            //GlobalVariablesAndMethods.MoveOrderToLogDatabase(Orderid);
                            currentProcessState = Tuple.Create(ProcessState.Completed, currentRowIndex, true);
                        }
                    }
                }
                catch (Exception Ex)
                {
                    currentProcessState = Tuple.Create(ProcessState.Failed, currentRowIndex, true);
                    MessageBox.Show(Ex.Message, "Error while process order", MessageBoxButtons.OK);
                }
                OrderTracker.EndTracking();
            }
            selectionOperationFinished();
            grdOrders.Invalidate();
            BindGrid();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ColumnChooser_click(object sender, EventArgs e)
        {
            ColumnChooser objChooser = new ColumnChooser(objDtToBindGrid, txtSearch.Text);
            objChooser.Show();
        }
        #endregion

        #region "Methods"
        private async void BindGrid()
        {
            //  ResetGridForFastLoad();
            PicError.Visible = false;
            PicLoading.Visible = true;
            int intErrorCount = 0;
            int intCountOfItem = 1;
            string[] arrColumns = new string[] { };
            InitializePickListPanel();

        ExecuteAgainIfDeadLock:
            try
            {
                strTextToLog = "Bind grid call begins";
                ColumnChooser objColumnChooser = new ColumnChooser();
                /* checking objDtToBindGrid because we are passing that from choose column screen as well
                 * if that is not null or count is more than 0 that means we don't have to make db call again and
                 * call "SetDataSet" method.
                 */
                if (objDtToBindGrid == null || objDtToBindGrid.Rows.Count == 0 || blIsNewSearchText || blIsOrderDeleted)
                {
                    strTextToLog += "&&" + "Creating database call";
                    conn = new SqlConnection();
                    conn.ConnectionString = MakeConnection.GetConnection();
                    conn.Open();

                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("@SearchInput", SqlDbType.VarChar, 8000);
                    param[0].Value = txtSearch.Text;

                    //DataTable objDt = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAllOpenOrders", param).Tables[0];
                    objDtToBindGrid = await Task.Run<DataTable>(() => { return SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAllOpenOrders_New", param).Tables[0]; });
                    strTextToLog += "&&" + "Got data from database";
                    if (objDtToBindGrid.Rows.Count > 1)
                    {
                        strTextToLog += "&&" + "Its a multiple item call, trying to create custom datasource";
                        objDtToBindGrid = await Task.Run<DataTable>(() => { return SetDataSet(objDtToBindGrid); });
                        strTextToLog += "&&" + "End of custom datasource";
                    }
                }
                // End of objDtToBindGrid check

                if (objDtToBindGrid.Rows.Count > 0)
                {
                    GlobalVariablesAndMethods.OrderIdToLog = new Guid(Convert.ToString(objDtToBindGrid.Rows[0]["Orderid"]));
                    grdOrders.Visible = true;
                    grdOrders.AllowUserToAddRows = false;
                    grdOrders.DataSource = objDtToBindGrid;
                    grdOrders.Columns["OrderId"].Visible = false;
                    DataTable objDtSelectedColumns = new DataTable();
                    int intResult = 100;
                    try
                    {
                        objDtSelectedColumns = objColumnChooser.GetAddOrUpdateColumnChooser("GET", GlobalVariablesAndMethods.UserDetails.Userid, "", ref intResult);
                        var visibleColumns = new List<string>();
                        if (objDtSelectedColumns.Rows.Count == 0)
                        {
                            string strColumnsToAdd = "";
                            for (int i = 1; i < objDtToBindGrid.Columns.Count; i++)
                            {
                                strColumnsToAdd = strColumnsToAdd == "" ? objDtToBindGrid.Columns[i].ColumnName : strColumnsToAdd + "," + objDtToBindGrid.Columns[i].ColumnName;
                            }
                            objColumnChooser.GetAddOrUpdateColumnChooser("UPDATE", GlobalVariablesAndMethods.UserDetails.Userid, strColumnsToAdd, ref intResult);
                            if (intResult == 2)
                            {
                                arrColumns = strColumnsToAdd.Split(',');
                            }
                        }
                        else
                        {
                            visibleColumns.RemoveAll(x => !string.IsNullOrEmpty(x));
                            arrColumns = Convert.ToString(objDtSelectedColumns.Rows[0]["SelectedColumns"]).Split(',');
                            visibleColumns.AddRange(arrColumns);
                        }

                        for (int i = 1; i < objDtToBindGrid.Columns.Count; i++)
                        {
                            grdOrders.Columns[i].ReadOnly = true;

                            string strColumnName = objDtToBindGrid.Columns[i].ColumnName;

                            if (visibleColumns.Any(x => x.ToUpper().Trim().Equals(strColumnName.ToUpper().Trim())))
                            {
                                grdOrders.Columns[strColumnName].Visible = true;
                            }
                            else
                            {
                                grdOrders.Columns[strColumnName].Visible = false;
                            }
                        }

                        grdOrders.Columns["Items"].Width = 250;
                        grdOrders.Columns["Items"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                        grdOrders.Columns["PostalServiceName"].Width = 145;

                        grdOrders.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                        lblMessage.Visible = false;
                        grdOrders.Columns["NumOrderId"].Width = 100;
                        grdOrders.Columns["SubSource"].Width = 100;
                        grdOrders.Columns["ProcessesCompleted"].DisplayIndex = grdOrders.Columns["NumOrderId"].Index + 1;

                        bool showProcessesCompletedColumn = false;
                        for (int rowIndex = 0; rowIndex < grdOrders.Rows.Count; rowIndex++)
                        {
                            var cellValue = grdOrders.Rows[rowIndex].Cells["ProcessesCompleted"].Value;
                            if (!Object.ReferenceEquals(cellValue, null))
                            {
                                if (!String.IsNullOrEmpty(cellValue.ToString()))
                                {
                                    showProcessesCompletedColumn = true;
                                    grdOrders.Rows[rowIndex].Cells["ProcessesCompleted"].Value = ParseCellValue(cellValue.ToString());
                                }
                            }
                            //
                            var isOpened = grdOrders.Rows[rowIndex].Cells["IsOpened"];
                            if (!Object.ReferenceEquals(isOpened, null))
                            {
                                try
                                {
                                    if (Convert.ToBoolean(isOpened.Value))
                                    {
                                        grdOrders.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                                        grdOrders.Rows[rowIndex].Selected = false;
                                    }
                                }
                                catch (Exception ex) { throw ex; }
                            }
                        }
                        //grdOrders.Columns["IsOpened"].Visible = false;
                        grdOrders.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
                        //    grdOrders.Columns["ProcessesCompleted"].Visible = showProcessesCompletedColumn;
                        grdOrders.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                        grdOrders.DoubleBuffered(true);
                        grdOrders.BackgroundColor = Color.WhiteSmoke;
                        grdOrders.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                        grdOrders.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                        grdOrders.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
                        grdOrders.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                        grdOrders.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                        grdOrders.ScrollBars = ScrollBars.Both;
                        grdOrders.Columns["ProcessesCompleted"].MinimumWidth = 150;
                        grdOrders.Columns["Vendor"].MinimumWidth = 80;
                        grdOrders.Columns["FullName"].MinimumWidth = 100;
                        grdOrders.Columns["PickListBatch"].MinimumWidth = 100;
                        //    grdOrders.Columns["PickListBatch"].Visible = true;

                        if (object.ReferenceEquals(grdOrders.Columns["process status"], null))
                        {
                            DataGridViewImageColumn iconColumn = new DataGridViewImageColumn();
                            iconColumn.Name = "process status";
                            iconColumn.HeaderText = String.Empty;
                            grdOrders.Columns.Insert(0, iconColumn);
                            var processStatusColumn = grdOrders.Columns["process status"];
                            processStatusColumn.DefaultCellStyle.NullValue = new Bitmap(1, 1);
                            grdOrders.Columns["process status"].MinimumWidth = 140;
                        }
                        grdOrders.Columns["process status"].Visible = false;
                        strTextToLog += "&&" + "End of Choose column process";
                        grdOrders.ClearSelection();
                        UpdateSelectedRowsStatus();
                        grdOrders.Columns["NumOrderId"].Visible = true;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error occurred while binding columns for grid\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
                else
                {
                    grdOrders.Visible = false;
                    lblMessage.Visible = true;
                }
                strSearchInput = txtSearch.Text;
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205 && intErrorCount < 4)
                {
                    intErrorCount++;
                    goto ExecuteAgainIfDeadLock;
                }
                else
                {
                    strTextToLog += "&&" + "SQL Error occurred while binding grid Exception :- " + ex.Message + "&&Inner Exception:- " + ex.InnerException;
                    GlobalVariablesAndMethods.LogHistory(strTextToLog);
                    MessageBox.Show("Error occurred please try again after sometime", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                strTextToLog += "&&" + "Error occurred while binding grid Exception :- " + ex.Message + "&&Inner Exception:- " + ex.InnerException;
                GlobalVariablesAndMethods.LogHistory(strTextToLog);
                MessageBox.Show(ex.Message, "Error while reading data", MessageBoxButtons.OK);
            }
            strTextToLog += "&&" + "End of Bind grid process";
            GlobalVariablesAndMethods.LogHistory(strTextToLog);
            PicLoading.Visible = false;
            if (grdOrders.Rows.Count > 0)
            {
                MainDataSource = objDtToBindGrid.Copy();
                ApplyPickPanelFilter(grdOrders);
                LoadPickingBatches();
            }
            Action act = () => { RemoveEmptyRow(); };
            Delegate del = act;
            ODM.Appliction.Extensions.DataGridViewFilter.ApplyFilter(grdOrders, objDtToBindGrid, del);
        }

        private void ResetGridForFastLoad()
        {
            grdOrders.DoubleBuffered(false);
            grdOrders.DefaultCellStyle.WrapMode = DataGridViewTriState.NotSet;
            grdOrders.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            grdOrders.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            grdOrders.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            grdOrders.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            grdOrders.ScrollBars = ScrollBars.None;
        }

        private void RemoveEmptyRow()
        {
            if (grdOrders.CheckIfFilterActivatedFor())
            {
                objDtToBindGrid = ODM.Appliction.Extensions.DataGridViewFilter.dataSource.Copy();
            }
            else
            {
                objDtToBindGrid = ODM.Appliction.Extensions.DataGridViewFilter.MainDataSource.Copy();
            }
            // grdOrders.DataSource = objDtToBindGrid;
            // LoadPickingBatches();
            ApplyPickPanelFilter(null);
            grdOrders.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private bool ValidateControl()
        {
            bool IsValid = true;
            if (txtSearch.Text.Trim().ToUpper() == strSearchInput.Trim().ToUpper())
            {
                blIsNewSearchText = false;
            }
            else
            {
                blIsNewSearchText = true;
            }
            lblRequired.Visible = false;
            //}
            return IsValid;
        }
        #endregion

        private void btnPrintPickList_Click(object sender, EventArgs e)
        {
            var commaSepratedselectedOrderIds = string.Empty;
            var totalItems = 0;
            var sku = new List<string>();
            if (grdOrders.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in grdOrders.SelectedRows)
                {
                    commaSepratedselectedOrderIds += row.Cells["OrderId"].Value.ToString() + ",";
                    totalItems += Convert.ToInt32(row.Cells["TotalNumberOfItems"].Value.ToString());
                    sku.AddRange(row.Cells["SKU"].Value.ToString().Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList());
                }
            }
            ShowBatchPromptandPrintPickList(commaSepratedselectedOrderIds, totalItems, sku.Distinct().ToList().Count());
        }

        private void ShowBatchPromptandPrintPickList(string commaSepratedselectedOrderIds,int totalNumberOfItems, int totalsku)
        {
            bool result = false;
            string batchName = GetBatchName();

            var dataSource = MainDataSource;

            var showPrompt = new Helpers.PrintPickList(batchName, commaSepratedselectedOrderIds,totalNumberOfItems,totalsku);
            if (showPrompt.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(Helpers.PrintPickList.batchName))
                {
                    batchName = Helpers.PrintPickList.batchName;
                    Action<string> logPickListPrint = (orderId) =>
                    {
                        GlobalVariablesAndMethods.OrderIdToLog = Guid.Parse(orderId);
                        GlobalVariablesAndMethods.LogHistory(string.Format("Pick list printed with batch {0}", batchName));
                    };

                    foreach (DataGridViewRow row in grdOrders.SelectedRows)
                    {                        
                        var orderId = row.Cells["OrderId"].Value.ToString();
                        DataRow dataRow = MainDataSource.AsEnumerable().Where(r => r["OrderId"].ToString().Equals(orderId)).First();
                        if (!object.ReferenceEquals(dataRow, null))
                        {
                            var picklistbatchCellValue = Object.ReferenceEquals(dataRow["PickListBatch"], null) ? string.Empty : dataRow["PickListBatch"].ToString();
                            if (String.IsNullOrEmpty(picklistbatchCellValue))
                            {
                                dataRow["PickListBatch"] = batchName;
                                dataRow["IsPickListPrinted"] = 1;
                                logPickListPrint(orderId);
                            }
                        }
                    }
                    MainDataSource.AcceptChanges();
                    UpdateSelectedRowsStatus();
                    LoadPickingBatches();
                    ApplyPickPanelFilter(grdOrders);
                }
            }
        }

        private void LoadPickingBatches()
        {
            var batchSelected = (string)cmbPickingBatchDrp.SelectedItem ?? "All";
            try
            {
                if (objDtToBindGrid != null)
                {
                    var batches = objDtToBindGrid.AsEnumerable().Select(x => x.Field<string>("PickListBatch")).Distinct().ToList();
                    batches.Sort();
                    cmbPickingBatchDrp.Items.Clear();
                    cmbPickingBatchDrp.Items.Add("All");

                    foreach (var batchName in batches)
                    {
                        if (!Object.ReferenceEquals(batchName, null) && !cmbPickingBatchDrp.Items.Contains(batchName))
                        {
                            if (!String.IsNullOrEmpty(batchName))
                            {
                                cmbPickingBatchDrp.Items.Add(batchName);
                            }
                        }
                    }
                }

                var batchList = new List<string>();
                if (cmbPickingBatchDrp.Items.Count > 1)
                {
                    batchList.Clear();
                    foreach (var batch in cmbPickingBatchDrp.Items)
                    {
                        batchList.Add(batch.ToString());
                    }
                    if (!batchList.Contains(batchSelected))
                    {
                        cmbPickingBatchDrp.SelectedItem = "All";
                    }
                    else
                    {
                        cmbPickingBatchDrp.SelectedItem = batchSelected;
                    }
                }

            }
            catch { }
        }

        private string GetBatchName()
        {
            var today = DateTime.UtcNow.Date;
            var result = String.Format("{0:0000}-{1:00}-{2:00} - ", today.Year, today.Month, today.Day);
            var multipleOrderItemSelectedOpt = (FilterOption)cmbMultipleItems.SelectedIndex;
            var urgentOrderSelectedOpt = (FilterOption)cmbUrgentOrders.SelectedIndex;

            if (urgentOrderSelectedOpt == FilterOption.Yes)
            {
                result += String.Format("{0} - ", "urgent");
            }
            if (multipleOrderItemSelectedOpt == FilterOption.All)
            {
                result += String.Format("mix - ");
            }
            if (multipleOrderItemSelectedOpt == FilterOption.No)
            {
                result += String.Format("singles - ");
            }
            if (multipleOrderItemSelectedOpt == FilterOption.Yes)
            {
                result += String.Format("multiples - ");
            }

            result += String.Format("({0})", grdOrders.SelectedRows.Count);
            return result;
        }

        private void cmbPickListPrinted_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyPickPanelFilter(sender);
        }

        private void cmbUrgentOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyPickPanelFilter(sender);
        }

        private void cmbPickingBatchDrp_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyPickPanelFilter(sender);
        }

        private void cmbMultipleItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyPickPanelFilter(sender);
        }

        private void ApplyPickPanelFilter(object sender)
        {
            var searchFieldslist = new List<CellAndValue>();
            if (grdOrders.CheckIfFilterActivatedFor())
            {
                if (objDtToBindGrid != null)
                    objDtToBindGrid.Clear();
                objDtToBindGrid = ODM.Appliction.Extensions.DataGridViewFilter.dataSource.Copy();
                if (objDtToBindGrid.Rows.Count > 0)
                {
                    foreach (DataColumn column in objDtToBindGrid.Columns)
                    {
                        if (string.IsNullOrEmpty(objDtToBindGrid.Rows[0]["OrderId"].ToString()))
                        {
                            searchFieldslist.Add(new CellAndValue()
                            {
                                CellColumnName = column.ColumnName,
                                CellValue = Convert.ToString(objDtToBindGrid.Rows[0][column.ColumnName].GetType().Equals(typeof(DBNull)) ? string.Empty : objDtToBindGrid.Rows[0][column.ColumnName] ?? string.Empty)
                            });
                        }
                    }
                }

                //objDtToBindGrid.Rows.RemoveAt(0);
                //objDtToBindGrid.AcceptChanges();
                foreach (DataRow row in objDtToBindGrid.Rows)
                {
                    if (row.Equals(objDtToBindGrid.NewRow()))
                    {
                        objDtToBindGrid.Rows.Remove(row);
                    }
                }
                objDtToBindGrid.AcceptChanges();
            }
            else
            {
                if (objDtToBindGrid != null)
                    objDtToBindGrid.Clear();
                objDtToBindGrid = MainDataSource.Copy();
            }
            if (grdOrders.CheckIfFilterActivatedFor())
            {
                var filterClear = true;
                if (grdOrders.Rows.Count > 0)
                {
                    foreach (DataGridViewCell cell in grdOrders.Rows[0].Cells)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(cell.Value ?? string.Empty)))
                        {
                            filterClear = false;
                            break;
                        }
                    }
                }
                if (objDtToBindGrid.Rows.Count == 0)
                {
                    filterClear = false;
                }
                if (filterClear)
                {
                    objDtToBindGrid = MainDataSource.Copy();
                }
            }
            if (!Object.ReferenceEquals(null, objDtToBindGrid) && objDtToBindGrid.Rows.Count > 0)
            {
                var multipleOrderItemSelectedOpt = (FilterOption)cmbMultipleItems.SelectedIndex;
                var urgentOrderSelectedOpt = (FilterOption)cmbUrgentOrders.SelectedIndex;
                var picklistPrintedSelectedOpt = (FilterOption)cmbPickListPrinted.SelectedIndex;
                var pickingBatchOpt = cmbPickingBatchDrp.SelectedItem ?? String.Empty;

                DataTable filteredDataTable = objDtToBindGrid.Clone();

                foreach (DataRow row in objDtToBindGrid.Rows)
                {
                    var numOfDistinctItems = row["NumOfDistinctItem"].GetType().Equals(typeof(DBNull)) ? 0 : row["NumOfDistinctItem"];
                    var urgentOrder = row["IsUrgent"].GetType().Equals(typeof(DBNull)) ? 0 : row["IsUrgent"];
                    var pickBatch = row["PickListBatch"].GetType().Equals(typeof(DBNull)) ? string.Empty : row["PickListBatch"];
                    var pickListPrinted = row["IsPickListPrinted"].GetType().Equals(typeof(DBNull)) ? 0 : row["IsPickListPrinted"];

                    var multipleorderitemResult = ((multipleOrderItemSelectedOpt == FilterOption.Yes && (Convert.ToInt32(numOfDistinctItems ?? 0) > 1)) ||
                        (multipleOrderItemSelectedOpt == FilterOption.No && (Convert.ToInt32(numOfDistinctItems ?? 0) == 1)) ||
                        (multipleOrderItemSelectedOpt == FilterOption.All));

                    var urgentOrderResult = ((urgentOrderSelectedOpt == FilterOption.Yes && Convert.ToBoolean(urgentOrder ?? false)) ||
                        (urgentOrderSelectedOpt == FilterOption.No && !Convert.ToBoolean(urgentOrder ?? false)) ||
                        (urgentOrderSelectedOpt == FilterOption.All));

                    var pickingBatchResult = ((!String.IsNullOrEmpty(pickingBatchOpt.ToString()) && pickBatch.Equals(pickingBatchOpt.ToString())) ||
                        (pickingBatchOpt.ToString().Equals("All")));

                    var pickListPrintedResult = ((picklistPrintedSelectedOpt == FilterOption.Yes && Convert.ToBoolean(pickListPrinted ?? false) ||
                        (picklistPrintedSelectedOpt == FilterOption.No && !Convert.ToBoolean(pickListPrinted ?? false)) ||
                        (picklistPrintedSelectedOpt == FilterOption.All)
                        ));

                    if (multipleorderitemResult && urgentOrderResult && pickingBatchResult && pickListPrintedResult)
                    {
                        filteredDataTable.Rows.Add(row.ItemArray);
                    }
                }

                if (sender != null)
                {
                    if (grdOrders.CheckIfFilterActivatedFor())
                    {
                        var newRow = filteredDataTable.NewRow();
                        if (filteredDataTable.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty((string)filteredDataTable.Rows[0]["OrderId"].ToString() ?? string.Empty))
                                filteredDataTable.Rows.InsertAt(newRow, 0);
                        }
                    }
                }

                grdOrders.DataSource = filteredDataTable;
                grdOrders.Invalidate();
                if (grdOrders.CheckIfFilterActivatedFor())
                {
                    searchFieldslist.RemoveAll(x => string.IsNullOrEmpty(x.CellValue));
                    if (grdOrders.Rows.Count >= 0)
                    {
                        if (grdOrders.Rows.Count == 0)
                        {
                            filteredDataTable.Rows.InsertAt(filteredDataTable.NewRow(), 0);
                        }

                        foreach (var cell in searchFieldslist)
                        {
                            grdOrders.Rows[0].Cells[cell.CellColumnName].Value = cell.CellValue;
                        }
                    }
                }
                UpdateSelectedRowsStatus();
                SaveUserSelection();
            }
        }

        private void SaveUserSelection()
        {
            GlobalVariablesAndMethods.UserAppliedFilters.MultipleItems = (FilterOption)Enum.Parse(typeof(FilterOption), (string)cmbMultipleItems.SelectedItem ?? FilterOption.All.ToString());
            GlobalVariablesAndMethods.UserAppliedFilters.UrgentOrders = (FilterOption)Enum.Parse(typeof(FilterOption), (string)cmbUrgentOrders.SelectedItem ?? FilterOption.All.ToString());
            GlobalVariablesAndMethods.UserAppliedFilters.PickListPrinted = (FilterOption)Enum.Parse(typeof(FilterOption), (string)cmbPickListPrinted.SelectedItem ?? FilterOption.All.ToString());
            GlobalVariablesAndMethods.UserAppliedFilters.PickingBatch = (string)cmbPickingBatchDrp.SelectedItem ?? "All";
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            grdOrders.SelectAll();
            if (grdOrders.CheckIfFilterActivatedFor() && grdOrders.Rows.Count > 0)
            {
                grdOrders.Rows[0].Selected = false;
            }
            UpdateSelectedRowsStatus();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            objDtToBindGrid = null;
            BindGrid();
            InitializePickListPanel();
        }

        private void btnResetFilters_Click(object sender, EventArgs e)
        {
            PicLoading.Visible = true;
            cmbMultipleItems.SelectedIndex = cmbPickingBatchDrp.SelectedIndex = cmbPickListPrinted.SelectedIndex = cmbUrgentOrders.SelectedIndex = 0;
            PicLoading.Visible = false;
        }

    }
}
