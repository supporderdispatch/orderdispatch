﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class SenderDetailsForRoyalMail : Form
    {
        #region "Global Variables"
        string _CourierName = "";
        string _CourierID = "";
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();

        //objects for royal mail testing
        //ShippingLabelRoyalMail objRoyalMail = new ShippingLabelRoyalMail();
        //RoyalMailPrintLabel objPrintLabel = new RoyalMailPrintLabel();
        //Order objOrder = new Order();
        //end of royal mail.

        #endregion

        #region "Constructors"
        public SenderDetailsForRoyalMail()
        {
            InitializeComponent();
        }
        public SenderDetailsForRoyalMail(string strCourierName, string strCourierID)
        {
            InitializeComponent();
           
            //For testing of royal mail api
            
            //objRoyalMail.CreateShipment();
            //Guid gid=new Guid();
            //objPrintLabel.ExecuteRoyalMailShippingAPI(objOrder, gid);
            //end of testing 
            
            _CourierName = strCourierName;
            _CourierID = strCourierID;
            PopulateControls();
        }
        #endregion

        #region "Methods"
        private void PopulateControls()
        {
            try
            {
                DataTable ObjdtCourier = new DataTable();
                ObjdtCourier = GlobalVar.GetCourierDetailsByNameOrId(_CourierName, _CourierID);
                if (ObjdtCourier.Rows.Count > 0)
                {
                    BindCountryDDL();
                    txtBuildingName.Text = Convert.ToString(ObjdtCourier.Rows[0]["BuildingName"]);
                    txtBuildingNumber.Text = Convert.ToString(ObjdtCourier.Rows[0]["BuildingNumber"]);
                    txtAddressLine1.Text = Convert.ToString(ObjdtCourier.Rows[0]["AddressLine1"]);
                    txtAddressLine2.Text = Convert.ToString(ObjdtCourier.Rows[0]["AddressLine2"]);
                    txtAddressLine3.Text = Convert.ToString(ObjdtCourier.Rows[0]["AddressLine3"]);
                    txtPostTown.Text = Convert.ToString(ObjdtCourier.Rows[0]["PostTown"]);
                    txtPostCode.Text = Convert.ToString(ObjdtCourier.Rows[0]["PostCode"]);
                    ddlCountry.SelectedValue = Convert.ToString(ObjdtCourier.Rows[0]["CountryID"]);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void BindCountryDDL()
        {
            try
            {
                ddlCountry.ValueMember = "CountryId";
                ddlCountry.DisplayMember = "CountryName";
                ddlCountry.DataSource = GlobalVar.GetAllCountries();
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void SaveSenderDetails()
        {
            try
            {
                SqlConnection connection = new SqlConnection();

                Guid CountryID = new Guid(ddlCountry.SelectedValue.ToString());
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();

                SqlParameter[] objParam = new SqlParameter[8];

                objParam[0] = new SqlParameter("@BuildingName", SqlDbType.VarChar, 100);
                objParam[0].Value = txtBuildingName.Text;

                objParam[1] = new SqlParameter("@BuildingNumber", SqlDbType.VarChar, 100);
                objParam[1].Value = txtBuildingNumber.Text;

                objParam[2] = new SqlParameter("@AddressLine1", SqlDbType.VarChar, 100);
                objParam[2].Value = txtAddressLine1.Text;

                objParam[3] = new SqlParameter("@AddressLine2", SqlDbType.VarChar, 100);
                objParam[3].Value = txtAddressLine2.Text;

                objParam[4] = new SqlParameter("@AddressLine3", SqlDbType.VarChar, 100);
                objParam[4].Value = txtAddressLine3.Text;

                objParam[5] = new SqlParameter("@PostTown", SqlDbType.VarChar, 100);
                objParam[5].Value = txtPostTown.Text;

                objParam[6] = new SqlParameter("@PostCode", SqlDbType.VarChar, 50);
                objParam[6].Value = txtPostCode.Text;

                objParam[7] = new SqlParameter("@CountryID", SqlDbType.UniqueIdentifier);
                objParam[7].Value = CountryID;

                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateSenderDetailsRoyalMail", objParam);
                this.Close();
                MessageBox.Show("Sender Details Saved Sucessfully");
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool ValidateControls()
        {
            bool IsValid = true;
            if (string.IsNullOrWhiteSpace(txtBuildingName.Text))
            {
                lblBuildingNameRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblBuildingNameRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtBuildingNumber.Text))
            {
                lblBuildingNumberRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblBuildingNumberRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAddressLine1.Text))
            {
                lblAddressLine1Required.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAddressLine1Required.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAddressLine2.Text))
            {
                lblAddressLine2Required.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAddressLine2Required.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtAddressLine3.Text))
            {
                lblAddressLine3Required.Visible = true;
                IsValid = false;
            }
            else
            {
                lblAddressLine3Required.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPostCode.Text))
            {
                lblPostCodeRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPostCodeRequired.Visible = false;
            }

            if (string.IsNullOrWhiteSpace(txtPostTown.Text))
            {
                lblPostTownRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblPostTownRequired.Visible = false;
            }

            if (ddlCountry.Text != "" && ddlCountry.SelectedValue == null)
            {
                lblCountryRequired.Visible = true;
                lblCountryRequired.Text = "Invalid Country";
                IsValid = false;
            }
            else if (ddlCountry.Text == "" && ddlCountry.SelectedValue == null)
            {
                lblCountryRequired.Visible = true;
                lblCountryRequired.Text = "Required";
                IsValid = false;
            }
            else
            {
                lblCountryRequired.Visible = false;
            }

            return IsValid;
        }
        #endregion

        #region "Events"
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                SaveSenderDetails();
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
