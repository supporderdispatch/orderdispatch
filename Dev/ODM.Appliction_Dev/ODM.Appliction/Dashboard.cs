﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class Dashboard : Form
    {
        private List<DashboardStatisticsData> _DashBoardData;
        private System.Timers.Timer AutoRefreshTimer;

        public Dashboard()
        {
            InitializeComponent();
            this.PicLoading.Visible = false;
            this.Load += Dashboard_Load;
            this.FormClosed += Dashboard_FormClosed;
            LoadDashBoardStatistics();
        }

        void Dashboard_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (AutoRefreshTimer != null)
                AutoRefreshTimer.Dispose();
        }

        void Dashboard_Load(object sender, EventArgs e)
        {
            cmbAutoRefreshInterval.Items.Clear();
            cmbAutoRefreshInterval.Items.AddRange(new string[] { "2", "5", "10", "15", "20", "30", "60" });
            cmbAutoRefreshInterval.SelectedIndex = 0;
            this.chkAutoRefresh.CheckedChanged+=chkAutoRefresh_CheckedChanged;
            this.cmbAutoRefreshInterval.SelectedIndexChanged+=cmbAutoRefreshInterval_SelectedIndexChanged;
            SetUnsetAutoRefresh(false);
        }

        private void SetUnsetAutoRefresh(bool run)
        {
            CheckForIllegalCrossThreadCalls = false;
            if (AutoRefreshTimer != null)
            {
                AutoRefreshTimer.Dispose();
            }

            AutoRefreshTimer = new System.Timers.Timer();
            AutoRefreshTimer.Interval = TimeSpan.FromMinutes(Convert.ToInt32(cmbAutoRefreshInterval.SelectedItem.ToString())).TotalMilliseconds;
            AutoRefreshTimer.Elapsed += (s, e) => { SetUnsetAutoRefresh(true); };

            if (chkAutoRefresh.Checked)
            {
                AutoRefreshTimer.Start();
                Action act = () => { 
                    if(run)
                        LoadDashBoardStatistics(); };
                this.BeginInvoke(act);

                lblLastAutoUpdate.Text = "Last updated at : " + DateTime.Now.ToLocalTime();
            }
            else
            {
                AutoRefreshTimer.Stop();
                lblLastAutoUpdate.Text = string.Empty;
            }
        }

        private void LoadDashBoardStatistics()
        {
            this.PicLoading.Visible = true;
            LoadData();
        }

        private async void LoadData()
        {
            var conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SearchInput", SqlDbType.VarChar, 8000);
            param[0].Value = string.Empty;
            await Task.Run(() =>
            {
                var objDt = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAllOpenOrders_New", param).Tables[0];
                if (objDt.Rows.Count > 0)
                {
                    try
                    {
                        objDt.Columns.Add("TotalNumberOfItems", typeof(Int32));
                        DataTable FinalDataSet = new DataTable();
                        DataView objDv = new DataView(objDt);
                        DataTable DistinctRows = objDv.ToTable(true, "NumOrderId");
                        FinalDataSet = objDt.Clone();
                        int intOrderNum, intCount;
                        for (int i = 0; i < DistinctRows.Rows.Count; i++)
                        {
                            intOrderNum = Convert.ToInt32(DistinctRows.Rows[i]["NumOrderId"]);
                            intCount = 0;

                            DataView DvFiltered = new DataView(objDt);
                            DvFiltered.RowFilter = "NumOrderId =" + intOrderNum;
                            DataTable DtFiltered = new DataTable();
                            DtFiltered = DvFiltered.ToTable();

                            var sku = string.Empty;
                            foreach (DataRow MainRow in DtFiltered.Rows)
                            {
                                if (intCount == 0)
                                {
                                    FinalDataSet.ImportRow(MainRow);
                                }
                                else
                                {
                                    FinalDataSet.Rows[i]["Items"] = Convert.ToString(FinalDataSet.Rows[i]["Items"]) + "\n\n" + Convert.ToString(MainRow["Items"]);
                                }
                                var totalItems = FinalDataSet.Rows[i]["TotalNumberOfItems"].GetType().Equals(typeof(DBNull)) ? 0 : FinalDataSet.Rows[i]["TotalNumberOfItems"];
                                var numberOfItems = Convert.ToInt32(MainRow["NumOfDistinctItem"].ToString());
                                FinalDataSet.Rows[i]["TotalNumberOfItems"] = (Convert.ToInt32(totalItems) + numberOfItems);
                                sku += MainRow["SKU"].ToString().TrimEnd() + ",";
                                intCount++;
                            }
                            FinalDataSet.Rows[i]["NumOfDistinctItem"] = DtFiltered.Rows.Count;
                            FinalDataSet.Rows[i]["SKU"] = sku;
                        }
                        if (FinalDataSet.Rows.Count > 0)
                        {
                            _DashBoardData = new List<DashboardStatisticsData>();
                            foreach (DataRow row in FinalDataSet.Rows)
                            {
                                var dashBoardData = new DashboardStatisticsData();
                                dashBoardData.NumOrderId = Convert.ToInt32(row["NumOrderId"].ToString());
                                dashBoardData.IsUrgent = Convert.ToBoolean(row["IsUrgent"]);
                                dashBoardData.IsMultiple = Convert.ToInt32(row["NumOfDistinctItem"].ToString()) > 1;
                                dashBoardData.IsPickListPrinted = Convert.ToBoolean(Convert.ToInt32(row["IsPickListPrinted"].ToString()));
                                dashBoardData.Items = Convert.ToInt32(row["TotalNumberOfItems"].ToString());
                                dashBoardData.Sku = row["SKU"].ToString();
                                _DashBoardData.Add(dashBoardData);
                            }
                            if (_DashBoardData.Count > 0)
                            {
                                Action populateDataIntoGrid = () =>
                                {
                                    lblTotalOpenOrders.Text = _DashBoardData.Count.ToString();
                                    lblPicked.Text = _DashBoardData.Where(x => x.IsPickListPrinted).ToList().Count.ToString();
                                    lblPickedSingle.Text = _DashBoardData.Where(x => x.IsPickListPrinted && !x.IsMultiple).ToList().Count.ToString();
                                    lblPickedSingleUrgent.Text = _DashBoardData.Where(x => x.IsPickListPrinted && !x.IsMultiple && x.IsUrgent).ToList().Count.ToString();
                                    lblPickedsinglenotUrgent.Text = _DashBoardData.Where(x => x.IsPickListPrinted && !x.IsMultiple && !x.IsUrgent).ToList().Count.ToString();
                                    lblPickedMultiple.Text = _DashBoardData.Where(x => x.IsPickListPrinted && x.IsMultiple).ToList().Count.ToString();
                                    lblPickedMultipleUrgent.Text = _DashBoardData.Where(x => x.IsPickListPrinted && x.IsMultiple && x.IsUrgent).ToList().Count.ToString();
                                    lblPickedMultipleNotUrgent.Text = _DashBoardData.Where(x => x.IsPickListPrinted && x.IsMultiple && !x.IsUrgent).ToList().Count.ToString();
                                    lblUnPicked.Text = _DashBoardData.Where(x => !x.IsPickListPrinted).ToList().Count.ToString();
                                    lblUnPickedSingle.Text = _DashBoardData.Where(x => !x.IsPickListPrinted && !x.IsMultiple).ToList().Count.ToString();
                                    lblUnPickedSingleUrgent.Text = _DashBoardData.Where(x => !x.IsPickListPrinted && x.IsMultiple && x.IsUrgent).ToList().Count.ToString();
                                    lblUnPickedsinglenotUrgent.Text = _DashBoardData.Where(x => !x.IsPickListPrinted && !x.IsMultiple && !x.IsUrgent).ToList().Count.ToString();
                                    lblUnPickedMultiple.Text = _DashBoardData.Where(x => !x.IsPickListPrinted && x.IsMultiple).ToList().Count.ToString();
                                    lblUnPickedMultipleUrgent.Text = _DashBoardData.Where(x => !x.IsPickListPrinted && x.IsMultiple && x.IsUrgent).ToList().Count.ToString();
                                    lblUnPickedMultipleNotUrgent.Text = _DashBoardData.Where(x => !x.IsPickListPrinted && x.IsMultiple && !x.IsUrgent).ToList().Count.ToString();
                                    lblItemCount.Text = _DashBoardData.Sum(x => x.Items).ToString();
                                    lblSkuCount.Text = GetSkuCount(_DashBoardData);
                                    this.PicLoading.Visible = false;
                                };
                                this.BeginInvoke(populateDataIntoGrid);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
            });
        }

        private string GetSkuCount(List<DashboardStatisticsData> _DashBoardData)
        {
            var sku = string.Empty;
            _DashBoardData.ForEach(x => 
            {
                sku += x.Sku + ",";
            });

            return sku.Split(',').ToList().Where(x => !string.IsNullOrEmpty(x)).ToList().Count.ToString();
        }

        private void chkAutoRefresh_CheckedChanged(object sender, EventArgs e)
        {
            SetUnsetAutoRefresh(true);
        }

        private void cmbAutoRefreshInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetUnsetAutoRefresh(true);
        }
    }
    public class DashboardStatisticsData
    {
        public int NumOrderId { get; set; }
        public bool IsUrgent { get; set; }
        public bool IsMultiple { get; set; }
        public bool IsPickListPrinted { get; set; }
        public int Items { get;set; }
        public string Sku { get; set; }
    }
}
