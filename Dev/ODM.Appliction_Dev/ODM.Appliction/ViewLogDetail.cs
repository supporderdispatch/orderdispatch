﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class ViewLogDetail : Form
    {
        #region "Constructors"
        public ViewLogDetail()
        {
            InitializeComponent();
        }
        public ViewLogDetail(DataTable objDt)
        {
            InitializeComponent();
            BindGrid(objDt);
        }
        #endregion

        #region "Methods"
        private void BindGrid(DataTable objDtLogDetail)
        {
            try
            {
                lblDetail.Text = "Order Number ID :- " + Convert.ToString(objDtLogDetail.Rows[0]["NumOrderId"])
                    + "\nLogged By :-" + Convert.ToString(objDtLogDetail.Rows[0]["LoggedByName"]).ToUpper()
                    + "\nLog ID :- " + Convert.ToString(objDtLogDetail.Rows[0]["ID"]).ToUpper();
                grdLogDetail.AllowUserToAddRows = false;
                grdLogDetail.DataSource = objDtLogDetail;
                grdLogDetail.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
                grdLogDetail.Columns["LogText"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                grdLogDetail.Columns["LogText"].Width = 600;
                for (int i = 0; i < grdLogDetail.Columns.Count; i++)
                {
                    if (grdLogDetail.Columns[i].Name != "LogText")
                    {
                        grdLogDetail.Columns[i].Visible = false;
                    }
                    grdLogDetail.Columns[i].ReadOnly = true;
                }
                SetGridStyle();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SetGridStyle()
        {
            grdLogDetail.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
            // grdOrders.Columns["ProcessesCompleted"].Visible = showProcessesCompletedColumn;
            grdLogDetail.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            grdLogDetail.DoubleBuffered(true);
            grdLogDetail.BackgroundColor = Color.WhiteSmoke;
            grdLogDetail.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            grdLogDetail.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            grdLogDetail.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            grdLogDetail.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grdLogDetail.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grdLogDetail.ScrollBars = ScrollBars.Both;
            grdLogDetail.DefaultCellStyle.Padding = new Padding(2, 2, 2, 2);
            grdLogDetail.RowHeadersVisible = false;
            grdLogDetail.Font=new Font("Arial",10);
            grdLogDetail.DefaultCellStyle.SelectionBackColor = Color.WhiteSmoke;
            grdLogDetail.DefaultCellStyle.SelectionForeColor = Color.Black;
        }
        #endregion
    }
}
