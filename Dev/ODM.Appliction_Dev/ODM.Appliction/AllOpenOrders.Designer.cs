﻿namespace ODM.Appliction
{
    partial class AllOpenOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AllOpenOrders));
            this.lblOrder = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.grdOrders = new System.Windows.Forms.DataGridView();
            this.lblRequired = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.PicRight = new System.Windows.Forms.PictureBox();
            this.PicLoading = new System.Windows.Forms.PictureBox();
            this.PicError = new System.Windows.Forms.PictureBox();
            this.lblSelectedOrdersStatus = new System.Windows.Forms.Label();
            this.lblPickListPrinted = new System.Windows.Forms.Label();
            this.cmbPickListPrinted = new System.Windows.Forms.ComboBox();
            this.cmbUrgentOrders = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPickingBatchDrp = new System.Windows.Forms.ComboBox();
            this.lblMultipleItems = new System.Windows.Forms.Label();
            this.cmbMultipleItems = new System.Windows.Forms.ComboBox();
            this.btnPrintPickList = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnResetFilters = new System.Windows.Forms.Button();
            this.chkAutoRefresh = new System.Windows.Forms.CheckBox();
            this.cmbAutoRefreshInterval = new System.Windows.Forms.ComboBox();
            this.lblLastAutoUpdate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicError)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Location = new System.Drawing.Point(7, 15);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(56, 13);
            this.lblOrder.TabIndex = 0;
            this.lblOrder.Text = "Find Order";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(69, 11);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(199, 20);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(107, 35);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // grdOrders
            // 
            this.grdOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdOrders.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.grdOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOrders.Location = new System.Drawing.Point(0, 93);
            this.grdOrders.Name = "grdOrders";
            this.grdOrders.RowTemplate.ReadOnly = true;
            this.grdOrders.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdOrders.Size = new System.Drawing.Size(1008, 486);
            this.grdOrders.TabIndex = 3;
            this.grdOrders.MouseClick += new System.Windows.Forms.MouseEventHandler(this.grdOrders_MouseClick);
            // 
            // lblRequired
            // 
            this.lblRequired.AutoSize = true;
            this.lblRequired.ForeColor = System.Drawing.Color.Red;
            this.lblRequired.Location = new System.Drawing.Point(273, 18);
            this.lblRequired.Name = "lblRequired";
            this.lblRequired.Size = new System.Drawing.Size(65, 13);
            this.lblRequired.TabIndex = 4;
            this.lblRequired.Text = "Invalid Input";
            this.lblRequired.Visible = false;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(480, 220);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(92, 13);
            this.lblMessage.TabIndex = 5;
            this.lblMessage.Text = "No Record Found";
            this.lblMessage.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(188, 35);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // PicRight
            // 
            this.PicRight.Image = ((System.Drawing.Image)(resources.GetObject("PicRight.Image")));
            this.PicRight.Location = new System.Drawing.Point(279, 15);
            this.PicRight.Name = "PicRight";
            this.PicRight.Size = new System.Drawing.Size(25, 18);
            this.PicRight.TabIndex = 28;
            this.PicRight.TabStop = false;
            // 
            // PicLoading
            // 
            this.PicLoading.Image = ((System.Drawing.Image)(resources.GetObject("PicLoading.Image")));
            this.PicLoading.Location = new System.Drawing.Point(276, 15);
            this.PicLoading.Name = "PicLoading";
            this.PicLoading.Size = new System.Drawing.Size(26, 20);
            this.PicLoading.TabIndex = 29;
            this.PicLoading.TabStop = false;
            // 
            // PicError
            // 
            this.PicError.Image = ((System.Drawing.Image)(resources.GetObject("PicError.Image")));
            this.PicError.Location = new System.Drawing.Point(276, 15);
            this.PicError.Name = "PicError";
            this.PicError.Size = new System.Drawing.Size(26, 20);
            this.PicError.TabIndex = 30;
            this.PicError.TabStop = false;
            // 
            // lblSelectedOrdersStatus
            // 
            this.lblSelectedOrdersStatus.AutoSize = true;
            this.lblSelectedOrdersStatus.Location = new System.Drawing.Point(356, 77);
            this.lblSelectedOrdersStatus.Name = "lblSelectedOrdersStatus";
            this.lblSelectedOrdersStatus.Size = new System.Drawing.Size(113, 13);
            this.lblSelectedOrdersStatus.TabIndex = 31;
            this.lblSelectedOrdersStatus.Text = "Selected 0 of 0 Orders";
            // 
            // lblPickListPrinted
            // 
            this.lblPickListPrinted.AutoSize = true;
            this.lblPickListPrinted.Location = new System.Drawing.Point(508, 18);
            this.lblPickListPrinted.Name = "lblPickListPrinted";
            this.lblPickListPrinted.Size = new System.Drawing.Size(80, 13);
            this.lblPickListPrinted.TabIndex = 32;
            this.lblPickListPrinted.Text = "PickList Printed";
            // 
            // cmbPickListPrinted
            // 
            this.cmbPickListPrinted.FormattingEnabled = true;
            this.cmbPickListPrinted.Location = new System.Drawing.Point(606, 12);
            this.cmbPickListPrinted.Name = "cmbPickListPrinted";
            this.cmbPickListPrinted.Size = new System.Drawing.Size(57, 21);
            this.cmbPickListPrinted.TabIndex = 33;
            this.cmbPickListPrinted.SelectedIndexChanged += new System.EventHandler(this.cmbPickListPrinted_SelectedIndexChanged);
            // 
            // cmbUrgentOrders
            // 
            this.cmbUrgentOrders.FormattingEnabled = true;
            this.cmbUrgentOrders.Location = new System.Drawing.Point(606, 58);
            this.cmbUrgentOrders.Name = "cmbUrgentOrders";
            this.cmbUrgentOrders.Size = new System.Drawing.Size(57, 21);
            this.cmbUrgentOrders.TabIndex = 34;
            this.cmbUrgentOrders.SelectedIndexChanged += new System.EventHandler(this.cmbUrgentOrders_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(514, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "Urgent Orders";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(699, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 36;
            this.label2.Text = "Picking Batch";
            // 
            // cmbPickingBatchDrp
            // 
            this.cmbPickingBatchDrp.FormattingEnabled = true;
            this.cmbPickingBatchDrp.Location = new System.Drawing.Point(778, 14);
            this.cmbPickingBatchDrp.Name = "cmbPickingBatchDrp";
            this.cmbPickingBatchDrp.Size = new System.Drawing.Size(179, 21);
            this.cmbPickingBatchDrp.TabIndex = 37;
            this.cmbPickingBatchDrp.SelectedIndexChanged += new System.EventHandler(this.cmbPickingBatchDrp_SelectedIndexChanged);
            // 
            // lblMultipleItems
            // 
            this.lblMultipleItems.AutoSize = true;
            this.lblMultipleItems.Location = new System.Drawing.Point(701, 61);
            this.lblMultipleItems.Name = "lblMultipleItems";
            this.lblMultipleItems.Size = new System.Drawing.Size(71, 13);
            this.lblMultipleItems.TabIndex = 38;
            this.lblMultipleItems.Text = "Multiple Items";
            // 
            // cmbMultipleItems
            // 
            this.cmbMultipleItems.FormattingEnabled = true;
            this.cmbMultipleItems.Location = new System.Drawing.Point(778, 58);
            this.cmbMultipleItems.Name = "cmbMultipleItems";
            this.cmbMultipleItems.Size = new System.Drawing.Size(66, 21);
            this.cmbMultipleItems.TabIndex = 39;
            this.cmbMultipleItems.SelectedIndexChanged += new System.EventHandler(this.cmbMultipleItems_SelectedIndexChanged);
            // 
            // btnPrintPickList
            // 
            this.btnPrintPickList.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnPrintPickList.Location = new System.Drawing.Point(873, 56);
            this.btnPrintPickList.Name = "btnPrintPickList";
            this.btnPrintPickList.Size = new System.Drawing.Size(84, 23);
            this.btnPrintPickList.TabIndex = 40;
            this.btnPrintPickList.Text = "Print Pick List";
            this.btnPrintPickList.UseVisualStyleBackColor = true;
            this.btnPrintPickList.Click += new System.EventHandler(this.btnPrintPickList_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnSelectAll.Location = new System.Drawing.Point(346, 42);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(75, 23);
            this.btnSelectAll.TabIndex = 41;
            this.btnSelectAll.Text = "Select All";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(346, 13);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 42;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnResetFilters
            // 
            this.btnResetFilters.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnResetFilters.Location = new System.Drawing.Point(427, 13);
            this.btnResetFilters.Name = "btnResetFilters";
            this.btnResetFilters.Size = new System.Drawing.Size(75, 23);
            this.btnResetFilters.TabIndex = 55;
            this.btnResetFilters.Text = "Reset Filters";
            this.btnResetFilters.UseVisualStyleBackColor = true;
            this.btnResetFilters.Click += new System.EventHandler(this.btnResetFilters_Click);
            // 
            // chkAutoRefresh
            // 
            this.chkAutoRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkAutoRefresh.AutoSize = true;
            this.chkAutoRefresh.Checked = true;
            this.chkAutoRefresh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoRefresh.Location = new System.Drawing.Point(10, 585);
            this.chkAutoRefresh.Name = "chkAutoRefresh";
            this.chkAutoRefresh.Size = new System.Drawing.Size(163, 17);
            this.chkAutoRefresh.TabIndex = 57;
            this.chkAutoRefresh.Text = "Auto Refresh    Interval (min):";
            this.chkAutoRefresh.UseVisualStyleBackColor = true;
            // 
            // cmbAutoRefreshInterval
            // 
            this.cmbAutoRefreshInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbAutoRefreshInterval.FormattingEnabled = true;
            this.cmbAutoRefreshInterval.Location = new System.Drawing.Point(177, 583);
            this.cmbAutoRefreshInterval.Name = "cmbAutoRefreshInterval";
            this.cmbAutoRefreshInterval.Size = new System.Drawing.Size(38, 21);
            this.cmbAutoRefreshInterval.TabIndex = 56;
            // 
            // lblLastAutoUpdate
            // 
            this.lblLastAutoUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLastAutoUpdate.AutoSize = true;
            this.lblLastAutoUpdate.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblLastAutoUpdate.Location = new System.Drawing.Point(233, 588);
            this.lblLastAutoUpdate.Name = "lblLastAutoUpdate";
            this.lblLastAutoUpdate.Size = new System.Drawing.Size(105, 13);
            this.lblLastAutoUpdate.TabIndex = 58;
            this.lblLastAutoUpdate.Text = "last auto update time";
            // 
            // AllOpenOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 608);
            this.Controls.Add(this.lblLastAutoUpdate);
            this.Controls.Add(this.chkAutoRefresh);
            this.Controls.Add(this.cmbAutoRefreshInterval);
            this.Controls.Add(this.btnResetFilters);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnPrintPickList);
            this.Controls.Add(this.cmbMultipleItems);
            this.Controls.Add(this.lblMultipleItems);
            this.Controls.Add(this.cmbPickingBatchDrp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbUrgentOrders);
            this.Controls.Add(this.cmbPickListPrinted);
            this.Controls.Add(this.lblPickListPrinted);
            this.Controls.Add(this.lblSelectedOrdersStatus);
            this.Controls.Add(this.PicError);
            this.Controls.Add(this.PicLoading);
            this.Controls.Add(this.PicRight);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblRequired);
            this.Controls.Add(this.grdOrders);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.lblOrder);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AllOpenOrders";
            this.Text = "All Open Orders";
            ((System.ComponentModel.ISupportInitialize)(this.grdOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicError)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView grdOrders;
        private System.Windows.Forms.Label lblRequired;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.PictureBox PicRight;
        private System.Windows.Forms.PictureBox PicLoading;
        private System.Windows.Forms.PictureBox PicError;
        private System.Windows.Forms.Label lblSelectedOrdersStatus;
        private System.Windows.Forms.Label lblPickListPrinted;
        private System.Windows.Forms.ComboBox cmbPickListPrinted;
        private System.Windows.Forms.ComboBox cmbUrgentOrders;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbPickingBatchDrp;
        private System.Windows.Forms.Label lblMultipleItems;
        private System.Windows.Forms.ComboBox cmbMultipleItems;
        private System.Windows.Forms.Button btnPrintPickList;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnResetFilters;
        private System.Windows.Forms.CheckBox chkAutoRefresh;
        private System.Windows.Forms.ComboBox cmbAutoRefreshInterval;
        private System.Windows.Forms.Label lblLastAutoUpdate;
    }
}