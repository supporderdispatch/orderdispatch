﻿using AForge.Video;
using AForge.Video.DirectShow;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class CaptureImage : Form
    {
        public CaptureImage()
        {
            InitializeComponent();

        }
        private FilterInfoCollection CaptureDevice;
        private VideoCaptureDevice finalFrame;
        private VideoCapabilities[] videoCapabilities;
        private VideoCapabilities[] snapshotCapabilities;
        [System.ComponentModel.DefaultValue(false)]
        public bool blIsImageCaptured { get; set; }
        public void populateControls()
        {
            CaptureDevice = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo device in CaptureDevice)
            {
                ddlVideoDevice.Items.Add(device.Name);
            }
            ddlVideoDevice.SelectedIndex = 0;
            finalFrame = new VideoCaptureDevice(CaptureDevice[ddlVideoDevice.SelectedIndex].MonikerString);
            EnumeratedFrameSizes(this.finalFrame);
            PopulateCameraSettings();
        }
        private void PopulateCameraSettings() {
            DataTable objdt = new DataTable();
            objdt = new GeneralSettings().GetUpdateCameraSettings("GET");

            if (objdt.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(objdt.Rows[0]["VideoDevice"].ToString()))
                {
                    ddlVideoDevice.SelectedItem = objdt.Rows[0]["VideoDevice"].ToString();

                }

                if (!string.IsNullOrEmpty(objdt.Rows[0]["VideoResolution"].ToString()))
                {
                    ddlVideoResolution.SelectedItem = objdt.Rows[0]["VideoResolution"].ToString();
                }
                if (!string.IsNullOrEmpty(objdt.Rows[0]["SnapshotResolution"].ToString()))
                {
                    ddlSnapshotResolution.SelectedItem = objdt.Rows[0]["SnapshotResolution"].ToString();
                }
            }
            else
            {
                ddlVideoResolution.SelectedIndex = 0;
                ddlSnapshotResolution.SelectedIndex = 0;
            }
        }
        private void EnumeratedFrameSizes(VideoCaptureDevice VideoDevice) {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                videoCapabilities = finalFrame.VideoCapabilities;
                snapshotCapabilities = finalFrame.SnapshotCapabilities;

                foreach (VideoCapabilities capabilty in videoCapabilities)
                {
                    ddlVideoResolution.Items.Add(string.Format("{0} x {1}",
                        capabilty.FrameSize.Width, capabilty.FrameSize.Height));
                }

                foreach (VideoCapabilities capabilty in snapshotCapabilities)
                {
                    ddlSnapshotResolution.Items.Add(string.Format("{0} x {1}",
                        capabilty.FrameSize.Width, capabilty.FrameSize.Height));
                }

                if (videoCapabilities.Length == 0)
                {
                    ddlVideoResolution.Items.Add("Not supported");
                    // ddlVideoResolution.SelectedIndex = 0;
                }
                if (snapshotCapabilities.Length == 0)
                {
                    ddlSnapshotResolution.Items.Add("Not supported");
                    //   ddlSnapshotResolution.SelectedIndex = 0;
                }


            }
            finally
            {
                this.Cursor = Cursors.Default;
            }                        
        }
        private void CaptureImage_Load(object sender, EventArgs e)
        {
            populateControls();
            StartCamera();
            EnableConnectionControls(true);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            StartCamera();
        }
        public void StartCamera() {
            if (finalFrame != null)
            {
                if ((videoCapabilities != null) && (videoCapabilities.Length != 0))
                {
                    finalFrame.VideoResolution = videoCapabilities[ddlVideoResolution.SelectedIndex];
                }

                if ((snapshotCapabilities != null) && (snapshotCapabilities.Length != 0))
                {
                    finalFrame.ProvideSnapshots = true;
                    finalFrame.SnapshotResolution = snapshotCapabilities[ddlSnapshotResolution.SelectedIndex];
                  //  this.picBoxCaptureImage.Height = finalFrame.SnapshotResolution.FrameSize.Height;
                  //  this.picBoxCaptureImage.Width = finalFrame.SnapshotResolution.FrameSize.Width;
                    //if (this.picBoxCaptureImage.Height >= panelCaptureImage.Size.Height)
                    //{
                    //    this.picBoxCaptureImage.Height = panelCaptureImage.Size.Height;
                    //}
                    //if (this.picBoxCaptureImage.Width >= panelCaptureImage.Size.Width)
                    //{
                    //    this.picBoxCaptureImage.Width = panelCaptureImage.Size.Width;
                    //}
                    //this.picBoxCaptureImage.Location = new Point((panelCaptureImage.Size.Width / 2) - (this.picBoxCaptureImage.Width / 2), (panelCaptureImage.Size.Height / 2) - (this.picBoxCaptureImage.Height / 2));
                    //finalFrame.SnapshotFrame += new NewFrameEventHandler(videoDevice_SnapshotFrame);
                    finalFrame = new VideoCaptureDevice(CaptureDevice[ddlVideoDevice.SelectedIndex].MonikerString);
                    finalFrame.NewFrame += new NewFrameEventHandler(finalFrame_NewFrame);
                    finalFrame.Start();
                    EnableConnectionControls(false);
                }
                else
                {
                    finalFrame.ProvideSnapshots = true;
                   
                  //  this.picBoxCaptureImage.Height = finalFrame.VideoResolution.FrameSize.Height;
                  //  this.picBoxCaptureImage.Width = finalFrame.VideoResolution.FrameSize.Width;
                    //if (this.picBoxCaptureImage.Height >= panelCaptureImage.Size.Height)
                    //{
                    //    this.picBoxCaptureImage.Height = panelCaptureImage.Size.Height;
                    //}
                    //if (this.picBoxCaptureImage.Width >= panelCaptureImage.Size.Width)
                    //{
                    //    this.picBoxCaptureImage.Width = panelCaptureImage.Size.Width;
                    //}
                    //int intXPoint = (panelCaptureImage.Width - this.picBoxCaptureImage.Width) / 2;
                    //int intYPoint = (panelCaptureImage.Height - this.picBoxCaptureImage.Height)/2;
                    //this.picBoxCaptureImage.Location = new Point(((panelCaptureImage.Width / 2) - (this.picBoxCaptureImage.Width / 2))+38, ((panelCaptureImage.Height / 2) - (this.picBoxCaptureImage.Height / 2))+112);
                    //this.picBoxCaptureImage.Location = new Point(intXPoint + panelCaptureImage.Location.X, intYPoint + panelCaptureImage.Location.Y);
                    //      finalFrame.SnapshotResolution = snapshotCapabilities[ddlSnapshotResolution.SelectedIndex];
                    //finalFrame.SnapshotFrame += new NewFrameEventHandler(videoDevice_SnapshotFrame);
                    finalFrame = new VideoCaptureDevice(CaptureDevice[ddlVideoDevice.SelectedIndex].MonikerString);
                    finalFrame.NewFrame += new NewFrameEventHandler(finalFrame_NewFrame);
                    finalFrame.Start();
                    EnableConnectionControls(false);
                    //videoSourcePlayer.VideoSource = videoDevice;
                    //videoSourcePlayer.Start();
                }
            }
        }
        void finalFrame_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            picBoxCaptureImage.Image = (Bitmap)eventArgs.Frame.Clone();
        }
        private void EnableConnectionControls(bool enable)
        {
            btnStart.Enabled = enable;
            btnStop.Enabled = !enable;
           //no capture button is always enabled.
            // btnCapture.Enabled = !enable;
            //btnCapture.Enabled = (!enable) && (snapshotCapabilities.Length != 0);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            Disconnect();
        }

        // disconnect from video device
        private void Disconnect()
        {
            if (this.picBoxCaptureImage.Image != null)
            {
                // stop video device
                finalFrame.SignalToStop();
                finalFrame.WaitForStop();
                
            //    finalFrame.VideoResolution.FrameSize ="810, 275" ; 
                Graphics graphic = Graphics.FromImage(this.picBoxCaptureImage.Image);
                this.picBoxCaptureImage.Image = null;
              
                graphic.Clear(Color.Black);
                if (finalFrame.ProvideSnapshots)
                {
                    finalFrame.SnapshotFrame -= new NewFrameEventHandler(finalFrame_NewFrame);
                }
                EnableConnectionControls(true);
            }
        }

        private void CaptureImage_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form frmSaveImage = Application.OpenForms["SaveImage"];
            if (frmSaveImage != null)
            {
                frmSaveImage.Close();
            }
            e.Cancel = false;
            Disconnect();        
        }

        private void btnCapture_Click(object sender, EventArgs e)       
        {
            if (this.picBoxCaptureImage.Image != null)
            {
                GlobalVariablesAndMethods.LogHistory("&&Capturing Image");
                SaveImage objSaveImage = new SaveImage((Bitmap)picBoxCaptureImage.Image.Clone());
                objSaveImage.ShowDialog();
                if (objSaveImage.blIsImageSaved) {
                    this.blIsImageCaptured = objSaveImage.blIsImageSaved;
                    this.Close();
                }
            }
        }
    }
}
