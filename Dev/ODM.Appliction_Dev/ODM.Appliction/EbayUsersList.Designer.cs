﻿namespace ODM.Appliction
{
    partial class EbayUsersList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EbayUsersList));
            this.grdEbayUsersList = new System.Windows.Forms.DataGridView();
            this.lblNoRecords = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdEbayUsersList)).BeginInit();
            this.SuspendLayout();
            // 
            // grdEbayUsersList
            // 
            this.grdEbayUsersList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEbayUsersList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdEbayUsersList.Location = new System.Drawing.Point(0, 0);
            this.grdEbayUsersList.Name = "grdEbayUsersList";
            this.grdEbayUsersList.Size = new System.Drawing.Size(912, 489);
            this.grdEbayUsersList.TabIndex = 0;
            this.grdEbayUsersList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.grdEbayUsersList_MouseClick);
            // 
            // lblNoRecords
            // 
            this.lblNoRecords.AutoSize = true;
            this.lblNoRecords.Location = new System.Drawing.Point(406, 218);
            this.lblNoRecords.Name = "lblNoRecords";
            this.lblNoRecords.Size = new System.Drawing.Size(97, 13);
            this.lblNoRecords.TabIndex = 1;
            this.lblNoRecords.Text = "No Records Found";
            this.lblNoRecords.Visible = false;
            // 
            // EbayUsersList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 489);
            this.Controls.Add(this.lblNoRecords);
            this.Controls.Add(this.grdEbayUsersList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EbayUsersList";
            this.Text = "EbayUsersList";
            ((System.ComponentModel.ISupportInitialize)(this.grdEbayUsersList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdEbayUsersList;
        private System.Windows.Forms.Label lblNoRecords;
    }
}