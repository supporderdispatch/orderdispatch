﻿using Microsoft.ApplicationBlocks.Data;
using ODM.Data.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction
{
    public partial class SpringCourierSettings_New : Form
    {
         string _CourierName = "";
        string _CourierID = "";
        SqlConnection connection;
        GlobalVariablesAndMethods GlobalVar = new GlobalVariablesAndMethods();
        string strDefaultPrinter = "";
       
        public SpringCourierSettings_New()
        {
            InitializeComponent();
            BindGrid();
        }
        public SpringCourierSettings_New(string strCourierName, string strCourierID)
        {
            InitializeComponent();
            _CourierName = strCourierName;
            _CourierID = strCourierID;
            PopulateControls();
            BindGrid();
        }
        private void PopulateControls()
        {
            try
            {
                DataTable ObjdtCourier = new DataTable();
                ObjdtCourier = GlobalVar.GetCourierDetailsByNameOrId(_CourierName, _CourierID);
                if (ObjdtCourier.Rows.Count > 0)
                {
                   
                    txtLiveClientID.Text = Convert.ToString(ObjdtCourier.Rows[0]["LiveClientID"]);
                    txtLiveSecret.Text = Convert.ToString(ObjdtCourier.Rows[0]["LiveSecret"]);
                    txtTestClientID.Text = Convert.ToString(ObjdtCourier.Rows[0]["TestClientID"]);
                    txtTestSecret.Text = Convert.ToString(ObjdtCourier.Rows[0]["TestSecret"]);
                    txtShopID.Text = Convert.ToString(ObjdtCourier.Rows[0]["ShopID"]);
                    txtContractID.Text = Convert.ToString(ObjdtCourier.Rows[0]["ContractID"]);
                    txtTestShopID.Text = Convert.ToString(ObjdtCourier.Rows[0]["TestShopID"]);
                    txtTestContractID.Text = Convert.ToString(ObjdtCourier.Rows[0]["TestContractID"]);
                    txtWeight.Text = Convert.ToString(ObjdtCourier.Rows[0]["DefaultWeight"]);
                    string strIsLive = Convert.ToString(ObjdtCourier.Rows[0]["IsLive"]);

                    chkIsLive.Checked = string.IsNullOrWhiteSpace(strIsLive) ? false : Convert.ToBoolean(strIsLive);

                   

                   
                }
                ddlPrinter.DataSource = GlobalVar.AttachedPrinters(ref strDefaultPrinter);


                ddlPrinter.SelectedItem = !string.IsNullOrWhiteSpace(GlobalVariablesAndMethods.SelectedPrinterNameForSpring_New) ?
                                          GlobalVariablesAndMethods.SelectedPrinterNameForSpring_New : strDefaultPrinter;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void BindGrid()
        {
            try
            {
                connection = new SqlConnection();
                DataTable objDtGridData = new DataTable();
                DataTable objCourierList = new DataTable();
                connection.ConnectionString = MakeConnection.GetConnection();
                objDtGridData = SqlHelper.ExecuteDataset(connection, "GetSpringMappedCouriers_New").Tables[0];
                objCourierList = SqlHelper.ExecuteDataset(connection, "GetAllSpringCourierToMapWithServices_New").Tables[0];

                if (objDtGridData.Rows.Count > 0 && !string.IsNullOrWhiteSpace(Convert.ToString(objDtGridData.Rows[0]["ID"])))
                {
                    grdMappedServices.Visible = true;
                    grdMappedServices.AllowUserToAddRows = false;
                    grdMappedServices.DataSource = objDtGridData;
                    grdMappedServices.AutoGenerateColumns = false;
                    grdMappedServices.Columns[0].Visible = false;
                    grdMappedServices.Columns[1].Visible = false;
                    grdMappedServices.Columns[2].Visible = false;
                    grdMappedServices.Columns[4].Visible = false;
                    grdMappedServices.Columns[3].ReadOnly = true;
                    grdMappedServices.Columns[3].Width = 370;

                    DataGridViewComboBoxColumn ddlCouriers = new DataGridViewComboBoxColumn();
                    ddlCouriers.HeaderText = "Courier Name";
                    ddlCouriers.Name = "Courier_Name";
                    ddlCouriers.ValueMember = "CourierID";
                    ddlCouriers.DataPropertyName = "MapedCourierId";
                    ddlCouriers.DisplayMember = "CourierName";
                    grdMappedServices.Columns.Add(ddlCouriers);
                    ddlCouriers.DataSource = objCourierList;
                    grdMappedServices.Columns[5].Width = 370;
                    lblMessage.Visible = false;
                }
                else
                {
                    grdMappedServices.Visible = false;
                    lblMessage.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An Error Occurred Contact the Administrator");
            }
            finally
            {
                connection.Close();
            }
        }

        private void btnSenderDetails_Click(object sender, EventArgs e)
        {
            Form frmSenderDetails = Application.OpenForms["SenderDetailsForSpring"];
            if (frmSenderDetails != null)
            {
                frmSenderDetails.Close();
            }
            SenderDetailsForSpring objSenderDetailsForSpring = new SenderDetailsForSpring(_CourierName, _CourierID);
            objSenderDetailsForSpring.Show();
        }
        private bool ValidateControls()
        {
            bool IsValid = true;
            if (string.IsNullOrWhiteSpace(txtLiveClientID.Text))
            {
                lblLiveClientIDRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblLiveClientIDRequired.Visible = false;
            }

            
            if (string.IsNullOrWhiteSpace(txtLiveSecret.Text))
            {
                lblLiveSecretRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblLiveSecretRequired.Visible = false;
            }


            if (string.IsNullOrWhiteSpace(txtShopID.Text))
            {
                lblShopIDRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblShopIDRequired.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(txtContractID.Text))
            {
                lblContractIDRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblContractIDRequired.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(txtTestShopID.Text))
            {
                lblTestShopIDRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblTestShopIDRequired.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(txtTestContractID.Text))
            {
                lblTestContractIDRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblTestContractIDRequired.Visible = false;
            }



            if (string.IsNullOrWhiteSpace(txtTestClientID.Text))
            {
                lblTestClientIDRequired.Visible = true;
                IsValid = false;
            }
            else
            {
                lblTestClientIDRequired.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(txtWeight.Text))
            {
                lblWeightRequired.Visible = true;
                lblWeightInvalid.Visible = false;
                IsValid = false;
            }
            else
            {
                double DefaultWeight = 0.00;
                bool IsValidWeight = double.TryParse(txtWeight.Text, out DefaultWeight);
                if (!IsValidWeight)
                {
                    lblWeightInvalid.Visible = true;
                    IsValid = false;
                }
                else
                {
                    lblWeightInvalid.Visible = false;
                }
                lblWeightRequired.Visible = false;
            }
            return IsValid;
        }
        private void SaveSettings()
        {
            connection = new SqlConnection();
            try
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.ConnectionString = MakeConnection.GetConnection();
                connection.Open();
                Guid CourierID = new Guid(_CourierID);
                SqlParameter[] objParams = new SqlParameter[13];

                objParams[0] = new SqlParameter("@CourierId", SqlDbType.UniqueIdentifier);
                objParams[0].Value = CourierID;

                objParams[1] = new SqlParameter("@CourierName", SqlDbType.VarChar, 500);
                objParams[1].Value = _CourierName;

                objParams[2] = new SqlParameter("@LiveClientID", SqlDbType.VarChar, 500);
                objParams[2].Value = txtLiveClientID.Text;

                objParams[3] = new SqlParameter("@LiveSecret", SqlDbType.VarChar, 500);
                objParams[3].Value = txtLiveSecret.Text;

                objParams[4] = new SqlParameter("@TestClientID", SqlDbType.VarChar, 500);
                objParams[4].Value = txtTestClientID.Text;

                objParams[5] = new SqlParameter("@TestSecret", SqlDbType.VarChar, 500);
                objParams[5].Value = txtTestSecret.Text;

                objParams[6] = new SqlParameter("@ShopID", SqlDbType.VarChar, 500);
                objParams[6].Value = txtShopID.Text;

                objParams[7] = new SqlParameter("@ContractID", SqlDbType.VarChar, 500);
                objParams[7].Value = txtContractID.Text;

                objParams[8] = new SqlParameter("@TestShopID", SqlDbType.VarChar, 500);
                objParams[8].Value = txtTestShopID.Text;

                objParams[9] = new SqlParameter("@TestContractID", SqlDbType.VarChar, 500);
                objParams[9].Value = txtTestContractID.Text;

                objParams[10] = new SqlParameter("@PrinterName", SqlDbType.VarChar, 5000);
                objParams[10].Value = ddlPrinter.SelectedItem;

                objParams[11] = new SqlParameter("@IsLive", SqlDbType.Bit);
                objParams[11].Value = chkIsLive.Checked;

                objParams[12] = new SqlParameter("@DefaultWeight", SqlDbType.Decimal);
                objParams[12].Value = txtWeight.Text;

                //objParams[11] = new SqlParameter("@IsBoseLogin", SqlDbType.Bit);
                //objParams[11].Value = chkIsBoseLogin.Checked;

                SqlHelper.ExecuteNonQuery(connection, "AddUpdateSpring_New", objParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        private void MapCourierToService()
        {
            try
            {
                int intResult = 100;
                if (grdMappedServices.Rows.Count > 0)
                {
                    for (int i = 0; i < grdMappedServices.Rows.Count; i++)
                    {
                        string strSelectedCourierID = Convert.ToString(grdMappedServices.Rows[i].Cells["ID"].Value);
                        string strSelectedValue = Convert.ToString(grdMappedServices[5, i].Value);
                        string strServiceName = Convert.ToString(grdMappedServices.Rows[i].Cells["PostalServiceName"].Value);
                        if (strSelectedValue == "")
                        {
                            MessageBox.Show(string.Format("Please select courier for the service {0}", strServiceName));
                            intResult = -1;
                            break;
                        }
                        else
                        {
                            connection = new SqlConnection();
                            Guid MappedID = new Guid(strSelectedValue);
                            Guid ID = new Guid(strSelectedCourierID);
                            Guid DummyID = new Guid();
                            connection.ConnectionString = MakeConnection.GetConnection();
                            SqlParameter[] objParams = new SqlParameter[6];

                            objParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                            objParams[0].Value = ID;

                            objParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                            objParams[1].Value = DummyID;

                            objParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                            objParams[2].Value = DummyID;

                            objParams[3] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                            objParams[3].Value = MappedID;

                            objParams[4] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                            objParams[4].Value = "UPDATE";

                            objParams[5] = new SqlParameter("@Result", SqlDbType.Int);
                            objParams[5].Direction = ParameterDirection.Output;

                            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteServicesToMapSpringCourier_New", objParams);

                            intResult = Convert.ToInt32(objParams[5].Value);
                        }
                    }
                    if (intResult == 1)
                    {
                        this.Close();
                        MessageBox.Show("Settings Saved Successfully !");
                    }
                }
                else
                {
                    this.Close();
                    MessageBox.Show("Settings Saved Successfully !");
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }
        private void DeleteService()
        {
            try
            {
                int RowIndex = 0;
                if (grdMappedServices.CurrentCell != null)
                {
                    RowIndex = Convert.ToInt32(grdMappedServices.CurrentCell.RowIndex);
                    string strSelectedCourierID = Convert.ToString(grdMappedServices.Rows[RowIndex].Cells[0].Value);
                    connection.ConnectionString = MakeConnection.GetConnection();
                    Guid ID = new Guid(strSelectedCourierID);
                    Guid DummyID = new Guid();

                    SqlParameter[] objParams = new SqlParameter[6];

                    objParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                    objParams[0].Value = ID;

                    objParams[1] = new SqlParameter("@CourierID", SqlDbType.UniqueIdentifier);
                    objParams[1].Value = DummyID;

                    objParams[2] = new SqlParameter("@ServiceID", SqlDbType.UniqueIdentifier);
                    objParams[2].Value = DummyID;

                    objParams[3] = new SqlParameter("@MapedCourierID", SqlDbType.UniqueIdentifier);
                    objParams[3].Value = DummyID;

                    objParams[4] = new SqlParameter("@Status", SqlDbType.VarChar, 50);
                    objParams[4].Value = "DELETE";

                    objParams[5] = new SqlParameter("@Result", SqlDbType.Int);
                    objParams[5].Direction = ParameterDirection.Output;

                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddUpdateDeleteServicesToMapSpringCourier_New", objParams);

                    int intResult = Convert.ToInt32(objParams[5].Value);
                    if (intResult == 2)
                    {
                        BindGrid();
                        MessageBox.Show("Record deleted successfully !");
                    }
                }
                else
                {
                    MessageBox.Show("Please select a record to delete");
                }
            }
            catch (Exception ex)
            {
                string strError = ex.Message;
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Form objAddService = Application.OpenForms["AddServices"];
            if (objAddService == null)
            {
                AddServices objServices = new AddServices(_CourierName, _CourierID);
                objServices.Show();
            }
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
            {
                SaveSettings();
                MapCourierToService();
                GlobalVariablesAndMethods.SelectedPrinterNameForSpring_New = ddlPrinter.SelectedItem.ToString();           
                GlobalVar.AddUpdatePrinterName();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure, you want to delete Postal Service ?", "Delete Postal Service", MessageBoxButtons.YesNo);
            if (result.Equals(DialogResult.Yes))
            {
                DeleteService();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Form frmAddService = Application.OpenForms["AddServices"];
            if (frmAddService != null)
            {
                frmAddService.Close();
            }
            this.Close();

        }
    }
}
