﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ODM.Appliction.Updates
{
    public class Updator
    {
        public static System.Timers.Timer updateChecker = new System.Timers.Timer();

        public static void CheckForUpdates()
        {
            new Updator().CheckUpdates();
            updateChecker.Interval = TimeSpan.FromMinutes(2).TotalMilliseconds;
            updateChecker.Elapsed += (s, e) => 
            {
                var updator = Application.OpenForms["UpdateWindow"];
                if (updator == null)
                {
                    new Updator().CheckUpdates();
                }
                else
                {
                    updator.WindowState = FormWindowState.Normal;
                    updator.Focus();
                }                
            };
            updateChecker.Start();
        }

        public async void CheckUpdates()
        {
            await Task.Run(() =>
            {
                var latestAppVersionDetails = GetLatestAppVersionDetails();
                if (GetRunningAppVersion()<GetLatesAppVersion(latestAppVersionDetails.AppVersion))
                {
                    LaunchUpdator(latestAppVersionDetails);
                }
            });
        }

        private void LaunchUpdator(Entities.AppVersionDetails latestAppVersionDetails)
        {
            new ODM.Appliction.Updates.UpdateWindow(latestAppVersionDetails).ShowDialog();
        }

        private Version GetLatesAppVersion(string ver)
        {
            var version = ver.Split('.');
            return new Version(Convert.ToInt32(version[0]),Convert.ToInt32(version[1]),Convert.ToInt32(version[2]),Convert.ToInt32(version[3]));
        }

        private ODM.Entities.AppVersionDetails GetLatestAppVersionDetails()
        {
            return ODM.Core.AppUpdator.GetInstance().GetLatestAppVersionDetails();
        }

        private Version GetRunningAppVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        }
    }
}
