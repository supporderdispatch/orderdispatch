﻿namespace ODM.Appliction.Updates
{
    partial class PushUpdates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PushUpdates));
            this.btnApprove = new System.Windows.Forms.Button();
            this.btnReject = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtApproveRemark = new System.Windows.Forms.RichTextBox();
            this.txtApproveDownloadUrl = new System.Windows.Forms.TextBox();
            this.txtApproveVersion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnAddVersion = new System.Windows.Forms.Button();
            this.txtAddRemark = new System.Windows.Forms.RichTextBox();
            this.txtAddDownloadUrl = new System.Windows.Forms.TextBox();
            this.txtAddAppVersion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnApprove
            // 
            this.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApprove.Location = new System.Drawing.Point(200, 239);
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Size = new System.Drawing.Size(75, 23);
            this.btnApprove.TabIndex = 0;
            this.btnApprove.Text = "Approve";
            this.btnApprove.UseVisualStyleBackColor = true;
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // btnReject
            // 
            this.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReject.Location = new System.Drawing.Point(281, 239);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(75, 23);
            this.btnReject.TabIndex = 1;
            this.btnReject.Text = "Reject";
            this.btnReject.UseVisualStyleBackColor = true;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(610, 305);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage1.Controls.Add(this.txtApproveRemark);
            this.tabPage1.Controls.Add(this.txtApproveDownloadUrl);
            this.tabPage1.Controls.Add(this.txtApproveVersion);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btnReject);
            this.tabPage1.Controls.Add(this.btnApprove);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(602, 279);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Approve New Version";
            // 
            // txtApproveRemark
            // 
            this.txtApproveRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApproveRemark.Location = new System.Drawing.Point(186, 141);
            this.txtApproveRemark.Name = "txtApproveRemark";
            this.txtApproveRemark.ReadOnly = true;
            this.txtApproveRemark.Size = new System.Drawing.Size(170, 51);
            this.txtApproveRemark.TabIndex = 7;
            this.txtApproveRemark.Text = "";
            // 
            // txtApproveDownloadUrl
            // 
            this.txtApproveDownloadUrl.Location = new System.Drawing.Point(186, 100);
            this.txtApproveDownloadUrl.Name = "txtApproveDownloadUrl";
            this.txtApproveDownloadUrl.ReadOnly = true;
            this.txtApproveDownloadUrl.Size = new System.Drawing.Size(170, 20);
            this.txtApproveDownloadUrl.TabIndex = 6;
            // 
            // txtApproveVersion
            // 
            this.txtApproveVersion.Location = new System.Drawing.Point(186, 57);
            this.txtApproveVersion.Name = "txtApproveVersion";
            this.txtApproveVersion.ReadOnly = true;
            this.txtApproveVersion.Size = new System.Drawing.Size(170, 20);
            this.txtApproveVersion.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Remark";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "DownloadUrl";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Version";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage2.Controls.Add(this.btnAddVersion);
            this.tabPage2.Controls.Add(this.txtAddRemark);
            this.tabPage2.Controls.Add(this.txtAddDownloadUrl);
            this.tabPage2.Controls.Add(this.txtAddAppVersion);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(602, 279);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Add New Version";
            // 
            // btnAddVersion
            // 
            this.btnAddVersion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddVersion.Location = new System.Drawing.Point(182, 214);
            this.btnAddVersion.Name = "btnAddVersion";
            this.btnAddVersion.Size = new System.Drawing.Size(75, 23);
            this.btnAddVersion.TabIndex = 14;
            this.btnAddVersion.Text = "Add";
            this.btnAddVersion.UseVisualStyleBackColor = true;
            this.btnAddVersion.Click += new System.EventHandler(this.btnAddVersion_Click);
            // 
            // txtAddRemark
            // 
            this.txtAddRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddRemark.Location = new System.Drawing.Point(182, 138);
            this.txtAddRemark.Name = "txtAddRemark";
            this.txtAddRemark.Size = new System.Drawing.Size(170, 51);
            this.txtAddRemark.TabIndex = 13;
            this.txtAddRemark.Text = "";
            // 
            // txtAddDownloadUrl
            // 
            this.txtAddDownloadUrl.Location = new System.Drawing.Point(182, 97);
            this.txtAddDownloadUrl.Name = "txtAddDownloadUrl";
            this.txtAddDownloadUrl.Size = new System.Drawing.Size(170, 20);
            this.txtAddDownloadUrl.TabIndex = 12;
            // 
            // txtAddAppVersion
            // 
            this.txtAddAppVersion.Location = new System.Drawing.Point(182, 54);
            this.txtAddAppVersion.Name = "txtAddAppVersion";
            this.txtAddAppVersion.Size = new System.Drawing.Size(170, 20);
            this.txtAddAppVersion.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Remark";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "DownloadUrl";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "version";
            // 
            // PushUpdates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 329);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PushUpdates";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PushUpdates";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnApprove;
        private System.Windows.Forms.Button btnReject;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox txtApproveRemark;
        private System.Windows.Forms.TextBox txtApproveDownloadUrl;
        private System.Windows.Forms.TextBox txtApproveVersion;
        private System.Windows.Forms.Button btnAddVersion;
        private System.Windows.Forms.RichTextBox txtAddRemark;
        private System.Windows.Forms.TextBox txtAddDownloadUrl;
        private System.Windows.Forms.TextBox txtAddAppVersion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}