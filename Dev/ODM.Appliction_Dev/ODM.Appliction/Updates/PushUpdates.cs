﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Entities;

namespace ODM.Appliction.Updates
{
    public partial class PushUpdates : Form
    {
        private readonly Core.AppUpdator _AppUpator;

        public PushUpdates()
        {
            InitializeComponent();
            _AppUpator = new Core.AppUpdator();
            LoadAppVersionToBeApproved();
        }

        private void LoadAppVersionToBeApproved()
        {
            var appVersion = _AppUpator.GetLatestAppVersionToBeApprove();
            if (!appVersion.Equals(new Entities.AppVersionDetails()))
            {
                txtApproveVersion.Text = appVersion.AppVersion;
                txtApproveDownloadUrl.Text = appVersion.DownloadUrl;
                txtApproveRemark.Text = appVersion.Remark;
            }
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            var appVersion = new AppVersionDetails();
            appVersion.AppVersion = txtApproveVersion.Text.Trim();
            _AppUpator.ApproveAppVersion(appVersion);
            Close();
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            var appVersion = new AppVersionDetails();
            appVersion.AppVersion = txtApproveVersion.Text.Trim();
            _AppUpator.RejectAppVersion(appVersion);
            Close();
        }

        private void btnAddVersion_Click(object sender, EventArgs e)
        {
            var appVersion = new AppVersionDetails();
            appVersion.AppVersion = txtAddAppVersion.Text;
            appVersion.DownloadUrl = txtAddDownloadUrl.Text;
            appVersion.Remark = txtAddRemark.Text;
            _AppUpator.AddAppVersion(appVersion);
            Close();
        }
    }
}
