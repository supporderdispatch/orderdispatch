﻿
namespace ODM.Appliction
{
    partial class UserStatisticsReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserStatisticsReport));
            this.btnShowTodayStat = new System.Windows.Forms.RadioButton();
            this.btnShowWeeklyStats = new System.Windows.Forms.RadioButton();
            this.picLoading = new System.Windows.Forms.PictureBox();
            this.pnlTodayUserStatistics = new System.Windows.Forms.Panel();
            this.lblAvgPerOrderTimeTaken = new System.Windows.Forms.Label();
            this.lblOrderPerHourRate = new System.Windows.Forms.Label();
            this.lblTotalOrdersProcessed = new System.Windows.Forms.Label();
            this.lblTotalLoggedInTime = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.grdRangeReportView = new System.Windows.Forms.DataGridView();
            this.btnShowCustomRangeStat = new System.Windows.Forms.RadioButton();
            this.dateTimePickFrom = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickTo = new System.Windows.Forms.DateTimePicker();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.lblToDate = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.cmbAutoRefreshInterval = new System.Windows.Forms.ComboBox();
            this.chkAutoRefresh = new System.Windows.Forms.CheckBox();
            this.lblLastAutoUpdate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).BeginInit();
            this.pnlTodayUserStatistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRangeReportView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnShowTodayStat
            // 
            this.btnShowTodayStat.AutoSize = true;
            this.btnShowTodayStat.Checked = true;
            this.btnShowTodayStat.Location = new System.Drawing.Point(16, 8);
            this.btnShowTodayStat.Name = "btnShowTodayStat";
            this.btnShowTodayStat.Size = new System.Drawing.Size(51, 17);
            this.btnShowTodayStat.TabIndex = 28;
            this.btnShowTodayStat.TabStop = true;
            this.btnShowTodayStat.Text = "today";
            this.btnShowTodayStat.UseVisualStyleBackColor = true;
            this.btnShowTodayStat.CheckedChanged += new System.EventHandler(this.btnShowTodayStat_CheckedChanged);
            // 
            // btnShowWeeklyStats
            // 
            this.btnShowWeeklyStats.AutoSize = true;
            this.btnShowWeeklyStats.Location = new System.Drawing.Point(73, 8);
            this.btnShowWeeklyStats.Name = "btnShowWeeklyStats";
            this.btnShowWeeklyStats.Size = new System.Drawing.Size(58, 17);
            this.btnShowWeeklyStats.TabIndex = 29;
            this.btnShowWeeklyStats.Text = "weekly";
            this.btnShowWeeklyStats.UseVisualStyleBackColor = true;
            this.btnShowWeeklyStats.CheckedChanged += new System.EventHandler(this.btnShowWeeklyStats_CheckedChanged);
            // 
            // picLoading
            // 
            this.picLoading.Image = ((System.Drawing.Image)(resources.GetObject("picLoading.Image")));
            this.picLoading.Location = new System.Drawing.Point(243, 8);
            this.picLoading.Name = "picLoading";
            this.picLoading.Size = new System.Drawing.Size(28, 28);
            this.picLoading.TabIndex = 30;
            this.picLoading.TabStop = false;
            // 
            // pnlTodayUserStatistics
            // 
            this.pnlTodayUserStatistics.Controls.Add(this.lblAvgPerOrderTimeTaken);
            this.pnlTodayUserStatistics.Controls.Add(this.lblOrderPerHourRate);
            this.pnlTodayUserStatistics.Controls.Add(this.lblTotalOrdersProcessed);
            this.pnlTodayUserStatistics.Controls.Add(this.lblTotalLoggedInTime);
            this.pnlTodayUserStatistics.Controls.Add(this.label4);
            this.pnlTodayUserStatistics.Controls.Add(this.label3);
            this.pnlTodayUserStatistics.Controls.Add(this.label2);
            this.pnlTodayUserStatistics.Controls.Add(this.label5);
            this.pnlTodayUserStatistics.Controls.Add(this.label1);
            this.pnlTodayUserStatistics.Controls.Add(this.label6);
            this.pnlTodayUserStatistics.Controls.Add(this.label8);
            this.pnlTodayUserStatistics.Controls.Add(this.lblUserName);
            this.pnlTodayUserStatistics.Location = new System.Drawing.Point(12, 62);
            this.pnlTodayUserStatistics.Name = "pnlTodayUserStatistics";
            this.pnlTodayUserStatistics.Size = new System.Drawing.Size(745, 427);
            this.pnlTodayUserStatistics.TabIndex = 5;
            // 
            // lblAvgPerOrderTimeTaken
            // 
            this.lblAvgPerOrderTimeTaken.AutoSize = true;
            this.lblAvgPerOrderTimeTaken.Location = new System.Drawing.Point(371, 69);
            this.lblAvgPerOrderTimeTaken.Name = "lblAvgPerOrderTimeTaken";
            this.lblAvgPerOrderTimeTaken.Size = new System.Drawing.Size(0, 13);
            this.lblAvgPerOrderTimeTaken.TabIndex = 26;
            // 
            // lblOrderPerHourRate
            // 
            this.lblOrderPerHourRate.AutoSize = true;
            this.lblOrderPerHourRate.Location = new System.Drawing.Point(289, 69);
            this.lblOrderPerHourRate.Name = "lblOrderPerHourRate";
            this.lblOrderPerHourRate.Size = new System.Drawing.Size(0, 13);
            this.lblOrderPerHourRate.TabIndex = 25;
            // 
            // lblTotalOrdersProcessed
            // 
            this.lblTotalOrdersProcessed.AutoSize = true;
            this.lblTotalOrdersProcessed.Location = new System.Drawing.Point(191, 69);
            this.lblTotalOrdersProcessed.Name = "lblTotalOrdersProcessed";
            this.lblTotalOrdersProcessed.Size = new System.Drawing.Size(0, 13);
            this.lblTotalOrdersProcessed.TabIndex = 24;
            // 
            // lblTotalLoggedInTime
            // 
            this.lblTotalLoggedInTime.AutoSize = true;
            this.lblTotalLoggedInTime.Location = new System.Drawing.Point(94, 69);
            this.lblTotalLoggedInTime.Name = "lblTotalLoggedInTime";
            this.lblTotalLoggedInTime.Size = new System.Drawing.Size(0, 13);
            this.lblTotalLoggedInTime.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(359, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Avg Per Order Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(267, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Order/Hour Rate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(168, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Orders Processed";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(83, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Logged In Time";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(390, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "(min)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(289, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "(min)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(94, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "(hh:mm:ss)";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(3, 69);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 13);
            this.lblUserName.TabIndex = 6;
            // 
            // grdRangeReportView
            // 
            this.grdRangeReportView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdRangeReportView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdRangeReportView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.grdRangeReportView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grdRangeReportView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdRangeReportView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdRangeReportView.Location = new System.Drawing.Point(1, 79);
            this.grdRangeReportView.Name = "grdRangeReportView";
            this.grdRangeReportView.Size = new System.Drawing.Size(744, 410);
            this.grdRangeReportView.TabIndex = 32;
            // 
            // btnShowCustomRangeStat
            // 
            this.btnShowCustomRangeStat.AutoSize = true;
            this.btnShowCustomRangeStat.Location = new System.Drawing.Point(137, 8);
            this.btnShowCustomRangeStat.Name = "btnShowCustomRangeStat";
            this.btnShowCustomRangeStat.Size = new System.Drawing.Size(95, 17);
            this.btnShowCustomRangeStat.TabIndex = 33;
            this.btnShowCustomRangeStat.TabStop = true;
            this.btnShowCustomRangeStat.Text = "Custom Range";
            this.btnShowCustomRangeStat.UseVisualStyleBackColor = true;
            // 
            // dateTimePickFrom
            // 
            this.dateTimePickFrom.Location = new System.Drawing.Point(56, 38);
            this.dateTimePickFrom.Name = "dateTimePickFrom";
            this.dateTimePickFrom.Size = new System.Drawing.Size(115, 20);
            this.dateTimePickFrom.TabIndex = 34;
            this.dateTimePickFrom.ValueChanged += new System.EventHandler(this.dateTimePickFrom_ValueChanged);
            // 
            // dateTimePickTo
            // 
            this.dateTimePickTo.Location = new System.Drawing.Point(248, 38);
            this.dateTimePickTo.Name = "dateTimePickTo";
            this.dateTimePickTo.Size = new System.Drawing.Size(135, 20);
            this.dateTimePickTo.TabIndex = 35;
            this.dateTimePickTo.ValueChanged += new System.EventHandler(this.dateTimePickTo_ValueChanged);
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Location = new System.Drawing.Point(20, 43);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(30, 13);
            this.lblFromDate.TabIndex = 36;
            this.lblFromDate.Text = "From";
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.Location = new System.Drawing.Point(222, 43);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(20, 13);
            this.lblToDate.TabIndex = 37;
            this.lblToDate.Text = "To";
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRefresh.Location = new System.Drawing.Point(243, 4);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(58, 28);
            this.btnRefresh.TabIndex = 38;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Visible = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // cmbAutoRefreshInterval
            // 
            this.cmbAutoRefreshInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAutoRefreshInterval.FormattingEnabled = true;
            this.cmbAutoRefreshInterval.Location = new System.Drawing.Point(708, 15);
            this.cmbAutoRefreshInterval.Name = "cmbAutoRefreshInterval";
            this.cmbAutoRefreshInterval.Size = new System.Drawing.Size(38, 21);
            this.cmbAutoRefreshInterval.TabIndex = 39;
            this.cmbAutoRefreshInterval.SelectedIndexChanged += new System.EventHandler(this.cmbAutoRefreshInterval_SelectedIndexChanged);
            // 
            // chkAutoRefresh
            // 
            this.chkAutoRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkAutoRefresh.AutoSize = true;
            this.chkAutoRefresh.Checked = true;
            this.chkAutoRefresh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoRefresh.Location = new System.Drawing.Point(541, 17);
            this.chkAutoRefresh.Name = "chkAutoRefresh";
            this.chkAutoRefresh.Size = new System.Drawing.Size(163, 17);
            this.chkAutoRefresh.TabIndex = 40;
            this.chkAutoRefresh.Text = "Auto Refresh    Interval (min):";
            this.chkAutoRefresh.UseVisualStyleBackColor = true;
            this.chkAutoRefresh.CheckedChanged += new System.EventHandler(this.chkAutoRefresh_CheckedChanged);
            // 
            // lblLastAutoUpdate
            // 
            this.lblLastAutoUpdate.AutoSize = true;
            this.lblLastAutoUpdate.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblLastAutoUpdate.Location = new System.Drawing.Point(538, 38);
            this.lblLastAutoUpdate.Name = "lblLastAutoUpdate";
            this.lblLastAutoUpdate.Size = new System.Drawing.Size(105, 13);
            this.lblLastAutoUpdate.TabIndex = 41;
            this.lblLastAutoUpdate.Text = "last auto update time";
            // 
            // UserStatisticsReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 488);
            this.Controls.Add(this.lblLastAutoUpdate);
            this.Controls.Add(this.chkAutoRefresh);
            this.Controls.Add(this.cmbAutoRefreshInterval);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.lblToDate);
            this.Controls.Add(this.lblFromDate);
            this.Controls.Add(this.dateTimePickTo);
            this.Controls.Add(this.dateTimePickFrom);
            this.Controls.Add(this.btnShowCustomRangeStat);
            this.Controls.Add(this.grdRangeReportView);
            this.Controls.Add(this.picLoading);
            this.Controls.Add(this.btnShowWeeklyStats);
            this.Controls.Add(this.btnShowTodayStat);
            this.Controls.Add(this.pnlTodayUserStatistics);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserStatisticsReport";
            this.Text = "UserStatisticsReport";
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).EndInit();
            this.pnlTodayUserStatistics.ResumeLayout(false);
            this.pnlTodayUserStatistics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRangeReportView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton btnShowTodayStat;
        private System.Windows.Forms.RadioButton btnShowWeeklyStats;
        private System.Windows.Forms.Panel pnlTodayUserStatistics;
        private System.Windows.Forms.Label lblAvgPerOrderTimeTaken;
        private System.Windows.Forms.Label lblOrderPerHourRate;
        private System.Windows.Forms.Label lblTotalOrdersProcessed;
        private System.Windows.Forms.Label lblTotalLoggedInTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.PictureBox picLoading;
        private System.Windows.Forms.DataGridView grdRangeReportView;
        private System.Windows.Forms.RadioButton btnShowCustomRangeStat;
        private System.Windows.Forms.DateTimePicker dateTimePickFrom;
        private System.Windows.Forms.DateTimePicker dateTimePickTo;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.Label lblToDate;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ComboBox cmbAutoRefreshInterval;
        private System.Windows.Forms.CheckBox chkAutoRefresh;
        private System.Windows.Forms.Label lblLastAutoUpdate;

    }
}