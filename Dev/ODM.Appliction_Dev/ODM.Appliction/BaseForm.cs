﻿using ODM.Data.Entity;
using System.Windows.Forms;

namespace ODM.Appliction
{
   public  class BaseForm : Form
    {
        public static User LoginedUser { get; set; }

        public static  string ConfigString { get; set; }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.SuspendLayout();
            // 
            // BaseForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BaseForm";
            this.Text = "Base Form";
            this.ResumeLayout(false);

        }       

    }
}
