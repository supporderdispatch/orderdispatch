﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ODM.Data.Util;
namespace ODM.Appliction
{
    public partial class RuleConditions : Form
    {
        public RuleConditions()
        {
            InitializeComponent();
            BindGridForConditions();
        }

        private void btnAddNewCondition_Click(object sender, EventArgs e)
        {
            AddRuleCondition objAddRuleCondition = new AddRuleCondition();
            objAddRuleCondition.ShowDialog();
            BindGridForConditions();
        }
        private void BindGridForConditions()
        {
            string strRuleConditionID = "00000000-0000-0000-0000-000000000000";
            DataTable objDt = new DataTable();
            objDt = GlobalVariablesAndMethods.GetAddUpdateMessageRuleCondition("GET", strRuleConditionID, "");
            grdRuleConditions.DataSource = objDt;
            if (objDt.Rows.Count > 0) { 
            
            
            grdRuleConditions.Columns["ID"].Visible = false;
            grdRuleConditions.Columns["ConditionText"].Width = 200;
            grdRuleConditions.Columns["ModifiedOn"].Visible = false;
            }
        }
        private void grdRuleConditions_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string strConditionText = grdRuleConditions.CurrentRow.Cells["ConditionText"].Value.ToString();
            string strConditionID = grdRuleConditions.CurrentRow.Cells["ID"].Value.ToString();
            AddRuleCondition objAddRuleCondition = new AddRuleCondition("UPDATE",strConditionText, strConditionID);
            objAddRuleCondition.ShowDialog();
            BindGridForConditions();
        }
    }
}
