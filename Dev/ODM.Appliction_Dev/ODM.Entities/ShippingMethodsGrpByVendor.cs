﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities
{
    public class ShippingMethodsGrpByVendor
    {
        public string Vendor { get; set; }
        public List<string> ShippingMethods { get; set; }
    }
}
