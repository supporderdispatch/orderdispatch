﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities
{
    public class UserStatics
    {
        public Guid UserId { get; set; }
        public Guid SessionId { get; set; }
        public DateTime? LoginTime { get; set; }
        public DateTime? LogoutTime { get; set; }
        public List<OrderExecutionDetails> OrderDigest { get; set; }
    }
    public class OrderExecutionDetails
    {
        public string OrderId { get; set; }
        public double TimeTakenForExecution { get; set; }
        public bool Processed { get; set; }
    }
    public class UserStatDigest
    {
        public Guid UserId { get; set; }
        public DateTime LoginTime {get;set;}
        public DateTime LogoutTime {get;set;}
        public int TotalOrdersProcessed {get;set;}
        public decimal TotalPerProcessRate {get;set;}
    }
    public class FinalViewDigest
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public DateTime OfDate { get; set; }
        public int TotalOrderProcessed { get; set; }
        public TimeSpan TotalLoggedInTime { get; set; }
        public decimal HourlyPerOrderProcessRate { get; set; }
        public decimal PerOrderAvgProcessTime { get; set; }
    }
}
