﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.Emails
{
    public class EmailTemplate
    {
        public Guid EmailTemplateId {get;set;}
        public string EmailTemplateName {get;set;}
        public bool WithImage {get;set;}
        public bool WithPdf {get;set;}
        public Guid EmailTemplateConditionId {get;set;}
        public bool IsEnabled {get;set;}
        public bool IsHtml {get;set;}
        public string Cc {get;set;}
        public string Bcc {get;set;}
        public string Subject { get; set; }
        public string Body {get;set;}
        public Guid MailFrom { get; set; }
    }
}
