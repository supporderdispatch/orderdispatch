﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.ProcessOrderInstrunctions
{
    public class CriteriaResult
    {
        public Guid ConditionCriteriaId { get; set; }
        public CriteriaResultType ResultType { get; set; }
        public string ImageResult { get; set; }
        public string TextResult { get; set; }
    }
}
