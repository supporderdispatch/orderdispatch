﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.ProcessOrderInstrunctions
{
    public enum CriteriaResultType
    {
        Text=1,
        Image=2,
        ImageAndText=3
    }
}
