﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities
{
    public class PickingBatchFilters
    { 
         public FilterOption MultipleItems { get; set;}
         public FilterOption PickListPrinted { get; set;}
         public FilterOption UrgentOrders { get; set; }
         public string PickingBatch { get; set; }
    }
}
