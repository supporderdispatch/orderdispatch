﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.InvoiceTemplate
{
    public class PageDimension
    {
        public float Height { get; set; }
        public float width { get; set; }
    }
}
