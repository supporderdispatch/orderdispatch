﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.InvoiceTemplate.InvoiceTemplateDataContent
{
    public class DataPrintSettings
    {
        public TableContentExceedOptions DataExceedActionSelected { get; set; }
        public Guid TemplateId { get; set; }
        public bool TryAdjustBlock { get; set; }
        public Guid TemplateSelectedWhenDataExceed { get; set; }

    }
}
