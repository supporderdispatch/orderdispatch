﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities.InvoiceTemplate
{
    public class PageSize
    {
        public string PageSizeName { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
