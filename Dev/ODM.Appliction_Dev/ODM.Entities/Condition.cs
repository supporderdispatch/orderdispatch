﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM.Entities
{
    public class Condition
    {
        public Guid ConditionId { get; set; }
        public string ConditionName { get; set; }
        public bool IsEnabled { get; set; }
    }

   public class ConditionCriteria
    {
        public Guid ConditionId { get; set; }
        public Guid ConditionCriteriaId { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string Creterion { get; set; }
        public string CompareWith { get; set; }
        public string CriteriaDataType { get; set; }
        public ProcessOrderInstrunctions.CriteriaResultType ResultType { get; set; }
    }
}
