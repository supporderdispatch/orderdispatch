﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace ODM.Reports
{
    public partial class frmReport : Form
    {
        public frmReport()
        {
            InitializeComponent();
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }
        public void BindReport(string strBarCode, string strCompanyName, string strCompanyAddress, string strCustomerName, string strCustomerAddress)
        {
            ReportParameter param1 = new ReportParameter("Barcode", strBarCode);
            ReportParameter param2 = new ReportParameter("CompanyName", strCompanyName);
            ReportParameter param3 = new ReportParameter("CompanyAddress", strCompanyAddress);
            ReportParameter param4 = new ReportParameter("CustomerName", strCustomerName);
            ReportParameter param5 = new ReportParameter("CustomerAddress", strCustomerAddress);
            this.reportViewer1.LocalReport.SetParameters(new ReportParameter[] { param1, param2, param3, param4, param5 });
            this.reportViewer1.RefreshReport();
        }

        private void btnCloseRtp_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
