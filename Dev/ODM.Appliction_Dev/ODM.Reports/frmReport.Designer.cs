﻿namespace ODM.Reports
{
    partial class frmReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReport));
            this.orderDispatchTestBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.OrderDispatchTest = new ODM.Reports.OrderDispatchTest();
            this.GetLabelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.getLabelBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.getLabelBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.getLabelTableAdapter = new ODM.Reports.OrderDispatchTestTableAdapters.GetLabelTableAdapter();
            this.btnCloseRtp = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.orderDispatchTestBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderDispatchTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GetLabelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getLabelBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getLabelBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // orderDispatchTestBindingSource
            // 
            this.orderDispatchTestBindingSource.DataSource = this.OrderDispatchTest;
            this.orderDispatchTestBindingSource.Position = 0;
            // 
            // OrderDispatchTest
            // 
            this.OrderDispatchTest.DataSetName = "OrderDispatchTest";
            this.OrderDispatchTest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // GetLabelBindingSource
            // 
            this.GetLabelBindingSource.DataMember = "GetLabel";
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.orderDispatchTestBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ODM.Reports.rptPrintLabel.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(-3, 43);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(850, 568);
            this.reportViewer1.TabIndex = 0;
            // 
            // getLabelBindingSource1
            // 
            this.getLabelBindingSource1.DataMember = "GetLabel";
            // 
            // getLabelBindingSource2
            // 
            this.getLabelBindingSource2.DataMember = "GetLabel";
            this.getLabelBindingSource2.DataSource = this.OrderDispatchTest;
            // 
            // getLabelTableAdapter
            // 
            this.getLabelTableAdapter.ClearBeforeFill = true;
            // 
            // btnCloseRtp
            // 
            this.btnCloseRtp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnCloseRtp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCloseRtp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseRtp.ForeColor = System.Drawing.Color.Red;
            this.btnCloseRtp.Location = new System.Drawing.Point(721, 3);
            this.btnCloseRtp.Name = "btnCloseRtp";
            this.btnCloseRtp.Size = new System.Drawing.Size(113, 34);
            this.btnCloseRtp.TabIndex = 1;
            this.btnCloseRtp.Text = "Close Report";
            this.btnCloseRtp.UseVisualStyleBackColor = false;
            this.btnCloseRtp.Click += new System.EventHandler(this.btnCloseRtp_Click);
            // 
            // frmReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 499);
            this.Controls.Add(this.btnCloseRtp);
            this.Controls.Add(this.reportViewer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmReport";
            this.Text = "Order Dispatch Report";
            this.Load += new System.EventHandler(this.frmReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.orderDispatchTestBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderDispatchTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GetLabelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getLabelBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getLabelBindingSource2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource GetLabelBindingSource;
      
        private System.Windows.Forms.BindingSource getLabelBindingSource1;
   
        private OrderDispatchTest OrderDispatchTest;
        private System.Windows.Forms.BindingSource getLabelBindingSource2;
        private OrderDispatchTestTableAdapters.GetLabelTableAdapter getLabelTableAdapter;
        private System.Windows.Forms.BindingSource orderDispatchTestBindingSource;
        private System.Windows.Forms.Button btnCloseRtp;
    
    }
}