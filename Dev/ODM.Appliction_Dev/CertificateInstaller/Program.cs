﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CertificateInstaller
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var certificateInstallRequire = true;
                var userStore = new X509Store(StoreLocation.CurrentUser);
                userStore.Open(OpenFlags.ReadOnly);
                foreach (var certificate in userStore.Certificates)
                {
                    var name = certificate.Subject;
                    if (!string.IsNullOrEmpty(name) && name.ToString().Contains("orderdispatch"))
                    {
                        certificateInstallRequire = false;
                    }
                }
                userStore.Close();


                var localStore = new X509Store(StoreLocation.LocalMachine);
                localStore.Open(OpenFlags.ReadOnly);
                foreach (var certificate in localStore.Certificates)
                {
                    var name = certificate.Subject;
                    if (!string.IsNullOrEmpty(name) && name.ToString().Contains("orderdispatch"))
                    {
                        certificateInstallRequire = false;
                    }
                }
                localStore.Close();

                if (certificateInstallRequire)
                {
                    var certPath=Path.GetTempPath() + "ODCert.pfx";
                    var webClient = new WebClient();
                    webClient.DownloadFile(new Uri("http://77.68.11.228:8686/Cert/ODCert.pfx"), Path.GetTempPath() + "\\ODCert.pfx");
                    Process p = new Process();
                    p.StartInfo.FileName = certPath;
                    p.Start();
                    webClient.Dispose();
                    
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
    }
}
