﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderDispatchWcfService
{
    class SendEmailEntity
    {
        public string NumberOrderID { get; set; }
        public string OrderID { get; set; }
        public string EmailID { get; set; }
        public string MailTemplate { get; set; }
        public string ID { get; set; }
        public string NumOrderIDs { get; set; }
        public string OrderIDs { get; set; }
        public string IDs { get; set; }
        public DateTime Date { get; set; }
        public string FullName { get; set; }
        public string Mode { get; set; }
        public string Source { get; set; }
        public string SubSource { get; set; }
        public string SentText { get; set; }
        public string SentSubject { get; set; }
    }
}
