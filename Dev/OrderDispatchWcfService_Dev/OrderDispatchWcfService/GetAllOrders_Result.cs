//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OrderDispatchWcfService
{
    using System;
    
    public partial class GetAllOrders_Result
    {
        public System.Guid OrderId { get; set; }
        public long Order_Number { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public Nullable<System.DateTime> Received_Date { get; set; }
        public Nullable<System.DateTime> Despatch_By_Date { get; set; }
        public string Shipping_Service { get; set; }
    }
}
