﻿using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Drawing;   


namespace OrderDispatchWcfService
{
    class StoreDataInLocaldb
    {
        SqlConnection connection = new SqlConnection();
        OrderDispatchEntities objContext = new OrderDispatchEntities();
        public static Guid GuidForLog = new Guid();
        public static Guid OrderIdToLog = new Guid();
        //     public static System.Collections.Generic.List<System.Drawing.Image> lstimgInvoice = new System.Collections.Generic.List<System.Drawing.Image>();
        Email objEmail = new Email();
        #region "Insert/Update Order/Order Items details"
        public void LoadJson(string strOrderData, Guid OrderId, int intTotalOrders, int intCurrentCount)
        {
            try
            {
                dynamic array = JsonConvert.DeserializeObject(strOrderData);
                string strJson = Convert.ToString(array);
                dynamic objData = JsonConvert.DeserializeObject(strJson);

                if (objData != null)
                {
                    Order objOrder = new Order();
                    Shipping objShipping = new Shipping();
                    Customer objCustomer = new Customer();
                    Address objCustomerAddress = new Address();
                    Address objBillingAddress = new Address();

                    //Generating new guids
                    Guid CustomerID = Guid.NewGuid();
                    Guid CreatedBy = Guid.NewGuid();
                    Guid ShippingID = Guid.NewGuid();
                    Guid ModifiedBy = new Guid();
                    Guid AddressID = Guid.NewGuid();
                    Guid BillingAddressID = Guid.NewGuid();
                    //End of Generating new guids

                    #region "Mapping of Order class"
                    string strGetOrderInfo = Convert.ToString(objData["GeneralInfo"]);
                    objOrder = JsonConvert.DeserializeObject<Order>(strGetOrderInfo);
                    objOrder.Orderid = (Guid)objData["OrderId"];
                    objOrder.NumOrderId = (long)objData["NumOrderId"];
                    objOrder.CustomerId = CustomerID;
                    objOrder.Processed = (Boolean)objData["Processed"];
                    string strProcessedDateTime = Convert.ToString(objData["ProcessedDateTime"]);
                    if (!String.IsNullOrWhiteSpace(strProcessedDateTime))
                    {
                        objOrder.ProcessedDateTime = Convert.ToDateTime(strProcessedDateTime.Replace("T", " "));
                    }
                    else
                    {
                        objOrder.ProcessedDateTime = null;
                    }
                    objOrder.FulfilmentLocationId = (Guid)objData["FulfilmentLocationId"];

                    //Map TotalsInfo node with Order Class
                    objOrder.Subtotal = (decimal)(objData["TotalsInfo"]["Subtotal"]);
                    objOrder.PostageCost = (decimal)(objData["TotalsInfo"]["PostageCost"]);
                    objOrder.Tax = (decimal)(objData["TotalsInfo"]["Tax"]);
                    objOrder.TotalCharge = (decimal)(objData["TotalsInfo"]["TotalCharge"]);
                    objOrder.PaymentMethod = Convert.ToString(objData["TotalsInfo"]["PaymentMethod"]);
                    objOrder.PaymentMethodId = (Guid)(objData["TotalsInfo"]["PaymentMethodId"]);
                    objOrder.ProfitMargin = (decimal)(objData["TotalsInfo"]["ProfitMargin"]);
                    objOrder.TotalDiscount = (decimal)(objData["TotalsInfo"]["TotalDiscount"]);
                    objOrder.Currency = Convert.ToString(objData["TotalsInfo"]["Currency"]);
                    objOrder.CountryTaxRate = (decimal)(objData["TotalsInfo"]["CountryTaxRate"]);
                    //End of Map TotalsInfo node with Order Class

                    #endregion

                    #region "Mapping of Customer class"
                    string strGetCustomerData = Convert.ToString(objData["CustomerInfo"]);
                    objCustomer = JsonConvert.DeserializeObject<Customer>(strGetCustomerData);
                    objCustomer.ChannelBuyerName = Convert.ToString(objData["CustomerInfo"]["ChannelBuyerName"]);
                    objCustomer.EmailAddress = Convert.ToString(objData["CustomerInfo"]["Address"]["EmailAddress"]);
                    objCustomer.CustomerId = CustomerID;

                    //(Unsure fields)We have to confirm that from where we will get these values 
                    //(these values are available in both node Address/BillingAddress)
                    //currently getting these values from Address Node to fill dummy data
                    objCustomer.Town = Convert.ToString(objData["CustomerInfo"]["Address"]["Town"]);
                    objCustomer.FullName = Convert.ToString(objData["CustomerInfo"]["Address"]["FullName"]);
                    objCustomer.Company = Convert.ToString(objData["CustomerInfo"]["Address"]["Company"]);
                    //End of (Unsure fields)

                    objCustomer.CreatedBy = CreatedBy;
                    objCustomer.ModifiedBy = ModifiedBy;
                    #endregion

                    #region "Mapping of Shipping class"
                    string strGetShippingData = Convert.ToString(objData["ShippingInfo"]);
                    objShipping = JsonConvert.DeserializeObject<Shipping>(strGetShippingData);
                    objShipping.OrderId = (Guid)(objData["OrderId"]);
                    objShipping.AddressId = AddressID;
                    objShipping.ShippingId = ShippingID;
                    #endregion

                    #region "Mapping of Address/BillingAddress for customer(Pointing Address Class)"
                    string strGetCustomerAddress = Convert.ToString(objData["CustomerInfo"]["Address"]);
                    objCustomerAddress = JsonConvert.DeserializeObject<Address>(strGetCustomerAddress);
                    objCustomerAddress.CustomerId = CustomerID;

                    string strGetCustomerBillingAddress = Convert.ToString(objData["CustomerInfo"]["BillingAddress"]);
                    objBillingAddress = JsonConvert.DeserializeObject<Address>(strGetCustomerBillingAddress);
                    objBillingAddress.CustomerId = CustomerID;
                    #endregion

                    #region "Mapping of Order item Table"
                    string strGetOrderitems = Convert.ToString(objData["Items"]);
                    //strGetOrderitems = strGetOrderitems.Replace("CompositeSubItems", "lstCompositeSubItems");                    
                    strGetOrderitems = strGetOrderitems.Replace("CompositeSubItems", "lstCompositeSubOrderItems");
                    strGetOrderitems = strGetOrderitems.Replace("AdditionalInfo", "lstAdditionalInfo");
                    //string strGetCompositeOrderItems = Convert.ToString(objData["Items"]);

               //     strGetCompositeOrderItems = strGetCompositeOrderItems.Replace("CompositeSubItems", "lstCompositeSubOrderItems");
                //    strGetCompositeOrderItems = strGetCompositeOrderItems.Replace("AdditionalInfo", "lstAdditionalInfo");

                    //Deserializing "Items" node with List<OrderItem> object because that is an Array node
                       List<OrderItem> objListOrderitem = (List<OrderItem>)JsonConvert.DeserializeObject(strGetOrderitems, typeof(List<OrderItem>));
                  //  List<OrderItem> objListCompositeOrderitem = (List<OrderItem>)JsonConvert.DeserializeObject(strGetCompositeOrderItems, typeof(List<OrderItem>));

                    #endregion

                    #region "Database Connectivity"
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                        {
                            connection.ConnectionString = MakeConnection.GetConnection();
                            connection.Open();
                        }
                        //Note Keep the insertion of customer first than order becasue of pk-fk relationship
                        #region "Insert Data In Customer Table"
                        SqlParameter[] objCustomerParams = new SqlParameter[10];

                        objCustomerParams[0] = new SqlParameter("@CustomerId", SqlDbType.UniqueIdentifier);
                        objCustomerParams[0].Value = objCustomer.CustomerId;

                        objCustomerParams[1] = new SqlParameter("@ChannelBuyerName", SqlDbType.NVarChar, 200);
                        objCustomerParams[1].Value = objCustomer.ChannelBuyerName;

                        objCustomerParams[2] = new SqlParameter("@EmailAddress", SqlDbType.NVarChar, 200);
                        objCustomerParams[2].Value = objCustomer.EmailAddress;

                        objCustomerParams[3] = new SqlParameter("@Town", SqlDbType.NVarChar, 200);
                        objCustomerParams[3].Value = objCustomer.Town;

                        objCustomerParams[4] = new SqlParameter("@FullName", SqlDbType.NVarChar, 200);
                        objCustomerParams[4].Value = objCustomer.FullName;

                        objCustomerParams[5] = new SqlParameter("@Company", SqlDbType.NVarChar, 200);
                        objCustomerParams[5].Value = objCustomer.Company;

                        objCustomerParams[6] = new SqlParameter("@CreatedBy", SqlDbType.UniqueIdentifier);
                        objCustomerParams[6].Value = objCustomer.CreatedBy;

                        objCustomerParams[7] = new SqlParameter("@ModifiedBy", SqlDbType.UniqueIdentifier);
                        objCustomerParams[7].Value = objCustomer.ModifiedBy;

                        objCustomerParams[8] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                        objCustomerParams[8].Value = "N";

                        objCustomerParams[9] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                        objCustomerParams[9].Direction = ParameterDirection.Output;

                        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "CustomerAddUpdateDelete", objCustomerParams);
                        string strCustomerResult = objCustomerParams[9].Value.ToString();

                        //If the customer is already exists then we need to get that cutomer id instead of inserting that again, and assign that customer id for further use.
                        if (strCustomerResult.Trim().ToUpper() == "CUSTOMER ALREADY EXIST")
                        {
                            Guid ExistsCustomerID = objContext.Customers.Where(x => x.EmailAddress == objCustomer.EmailAddress && x.Town == objCustomer.Town && x.FullName == objCustomer.FullName && x.Company == objCustomer.Company).Select(x => x.CustomerId).FirstOrDefault();
                            objOrder.CustomerId = ExistsCustomerID;
                            objCustomerAddress.CustomerId = ExistsCustomerID;
                            objBillingAddress.CustomerId = ExistsCustomerID;
                        }

                        #endregion

                        #region "Insert Data In Order Table"
                        SqlParameter[] objOrderParams = new SqlParameter[36];

                        objOrderParams[0] = new SqlParameter("@Orderid", SqlDbType.UniqueIdentifier);
                        objOrderParams[0].Value = objOrder.Orderid;

                        objOrderParams[1] = new SqlParameter("@NumOrderId", SqlDbType.BigInt);
                        objOrderParams[1].Value = objOrder.NumOrderId;

                        objOrderParams[2] = new SqlParameter("@Status", SqlDbType.BigInt);
                        objOrderParams[2].Value = objOrder.Status;

                        objOrderParams[3] = new SqlParameter("@LabelPrinted", SqlDbType.BigInt);
                        objOrderParams[3].Value = objOrder.LabelPrinted;

                        objOrderParams[4] = new SqlParameter("@LabelError", SqlDbType.NVarChar, 100);
                        objOrderParams[4].Value = objOrder.LabelError;

                        objOrderParams[5] = new SqlParameter("@InvoicePrinted", SqlDbType.Bit);
                        objOrderParams[5].Value = objOrder.InvoicePrinted;

                        objOrderParams[6] = new SqlParameter("@PickListPrinted", SqlDbType.BigInt);
                        objOrderParams[6].Value = objOrder.PickListPrinted;

                        objOrderParams[7] = new SqlParameter("@Notes", SqlDbType.NVarChar, 200);
                        objOrderParams[7].Value = objOrder.Notes;

                        objOrderParams[8] = new SqlParameter("@PartShipped", SqlDbType.Bit);
                        objOrderParams[8].Value = objOrder.PartShipped;

                        objOrderParams[9] = new SqlParameter("@Marker", SqlDbType.BigInt);
                        objOrderParams[9].Value = objOrder.Marker;

                        objOrderParams[10] = new SqlParameter("@ReferenceNum", SqlDbType.NVarChar, 100);
                        objOrderParams[10].Value = objOrder.ReferenceNum;

                        objOrderParams[11] = new SqlParameter("@SecondaryReference", SqlDbType.NVarChar, 100);
                        objOrderParams[11].Value = objOrder.SecondaryReference;

                        objOrderParams[12] = new SqlParameter("@ExternalReferenceNum", SqlDbType.NVarChar);
                        objOrderParams[12].Value = objOrder.ExternalReferenceNum;

                        objOrderParams[13] = new SqlParameter("@ReceivedDate", SqlDbType.DateTime);
                        objOrderParams[13].Value = objOrder.ReceivedDate;

                        objOrderParams[14] = new SqlParameter("@Source", SqlDbType.NVarChar, 50);
                        objOrderParams[14].Value = objOrder.Source;

                        objOrderParams[15] = new SqlParameter("@SubSource", SqlDbType.NVarChar, 50);
                        objOrderParams[15].Value = objOrder.SubSource;

                        objOrderParams[16] = new SqlParameter("@HoldOrCancel", SqlDbType.Bit);
                        objOrderParams[16].Value = objOrder.HoldOrCancel;

                        objOrderParams[17] = new SqlParameter("@DespatchByDate", SqlDbType.Date);
                        objOrderParams[17].Value = objOrder.DespatchByDate;

                        objOrderParams[18] = new SqlParameter("@Location", SqlDbType.UniqueIdentifier);
                        objOrderParams[18].Value = objOrder.Location;

                        objOrderParams[19] = new SqlParameter("@NumItems", SqlDbType.Bit);
                        objOrderParams[19].Value = objOrder.NumItems;

                        objOrderParams[20] = new SqlParameter("@Subtotal", SqlDbType.Money);
                        objOrderParams[20].Value = objOrder.Subtotal;

                        objOrderParams[21] = new SqlParameter("@PostageCost", SqlDbType.Money);
                        objOrderParams[21].Value = objOrder.PostageCost;

                        objOrderParams[22] = new SqlParameter("@Tax", SqlDbType.Decimal);
                        objOrderParams[22].Value = objOrder.Tax;

                        objOrderParams[23] = new SqlParameter("@TotalCharge", SqlDbType.Money);
                        objOrderParams[23].Value = objOrder.TotalCharge;

                        objOrderParams[24] = new SqlParameter("@PaymentMethod", SqlDbType.NVarChar, 100);
                        objOrderParams[24].Value = objOrder.PaymentMethod;

                        objOrderParams[25] = new SqlParameter("@PaymentMethodId", SqlDbType.UniqueIdentifier);
                        objOrderParams[25].Value = objOrder.PaymentMethodId;

                        objOrderParams[26] = new SqlParameter("@ProfitMargin", SqlDbType.Money);
                        objOrderParams[26].Value = objOrder.ProfitMargin;

                        objOrderParams[27] = new SqlParameter("@TotalDiscount", SqlDbType.Decimal);
                        objOrderParams[27].Value = objOrder.TotalDiscount;

                        objOrderParams[28] = new SqlParameter("@Currency", SqlDbType.NVarChar, 100);
                        objOrderParams[28].Value = objOrder.Currency;

                        objOrderParams[29] = new SqlParameter("@CountryTaxRate", SqlDbType.Decimal);
                        objOrderParams[29].Value = objOrder.CountryTaxRate;

                        objOrderParams[30] = new SqlParameter("@CustomerId", SqlDbType.UniqueIdentifier);
                        objOrderParams[30].Value = objOrder.CustomerId;

                        objOrderParams[31] = new SqlParameter("@Processed", SqlDbType.Bit);
                        objOrderParams[31].Value = objOrder.Processed;

                        objOrderParams[32] = new SqlParameter("@ProcessedDateTime", SqlDbType.DateTime);
                        objOrderParams[32].Value = objOrder.ProcessedDateTime;

                        objOrderParams[33] = new SqlParameter("@FulfilmentLocationId", SqlDbType.UniqueIdentifier);
                        objOrderParams[33].Value = objOrder.FulfilmentLocationId;

                        objOrderParams[34] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                        objOrderParams[34].Value = "N";

                        objOrderParams[35] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                        objOrderParams[35].Direction = ParameterDirection.Output;

                        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "OrderAddUpdateDelete", objOrderParams);
                        string strOrderResult = objOrderParams[35].Value.ToString();
                        if (strOrderResult.ToUpper() == "RECORD SAVED SUCCESSFULLY")
                        {
                            Utill.LogHistory(string.Format("Order with Order Number Id {0} saved in our database", objOrder.NumOrderId.ToString()), Guid.NewGuid(), objOrder.Orderid);
                        }
                        #endregion

                        #region "Insert data in Address Table"

                        //Loopig because Address is containing two types of adress 1.Simple addres 2.Billing Address
                        for (int i = 0; i < 2; i++)
                        {
                            Address objAddress = new Address();
                            if (i == 0)
                            {
                                objAddress = objCustomerAddress;
                                objAddress.IsBilling = false;
                                objAddress.IsShipping = true;
                                objAddress.AddressId = AddressID;
                            }
                            else
                            {
                                objAddress = objBillingAddress;
                                objAddress.IsBilling = true;
                                objAddress.IsShipping = false;
                                objAddress.AddressId = BillingAddressID;
                            }

                            Guid CountryID = objContext.Countries.Where(x => x.CountryName == objAddress.Country).Select(x => x.CountryId).FirstOrDefault();
                            objAddress.CountryId = CountryID;

                            SqlParameter[] objAddressParams = new SqlParameter[19];

                            objAddressParams[0] = new SqlParameter("@AddressId", SqlDbType.UniqueIdentifier);
                            objAddressParams[0].Value = objAddress.AddressId;

                            objAddressParams[1] = new SqlParameter("@IsBilling", SqlDbType.Bit);
                            objAddressParams[1].Value = objAddress.IsBilling;

                            objAddressParams[2] = new SqlParameter("@IsShipping", SqlDbType.Bit);
                            objAddressParams[2].Value = objAddress.IsShipping;

                            objAddressParams[3] = new SqlParameter("@EmailAddress", SqlDbType.NVarChar, 200);
                            objAddressParams[3].Value = objAddress.EmailAddress == null ? "" : objAddress.EmailAddress;

                            objAddressParams[4] = new SqlParameter("@Address1", SqlDbType.NVarChar, 200);
                            objAddressParams[4].Value = objAddress.Address1;

                            objAddressParams[5] = new SqlParameter("@Address2", SqlDbType.NVarChar, 200);
                            objAddressParams[5].Value = objAddress.Address2;

                            objAddressParams[6] = new SqlParameter("@Address3", SqlDbType.NVarChar, 200);
                            objAddressParams[6].Value = objAddress.Address3;

                            objAddressParams[7] = new SqlParameter("@Town", SqlDbType.NVarChar, 200);
                            objAddressParams[7].Value = objAddress.Town;

                            objAddressParams[8] = new SqlParameter("@Region", SqlDbType.NVarChar, 200);
                            objAddressParams[8].Value = objAddress.Region;

                            objAddressParams[9] = new SqlParameter("@PostCode", SqlDbType.NVarChar, 200);
                            objAddressParams[9].Value = objAddress.PostCode;

                            objAddressParams[10] = new SqlParameter("@Country", SqlDbType.NVarChar, 200);
                            objAddressParams[10].Value = objAddress.Country;

                            objAddressParams[11] = new SqlParameter("@FullName", SqlDbType.NVarChar, 200);
                            objAddressParams[11].Value = objAddress.FullName;

                            objAddressParams[12] = new SqlParameter("@Company", SqlDbType.NVarChar, 200);
                            objAddressParams[12].Value = objAddress.Company;

                            objAddressParams[13] = new SqlParameter("@PhoneNumber", SqlDbType.NVarChar, 200);
                            objAddressParams[13].Value = objAddress.PhoneNumber;

                            objAddressParams[14] = new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier);
                            objAddressParams[14].Value = objAddress.CountryId;

                            objAddressParams[15] = new SqlParameter("@CustomerId", SqlDbType.UniqueIdentifier);
                            objAddressParams[15].Value = objAddress.CustomerId;

                            objAddressParams[16] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                            objAddressParams[16].Value = objOrder.Orderid;

                            objAddressParams[17] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                            objAddressParams[17].Value = "N";

                            objAddressParams[18] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                            objAddressParams[18].Direction = ParameterDirection.Output;

                            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "AddressAddUpdateDelete", objAddressParams);
                            string strAddressResult = objAddressParams[18].Value.ToString();

                            //If the address with the same email,orderid,fullname etc is already exists then get the address id(but not the billing addressid)
                            if (strAddressResult.Trim().ToUpper() == "ADDRESS ALREADY EXIST" && !objAddress.IsBilling)
                            {
                                objShipping.AddressId = objContext.Addresses.Where(x => x.EmailAddress == objAddress.EmailAddress && x.FullName == objAddress.FullName && x.CountryId == objAddress.CountryId && x.CustomerId == objAddress.CustomerId && x.OrderId == objOrder.Orderid).Select(x => x.AddressId).FirstOrDefault();
                            }
                        }
                        #endregion

                        #region "Insert data in OrderItem Table"
                        for (int i = 0; i < objListOrderitem.Count; i++)
                        {
                            Guid OrderitemId = Guid.NewGuid();
                            OrderItem objOrderitem = new OrderItem();
                            OrderItem objCompositeOrderItem = new OrderItem();
                            objOrderitem = objListOrderitem[i];
                            objOrderitem.OrderItemId = OrderitemId;
                            objOrderitem.CreatedBy = CreatedBy;
                            objOrderitem.ModifiedBy = ModifiedBy;
                            SqlParameter[] objOrderitemsParams = new SqlParameter[43];



                            if (objOrderitem.lstCompositeSubOrderItems.Count > 0)
                            {
                                for (int j = 0; j < objOrderitem.lstCompositeSubOrderItems.Count; j++)
                                {


                                    objCompositeOrderItem = objOrderitem.lstCompositeSubOrderItems[j];
                                    //objCompositeOrderItem.OrderItemId = ;
                                    objCompositeOrderItem.OrderItemId = OrderitemId;
                                    objCompositeOrderItem.CreatedBy = CreatedBy;
                                    objCompositeOrderItem.ModifiedBy = ModifiedBy;
                             //       List<OrderItem> lstOrderItemsToSaveInDb = new List<OrderItem>();
                                   
                                    
                                    if (objCompositeOrderItem.lstCompositeSubOrderItems.Count > 0)
                                    {
                                        CheckCompositeNonCompositeItemsInOrderItems(objCompositeOrderItem.lstCompositeSubOrderItems[j],objOrder);
                                    }   
                                    else
                                    {
                                        string strOrderitemsResult = SaveOrderItemsCompositeNonComposite(objCompositeOrderItem, objOrder);

                                        if (strOrderitemsResult.ToUpper().Contains("ERROR"))
                                        {
                                            throw new CustomException("Error Occurred in SP OrderItemAddUpdateDelete:- " + strOrderitemsResult);
                                        }
                                    }

                                }
                            }
                            else
                            {
                                string strOrderitemsResult = SaveOrderItemsCompositeNonComposite(objOrderitem, objOrder);

                                if (strOrderitemsResult.ToUpper().Contains("ERROR"))
                                {
                                    throw new CustomException("Error Occurred in SP OrderItemAddUpdateDelete:- " + strOrderitemsResult);
                                }
                            }

                        }
                        #endregion

                        #region "Insert data in Shipping table"

                        SqlParameter[] objshippingParams = new SqlParameter[18];
                        objshippingParams[0] = new SqlParameter("@ShippingId", SqlDbType.UniqueIdentifier);
                        objshippingParams[0].Value = objShipping.ShippingId;

                        objshippingParams[1] = new SqlParameter("@Vendor", SqlDbType.NVarChar, 200);
                        objshippingParams[1].Value = objShipping.Vendor;

                        objshippingParams[2] = new SqlParameter("@PostalServiceId", SqlDbType.UniqueIdentifier);
                        objshippingParams[2].Value = objShipping.PostalServiceId;

                        objshippingParams[3] = new SqlParameter("@PostalServiceName", SqlDbType.NVarChar, 200);
                        objshippingParams[3].Value = objShipping.PostalServiceName;

                        objshippingParams[4] = new SqlParameter("@TotalWeight", SqlDbType.Decimal);
                        objshippingParams[4].Value = objShipping.TotalWeight;

                        objshippingParams[5] = new SqlParameter("@ItemWeight", SqlDbType.Decimal);
                        objshippingParams[5].Value = objShipping.ItemWeight;

                        objshippingParams[6] = new SqlParameter("@PackageCategoryId", SqlDbType.UniqueIdentifier);
                        objshippingParams[6].Value = objShipping.PackageCategoryId;

                        objshippingParams[7] = new SqlParameter("@PackageCategory", SqlDbType.NVarChar, 200);
                        objshippingParams[7].Value = objShipping.PackageCategory;

                        objshippingParams[8] = new SqlParameter("@PackageTypeId", SqlDbType.UniqueIdentifier);
                        objshippingParams[8].Value = objShipping.PackageTypeId;

                        objshippingParams[9] = new SqlParameter("@PackageType", SqlDbType.NVarChar, 100);
                        objshippingParams[9].Value = objShipping.PackageType;

                        objshippingParams[10] = new SqlParameter("@PostageCost", SqlDbType.Money);
                        objshippingParams[10].Value = objShipping.PostageCost;

                        objshippingParams[11] = new SqlParameter("@PostageCostExTax", SqlDbType.Decimal);
                        objshippingParams[11].Value = objShipping.PostageCost;

                        objshippingParams[12] = new SqlParameter("@TrackingNumber", SqlDbType.NVarChar, 200);
                        objshippingParams[12].Value = objShipping.TrackingNumber;

                        objshippingParams[13] = new SqlParameter("@ManualAdjust", SqlDbType.Bit);
                        objshippingParams[13].Value = objShipping.ManualAdjust;

                        objshippingParams[14] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
                        objshippingParams[14].Value = objShipping.OrderId;

                        objshippingParams[15] = new SqlParameter("@AddressId", SqlDbType.UniqueIdentifier);
                        objshippingParams[15].Value = objShipping.AddressId;

                        objshippingParams[16] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
                        objshippingParams[16].Value = "N";

                        objshippingParams[17] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
                        objshippingParams[17].Direction = ParameterDirection.Output;

                        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "ShippingAddUpdateDelete", objshippingParams);
                        string str = objshippingParams[17].Value.ToString();
                        #endregion
                    }
                    catch (IndexOutOfRangeException IndexEx)
                    {
                        Utill.LogError(IndexEx, string.Format("IndexOutOfRangeException While reading OrderId {0} ", OrderId), intTotalOrders, intCurrentCount);
                    }
                    catch (SqlException SqlEx)
                    {
                        Utill.LogError(SqlEx, string.Format("SqlException While reading OrderId {0} Procedure name {1}", OrderId, SqlEx.Procedure), intTotalOrders, intCurrentCount);
                    }
                    finally
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Utill.LogError(ex, string.Format("Main Exception In LoadJson Method with orderid {0} ", OrderId), intTotalOrders, intCurrentCount);
            }
        }
        #endregion

        // return array of order items to save in the database based on composite/non composite items
        #region To Check if the composite items exists
        public void CheckCompositeNonCompositeItemsInOrderItems(OrderItem objCompositeOrderItem,Order objOrder)
        {
        //    List<OrderItem> lstCompositeOrderItems = new List<OrderItem>();
            if (objCompositeOrderItem.lstCompositeSubOrderItems.Count>0)
            {
                for (int i = 0; i < objCompositeOrderItem.lstCompositeSubOrderItems.Count; i++)
                 {
                    CheckCompositeNonCompositeItemsInOrderItems(objCompositeOrderItem.lstCompositeSubOrderItems[i],objOrder);    

                }
                
            }
            else
            {
                string strOrderitemsResult = SaveOrderItemsCompositeNonComposite(objCompositeOrderItem, objOrder);

                if (strOrderitemsResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException("Error Occurred in SP OrderItemAddUpdateDelete:- " + strOrderitemsResult);
                }
                 //lstCompositeOrderItems.Add(objCompositeOrderItem);
            }
           
            //return lstCompositeOrderItems;
        }



        #endregion

        // save order item data for composite and non composite items
        #region Save order item data for composite and non composite items.
        public string SaveOrderItemsCompositeNonComposite(OrderItem objOrderitem, Order objOrder)
        {
            SqlParameter[] objOrderitemsParams = new SqlParameter[43];
            objOrderitemsParams[0] = new SqlParameter("@OrderItemId", SqlDbType.UniqueIdentifier);
            objOrderitemsParams[0].Value = objOrderitem.OrderItemId;

            objOrderitemsParams[1] = new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier);
            objOrderitemsParams[1].Value = objOrder.Orderid;

            objOrderitemsParams[2] = new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier);
            objOrderitemsParams[2].Value = objOrderitem.ItemId;

            objOrderitemsParams[3] = new SqlParameter("@StockItemId", SqlDbType.UniqueIdentifier);
            objOrderitemsParams[3].Value = objOrderitem.StockItemId;

            objOrderitemsParams[4] = new SqlParameter("@SKU", SqlDbType.NChar, 100);
            objOrderitemsParams[4].Value = objOrderitem.SKU;

            objOrderitemsParams[5] = new SqlParameter("@ItemSource", SqlDbType.NChar, 200);
            objOrderitemsParams[5].Value = objOrderitem.ItemSource;

            objOrderitemsParams[6] = new SqlParameter("@Title", SqlDbType.NChar, 200);
            objOrderitemsParams[6].Value = objOrderitem.Title;

            objOrderitemsParams[7] = new SqlParameter("@Quantity", SqlDbType.Decimal);
            objOrderitemsParams[7].Value = objOrderitem.Quantity;

            objOrderitemsParams[8] = new SqlParameter("@CategoryName", SqlDbType.NChar, 100);
            objOrderitemsParams[8].Value = objOrderitem.CategoryName;

            objOrderitemsParams[9] = new SqlParameter("@CompositeAvailablity", SqlDbType.NChar, 100);
            objOrderitemsParams[9].Value = objOrderitem.CompositeAvailablity;

            objOrderitemsParams[10] = new SqlParameter("@RowId", SqlDbType.UniqueIdentifier);
            objOrderitemsParams[10].Value = objOrderitem.RowId;

            objOrderitemsParams[11] = new SqlParameter("@StockLevelsSpecified", SqlDbType.NVarChar, 100);
            objOrderitemsParams[11].Value = objOrderitem.StockLevelsSpecified;

            objOrderitemsParams[12] = new SqlParameter("@OnOrder", SqlDbType.BigInt);
            objOrderitemsParams[12].Value = objOrderitem.OnOrder;

            objOrderitemsParams[13] = new SqlParameter("@InOrderBook", SqlDbType.BigInt);
            objOrderitemsParams[13].Value = objOrderitem.InOrderBook;

            objOrderitemsParams[14] = new SqlParameter("@Level", SqlDbType.Bit);
            objOrderitemsParams[14].Value = objOrderitem.Level;

            objOrderitemsParams[15] = new SqlParameter("@MinimumLevel", SqlDbType.BigInt);
            objOrderitemsParams[15].Value = objOrderitem.MinimumLevel;

            objOrderitemsParams[16] = new SqlParameter("@AvailableStock", SqlDbType.Decimal);
            objOrderitemsParams[16].Value = objOrderitem.AvailableStock;

            objOrderitemsParams[17] = new SqlParameter("@PricePerUnit", SqlDbType.Money);
            objOrderitemsParams[17].Value = objOrderitem.PricePerUnit;

            objOrderitemsParams[18] = new SqlParameter("@UnitCost", SqlDbType.Money);
            objOrderitemsParams[18].Value = objOrderitem.UnitCost;

            objOrderitemsParams[19] = new SqlParameter("@Discount", SqlDbType.Decimal);
            objOrderitemsParams[19].Value = objOrderitem.Discount;

            objOrderitemsParams[20] = new SqlParameter("@Tax", SqlDbType.Decimal);
            objOrderitemsParams[20].Value = objOrderitem.Tax;

            objOrderitemsParams[21] = new SqlParameter("@TaxRate", SqlDbType.Decimal);
            objOrderitemsParams[21].Value = objOrderitem.TaxRate;

            objOrderitemsParams[22] = new SqlParameter("@Cost", SqlDbType.Money);
            objOrderitemsParams[22].Value = objOrderitem.Cost;

            objOrderitemsParams[23] = new SqlParameter("@CostIncTax", SqlDbType.Decimal);
            objOrderitemsParams[23].Value = objOrderitem.CostIncTax;

            objOrderitemsParams[24] = new SqlParameter("@CompositeSubItems", SqlDbType.NChar, 100);
            objOrderitemsParams[24].Value = objOrderitem.lstCompositeSubOrderItems.Count > 0 ? objOrderitem.lstCompositeSubItems[0].TempProperty : "";

            objOrderitemsParams[25] = new SqlParameter("@IsService", SqlDbType.Bit);
            objOrderitemsParams[25].Value = objOrderitem.IsService;

            objOrderitemsParams[26] = new SqlParameter("@SalesTax", SqlDbType.Decimal);
            objOrderitemsParams[26].Value = objOrderitem.SalesTax;

            objOrderitemsParams[27] = new SqlParameter("@TaxCostInclusive", SqlDbType.Bit);
            objOrderitemsParams[27].Value = objOrderitem.TaxCostInclusive;

            objOrderitemsParams[28] = new SqlParameter("@PartShipped", SqlDbType.Bit);
            objOrderitemsParams[28].Value = objOrderitem.PartShipped;

            objOrderitemsParams[29] = new SqlParameter("@Weight", SqlDbType.Decimal);
            objOrderitemsParams[29].Value = objOrderitem.Weight;

            objOrderitemsParams[30] = new SqlParameter("@BarcodeNumber", SqlDbType.NChar, 200);
            objOrderitemsParams[30].Value = objOrderitem.BarcodeNumber;

            objOrderitemsParams[31] = new SqlParameter("@Market", SqlDbType.BigInt);
            objOrderitemsParams[31].Value = objOrderitem.Market;

            objOrderitemsParams[32] = new SqlParameter("@ChannelSKU", SqlDbType.NChar, 10);
            objOrderitemsParams[32].Value = objOrderitem.ChannelSKU;

            objOrderitemsParams[33] = new SqlParameter("@ChannelTitle", SqlDbType.NChar, 10);
            objOrderitemsParams[33].Value = objOrderitem.ChannelTitle;

            objOrderitemsParams[34] = new SqlParameter("@HasImage", SqlDbType.Bit);
            objOrderitemsParams[34].Value = objOrderitem.HasImage;

            objOrderitemsParams[35] = new SqlParameter("@ImageId", SqlDbType.UniqueIdentifier);
            objOrderitemsParams[35].Value = objOrderitem.ImageId;

            objOrderitemsParams[36] = new SqlParameter("@AdditionalInfo", SqlDbType.NChar, 100);
            objOrderitemsParams[36].Value = objOrderitem.lstAdditionalInfo.Count > 0 ? objOrderitem.lstAdditionalInfo[0].Value : "";

            objOrderitemsParams[37] = new SqlParameter("@StockLevelIndicator", SqlDbType.Decimal);
            objOrderitemsParams[37].Value = objOrderitem.StockLevelIndicator;

            objOrderitemsParams[38] = new SqlParameter("@BinRack", SqlDbType.NChar, 100);
            objOrderitemsParams[38].Value = objOrderitem.BinRack;

            objOrderitemsParams[39] = new SqlParameter("@CreatedBy", SqlDbType.UniqueIdentifier);
            objOrderitemsParams[39].Value = objOrderitem.CreatedBy;

            objOrderitemsParams[40] = new SqlParameter("@ModifiedBy", SqlDbType.UniqueIdentifier);
            objOrderitemsParams[40].Value = objOrderitem.ModifiedBy;

            objOrderitemsParams[41] = new SqlParameter("@RecordStatus", SqlDbType.NVarChar, 10);
            objOrderitemsParams[41].Value = "N";

            objOrderitemsParams[42] = new SqlParameter("@Result", SqlDbType.NVarChar, 50);
            objOrderitemsParams[42].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "OrderItemAddUpdateDelete", objOrderitemsParams);
            return objOrderitemsParams[42].Value.ToString();
        }
        #endregion

        #region Insert/Update Data in tblShippingMethods and tblShipping
        public void LoadShippingMethods(string JsonData)
        {
            try
            {
                dynamic array = JsonConvert.DeserializeObject(JsonData);
                string strJson = Convert.ToString(array);
                dynamic objData = JsonConvert.DeserializeObject(strJson);
                if (connection.State == ConnectionState.Closed)
                {
                    connection.ConnectionString = MakeConnection.GetConnection();
                    connection.Open();
                }

                #region "Objects and variables"
                ShippingMethod objShippingMethod = new ShippingMethod();
                List<ShippingMethods> lstShippingMethods = new List<ShippingMethods>();
                #endregion

                #region "Map json to class"
                lstShippingMethods = (List<ShippingMethods>)JsonConvert.DeserializeObject(Convert.ToString(objData), typeof(List<ShippingMethods>));
                #endregion

                for (int i = 0; i < lstShippingMethods.Count; i++)
                {
                    for (int j = 0; j < lstShippingMethods[i].PostalServices.Count; j++)
                    {
                        SqlParameter[] objShippingParams = new SqlParameter[5];

                        objShippingParams[0] = new SqlParameter("@pKPostalServiceID", SqlDbType.UniqueIdentifier);
                        objShippingParams[0].Value = lstShippingMethods[i].PostalServices[j].pkPostalServiceId;

                        objShippingParams[1] = new SqlParameter("@Vendor", SqlDbType.VarChar, 500);
                        objShippingParams[1].Value = lstShippingMethods[i].PostalServices[j].Vendor;

                        objShippingParams[2] = new SqlParameter("@PostalServiceName", SqlDbType.VarChar, 500);
                        objShippingParams[2].Value = lstShippingMethods[i].PostalServices[j].PostalServiceName;

                        objShippingParams[3] = new SqlParameter("@TrackingNumberRequired", SqlDbType.Bit);
                        objShippingParams[3].Value = lstShippingMethods[i].PostalServices[j].TrackingNumberRequired;

                        objShippingParams[4] = new SqlParameter("@Result", SqlDbType.Int);
                        objShippingParams[4].Direction = ParameterDirection.Output;

                        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "InsertUpdate_tblShippingMethods", objShippingParams);
                        int Result = Convert.ToInt32(objShippingParams[4].Value);
                    }
                }
            }
            catch (IndexOutOfRangeException IndexEx)
            {
                Utill.LogError(IndexEx, "IndexOutOfRangeException");
            }
            catch (SqlException SqlEx)
            {
                Utill.LogError(SqlEx, "LoadShippingMethods SqlException");
            }
            catch (Exception ex)
            {
                Utill.LogError(ex, "Main Exception In LoadShipping Method");
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region "Insert/Update Data in tblPackagingCategory/tblPackageTypes"
        public void LoadPackagingGroup(string strPackageTypes)
        {
            try
            {
                dynamic array = JsonConvert.DeserializeObject(strPackageTypes);
                string strJson = Convert.ToString(array);
                dynamic objData = JsonConvert.DeserializeObject(strJson);
                if (connection.State == ConnectionState.Closed)
                {
                    connection.ConnectionString = MakeConnection.GetConnection();
                    connection.Open();
                }

                #region "Objects and variables"
                List<PackageGroups> lstPackageGroups = new List<PackageGroups>();
                #endregion

                #region "Map json to class"
                lstPackageGroups = (List<PackageGroups>)JsonConvert.DeserializeObject(Convert.ToString(objData), typeof(List<PackageGroups>));
                #endregion

                #region "Database Connectivity"
                for (int i = 0; i < lstPackageGroups.Count; i++)
                {
                    for (int j = 0; j < lstPackageGroups[i].PackageTypes.Count; j++)
                    {
                        SqlParameter[] objPackageParams = new SqlParameter[16];

                        objPackageParams[0] = new SqlParameter("@PackageCategoryID", SqlDbType.UniqueIdentifier);
                        objPackageParams[0].Value = lstPackageGroups[i].PackageCategoryID;

                        objPackageParams[1] = new SqlParameter("@PackageCategory", SqlDbType.VarChar, 500);
                        objPackageParams[1].Value = lstPackageGroups[i].PackageCategory;

                        objPackageParams[2] = new SqlParameter("@RowGuid", SqlDbType.UniqueIdentifier);
                        objPackageParams[2].Value = lstPackageGroups[i].rowguid;

                        objPackageParams[3] = new SqlParameter("@PackageTypeID", SqlDbType.UniqueIdentifier);
                        objPackageParams[3].Value = lstPackageGroups[i].PackageTypes[j].PackageTypeID;

                        objPackageParams[4] = new SqlParameter("@PackageGroupId", SqlDbType.UniqueIdentifier);
                        objPackageParams[4].Value = lstPackageGroups[i].PackageTypes[j].PackageGroupId;

                        objPackageParams[5] = new SqlParameter("@PackageTitle", SqlDbType.VarChar, 500);
                        objPackageParams[5].Value = lstPackageGroups[i].PackageTypes[j].PackageTitle;

                        objPackageParams[6] = new SqlParameter("@FromGramms", SqlDbType.Decimal);
                        objPackageParams[6].Value = lstPackageGroups[i].PackageTypes[j].FromGramms;

                        objPackageParams[7] = new SqlParameter("@ToGramms", SqlDbType.Decimal);
                        objPackageParams[7].Value = lstPackageGroups[i].PackageTypes[j].ToGramms;

                        objPackageParams[8] = new SqlParameter("@PackagingWeight", SqlDbType.Decimal);
                        objPackageParams[8].Value = lstPackageGroups[i].PackageTypes[j].PackagingWeight;

                        objPackageParams[9] = new SqlParameter("@PackagingCapacity", SqlDbType.Decimal);
                        objPackageParams[9].Value = lstPackageGroups[i].PackageTypes[j].PackagingCapacity;

                        objPackageParams[10] = new SqlParameter("@Rowguid_PackageType", SqlDbType.UniqueIdentifier);
                        objPackageParams[10].Value = lstPackageGroups[i].PackageTypes[j].Rowguid;

                        objPackageParams[11] = new SqlParameter("@Width", SqlDbType.Decimal);
                        objPackageParams[11].Value = lstPackageGroups[i].PackageTypes[j].Width;

                        objPackageParams[12] = new SqlParameter("@Height", SqlDbType.Decimal);
                        objPackageParams[12].Value = lstPackageGroups[i].PackageTypes[j].Height;

                        objPackageParams[13] = new SqlParameter("@Depth", SqlDbType.Decimal);
                        objPackageParams[13].Value = lstPackageGroups[i].PackageTypes[j].Depth;

                        objPackageParams[14] = new SqlParameter("@ResultPackageCategory", SqlDbType.Int);
                        objPackageParams[14].Direction = ParameterDirection.Output;

                        objPackageParams[15] = new SqlParameter("@ResultPackageType", SqlDbType.Int);
                        objPackageParams[15].Direction = ParameterDirection.Output;

                        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "InsertUpdate_tblPackagingCategoryAndtblPackageTypes", objPackageParams);

                        //Check status of table tblPackagingCategory ResultPackageCategory=0 means update and ResultPackageCategory=1 means insert
                        int ResultPackageCategory = Convert.ToInt32(objPackageParams[14].Value);

                        //Check status of table tblPackageTypes ResultPackageTypes=0 means update and ResultPackageTypes=1 means insert
                        int ResultPackageTypes = Convert.ToInt32(objPackageParams[15].Value);
                    }
                }
                #endregion
            }
            catch (IndexOutOfRangeException IndexEx)
            {
                Utill.LogError(IndexEx, "IndexOutOfRangeException");
            }
            catch (SqlException SqlEx)
            {
                Utill.LogError(SqlEx, "LoadPackagingGroup SqlException");
            }
            catch (Exception ex)
            {
                Utill.LogError(ex, "Main Exception In LoadShipping Method");
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region "Insert/Update Data in tblChannels/tblShippingServices/tblPostalServices"
        public void LoadPostalServices(string strPostalServices)
        {
            try
            {
                dynamic array = JsonConvert.DeserializeObject(strPostalServices);
                string strJson = Convert.ToString(array);
                dynamic objData = JsonConvert.DeserializeObject(strJson);
                if (connection.State == ConnectionState.Closed)
                {
                    connection.ConnectionString = MakeConnection.GetConnection();
                    connection.Open();
                }
                #region "Objects and variables"
                List<clsPostalServices> objPostalServices = new List<clsPostalServices>();
                #endregion

                #region "Map json to class"
                objPostalServices = JsonConvert.DeserializeObject<List<clsPostalServices>>(Convert.ToString(objData));
                #endregion

                #region "Database Connectivity"
                for (int i = 0; i < objPostalServices.Count; i++)
                {
                    SqlParameter[] objPostalParams = new SqlParameter[15];

                    objPostalParams[0] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                    objPostalParams[0].Value = objPostalServices[i].id;

                    objPostalParams[1] = new SqlParameter("@hasMappedShippingService", SqlDbType.Bit);
                    objPostalParams[1].Value = objPostalServices[i].hasMappedShippingService;

                    objPostalParams[2] = new SqlParameter("@PostalServiceName", SqlDbType.VarChar, 500);
                    objPostalParams[2].Value = objPostalServices[i].PostalServiceName;

                    objPostalParams[3] = new SqlParameter("@PostalServiceTag", SqlDbType.VarChar, 8000);
                    objPostalParams[3].Value = objPostalServices[i].PostalServiceTag;

                    objPostalParams[4] = new SqlParameter("@ServiceCountry", SqlDbType.VarChar, 500);
                    objPostalParams[4].Value = objPostalServices[i].ServiceCountry;

                    objPostalParams[5] = new SqlParameter("@PostalServiceCode", SqlDbType.VarChar, 500);
                    objPostalParams[5].Value = objPostalServices[i].PostalServiceCode;

                    objPostalParams[6] = new SqlParameter("@Vendor", SqlDbType.VarChar, 500);
                    objPostalParams[6].Value = objPostalServices[i].Vendor;

                    objPostalParams[7] = new SqlParameter("@PrintModule", SqlDbType.VarChar, 500);
                    objPostalParams[7].Value = objPostalServices[i].PrintModule;

                    objPostalParams[8] = new SqlParameter("@PrintModuleTitle", SqlDbType.VarChar, 500);
                    objPostalParams[8].Value = objPostalServices[i].PrintModuleTitle;

                    objPostalParams[9] = new SqlParameter("@pkPostalServiceId", SqlDbType.UniqueIdentifier);
                    objPostalParams[9].Value = objPostalServices[i].pkPostalServiceId;

                    objPostalParams[10] = new SqlParameter("@TrackingNumberRequired", SqlDbType.Bit);
                    objPostalParams[10].Value = objPostalServices[i].TrackingNumberRequired;

                    objPostalParams[11] = new SqlParameter("@WeightRequired", SqlDbType.Bit);
                    objPostalParams[11].Value = objPostalServices[i].WeightRequired;

                    objPostalParams[12] = new SqlParameter("@IgnorePackagingGroup", SqlDbType.VarChar, 500);
                    objPostalParams[12].Value = objPostalServices[i].IgnorePackagingGroup;

                    objPostalParams[13] = new SqlParameter("@fkShippingAPIConfigId", SqlDbType.VarChar, 500);
                    objPostalParams[13].Value = objPostalServices[i].fkShippingAPIConfigId;

                    objPostalParams[14] = new SqlParameter("@IntegratedServiceId", SqlDbType.VarChar, 500);
                    objPostalParams[14].Value = objPostalServices[i].IntegratedServiceId;

                    SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "PostalServicesAddUpdateDelete", objPostalParams);
                    for (int j = 0; j < objPostalServices[i].Channels.Count; j++)
                    {
                        SqlParameter[] objChannelsParams = new SqlParameter[4];

                        objChannelsParams[0] = new SqlParameter("@pkPostalServiceId", SqlDbType.UniqueIdentifier);
                        objChannelsParams[0].Value = objPostalServices[i].Channels[j].pkPostalServiceId;

                        objChannelsParams[1] = new SqlParameter("@PostalServiceName", SqlDbType.VarChar, 500);
                        objChannelsParams[1].Value = objPostalServices[i].Channels[j].PostalServiceName;

                        objChannelsParams[2] = new SqlParameter("@Source", SqlDbType.VarChar, 500);
                        objChannelsParams[2].Value = objPostalServices[i].Channels[j].Source;

                        objChannelsParams[3] = new SqlParameter("@SubSource", SqlDbType.VarChar, 500);
                        objChannelsParams[3].Value = objPostalServices[i].Channels[j].SubSource;

                        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "ChannelsAddUpdateDelete", objChannelsParams);
                    }
                    for (int j = 0; j < objPostalServices[i].ShippingServices.Count; j++)
                    {
                        SqlParameter[] objShippingParams = new SqlParameter[4];

                        objShippingParams[0] = new SqlParameter("@pkPostalServiceId", SqlDbType.UniqueIdentifier);
                        objShippingParams[0].Value = objPostalServices[i].ShippingServices[j].pkPostalServiceId;

                        objShippingParams[1] = new SqlParameter("@PostalServiceName", SqlDbType.VarChar, 500);
                        objShippingParams[1].Value = objPostalServices[i].ShippingServices[j].PostalServiceName;

                        objShippingParams[2] = new SqlParameter("@Vendor", SqlDbType.VarChar, 500);
                        objShippingParams[2].Value = objPostalServices[i].ShippingServices[j].vendor;

                        objShippingParams[3] = new SqlParameter("@accountid", SqlDbType.VarChar, 500);
                        objShippingParams[3].Value = objPostalServices[i].ShippingServices[j].accountid;

                        SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "ShippingServicesAddUpdateDelete", objShippingParams);
                    }
                }
                #endregion
            }
            catch (IndexOutOfRangeException IndexEx)
            {
                Utill.LogError(IndexEx, "LoadPostalServices IndexOutOfRangeException");
            }
            catch (SqlException SqlEx)
            {
                Utill.LogError(SqlEx, "LoadPostalServices SqlException");
            }
            catch (Exception ex)
            {
                Utill.LogError(ex, "Main Exception In LoadPostalServices Method");
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region "Clear Cancelled and Processed Orders"
        public void ClearCancelledAndProcessedOrders(string OrderID)
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.ConnectionString = MakeConnection.GetConnection();
                    connection.Open();
                }
                //We are using sqlcommand here because we need to set timeout for execution, because we are passing >100 orderids to clear at once
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@OrderID", SqlDbType.VarChar, int.MaxValue).Value = OrderID;
                cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "RemoveCancelledAndProcessedOrders";
                cmd.CommandTimeout = 200;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
                int intResult = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                if (intResult != 1)
                {
                    throw new CustomException("Error Occurred in clearing data stored procedure, Error code=" + intResult);
                }
                else if (intResult == 1)
                {
                    //Replacing ,(comma) with && because we are replacing && with new line in c# to watch log.
                    string strOrderIds = System.Text.RegularExpressions.Regex.Replace(OrderID, @"\,+", "&&");

                    Utill.LogHistory("Order Ids to remove " + strOrderIds, Guid.NewGuid());
                }
            }
            catch (IndexOutOfRangeException IndexEx)
            {
                Utill.LogError(IndexEx, "ClearCancelledAndProcessedOrders IndexOutOfRangeException");
            }
            catch (SqlException SqlEx)
            {
                Utill.LogError(SqlEx, "ClearCancelledAndProcessedOrders SqlException");
            }
            catch (Exception ex)
            {
                Utill.LogError(ex, "Main Exception In ClearCancelledAndProcessedOrders Method");
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region "Add update Messages in the Database"
        public void GetAddUpdateMessages(System.Net.Mail.MailMessage Mail, string strMode, string strMessageUID, string strMessageType, string CustomerId, string Subject, string strMessageBody, Guid FolderID, Guid AccountID, Guid EbayUserID = new Guid(), string strExtMessageID = "", string strItemID = "")
        {

            DataTable objDt = new DataTable();
            DataSet objDs = new DataSet();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            byte btIsAttachment;
            string strResult;
            if (Mail.Attachments.Count != 0)
            {
                btIsAttachment = 1;
            }
            else
            {
                btIsAttachment = 0;
            }
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[14];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objParam[1] = new SqlParameter("@MessageUID", SqlDbType.VarChar, 500);
                objParam[1].Value = strMessageUID;

                objParam[2] = new SqlParameter("@CustomerId", SqlDbType.VarChar, 100);
                objParam[2].Value = CustomerId;

                objParam[3] = new SqlParameter("@MessageType", SqlDbType.VarChar, 50);
                objParam[3].Value = strMessageType.ToUpper();

                objParam[4] = new SqlParameter("@Subject", SqlDbType.VarChar, 200);
                objParam[4].Value = Subject;

                objParam[5] = new SqlParameter("@MessageBody", SqlDbType.VarChar, strMessageBody.Length);
                objParam[5].Value = strMessageBody;

                objParam[6] = new SqlParameter("@IsAttachment", SqlDbType.Bit);
                objParam[6].Value = btIsAttachment;

                objParam[8] = new SqlParameter("@FolderID", SqlDbType.UniqueIdentifier);
                objParam[8].Value = FolderID;

                objParam[9] = new SqlParameter("@AccountID", SqlDbType.UniqueIdentifier);
                objParam[9].Value = AccountID;

                objParam[10] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[10].Direction = ParameterDirection.Output;

                objParam[11] = new SqlParameter("@ItemID", SqlDbType.VarChar, 500);
                objParam[11].Value = strItemID;

                objParam[12] = new SqlParameter("@ExtMessageID", SqlDbType.VarChar, 500);
                objParam[12].Value = strExtMessageID;

                objParam[13] = new SqlParameter("@EbayUserID", SqlDbType.UniqueIdentifier);
                objParam[13].Value = EbayUserID;

                if (strMode.ToUpper() == "UPDATE" || strMode.ToUpper() == "INSERT")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateMessages", objParam);
                }
                strResult = objParam[10].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    Utill.LogHistory(strResult, new Guid());
                }
                string strLog = "Folder ID for this rule is: " + FolderID + "Message sent by: " + CustomerId + "Message UID: " + strMessageUID;
                Utill.LogHistory(strLog, new Guid());

            }
            catch (Exception ex)
            {
                Utill.LogError(ex, "Main Exception In GetAddUpdateMessages Method");
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region "Get Message IDs to match with MessageIDs from ebay GetMessageAPI to store only unread messages"
        public DataTable GetMessageIDsForMessages(Guid AccountID)
        {
            DataTable objDt = new DataTable();
            DataSet objDs = new DataSet();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {


                SqlParameter[] objParam = new SqlParameter[2];

                objParam[0] = new SqlParameter("@AccountID", SqlDbType.UniqueIdentifier);
                objParam[0].Value = AccountID;

                objParam[1] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[1].Direction = ParameterDirection.Output;

                objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetMessageIdsForAccount", objParam);
                string strResult = objParam[1].Value.ToString();

                if (strResult == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            finally { conn.Close(); }

            return objDt;
        }
        #endregion

        #region "Get Rules for Messages to match them"
        public dynamic GetAddUpdateRules(string strMode, Guid ID, Guid FolderID, string strName, string strConditon, int Position, bool IsActive, string strConditonID)
        {
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[9];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode;

                objParam[1] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = ID;

                objParam[2] = new SqlParameter("@FolderID", SqlDbType.UniqueIdentifier);
                objParam[2].Value = FolderID;

                objParam[3] = new SqlParameter("@Name", SqlDbType.VarChar, 500);
                objParam[3].Value = strName;

                objParam[4] = new SqlParameter("@Condition", SqlDbType.VarChar, 500);
                objParam[4].Value = strConditon;

                objParam[5] = new SqlParameter("@Position", SqlDbType.SmallInt);
                objParam[5].Value = Position;

                objParam[6] = new SqlParameter("@IsActive", SqlDbType.Bit);
                objParam[6].Value = Convert.ToByte(IsActive);

                objParam[7] = new SqlParameter("@ConditionID", SqlDbType.UniqueIdentifier);
                objParam[7].Value = new Guid(strConditonID);

                objParam[8] = new SqlParameter("@Result", SqlDbType.VarChar, 2000);
                objParam[8].Direction = ParameterDirection.Output;
                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetAddUpdateRules", objParam);
                }
                else if (strMode.ToUpper() == "UPDATE")
                {
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "GetAddUpdateRules", objParam);
                }
                string strResult = objParam[8].Value.ToString();
                //if (strResult.ToUpper().Contains("ERROR"))
                //{
                //    throw;
                //}
                if (strResult == "0")
                {
                    if (objDs.Tables[0].Rows.Count > 0)
                    {
                        objDt = objDs.Tables[0];
                    }
                    return objDt;
                }
                else
                {
                    return strResult;
                }
            }
            catch (Exception ex)
            {
                Utill.LogError(ex, "Main Exception In GetAddUpdateRules Method");
                if (strMode.ToUpper() == "UPDATE")
                {
                    return objDt;
                }
                else
                {
                    return "ERROR OCCURRED";
                }

            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region "Get Email Accounts to fetch Messages From"
        public dynamic GetMessageAccount(string strMode)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetMessageAccounts", objParam);

                if (objDs.Tables[0].Rows.Count > 0)
                {
                    objDt = objDs.Tables[0];
                }
                return objDt;

            }
            catch (Exception ex)
            {
                Utill.LogError(ex, "Main Exception In GetMessageAccount Method");
                return objDt;
            }
            finally
            {

                conn.Close();

            }

        }

        #endregion

        #region "Get Ebay User Details"
        public DataTable GetEbayUsers(string strMode, Guid ID, Guid AccountID, string UserName, string EbayAuthToken)
        {
            DataSet objDs = new DataSet();
            DataTable objDt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = MakeConnection.GetConnection();
            conn.Open();
            try
            {
                SqlParameter[] objParam = new SqlParameter[6];

                objParam[0] = new SqlParameter("@Mode", SqlDbType.VarChar, 50);
                objParam[0].Value = strMode.ToUpper();

                objParam[1] = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
                objParam[1].Value = ID;

                objParam[2] = new SqlParameter("@AccountID", SqlDbType.UniqueIdentifier);
                objParam[2].Value = AccountID;

                objParam[3] = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
                objParam[3].Value = UserName;

                objParam[4] = new SqlParameter("@EbayAuthToken", SqlDbType.VarChar, EbayAuthToken.Length);
                objParam[4].Value = EbayAuthToken;

                objParam[5] = new SqlParameter("@Result", SqlDbType.VarChar, 50);
                objParam[5].Direction = ParameterDirection.Output;

                if (strMode.ToUpper() == "GET")
                {
                    objDs = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GetEbayTokenWithUserName", objParam);
                }

                string strResult = objParam[5].Value.ToString();
                if (strResult.ToUpper().Contains("ERROR"))
                {
                    throw new CustomException(strResult);
                }
                if (strResult == "0")
                {
                    if (objDs.Tables.Count > 0)
                    {
                        if (objDs.Tables[0].Rows.Count > 0)
                        {
                            objDt = objDs.Tables[0];

                        }
                    }

                }
                return objDt;

            }
            catch (Exception ex)
            {
                throw;
            }
            finally { conn.Close(); }
        }
        #endregion

        #region "Get Pending Emails"
        public DataTable GetPendingOrSentEmail(string strSentOrPending, string FromDate = "", string ToDate = "", string strSearchValue = "")
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = MakeConnection.GetConnection();
            connection.Open();
            DataTable objDt = new DataTable();
            try
            {
                SqlParameter[] objParam = new SqlParameter[4];

                objParam[0] = new SqlParameter("@SentOrPending", SqlDbType.VarChar, 20);
                objParam[0].Value = strSentOrPending;

                objParam[1] = new SqlParameter("@FromDate", SqlDbType.VarChar, 10);
                objParam[1].Value = FromDate;

                objParam[2] = new SqlParameter("@ToDate", SqlDbType.VarChar, 10);
                objParam[2].Value = ToDate;

                objParam[3] = new SqlParameter("@SearchValue", SqlDbType.VarChar, 40);
                objParam[3].Value = strSearchValue;

                objDt = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GetDataForPendingEmail", objParam).Tables[0];
            }
            catch (Exception ex)
            {

                Utill.LogHistory(string.Format("Exception occurred while getting data for {0}&&Exception:- {1}", strSentOrPending, ex.Message), Guid.NewGuid());
            }
            finally { connection.Close(); }
            return objDt;
        }

        #endregion
        #region "Custom Data Source for Sending Emails"
        public DataTable CreateCustomDataSource(DataTable objDtPendingEmails)
        {
            DataTable objDtCondition = new DataTable();
            DataView dvFilterCondition = new DataView();
            DataTable objDtTemplateDetails = new DataTable();
            DataTable objDtDataToShowInTemplate = new DataTable();
            Guid OrderId = new Guid();
            int intRowCount = 0;
            if (objDtCondition.Rows.Count == 0)
            {
                objDtCondition = Utill.GetEmailConditions();
            }
            string strIDs = "", strMailID = "";
        ExecuteIfErrorInMainLoop:
            try
            {
                for (int i = intRowCount; i < objDtPendingEmails.Rows.Count; i++)
                {
                    string strDataColumns = "", strDataColumnsForSubject = "", strDataColumnsForDatabaseUse = "", strSubject = "";
                    string[] strArrColumnsInSubject = new string[] { };
                    dvFilterCondition = new DataView(objDtCondition);
                    Guid ConditionID = new Guid();
                    Guid TemplateID = new Guid();
                    string strSource = Convert.ToString(objDtPendingEmails.Rows[i]["Source"]);
                    string strSubSource = Convert.ToString(objDtPendingEmails.Rows[i]["SubSource"]);
                    OrderId = new Guid(Convert.ToString(objDtPendingEmails.Rows[i]["OrderId"]));
                    strMailID = Convert.ToString(objDtPendingEmails.Rows[i]["ID"]);
                    ConditionID = objEmail.GetConditionIDByConditionText(strSource, strSubSource, dvFilterCondition);
                ExecuteAgainIfTemplateIsDisabled:
                    objDtTemplateDetails = Utill.GetTemplateDetailsByTemplateId(TemplateID, ConditionID);
                    bool blIsEnabled = false;
                    if (objDtTemplateDetails.Rows.Count > 0)
                    {
                        blIsEnabled = Convert.ToBoolean(objDtTemplateDetails.Rows[0]["IsEnabled"]);
                    }
                    else
                    {
                        ConditionID = new Guid();
                        goto ExecuteAgainIfTemplateIsDisabled;
                    }
                    /* ConditionID can only be equal to 00000000-0000-0000-0000-000000000000 when either matched template is disabled 
                     * or don't have any matched template
                     * So in that case we need to skip those emails
                     */
                    if (ConditionID.ToString() == "00000000-0000-0000-0000-000000000000" && !blIsEnabled)
                    {
                        strIDs = strIDs == "" ? strMailID : strIDs + "," + strMailID;
                    }
                    /*Checking if this template is enabled or not, if not then setting ConditionID to default value of guid,
                      and executing GetTemplateDetailsByTemplateId again to get default template and settings
                     */
                    if (blIsEnabled)
                    {
                        strSubject = Convert.ToString(objDtTemplateDetails.Rows[0]["Subject"]);
                        if (strSubject.Contains("[{"))
                        {
                            objEmail.GetColumnNamesToGetDataFromDB(strSubject, ref strDataColumnsForDatabaseUse, ref strDataColumnsForSubject, ref strDataColumns, ref strArrColumnsInSubject, true);
                        ExecuteIf207Error:
                            try
                            {
                                //strDataColumnsForDatabaseUse ="A.XYZ AS [A.XYZ],"+ strDataColumnsForDatabaseUse + ",A.ABC AS [A.ABC]";
                                objDtDataToShowInTemplate = objEmail.GetDataToShowInTemplate(strDataColumnsForDatabaseUse, Convert.ToString(objDtPendingEmails.Rows[i]["OrderId"]));
                            }
                            catch (SqlException sqlEx)
                            {
                                /*If 207 error(means invalid column call) then RemoveInvalidColumnIfExistsInString method is
                                 *removing that column from list of text and database column string as well.
                                 * Then executing again with valid columns
                                 */
                                if (sqlEx.Number == 207)
                                {
                                    string strStringToCompare = strDataColumns;
                                    Utill.RemoveInvalidColumnIfExistsInString(ref strDataColumnsForSubject, ref strDataColumnsForDatabaseUse, ref strArrColumnsInSubject, sqlEx.Message);
                                    goto ExecuteIf207Error;
                                }
                            }
                            if (objDtDataToShowInTemplate.Rows.Count > 0)
                            {
                                strSubject = objEmail.ReplaceColumnWithData(strDataColumns, strDataColumnsForSubject, strSubject, objDtDataToShowInTemplate, true, strArrColumnsInSubject);
                            }
                        }
                        objDtPendingEmails.Rows[i]["Subject"] = strSubject;
                        intRowCount++;
                    }
                    else if (ConditionID.ToString() != "00000000-0000-0000-0000-000000000000")
                    {
                        ConditionID = new Guid();//Setting condition id to default
                        goto ExecuteAgainIfTemplateIsDisabled;
                    }
                }
            }
            catch (Exception ex)
            {
                intRowCount++;
                strIDs = strIDs == "" ? strMailID : strIDs + "," + strMailID;
                GuidForLog = Guid.NewGuid();
                OrderIdToLog = OrderId;
                Utill.LogHistory("Error occurred while creating custom data source to bind pending email screen&&" + ex.Message, Guid.NewGuid());
                goto ExecuteIfErrorInMainLoop;
            }
            finally
            {
                if (strIDs != "")
                {
                    string[] strArrMailsToSkip = strIDs.Split(',');
                    for (int i = 0; i < strArrMailsToSkip.Length; i++)
                    {
                        DataView dv = new DataView(objDtPendingEmails);
                        dv.RowFilter = "ID <> '" + strArrMailsToSkip[i] + "'";
                        objDtPendingEmails = dv.ToTable();
                    }
                }
            }
            // blLoadAllData = false;
            return objDtPendingEmails;
        }
        #endregion
    }
}
