﻿using System;
using System.Configuration;
namespace OrderDispatchWcfService
{
    public class MakeConnection
    {
        public static string GetConnection()
        {
            string strConnection = "";
            try
            {
                strConnection = ConfigurationManager.ConnectionStrings["OrderDispatchConnection"].ToString();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return strConnection;
        }
    }
}
