﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
namespace OrderDispatchWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IOrderDispatch" in both code and config file together.
    [ServiceContract]
    public interface IOrderDispatch
    {
        [OperationContract]
        void SyncData();

        [OperationContract]
        string StopService();

        [OperationContract]
        string StartService();
        
        [OperationContract]
        void StartServiceOnLoad();

        [OperationContract]
        void SyncMessages();

        [OperationContract]
        void AutoSendEmailsForProcessedOrders();
    }
}
