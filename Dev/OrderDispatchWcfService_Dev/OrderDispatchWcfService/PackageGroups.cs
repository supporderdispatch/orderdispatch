﻿using System;
using System.Collections.Generic;
namespace OrderDispatchWcfService
{
    class PackageGroups
    {
        public Guid PackageCategoryID { get; set; }
        public string PackageCategory { get; set; }
        public Guid rowguid { get; set; }

        public List<PackageType> PackageTypes { get; set; }
    }
}
