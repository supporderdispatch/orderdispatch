﻿using Newtonsoft.Json;
using RestSharp;
using S22.Imap;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;

namespace OrderDispatchWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "OrderDispatch" in both code and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class OrderDispatch : IOrderDispatch
    {
        OrderDispatchEntities objContext = new OrderDispatchEntities();
        StoreDataInLocaldb objdb = new StoreDataInLocaldb();
        System.Timers.Timer timer = new System.Timers.Timer();
        string strToken = "", strServer = "";

        //for auto email sender
        string strIdsToDeleteOrUpdate = "", strNumOrderIds = "", strOrderIds = "";
        SendEmailEntity objSendEmail = new SendEmailEntity();
        List<SendEmailEntity> lstEmails = new List<SendEmailEntity>();
        Email objEmail = new Email();
        DataTable objDtBindGrid = new DataTable();

        public void StartServiceOnLoad()
        {
            timer = new System.Timers.Timer();
            timer.Elapsed += new ElapsedEventHandler(WCFService_Elapsed);
            timer.Interval = Convert.ToDouble(ConfigurationManager.AppSettings["TimeIntervalInMin"]);
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Start();
            // to be uncommented
            SyncMessages();
         //   SyncData();
           // AutoSendEmailsForProcessedOrders();
        }
        /// <summary>
        /// To stop sync service, that will stop the timmer but, if sync is executing then it will get execute.
        /// Exp. if sync is running and user pressed stop button on web version then the sync will complete its on going execution and will not get execute after that
        /// </summary>
        /// <returns>Sarvice status to web version of OD, which is consuming this service</returns>
        public string StopService()
        {
            timer.Stop();
            return "Service Stopped Successfully!";
        }

        /// <summary>
        /// Will start the timmer then sync will get execute after time intervals
        /// </summary>
        /// <returns>Service status to web version of OD, which is consuming this service</returns>
        public string StartService()
        {
            try
            {
                timer.Stop();
                timer.Start();
                return "Service Started Successfully!";
            }
            catch (Exception ExStart)
            {
                Utill.LogError(ExStart, "Erro in StartService method");
                return "Error";
            }
        }

        /// <summary>
        /// An event to trigger the timmer of sync service
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void WCFService_Elapsed(object sender, ElapsedEventArgs e)
        {
            // to be uncommented
            //SyncMessages();
            //SyncData();
            //AutoSendEmailsForProcessedOrders();
            //ClearProcessedAndCancelledOrders();
        }

        /// <summary>
        /// This method is main, this is handling sync process for OD,
        /// this method is calling required apis and methods to sync data between linnwork and OD db
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void SyncData()
        {
            try
            {
                timer.Stop();
                int intIntervalTime = Convert.ToInt32(ConfigurationManager.AppSettings["TimeIntervalInMin"]);
                string strServiceStatus = objContext.WebServiceStatus.Select(x => x.IsServiceOn).FirstOrDefault();
                WebServiceStatu objServiceStatus = objContext.WebServiceStatus.Where(x => x.ServiceID == 1).FirstOrDefault();
                objServiceStatus.HostStartedOn = DateTime.Now;
                objServiceStatus.IsServiceOn = strServiceStatus;
                objServiceStatus.NextCall = intIntervalTime;
                objContext.WebServiceStatus.Attach(objServiceStatus);
                objContext.Entry(objServiceStatus).State = EntityState.Modified;
                objContext.SaveChanges();
                Console.WriteLine("Current state of service is " + strServiceStatus);
               
                if (Convert.ToString(strServiceStatus) != null && Convert.ToString(strServiceStatus).ToUpper() == "Y")//means user want to call api automatically
                {
                    string strShippingMethodsData = "";
                    string strPackageTypes = "";
                    string strShippingServices = "";
                    List<Guid> lstOrderIds = new List<Guid>();
                    try
                    {
                        //Calling of Authorize api to get the Authorization key(Token key) to call the other order dispatch APIs
                        using (var AuthClient = new HttpClient())
                        {
                            var urls = "https://api.linnworks.net//api/Auth/AuthorizeByApplication?applicationId=53e8b308-3563-439d-b7b7-36c4b91e132a&applicationSecret=32d0c53e-76dc-460a-b63f-bafc3f1bb6d3&token=be6c4095945b0457151129ac87f2ef9e";
                            AuthClient.BaseAddress = new Uri(urls);
                            HttpResponseMessage responsemsg = AuthClient.GetAsync(urls).Result;
                            string jsonData = responsemsg.Content.ReadAsStringAsync().Result;
                            dynamic objData = JsonConvert.DeserializeObject(jsonData);
                            strServer = Convert.ToString(objData["Server"]);
                            strToken = Convert.ToString(objData["Token"]);
                        }
                        //End of Calling of Authorize api
                        //Code to call api of shipping methods to store returned data into our local database
                        using (var client = new HttpClient())
                        {
                            client.DefaultRequestHeaders.Accept.Clear();
                            //api call for Get All Open Orders api
                            // string sorting = "{'ListFields': [{'FieldCode':'GENERAL_INFO_STATUS','Type':0,'Value':1}, {'FieldCode':'GENERAL_INFO_STATUS','Type':0,'Value':4}, {'FieldCode':'GENERAL_INFO_STATUS','Type':1,'Value':5}, {'FieldCode':'GENERAL_INFO_SOURCE','Type':1,'Value':'DATAIMPORTEXPORT'}, {'FieldCode':'FOLDER','Type':0,'Value':'PACKING_LIST'}, {'FieldCode':'FOLDER','Type':0,'Value':'LATE_ORDERS'}, {'FieldCode':'FOLDER','Type':0,'Value':'LATE_ORDERS_1_DAY'}, {'FieldCode':'FOLDER','Type':0,'Value':'URGENT'}, {'FieldCode':'FOLDER','Type':1,'Value':'USACHECK'}]}&sorting=[]&fulfilmentCenter=00000000-0000-0000-0000-000000000000&additionalFilter=''";
                            string sorting = "{'ListFields': [{'FieldCode':'GENERAL_INFO_STATUS','Type':0,'Value':1}, {'FieldCode':'GENERAL_INFO_STATUS','Type':0,'Value':4}, {'FieldCode':'GENERAL_INFO_STATUS','Type':1,'Value':5}, {'FieldCode':'FOLDER','Type':0,'Value':'PACKING_LIST'}, {'FieldCode':'FOLDER','Type':0,'Value':'LATE_ORDERS'}, {'FieldCode':'FOLDER','Type':0,'Value':'LATE_ORDERS_1_DAY'}, {'FieldCode':'FOLDER','Type':0,'Value':'URGENT'}, {'FieldCode':'FOLDER','Type':1,'Value':'USACHECK'}]}&sorting=[]&fulfilmentCenter=00000000-0000-0000-0000-000000000000&additionalFilter=''";
                            string urls = strServer + "//api/Orders/GetAllOpenOrders?filters=" + sorting;
                            client.DefaultRequestHeaders.Add("Accept", "application/*+xml;version=5.1");
                            client.DefaultRequestHeaders.Add("Authorization", strToken);
                            client.BaseAddress = new Uri(urls);
                            HttpResponseMessage responsemsg = client.GetAsync(urls).Result;
                            string strOrderIds = responsemsg.Content.ReadAsStringAsync().Result;
                            lstOrderIds = JsonConvert.DeserializeObject<List<Guid>>(strOrderIds);
                        }
                        //End of Code to call api of shipping methods
                        //Code to call api of shipping methods to store returned data into our local database

                        using (var client = new HttpClient())
                        {
                            client.DefaultRequestHeaders.Accept.Clear();
                            string urls = strServer + "//api/Orders/GetShippingMethods";
                            client.DefaultRequestHeaders.Add("Accept", "application/*+xml;version=5.1");
                            client.DefaultRequestHeaders.Add("Authorization", strToken);
                            client.BaseAddress = new Uri(urls);
                            HttpResponseMessage responsemsg = client.GetAsync(urls).Result;
                            strShippingMethodsData = responsemsg.Content.ReadAsStringAsync().Result;
                        }
                        //End of Code to call api of shipping methods

                        //Code to call packageing group api to store returned data into our local databse
                        using (var Client = new HttpClient())
                        {
                            Client.DefaultRequestHeaders.Accept.Clear();
                            string urls = strServer + "//api/Orders/GetPackagingGroups";
                            Client.DefaultRequestHeaders.Add("Accept", "application/*+xml;version=5.1");
                            Client.DefaultRequestHeaders.Add("Authorization", strToken);
                            Client.BaseAddress = new Uri(urls);
                            HttpResponseMessage responsemsg = Client.GetAsync(urls).Result;
                            strPackageTypes = responsemsg.Content.ReadAsStringAsync().Result;
                        }
                        //End of Code to call packageing group


                        //Code to call Shipping Services api to store returned data into our local databse
                        using (var Client = new HttpClient())
                        {
                            Client.DefaultRequestHeaders.Accept.Clear();
                            string urls = strServer + "//api/PostalServices/GetPostalServices";
                            Client.DefaultRequestHeaders.Add("Accept", "application/*+xml;version=5.1");
                            Client.DefaultRequestHeaders.Add("Authorization", strToken);
                            Client.BaseAddress = new Uri(urls);
                            HttpResponseMessage responsemsg = Client.GetAsync(urls).Result;
                            strShippingServices = responsemsg.Content.ReadAsStringAsync().Result;
                        }
                        //End of Code to call Shipping Services
                        Utill.LogServiceStatus(lstOrderIds.Count);

                        objdb.LoadShippingMethods(strShippingMethodsData);
                        objdb.LoadPackagingGroup(strPackageTypes);
                        objdb.LoadPostalServices(strShippingServices);
                        GetOrdersByOrderId(lstOrderIds);
                        ClearProcessedAndCancelledOrders();
                        //SyncMessages();
                    }
                    catch (ThreadAbortException threadEx)
                    {
                        Utill.LogError(threadEx, "ThreadAbortException in SyncData call");
                        StartServiceOnLoad();
                    }
                    catch (Exception ex)
                    {
                        Utill.LogError(ex, "Main Exception While Creating API Call");
                    }
                }
            }
            finally
            {
                timer.Start();
            }
        }

        private void BindGrid()
        {
            try
            {
                DataTable objDt = objdb.GetPendingOrSentEmail("Pending");
                if (objDt.Rows.Count > 0)
                {
                    for (int i = 0; i < objDtBindGrid.Rows.Count; i++)
                    {
                        DataView dv = new DataView(objDt);
                        dv.RowFilter = "OrderId <> '" + Convert.ToString(objDtBindGrid.Rows[i]["OrderId"]) + "'";
                        objDt = dv.ToTable();
                    }
                    if (objDt.Rows.Count > 0)
                    {
                        objDtBindGrid.Merge(objdb.CreateCustomDataSource(objDt));
                    }

                    //btnAutoSend.Enabled = true;
                    //btnSendSelected.Enabled = true;
                    //btnDeleteSelected.Enabled = true;
                }
                else
                {
                    //btnAutoSend.Enabled = false;
                    //btnSendSelected.Enabled = false;
                    //btnDeleteSelected.Enabled = false;


                }
            }
            catch (Exception ex)
            {
                Utill.LogHistory(ex.Message, Guid.NewGuid());
                // MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AutoSendEmailsForProcessedOrders()
        {
            DataTable objDt = objdb.GetPendingOrSentEmail("Pending");
            //     CheckForIllegalCrossThreadCalls = false;
            if (objDt.Rows.Count > 0)
            {
                objDt = objdb.CreateCustomDataSource(objDt);
                for (int i = 0; i < objDt.Rows.Count; i++)
                {
                    try
                    {
                        string strSelectedEmailID = "", strNumOrderID = "", strOrderID = "", strSubject = "", strID = "";
                        strIdsToDeleteOrUpdate = ""; strNumOrderIds = "";
                        objSendEmail = new SendEmailEntity();
                        strSelectedEmailID = Convert.ToString(objDt.Rows[i]["To Mail"]);
                        strNumOrderID = Convert.ToString(objDt.Rows[i]["NumOrderId"]);
                        strOrderID = Convert.ToString(objDt.Rows[i]["OrderId"]);
                        strSubject = Convert.ToString(objDt.Rows[i]["Subject"]);
                        strID = Convert.ToString(objDt.Rows[i]["ID"]);

                        strIdsToDeleteOrUpdate = strIdsToDeleteOrUpdate == "" ? strID : strIdsToDeleteOrUpdate + "," + strID;
                        strNumOrderIds = strNumOrderIds == "" ? strNumOrderID : strNumOrderIds + "," + strNumOrderID;
                        strOrderIds = strOrderIds == "" ? strOrderID : strOrderIds + "," + strOrderID;

                        //If its not an email call then we dont need other details then id to delete and numOrderIds to log
                        if (!string.IsNullOrEmpty(strSelectedEmailID))
                        {
                            if (!Convert.ToBoolean(Convert.ToString(objDt.Rows[i]["IsMailSent"])))
                            {
                                lstEmails = new List<SendEmailEntity>();
                                objSendEmail.EmailID = strSelectedEmailID;
                                objSendEmail.NumberOrderID = strNumOrderID;
                                objSendEmail.OrderID = strOrderID;
                                objSendEmail.SentSubject = strSubject;
                                objSendEmail.ID = strID;
                                objSendEmail.Source = Convert.ToString(objDt.Rows[i]["Source"]);
                                objSendEmail.SubSource = Convert.ToString(objDt.Rows[i]["SubSource"]);
                                lstEmails.Add(objSendEmail);
                                if (lstEmails.Count>0)
                                {
                                    Utill.LogHistory("GridBinded for sending Orders", new Guid());
                                    objEmail.SendEmailForDispatchedOrders(lstEmails, ref objDt);
                                    lstEmails = new List<SendEmailEntity>();
                                }                                              
                            }
                        }
                    }
                    catch { throw; }
                }               
            }
            else
            {
                lstEmails = new List<SendEmailEntity>();              
            }

        }

        // <summary>
        // This is used to handle messages coming from Messages Accounts table in the database.
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void SyncMessages()
        {//StackOverflowException ex=new StackOverflowException();
            // Utill.LogError(ex, "inside sync message method");

            try
            {



                timer.Stop();
                DataTable objMessageAccountsForEmails = new DataTable();
                objMessageAccountsForEmails = objdb.GetMessageAccount("EMAIL");
                DataTable objDtRules = new DataTable();
                objDtRules = objdb.GetAddUpdateRules("GET", new Guid(), new Guid(), "", "", 1, true, "00000000-0000-0000-0000-000000000000");

                //now getting token for ebay users

                GetEbayMessages(objDtRules);

                if (objMessageAccountsForEmails.Rows.Count > 0)
                {


                    for (int Account = 0; Account < objMessageAccountsForEmails.Rows.Count; Account++)
                    {
                        string strAccountName = objMessageAccountsForEmails.Rows[Account].Field<string>("AccountName");
                        Guid AccountID = objMessageAccountsForEmails.Rows[Account].Field<Guid>("ID");
                        string strHostName = objMessageAccountsForEmails.Rows[Account].Field<string>("HostName");
                        string strUserName = objMessageAccountsForEmails.Rows[Account].Field<string>("UserName");
                        string strPassword = objMessageAccountsForEmails.Rows[Account].Field<string>("Password");
                        using (ImapClient client = new ImapClient(strHostName, 993, strUserName, strPassword, AuthMethod.Login, true))
                        {
                            int intEmailCondition = -1, intTicketCondition = -1, intAddressCondition = -1;
                            var inbox = client.DefaultMailbox;
                            IEnumerable<uint> lstMessageUID;
                            lstMessageUID = client.Search(SearchCondition.Unseen(), inbox);

                            string strMessageType = "EMAIL";
                            //   string strAccountName = "Amazon";

                            IEnumerable<System.Net.Mail.MailMessage> ListMessages = client.GetMessages(lstMessageUID);
                            string strResult = "";
                            try
                            {
                                // need to assign somewhere here to assign folders to the new messages.
                                if (ListMessages.Count() > 0)
                                {

                                    for (int i = 0; i < ListMessages.Count(); i++)
                                    {
                                        if (objDtRules.Rows.Count > 0)
                                        {
                                            string strCustomCondition = "";


                                            //  string[] strCondition = objDtRules.Columns.OfType<DataColumn>().Select(Convert.ToString(k => k.ColumnName[4].ToString().ToArray());
                                            //   string[] strRuleConditionText = objDtRules.Columns.OfType<DataColumn>().Select(k => k.ToString().ToArray());
                                             //   string[] strRuleConditionText = objDtRules.AsEnumerable().Select(x => x.Field<string>("ConditionText")).ToArray();
                                            for (int j = 0; j < objDtRules.Rows.Count; j++)
                                            {
                                                if (Convert.ToBoolean(objDtRules.Rows[j]["IsActive"]))
                                                {
                                                    strCustomCondition = objDtRules.Rows[j]["ConditionText"] + " " + objDtRules.Rows[j]["Condition"];
                                                    // if the rule contains emails in it.
                                                    if (objDtRules.Rows[j]["ConditionText"].ToString().ToUpper().TrimStart().Contains("EMAIL"))
                                                    {
                                                        if (objDtRules.Rows[j]["ConditionText"].ToString().ToUpper().Trim().Contains("EMAIL CONTAINS"))
                                                        {
                                                            //intEmailCondition = 0;
                                                            if (Convert.ToBoolean(ListMessages.ElementAt(i).From.Address.ToUpper().Contains(objDtRules.Rows[j]["Condition"].ToString().ToUpper())))
                                                            {
                                                                objdb.GetAddUpdateMessages(ListMessages.ElementAt(i), "INSERT", lstMessageUID.ElementAt(i).ToString(), strMessageType, ListMessages.ElementAt(i).From.Address.ToString(), ListMessages.ElementAt(i).Subject.ToString(), ListMessages.ElementAt(i).Body.ToString(), objDtRules.Rows[j].Field<Guid>("FolderID"), AccountID);
                                                                intEmailCondition = 1;
                                                                break;
                                                            }
                                                        }
                                                        else if (objDtRules.Rows[j]["ConditionText"].ToString().ToUpper().Trim().Contains("EMAIL BEGINS WITH"))
                                                        {
                                                            //  intEmailCondition = 0;
                                                            if (Convert.ToBoolean(ListMessages.ElementAt(i).From.Address.ToUpper().StartsWith(objDtRules.Rows[j]["Condition"].ToString().ToUpper())))
                                                            {
                                                                objdb.GetAddUpdateMessages(ListMessages.ElementAt(i), "INSERT", lstMessageUID.ElementAt(i).ToString(), strMessageType, ListMessages.ElementAt(i).From.Address.ToString(), ListMessages.ElementAt(i).Subject.ToString(), ListMessages.ElementAt(i).Body.ToString(), objDtRules.Rows[j].Field<Guid>("FolderID"), AccountID);
                                                                intEmailCondition = 1;
                                                                break;
                                                            }

                                                        }
                                                        else if (objDtRules.Rows[j]["ConditionText"].ToString().ToUpper().Trim().Contains("EMAIL ENDS WITH"))
                                                        {
                                                            // intEmailCondition = 0;
                                                            if (Convert.ToBoolean(ListMessages.ElementAt(i).From.Address.ToUpper().EndsWith(objDtRules.Rows[j]["Condition"].ToString().ToUpper())))
                                                            {
                                                                objdb.GetAddUpdateMessages(ListMessages.ElementAt(i), "INSERT", lstMessageUID.ElementAt(i).ToString(), strMessageType, ListMessages.ElementAt(i).From.Address.ToString(), ListMessages.ElementAt(i).Subject.ToString(), ListMessages.ElementAt(i).Body.ToString(), objDtRules.Rows[j].Field<Guid>("FolderID"), AccountID);
                                                                intEmailCondition = 1;
                                                                break;
                                                            }

                                                        }


                                                    }
                                                    else if (objDtRules.Rows[j]["ConditionText"].ToString().ToUpper().TrimStart().Contains("TICKET"))
                                                    {
                                                        //intTicketCondition = 0;
                                                        if (objDtRules.Rows[j]["ConditionText"].ToString().ToUpper().Trim().Contains("TICKET CHANNEL EQUALS"))
                                                        {
                                                            if (objDtRules.Rows[j]["Condition"].ToString().ToUpper() == strAccountName.ToUpper())
                                                            {
                                                                objdb.GetAddUpdateMessages(ListMessages.ElementAt(i), "INSERT", lstMessageUID.ElementAt(i).ToString(), strMessageType, ListMessages.ElementAt(i).From.Address.ToString(), ListMessages.ElementAt(i).Subject.ToString(), ListMessages.ElementAt(i).Body.ToString(), objDtRules.Rows[j].Field<Guid>("FolderID"), AccountID);
                                                                intTicketCondition = 1;
                                                                break;
                                                            }
                                                        }

                                                    }
                                                    else if (objDtRules.Rows[j]["ConditionText"].ToString().ToUpper().TrimStart().Contains("FROM ADDRESS"))
                                                    {
                                                        //  intAddressCondition = 0;

                                                        if (objDtRules.Rows[j]["Condition"].ToString().ToUpper().Trim() == ListMessages.ElementAt(i).From.Address.ToUpper().ToString().Trim())
                                                        {
                                                            objdb.GetAddUpdateMessages(ListMessages.ElementAt(i), "INSERT", lstMessageUID.ElementAt(i).ToString(), strMessageType, ListMessages.ElementAt(i).From.Address.ToString(), ListMessages.ElementAt(i).Subject.ToString(), ListMessages.ElementAt(i).Body.ToString(), objDtRules.Rows[j].Field<Guid>("FolderId"), AccountID);
                                                            intAddressCondition = 1;
                                                            break;
                                                        }



                                                    }


                                                }

                                            }
                                            if (intEmailCondition != 1 && intTicketCondition != 1 && intAddressCondition != 1)
                                            {
                                                objdb.GetAddUpdateMessages(ListMessages.ElementAt(i), "INSERT", lstMessageUID.ElementAt(i).ToString(), strMessageType, ListMessages.ElementAt(i).From.Address.ToString(), ListMessages.ElementAt(i).Subject.ToString(), ListMessages.ElementAt(i).Body.ToString(), new Guid("00000000-0000-0000-0000-000000000000"), AccountID);

                                            }

                                        }

                                    }

                                }
                            }
                            catch (Exception exception)
                            {
                                Utill.LogError(exception, "Sync Messages");
                                throw;
                            }
                        }
                    }
                }


            }
            finally
            {
                timer.Start();
            }


        }
        private void GetEbayMessages(DataTable objDtRules)
        {

            Guid EbayUserID = new Guid();
            string strEbayUserName = string.Empty;
            try
            
            {

                string strExternalMessageID = string.Empty, strItemID = string.Empty;
                string EbayMessageSender, EbayMessageSubject, EbayMessageBody, strResponseData = string.Empty, EbayAuthToken, EbayMessageUID, strMessageType = string.Empty;
                //int[] EbayMessageUID;
                int intEmailCondition = -1, intCount = -1;
                bool IsEbayMessageRead, IsEbayMessageReplySent;
                //string[] MessageID = null;

                DataTable objEbayUsers = new DataTable();
                DataTable objEbayMessageID = new DataTable();

                objEbayUsers = objdb.GetEbayUsers("GET", Guid.Empty, Guid.Empty, "", "");

                for (int users = 0; users < objEbayUsers.Rows.Count; users++)
                {
                    var client = new RestClient("https://www.google.com");
                    EbayUserID = new Guid(objEbayUsers.Rows[users]["EbayUserID"].ToString());
                    strEbayUserName = objEbayUsers.Rows[users]["UserName"].ToString();
                    Guid AccountID = new Guid(objEbayUsers.Rows[users]["AccountID"].ToString());
                    objEbayMessageID = objdb.GetMessageIDsForMessages(AccountID);

                    //for Sandbox Ebay Account
                    if (Convert.ToBoolean(objEbayUsers.Rows[users]["IsEbaySandbox"].ToString()))
                    {
                        client = new RestClient("https://api.sandbox.ebay.com/ws/api.dll");
                    }
                    else
                    {
                        client = new RestClient("https://api.ebay.com/ws/api.dll");
                    }

                    var request = new RestRequest(Method.POST);
                    EbayAuthToken = objEbayUsers.Rows[users]["EbayAuthToken"].ToString();
                    request.AddHeader("X-EBAY-API-SITEID", "3");
                    request.AddHeader("X-EBAY-API-COMPATIBILITY-LEVEL", "1073");
                    request.AddHeader("X-EBAY-API-CALL-NAME", "GetMyMessages");
                    request.AddHeader("X-EBAY-API-APP-NAME", objEbayUsers.Rows[users]["AppKey"].ToString());
                    request.AddHeader("X-EBAY-API-DEV-NAME", objEbayUsers.Rows[users]["DevKey"].ToString());
                    request.AddHeader("X-EBAY-API-CERT-NAME", objEbayUsers.Rows[users]["CertKey"].ToString());
                    string strxml = @"<?xml version=""1.0"" encoding=""utf-8""?>
   <GetMyMessagesRequest xmlns=""urn:ebay:apis:eBLBaseComponents"">
   <RequesterCredentials>
   <eBayAuthToken>" + EbayAuthToken + @"</eBayAuthToken>
   </RequesterCredentials>
   <ErrorLanguage>en_US</ErrorLanguage>
<WarningLevel>High</WarningLevel>
<DetailLevel>ReturnHeaders</DetailLevel>
<FolderID>0</FolderID>
</GetMyMessagesRequest>";
                    request.AddParameter("text/xml", strxml, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    strResponseData = response.Content;


                    if (GetSubString(strResponseData, "<Ack>", "</Ack>").ToUpper() == "SUCCESS")
                    {


                        //EbayMessageSender = GetSubString(strResponseData, "<Sender>", "</Sender>");
                        //EbayMessageSubject = GetSubString(strResponseData, "<Subject>", "</Subject>");
                        XmlDocument xmlDocument = new XmlDocument();
                        //var items = from item1 in xdoc.Descendants("Message") select item1;
                        TextReader reader = new StringReader(strResponseData);
                        xmlDocument.Load(reader);
                        XmlNodeList xmlNodeList = xmlDocument.GetElementsByTagName("Message");
                        // string[] MessageID = new string[xmlNodeList.Count];
                        string MessageIDs = string.Empty;
                        int c = 0;
                        foreach (XmlNode node in xmlNodeList)
                        {

                            if (node.HasChildNodes)
                            {
                                intCount = intCount + 1;
                                string EbayMessage = node.OuterXml.ToString();
                                //EbayMessageSender = GetSubString(EbayMessage, "<Sender>", "</Sender>");
                                //EbayMessageSubject = GetSubString(strResponseData, "<Subject>", "</Subject>");
                                //EbayMessageUID[i] = Convert.ToInt32(GetSubString(strResponseData, "<MessageID>", "</MessageID>"));
                                // MessageID = MessageID + "<MessageID>" + GetSubString(EbayMessage, "<MessageID>", "</MessageID>") + "</MessageID>" + Environment.NewLine;
                                string IsRead = GetSubString(EbayMessage, "<Read>", "</Read>");

                                if (IsRead.ToUpper() == "FALSE")
                                {
                                    c = c + 1;
                                    string strMessageID = GetSubString(EbayMessage, "<MessageID>", "</MessageID>");

                                    if (objEbayMessageID.Rows.Count > 0)
                                    {

                                        DataView dv = new DataView(objEbayMessageID);
                                        dv.RowFilter = "MessageUID=" + strMessageID;

                                        if (!(dv.ToTable().Rows.Count > 0))
                                        {
                                            //if (intCount == xmlNodeList.Count-1)
                                            //{
                                            //    MessageIDs = MessageIDs + "<MessageID>" + strMessageID + "</MessageID>";    
                                            //}
                                            //else
                                            //{
                                            //    MessageIDs = MessageIDs + "<MessageID>" + strMessageID + "</MessageID>" + ",";
                                            //}
                                            MessageIDs = MessageIDs + "<MessageID>" + strMessageID + "</MessageID>" + ",";
                                            // MessageID[intCount] = "<MessageID>" + strMessageID + "</MessageID>";

                                        }
                                    }
                                    else
                                    {
                                        MessageIDs = MessageIDs + "<MessageID>" + strMessageID + "</MessageID>" + ",";
                                        //if (intCount == xmlNodeList.Count-1)
                                        //{
                                        //    MessageIDs = MessageIDs + "<MessageID>" + strMessageID + "</MessageID>";
                                        //}
                                        //else
                                        //{
                                        //    MessageIDs = MessageIDs + "<MessageID>" + strMessageID + "</MessageID>" + ",";
                                        //}
                                    }

                                }



                            }
                        }
                        if (intCount > 10)
                        {
                            if (MessageIDs.Length > 0)
                            {
                                MessageIDs = MessageIDs.Remove(MessageIDs.Length - 1);
                            }
                            if (!string.IsNullOrEmpty(MessageIDs))
                            {
                                string[] MessageID = MessageIDs.Split(',');

                                int MessageDetailRequestCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(MessageID.Length / 10)));
                                int LastBatchLength = Convert.ToInt32(MessageID.Length % 10);
                                if (LastBatchLength > 0)
                                {
                                    MessageDetailRequestCount++;
                                }
                                int j = 0;
                                for (int i = 1; i <= MessageDetailRequestCount; i++)
                                {


                                    var MessageDetailsRequest = new RestRequest(Method.POST);
                                    MessageDetailsRequest.AddHeader("X-EBAY-API-SITEID", "3");
                                    MessageDetailsRequest.AddHeader("X-EBAY-API-COMPATIBILITY-LEVEL", "1073");
                                    MessageDetailsRequest.AddHeader("X-EBAY-API-CALL-NAME", "GetMyMessages");
                                    MessageDetailsRequest.AddHeader("X-EBAY-API-APP-NAME", objEbayUsers.Rows[users]["AppKey"].ToString());
                                    MessageDetailsRequest.AddHeader("X-EBAY-API-DEV-NAME", objEbayUsers.Rows[users]["DevKey"].ToString());
                                    MessageDetailsRequest.AddHeader("X-EBAY-API-CERT-NAME", objEbayUsers.Rows[users]["CertKey"].ToString());
                                    string strMessageID = string.Empty;
                                    if (i < MessageDetailRequestCount)
                                    {
                                        while (j < (10 * i))
                                        {
                                            strMessageID = strMessageID + MessageID[j] + Environment.NewLine;
                                            j++;
                                        }
                                    }
                                    else
                                    {
                                        int intLoopCount;
                                        if (LastBatchLength != 0)
                                        {
                                            intLoopCount = LastBatchLength;

                                        }
                                        else
                                        {
                                            intLoopCount = 10 * i;
                                        }
                                        while (j < (intLoopCount))
                                        {
                                            strMessageID = strMessageID + MessageID[j] + Environment.NewLine;
                                            j++;
                                        }

                                    }


                                    string strXmlForMessageDetails = @"<?xml version=""1.0"" encoding=""utf-8""?>
   <GetMyMessagesRequest xmlns=""urn:ebay:apis:eBLBaseComponents"">
   <RequesterCredentials>
   <eBayAuthToken>" + EbayAuthToken + @"</eBayAuthToken>
                    </RequesterCredentials>
                    <ErrorLanguage>en_US</ErrorLanguage>
<WarningLevel>High</WarningLevel>
<DetailLevel>ReturnMessages</DetailLevel>
 <MessageIDs>" + strMessageID + @"</MessageIDs>
</GetMyMessagesRequest>";
                                    MessageDetailsRequest.AddParameter("text/xml", strXmlForMessageDetails, ParameterType.RequestBody);
                                    IRestResponse MessageDetailResponse = client.Execute(MessageDetailsRequest);
                                    string strMessageDetailResponseData = MessageDetailResponse.Content;
                                    if (GetSubString(strMessageDetailResponseData, "<Ack>", "</Ack>").ToUpper() == "SUCCESS")
                                    {

                                        TextReader MessageDeatilReader = new StringReader(strMessageDetailResponseData);
                                        xmlDocument.Load(MessageDeatilReader);

                                        XmlNodeList xmlNodeListMessageDetails = xmlDocument.GetElementsByTagName("Message");

                                        foreach (XmlNode node in xmlNodeListMessageDetails)
                                        {

                                            if (node.HasChildNodes)
                                            {
                                                string EbayMessage = node.OuterXml.ToString();
                                                EbayMessageSender = GetSubString(EbayMessage, "<Sender>", "</Sender>");
                                                EbayMessageSubject = GetSubString(EbayMessage, "<Subject>", "</Subject>");
                                                EbayMessageUID = GetSubString(EbayMessage, "<MessageID>", "</MessageID>");
                                                IsEbayMessageRead = Convert.ToBoolean(GetSubString(EbayMessage, "<Read>", "</Read>"));
                                                //string strMessageDetailText = GetSubString(EbayMessage, "<Text>", "</Text>");
                                                string strConvertedHTMLTagMessageBody = GetSubString(EbayMessage, "<Text>", "</Text>").Replace("&lt;", "<");
                                                string strConvertedHTMLMessageBody = strConvertedHTMLTagMessageBody.Replace("&gt;", ">");
                                                string strFinalConvertedHTMLMessageBody = strConvertedHTMLMessageBody.Replace("&quot;", "'");
                                                EbayMessageBody = strFinalConvertedHTMLMessageBody;
                                                //EbayMessageBody = GetPlainTextFromHtml(strFinalConvertedHTMLMessageBody);
                                                IsEbayMessageReplySent = Convert.ToBoolean(GetSubString(EbayMessage, "<Replied>", "</Replied>"));
                                                strExternalMessageID = GetSubString(EbayMessage, "<ExternalMessageID>", "</ExternalMessageID>");
                                                strMessageType = GetSubString(EbayMessage, "<MessageType>", "</MessageType>");
                                                strItemID = GetSubString(EbayMessage, "<ItemID>", "</ItemID>");
                                                //saving only those ebay messages which are unread
                                                if (IsEbayMessageRead == false)
                                                {
                                                    if (objDtRules.Rows.Count > 0)
                                                    {
                                                        string strCustomCondition = "";
                                                        //  string[] strCondition = objDtRules.Columns.OfType<DataColumn>().Select(Convert.ToString(k => k.ColumnName[4].ToString().ToArray());
                                                        //   string[] strRuleConditionText = objDtRules.Columns.OfType<DataColumn>().Select(k => k.ToString().ToArray());
                                                        //   string[] strRuleConditionText = objDtRules.AsEnumerable().Select(x => x.Field<string>("ConditionText")).ToArray();
                                                        for (int k = 0; k < objDtRules.Rows.Count; k++)
                                                        {
                                                            if (Convert.ToBoolean(objDtRules.Rows[k]["IsActive"]))
                                                            {
                                                                strCustomCondition = objDtRules.Rows[k]["ConditionText"] + " " + objDtRules.Rows[k]["Condition"];
                                                                // if the rule contains emails in it.
                                                                if (objDtRules.Rows[k]["ConditionText"].ToString().ToUpper().TrimStart().Contains("SENDER"))
                                                                {
                                                                    if (objDtRules.Rows[k]["ConditionText"].ToString().ToUpper().Trim().Contains("SENDER CONTAINS"))
                                                                    {
                                                                        //intEmailCondition = 0;
                                                                        if (Convert.ToBoolean(EbayMessageSender.ToUpper().Contains(objDtRules.Rows[k]["Condition"].ToString().ToUpper())))
                                                                        {
                                                                            objdb.GetAddUpdateMessages(new System.Net.Mail.MailMessage(), "INSERT", EbayMessageUID, strMessageType, EbayMessageSender, EbayMessageSubject, EbayMessageBody, objDtRules.Rows[k].Field<Guid>("FolderID"), AccountID, EbayUserID, strExternalMessageID, strItemID);
                                                                            intEmailCondition = 1;
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                else if (objDtRules.Rows[k]["ConditionText"].ToString().ToUpper().TrimStart().Contains("EBAY ACCOUNT"))
                                                                {
                                                                    if (objDtRules.Rows[k]["ConditionText"].ToString().ToUpper().Trim().Contains("EBAY ACCOUNT EQUALS"))
                                                                    {
                                                                        if (strEbayUserName.ToUpper()==objDtRules.Rows[k]["Condition"].ToString().ToUpper())
                                                                        {
                                                                            objdb.GetAddUpdateMessages(new System.Net.Mail.MailMessage(), "INSERT", EbayMessageUID, strMessageType, EbayMessageSender, EbayMessageSubject, EbayMessageBody, objDtRules.Rows[k].Field<Guid>("FolderID"), AccountID, EbayUserID, strExternalMessageID, strItemID);
                                                                            intEmailCondition = 1;
                                                                            break;
                                                                        }

                                                                    }
                                                                }
                                                               
                                                            }
                                                        }
                                                        if (intEmailCondition != 1)
                                                        {
                                                            objdb.GetAddUpdateMessages(new System.Net.Mail.MailMessage(), "INSERT", EbayMessageUID, strMessageType, EbayMessageSender, EbayMessageSubject, EbayMessageBody, new Guid("00000000-0000-0000-0000-000000000000"), AccountID, EbayUserID, strExternalMessageID, strItemID);

                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }


                        }






                        // if bracket close for success
                    }

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        private string GetPlainTextFromHtml(string strHtml)
        {
            string htmlTagPattern = "<.*?>";
            var regexCss = new Regex("(\\<script(.+?)\\</script\\>)|(\\<style(.+?)\\</style\\>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            //  htmlString = regexCss.Replace(htmlString, string.Empty);
            strHtml = Regex.Replace(strHtml, regexCss.ToString(), "", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            //htmlString = htmlString.Replace("\\s","");
            strHtml = Regex.Replace(strHtml, htmlTagPattern, string.Empty, RegexOptions.IgnorePatternWhitespace);
            strHtml = Regex.Replace(strHtml, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
            strHtml = strHtml.Replace("&nbsp;", Environment.NewLine);
            char tab = '\u0009';
            strHtml = strHtml.Replace(tab.ToString(), "");
            return strHtml;
        }
        public string GetSubString(string strCompleteString, string strStartString, string strEndString)
        {
            try
            {
                string strData = string.Empty;
                int intStartOfString = strCompleteString.IndexOf(strStartString) + strStartString.Length;
                int intEndOfString = strCompleteString.IndexOf(strEndString);
                if (intEndOfString != -1 && intStartOfString != -1)
                {
                    strData = strCompleteString.Substring(intStartOfString, intEndOfString - intStartOfString);
                }


                return strData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method will get the details of order items by order id
        /// </summary>
        /// <param name="OrderIds">List of order ids to get it's item's detials to store that in local db</param>
        private void GetOrdersByOrderId(List<Guid> OrderIds)
        {
            List<Guid> currentOrderID = new List<Guid>();
            HttpResponseMessage responsemsg = new HttpResponseMessage();
            int CountIfException = 0;
            Guid OrderID = new Guid();
            int intCountOfError = 0;

            //This variable is to determine how many times Goto will execute, currently we are using 2 times of total records we can increase that as per our requirement
            int intCheckErrorTimes = OrderIds.Count * 2;

        //A label set to try to read the value of order agin if there is error.
        RestartLoopIfException:
            try
            {
                if (OrderIds.Count > 0)
                {
                    try
                    {
                        Console.WriteLine("Total Orderids = " + OrderIds.Count.ToString());
                        for (int i = CountIfException; i < OrderIds.Count; i++)
                        {
                            string strOrderData = "";
                            CountIfException = i;
                            OrderID = OrderIds[i];
                            string strOrderID = Convert.ToString(OrderID);
                            using (var Client = new HttpClient())
                            {
                                Client.DefaultRequestHeaders.Accept.Clear();
                                string urls = strServer + "//api/Orders/GetOrderById?pkOrderId=" + OrderIds[i];
                                Client.DefaultRequestHeaders.Add("Accept", "application/*+xml;version=5.1");
                                Client.DefaultRequestHeaders.Add("Authorization", strToken);
                                Client.BaseAddress = new Uri(urls);
                                responsemsg = Client.GetAsync(urls).Result;
                                Console.WriteLine("CurrentCount = {0}  OrderID = {1} Status = {2}", i, OrderIds[i], responsemsg.StatusCode);
                                strOrderData = responsemsg.Content.ReadAsStringAsync().Result;
                                dynamic Data = JsonConvert.DeserializeObject(strOrderData);

                                if (Data["Code"] != null && Data["Message"] != null)
                                {
                                    string strErrorResponse = string.Empty;
                                    strErrorResponse = Convert.ToString(Data["Message"]);
                                    throw new Exception("Error in item details api call response, Error:- " + strErrorResponse);
                                }
                                else if (strOrderData != null && strOrderData.ToUpper() != "NULL" && Data["OrderId"] != null)
                                {
                                    objdb.LoadJson(strOrderData, OrderIds[i], OrderIds.Count, i);
                                }
                            }
                            intCountOfError++;
                        }
                    }
                    catch (HttpRequestException RequestEx)
                    {
                        Console.WriteLine("HttpRequestException Inner Exception = {0} Exception Message = {1} ", RequestEx.InnerException, RequestEx.Message);
                        Utill.LogError(RequestEx, string.Format("HttpRequestException While reading OrderId {0} ", OrderID), OrderIds.Count, CountIfException);
                        if (intCountOfError < intCheckErrorTimes)
                        {
                            intCountOfError++;
                            goto RestartLoopIfException;
                        }
                    }
                    catch (TaskCanceledException TaskEx)
                    {
                        Console.WriteLine("TaskCanceledException Inner Exception = {0} Exception Message = {1}", TaskEx.InnerException, TaskEx.Message);
                        Utill.LogError(TaskEx, string.Format("TaskCanceledException While reading OrderId {0} ", OrderID), OrderIds.Count, CountIfException);
                        if (intCountOfError < intCheckErrorTimes)
                        {
                            intCountOfError++;
                            goto RestartLoopIfException;
                        }
                    }
                    catch (WebException WebEx)
                    {
                        Console.WriteLine("WebException Inner Exception = {0} Exception Message = {1}", WebEx.InnerException, WebEx.Message);
                        Utill.LogError(WebEx, string.Format("WebException While reading OrderId {0} ", OrderID), OrderIds.Count, CountIfException);
                        if (intCountOfError < intCheckErrorTimes)
                        {
                            intCountOfError++;
                            goto RestartLoopIfException;
                        }
                    }
                }
            }
            catch (ThreadAbortException threadEx)
            {
                Utill.LogError(threadEx, "ThreadAbortException in GetOrdersByOrderId call");
                StartServiceOnLoad();
                Thread.ResetAbort();
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Exception OrderId = {0}", OrderID);
                Utill.LogError(ex, string.Format("Main Exception During HTTP Request While reading OrderId {0} ", OrderID), OrderIds.Count, CountIfException);
                if (intCountOfError < intCheckErrorTimes)
                {
                    if (ex.Message.ToUpper().Contains("ERROR IN ITEM DETAILS API CALL"))
                    { CountIfException++; }
                    intCountOfError++;
                    goto RestartLoopIfException;
                }
            }
        }

        /// <summary>
        /// To get the list of cancelled or processed orders so that we could clear that from our local db
        /// </summary>
        public void ClearProcessedAndCancelledOrders()
        {
            Guid OrderId = new Guid();
            List<clsCancelledAndProcessedOrders> lstOrderIds = new List<clsCancelledAndProcessedOrders>();
            try
            {
                //string strQuery = " SELECT DISTINCT o.pkOrderId from [order] o LEFT JOIN [Order_LifeHistory] olh ON olh.fkOrderId = o.pkOrderId AND olh.UpdatedBy = 'Dispatch Manager' WHERE dReceievedDate > current_timestamp - 1 AND bProcessed = 'TRUE' AND olh.fkOrderId IS NULL";
                string strCancelledQuery = "SELECT pkOrderID from [order] o WHERE dReceievedDate > current_timestamp - 30 AND HoldOrCancel = 'TRUE' AND bProcessed = 'TRUE'";
                string strProcessedQuery = "SELECT DISTINCT o.pkOrderId from [order] o LEFT JOIN [Order_LifeHistory] olh ON olh.fkOrderId = o.pkOrderId AND olh.UpdatedBy = 'Dispatch Manager' WHERE dProcessedOn > current_timestamp - 0.25 AND bProcessed = 'TRUE' AND olh.fkOrderId IS NULL";
                string urls = strServer + "//api/Dashboards/ExecuteCustomScriptQuery?script=";

                using (var Client = new HttpClient())
                {
                    string strUrl = urls + strCancelledQuery;
                    Client.DefaultRequestHeaders.Accept.Clear();
                    Client.DefaultRequestHeaders.Add("Accept", "application/*+xml;version=5.1");
                    Client.DefaultRequestHeaders.Add("Authorization", strToken);
                    Client.BaseAddress = new Uri(strUrl);
                    HttpResponseMessage responsemsg = Client.GetAsync(strUrl).Result;
                    string strOrderData = responsemsg.Content.ReadAsStringAsync().Result;
                    dynamic objData = JsonConvert.DeserializeObject(strOrderData);
                    string strOrderIDs = Convert.ToString(objData["Results"]);
                    lstOrderIds.AddRange(JsonConvert.DeserializeObject<List<clsCancelledAndProcessedOrders>>(strOrderIDs));
                }
                using (var Client = new HttpClient())
                {
                    string strUrl = urls + strProcessedQuery;
                    Client.DefaultRequestHeaders.Accept.Clear();
                    Client.DefaultRequestHeaders.Add("Accept", "application/*+xml;version=5.1");
                    Client.DefaultRequestHeaders.Add("Authorization", strToken);
                    Client.BaseAddress = new Uri(strUrl);
                    HttpResponseMessage responsemsg = Client.GetAsync(strUrl).Result;
                    string strOrderData = responsemsg.Content.ReadAsStringAsync().Result;
                    dynamic objData = JsonConvert.DeserializeObject(strOrderData);
                    string strOrderIDs = Convert.ToString(objData["Results"]);
                    lstOrderIds.AddRange(JsonConvert.DeserializeObject<List<clsCancelledAndProcessedOrders>>(strOrderIDs));
                }
                string strOrderId = string.Empty;
                for (int i = 0; i < lstOrderIds.Count; i++)
                {
                    strOrderId = strOrderId == string.Empty ? Convert.ToString(lstOrderIds[i].pkOrderId) : strOrderId + "," + Convert.ToString(lstOrderIds[i].pkOrderId);
                    //OrderId = lstOrderIds[i].pkOrderId;
                    //objdb.ClearCancelledAndProcessedOrders(OrderId);
                }
                if (strOrderId != string.Empty)
                    objdb.ClearCancelledAndProcessedOrders(strOrderId);
            }
            catch (HttpRequestException RequestEx)
            {
                Console.WriteLine("HttpRequestException in ClearProcessedAndCancelledOrders Inner Exception = {0} Exception Message = {1} ", RequestEx.InnerException, RequestEx.Message);
                Utill.LogError(RequestEx, string.Format("HttpRequestException in ClearProcessedAndCancelledOrders While reading OrderId {0} ", OrderId), lstOrderIds.Count);
            }
            catch (TaskCanceledException TaskEx)
            {
                Console.WriteLine("TaskCanceledException in ClearProcessedAndCancelledOrders Inner Exception = {0} Exception Message = {1}", TaskEx.InnerException, TaskEx.Message);
                Utill.LogError(TaskEx, string.Format("TaskCanceledException in ClearProcessedAndCancelledOrders While reading OrderId {0} ", OrderId), lstOrderIds.Count);
            }
            catch (WebException WebEx)
            {
                Console.WriteLine("WebException in ClearProcessedAndCancelledOrders Inner Exception = {0} Exception Message = {1}", WebEx.InnerException, WebEx.Message);
                Utill.LogError(WebEx, string.Format("WebException in ClearProcessedAndCancelledOrders While reading OrderId {0} ", OrderId), lstOrderIds.Count);
            }
        }
    }





    /// <summary>
    /// Class for custom exceptions which may get as response of apis and we will log that in txt
    /// </summary>
    public class CustomException : Exception
    {
        public CustomException(string strErrorMsg)
            : base(strErrorMsg)
        {

        }
    }
}
