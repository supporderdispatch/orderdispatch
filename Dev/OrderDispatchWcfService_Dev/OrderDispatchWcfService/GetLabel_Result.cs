//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OrderDispatchWcfService
{
    using System;
    
    public partial class GetLabel_Result
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string Barcode { get; set; }
        public System.Guid Orderid { get; set; }
    }
}
