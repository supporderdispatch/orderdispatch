﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderDispatchWcfService
{
    class ShippingMethods
    {
        public string Vendor { get; set; }
        public List<ShippingMethod> PostalServices { get; set; }
    }
}
