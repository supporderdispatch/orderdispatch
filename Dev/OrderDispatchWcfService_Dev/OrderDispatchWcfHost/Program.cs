﻿using System;
using System.ServiceModel;
namespace OrderDispatchWcfHost
{
    class Program
    {
        static void Main()
        {
            try
            {
                using (ServiceHost host = new ServiceHost(typeof(OrderDispatchWcfService.OrderDispatch)))
                {
                 //   host.Close();
                    host.Open();
                    Console.WriteLine("Dev Service started @ " + DateTime.Now.ToString());

              
                    OrderDispatchWcfService.OrderDispatch obj = new OrderDispatchWcfService.OrderDispatch();
                    obj.StartServiceOnLoad();
                    //obj.AutoSendEmailsForProcessedOrders();
                  
                    //  Console.ReadLine();
                    
                }
            }
            catch(Exception ex)
            {
                string sterError = ex.Message;
            }
        }
    }
}
