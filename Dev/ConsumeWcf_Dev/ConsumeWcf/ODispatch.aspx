﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ODispatch.aspx.cs" Inherits="ConsumeWcf.ODispatch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Service Status</title>
    <link rel="icon" type="image/png" href="images/favicon.ico" />
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <script>
        var TimePeriod = setInterval(SetData, 120000);
        var TimeToReloadPage = setInterval(ReloadPageIfError, 30000);
            $(function () {
                SetData()
            });

        //This method is to refresh/reload the page again(if error occurred), this method will get called after every 3 seconds to check error.
        function ReloadPageIfError() {
            var Counter = 0;

            //Adding 3 second wait to refresh the page
            var CurrentReloadTime = setInterval(function () {
                Counter++;
                if (Counter == 3) {
                    var PageUrl = location.href;
                    location.href = PageUrl;
                }
                    clearInterval(CurrentReloadTime);
            }, 10000)
        }

        //This method will get service execution time and will display that on browser
        function SetData() {
            $.ajax({
                type: "POST",
                contentType: "application/json; utf-8",
                datatype: "json",
                url: "/ODispatch.aspx/GetServiceRunningDetails",
                success: function (data) {
                    var Data = data.d;
                    var arrData = Data.split(',');
                    if (Data == "Reload") {
                        ReloadPageIfError();
                    }
                    else {
                        $("#trTimeDetails").show();
                        var htmlToShow = "Last execution : " + "<span style='color:green'>" + arrData[0] + "</span>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Next execution : " + "<span style='color:green'>" + arrData[1] + "</span>";
                        $("#TimeDetails").html(htmlToShow);
                        clearInterval(TimeToReloadPage);
                    }
                },
                error: function (xhr, status, error) {
//                    alert("xhr " + xhr.statusText + " Status " + status + " error" + error);
                }
            });
        }
        function WantClearData()
        {
            if(confirm("Are you sure that you want to clear data?")==true)
            {
                return true;
            }
            else
            {
                return false
            }
        }
    </script>
    <style>
        #tblWcfServiceDetails {
            margin: 0 auto;
            margin-top: 100px;
        }

        #btnStop:hover {
            border: 3px solid red;
            background: transparent;
        }

        .custom_button {
            padding: 7px 20px;
            margin: 7px;
        }

        #btnStart:hover, #btnClearData:hover {
            border: 3px solid green;
            background: transparent;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="tblWcfServiceDetails" class="wrapper-table" border="1" style="width: 62%;">
                <tr style="display: none; text-align: center" id="trMsg">
                    <td colspan="2">
                        <label id="lblMsg" style="font-size: 20px"></label>
                    </td>
                </tr>
                <tr runat="server" id="trmsg" visible="false">
                    <td colspan="2" style="text-align: center">
                        <b>
                            <asp:Label Text="" runat="server" ID="lblMessage" Font-Size="X-Large" />
                        </b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <img src="images/OD-NewLogo.png" />
                    </td>
                </tr>
                <tr style="text-align: center">
                    <td style="width: 412px;">
                        <b>Current Service State:</b>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblCurrentState" Font-Size="Large"></asp:Label>
                    </td>
                </tr>
                <tr style="text-align: center; display: none" id="trTimeDetails">
                    <td colspan="2">
                        <%--<asp:Label runat="server" ID="TimeDetails" Font-Size="Large"></asp:Label>--%>
                        <label id="TimeDetails" style="font-size: 20px"></label>
                    </td>
                </tr>
                <tr style="text-align: center">
                    <td colspan="2">
                        <asp:Button runat="server" Text="Clear All Record" ID="btnClearData" CssClass="custom_button" OnClientClick="return WantClearData()" OnClick="btnClearData_Click" />
                        <asp:Button Text="Start Service To Sync Data" CssClass="custom_button" ID="btnStart" runat="server" OnClick="btnStart_Click" />
                        <asp:Button Text="Stop Service" CssClass="custom_button" ID="btnStop" runat="server" OnClick="btnStop_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
